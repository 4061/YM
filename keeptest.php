<?php
// Генерация XML запроса к серверу, пример.
$xml = '
<?xml version="1.0" encoding="windows-1251"?>
<RK7Query>
  <RK7CMD CMD="GetSystemInfo"/>
</RK7Query>';
// Создаем подключение 
$ch = curl_init();
//URL, с которым будет производиться операция. Значение этого параметра также может быть задано в вызове функции curl_init(). 
    curl_setopt($ch, CURLOPT_URL, '213.221.44.107:5485/rk7api/v0/xmlinterface.xml');
//При установке этого параметра в ненулевое значение CURL будет возвращать результат, а не выводить его.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//Массив с HTTP заголовками.
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8"));
//При установке этого параметра в ненулевое значение результат будет включать полученные заголовки.
    curl_setopt($ch, CURLOPT_HEADER, 0);
//Передаёт строку, содержащую полные данные для передачи операцией HTTP "POST".
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
//При установке этого параметра в ненулевое значение будет отправлен HTTP запрос методом POST типа application/x-www-form-urlencoded, используемый браузерами при отправке форм.
    curl_setopt($ch, CURLOPT_POST, 1);
//Установите этот параметр в ноль, чтобы запретить проверку сертификата удаленного сервера
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
// загрузка страницы и выдача её браузеру
    $response = curl_exec($ch);
// завершение сеанса и освобождение ресурсов
    curl_close($ch);
    echo $response;
