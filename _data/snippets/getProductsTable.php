id: 142
source: 1
name: getProductsTable
properties: 'a:0:{}'

-----

$prods = [];
$json = [];
$delivery = $_GET['delivery'];
if ($delivery){
    $delivery_placeholder = " AND modx_ms2_orders.delivery = $delivery";
}
$sql = "
SELECT
    product_id,
    price,
    count,
    name,
    order_id,
    modx_site_content.parent as category,
    modx_ms2_orders.address as address_id,
    modx_ms2_order_addresses.index as restourant_id
FROM
    modx_ms2_order_products,
    modx_site_content,
    modx_ms2_orders,
    modx_ms2_order_addresses
WHERE
    modx_ms2_order_products.product_id = modx_site_content.id AND
    modx_ms2_orders.id = order_id AND
    modx_ms2_order_addresses.id = modx_ms2_orders.address
    $delivery_placeholder
";
$info_products = $modx->query($sql);
$info_products = $info_products->fetchAll(PDO::FETCH_ASSOC);
foreach ($info_products as $key => $product){
    $prods[$product['name']][] = $product;
}
foreach ($prods as $key => $prod){
    foreach ($prods[$key] as $prod){
        $rest_id = "restourant_".$prod['restourant_id'];
        if (!$prod['restourant_id']){
            $rest_id = $rest_id.'null';
        }
        $json[$prod['name']][$rest_id] +=  $prod['count'];
        $json[$prod['name']]["price"] = $prod['price'];
        $json[$prod['name']]["parent"] = $prod['category'];
        $json[$prod['name']]["name"] = $prod['name'];
    }
}
return $json;