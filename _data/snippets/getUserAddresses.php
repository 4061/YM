id: 152
source: 1
name: getUserAddresses
properties: 'a:0:{}'

-----

//unset($_SESSION['minishop2']);
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();
    
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    
    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $profile = $modx->getObject('modUserProfile', ['website' => $token]); //получаем пользователя по token
        $user_id = $profile->get('internalKey');
        $sql = "
        SELECT 
            modx_ms2_order_addresses.street as street,
            modx_ms2_order_addresses.metro as doorphone,
            modx_ms2_order_addresses.floor as floor,
            modx_ms2_order_addresses.house_num as house_num,
            modx_ms2_order_addresses.flat_num as flat_num,
            modx_ms2_order_addresses.city as city,
            modx_ms2_order_addresses.id as id,
            modx_ms2_order_addresses.index as org_id,
            modx_ms2_order_addresses.entrance as entrance
        FROM
            modx_ms2_order_addresses,modx_ms2_orders
        WHERE
            modx_ms2_order_addresses.user_id = $user_id AND
            modx_ms2_orders.delivery = 2 AND
            modx_ms2_orders.address = modx_ms2_order_addresses.id
        GROUP BY city, flat_num, house_num
        ORDER BY `modx_ms2_order_addresses`.`id` DESC
        LIMIT 10";
        $info_user= $modx->query($sql);
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        $json['addresses'] = $info_user;
        return json_encode($json);
    } else {
       ($json['data'][]['error']="Нет токена");
       return json_encode($json);
        
    }
}