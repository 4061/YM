id: 234
name: getPromoList
description: 'Список с промокодами'
category: 'Шаблоны Админки'
properties: null

-----

//modx_promo_base

$where = array(
    "activity" => 1,
    //"type:IN" => "1,4",
    "availability" => 2
    //'date_end:>' => time(),
);

$promoList = $modx->getCollection("PromoBase",$where);

$promoMass = array();
foreach($promoList as $promo){
    
    $promoMass[] = array(
        "id" => $promo->get("id_promo"),
        "name" => $promo->get("name"),
        'date_start' => $promo->get("date_start"),
    );

}

return $promoMass;