id: 214
name: setRatingComment_OLD
description: 'Сниппет для добавления комментария и рейтинга пользователем к заказу'
properties: 'a:0:{}'

-----

/* старый запрос
{
  "id_resource": 3681,
  "id_user": 31,
  "rating_value": 5,
  "comment": "Тестовый текст 22"
}
*/
// ссылка на обработчик http://dev-maxim.appsj.su/set-rating-comment.json
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    // получаем значение запроса в формате JSON
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    // получаем все заголовки
    $token = getallheaders();
    // если есть параметр авторизации (токен), то присваиваем его значение второму токену
    if ($token['Authorization']) $token2 = $token['Authorization'];
    // вырезаем первые 7 символов второго токена и передаём значение переменной токен
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    // проверяем существует ли токен
    if ($token){
        
        // получаем переданную json строку с данными
        $data = $_POST['JSON'];
        
        // проверяем указан ли был рейтинг, а так же не является ли комментарий пустым
        if ($data["rating_value"] > 0 && $data["comment"] != "") {
            // формируем SQL запрос, что бы понять создан ли уже комментарий текущий пользователь для указанного объекта
            $sql = 'SELECT id_rating FROM modx_rating WHERE id_resource = ' .$data['id_resource']. ' AND id_user = ' .$data['id_user'];
            $elementQuery = $modx->query($sql);
            $element = $elementQuery->fetchAll(PDO::FETCH_ASSOC)[0];
            // если создал ..
            if($element){
                
                $sql = 'UPDATE modx_rating SET `comment_rating` = "' .$data['comment']. '", `rating_value` = '.$data['rating_value'].' `date_of_editing` = "' .date("Y-m-d H:i:s"). '" WHERE id_rating = ' .$element['id_rating'];
                $elementQuery = $modx->query($sql);
                /*// .. то получаем его объект и меняем данные
                $rating = $modx->getObject("Rating",$element['id_rating']);
                // устанавливаем дату и время изменения комментария
                $rating->set("date_of_editing",date("Y-m-d H:i:s"));*/
                
                // параметры для ответа
                $result["id"] = $element['id_rating'];
                $result["action"] = "edit";
                
            } else {
                // если комментарий отсутствует, то создаём его
                $rating = $modx->newObject("Rating");
                $rating->set("active",1);
                
                // заносим нужные нам зачения
                $rating->set("id_resource",$data['id_resource']);
                $rating->set("id_user",$data['id_user']);
                $rating->set("comment_rating",$data['comment']);
                $rating->set("rating_value",$data['rating_value']);
                $rating->save();
                
                // получаем id толкьо что созданного комментария
                $sql = 'SELECT id_rating FROM modx_rating WHERE id_resource = ' .$data['id_resource']. ' AND id_user = ' .$data['id_user'];
                $elementQueryLast = $modx->query($sql);
                $result["id"] = $elementQueryLast->fetchAll(PDO::FETCH_ASSOC)[0]["id_rating"];
                
                $result["action"] = "created";
            }
            
            
            // формируем запрос для получения всех оценок по данному ресурсу
            $sql = 'SELECT rating_value FROM modx_rating WHERE id_resource = ' .$data['id_resource']. " AND active = 1";
            $allRatingElements = $modx->query($sql);
            // обнуляем счётчик и сумму для рейтинга
            $count = $summRating = 0;
            // перебераем полученные значения
            foreach($allRatingElements as $value) {
                // считаем сумму рейтинга
                $summRating += $value['rating_value'];
                // увеличиваем счётчик
                $count++;
            }
            // проверяем не равна ли сумма рейтинга 0
            if($summRating) {
                // если не равна, то рассчитываем рейтинг по формуле
                $allRating = round(($summRating/$count), 1);
            } else {
                // если равна 0, то присваиваем 0 для общего рейтинга
                $allRating = 0;
            }
            
            // получа объект товара для изменения рейтинга
            $sql = "UPDATE modx_ms2_product_options SET value = " .$allRating. " WHERE product_id = " .$data['id_resource'] ." AND `key` = 'item_rating'";
            $modx->query($sql);
            
            // заносим новый рейтинг в ответ
            $result["allRating"] = $allRating;
            $result["countRating"] = $count;
        
        // если комментарий есть, а рейтинг не выставлен, возвращаем ошибку    
        } else if ($data["rating_value"] > 0) {
            $result["error"] = "Вы не указали комментарий для объекта";
        // если рейтинг есть, а комментарий отсутствует, возвращаем ошибку    
        } else if ($data["comment"] != "") {
            $result["error"] = "Вы не указали рейтинг для объекта";
        // если ничего не указано, возвращаем ошибку
        } else {
            $result["error"] = "Вы не указали ни комментарий, ни рейтинг для объекта";
        }
        
        $json["result"] = $result;

        // возвращаем результат
        return json_encode($json);
        
    }
}