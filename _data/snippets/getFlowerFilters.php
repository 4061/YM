id: 269
source: 1
name: getFlowerFilters
properties: 'a:0:{}'

-----

$colors = $modx->query("SELECT DISTINCT flower_color as title FROM modx_ms2_products");
$colors = $colors->fetchAll(PDO::FETCH_ASSOC);
$i = 1;
foreach ($colors as $key=> $color){
    if ($color['title'] != null && $color['title'] != ""){
        $colors[$key]["id"] = $i;
        $i++;
    } else {
        unset($colors[$key]);
    }
}
$response["colors"] = array_values($colors);
$origins = $modx->query("SELECT DISTINCT origin as title FROM modx_ms2_products");
$origins = $origins->fetchAll(PDO::FETCH_ASSOC);
$i = 1;
foreach ($origins as $key=> $origin){
    if ($origin['title'] != null && $origin['title'] != ""){
        $origins[$key]["id"] = $i;
        $i++;
    } else {
        unset($origins[$key]);
    }

}
$response["origins"] = array_values($origins);
return json_encode($response);