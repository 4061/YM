id: 253
source: 1
name: getOrdersAdmin
snippet: "$pdoFetch = new pdoFetch($modx);\n$orders = array(\n    'class' => 'msOrder',\n    'leftJoin' => [\n        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],\n        'Receiver' => ['class' => 'modUserProfile', 'on' => 'msOrder.user_id = Receiver.internalKey'],\n        'CourierOrders' => ['class' => 'CourierOrders', 'on' => 'msOrder.id = CourierOrders.order_id'],\n        'Flight' => ['class' => 'CourierFlights', 'on' => 'CourierOrders.flight_id = Flight.id'],\n        'Courier' => ['class' => 'modUserProfile', 'on' => 'Flight.courier_id = Courier.internalKey'],\n        'Payment' => ['class'=> 'msPayment', 'on'=> 'Payment.id = msOrder.payment'],\n        'CourierOrderStatuses' => ['class'=> 'CourierOrderStatuses', 'on'=> 'CourierOrderStatuses.id = CourierOrders.status_id']\n    ],\n    'sortdir' => \"desc\",\n    'return' => 'data',\n    'select' => [\n        'msOrder' => '\n            msOrder.id,\n            msOrder.cost',\n        'Address' => '\n            Address.country, \n            Address.city, \n            Address.street, \n            Address.house_num, \n            Address.street, \n            Address.entrance, \n            Address.floor, \n            Address.flat_num, \n            Address.metro as doorphone',\n        'Receiver' => '\n            Receiver.fullname as receiver_name,\n            Receiver.mobilephone as receiver_mobilephone',\n        'CourierOrders' => 'flight_id',\n        'Courier' => 'Courier.fullname as courier_name',\n        'Payment' => 'Payment.name as payment',\n        'CourierOrderStatuses' => '\n            CourierOrderStatuses.name as status_name,\n            CourierOrderStatuses.description as status_description'\n    ],\n    'limit' => 10000,\n    'where' => [\"flight_id <> 0\"]\n);\n$pdoFetch->setConfig($orders);\n$orders = $pdoFetch->run();\nreturn $orders;"
properties: 'a:0:{}'
static: 1
static_file: /core/components/courier/elements/snippets/getOrdersAdmin.php
content: "$pdoFetch = new pdoFetch($modx);\n$orders = array(\n    'class' => 'msOrder',\n    'leftJoin' => [\n        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],\n        'Receiver' => ['class' => 'modUserProfile', 'on' => 'msOrder.user_id = Receiver.internalKey'],\n        'CourierOrders' => ['class' => 'CourierOrders', 'on' => 'msOrder.id = CourierOrders.order_id'],\n        'Flight' => ['class' => 'CourierFlights', 'on' => 'CourierOrders.flight_id = Flight.id'],\n        'Courier' => ['class' => 'modUserProfile', 'on' => 'Flight.courier_id = Courier.internalKey'],\n        'Payment' => ['class'=> 'msPayment', 'on'=> 'Payment.id = msOrder.payment'],\n        'CourierOrderStatuses' => ['class'=> 'CourierOrderStatuses', 'on'=> 'CourierOrderStatuses.id = CourierOrders.status_id']\n    ],\n    'sortdir' => \"desc\",\n    'return' => 'data',\n    'select' => [\n        'msOrder' => '\n            msOrder.id,\n            msOrder.cost',\n        'Address' => '\n            Address.country, \n            Address.city, \n            Address.street, \n            Address.house_num, \n            Address.street, \n            Address.entrance, \n            Address.floor, \n            Address.flat_num, \n            Address.metro as doorphone',\n        'Receiver' => '\n            Receiver.fullname as receiver_name,\n            Receiver.mobilephone as receiver_mobilephone',\n        'CourierOrders' => 'flight_id',\n        'Courier' => 'Courier.fullname as courier_name',\n        'Payment' => 'Payment.name as payment',\n        'CourierOrderStatuses' => '\n            CourierOrderStatuses.name as status_name,\n            CourierOrderStatuses.description as status_description'\n    ],\n    'limit' => 10000,\n    'where' => [\"flight_id <> 0\"]\n);\n$pdoFetch->setConfig($orders);\n$orders = $pdoFetch->run();\nreturn $orders;"

-----


$pdoFetch = new pdoFetch($modx);
$orders = array(
    'class' => 'msOrder',
    'leftJoin' => [
        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],
        'Receiver' => ['class' => 'modUserProfile', 'on' => 'msOrder.user_id = Receiver.internalKey'],
        'CourierOrders' => ['class' => 'CourierOrders', 'on' => 'msOrder.id = CourierOrders.order_id'],
        'Flight' => ['class' => 'CourierFlights', 'on' => 'CourierOrders.flight_id = Flight.id'],
        'Courier' => ['class' => 'modUserProfile', 'on' => 'Flight.courier_id = Courier.internalKey'],
        'Payment' => ['class'=> 'msPayment', 'on'=> 'Payment.id = msOrder.payment'],
        'CourierOrderStatuses' => ['class'=> 'CourierOrderStatuses', 'on'=> 'CourierOrderStatuses.id = CourierOrders.status_id']
    ],
    'sortdir' => "desc",
    'return' => 'data',
    'select' => [
        'msOrder' => '
            msOrder.id,
            msOrder.cost',
        'Address' => '
            Address.country, 
            Address.city, 
            Address.street, 
            Address.house_num, 
            Address.street, 
            Address.entrance, 
            Address.floor, 
            Address.flat_num, 
            Address.metro as doorphone',
        'Receiver' => '
            Receiver.fullname as receiver_name,
            Receiver.mobilephone as receiver_mobilephone',
        'CourierOrders' => 'flight_id',
        'Courier' => 'Courier.fullname as courier_name',
        'Payment' => 'Payment.name as payment',
        'CourierOrderStatuses' => '
            CourierOrderStatuses.name as status_name,
            CourierOrderStatuses.description as status_description'
    ],
    'limit' => 10000,
    'where' => ["flight_id <> 0"]
);
$pdoFetch->setConfig($orders);
$orders = $pdoFetch->run();
return $orders;