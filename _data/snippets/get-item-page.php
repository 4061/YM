id: 223
name: get-item-page
description: 'Получение данные по конкретной странице товара/объекта'
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    // получаем значение запроса в формате JSON
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    // получаем все заголовки
    $token = getallheaders();
    // если есть параметр авторизации (токен), то присваиваем его значение второму токену
    if ($token['Authorization']) $token2 = $token['Authorization'];
    // вырезаем первые 7 символов второго токена и передаём значение переменной токен
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    // проверяем существует ли токен
    if ($token){
        //"resource_id": 1805
        $resourceId = $_POST['JSON']["resource_id"];
        
        //return $resourceId;
        // формируем запрос на получение объекта и всех его параметров
        $sql = 'SELECT
                    modx_site_content.id,
                    modx_site_content.pagetitle,
                    modx_site_content.description,
                    modx_site_content.content,
                    modx_ms2_products.image,
                    modx_ms2_products.thumb
                FROM
                    modx_site_content,modx_ms2_products
                WHERE
                    modx_site_content.id = '.$resourceId.'
                AND
                    modx_site_content.published = 1
                AND
                    modx_ms2_products.id = modx_site_content.id';
        // обращаемся в базу данных            
        $result = $modx->query($sql);
        $data["object"] = $result->fetch(PDO::FETCH_ASSOC);
        // формируем два пустых массива с доп.параметрами
        $data["options"] = array();
        $data["rating"] = array();
        // перечень параметров (Option) для товара что необходимо вернуть вместе с объектом
        $massOptions = array("add_address","add_time_work","link_audio","link_coordinates","link_fb","link_instagram","link_odnoklassniki","link_phone","link_site","link_video","link_vk","item_rating");
        // перебераем массив параметров и заносим данные в соответствующие части массива для ответа
        foreach($massOptions as $option) {
            if($option != "item_rating"){
                $sql = 'SELECT value FROM modx_ms2_product_options WHERE product_id = '.$resourceId.' AND modx_ms2_product_options.key = "'.$option.'"';
                $result = $modx->query($sql);
                $data["options"][$option] = $result->fetch(PDO::FETCH_ASSOC)["value"];
            } else {
                $data["rating"][$option] = $result->fetch(PDO::FETCH_ASSOC)["value"];
            }
        }
        // формируем массив с рейтингом/комментриями
        $data["rating"]["rating_list"] = array();
        // формируем запрос и осуществляем обращение к БД
        $sql = 'SELECT * FROM modx_rating WHERE id_resource = '.$resourceId.' AND active = 1';
        $result = $modx->query($sql);
        // заносим полученные данные в соответствующий массив
        $data["rating"]["rating_list"] = $result->fetchALL(PDO::FETCH_ASSOC);
        // возвращаем ответ от метода
        return json_encode($data);
        
    } else {
        return json_encode(array("error"=>"Отсутствует токен"));
    }
} else {
    return json_encode(array("error"=>"Неверный формат данных"));
}