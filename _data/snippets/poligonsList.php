id: 230
name: poligonsList
description: 'Получить список полигонов с ценами'
category: 'Шаблоны Админки'
properties: 'a:0:{}'

-----

$profiles = $modx->getCollection("PolygonsBase");

$userListMass = array();
foreach($profiles as $key => $profile){
    
    if($profile->get("internalKey") != 1){
        $userListMass[$key]['id'] = $profile->get("Id");
        $userListMass[$key]['Restourant_id'] = $profile->get("Restourant_id");
        $restorant = $modx->getObject("modResource",$userListMass[$key]['Restourant_id']);
        $userListMass[$key]['Restourant_title'] = $restorant->get("pagetitle");
        $userListMass[$key]['Restourant_name'] = $profile->get("Restourant_name");
        $userListMass[$key]['Polygon_coords'] = $profile->get("Polygon_coords");
        $userListMass[$key]['Polygon_coords_short'] = substr($profile->get("Polygon_coords"), 0, 100)."...";
        $userListMass[$key]['min_count_cost'] = $profile->get("min_count_cost");
        $userListMass[$key]['delivery_price'] = $profile->get("delivery_price");
        $userListMass[$key]['free_shipping'] = $profile->get("free_shipping");
        $userListMass[$key]['status'] = $profile->get("status");
    }
    
}

return $userListMass;