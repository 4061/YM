id: 212
name: getTagList
description: 'Сниппет для формирования общего списка тегов'
properties: null

-----

// ссылка для обращения - http://dev-maxim.appsj.su/tag-list.json

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    // получаем значение запроса в формате JSON
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    // получаем все заголовки
    $token = getallheaders();
    // если есть параметр авторизации (токен), то присваиваем его значение второму токену
    if ($token['Authorization']) $token2 = $token['Authorization'];
    // вырезаем первые 7 символов второго токена и передаём значение переменной токен
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    // проверяем существует ли токен
    if ($token){
        
        $sql = 'SELECT id,pagetitle FROM modx_site_content WHERE template = 4 AND parent = 3505';
        // отправляем запрос
        $result = $modx->query($sql);
        // получаем ответ
        $list = $result->fetchAll(PDO::FETCH_ASSOC);
        // создаём пустой массив где будут хранится тэги
        $tagList = array();
        // перебераем полученные значения
        foreach ($list as $key => $tag) {
            // формируем массив для элемента
            $tagList[$key]["tag_id"] = $tag['id'];
            $tagList[$key]["title"] = $tag['pagetitle'];
            // формируем запрос к БД
            $sql = 'SELECT image FROM modx_ms2_products WHERE id = '.$tag['id'];
            // отправляем запрос
            $result = $modx->query($sql);
            // получаем ответ и вынимаем оттуда картинку
            $tagList[$key]["image"] = $result->fetchAll(PDO::FETCH_ASSOC)[0]['image'];
        }
        
        $json["tagList"] = $tagList;

        // возвращаем результат
        return json_encode($json);
    }
}