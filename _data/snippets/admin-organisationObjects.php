id: 225
name: admin_organisationObjects
description: 'Формирование списка всех организаций'
category: 'Шаблоны Админки'
properties: 'a:0:{}'

-----

//<optgroup label="Семейство «Паслёновые»">

$sql = "SELECT id,pagetitle FROM modx_site_content WHERE parent = 1802 AND template = 0";
$result = $modx->query($sql);
$parentOrgs = $result->fetchAll(PDO::FETCH_ASSOC);

$parentMassOrg = $parentMassOrgNew = $massOrg = array();

foreach ($parentOrgs as $key => $parentOrg) {
    $parentMassOrg[$parentOrg['id']] = $parentOrg['pagetitle'];
}

//print_r($parentMassOrg);

$sql = "SELECT id,pagetitle,parent FROM modx_site_content WHERE template = 18 AND published = 1";
$result = $modx->query($sql);
$organisationList = $result->fetchAll(PDO::FETCH_ASSOC);

//print_r($organisationList);

$parentMassOrgNew = array();
$parentMassOrgNew[0] = array();
$parentMassOrgNew[0]['name'] = "Без категории";
$parentMassOrgNew[0]['data'] = array();

//$organisationElement['parent'] = 3333;

//echo is_array($parentMassOrgNew[$organisationElement['parent']]);

foreach ($organisationList as $key => $organisationElement) {
    
    if(in_array($organisationElement['parent'],array_keys($parentMassOrg))) {
        
        if(!is_array($parentMassOrgNew[$organisationElement['parent']])) $parentMassOrgNew[$organisationElement['parent']] = array();
        if(!is_array($parentMassOrgNew[$organisationElement['parent']]['data'])) $parentMassOrgNew[$organisationElement['parent']]["data"] = array();
        $parentMassOrgNew[$organisationElement['parent']]["name"] = $parentMassOrg[$organisationElement['parent']];
        
        array_push($parentMassOrgNew[$organisationElement['parent']]["data"],$organisationElement);
    } else {
        array_push($parentMassOrgNew[0]["data"],$organisationElement);
    }
}
$sql = "SELECT orgs FROM modx_promo_base WHERE id_promo = {$id_promo}";
$result = $modx->query($sql);
$getParam = $result->fetch(PDO::FETCH_ASSOC)['orgs'];

$selectList = explode(",",$getParam);

foreach ($parentMassOrgNew as $key => $parents) {
    
    $massListName[] = array("tag" => "optgroup", "title" => $parents['name']);
    foreach ($parents["data"] as $resource) {
        $select = (in_array($resource['id'], $selectList))? "selected" : "";
        $massListName[] = array("tag" => "option","id" => $resource['id'], "title" => $resource['pagetitle'], "selected" => $select);
    }
    
}

return $massListName;