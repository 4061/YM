id: 241
source: 1
name: 1c-update-statuses
category: flowerheaven
properties: 'a:0:{}'

-----

require MODX_CORE_PATH."vendor/autoload.php";
$generator = new Picqer\Barcode\BarcodeGeneratorJPG();
$SoftjetSync = $modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/');
$action = $SoftjetSync->getOrderActions();
$sql = "
SELECT 
    modx_ms2_orders.id as modx_order,
    modx_ms2_order_addresses.id_integration_order as 1c_order
FROM 
    modx_ms2_orders, modx_ms2_order_addresses
WHERE
    modx_ms2_order_addresses.id = modx_ms2_orders.address AND 
    modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'7' DAY) AND 
    modx_ms2_orders.status <> 14
";
$orders = $modx->query($sql);
$orders = $orders->fetchAll(PDO::FETCH_ASSOC);
foreach ($orders as $order){
    $order = $modx->getObject("msOrder", $order["modx_order"]);
    $contacts = $modx->getObject('msOrderAddress', array('id'=> $order->address));
    $order_info = $action->getOrder($contacts->get("id_integration_order"));
    $status = $modx->getObject("msOrderStatus", array("name" => $order_info[0]["status"]));
    $order->set("status", $status->get("id"));
    $order->save();
    file_put_contents(MODX_BASE_PATH.'/uploads/barcodes/'.$contacts->get("id_integration_order").".jpg", $generator->getBarcode($contacts->get("id_integration_order"), $generator::TYPE_CODE_128));
}