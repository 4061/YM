id: 226
name: notificationsSegment
description: 'Сниппет для работы с рассылками'
properties: 'a:0:{}'

-----

$pdoFetch = new pdoFetch($modx);
$default = array(
    'class' => 'SegmentNotifications',
    'leftJoin' => [
        'sSettings' => ['class' => 'SegmentSegments', 'on' => 'sSettings.segment_id = SegmentNotifications.segment_setting_id',],
    ],
    'select' => [
        'SegmentNotifications' => 'id,title_notification,type_notification,segment_setting_id,date_of_creation,date_of_editing,date_last_notification,promo,only_new_users,time_minutes,time_hours,time_days,time_months,time_days_week,active',
        'sSettings' => '`sSettings`.`segment_name`,`sSettings`.`settings_id`',
    ],
    'sortby' => 'SegmentNotifications.id',
    'sortdir' => 'ASC',
    'groupby' => 'SegmentNotifications.id',
    'return' => 'data',
    'limit' => 1000
);
if ($search_results){
    $default["where"]["id:IN"] = $search_results;
}
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();

return $rows;