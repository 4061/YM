id: 215
name: admin_restaurantsObjects
description: 'Массив для получения уникальных значений ресторанов'
category: 'Шаблоны Админки'
properties: null

-----

$sql = "SELECT ";
$sql .= "modx_ms2_order_addresses.index, modx_site_content.pagetitle";
$sql .= " FROM modx_ms2_order_addresses,modx_site_content WHERE modx_site_content.id = modx_ms2_order_addresses.index ORDER BY modx_site_content.pagetitle";

$resources = $modx->query($sql);
$resources = $resources->fetchAll(PDO::FETCH_ASSOC);

$massList = $massListName = array();

$getParam = $_GET['restaurants'];
$selectList = explode(",",$getParam);

foreach ($resources as $resource) {
    if(!in_array($resource['pagetitle'],$massList)) {
        array_push($massList,$resource['pagetitle']);
        $select = (in_array($resource['index'], $selectList))? "selected" : "";
        $massListName[] = array("id" => $resource['index'], "title" => $resource['pagetitle'], "selected" => $select);
    }
}

return $massListName;