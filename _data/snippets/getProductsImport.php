id: 213
name: getProductsImport
description: 'Импорт товаров и городов. Формирование цены товара относительно города.'
properties: 'a:0:{}'

-----

// корневая директория каталога
$mainCategory = 1561;
$orgCategory = 1802;
$start = microtime(true);
// функция для получения данных
function getData($urlGet,$parameterGET){
    // добавляем ? к url
    $urlGet .= "?";
    
    // перебераем указанные параметры и добавляем в ссылку
    foreach ($parameterGET as $key => $value) {
        // собираем их в строку для GET запроса
        $urlGet .= "&".$key."=".$value;
    }
    
    // делаем запрос по полученному URL с добавленными в него GET параметрами
    return json_decode(file_get_contents($urlGet), true);
}
$subdomain = "";
/* ======== Добавляем/обновляем города ========= */
$urlGet = "https://flowers-cvety.ru/api/get-cities";
// передаём параметры для города
$parameterGET = array(
    "page" => "0",
    "sort_order" => "ASC",
    "limit" => "100"    
);

// передаём параметры для товара
$parameterGETproduct = array(
    //"prot" => "http:",
    //"dom" => "sarapul.flowers-cvety.ru",
    "page" => "0",
    "sort_order" => "ASC",
    "limit" => "1000"    
);
// передаём параметры для получения данных партнёра
$parameterGETpartners = array(
    "page" => "0",
    "sort_order" => "ASC",
    "limit" => "50"    
);
// авторизуемся как пользователь (необходима для импорта картинок)
$dataUser = array(
    'username' => 'admin', // юзер должен обладать правами менеджера магазина, как минимум
    'password' => 'TsY-4FU-8Pt-qCG', // настройку прав тут не буду описывать, равно как и безопасность
    'rememberme' => false,
    'login_context' => 'mgr' // логинимся в админку
);
$response = $modx->runProcessor('/security/login', $dataUser);
$user = $modx->getObject('modUser', array('username' => $dataUser['username']));
$modx->user = $user;

$parentsListPrice = array(0,3500,5000,10000);

$keyMax = count($parentsListPrice) - 1;
$maxPrice = 999999999;
$massCategory = array();

// код отвечающий за формирование категорий с промежутками цен
foreach ($parentsListPrice as $key => $parentsPrice) {
    $sql = 'SELECT id FROM modx_site_content WHERE longtitle = "min'.$parentsPrice.'"';
    $result = $modx->query($sql);
    $parentsId = $result->fetch(PDO::FETCH_ASSOC);
    
    $massCategory[$key] = array();
    
    if($parentsId["id"]) {
        $massCategory[$key]["id"] = $parentsId['id'];
    } else {
        if ($key < 1) {
            $parentsName = "До ".$parentsListPrice[$key + 1]." руб.";
        } else if ($key == $keyMax) {
            $parentsName = "От ".$parentsPrice." руб.";
        } else {
            $parentsName = "От ".$parentsPrice." руб. до ".$parentsListPrice[$key + 1]." руб.";
        }
        $category = $modx->newObject("msCategory");
        $category->set("pagetitle",$parentsName);
        $category->set("longtitle","min".$parentsPrice);
        $category->set("parent",$mainCategory);
        $category->set("template",3);
        $category->set("isfolder",1);
        $category->set("published",1);
        $category->save();
        $massCategory[$key]['id'] = $category->get("id");
    }
    
    $massCategory[$key]["min"] = $parentsPrice;
    $minPrice = $parentsPrice;
    
    if ($key < 1) {
        $massCategory[$key]["max"] = $parentsListPrice[$key + 1];
    } else if ($key == $keyMax) {
        $massCategory[$key]["max"] = $maxPrice;
    } else {
        $massCategory[$key]["max"] = $parentsListPrice[$key + 1];
    }
}
            
// функция на обновление опций товара
function setOptionList($data,$element,$itemId,$modx){
    $options = $data->get("options");
    $updateOption = $insert = 0;
    
    //print_r($options);
    //print_r($element);
    //echo "<hr>";
    
    if($options["tags_list"][0]) {
        $options["tags_list"][0] = json_encode($element["field_tags"], JSON_UNESCAPED_UNICODE);
        $updateOption++;
    } else {
        // заносим цену в БД для данного товара и города
        $sql = 'INSERT INTO modx_ms2_product_options ';
        $sql .= '(`product_id`, `key`, `value`) VALUES ';
        $sql .= '("'.$itemId.'", "tags_list","'.$element["field_tags"].'")';
        $modx->query($sql);
    }
    if($options["tags_addressat"][0]) {
        $options["tags_addressat"][0] = json_encode($element["field_bq_addressat"], JSON_UNESCAPED_UNICODE);
        $updateOption++;
    } else {
        // заносим цену в БД для данного товара и города
        $sql = 'INSERT INTO modx_ms2_product_options ';
        $sql .= '(`product_id`, `key`, `value`) VALUES ';
        $sql .= '("'.$itemId.'", "tags_addressat","'.$element["field_bq_addressat"].'")';
        $modx->query($sql);
    }
    if($options["tags_reason"][0]) {
        $options["tags_reason"][0] = json_encode($element["field_bq_reason"], JSON_UNESCAPED_UNICODE);
        $updateOption++;
    } else {
        // заносим цену в БД для данного товара и города
        $sql = 'INSERT INTO modx_ms2_product_options ';
        $sql .= '(`product_id`, `key`, `value`) VALUES ';
        $sql .= '("'.$itemId.'", "tags_reason","'.$element["field_bq_reason"].'")';
        $modx->query($sql);
    }
            
    if($updateOption) {
        $data->set('options', $options);
	    $data->save();
    }
}

// ссылка на метод получения партнёров
$urlGetPart = "https://flowers-cvety.ru/api/get-partners";
// обращаемся к функции для получения городов
$partners = getData($urlGetPart,$parameterGETpartners);
// формируем пустой массив для размещения в него массива с партнёрами
$partnersMass = array();
// перебераем массив с партнёрами, формируем массив где ключ является uid массива
foreach($partners as $partner) {
    $partnersMass[$partner['uid']] = $partner;
}

// обращаемся к функции для получения городов
$answer = getData($urlGet,$parameterGET);

//print_r($answer);

// перебераем массив с городами, который пришёл нам в ответе
foreach($answer as $key => $element){
    // обращаемся к БД, что бы найти существует ли город с указанным названием
    $sql = 'SELECT id FROM modx_site_content WHERE template = 18 AND introtext = "'.$element["tid"].'"';
    $result = $modx->query($sql);
    $itemId = $result->fetch(PDO::FETCH_ASSOC)['id'];
    // убераем % из параметра надбавки, что бы у нас было число
    $element['field_markup'] = str_replace("%","",$element['field_markup']);
    // проверяем получен ли ID
    if (!$itemId) {
        // формируем родительскую директорию
        $category = $modx->newObject("modResource");
        $category->set("pagetitle",$element["name"]);
        $category->set("template",0);
        $category->set("parent",$orgCategory);
        $category->set("published",1);
        $category->set("isfolder",1);
        $category->save();
        $newParent = $category->get("id");
    
        // если ID не найден, то формируем новый город
        $city = $modx->newObject("modResource");
        $city->set("pagetitle",$element["name"]);
        $city->set("introtext",$element["tid"]);
        $city->set("parent",$newParent);
        $city->set("alias",$element["name"]."-org");
        $city->set("template",18);
        $city->set("published",1);
        $city->save();
        
        // получаем ID созданного ресурса
        $itemId = $city->get("id");
        // формируем цены для созданного ресурса под указанный город
        $sql = 'INSERT INTO modx_softjetsync_price_types ';
        $sql .= '(uuid, name, currency) VALUES ';
        $sql .= '("'.$element['uid'].'", "'.$element['field_geo_subdomain'].'", "RUB")';
        $result = $modx->query($sql);
        $typeId =  $modx->lastInsertId();
        // задаём параметры города (ID из БД, цена надбавки, цена доставки)
        $city->setTvValue("tvType",$typeId);
        $city->setTvValue("tvTypeRestourant",$typeId);
        $city->setTvValue("tvTypeDelivery",$typeId);
        $city->setTvValue("tvFieldMarkup",$element['field_markup']);
        $city->setTvValue("tvFieldGeoDeliveryPrice",$element['field_markup']); // $element['field_geo_delivery_price']
        $city->setTvValue("tvEmailManager",$partnersMass[$element['uid']]['mail']);
        $city->save();
        // field_markup, field_geo_delivery_price
    } else {
        // делаем запрос на получение ID типа
        $sql = 'SELECT id FROM modx_softjetsync_price_types WHERE uuid = "'.$element['uid'].'" AND name = "'.$element['field_geo_subdomain'].'"';
        $result = $modx->query($sql);
        $typeId = $result->fetch(PDO::FETCH_ASSOC)['id'];
        // получаем объект города
        $productItem = $modx->getObject("modResource",$itemId);
        $productItem->set("alias",$element["name"]."-org");
        // задаём параметры города (ID из БД, цена надбавки, цена доставки)
        $productItem->setTvValue("tvTypeRestourant",$typeId);
        $productItem->setTvValue("tvTypeDelivery",$typeId);
        $productItem->setTvValue("tvFieldMarkup",$element['field_markup']);
        $productItem->setTvValue("tvFieldGeoDeliveryPrice",$element['field_geo_delivery_price']);
        $productItem->setTvValue("tvEmailManager",$partnersMass[$element['uid']]['mail']);
        $productItem->save();
    }
    // формируем массив с id и названием городов
    $subdomainsMassive[] = array("type" => $typeId,"subdomain" => $element['field_geo_subdomain'].".");
    
}

// перебераем сформированный массив городов
foreach ($subdomainsMassive as $subdomain) {


    /* ======== Добавляем/обновляем товары ========= */
    // ссылка для обращения к стороннему API
    $urlGet = "https://".$subdomain['subdomain']."flowers-cvety.ru/api/get-bouquet";
    
    // обращаемся к функции
    $answer = getData($urlGet,$parameterGETproduct);
    
    // если в ответе присутствуют товары с параметром img, то обрабатываем их
    if($answer["results"][0]['img']) {
        // перебераем все элементы полученного массива
        foreach($answer["results"] as $key => $element) {
            // вырезаем лишнее из значения
            $img = explode('src="',$element["img"])[1];
            $img = explode('?itok',$img)[0];
            // заносим сформированный адрес до картинки
            $answer["results"][$key]['img'] = $img;
        }
    }
    
    $maxPrice = array();
    
    // перебераем массив ответа от сайта
    foreach($answer["results"] as $key => $element){
        if(!$maxPrice[$key]) $maxPrice[$key] = $element['price'];
        if($maxPrice[$key] < $element['price']) $maxPrice[$key] = $element['price'];
        // формируем SQL запрос, что бы узнать существует ли товар с указанным названием
        $sql = 'SELECT id FROM modx_site_content WHERE class_key = "msProduct" AND pagetitle = "'.$element["title"].'"';
        $result = $modx->query($sql);
        $itemId = $result->fetch(PDO::FETCH_ASSOC)['id'];
        // делаем пометку о том что товар новый, значение по-умолчанию
        $action = "Новый";
        // проверяем, получен ли ID
        if ($itemId) {
            // если ID найден, формируем запрос к БД, что бы проверить существование цены для данного ресурса и города
            $sql = 'SELECT id FROM modx_softjetsync_prices WHERE product_id = '.$itemId.' AND type = '.$subdomain['type'];
            $result = $modx->query($sql);
            $priceId = $result->fetch(PDO::FETCH_ASSOC)['id'];
            // если ID цены найден..
            if($priceId) {
                // .. то обновляем у него цену
                $sql = 'UPDATE modx_softjetsync_prices SET price = "'.$element["price"].'" WHERE product_id = '.$itemId.' AND type = '.$subdomain['type'];
                $modx->query($sql);
            } else {
                // если ID не найден, то создаём новый показатель цены для данного ресурса и города
                $sql = 'INSERT INTO modx_softjetsync_prices ';
                $sql .= '(product_id, product_uuid, type, price) VALUES ';
                $sql .= '("'.$itemId.'", "'.$element['nid'].'","'.$subdomain['type'].'" ,"'.$element["price"].'")';
                $modx->query($sql);
            }
            // получаем объект товара (используется дальше по коду)
            $product = $modx->getObject("msProduct",$itemId);
            $product->set("parent",$mainCategory);
            $product->set("hidden_in_tree",1);
            $product->set("price",$maxPrice[$key]);
            $product->set("order_types","1,2,3,4");
            $product->save();
            // ID для ресурса на ответ
            $newId = $itemId;
            // помечаем что мы обновляем элемент
            $action = "Обновляем";
        } else {
            // формируем объект и задаём ему нужные значения
            $product = $modx->newObject("msProduct");
            $product->set("pagetitle",$element["title"]);
            $product->set("introtext",$element["summary"]);
            // $product->set("price",$element["price"]);
            $product->set("article",$element["nid"]);
            $product->set("parent",$mainCategory);
            $product->set("published",1);
            $product->set("source",2);
            $product->set("price",$maxPrice[$key]);
            $product->set("order_types","1,2,3,4");
            $product->setContent($element["body"]);
            // сохраняем
            $product->save();
            $product->set("source",2);
            $product->save();
            // получаем id созданного товара
            $newId = $product->get("id");
            
            // запускам процесс прикрепления картинки к товару по ссылке
            $response = $modx->runProcessor('gallery/upload',
            	array('id' => $newId, 'file' => $element["img"]),
            	array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/')
            );
            
            //print_r($response);
            // сброс ошибок MODX - для работы импорта в галерею
            $modx->error->reset();
            $modx->cacheManager->refresh();
            
            // заносим цену в БД для данного товара и города
            $sql = 'INSERT INTO modx_softjetsync_prices ';
            $sql .= '(product_id, product_uuid, type, price) VALUES ';
            $sql .= '("'.$itemId.'", "'.$element['nid'].'","'.$subdomain['type'].'" ,"'.$element["price"].'")';
            $modx->query($sql);
        }
        
        $maxKey = count($massCategory) - 1;
        foreach ($massCategory as $keyCat => $itemCategory){
            
            if ($itemCategory["min"] < $maxPrice[$key] && $maxPrice[$key] <= $itemCategory['max']) {
                $product->set("parent",$itemCategory["id"]);
                $product->save();
            }
            
        };
        
        $data = $product->getOne("Data");
        
        setOptionList($data,$element,$itemId,$modx);
        
        // вывод на страницу результата
        echo $key.". ID товара ".$newId.", - ".$element["title"]." (" .$action. ") - " .$subdomain['subdomain']. " - ".$element["price"]." (".$maxPrice[$key].")<br>";
    }
}
// проверяем сколько ушло времени
$time = microtime(true) - $start;
// выводим затраченное время
echo "<br>Время на выполнение импорта/обновления: ".$time." сек";