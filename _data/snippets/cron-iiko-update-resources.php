id: 193
source: 1
name: cron-iiko-update-resources
description: 'Обновляет ресурсы айко'
properties: 'a:0:{}'

-----

$iiko_id = "YaponaMP";
$iiko_pass = "A8BVoS3h";
$token = str_replace('"',"",file_get_contents('https://iiko.biz:9900/api/0/auth/access_token?user_id='.$iiko_id.'&user_secret='.$iiko_pass));
$org = json_decode( file_get_contents('https://iiko.biz:9900/api/0/organization/list?access_token='.$token.'&request_timeout=00%3A01%3A00'))[0];
$nom = json_decode( file_get_contents('https://iiko.biz:9900/api/0/nomenclature/'.$org->id.'?access_token='.$token.'&revision=0'),true);
//return 'https://iiko.biz:9900/api/0/nomenclature/'.$org->id.'?access_token='.$token.'&revision=0';
$init_catalog = array_combine(array_column($nom["products"], 'id'), $nom["products"]);
$groups = $nom['groups'];
foreach ($groups as $key => $group){
    if (!$group['isIncludedInMenu']){
        unset($groups[$key]);
    }
}
function getCatalog($parent_id_cat, $groups){
    foreach ($groups as $key=> $group){
        if ($group['parentGroup'] == $parent_id_cat){
           $app_menu[] = array("id" => $group["id"], "name"=> $group['name'], "tags"=>$group['tags']);
           $res = getCatalog($group["id"],$groups);
           if ($res){
               foreach ($res as $re){
                   $app_menu[] = $re;
               }
           }
        }
    }
    return $app_menu;
}
$groups = (getCatalog("6a31d32b-3103-432e-b4ae-9c88c0a1ec77", $groups));
foreach ($groups as $group){
    $q = array(
        'pagetitle' => $group['name'],
    );
    if ($res = $modx->getObject('modResource', $q)){
        $res->setTVValue('categoryId', $group['id']);
    } else {
        $response = $modx->runProcessor('resource/create', array(
          'class_key' => 'msCategory',
          'pagetitle' => $group['name'],
          'parent' => 1561,
          'template' => 3,
          'show_in_tree' => 1,
        ));
        $id = $response->response['object']['id'];
        $resource = $modx->getObject('modResource', array('id' => $id));
        $resource->setTVValue('categoryId', $group['id']);
    }
}
$resaveGroups = array_combine(array_column($groups, 'id'), $groups);
if ($nom['products']){
    $products = $nom['products'];
    foreach ($products as $product){
        foreach ($groups as $group){
            if ($product['parentGroup'] == $group['id']){
                $data[] = $product;
            } 
        }
    }
    $products = array_combine(array_column($data, 'id'), $data);
    //print_r($products);
    //foreach ($products as $key=> $product){
    //    if ($resaveGroups[$product["parentGroup"]]["id"] == "3db07570-8756-4664-95be-e4331636ea93"){
            //print_r($resaveGroups[$product["parentGroup"]]["tags"]);
            //print_r($resaveGroups[$product["parentGroup"]]);
    //    }
    //}
    //return 0;
    foreach ($products as $key => $product){
        $sql = "SELECT modx_ms2_product_options.product_id FROM modx_ms2_product_options WHERE modx_ms2_product_options.value = '$key'";
        $ms2_product = $modx->query($sql);
        $ms2_product = $ms2_product->fetchAll(PDO::FETCH_ASSOC);
        if ($ms2_product){
            if ($ms2_product[0]['product_id'] !=0){
                $resource = $modx->getObject('modResource',$ms2_product[0]['product_id']);
                $resource->set("pagetitle", $product['name']);
                $resource->set("alias", $product['id']);
                $resource->set("description", $product['description']);
                $resource_parent = $resource->get("parent");
                $resource_parent = $modx->getObject('modResource', $resource_parent);
                $parent_iiko_id = $resource_parent->getTVValue("categoryId");
                foreach ($resaveGroups[$parent_iiko_id]['tags'] as $group_tag_item){
                    if ($group_tag_item == "без скидки"){
                        $resource->set("price", $product['price']);
                        $resource->set("price2", $product['price']);
                        $resource->save();
                    } else {
                        $resource->set("price2", $product['price']*0.8);
                        $resource->set("price", $product['price']);
                        $resource->save();
                    }
                }
                $order_types = [];
                foreach ($product['tags'] as $product_tag){
                    if ($product_tag == "самовывоз"){
                        echo $product_tag."НАШЛО</br>";
                        array_push($order_types, 1);
                    }
                    if ($product_tag == "курьером"){
                        echo $product_tag."НАШЛО</br>";
                        array_push($order_types, 2);
                    }
                    if ($product_tag == "в зале"){
                        echo $product_tag."НАШЛО</br>";
                        array_push($order_types, 3);
                    }
                }
                $tmblr = false;
                foreach ($product['tags'] as $product_tag){
                    if ($product_tag == "без скидки"){
                        $tmblr = true;
                    }
                }
                foreach ($resaveGroups[$product["parentGroup"]]["tags"] as $key=> $parent_tag){
                    if ($parent_tag == "без скидки"){
                        $tmblr = true;
                    }
                }
                if (!$tmblr){
                    $resource->set("price2", $product['price']*0.8);
                    $resource->set("price", $product['price']);
                    $resource->save();
                } else {
                    $resource->set("price2", $product['price']);
                    $resource->set("price", $product['price']);
                    $resource->save(); 
                }
                $order_types_string = implode(",",$order_types); //типы
                $resource->set("order_types",$order_types_string);
                $resource->save();
                $res_this_id = $resource->get("id");
                $energy_oils_val = $product['fatFullAmount'];
                $energy_protein_val = $product['fiberFullAmount'];
                $energy_carbohydrates_val = $product['carbohydrateFullAmount'];
                $energy_value_val = $product['energyFullAmount'];
                $energy_allegrens_val = "-";
                $energy_size_val = (double) ($product['weight']);
                $options = array(
                    "energy_oils" => $energy_oils_val, 
                    "energy_protein" => $energy_protein_val, 
                    "energy_carbohydrates" => $energy_carbohydrates_val, 
                    "energy_value" => $energy_value_val, 
                    "energy_allergens" => $energy_allegrens_val, 
                    "energy_size" => $energy_size_val, 
                    "guid" => $product['id'],
                    "parentGroup" => $product['parentGroup']
                );
                if ($product['additionalInfo']){
                    //echo $product['additionalInfo']."</br>";
                    $options["energy_size"] = $product['additionalInfo'];
                } else {
                    $energy_size_val = str_replace(",", ".",$energy_size_val)*1000;
                    $energy_size_val = $energy_size_val." г";
                    //echo $energy_size_val."</br>";
                    $options["energy_size"] = $energy_size_val;
                }
                        $table = $modx->getTableName('msProductOption');
                        foreach ($options as $key => $option){
                            $sql = "UPDATE `modx_ms2_product_options` SET `value` = '$option' WHERE `modx_ms2_product_options`.`product_id` = '$res_this_id' AND `modx_ms2_product_options`.`key` = '$key'";
                            $modx->query($sql);
                            //echo $sql."</br>";
                        }
                //print_r($product["groupModifiers"]);
                foreach ($product["groupModifiers"] as $parent_modifier){
                    if ($parent_modifier["required"] == true && $parent_modifier["minAmount"] >=1){
                        foreach ($parent_modifier["childModifiers"] as $childModifier){
                            $modifier_id = $childModifier["modifierId"];
                            $sql = "SELECT * FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.key = 'guid' AND modx_ms2_product_options.value = '$modifier_id'";
                            $modifier_product = $modx->query($sql);
                            $modifier_product = $modifier_product->fetchAll(PDO::FETCH_ASSOC);
                            if ($modifier_product){ //если модификатор уже существует
                                $modifi_this_product_id = $modifier_product[0]["product_id"];
                                $adds_options["guid"] = $init_catalog[$modifier_id]["id"];
                                $adds_options["parentGroup"] = $init_catalog[$modifier_id]["groupId"];
                                foreach ($adds_options as $key => $option){
                                    $sql = "UPDATE `modx_ms2_product_options` SET `value` = '$option' WHERE `modx_ms2_product_options`.`product_id` = '$modifi_this_product_id' AND `modx_ms2_product_options`.`key` = '$key'";
                                    $modx->query($sql);
                                }
                                $modifi_result_id = $modifi_this_product_id;
                            } else { //если не существует
                                $response = $modx->runProcessor('resource/create', array(
                                  'class_key' => 'msProduct',
                                  'pagetitle' => $init_catalog[$modifier_id]["name"],
                                  'description' => $init_catalog[$modifier_id]["description"],
                                  'parent' => 1562,
                                  'template' => 4,
                                  'show_in_tree' => 1,
                                  'alias' => rand(100000, 999999),
                                
                                  //Данные
                                  'price' => (int)$init_catalog[$modifier_id]["price"],
                                  'article' => (int)$init_catalog[$modifier_id]["code"],
                                  'old_price' => 0,
                                  'favorite' => 0,
                                  'popular' => 0,
                                  'additive' => 0,
                                )); //создали, теперь добавляем опции
                                $adds_options["guid"] = $init_catalog[$modifier_id]["id"];
                                $adds_options["parentGroup"] = $init_catalog[$modifier_id]["groupId"];
                                $adds_response_id = $response->response['object']['id'];
                                foreach ($adds_options as $key => $option){
                                    $sql = "INSERT INTO `modx_ms2_product_options` (`id`, `product_id`, `key`, `value`) VALUES (NULL, '$adds_response_id', '$key', '$option')";
                                    $modx->query($sql);
                                }
                                $modifi_result_id = $adds_response_id;
                                echo $response->response['object']['id']." ";
                            }
                            $sql = "SELECT * FROM modx_ms2_product_links WHERE modx_ms2_product_links.link = 9 AND modx_ms2_product_links.master = '$res_this_id' AND modx_ms2_product_links.slave = '$modifi_result_id'";
                            $checkLink = $modx->query($sql);
                            $checkLink = $checkLink->fetchAll(PDO::FETCH_ASSOC);
                            if (!$checkLink){
                                $sql = "INSERT INTO `modx_ms2_product_links` (`link`, `master`, `slave`) VALUES ('9', '$res_this_id', '$modifi_result_id')";
                                $modx->query($sql);
                            }
                            echo $resource->get("pagetitle").": ".$childModifier["modifierId"]."</br>";
                        }
                    }
                }
                echo $resource->get("pagetitle")." UPDATED! ".$resource->get("id");
                echo "</br>";
            }
        } else {
            echo "новый продукт. ".$product["name"];
            echo "</br>";
            $parentGroup = $product['parentGroup'];
            $sql = "
            SELECT
                contentid as cat
            FROM
                modx_site_tmplvar_contentvalues
            WHERE
                value = '$parentGroup' 
            ";
            $cat = $modx->query($sql);
            $cat = $cat->fetchAll(PDO::FETCH_ASSOC);
            $parent = $cat[0]['cat']; //нашли parent
            if ($parent){
                //echo $parent.$product['name'];
            $description = $product['description'];
            $pagetitle = $product['name'];
            $energy_oils_val = $product['fatFullAmount'];
            $energy_protein_val = $product['fiberFullAmount'];
            $energy_carbohydrates_val = $product['carbohydrateFullAmount'];
            $energy_value_val = $product['energyFullAmount'];
            $energy_allegrens_val = "null";
            $energy_size_val = (double) ($product['weight']);
            $options = array(
                "energy_oils" => $energy_oils_val, 
                "energy_protein" => $energy_protein_val, 
                "energy_carbohydrates" => $energy_carbohydrates_val, 
                "energy_value" => $energy_value_val, 
                "energy_allergens" => $energy_allegrens_val, 
                "energy_size" => $energy_size_val, 
                "guid" => $product['id'],
                "parentGroup" => $product['parentGroup']
            );
            
                if ($product['additionalInfo']){
                    echo $product['additionalInfo']."</br>";
                    $options["energy_size"] = $product['additionalInfo'];
                } else {
                    $energy_size_val = str_replace(",", ".",$energy_size_val)*1000;
                    $energy_size_val = $energy_size_val." г";
                    echo $energy_size_val."</br>";
                    $options["energy_size"] = $energy_size_val;
                }
                    $q = array(
                'alias' => $product['id'],
            );
            if ($res = $modx->getObject('modResource', $q)) {
                echo 'Существует уже '.$res->get('id');
                echo '</br>';
            } else {
                $response = $modx->runProcessor('resource/create', array(
                  'class_key' => 'msProduct',
                  'pagetitle' => $product['name'],
                  'description' => $product['description'],
                  'parent' => (int)$parent,
                  'template' => 4,
                  'show_in_tree' => 1,
                  'alias' => rand(100000, 999999),
                
                  //Данные
                  'price' => (int)$product['price'],
                  'article' => (int)$product['code'],
                  'old_price' => 0,
                  'favorite' => 0,
                  'popular' => 0,
                  'additive' => 0,
                ));
                
                    $id = $response->response['object']['id'];
                    $jej = $response->getAllErrors();
                    foreach ($jej as $je){
                        echo $je;
                        echo '</br>';
                    }
                    echo 'созд рес. id is '.$id," Название ".$product['name']." Категория ".$parent." Описание ".$product["description"]." ЦЕНА ".$product['price']." АРТИКУЛ ".$product['code'];
                    echo '</br>';
                    if ($id){
                        $table = $modx->getTableName('msProductOption');
                        foreach ($options as $key => $option){
                            $sql = "INSERT INTO `modx_ms2_product_options` (`id`, `product_id`, `key`, `value`) VALUES (NULL, '$id', '$key', '$option')";
                            $modx->query($sql);
                        }
                    }
            }}
        }
    }
    
    
    
    
    //$sql = "SELECT modx_ms2_products.id, modx_ms2_product_options.value FROM modx_ms2_products, modx_ms2_product_options WHERE modx_ms2_products.id = modx_ms2_product_options.product_id AND modx_ms2_product_options.key = 'guid'";
    //$our_products = $modx->query($sql);
    //$our_products = $our_products->fetchAll(PDO::FETCH_ASSOC);
    $pdoFetch = new pdoFetch($modx);
    $default = array(
        'class' => 'msProduct',
        'parents' => 1561,
        'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
        'leftJoin' => ['Data' => ['class' => 'msProductData'],
            'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
            'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
            'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
            'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
            'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
            'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
            'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "guid"'],
        ],
        'select' => [
            'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle`, parent, `msProduct`.`description`',
            'Data' => '`Data`.`restourants`,`Data`.`price`,`Data`.`price2`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
            'Option1' => 'Option1.value as energy_size',
            'Option2' => 'Option2.value as energy_allergens',
            'Option3' => 'Option3.value as energy_value',
            'Option4' => 'Option4.value as energy_carbohydrates',
            'Option5' => 'Option5.value as energy_protein',
            'Option6' => 'Option6.value as energy_oils',
            'Option7' => 'Option7.value as value',
        ],
        'sortby' => 'msProduct.id',
        'sortdir' => 'ASC',
        'groupby' => 'msProduct.id',
        'return' => 'data',
        'limit' => 1000
    );
    $pdoFetch->setConfig($default);
    $our_products = $pdoFetch->run();
    echo "</br>";echo "</br>";echo "</br>";echo "</br>";
    foreach ($our_products as $our_product){
        if ($products[$our_product["value"]]){
            echo $our_product['id']." TRUE!";
            echo "</br>";
        } else {
            $res = $modx->getObject('modResource',$our_product["id"]);
            echo $res->get("id")." FALSE! ".$our_product['value'];
            echo "</br>";
            $res->remove();
        }
    }
}