id: 248
source: 1
name: getFlights
snippet: "$pdoFetch = new pdoFetch($modx);\n$orders = $pdoFetch->run();\n$flights = array(\n    'class' => 'CourierFlights',\n    'leftJoin' => [\n        'Status' => ['class'=> 'CourierFlightStatuses', 'on'=>'Status.id = CourierFlights.status_id'],\n        'modUserProfile' => ['class'=> 'modUserProfile', 'on'=> 'CourierFlights.courier_id = modUserProfile.internalKey'],\n        ],\n    'select' => [\n        'CourierFlights'=>'CourierFlights.id, CourierFlights.status_id, CourierFlights.comment, CourierFlights.courier_id, CourierFlights.date_start, CourierFlights.date_end', \n        'Status' => 'Status.name as status_name, Status.description as status_description, Status.text_color as status_text_color, Status.wrapper_color as status_wrapper_color',\n        'modUserProfile' => 'modUserProfile.fullname as courier'\n        ],\n    'sortdir' => \"ASC\",\n    'return' => 'data',\n    'limit' => 10000\n);\n$pdoFetch->setConfig($flights);\n$flights = $pdoFetch->run();\nreturn $flights;"
properties: 'a:0:{}'
static: 1
static_file: /core/components/courier/elements/snippets/getFlights.php
content: "$pdoFetch = new pdoFetch($modx);\n$orders = $pdoFetch->run();\n$flights = array(\n    'class' => 'CourierFlights',\n    'leftJoin' => [\n        'Status' => ['class'=> 'CourierFlightStatuses', 'on'=>'Status.id = CourierFlights.status_id'],\n        'modUserProfile' => ['class'=> 'modUserProfile', 'on'=> 'CourierFlights.courier_id = modUserProfile.internalKey'],\n        ],\n    'select' => [\n        'CourierFlights'=>'CourierFlights.id, CourierFlights.status_id, CourierFlights.comment, CourierFlights.courier_id, CourierFlights.date_start, CourierFlights.date_end', \n        'Status' => 'Status.name as status_name, Status.description as status_description, Status.text_color as status_text_color, Status.wrapper_color as status_wrapper_color',\n        'modUserProfile' => 'modUserProfile.fullname as courier'\n        ],\n    'sortdir' => \"ASC\",\n    'return' => 'data',\n    'limit' => 10000\n);\n$pdoFetch->setConfig($flights);\n$flights = $pdoFetch->run();\nreturn $flights;"

-----


$pdoFetch = new pdoFetch($modx);
$orders = $pdoFetch->run();
$flights = array(
    'class' => 'CourierFlights',
    'leftJoin' => [
        'Status' => ['class'=> 'CourierFlightStatuses', 'on'=>'Status.id = CourierFlights.status_id'],
        'modUserProfile' => ['class'=> 'modUserProfile', 'on'=> 'CourierFlights.courier_id = modUserProfile.internalKey'],
        ],
    'select' => [
        'CourierFlights'=>'CourierFlights.id, CourierFlights.status_id, CourierFlights.comment, CourierFlights.courier_id, CourierFlights.date_start, CourierFlights.date_end', 
        'Status' => 'Status.name as status_name, Status.description as status_description, Status.text_color as status_text_color, Status.wrapper_color as status_wrapper_color',
        'modUserProfile' => 'modUserProfile.fullname as courier'
        ],
    'sortdir' => "ASC",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($flights);
$flights = $pdoFetch->run();
return $flights;