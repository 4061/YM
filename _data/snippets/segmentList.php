id: 227
name: segmentList
category: 'Шаблоны Админки'
properties: null

-----

/*$sql = "SELECT ";
$sql .= "segment_id,settings_id,segment_name";
$sql .= " FROM modx_segment_segments";*/

$sql='SELECT 
        modx_segment_segments.segment_id,
        modx_segment_segments.settings_id,
        modx_segment_segments.segment_createdon,
        modx_segment_segments.segment_name,
        modx_segment_settings.segment_settings
    FROM 
        modx_segment_segments,modx_segment_settings
    WHERE
        modx_segment_segments.settings_id = modx_segment_settings.segment_id
    ORDER BY 
        segment_id DESC';

$resources = $modx->query($sql);
$resources = $resources->fetchAll(PDO::FETCH_ASSOC);

$massList = array();

foreach ($resources as $resource) {
    $massList[] = array("id" => $resource['segment_id'], "title" => $resource['segment_name']);
}

return $massList;