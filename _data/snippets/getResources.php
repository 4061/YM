id: 231
name: getResources
description: 'Сниппет для получения списка товаров'
properties: 'a:0:{}'

-----

$sql = "SELECT tmplvarid,value,contentid,modx_site_content.pagetitle,modx_site_content.id FROM modx_site_tmplvar_contentvalues,modx_site_content WHERE tmplvarid IN (37,40,41) AND modx_site_content.id = modx_site_tmplvar_contentvalues.contentid";
$query = $modx->query($sql);
$prisesTvs = $query->fetchAll(PDO::FETCH_ASSOC);

$massTypesTv = $massIdOrg = array();
foreach ($prisesTvs as $prisesTv) {
    $massTypesTv[$prisesTv['pagetitle']][$prisesTv['value']] = $prisesTv['tmplvarid'];
    $massIdOrg[$prisesTv['pagetitle']] = $prisesTv['id'];
}

//print_r($massTypesTv);

$pdoFetch = new pdoFetch($modx);

$default = array(
    'class' => 'SoftjetsyncPriceTypes',
    'select' => [
        'SoftjetsyncPriceTypes' => 'id,name',
    ],
    'sortby' => 'SoftjetsyncPriceTypes.id',
    'sortdir' => 'ASC',
    'groupby' => 'SoftjetsyncPriceTypes.id',
    'return' => 'data',
    'limit' => 10000
);

$pdoFetch->setConfig($default);
$priseTypesQuery = $pdoFetch->run();

foreach($priseTypesQuery as $priseType) {
    $priseTypes[$priseType['id']] = $priseType['name'];
}

$defaultCategory = ($defaultCategory)? ",".$defaultCategory : "";
$categoryes = ($categoryes && $categoryes != "")? ",".$categoryes : $defaultCategory;

$default = array(
    'class' => 'msProduct',
    'parents' => "-1739,-3505,-360,-3706,-1562".$categoryes,
    'where' => ['class_key' => 'msProduct', 'template' => 4, 'deleted' => 0],
    'leftJoin' => [
        'resCategory' => ['class' => 'modResource', 'on' => 'resCategory.id = msProduct.parent'],
    ],
    'select' => [
        'msProduct' => 'id,pagetitle,image,published',
        'resCategory' => '`resCategory`.`id` as parent_id,`resCategory`.`pagetitle` as parent_pagetitle,`resCategory`.`parent` as parent_parent',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => 1000
);
if ($search_results){
    $default["where"]["id:IN"] = $search_results;
}
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();

// получаем массив стикеров
$stickers = $modx->getCollection("msProduct",array("template" => 42));
// перебераем стикеры
foreach ($stickers as $stycker) {
    // получаем опцию где хранится id организации к которой он относится (их может быть несколько)
    $option = $modx->getObject("msProductOption",array("key" => "add_id_org","product_id" => $stycker->id));
    // формируем массив с перечнем стикеров, в котором хранятся названия и организации к которым они относятся
    $stickerMass[$stycker->id] = array(
        "name" => $stycker->pagetitle,
        "id_org" => explode(",",$option->value)
    );
}

foreach ($rows as $key => $row) {
    $prices = $modx->getCollection("SoftjetsyncPrices",array("product_id" => $row['id']));
    $massPrices = array();
    foreach ($prices as $price) {
        $massPrices[] = array (
            "id" => $price->get("id"),
            "type_price" => $price->get("type"),
            "type_name" => $priseTypes[$price->get("type")],
            "price" => $price->get("price"),
            "old_price" => $price->get("old_price"),
            "tv" => $massTypesTv[$priseTypes[$price->get("type")]][$price->get("type")],
            "id_org" => $massIdOrg[$priseTypes[$price->get("type")]]
        );
    }
    
    $rows[$key]['prices'] = $massPrices;
     
}

return $rows;