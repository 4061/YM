id: 137
source: 1
name: cart-promocode
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        if (!$_SESSION['minishop2']['promocodes']){
        $code = $_POST['JSON']['promocode'];
        if ($code == ""){
            $json['data'] = array("errorMessage" =>"Промокод не активен" );
            return json_encode($json);
        }
        $profile = $modx->getObject('modUserProfile', ['website' => $token]);
        $internalKey = $profile -> get('internalKey');
        $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
        $current_code = $modx->query($sql);
        $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
        $id_promo = $current_code[0]['id_promo'];
        $max_use  = $current_code[0]['user_use_count']; // получаем макисмальное кол-во использований этого промокода пользователем..
        $type = $current_code[0]['type'];
        if ($current_code[0]['activity']){
            if ($type == 1){
                if ($current_code[0]['use_count'] != 0){
                        $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$internalKey' AND type = '$type'";
                        $countCheck = $modx->query($sql);
                        $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
                        if (count($countCheck) < $max_use){
                            $sql = "
                            INSERT INTO 
                                `modx_promo_users` 
                                    (
                                        `user_id`,
                                        `promo_id`,
                                        `use_count`,
                                        `activation_status`,
                                        `order_id`,
                                        `type`,
                                        `use_time`
                                    ) 
                                    VALUES 
                                    (
                                        '$internalKey',
                                        '$id_promo',
                                        '',
                                        '0',
                                        '',
                                        '$type',
                                        CURRENT_TIMESTAMP
                                    )";
                            foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                                $total_cost_product = $cart_item['total_cost']*$cart_item['count'];
                                $total_cost = $total_cost + $total_cost_product;
                                $total_count = $total_count + $cart_item['count'];
                            }
                            $_SESSION['minishop2']['total_cost'] = $total_cost;
                            $_SESSION['minishop2']['total_count'] = $total_count;
                            $_SESSION['minishop2']['promocodes']=$code;
                            $discount_percent = $current_code[0]['child_discount']/100;
                            $actual_discount = round($total_cost*$discount_percent);
                            $discount_cost = round($total_cost - $actual_discount);
                            $_SESSION['minishop2']['discount_cost'] = $discount_cost;
                            $_SESSION['minishop2']['actual_discount'] = $actual_discount;
                            //$json['products'][] = $_SESSION['minishop2']['cart']; //передаем все товары в корзине
                            $cart_items = $_SESSION['minishop2']['cart'];
                            foreach ($cart_items as $key=> $cart_item){
                                $cart_items[$key]['key'] = $key;
                                $sql = "SELECT value FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.product_id = {$cart_item['id']} AND modx_ms2_product_options.key = 'max_count'";
                                $max_product_count = $modx->query($sql);
                                $max_product_count = $max_product_count->fetchAll(PDO::FETCH_ASSOC);
                                $max_product_count = ($max_product_count[0]['value']);
                                if ($max_product_count){
                                    $cart_items[$key]['max_count'] = (int)$max_product_count;
                                } else {
                                    $cart_items[$key]['max_count'] = 999999;
                                }
                                $res = $modx->getObject('modResource',$cart_item["id"]);
                                $cart_items[$key]["img_url"] = $res->get('image');
                                $cart_items[$key]["title"] = $res->get('pagetitle');
                                $cart_items[$key]["desc"] = $res->get('description');
                            }
                            $cart_items = array_values($cart_items);
                            $json['products'] = $cart_items;
                            $json['promocodes'] = $_SESSION['minishop2']['promocodes']; //промокоды 
                            $json['total_cost'] = $_SESSION['minishop2']['total_cost']; //цена без скидки
                            $json['total_count'] = $_SESSION['minishop2']['total_count']; //кол-во товаров
                            $json['discount_cost'] = $_SESSION['minishop2']['discount_cost']; //цена со скидкой
                            $json['actual_discount'] = $_SESSION['minishop2']['actual_discount']; //цена со скидкой
                            return json_encode($json);
                    } else {
                        $json['data'] = array("response" => false, "errorMessage" =>"Вы использовали промокод максимальное количество раз." );
                        return json_encode($json);
                    }
                } else {
                    $json['data'] = array("response" => false, "errorMessage" =>"Промокод использован максимальное количество раз." );
                    return json_encode($json);
                }
            }
            if ($type == 2){
                foreach ($_SESSION['minishop2']['cart'] as $key => $cart_item){
                    if ($cart_item['id'] == $current_code[0]['id_product']){
                        $json['data'] = array("response" => false, "errorMessage" =>"Достигнуто максимальное количество использований промокода в корзине!");
                        return json_encode($json);
                    }
                }
                if ($current_code[0]['use_count'] != 0){
                    $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$internalKey' AND type = '$type'";
                    $countCheck = $modx->query($sql);
                    $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
                    if (count($countCheck) < $max_use){
                        $sql = "
                        INSERT INTO 
                            `modx_promo_users` 
                                (
                                    `user_id`,
                                    `promo_id`,
                                    `use_count`,
                                    `activation_status`,
                                    `order_id`,
                                    `type`,
                                    `use_time`
                                ) 
                                VALUES 
                                (
                                    '$internalKey',
                                    '$id_promo',
                                    '',
                                    '0',
                                    '',
                                    '$type',
                                    CURRENT_TIMESTAMP
                                )";
                        $modx->query($sql);
                        $option['childs'] = array(
                        "ingredients"=>[], 
                        "additives"=>[],
                        "size"=>null,
                        "type"=>null,
                        "rating" => []
                        );
                            $ms2 = $modx->getService('miniShop2');
                        $ms2->initialize($modx->context->key);
                        $cart = $ms2->cart->get();
                        $cart = $ms2->cart->add($current_code[0]['id_product'], 1, $option);
                        foreach ($_SESSION['minishop2']['cart'] as $key => $cart_item){
                            $dop_price = 0; // доп цена 0 
                           //$_SESSION['minishop2']['cart'][$key]['price'] = 0;
                            foreach ($cart_item['options']['childs']['additives'] as $additive){
                                $additive_id = $additive['id'];
                                $additive_count = $additive['count'];
                                $resources = $modx->getCollection('modResource', array("id" => $additive_id));
                                foreach ($resources as $res){
                                    $dop_price =$dop_price+  ($res->get('price')*$additive_count);
                                }
                                //$cart = $ms2->cart->add($additive_id, $additive_count);
                            }
                            $_SESSION['minishop2']['cart'][$key]['additional_cost'] = $dop_price;
                            $_SESSION['minishop2']['cart'][$key]['total_cost'] = $_SESSION['minishop2']['cart'][$key]['price'] + $dop_price;
                        }
                        foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                            $total_cost_product = $cart_item['total_cost']*$cart_item['count'];
                            $total_cost = $total_cost + $total_cost_product;
                            $total_count = $total_count + $cart_item['count'];
                        }
                        //$cart = $ms2->cart->add($current_code[0]['id_product'], 1);
                        $_SESSION['minishop2']['promocodes']=$code;
                        //$json['products'][] = $_SESSION['minishop2']['cart']; //передаем все товары в корзине
                            $cart_items = $_SESSION['minishop2']['cart'];
                            foreach ($cart_items as $key=> $cart_item){
                                $cart_items[$key]['key'] = $key;
                                $cart_items[$key]['id'] = (int)$cart_item["id"];
                                $cart_items[$key]['max_count'] = 999999;
                                $res = $modx->getObject('modResource',$cart_item["id"]);
                                $cart_items[$key]["img_url"] = $res->get('image');
                                $cart_items[$key]["title"] = $res->get('pagetitle');
                                $cart_items[$key]["desc"] = $res->get('description');
                            }
                            $cart_items = array_values($cart_items);
                            $json['products'] = $cart_items;
                        $json['promocodes'] = $_SESSION['minishop2']['promocodes']; //промокоды 
                        $json['total_cost'] = $_SESSION['minishop2']['total_cost']; //цена без скидки
                        $json['total_count'] = $_SESSION['minishop2']['total_count']; //кол-во товаров
                        $json['discount_cost'] = $_SESSION['minishop2']['discount_cost']; //цена со скидкой
                        $json['actual_discount'] = $_SESSION['minishop2']['actual_discount']; //цена со скидкой
                        $json["SumDiscount"] = $response["SumDiscount"]; //сумма скидкой из ПХ  
                        $json["PrintMessage"] = $response["PrintMessage"]; //- Сообщение на печать
                        $json["BonusBalance"] = $response["BonusBalance"]; //бонусный баланс
                        $json["MaxBonus"] = $response["MaxBonus"]; //бонусов для списания
                        $json["delivery_cost"] = $_SESSION["delivery_cost"];
                        $json["operation_id"] = $_POST["JSON"]["operation_id"];
                        $discount_cost = 0;
                        $total_cost = 0;
                        $json['ewewaaSX'] = "";
                        if ($_SESSION["order_type"] == 1){
                            $json["PrintMessage"] = "Скидка на самовывоз 20%, за исключением напитков.";
                            foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                                $res = $modx->getObject("modResource", $cart_item['id']);
                                $discount_cost = $discount_cost + ($res->get("price2")*$cart_item["count"]);
                                $total_cost = $total_cost + $res->get("price");
                                if ($cart_item['options']['childs']["additives"] != []){
                                    foreach ($cart_item['options']['childs']["additives"] as $add){
                                        $res = $modx->getObject("modResource", $add['id']);
                                        $discount_cost = $discount_cost + ($res->get("price2")*$cart_item["count"]);
                                        $total_cost = $total_cost + $res->get("price");
                                    }
                                }
                            }
                        } elseif ($_SESSION["order_type"] == 2){
                            foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                                $res = $modx->getObject("modResource", $cart_item['id']);
                                $discount_cost = $discount_cost + ($res->get("price")*$cart_item["count"]);
                                $total_cost = $total_cost + $res->get("price");
                                if ($cart_item['options']['childs']["additives"] != []){
                                    foreach ($cart_item['options']['childs']["additives"] as $add){
                                        $res = $modx->getObject("modResource", $add['id']);
                                        $discount_cost = $discount_cost + ($res->get("price")*$cart_item["count"]);
                                        $total_cost = $total_cost + $res->get("price");
                                    }
                                }
                            }
                        }
                        //$json['discount_cost'] = $_SESSION['minishop2']['discount_cost']; //цена со скидкой
                        $json['discount_cost'] = $discount_cost; //цена со скидкой
                        return json_encode($json);
                    }
                } else {
                    $json['data'] = array("response" => false, "errorMessage" =>"Промокод использован максимальное количество раз." );
                    return json_encode($json);
                }
            }
            if ($type == 3){
                $sql = "SELECT * FROM modx_ms2_orders WHERE user_id = '$internalKey'";
                $userOrders = $modx->query($sql);
                $userOrders = $userOrders->fetchAll(PDO::FETCH_ASSOC); //проверяем первый ли заказ
                if (!$userOrders){ //если первый заказ
                    $codebase = $modx->getObject("PromoBase", $current_code[0]["id_promo"]);
                    if ($codebase->get("author_id") == $internalKey){
                        $json['data'] = array("response" => false, "errorMessage" =>"Вы используете собственный промокод." );
                        return json_encode($json);
                    } else {
                        if ($current_code[0]['use_count'] != 0){
                                $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$internalKey' AND type = '$type'";
                                $countCheck = $modx->query($sql);
                                $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
                                if (count($countCheck) < $max_use){
                                    $sql = "
                                    INSERT INTO 
                                        `modx_promo_users` 
                                            (
                                                `user_id`,
                                                `promo_id`,
                                                `use_count`,
                                                `activation_status`,
                                                `order_id`,
                                                `type`,
                                                `use_time`
                                            ) 
                                            VALUES 
                                            (
                                                '$internalKey',
                                                '$id_promo',
                                                '',
                                                '0',
                                                '',
                                                '$type',
                                                CURRENT_TIMESTAMP
                                            )";
                                    $modx->query($sql);
                                foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                                    $total_cost_product = $cart_item['total_cost']*$cart_item['count'];
                                    $total_cost = $total_cost + $total_cost_product;
                                    $total_count = $total_count + $cart_item['count'];
                                }
                                $_SESSION['minishop2']['total_cost'] = $total_cost;
                                $_SESSION['minishop2']['total_count'] = $total_count;
                                $_SESSION['minishop2']['promocodes']=$code;
                                $costfourth = $total_cost * 0.25;
                                if ($costfourth>=200){
                                    $actual_discount = 200;
                                } else {
                                    $actual_discount = $costfourth;
                                }
                                $discount_cost = $total_cost - $actual_discount;
                                $_SESSION['minishop2']['discount_cost'] = $discount_cost;
                                $_SESSION['minishop2']['actual_discount'] = $actual_discount;
                                //$json['products'][] = $_SESSION['minishop2']['cart']; //передаем все товары в корзине
                                    $cart_items = $_SESSION['minishop2']['cart'];
                                    foreach ($cart_items as $key=> $cart_item){
                                        $cart_items[$key]['key'] = $key;
                                    }
                                    $cart_items = array_values($cart_items);
                                    $json['products'] = $cart_items;
                                $json['promocodes'] = $_SESSION['minishop2']['promocodes']; //промокоды 
                                $json['total_cost'] = $_SESSION['minishop2']['total_cost']; //цена без скидки
                                $json['total_count'] = $_SESSION['minishop2']['total_count']; //кол-во товаров
                                $json['discount_cost'] = $_SESSION['minishop2']['discount_cost']; //цена со скидкой
                                $json['actual_discount'] = $_SESSION['minishop2']['actual_discount']; //цена со скидкой
                                return json_encode($json);
                            } else {
                                $json['data'] = array("response" => false, "errorMessage" =>"Вы использовали промокод максимальное количество раз." );
                                return json_encode($json);
                            }
                        } else {
                            $json['data'] = array("response" => false, "errorMessage" =>"Промокод использован максимальное количество раз." );
                            return json_encode($json);
                        }
                    }
                } else {
                    $json['data'] = array("response" => false, "errorMessage" =>"Вы уже совершали заказы." );
                    return json_encode($json);
                }
            }
        } else {
            $json['data'] = array("errorMessage" =>"Промокод не активен" );
            return json_encode($json);
        }
        }
        else {
            $json['data'] = array("response" => false, "errorMessage" =>"Вы уже использовали промокод" );
            return json_encode($json);
        }
    } else {
        $json['data'] = array("errorMessage" =>"Токен устарел" );
        return json_encode($json);
    }
}