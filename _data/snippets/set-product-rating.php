id: 125
name: set-product-rating
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $orderId = $_POST['JSON']['orderId'];
        $ratings = $_POST['JSON']['ratings'];
        $comment = $_POST['JSON']['comment'];
        $bad_vals = [];
        foreach ($ratings as $rating){
            $product_id = $rating['id'];
            $stars = $rating['rating'];
            $page = $modx->getObject('modResource', $product_id);
            if ($stars <=2){
              array_push($bad_vals, array($page->get("pagetitle") => $stars));
            }
            $jsonTV = $page->getTVValue('product_rating');
            $optionsTV = json_decode($jsonTV, true);
            foreach ($optionsTV as &$value) {
                $value['count'] = $value['count'] +1;
                $value['sum'] = $value['sum'] + $stars;
            }
            $optionsTV = json_encode($optionsTV);
            $page->setTVValue('product_rating', $optionsTV);
            $page->save();
            $sql = "SELECT id, options, count, price FROM modx_ms2_order_products WHERE order_id = '$orderId' AND product_id = '$product_id'";
            $order_options = $modx->query($sql);
            $order_options = $order_options->fetchAll(PDO::FETCH_ASSOC);
            $count = $order_options[0]['count'];
            $price = $order_options[0]['price'];
            $order_product_id = $order_options[0]['id'];
            $pie = json_decode($order_options[0]['options'], true);
            $pie["childs"]["rating"] = [$stars];
            $data = array(
                'username' => 'admin', // юзер должен обладать правами менеджера магазина, как минимум
                'password' => 'admin1234', // настройку прав тут не буду описывать, равно как и безопасность
                'rememberme' => false,
                'login_context' => 'mgr' // логинимся в админку
            );
            $response = $modx->runProcessor('/security/login', $data);
            $user = $modx->getObject('modUser', array('username' => $data['username']));
            $modx->user = $user;
            $res = $modx->runProcessor(
            'mgr/orders/product/update', 
            array(
                'id' => $order_product_id,// тут внимательнее, нужен id товара из таблицы prefix_ms2_order_products,
                'count' => $count,
                'options' => json_encode($pie),
                'price'=>$price
                ),
            array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/') 
            );
        }
        if ($comment){
            $miniShop2 = $modx->getService('miniShop2'); 
            $miniShop2->initialize($modx->context->key, $scriptProperties);
            $actualorder = $modx->getObject('msOrder', 8);
            $createdon = $actualorder->get("createdon");
            $sql = "
            UPDATE
                modx_ms2_order_addresses
            SET
                commemtAssessment = '$comment'
            WHERE
                createdon = '$createdon'
            ";
                $modx->query($sql);
        }
        if (count($bad_vals) >0 ){
            $prop = array('type'=> 'products', 'products' => $bad_vals, "orderId"=>$orderId, "comment"=>$comment);
            $modx->runSnippet('sendBadRating',$prop);
            echo json_encode($prop);
        }
    }
}