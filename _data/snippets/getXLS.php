id: 141
source: 1
name: getXLS
properties: 'a:0:{}'

-----

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
$random = generateRandomString();
include  $_SERVER["DOCUMENT_ROOT"].'/file/PHPExcel-1.8/Classes/PHPExcel.php'; 
include  $_SERVER["DOCUMENT_ROOT"].'/file/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php'; 
$xls = new PHPExcel();

$segment_id = $_GET['segment_id'];
$delivery = $_GET['delivery'];
if ($segment_id){
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("A1", "Имя")
    ->getColumnDimension("A")->setWidth(30)
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("B1", "Email")
    ->getColumnDimension("B")->setWidth(30)
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("C1", "Телефон")
    ->getColumnDimension("C")->setWidth(30)
    ;
    $birthday_1 = $_GET['birthday_1'];
    $birthday_2 = $_GET['birthday_2'];
    $min_dob = $_GET['min_dob'];
    $max_dob = $_GET['max_dob'];
    $last_buy1 = $_GET['last_buy1'];
    $last_buy2 = $_GET['last_buy2'];
    $date1 = $_GET['date_1'];
    $date2 = $_GET['date_2'];
    $min_cartcost = $_GET['min_cartcost'];
    $max_cartcost = $_GET['max_cartcost'];
    $gender = $_GET['gender'];
    $max_avg = $_GET['max_avg']; //максимальный средний чек
    $min_avg = $_GET['min_avg']; //минимальный средний чек
    $lastdays = $_GET['days']; //за сколько последних дней брать информацию
    $min_sum = $_GET['min_sum']; //минимальная сумма покупок от (price)
    $max_sum = $_GET['max_sum']; //максимальная сумма покупок до (price)
    $min_count = $_GET['min_count']; //минимальное количество заказов
    $max_count = $_GET['max_count']; //максимальное количество заказов
    $products = array( //покупки определенных продуктов
        "1" => $_GET['product_1'], 
        "2" => $_GET['product_2'],
        "3" => $_GET['product_3'],
        "4" => $_GET['product_4']
    );
    $product_keys_placeholder = '';
    foreach ($products as $product){
        if ($product){
            $product_keys_placeholder = $product_keys_placeholder." AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
        }
    }
    if ($birthday_1){
        $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
    }
    if ($birthday_2){
        $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
    }
    if ($min_dob>0 or $max_dob>0){
        $dobswither = " AND dob <> 0";
        $modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
    }
    if ($min_dob){
        $min_dob = $min_dob*365*24*60*60;
        $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
    }
    if ($max_dob){
        $max_dob = $max_dob*365*24*60*60;
        $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
    }
    if ($last_buy1){
        $last_buy1_placeholder = " AND time_end <= DATE_ADD(CURDATE(), INTERVAL -'$last_buy1' DAY)";
    }
    if ($last_buy2){
        $last_buy2_placeholder = " AND time_end >= DATE_ADD(CURDATE(), INTERVAL -'$last_buy2' DAY)";
    }
    if ($date1){
        $date1_placeholder = " AND modx_ms2_orders.createdon >= '$date1'";
    }
    if ($date2){
        $date2_placeholder = " AND modx_ms2_orders.createdon <= '$date2'";
    }
    if ($gender){
        $gender_placeholder = " AND modx_segment_users.user_id IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
    }
    if ($max_cartcost>0){
        $max_cartcost_placeholder = "AND modx_ms2_orders.cart_cost<='$max_cartcost'";
    }
    if ($min_cartcost>0){
        $min_cartcost_placeholder = " AND modx_ms2_orders.cart_cost>='$min_cartcost'";
    }
    if ($min_sum>0){
        $min_sum_placeholder = " AND ord_sum>='$min_sum'";
    }
    if ($max_sum>0){
        $max_sum_placeholder = " AND ord_sum<='$max_sum'";
    }
    if ($min_count>0){
        $min_count_placeholder = " AND ord_count>='$min_count'";
    }
    if ($max_count>0){
        $max_count_placeholder = " AND ord_count<='$max_count'";
    }
    if ($min_avg>0){
        $min_avg_placeholder = " AND ord_avg>='$min_avg'";
    }
    if ($max_avg>0){
        $max_avg_placeholder = " AND ord_avg<='$max_avg'";
    }
    if ($lastdays){
        $lastdays_placeholder = " AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'$lastdays' DAY)";
    }
    $sql = "
    SELECT 
        modx_segment_users.user_name as user_name,
        modx_user_attributes.email as email,
        modx_user_attributes.mobilephone as mobilephone
    FROM 
        modx_segment_users,modx_ms2_orders,modx_user_attributes
    WHERE 
        modx_segment_users.user_id = modx_ms2_orders.user_id AND
        modx_segment_users.user_id = modx_user_attributes.internalKey AND
        modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>=0 $lastdays_placeholder $date1_placeholder $date2_placeholder $last_buy1_placeholder $last_buy2_placeholder) AND
        modx_segment_users.user_id IN (Select internalKey FROM modx_user_attributes WHERE id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder)
        $min_avg_placeholder 
        $max_avg_placeholder
        $min_sum_placeholder
        $max_sum_placeholder
        $gender_placeholder
        $min_count_placeholder
        $max_count_placeholder
        $product_keys_placeholder
        $min_cartcost_placeholder
        $max_cartcost_placeholder AND
        modx_segment_users.user_id IN (Select user_id FROM modx_segment_base WHERE segment_id = '$segment_id')
    Group By 
        modx_segment_users.user_id";
    
    $info_user= $modx->query($sql);
    $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
    for ($i=0;$i<count($info_user);$i++){
        $j = $i + 2;
        if (!$info_user[$i]['user_name']){
            $info_user[$i]['user_name'] = "Не заполнен";
        }
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("A".$j, $info_user[$i]['user_name'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("B".$j, $info_user[$i]['email'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("C".$j, $info_user[$i]['mobilephone'])
        ;
    }
    $objWriter = PHPExcel_IOFactory::createWriter($xls, "Excel5");
    $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/segments_xls/'.$random.'.xls');
    $url = $modx->getOption('site_url');
    return $url.'segments_xls/'.$random.'.xls';
}
if ($delivery){
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("A1", "Наименование")
    ->getColumnDimension("A")->setWidth(15)
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("B1", "Цена")
    ->getColumnDimension("B")->setWidth(15)
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("C1", "ID категории")
    ->getColumnDimension("C")->setWidth(15)
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("D1", "Название категории")
    ->getColumnDimension("D")->setWidth(15)
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("E1", "Точка 1")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("F1", "Точка 2")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("G1", "Точка 3")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("H1", "Точка 4")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("I1", "Точка 5")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("J1", "Точка 6")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("K1", "Точка 7")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("L1", "Точка 8")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("M1", "Точка 9")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("N1", "Точка 10")
    ;
    $xls
    ->setActiveSheetIndex(0)
    ->setCellValue("O1", "Точка не выбрана")
    ->getColumnDimension("O")->setWidth(20)
    ;
    $delivery_placeholder = " AND modx_ms2_orders.delivery = $delivery";
    $prods = [];
    $json = [];
    $sql = "
    SELECT
        product_id,
        price,
        count,
        name,
        order_id,
        modx_site_content.parent as category,
        modx_ms2_orders.address as address_id,
        modx_ms2_order_addresses.index as restourant_id
    FROM
        modx_ms2_order_products,
        modx_site_content,
        modx_ms2_orders,
        modx_ms2_order_addresses
    WHERE
        modx_ms2_order_products.product_id = modx_site_content.id AND
        modx_ms2_orders.id = order_id AND
        modx_ms2_order_addresses.id = modx_ms2_orders.address
        $delivery_placeholder
    ";
    $info_products = $modx->query($sql);
    $info_products = $info_products->fetchAll(PDO::FETCH_ASSOC);
    foreach ($info_products as $key => $product){
        $prods[$product['name']][] = $product;
    }
    foreach ($prods as $key => $prod){
        foreach ($prods[$key] as $prod){
            $rest_id = "restourant_".$prod['restourant_id'];
            if (!$prod['restourant_id']){
                $rest_id = $rest_id.'null';
            }
            $json[$prod['name']][$rest_id] +=  $prod['count'];
            $json[$prod['name']]["price"] = $prod['price'];
            $json[$prod['name']]["parent"] = $prod['category'];
            $json[$prod['name']]["name"] = $prod['name'];
        }
    }
    $json = array_values($json);
    for ($i=0;$i<count($json);$i++){
        $j = $i + 2;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("A".$j, $json[$i]['name'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("B".$j, $json[$i]['price'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("C".$j, $json[$i]['parent'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("D".$j, $json[$i]['parent'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("E".$j, $json[$i]['restourant_1'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("F".$j, $json[$i]['restourant_2'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("G".$j, $json[$i]['restourant_3'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("H".$j, $json[$i]['restourant_4'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("I".$j, $json[$i]['restourant_5'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("J".$j, $json[$i]['restourant_6'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("K".$j, $json[$i]['restourant_7'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("L".$j, $json[$i]['restourant_8'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("M".$j, $json[$i]['restourant_9'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("N".$j, $json[$i]['restourant_10'])
        ;
        $xls
        ->setActiveSheetIndex(0)
        ->setCellValue("O".$j, $json[$i]['restourant_null'])
        ;
    }
    $objWriter = PHPExcel_IOFactory::createWriter($xls, "Excel5");
    $objWriter->save($_SERVER["DOCUMENT_ROOT"].'/segments_xls/'.$random.'.xls');
    $url = $modx->getOption('site_url');
    return $url.'segments_xls/'.$random.'.xls';
}