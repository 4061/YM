id: 217
name: setRatingComment
description: 'Сниппет для добавления комментария и рейтинга пользователем к заказу'
properties: 'a:0:{}'

-----

/*
{
  "orderId": 0,
  "comment": "string",
  "ratings": [
    {
      "id": 3681,
      "rating": 5,
      "comment": "Тестовый текст 22"
    }
  ]
}
*/

// ссылка на обработчик http://dev-maxim.appsj.su/set-rating-comment.json
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    // получаем значение запроса в формате JSON
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    // получаем все заголовки
    $token = getallheaders();
    // если есть параметр авторизации (токен), то присваиваем его значение второму токену
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    // вырезаем первые 7 символов второго токена и передаём значение переменной токен
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    // проверяем существует ли токен
    if ($token){
        
        $sql='SELECT internalKey FROM modx_user_attributes WHERE website = "'.$token.'"';
        $sqlOutput = $modx->query($sql);
        $idUser = $sqlOutput->fetch(PDO::FETCH_ASSOC)['internalKey'];
        
        if($idUser) {
        
            // получаем переданную json строку с данными
            $dataList = $_POST['JSON']['ratings'];
            
            //return json_encode($dataList);
            
            foreach($dataList as $key => $data){
                
                // проверяем указан ли был рейтинг, а так же не является ли комментарий пустым
                if ($data["rating"] > 0) { //  && $data["comment"] != ""
                    // формируем SQL запрос, что бы понять создан ли уже комментарий текущий пользователь для указанного объекта
                    $sql = 'SELECT id_rating FROM modx_rating WHERE id_resource = ' .$data['id']. ' AND id_user = ' .$idUser;
                    //return $sql;
                   
                    $elementQuery = $modx->query($sql);
                    $id_rating = $elementQuery->fetch(PDO::FETCH_ASSOC)['id_rating'];
                    // если создал ..
                    if($id_rating){
                        
                        $sql = 'UPDATE modx_rating SET `comment_rating` = "' .$data['comment']. '", `rating_value` = '.$data['rating'].', `date_of_editing` = "' .date("Y-m-d H:i:s"). '" WHERE id_rating = ' .$id_rating;
                        $elementQuery = $modx->query($sql);
                        /*// .. то получаем его объект и меняем данные
                        $rating = $modx->getObject("Rating",$element['id_rating']);
                        // устанавливаем дату и время изменения комментария
                        $rating->set("date_of_editing",date("Y-m-d H:i:s"));*/
                        
                        // параметры для ответа
                        $result[$key]["id_rating"] = $id_rating;
                        $result[$key]["action"] = "edit";
                        
                    } else {
        
                        // если комментарий отсутствует, то создаём его
                        $rating = $modx->newObject("Rating");
                        $rating->set("active",1);
                        
                        // заносим нужные нам зачения
                        $rating->set("id_resource",$data['id']);
                        $rating->set("id_user",$idUser);
                        $rating->set("comment_rating",$data['comment']);
                        $rating->set("rating_value",$data['rating']);
                        $rating->save();
                        
                        //return $data["rating"];
                        
                        // получаем id толкьо что созданного комментария
                        $sql = 'SELECT id_rating FROM modx_rating WHERE id_resource = ' .$data['id']. ' AND id_user = ' .$idUser;
                        //return $sql;
                        $elementQueryLast = $modx->query($sql);
                        $result[$key]["id_rating"] = $elementQueryLast->fetch(PDO::FETCH_ASSOC)["id_rating"];
                        
                        $result[$key]["action"] = "created";
                    }
                    
                    
                    // формируем запрос для получения всех оценок по данному ресурсу
                    $sql = 'SELECT rating_value FROM modx_rating WHERE id_resource = ' .$data['id']. " AND active = 1";
                    $allRatingElements = $modx->query($sql);
                    // обнуляем счётчик и сумму для рейтинга
                    $count = $summRating = 0;
                    // перебераем полученные значения
                    foreach($allRatingElements as $value) {
                        // считаем сумму рейтинга
                        $summRating += $value['rating_value'];
                        // увеличиваем счётчик
                        $count++;
                    }
                    // проверяем не равна ли сумма рейтинга 0
                    if($summRating) {
                        // если не равна, то рассчитываем рейтинг по формуле
                        $allRating = (float)(round(($summRating/$count), 1));
                    } else {
                        // если равна 0, то присваиваем 0 для общего рейтинга
                        $allRating = 0;
                    }
                    
                    $sql = 'SELECT id FROM modx_ms2_product_options WHERE product_id = ' .$data['id']. ' AND `key` = "item_rating"';
                    $ratingAllQuery = $modx->query($sql);
                    $id = $ratingAllQuery->fetch(PDO::FETCH_ASSOC)["id"];
                    
                    if($id) {
                        $sql = "UPDATE modx_ms2_product_options SET value = '" .$allRating. "' WHERE product_id = " .$data['id']. " AND `key` = 'item_rating'";
                        $modx->query($sql);
                    } else {
                        $sql = "INSERT INTO modx_ms2_product_options (`product_id`,`key`,`value`) VALUES (".$data['id'].",'item_rating','".$allRating."')";
                        $modx->query($sql);
                    }
                    
                    // заносим новый рейтинг в ответ
                    $result[$key]["allRating"] = round($allRating,4); // number_format($allRating, 1, '.', '');
                    $result[$key]["countRating"] = $count;
                
                // если комментарий есть, а рейтинг не выставлен, возвращаем ошибку    
                /*} else if ($data["rating"] > 0) {
                    $result["error"] = "Вы не указали комментарий для объекта";
                    break;*/
                // если рейтинг есть, а комментарий отсутствует, возвращаем ошибку    
                } else if ($data["comment"] != "") {
                    $result["error"] = "Вы не указали рейтинг для объекта";
                    break;
                // если ничего не указано, возвращаем ошибку
                } else {
                    $result["error"] = "Вы не указали ни комментарий, ни рейтинг для объекта";
                    break;
                }
                
            }
        
        } else {
            $result["error"] = "Пользователь с таким токеном отсуствует";
        }
        
        $json["result"] = $result;

        // возвращаем результат
        return json_encode($json);
        
    }
}