id: 126
source: 1
name: sendBadRating
properties: 'a:0:{}'

-----

$type = $modx->getOption('type', $scriptProperties);
if ($type == 'products'){
    $orderId = $modx->getOption('orderId', $scriptProperties);
    $miniShop2 = $modx->getService('miniShop2'); 
    $miniShop2->initialize($modx->context->key, $scriptProperties);
    $actualorder = $modx->getObject('msOrder', $orderId);
    $user_id = $actualorder->get('user_id');
    $moduser = $modx->getObject('modUser', array('id' => $user_id)); 
    $user_profile = $modx->getObject('modUserProfile', array('internalKey' => $user_id));
    $url =  $modx->config['site_url'];
    $url = str_replace('//','-',$url);
    $url = str_replace('/','',$url);
    $url = str_replace('-','//',$url);
    $properties['url'] = $url;
    $userArray = [
                "userid" => $user_id,
                "useremail" => $user_profile->get('email'),
                "mobilephone" => $user_profile->get('mobilephone')
                ];
    $ratingArray = $modx->getOption('products', $scriptProperties);
    $comment = $modx->getOption('comment', $scriptProperties);
    //----записываем передаваемые в чанк переменные
    $properties['order_id'] = $actualorder->get("id");
    $properties['num'] = $actualorder->get("num");
    $properties['createdon'] = $actualorder->get("createdon");
    $properties['userid'] = $user_id;
    $properties['username'] = $moduser->get('username');
    $properties['useremail'] = $user_profile->get('email');
    $properties['mobilephone'] = $user_profile->get('mobilephone');
    $properties['products'] = $ratingArray;
    $properties['name'] = $user_profile->get('fullname');
    $properties['comment'] = $comment;
    //---
    $modx->getService('mail', 'mail.modPHPMailer');
    $modx->mail->set(modMail::MAIL_FROM, $modx->getOption('emailsender'));
    $modx->mail->set(modMail::MAIL_FROM_NAME, $modx->getOption('site_name'));
    
    /*Адрес получателя нашего письма*/
    $emails = $modx->getOption('ms2_email_quality');
    $emails = explode(",",$emails);
    foreach ($emails as $email){
        $modx->mail->address('to', $email); 
        /*Заголовок сообщения*/
        $modx->mail->set(modMail::MAIL_SUBJECT, 'Получена негативная оценка по Заказу №'.$properties['num']);
        
        /*Подставляем чанк с телом письма (предварительно его нужно создать)*/
        $pdo = $modx->getService('pdoTools');
        $modx->mail->set(modMail::MAIL_BODY, $pdo->getChunk('tpl.badProductRatingEmail', array("elem" => $properties)));
        
        /*Отправляем*/
        $modx->mail->setHTML(true);
        if (!$modx->mail->send()) {
            $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
        }
        $modx->mail->reset();  
    }
}
if ($type == 'order'){
    $orderId = $modx->getOption('orderId', $scriptProperties);
    $taste = $modx->getOption('taste', $scriptProperties);
    $beauty = $modx->getOption('beauty', $scriptProperties);
    $speed = $modx->getOption('speed', $scriptProperties);
    $temp = $modx->getOption('temp', $scriptProperties);
    $comment = $modx->getOption('comment', $scriptProperties);
    $courier = $modx->getOption('courier', $scriptProperties);
    $rating = $modx->getOption('rating', $scriptProperties);
    $miniShop2 = $modx->getService('miniShop2'); 
    $miniShop2->initialize($modx->context->key, $scriptProperties);
    $actualorder = $modx->getObject('msOrder', $orderId);
    $user_id = $actualorder->get('user_id');
    $moduser = $modx->getObject('modUser', array('id' => $user_id)); 
    $user_profile = $modx->getObject('modUserProfile', array('internalKey' => $user_id));
    $userArray = [
                "userid" => $user_id,
                "useremail" => $user_profile->get('email'),
                "mobilephone" => $user_profile->get('mobilephone')
                ];
    
    //----записываем передаваемые в чанк переменные
    $properties['order_id'] = $actualorder->get("id");
    $properties['num'] = $actualorder->get("num");
    $properties['createdon'] = $actualorder->get("createdon");
    $properties['userid'] = $user_id;
    $properties['username'] = $moduser->get('username');
    $properties['useremail'] = $user_profile->get('email');
    $properties['mobilephone'] = $user_profile->get('mobilephone');
    $properties['name'] = $user_profile->get('fullname');
    $properties['taste'] = $taste;
    $properties['beauty'] = $beauty;
    $properties['speed'] = $speed;
    $properties['temp'] = $temp;
    $properties['courier'] = $courier;
    $properties['comment'] = $comment;
    $properties['rating'] = $rating;
    //---
    $url =  $modx->config['site_url'];
    $url = str_replace('//','-',$url);
    $url = str_replace('/','',$url);
    $url = str_replace('-','//',$url);
    $properties['url'] = $url;
    $modx->getService('mail', 'mail.modPHPMailer');
    $modx->mail->set(modMail::MAIL_FROM, $modx->getOption('emailsender'));
    $modx->mail->set(modMail::MAIL_FROM_NAME, $modx->getOption('site_name'));
    
    /*Адрес получателя нашего письма*/
    $emails = $modx->getOption('ms2_email_quality');
    $emails = explode(",",$emails);
    foreach ($emails as $email){
        $modx->mail->address('to', $email);
        
        /*Заголовок сообщения*/
        $modx->mail->set(modMail::MAIL_SUBJECT, 'Получена негативная оценка по Заказу №'.$properties['num']);
        
        /*Подставляем чанк с телом письма (предварительно его нужно создать)*/
        $modx->mail->set(modMail::MAIL_BODY, $modx->getChunk('tpl.badRatingEmail', $properties));
        
        /*Отправляем*/
        $modx->mail->setHTML(true);
        if (!$modx->mail->send()) {
            $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
        }
        $modx->mail->reset();
    }
}