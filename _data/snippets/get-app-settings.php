id: 198
source: 1
name: get-app-settings
properties: 'a:0:{}'

-----

if ((int)$modx->getOption('app_state_status') == 0){
    $json["app_state_status"] = false; //Приложение активно / не активно.
} else {
    $json["app_state_status"] = true; //Приложение активно / не активно.
}
if ((int)$modx->getOption('app_cashback_status') == 0){
    $json["app_cashback_status"] = false; //Статус кешбек системы
} else {
    $json["app_cashback_status"] = true; //Статус кешбек системы
}
if ((int)$modx->getOption('app_promocode_status') == 0){
    $json["app_promocode_status"] = false; //Активность показывать ли строку ввода промо-кода
} else {
    $json["app_promocode_status"] = true; //Активность показывать ли строку ввода промо-кода
}
if ((int)$modx->getOption('app_referal_status') == 0){
    $json["app_referal_status"] = false; //Активность показывать ли строку "Скидка для друга"
} else {
    $json["app_referal_status"] = true; //Активность показывать ли строку "Скидка для друга"
}
if ((int)$modx->getOption('app_reviews_status') == 0){
    $json["app_reviews_status"] = false; //Показывать ли отзывы
} else {
    $json["app_reviews_status"] = true; //Показывать ли отзывы
}
if ((int)$modx->getOption('app_org_change') == 0){
    $json["app_org_change"] = false; //Показывать ли точки продаж
} else {
    $json["app_org_change"] = true; //Показывать ли точки продаж
}
if ((int)$modx->getOption('app_phone_check') == 0){
    $json["app_phone_check"] = false; //Проверять ли телефон на титульной странице приложения
} else {
    $json["app_phone_check"] = true; //Проверять ли телефон на титульной странице приложения
}
if ((int)$modx->getOption('app_modal_type_order') == 0){
    $json["app_modal_type_order"] = false; //Выводить ли модальное с выбором типа подачи
} else {
    $json["app_modal_type_order"] = true; //Выводить ли модальное с выбором типа подачи
}
if ((int)$modx->getOption('app_repeat_order') == 0){
    $json["app_repeat_order"] = false; //Давать ли возможность повтороного заказа
} else {
    $json["app_repeat_order"] = true; //Давать ли возможность повтороного заказа
}
if ((int)$modx->getOption('app_white_list') == 0){
    $json["app_white_list"] = false; //Использовать ли "белый список"
} else {
    $json["app_white_list"] = true; //Использовать ли "белый список"
}
if ((int)$modx->getOption('app_black_list') == 0){
    $json["app_black_list"] = false; //Использовать ли "белый список"
} else {
    $json["app_black_list"] = true; //Использовать ли "белый список"
}
$json["app_screen_type"] = $modx->getOption('app_screen_type'); // Какой тип экрана будет отображатся в приложении
$json["app_state_disable_text"] = $modx->getOption('app_state_disable_text');//Какой текст вывести если приложение не активно
//$json["app_cashback_status"] = (int)$modx->getOption('app_cashback_status'); //Статус кешбек системы
//$json["app_promocode_status"] =(int) $modx->getOption('app_promocode_status'); //Активность показывать ли строку ввода промо-кода
//$json["app_referal_status"] = (int)$modx->getOption('app_referal_status'); //Активность показывать ли строку "Скидка для друга"
$json["app_ver_api"] = $modx->getOption('app_ver_api');//версия апи
return json_encode($json);