id: 149
source: 1
name: msOptionsPrice.initialize
category: msOptionsPrice2
properties: 'a:4:{s:11:"frontendCss";a:7:{s:4:"name";s:11:"frontendCss";s:4:"desc";s:31:"msoptionsprice_prop_frontendCss";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:25:"msoptionsprice:properties";s:4:"area";s:0:"";}s:10:"frontendJs";a:7:{s:4:"name";s:10:"frontendJs";s:4:"desc";s:30:"msoptionsprice_prop_frontendJs";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:0:"";s:7:"lexicon";s:25:"msoptionsprice:properties";s:4:"area";s:0:"";}s:11:"processCart";a:7:{s:4:"name";s:11:"processCart";s:4:"desc";s:31:"msoptionsprice_prop_processCart";s:4:"type";s:13:"combo-boolean";s:7:"options";a:0:{}s:5:"value";b:1;s:7:"lexicon";s:25:"msoptionsprice:properties";s:4:"area";s:0:"";}s:9:"actionUrl";a:7:{s:4:"name";s:9:"actionUrl";s:4:"desc";s:29:"msoptionsprice_prop_actionUrl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:24:"[[+assetsUrl]]action.php";s:7:"lexicon";s:25:"msoptionsprice:properties";s:4:"area";s:0:"";}}'
static_file: core/components/msoptionsprice/elements/snippets/snippet.initialize.php

-----

/** @var array $scriptProperties */
$corePath = $modx->getOption('msoptionsprice_core_path', null,
    $modx->getOption('core_path', null, MODX_CORE_PATH) . 'components/msoptionsprice/');
/** @var msoptionsprice $msoptionsprice */
$msoptionsprice = $modx->getService('msoptionsprice', 'msoptionsprice', $corePath . 'model/msoptionsprice/',
    array('core_path' => $corePath));
if (!$msoptionsprice) {
    return 'Could not load msoptionsprice class!';
}
$msoptionsprice->initialize($modx->context->key, $scriptProperties);
$msoptionsprice->loadResourceJsCss($scriptProperties);

/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);

$processCart = $scriptProperties['processCart'] = (bool)$modx->getOption('processCart', $scriptProperties);

if ($processCart AND $msoptionsprice->getOption('allow_remains')) {
    $items = $miniShop2->cart->get();

    foreach ($items as $key => $item) {
        $count = $modx->getOption('count', $item, 0, true);
        $options = $modx->getOption('options', $item, array(), true);
        $mid = (int)$modx->getOption('modification', $options);
        if (empty($mid)) {
            continue;
        }
        $mo = $modx->getObject('msopModification', array('id' => $mid));
        if (!$mo) {
            continue;
        }

        $mCount = $mo->get('count');
        if ($count < $mCount) {
            continue;
        } else {
            $count = $mCount;
        }

        if ($count < 1) {
            $miniShop2->cart->remove($key);
        } else {
            $miniShop2->cart->change($key, $count);
        }
    }
}