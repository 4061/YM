id: 218
source: 1
name: addSecretPromotionsToSegment
description: 'Добавление акции для определенного сегмента.'
properties: 'a:0:{}'

-----

$_REQUEST['JSON'] = json_decode(
         file_get_contents('php://input'), true
);
$token = getallheaders();

if ($token['Authorization']) $token2 = $token['Authorization'];
if ($token['authorization']) $token2 = $token['authorization'];
$token = substr($token2, 7);
$_POST['JSON'] = & $_REQUEST['JSON'];
    $str = $_POST["JSON"]["segments"];
    $promo_id = $_POST["JSON"]["promo_id"];
    $arr = explode(", ",$str);
    $pdoFetch = new pdoFetch($modx);
    $segments = array(
        'class' => 'SegmentSegments',
        'where' => ["segment_name:IN" => $arr],
        'sortdir' => "ASC",
        'sortby' => "segment_id",
        'return' => 'data',
        'limit' => 10000
    );
    $pdoFetch->setConfig($segments);
    $segments = $pdoFetch->run();
    $master_ids = array_column($segments, 'segment_id');
    $users = array(
        'class' => 'SegmentBase',
        'where' => ["segment_id:IN" => $master_ids],
        'sortdir' => "ASC",
        'return' => 'data',
        'limit' => 10000
    );
    $pdoFetch->setConfig($users);
    $users = $pdoFetch->run();
    foreach ($users as $user){
        if (!$res=$modx->getObject("PromoUsersSecret", array("id_user"=>$user["user_id"], "id_promo"=>$promo_id))){
            $secret_user = $modx->newObject("PromoUsersSecret");
            $secret_user->set("id_user", $user["user_id"]);
            $secret_user->set("id_promo", $promo_id);
            $secret_user->set("segment_id", $user["segment_id"]);
            $secret_user->save();
        }
    }
    return 1;