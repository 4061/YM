id: 138
source: 1
name: check-code
properties: 'a:0:{}'

-----

$code = $_SESSION['minishop2']['promocodes'];
$sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
$current_code = $modx->query($sql);
$current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);

if ($current_code[0]['type'] == 3){
    $costfourth = $_SESSION['minishop2']['total_cost'] * 0.25;
    if ($costfourth>=200){
        $actual_discount = 200;
    } else {
        $actual_discount = $costfourth;
    }
    $discount_cost = $_SESSION['minishop2']['total_cost'] - $actual_discount;
    $_SESSION['minishop2']['discount_cost'] = $discount_cost;
    $_SESSION['minishop2']['actual_discount'] = $actual_discount;
}
if ($current_code[0]['type'] == 1){
    $discount_percent = $current_code[0]['child_discount']/100;
    $actual_discount = round($_SESSION['minishop2']['total_cost']*$discount_percent);
    $discount_cost = round($_SESSION['minishop2']['total_cost'] - $actual_discount);
    $_SESSION['minishop2']['discount_cost'] = $discount_cost;
    $_SESSION['minishop2']['actual_discount'] = $actual_discount;
}