id: 229
name: userList
description: 'Список пользователей'
category: 'Шаблоны Админки'
properties: 'a:0:{}'

-----

$profiles = $modx->getCollection("modUserProfile");

$userListMass = array();
foreach($profiles as $key => $profile){
    
    if($profile->get("internalKey") != 1){
        $userListMass[$key]['id'] = $profile->get("internalKey");
        $userListMass[$key]['fullname'] = $profile->get("fullname");
        $userListMass[$key]['email'] = $profile->get("email");
        $userListMass[$key]['mobilephone'] = $profile->get("mobilephone");
        $userListMass[$key]['comment'] = $profile->get("comment");
        $userListMass[$key]['blocked'] = $profile->get("blocked");
        $userListMass[$key]['black_list_type'] = $profile->get("black_list_type");
    }
    
}

return $userListMass;