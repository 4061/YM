id: 60
source: 1
name: user
properties: 'a:0:{}'

-----

/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
*/


if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if($token) {
     if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
         $phone = $profile->get('mobilephone');
         $birthday = date("Y-m-d\TH:i:s",$profile->get('dob'));
        $birthday = substr($birthday, 0, 10);
        if ($birthday == '1970-01-01'){
            $birthday = 'Не указан';
        } else {
            $birthday = date("Y-m-d\TH:i:s",$profile->get('dob'));
        }
         $gender = $profile->get('gender');
         if ($gender == 2) {
             $gender = 'Женский';
         } elseif ($gender == 1){
             $gender = 'Мужской';
         } elseif ($gender == 0){
             $gender = 'Не указан';
         }
$sql = "SELECT *
FROM modx_msbonus2_users, modx_user_attributes, modx_msbonus2_levels
WHERE modx_user_attributes.mobilephone = '$phone' and modx_msbonus2_users.user = modx_user_attributes.internalKey and modx_msbonus2_users.level = modx_msbonus2_levels.id";
$info_user= $modx->query($sql);
$info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
if ($info_user) {
$bonus = $info_user[0]['points'];
$bonus_persent = $info_user[0]['bonus'];
$paid_money = $info_user[0]['paid_money'];
$level = $info_user[0]['level'];
$level_name = $info_user[0]['name'];
$sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = $level + 1 or id = $level ORDER BY id DESC LIMIT 1";
if ($modx->query($sql2)) {
$next_level = $modx->query($sql2);
$next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
$next_level = '{"id":"'.$next_level[0]['id'].'","bonus":"'.$next_level[0]['bonus'].'","cost":"'.$next_level[0]['cost'].'","name":"'.$next_level[0]['name'].'"}';
}
$cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');

} else {
    $bonus = '0';
    $bonus_persent = '0';
    $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
    $level = '1';
    $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = '2'";
if ($modx->query($sql2)) {
$next_level = $modx->query($sql2);
$next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
$level_name = $next_level[0]['name'];
$next_level = '{"id":"'.$next_level[0]['id'].'","bonus":"'.$next_level[0]['bonus'].'","cost":"'.$next_level[0]['cost'].'","name":"'.$next_level[0]['name'].'"}';
}
    $paid_money = '0';
}
         $email = $profile->get('email');
          $email = substr($email, -11);
if ($email == "apksite.ru") {
    $email = '';
} else {
    $email = $profile->get('email');
}
        $bonus_obj = $modx->getObject('msb2User', array("user"=>$profile->internalKey));
        $internalKey = $profile -> get('internalKey');
        $sql = "SELECT code FROM modx_promo_base WHERE author_id = '$internalKey'";
        $current_code = $modx->query($sql);
        $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
        $json['data'][] = array(
            "name"=>$profile->get('fullname'),
            "email"=>$email,
            "gender"=>$gender,
            "birthday"=>$birthday,
            "bonus_card" => "/uploads/barcodes/".$internalKey."card.jpg",
            "phone"=>'+'.$phone,
            "referal"=>array(
                "code"=>$current_code[0]['code'],
                "my_bonus"=>100,
                "friend_bonus"=>200,
                "ref_link"=>"google.com"
            ),
            "bonus"=>$bonus_obj->points
        );
        return json_encode($json);
echo '
{
    "data": [
            {
                "name": "'.$profile->get('fullname').'",
                "email": "'.$email.'",
                "bonus": "'.$profile->get("state").'",
                "gender": "'.$gender.'",
                "birthday": "'.$birthday.'",
                "phone": "+'.$phone.'",
                "referal_code": "'.$current_code[0]['code'].'"
            }
    ]
}';
}   else {
echo '
{
    "data": [
        {"error":"Токен устарел"}
    ]
}';
http_response_code(401);
} 
} else {
echo '
{
    "data": [
        {"error":"Нет Токена"}
    ]
}';
http_response_code(401);
} 
    
} else {
echo '
{
    "data": [
        {"error":"Ошибка формата"}
    ]
}';
http_response_code(400);
}