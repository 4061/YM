id: 153
source: 1
name: newcatalog
description: 'рекурсивный возврат категорий дерева ресурсов '
properties: 'a:0:{}'

-----

function getCatalog($parent_id_cat){
    global $modx;
    $sql = "
    SELECT 
        modx_site_content.id, 
        modx_site_content.pagetitle as title, 
        modx_site_content.description,
        modx_site_content.menuindex        
    FROM 
        `modx_site_content`
    WHERE 
        modx_site_content.parent = '$parent_id_cat' AND 
        modx_site_content.class_key = 'msCategory'";
    $content_id = $modx->query($sql);
    $resources = $content_id->fetchAll(PDO::FETCH_ASSOC);
    foreach ($resources as $key => $res){
        $res_obj = $modx->getObject('modResource', $res['id']);
        $resources[$key]['image']=$res_obj->getTVValue('image');
        $resources[$key]['parent']=$res_obj->get('parent');
        if ($res["id"] == 39268){
            $resources[$key]['show_full_category']=true;   
            $resources[$key]['parent_id']=4978;   
        } else {
            $resources[$key]['show_full_category']=false; 
        }
    }
    foreach ($resources as $key => $res){
        $res = getCatalog($res['id']);
        if ($res){
            usort($res, 'mysort1');
            $resources[$key]['data'] = $res;  
        }
    } 
    return $resources;
}
function mysort1($a, $b){
    return $a['menuindex'] <=> $b['menuindex'];
}
$json["data"] = getCatalog(4975);
usort($json["data"], 'mysort1');
return json_encode($json);