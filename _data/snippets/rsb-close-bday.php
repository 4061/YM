id: 170
source: 1
name: rsb-close-bday
properties: 'a:0:{}'

-----

$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$url = str_replace('http://','',$url);
$sql = "SELECT id FROM modx_site_content WHERE template = 18";
$info_user= $modx->query($sql);
$info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
foreach ($info_user as $org){
    $res = $modx->getObject("modResource", $org["id"]);
    $merchant = $res->getTVValue("merchantID");
    $params = array(
        'command' => 'b',
        'amount' => 15600,
        'description' => 'Заказ #' . 132,
        'currency' => 643,
        'client_ip_addr'=>"5.101.157.102");
    $request = http_build_query($params);
    $curlOptions = array(
        CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
        CURLOPT_CONNECTTIMEOUT => 5,
        CURLOPT_TIMEOUT => 35,
        CURLOPT_HEADER => false,
        CURLOPT_POST => true,
        CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
        CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
        CURLOPT_SSLVERSION => 6,
        CURLOPT_POSTFIELDS => $request,
    );
        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);
        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
        }
        curl_close($ch);
}