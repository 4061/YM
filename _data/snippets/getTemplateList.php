id: 244
name: getTemplateList
description: 'Сниппет для получения списка шаблонов (специальные чанки)'
category: 'Шаблоны Админки'
properties: null

-----

$where = array(
    'category' => 32,
);
$chankList = $modx->getCollection('modChunk',$where);

$chankMass = array();
foreach($chankList as $chank) {
    $chankMass[] = array(
        "id" => $chank->get("id"),
        "title" => $chank->get("name"),
    );
}

return $chankMass;