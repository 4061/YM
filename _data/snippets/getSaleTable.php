id: 101
name: getSaleTable
properties: null

-----

$maxavg = $_GET['max_avg'];
$lastdays = $_GET['days'];
$min_cart_cost = $_GET['min_cartcost'];
$max_cart_cost = $_GET['max_cartcost'];
$min_count = $_GET['min_count'];
$max_count = $_GET['max_count'];
$products = array(
    "1" => $_GET['product_1'], 
    "2" => $_GET['product_2'],
    "3" => $_GET['product_3'],
    "4" => $_GET['product_4']
);
$product_keys_placeholder = '';
foreach ($products as $product){
    if ($product){
        $product_keys_placeholder = $product_keys_placeholder." AND ord.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
    }
}
if ($max_cart_cost){
    $max_cart_cost_placeholder = " AND ord.id IN (Select id FROM modx_ms2_orders WHERE cart_cost<='$max_cart_cost')";
}
if ($min_cart_cost){
    $min_cart_cost_placeholder = " AND ord.id IN (Select id FROM modx_ms2_orders WHERE cart_cost>='$min_cart_cost')";
}
$minavg = $_GET['min_avg'];
if ($lastdays){
    $date_placeholder = " AND ord.createdon > CURRENT_TIMESTAMP - '$lastdays'*864000";
}
$sql = 
"SELECT 
    MAX(us.id) as ID_us, 
    MAX(us_atr.fullname) as ID_fullname, 
    MIN(us.username) as ID_username, 
    MIN(ord.createdon) as time_start,
    MAX(ord.createdon) as time_END,
    Count(*) as ord_count, 
    AVG(ord.cart_cost) as  ord_avg, 
    SUM(ord.cart_cost) as  ord_summ
FROM modx_users as us,
   modx_user_attributes as us_atr,
   modx_ms2_orders as ord
WHERE 
    us.id= ord.user_id AND 
    us.id= us_atr.internalKey AND
    ord.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>0 $date_placeholder) $min_cart_cost_placeholder $max_cart_cost_placeholder $product_keys_placeholder
Group By us.id
HAVING 1";
if ($maxavg>0){
    $sql =$sql." AND ord_avg <= ".$maxavg;
} 
if ($minavg>0){
    $sql =$sql." AND ord_avg >= ".$minavg;
}
if ($min_count){
    $sql = $sql." AND Count(*)>=".$min_count;
}
if ($max_count){
    $sql = $sql." AND Count(*)<=".$max_count;
}
$info_user= $modx->query($sql);
$info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
for ($i = 0; $i <= count($info_user)-1 ; $i++){
    $info_user[$i]['ord_summ'] = round($info_user[$i]['ord_summ']);
    $info_user[$i]['ord_avg'] = round($info_user[$i]['ord_avg']);
}
return $info_user;