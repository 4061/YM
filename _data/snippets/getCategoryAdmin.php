id: 233
source: 1
name: getCategoryAdmin
description: 'Получение списка категорий для админ.панели'
category: 'Шаблоны Админки'
properties: 'a:0:{}'

-----

$where = array(
    "parent" => 1561,
    "template" => 3
);

$categoryes = $modx->getCollection("msCategory",$where);

$defaultCategory = ($defaultCategory)? $defaultCategory : "";
$categoryId = ($categoryId && $categoryId != "")? $categoryId : $defaultCategory;

$categoryList = array();
foreach ($categoryes as $category) {
    $selected = ($categoryId == $category->get("id"))? "selected" : "";
    $categoryList[] = array(
            "id" => $category->get("id"),
            "title" => $category->get("pagetitle"),
            "selected" => $selected
        );
}

return $categoryList;