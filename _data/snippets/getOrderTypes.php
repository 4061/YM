id: 262
source: 1
name: getOrderTypes
properties: 'a:0:{}'

-----

$pdoFetch = new pdoFetch($modx);
$orderTypes = array(
    'class' => 'msDelivery',
    'select' => ["id", "name", "description"],
    'where' => ["active" => true],
    'sortdir' => "ASC",
    'sortby' => "rank",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($orderTypes);
$orderTypes = $pdoFetch->run();
return($orderTypes);