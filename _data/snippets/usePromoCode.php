id: 128
source: 1
name: usePromoCode
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $code = $_POST['JSON']['promocode'];
        $cart = $_POST['JSON']['products'];
        $profile = $modx->getObject('modUserProfile', ['website' => $token]);
        $internalKey = $profile -> get('internalKey');
        $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
        $current_code = $modx->query($sql);
        $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
        $id_promo = $current_code[0]['id_promo'];
        $max_use  = $current_code[0]['user_use_count']; // получаем макисмальное кол-во использований этого промокода пользователем..
        $type = $current_code[0]['type'];
        if ($current_code[0]['activity']){
            if ($type == 1){
                if ($current_code[0]['use_count'] != 0){
                        $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$internalKey' AND promo_id = '$id_promo'";
                        $countCheck = $modx->query($sql);
                        $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
                        if (count($countCheck) < $max_use){
                            $sql = "
                            INSERT INTO 
                                `modx_promo_users` 
                                    (
                                        `user_id`,
                                        `promo_id`,
                                        `use_count`,
                                        `activation_status`,
                                        `order_id`,
                                        `type`,
                                        `use_time`
                                    ) 
                                    VALUES 
                                    (
                                        '$internalKey',
                                        '$id_promo',
                                        '',
                                        '0',
                                        '',
                                        '$type',
                                        CURRENT_TIMESTAMP
                                    )";
                            $modx->query($sql);
                        $json['products'] = $cart;
                        $json['activated_promocode'] = $code;
                        $json['discount'] = $current_code[0]['child_discount']; 
                        return json_encode($json);
                    } else {
                        $json['data'] = array("response" => false, "errorMessage" =>"Вы использовали промокод максимальное количество раз." );
                        return json_encode($json);
                    }
                } else {
                    $json['data'] = array("response" => false, "errorMessage" =>"Промокод использован максимальное количество раз." );
                    return json_encode($json);
                }
            }
            if ($type == 2){
                if ($current_code[0]['use_count'] != 0){
                    foreach ($cart as $cart_item){
                        if ($cart_item['id'] == $current_code[0]['id_product']){
                            $json['data'] = array("response" => false, "errorMessage" =>"Достигнуто максимальное количество использований промокода в корзине!");
                            return json_encode($json);
                        } else {
                            $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$internalKey' AND promo_id = '$id_promo'";
                            $countCheck = $modx->query($sql);
                            $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
                            if (count($countCheck) < $max_use){
                                $sql = "
                                INSERT INTO 
                                    `modx_promo_users` 
                                        (
                                            `user_id`,
                                            `promo_id`,
                                            `use_count`,
                                            `activation_status`,
                                            `order_id`,
                                            `type`,
                                            `use_time`
                                        ) 
                                        VALUES 
                                        (
                                            '$internalKey',
                                            '$id_promo',
                                            '',
                                            '0',
                                            '',
                                            '$type',
                                            CURRENT_TIMESTAMP
                                        )";
                                $modx->query($sql);
                                $promocode_product = array(
                                    "id"=>intval($current_code[0]['id_product']),
                                    "count"=>1,
                                    "promo_product" => true
                                );
                                array_push($cart,$promocode_product);
                                $json['products'] = $cart;
                                $json['activated_promocode'] = $code;
                                $json['discount'] = $current_code[0]['child_discount']; 
                                return json_encode($json);
                            } else {
                                $json['data'] = array("response" => false, "errorMessage" =>"Вы использовали промокод максимальное количество раз." );
                                return json_encode($json);
                            }
                        }
                    }
                } else {
                    $json['data'] = array("response" => false, "errorMessage" =>"Промокод использован максимальное количество раз." );
                    return json_encode($json);
                }
            }
            if ($type == 3){
                $use_count = $current_code[0]['use_count'];
                if ($use_count != 0){
                    $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$internalKey'";
                    $referalCheck = $modx->query($sql);
                    $referalCheck = $referalCheck->fetchAll(PDO::FETCH_ASSOC);
                    if (count($referalCheck) == 0){
                        $sql = "
                        INSERT INTO 
                            `modx_promo_users` 
                                (
                                    `user_id`,
                                    `promo_id`,
                                    `use_count`,
                                    `activation_status`,
                                    `order_id`,
                                    `type`,
                                    `use_time`
                                ) 
                                VALUES 
                                (
                                    '$internalKey',
                                    '$id_promo',
                                    '',
                                    '0',
                                    '',
                                    '3',
                                    CURRENT_TIMESTAMP
                                )";
                        $modx->query($sql);
                        $json['products'] = $cart;
                        $json['activated_promocode'] = $code;
                        $json['discount'] = $current_code[0]['child_discount']; 
                        return json_encode($json);
                    } else {
                        $json['data'] = array("response" => false, "errorMessage" =>"Вам недоступно использование данного кода." );
                        return json_encode($json);
                    }
                } else {
                    $json['data'] = array("response" => false, "errorMessage" =>"Промокод использован максимальное количество раз." );
                    return json_encode($json);
                }
            }
        } else {
            $json['data'] = array("errorMessage" =>"Промокод не активен" );
            return json_encode($json);
        }
    } else {
        $json['data'] = array("errorMessage" =>"Токен устарел" );
        return json_encode($json);
    }
}