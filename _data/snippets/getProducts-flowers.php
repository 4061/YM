id: 202
source: 1
name: getProducts_flowers
category: flowerheaven
properties: 'a:0:{}'

-----

$modx->addPackage('softjetsync', MODX_CORE_PATH . 'components/softjetsync/model/');
$SoftjetSync = $modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/');
if (
    'application/json' == $_SERVER['CONTENT_TYPE']
    && 'POST' == $_SERVER['REQUEST_METHOD']
) {
    
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $_POST['JSON'] = &$_REQUEST['JSON'];
    $rest_id = $_POST['JSON']['rest_id'];
    $category_id = $_POST["JSON"]["category_id"];
    $barcodes_arr = $_POST["JSON"]["barcodes"];
    //return json_encode($barcodes_arr);;
    $_SESSION['minishop2']['order_type'] = $_POST['JSON']['order_type'];
    if ($_POST['JSON']['order_type'] == 1){
        unset($_SESSION["min_delivery_cost"]);
    }
    if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
        $modx->log(1,json_encode($profile));
        $userid = $profile->get('id');
        $modx->log(xPDO::LOG_LEVEL_ERROR, 'id юзера: ' . $userid);
        $sql = "SELECT *
                FROM modx_msbonus2_users, modx_user_attributes, modx_msbonus2_levels
                WHERE modx_user_attributes.id = '$userid' and modx_msbonus2_users.user = modx_user_attributes.internalkey and modx_msbonus2_users.level = modx_msbonus2_levels.id";
        $info_user = $modx->query($sql);
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        if ($info_user) {

            $bonus = $info_user[0]['points'];
            $bonus_persent = $info_user[0]['bonus'];
            $paid_money = $info_user[0]['paid_money'];
            $level = $info_user[0]['level'];
            $level_name = $info_user[0]['name'];
            $modx->log(xPDO::LOG_LEVEL_ERROR, 'количество бонусов: ' . $bonus);
         

            $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = $level";
            if ($modx->query($sql2)) {
                $next_level = $modx->query($sql2);
                $next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
                $bonus_persent = $next_level[0]['bonus'];
            }
            $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
        } else {

            $sql = "SELECT * FROM `modx_msbonus2_levels` WHERE `cost` = 0";
            if ($modx->query($sql)) {
                $bonus_persent = $modx->query($sql);
                $bonus_persent = $bonus_persent->fetchAll(PDO::FETCH_ASSOC);
                $bonus_persent = $bonus_persent[0]['bonus'];
                $bonus = $bonus_persent[0]['name'];
            }
            $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
            $level = '1';
            $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = '2'";
            if ($modx->query($sql2)) {
                $next_level = $modx->query($sql2);
                $next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
                $level_name = $next_level[0]['name'];
                $next_level = '{"id":"' . $next_level[0]['id'] . '","bonus":"' . $next_level[0]['bonus'] . '","cost":"' . $next_level[0]['cost'] . '","name":"' . $next_level[0]['name'] . '"}';
            }
            $paid_money = '0';
        }
    }
} else {
    $sql = "SELECT * FROM `modx_msbonus2_levels` WHERE `cost` = 0";
    if ($modx->query($sql)) {
        $bonus_cost = $modx->query($sql);
        $bonus_cost = $bonus_cost->fetchAll(PDO::FETCH_ASSOC);
        $bonus_persent = $bonus_cost[0]['bonus'];
    }
}
$link = 1;
$innerJoin = array();
if (!empty($link) && !empty($master)) {
    $innerJoin['Link'] = array(
        'class' => 'msProductLink',
        'on' => 'msProduct.id = Link.slave AND Link.link = ' . $link,
    );
    $where['Link.master'] = $master;
} elseif (!empty($link) && !empty($slave)) {
    $innerJoin['Link'] = array(
        'class' => 'msProductLink',
        'on' => 'msProduct.id = Link.master AND Link.link = ' . $link,
    );
    $where['Link.slave'] = $slave;
}

if (!$category_id){
    $category_id = 2;
}
$pdoFetch = new pdoFetch($modx);
/*$default = array(
    'class' => 'msProduct',
    'parents' => $category_id,
    'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
    'leftJoin' => ['Data' => ['class' => 'msProductData'],
        'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
        'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
        'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
        'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
        'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
        'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
        'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "times_bought"'],
    ],
    'select' => [
        'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle`, parent, `msProduct`.`description`',
        'Data' => '`Data`.`length`,`Data`.`restourants`,`Data`.`quantityinpack`,`Data`.`order_types`,`Data`.`price`,`Data`.`price2`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
        'Option1' => 'Option1.value as energy_size',
        'Option2' => 'Option2.value as energy_allergens',
        'Option3' => 'Option3.value as energy_value',
        'Option4' => 'Option4.value as energy_carbohydrates',
        'Option5' => 'Option5.value as energy_protein',
        'Option6' => 'Option6.value as energy_oils',
        'Option7' => 'Option7.value as times_bought',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => 1000
);
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();
*/
$search_query = $_POST["JSON"]["search_string"];
if ($search_query){
    $search_results = $modx->runSnippet("apiSearch", ["parents" => 3524, "query" => $search_query, "returnIds" => true, "limit" => 10000]);
    
    $search_results = explode(",",$search_results);
    //return json_encode($search_results);
}
$pdoFetch = new pdoFetch($modx);
$barcodes = array(
    'class' => 'SoftjetsyncBarcodes',
    'parents' => 1561,
    'where' => ["barcode:IN" => $barcodes_arr],
    'select' => [
        'SoftjetsyncBarcodes' => '`SoftjetsyncBarcodes`.`product_id`',
    ],
    'sortdir' => 'ASC',
    'return' => 'data',
    'limit' => 1000
);
$pdoFetch->setConfig($barcodes);
$barcodes = $pdoFetch->run();
foreach ($barcodes as $barcode){
    $search_results[] = $barcode["product_id"];
}
if ($profile){
    $price_type = $profile->get("state");
}
if (!$price_type){
    $price_type = 1;
}
$org = $modx->getObject("modResource", $rest_id);
$price_type = $org->getTVValue("deliveryTerminalId");
//$price_type = 1;
$category_id = (int)$category_id;
$default = array(
    'class' => 'msProduct',
    'parents' => $category_id,
    'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0,'Price.price:IS NOT' => null],
    'leftJoin' => [
      'Data' => ['class' => 'msProductData'],
      'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type],
      'Remains' => ['class' => 'SoftjetsyncRemains', 'on' => 'Remains.product_id = msProduct.id'],
    ],
    'select' => [
        'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`',
        'Price' => 'Price.price as price',
        'Remains' => 'remnant, coming',
        'Data' => '`Data`.`length` as weight,`Data`.`restourants`,`Data`.`quantityinpack`,`Data`.`order_types`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => 1000,
    'offset' => ($page-1)*$limit
);
//return json_encode($search_results);
if ($search_results){
    $default["where"]["id:IN"] = $search_results;
    //return json_encode($search_results);
}
if ($barcodes_arr && !$barcodes){ // если есть поиск по баркоду но продукт не нашло
   $default["where"]["id:IN"] = 1;
}
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();

$SoftjetSync->shuffle($rows, 'SoftjetsyncBarcodes', 'barcodes', 'id', 'product_id');
//Собираем все id товаров в отдельный массив
$master_ids = array_column($rows, 'id');
$adds = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 3, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($adds);
$dobavki = $pdoFetch->run();
$types = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 9, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($types);
$types = $pdoFetch->run();

$ingredients = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 8, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`, `Data`.`image` as img_url, `Product`.`pagetitle` as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($ingredients);
$ingredients = $pdoFetch->run();

$analogs = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 10, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($analogs);
$analogs = $pdoFetch->run();
$json['data'] = array();
$rows = array_combine(array_column($rows, 'id'), $rows);
foreach ($analogs as $analog){
    $rows[$analog['master']]['analog'][] = $analog;
}
foreach ($ingredients as $ingredient){
    $rows[$ingredient['master']]['ing'][] = $ingredient;
}
foreach ($dobavki as $dobavka){
    $rows[$dobavka['master']]['adds'][] = $dobavka;
}
foreach ($types as $type){
    $rows[$type['master']]['types'][] = $type;
}
//return json_encode($rest_id);
foreach ($rows as $key => $row){
            $json['data'][$key]['id'] = $row['id'];
            $json['data'][$key]['title'] = $row['title'];
            $json['data'][$key]['parent'] = $row['parent'];
            $json['data'][$key]['article'] = (int)$row['article'];
            /*if ($_POST['JSON']['order_type'] == 1){
                $json['data'][$key]['price'] = (int)$row['price2'];
            } else {
                $json['data'][$key]['price'] = (int)$row['price'];    
            }*/
            $json['data'][$key]['price'] = (double)$row['price'];
            $json['data'][$key]['desc'] = $row['description'];
            $json['data'][$key]['new'] = (int)$row['new'];
            $json['data'][$key]['popular'] = (int)$row['popular'];
            $json['data'][$key]['favorite'] = (int)$row['favorite'];
            $json['data'][$key]['img_big'] = $row['img_big'];
            $json['data'][$key]['img_small'] = $row['img_small'];
            $json['data'][$key]['bonus_add'] = floor($row['price'] / 100 * $bonus_persent);
            $json['data'][$key]['times_bought'] = (int)$row['times_bought'];
            $json['data'][$key]['remains_count'] = (int)$row["remnant"];
            if ((int)$row["remnant"] != 0){
                $json['data'][$key]['remains_text'] = "В наличии";
            } else {
                $json['data'][$key]['remains_text'] = "Ожидается ".$row["coming"];
            }
            
            $json['data'][$key]['barcode'] = $row["barcode"];
            $json['data'][$key]['length'] = (int)$row["weight"]." см";
            $json['data'][$key]['pieces_per_package'] = (int)$row["quantityinpack"];
            /*$en_size = explode(",",$row['energy_size']);
            $l = $en_size[1]*100;
            $l = $en_size[1]*1;
            $l = $l/10;
            $k = $en_size[0];
            $k = $k*1000;
            $fin = $fin+$l;*/
            $json['data'][$key]['energy'] = array(
                "energy_size" => (int)$row["weight"]." см",
                "energy_value" => round($row['energy_value']),    
                "energy_allergens" => $row['energy_allergens'],
                "energy_carbohydrates" => round($row['energy_carbohydrates']),
                "energy_protein" => round($row['energy_protein']),
                "energy_oils" => round($row['energy_oils']),
            );
            for ($i=0; $i<count($row['adds']); $i++){
                $row['adds'][$i]['price'] = (int)$row['adds'][$i]['price'];
                $row['types'][$i]['price'] = (int)$row['types'][$i]['price'];
            }
            if ($row['adds']){
                $json['data'][$key]['additives'] = $row['adds'];
            } else {
                $json['data'][$key]['additives'] = [];
            }
            if ($row['ing']){
                $json['data'][$key]['ingredients'] = $row['ing'];    
            } else {
                $json['data'][$key]['ingredients'] = [];
            }
            if ($row['types']){
                $json['data'][$key]['types'] = $row['types'];
            } else {
                $json['data'][$key]['types'] = [];
            }
            if ($row['analog']){
                $json['data'][$key]['analogs'] = $row['analog'];
            } else {
                $json['data'][$key]['analogs'] = [];
            }
}
if ($modx->getOption("getRating")){
    $str = implode(",",$master_ids);
    $sql = "SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE contentid in ($str) AND tmplvarid=11";
    $info_user = $modx->query($sql);
    $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
    $vals1 = array_column($info_user, 'value');
    $vals2 = array_column($info_user, 'contentid');
    $vals = array_combine($vals2, $vals1);
    foreach ($vals as $key => $val){
        $product_rating = json_decode($val, true);
        $middle_rating = (floatval($product_rating[0]['sum'])+(floatval($product_rating[0]['five'])*5))/(floatval($product_rating[0]['count'])+(floatval($product_rating[0]['five'])));
        if ($product_rating[0]['count'] == 0){
            $middle_rating = 5;
        }
        if ($json['data'][$key]){
            $json['data'][$key]['rating'] = (double)$middle_rating;
            $json['data'][$key]['rating'] = round($json['data'][$key]['rating']);
        }
    }
    foreach ($json['data'] as $key => $js){
        if (!$js['rating']){
            if ($json['data'][$key]){
                $json['data'][$key]['rating'] = 5;
            }
        }
    }
}
$json1['data'] = array_values($json['data']);
return json_encode($json1);