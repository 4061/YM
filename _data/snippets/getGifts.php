id: 260
source: 1
name: getGifts
properties: 'a:0:{}'

-----

$pdoFetch = new pdoFetch($modx);
$gifts = array(
    'class' => 'modResource',
    'sortdir' => "desc",
    'return' => 'data',
    'select' => ["id", "pagetitle"],
    'where' => ["parent"=>1739],
    'limit' => 10000
);
$pdoFetch->setConfig($gifts);
$gifts = $pdoFetch->run();
return($gifts);