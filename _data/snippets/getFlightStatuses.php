id: 250
source: 1
name: getFlightStatuses
snippet: "$pdoFetch = new pdoFetch($modx);\n$flight_statuses = array(\n    'class' => 'CourierFlightStatuses',\n    'sortdir' => \"ASC\",\n    'return' => 'data',\n    'limit' => 10000\n);\n$pdoFetch->setConfig($flight_statuses);\n$flight_statuses = $pdoFetch->run();\nreturn $flight_statuses;"
properties: 'a:0:{}'
static: 1
static_file: /core/components/courier/elements/snippets/getFlightStatuses.php
content: "$pdoFetch = new pdoFetch($modx);\n$flight_statuses = array(\n    'class' => 'CourierFlightStatuses',\n    'sortdir' => \"ASC\",\n    'return' => 'data',\n    'limit' => 10000\n);\n$pdoFetch->setConfig($flight_statuses);\n$flight_statuses = $pdoFetch->run();\nreturn $flight_statuses;"

-----


$pdoFetch = new pdoFetch($modx);
$flight_statuses = array(
    'class' => 'CourierFlightStatuses',
    'sortdir' => "ASC",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($flight_statuses);
$flight_statuses = $pdoFetch->run();
return $flight_statuses;