id: 139
source: 1
name: getPromotions
properties: 'a:0:{}'

-----

$_REQUEST['JSON'] = json_decode(
    file_get_contents('php://input'), true
);
$token = getallheaders();
if ($token['Authorization']) $token2 = $token['Authorization'];
if ($token['authorization']) $token2 = $token['authorization'];
$token = substr($token2, 7);
$_POST['JSON'] = & $_REQUEST['JSON'];
    $sql = "
    SELECT 
        id_promo as id,
        name as title,
        description as content,
        date_end,
        id_product as product_id,
        code as promocode,
        type as type,
        child_discount as discount,
        image as img_big,
        url as url
    FROM 
        modx_promo_base 
    WHERE
        (type = 1 and activity = 1 OR
        type = 2 and activity = 1) AND
        availability <> 2
    ";
    $promocodes = $modx->query($sql);
    $promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);
if ($profile = $modx->getObject('modUserProfile', ['website' => $token])){
    $ids = [];
    $categories = $modx->getCollection("PromoUsersSecret",array("id_user" => $profile->get("internalKey")));
    foreach ($categories as $cat){
        $ids[] = $cat->get("id_promo");
    }
    $ids = array_unique($ids);
    $pdoFetch = new pdoFetch($modx);
    $secret_promotions = array(
        'class' => 'PromoBase',
        'where' => ["id_promo:IN" => $ids],
        'select' => ["
            `PromoBase`.`id_promo` as id, 
            name as title, description as content,
            date_end,
            id_product as product_id,
            code as promocode,
            type as type,
            child_discount as discount,
            image as img_big,
            url as url"],
        'sortdir' => "ASC",
        'return' => 'data',
        'limit' => 10000
    );
    $pdoFetch->setConfig($secret_promotions);
    $secret_promotions = $pdoFetch->run();
    if (is_array($secret_promotions)){
        $promocodes = array_merge($promocodes,$secret_promotions);
    }
} 
foreach ($promocodes as $key => $promocode){
        $image_small = $modx->runSnippet('pthumb', [
        'input' => substr($promocode["img_big"], 1),
        'options' => '&h=120&q=100',]);
        $promocodes[$key]['img_small'] = $image_small ;
        if ($promocodes[$key]['date_end']){
            $promocodes[$key]['date_end'] = strtotime($promocode['date_end']);
        }
}
return json_encode($promocodes);