id: 183
source: 1
name: admin_changebonussystem
snippet: "$_REQUEST['JSON'] = json_decode(\n             file_get_contents('php://input'), true\n    );\n    foreach ($_REQUEST['JSON'] as $key => $system){\n        $USA = $modx->getObject('modSystemSetting', $key);\n        if (!$system){\n            $system = 0;\n        }\n        $USA->set('value', $system);\n        $USA->save();\n        $arr[$key] = $system;\n    }\n    $modx->cacheManager->refresh(array('system_settings' => array()));\n    return json_encode($arr);"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_changebonussystem.php
content: "$_REQUEST['JSON'] = json_decode(\n             file_get_contents('php://input'), true\n    );\n    foreach ($_REQUEST['JSON'] as $key => $system){\n        $USA = $modx->getObject('modSystemSetting', $key);\n        if (!$system){\n            $system = 0;\n        }\n        $USA->set('value', $system);\n        $USA->save();\n        $arr[$key] = $system;\n    }\n    $modx->cacheManager->refresh(array('system_settings' => array()));\n    return json_encode($arr);"

-----


$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    foreach ($_REQUEST['JSON'] as $key => $system){
        $USA = $modx->getObject('modSystemSetting', $key);
        if (!$system){
            $system = 0;
        }
        $USA->set('value', $system);
        $USA->save();
        $arr[$key] = $system;
    }
    $modx->cacheManager->refresh(array('system_settings' => array()));
    return json_encode($arr);