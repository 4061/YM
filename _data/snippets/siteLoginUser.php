id: 106
name: siteLoginUser
properties: null

-----

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} //функция генерации пароля
//$token = "911a172d43ddfa2d79432fc97fefc094"; //получаем токен
$token = $_GET['token'];
$pass = generateRandomString(6); //генерируем новый пароль
$profile = $modx->getObject('modUserProfile', ['website' => $token]); //получаем объект юзера по токену
$internalKey = $profile->get('internalKey');
$user = $modx->getObject('modUser', $internalKey); //получаем пользователя
$username = $user->get('username');
            $user->set('password', $pass); //устанавлиаем пароль);
            $user->save(); //сохраняем профиль
$logindata = array(
  'username' => $username,   // имя пользователя
  'password' => $pass, // пароль
  'rememberme' => true        // запомнить?
);
$response = $modx->runProcessor('/security/login', $logindata);
if ($response->isError()) {
  // произошла ошибка, например неверный пароль
  $modx->log(modX::LOG_LEVEL_ERROR, 'Login error. Message: '.$response->getMessage());
  $data = false;
} else{
    $data = true;
}
return json_encode($data);