id: 182
source: 1
name: admin_loyalty
snippet: "$PH_PROGRAM = $modx->getOption('loyalty_PH_program');\n$MAIN_PROGRAM = $modx->getOption('loyalty_main_program');\n$programms[\"prime_hill\"] = array(\"title\" => \"Прайм-Хилл\", \"desc\" => \"Бонусная система Prime-Hill.\", \"value\" => $PH_PROGRAM, \"setting\" => \"loyalty_PH_program\" );\n$programms[\"main_programm\"] = array(\"title\" => \"SOFTJET\", \"desc\" => \"Основная бонусная система.\", \"value\" => $MAIN_PROGRAM, \"setting\" => \"loyalty_main_program\" );\nreturn($programms);"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_loyalty.php
content: "$PH_PROGRAM = $modx->getOption('loyalty_PH_program');\n$MAIN_PROGRAM = $modx->getOption('loyalty_main_program');\n$programms[\"prime_hill\"] = array(\"title\" => \"Прайм-Хилл\", \"desc\" => \"Бонусная система Prime-Hill.\", \"value\" => $PH_PROGRAM, \"setting\" => \"loyalty_PH_program\" );\n$programms[\"main_programm\"] = array(\"title\" => \"SOFTJET\", \"desc\" => \"Основная бонусная система.\", \"value\" => $MAIN_PROGRAM, \"setting\" => \"loyalty_main_program\" );\nreturn($programms);"

-----


$PH_PROGRAM = $modx->getOption('loyalty_PH_program');
$MAIN_PROGRAM = $modx->getOption('loyalty_main_program');
$programms["prime_hill"] = array("title" => "Прайм-Хилл", "desc" => "Бонусная система Prime-Hill.", "value" => $PH_PROGRAM, "setting" => "loyalty_PH_program" );
$programms["main_programm"] = array("title" => "SOFTJET", "desc" => "Основная бонусная система.", "value" => $MAIN_PROGRAM, "setting" => "loyalty_main_program" );
return($programms);