id: 130
source: 1
name: getOrderByHistory
properties: 'a:0:{}'

-----

$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();
    
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    
    $_POST['JSON'] = & $_REQUEST['JSON'];
    $order_id = $_POST['JSON']['order_id'];
    if ($token){
        $profile = $modx->getObject('modUserProfile', ['website' => $token]); //получаем пользователя по token
        $user_id = $profile->get('internalKey');
        $sql = "
        SELECT 
            modx_ms2_orders.cost as cost,
            modx_ms2_orders.id as order_id,
            modx_ms2_orders.status as status_id,
            modx_ms2_order_statuses.color as status_color,
            modx_ms2_order_statuses.description as status_name,
            modx_ms2_orders.delivery as delivery_id,
            modx_ms2_deliveries.name as delivery_name,
            modx_ms2_orders.payment as payment_id,
            modx_ms2_payments.name as payment_name,
            modx_ms2_orders.createdon as createdon,
            modx_ms2_orders.comment as comment,
            modx_ms2_order_addresses.city as city,
            modx_ms2_order_addresses.index as org_id,
            modx_ms2_order_addresses.street as street,
            modx_ms2_order_addresses.floor as floor,
            modx_ms2_order_addresses.region as region,
            modx_ms2_order_addresses.house_num as house_num,
            modx_ms2_order_addresses.flat_num as flat_num,
            modx_ms2_order_addresses.entrance as entrance,
            modx_ms2_order_addresses.referalDiscount as promocode,
            modx_ms2_order_addresses.discount as discount,
            modx_ms2_order_addresses.tasteAssessment as rating,
            modx_msbonus2_orders.writeoff as bonus_writeoff,
            modx_msbonus2_orders.accrual as bonus_accrual
        FROM 
            modx_ms2_orders,modx_ms2_order_addresses,modx_msbonus2_orders,modx_ms2_order_statuses,modx_ms2_payments,modx_ms2_deliveries
        WHERE
            modx_ms2_order_addresses.id = modx_ms2_orders.address AND
            modx_msbonus2_orders.order = modx_ms2_orders.id AND
            modx_ms2_order_statuses.id = modx_ms2_orders.status AND
            modx_ms2_payments.id = modx_ms2_orders.payment AND
            modx_ms2_deliveries.id = modx_ms2_orders.delivery AND
            modx_ms2_orders.id = '$order_id'
        ";
        $info_user= $modx->query($sql);
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        $additives = array();
        for ($i = 0; $i<count($info_user);$i++){
            $order_id = $info_user[$i]['order_id'];
                        $sql = "SELECT
                        modx_ms2_order_products.name as title, 
                        modx_ms2_order_products.product_id, 
                        modx_ms2_order_products.count, 
                        modx_ms2_order_products.price, 
                        modx_ms2_order_products.options,
                        modx_ms2_products.image as img_url
                    FROM 
                        modx_ms2_order_products, modx_ms2_products
                    WHERE 
                        modx_ms2_order_products.product_id = modx_ms2_products.id AND
                        modx_ms2_order_products.order_id = '$order_id'
                    GROUP BY 
                        modx_ms2_order_products.name
                    ";
            $info_product= $modx->query($sql);
            $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
            foreach ($info_product as $key=> $info_produc){
                $prod_id = (int)$info_product[$key]['product_id'];
                $info_product[$key]['product_id'] = (int)$info_product[$key]['product_id'];
                $info_product[$key]['count'] = (int)$info_product[$key]['count'];
                $info_product[$key]['price'] = (double)$info_product[$key]['price'];
                $info_product[$key]['img_url'] = $url.$info_product[$key]['img_url'];
                $sql = "
                SELECT 
                    modx_ms2_product_options.value 
                FROM 
                    modx_ms2_product_options 
                WHERE 
                    modx_ms2_product_options.key = 'energy_size' AND 
                    modx_ms2_product_options.product_id = '$prod_id'
                ";
                    $info_weight = $modx->query($sql);
                    $info_weight = $info_weight->fetchAll(PDO::FETCH_ASSOC);
                $info_product[$key]['weight'] = (int)$info_weight[0]["value"];
            }
            $arr = array();
            foreach ($info_product as $key => $product){
                $productOptions = json_decode($product['options'], true);
                $adds1 = $productOptions['childs']['additives'];
                $adds2 = $productOptions['childs']['type'];
                $adds = array_merge($adds1, $adds2);
                foreach ($adds as $add){
                    array_push($additives, $add["id"]);
                }
            }
            for ($j = 0; $j<count($info_product);$j++){
                $info_product[$j]['options'] = json_decode($info_product[$j]['options'],true);
                array_push($arr,$info_product[$j]);
            }
            $info_user[$i]['products'] = $arr;
        }
        /*
        for ($i = 0; $i<count($info_user);$i++){
            $order_id = $info_user[$i]['order_id'];
            $sql = "SELECT options, product_id,count, price, name FROM modx_ms2_order_products WHERE order_id = $order_id";
            $info_product= $modx->query($sql);
            $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($info_product as $product){
                $json_adds =  json_decode($product['options'], true);
                $json_adds = json_decode($json_adds['childs'][1], true); 
                $addsCount = count($json_adds);
                $ads = array();
                for ($j=0; $j<=$addsCount-1;$j++){
                    $childid = $json_adds[$j]['id'];
                    $childpagetitle = $json_adds[$j]['pagetitle'];
                    $childcount = $json_adds[$j]['count'];
                    $childprice = $json_adds[$j]['price'];
                    $thisAdd = array(
                        "product_id"=>$childid,
                        "title"=>$childpagetitle,
                        "price"=>$childprice,
                        "count"=>$childcount,
                        );
                    array_push($additives,$childid);
                    array_push($ads, $thisAdd);
                }
                $product = array(
                    "title"=>$product['name'],
                    "product_id"=>$product['product_id'], 
                    "count"=>$product['count'], 
                    "price"=>$product['price'], 
                    "options"=>$ads
                );
                array_push($arr,$product); 
            }
            $info_user[$i]['products'] = $arr;
        }
        */
        foreach ($info_user[0]['products'] as $key => $product){
            if (in_array($product['product_id'],$additives)){
                unset($info_user[0]['products'][$key]);
            }
        }
        $info_user[0]['products'] = array_values($info_user[0]['products']);
        $info_user[0]['cost'] = (double)$info_user[0]['cost'];
        $info_user[0]['order_id'] = (int)$info_user[0]['order_id'];
        $info_user[0]['status_id'] = (int)$info_user[0]['status_id'];
        $info_user[0]['delivery_id'] = (int)$info_user[0]['delivery_id'];
        $info_user[0]['payment_id'] = (int)$info_user[0]['payment_id'];
        $region =  explode(",", $info_user[0]['region']);
        $info_user[0]['region'] = array(
            "latitude" => $region[0],
            "longitude" => $region[1],
        );
        foreach ($info_user[0]['products'] as $key => $product){
            $info_user[0]['products'][$key]["options"]["childs"]["size"] = [];
        }
        return json_encode($info_user[0]);
        //$additives = array_unique($additives);
        //return json_encode($additives);
    }
}