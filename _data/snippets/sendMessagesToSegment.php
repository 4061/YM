id: 120
source: 1
name: sendMessagesToSegment
properties: 'a:0:{}'

-----

$action = $_GET['action'];
switch ($action){
    case "save":
        $user_id = $_GET['user_id'];
        $segment_id = $_GET['segment_id'];
        $message_id = $_GET['message_id'];
        $channel = $_GET['channel'];
        $sql = "
        INSERT INTO 
            modx_messages_logs
            (
                `user_id`, 
                `segment_id`,
                `message_id`,
                `channel`
            )
            VALUES
            (
                '$user_id',
                '$segment_id',
                '$message_id',
                '$channel'
            )
        ";
        $modx->query($sql);
        $sql = "SELECT * FROM `modx_messages_logs` ORDER BY `modx_messages_logs`.`id` desc";
        $messages_base= $modx->query($sql);
        $messages_base = $messages_base->fetchAll(PDO::FETCH_ASSOC);
        //return json_encode($messages_base);
        $id = $messages_base[0]['id'];
        $segment_id=$messages_base[0]['segment_id'];
        $message_id=$messages_base[0]['message_id'];
        $sql="
        SELECT 
            modx_segment_base.user_id,
            modx_messages_logs.message_id,
            modx_messages_blanks.message,
            modx_messages_logs.channel,
            modx_messages_logs.createdon
        FROM
            modx_segment_base,modx_messages_blanks,modx_messages_logs
        WHERE
            modx_messages_blanks.message_id = '$message_id' AND
            modx_segment_base.segment_id='$segment_id' AND
            modx_messages_logs.id = '$id'
            
            
        ";
        $messages_new_users= $modx->query($sql);
        $messages_new_users = $messages_new_users->fetchAll(PDO::FETCH_ASSOC);
        foreach ($messages_new_users as $new_user){
            $user_id = $new_user['user_id'];
            $message_id = $new_user['message_id'];
            $message = $new_user['message'];
            $channel = $new_user['channel'];
            $createdon = $new_user['createdon'];
            $sql = "
            INSERT INTO 
                modx_messages_base
                (
                    `user_id`, 
                    `message_id`,
                    `message`,
                    `createdon`,
                    `datesend`,
                    `channel`
                )
                VALUES
                (
                    '$user_id',
                    '$message_id',
                    '$message',
                    '$createdon',
                    '$createdon',
                    '$channel'
                )
            ";
            $modx->query($sql);
            echo json_encode($new_user);
            echo('</br>');
        }
        return(true);
    break;
    case "send":
        function send($host, $port, $login, $password, $phone, $text, $sender = false, $wapurl = false )
        {
          $fp = fsockopen($host, $port, $errno, $errstr);
          if (!$fp) {
            return "errno: $errno \nerrstr: $errstr\n";
          }
          fwrite($fp, "GET /send/" .
            "?phone=" . rawurlencode($phone) .
            "&text=" . rawurlencode($text) .
            ($sender ? "&sender=" . rawurlencode($sender) : "") .
            ($wapurl ? "&wapurl=" . rawurlencode($wapurl) : "") .
            " HTTP/1.0\n");
          fwrite($fp, "Host: " . $host . "\r\n");
          if ($login != "") {
            fwrite($fp, "Authorization: Basic " . 
              base64_encode($login. ":" . $password) . "\n");
          }
          fwrite($fp, "\n");
          $response = "";
          while(!feof($fp)) {
            $response .= fread($fp, 1);
          }
          fclose($fp);
          list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
          return $responseBody;
        }
        $sql = "SELECT * FROM modx_messages_base WHERE createdon >= DATE_ADD(CURDATE(), INTERVAL -12 HOUR)";
        $sms_users= $modx->query($sql);
        $sms_users = $sms_users->fetchAll(PDO::FETCH_ASSOC);
        foreach ($sms_users as $user){
            if ($user['status'] == 0){
                $current_user = $modx->getObject('modUserProfile', array('internalKey' => $user['user_id'])); 
                $mobilephone = $current_user->get('mobilephone');
                send($modx->getOption('ms2_sms_gate'), 80, $modx->getOption('ms2_sms_login'), $modx->getOption('ms2_sms_password'), $mobilephone, $user['message']);
                $id = $user['id'];
                $sql = "UPDATE modx_messages_base SET status = 1 WHERE modx_messages_base.id = '$id'";
                $modx->query($sql);
            }
        }
        return(true);
    break;
    case "cron":
        $segment_id = $_GET['segment_id'];
        $message_id = $_GET['message_id'];
        $minutes = $_GET['minutes'];
        $hours = $_GET['hours'];
        $days = $_GET['days'];
        $weekdays = $_GET['weekdays'];
        $user_id = $_GET['user_id'];
        $sql = "
        INSERT INTO 
            modx_messages_cron
            (
                `segment_id`,
                `message_id`,
                `mins`,
                `hours`,
                `days`,
                `weekdays`,
                `user_id`
            )
            VALUES
            (
                '$segment_id',
                '$message_id',
                '$minutes',
                '$hours',
                '$days',
                '$weekdays',
                '$user_id'
            )
        ";
        $modx->query($sql);
    break;
}