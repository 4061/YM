id: 208
name: admin_getRatingObjects
description: 'Массив для получения уникальных значений'
category: 'Шаблоны Админки'
properties: null

-----

$sql = "SELECT ";
$sql .= "id_rating,id_resource,id_user,rating_value,comment_rating,date_of_creation,date_of_editing,active,DATE_FORMAT(date_of_creation, '%d.%m.%Y') as new_date_creation,";
$sql .= "modx_user_attributes.internalKey,modx_user_attributes.fullname,modx_user_attributes.email,modx_user_attributes.mobilephone,";
$sql .= "modx_site_content.pagetitle";
$sql .= " FROM modx_rating, modx_user_attributes, modx_site_content WHERE modx_user_attributes.internalKey = modx_rating.id_user AND modx_site_content.id = modx_rating.id_resource ORDER BY modx_site_content.pagetitle";

$resources = $modx->query($sql);
$resources = $resources->fetchAll(PDO::FETCH_ASSOC);

$massList = $massListName = array();

foreach ($resources as $resource) {
    //echo $resource['pagetitle']."<br>";
    if(!in_array($resource['pagetitle'],$massList)) {
        array_push($massList,$resource['pagetitle']);
        $massListName[] = array("id" => $resource['id_resource'], "pagetitle" => $resource['pagetitle']);
    }
}

return $massListName;