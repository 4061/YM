id: 132
source: 1
name: sendPushToSegment
properties: 'a:0:{}'

-----

$url = 'https://fcm.googleapis.com/fcm/send';
                $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
                $request_headers = [
                    'Content-Type: application/json',
                    'Authorization: key=' . $YOUR_API_KEY,
                    'apns-push-type: alert'
                ];
if ($_GET["action"] == "all"){
    $users = $modx->getCollection("modUserProfile", array("city"));
    $tokens = [];
    foreach ($users as $user){
        if ($user->get("city") && $user->get("website")){
            $tokens[] = $user->get("city");
        }
    }
    $tokens = array_chunk($tokens, 500);
    foreach ($tokens as $token){
                    $request_body1['registration_ids'] = $token; 
                    //$request_body1['registration_ids'] = ["cPtGlMUNTaWDFwVA6JcETD:APA91bGdLNgdgNG-EEwUhWdPo7ghsxp5TnEzN_FNc4eTVYA5tp2IRaiM4jQMb4oUHUAkKiZQk5ClI-gE-wc4UgrIDOnRRFZb7nO0aUvAs651QbDGxvWGT_XW1hi18FQTB4mJ27pn7H5z"];
                    $request_body1['notification'] = array("title" => $_GET['title'], "body" => $_GET['body']);
                    $request_body1['data'] = array(
                        "type" => "alert", 
                        "data" => array(
                            "title" => "Banners", 
                            "body" => "test",         
                            "type" => "product", 
                            "product_id" => 3238),
                        "mandatory" => true
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    echo $response;
    }
} else {
    $title = $_GET['title'];
    $description = $_GET['desc'];
    $pic = $_GET['pic'];
    $link = $_GET['link'];
    $users = $_GET['users'];
    foreach ($users as $user){
    $profile = $modx->getObject('modUserProfile', ['internalKey' => $user]);
    
        if ($profile->get("website")){
    
            $YOUR_TOKEN_ID = $profile->get('city');
            $request_body1['registration_ids'] = [$YOUR_TOKEN_ID]; 
            $request_body1['notification'] = array("title" => $title, "body" => $description);
            $request_body1['data'] = array(
                "type" => "alert", 
                "data" => array(
                    "title" => "Banners", 
                    "body" => "test",         
                    "type" => "product", 
                    "product_id" => 3238),
                "mandatory" => true
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($ch);
            curl_close($ch);
        }
    }
    return $response;
    
}