id: 135
source: 1
name: cart-remove-product
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $product_key = $_POST['JSON']['key']; //key продукта
        $ms2 = $modx->getService('miniShop2');
        $ms2->initialize($modx->context->key);
        $cart = $ms2->cart->get();
        $cart = $ms2->cart->remove($product_key);
        $total_cost = 0;
        $total_count = 0;
        foreach ($_SESSION['minishop2']['cart'] as $cart_item){
            $total_cost_product = $cart_item['total_cost']*$cart_item['count'];
            $total_cost = $total_cost + $total_cost_product;
            $total_count = $total_count + $cart_item['count'];
        }
        $_SESSION['minishop2']['total_cost'] = $total_cost;
        $_SESSION['minishop2']['total_count'] = $total_count;
        $json['promocodes'] = $_SESSION['minishop2']['promocodes'];
        $json['total_cost'] = $_SESSION['minishop2']['total_cost'];
        $json['total_count'] = $_SESSION['minishop2']['total_count'];
        $json['products'][] = $_SESSION['minishop2']['cart'];
        if ($_SESSION['minishop2']['promocodes']){
            $modx->runSnippet('check-code');
        } 
        $json['discount_cost'] = $_SESSION['minishop2']['discount_cost']; //цена со скидкой
        $json['actual_discount'] = $_SESSION['minishop2']['actual_discount']; //цена со скидкой
        return json_encode($json);
    }
}