id: 148
source: 1
name: getProducts
properties: 'a:0:{}'

-----

if (
    'application/json' == $_SERVER['CONTENT_TYPE']
    && 'POST' == $_SERVER['REQUEST_METHOD']
) {

    $_REQUEST['JSON'] = json_decode(
        file_get_contents('php://input'),
        true
    );
    if ($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) {
    $token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
    $token = substr($token, 7);
    }
    
    $_POST['JSON'] = &$_REQUEST['JSON'];
    $rest_id = $_POST['JSON']['rest_id'];
    $category_id = $_POST["JSON"]["category_id"];
    $search_query = $_POST["JSON"]["search_string"];

    $_SESSION['minishop2']['order_type'] = $_POST['JSON']['order_type'];
    if ($_POST['JSON']['order_type'] == 1){
        unset($_SESSION["min_delivery_cost"]);
    }
    if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
        $modx->log(1,json_encode($profile));
        $userid = $profile->get('id');
        $modx->log(xPDO::LOG_LEVEL_ERROR, 'id юзера: ' . $userid);
        $sql = "SELECT *
                FROM modx_msbonus2_users, modx_user_attributes, modx_msbonus2_levels
                WHERE modx_user_attributes.id = '$userid' and modx_msbonus2_users.user = modx_user_attributes.internalkey and modx_msbonus2_users.level = modx_msbonus2_levels.id";
        $info_user = $modx->query($sql);
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        if ($info_user) {

            $bonus = $info_user[0]['points'];
            $bonus_persent = $info_user[0]['bonus'];
            $paid_money = $info_user[0]['paid_money'];
            $level = $info_user[0]['level'];
            $level_name = $info_user[0]['name'];
            $modx->log(xPDO::LOG_LEVEL_ERROR, 'количество бонусов: ' . $bonus);
         

            $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = $level";
            if ($modx->query($sql2)) {
                $next_level = $modx->query($sql2);
                $next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
                $bonus_persent = $next_level[0]['bonus'];
            }
            $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
        } else {

            $sql = "SELECT * FROM `modx_msbonus2_levels` WHERE `cost` = 0";
            if ($modx->query($sql)) {
                $bonus_persent = $modx->query($sql);
                $bonus_persent = $bonus_persent->fetchAll(PDO::FETCH_ASSOC);
                $bonus_persent = $bonus_persent[0]['bonus'];
                $bonus = $bonus_persent[0]['name'];
            }
            $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
            $level = '1';
            $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = '2'";
            if ($modx->query($sql2)) {
                $next_level = $modx->query($sql2);
                $next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
                $level_name = $next_level[0]['name'];
                $next_level = '{"id":"' . $next_level[0]['id'] . '","bonus":"' . $next_level[0]['bonus'] . '","cost":"' . $next_level[0]['cost'] . '","name":"' . $next_level[0]['name'] . '"}';
            }
            $paid_money = '0';
        }
    }
} else {
    $sql = "SELECT * FROM `modx_msbonus2_levels` WHERE `cost` = 0";
    if ($modx->query($sql)) {
        $bonus_cost = $modx->query($sql);
        $bonus_cost = $bonus_cost->fetchAll(PDO::FETCH_ASSOC);
        $bonus_persent = $bonus_cost[0]['bonus'];
    }
}
$link = 1;
$innerJoin = array();
if (!empty($link) && !empty($master)) {
    $innerJoin['Link'] = array(
        'class' => 'msProductLink',
        'on' => 'msProduct.id = Link.slave AND Link.link = ' . $link,
    );
    $where['Link.master'] = $master;
} elseif (!empty($link) && !empty($slave)) {
    $innerJoin['Link'] = array(
        'class' => 'msProductLink',
        'on' => 'msProduct.id = Link.master AND Link.link = ' . $link,
    );
    $where['Link.slave'] = $slave;
}
if (!$category_id){
    $category_id = 1561;
}

if ($search_query){
    $search_results = $modx->runSnippet("apiSearch", ["parents" => 1561, "query" => $search_query, "returnIds" => true, "limit" => 10000]);
    $search_results = explode(",",$search_results);
}
$pdoFetch = new pdoFetch($modx);
/*$default = array(
    'class' => 'msProduct',
    'parents' => $category_id,
    'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
    'leftJoin' => ['Data' => ['class' => 'msProductData'],
        'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
        'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
        'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
        'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
        'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
        'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
        'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "times_bought"'],
    ],
    'select' => [
        'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle`, parent, `msProduct`.`description`',
        'Data' => '`Data`.`restourants`,`Data`.`order_types`,`Data`.`price`,`Data`.`price2`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
        'Option1' => 'Option1.value as energy_size',
        'Option2' => 'Option2.value as energy_allergens',
        'Option3' => 'Option3.value as energy_value',
        'Option4' => 'Option4.value as energy_carbohydrates',
        'Option5' => 'Option5.value as energy_protein',
        'Option6' => 'Option6.value as energy_oils',
        'Option7' => 'Option7.value as times_bought',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => 1000
);*/
//if ($search_results){
//    $default["where"]["id:IN"] = $search_results;
//}
//$pdoFetch->setConfig($default);
//$rows = $pdoFetch->run();

if($modx->getOption("app_pricy_type") == "userprice"){
    if ($profile){
        $price_type = $profile->get("state");
    } else {
        $price_type = $modx->getOption("add_default_type_price");;
    }
} else {
    $sql = 'SELECT value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 37 AND contentid = '.$rest_id;
    $output = $modx->query($sql);
    $city = $output->fetch(PDO::FETCH_ASSOC)['value'];
    $price_type = ($city)? $city : 1;
}
function mergeRating($rows){
    global $modx;
    $pdoFetch = new pdoFetch($modx);
    $master_ids = array_column($rows, 'id');
    $ratings = array(
        'class' => 'Rating',
        'where' => ["id_resource:IN" => $master_ids, "active" => 1],
        'leftJoin' => [
            'User' => ['class' => 'modUserProfile', 'on' => 'User.internalKey = Rating.id_user'],
        ],
        'select' => [
           'Rating' => '`Rating`.`id_rating`,
               `Rating`.`id_resource`,
               `Rating`.`id_user`,
               `Rating`.`comment_rating`,
               `Rating`.`rating_value`,
               `Rating`.`date_of_creation`,
               `Rating`.`date_of_editing`',
            'User' => '`User`.`fullname`',
        ],
        'sortdir' => "ASC",
        'sortby' => "id_rating",
        'return' => 'data',
        'limit' => 1000
    );
    $pdoFetch->setConfig($ratings);
    $ratings = $pdoFetch->run();
    $rows_array = array_combine(array_column($rows, 'id'), $rows);
    foreach ($ratings as $rating){
        $rows_array[$rating["id_resource"]]["ratings"][] = $rating;
    }
    return $rows_array;
}
$default = array(
    'class' => 'msProduct',
    'parents' => $category_id,
    'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
    'leftJoin' => [
      'Data' => ['class' => 'msProductData'],
      'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type],
      'Option19' => ['class' => 'msProductOption', 'on' => 'Option19.product_id = msProduct.id AND Option19.key = "card_type"'],
      'allRating' => ['class' => 'msProductOption', 'on' => 'allRating.product_id = msProduct.id AND allRating.key = "item_rating"'],
    ],
    'select' => [
        'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`, `msProduct`.`content`',
        'Price' => 'Price.price, Price.old_price',
        'Data' => '`Data`.`restourants`,`Data`.`order_types`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
        'Option19' => 'Option19.value as card_type',
        'allRating' => 'allRating.value as item_rating',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => 1000,
    'offset' => ($page-1)*$limit
);


/* ================== Подгружаем дополнительные параметры ==================== */
// подгружаем системный параметр energy, если он не нулевой, то подтягиваем значения связанные с ним
if($modx->getOption("app_parameters_energy")) {
    $massEnergyLJ = array(
        'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
        'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
        'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
        'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
        'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
        'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
        'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "times_bought"'],
    );
    $massEnergySc = array(
        'Option1' => 'Option1.value as energy_size',
        'Option2' => 'Option2.value as energy_allergens',
        'Option3' => 'Option3.value as energy_value',
        'Option4' => 'Option4.value as energy_carbohydrates',
        'Option5' => 'Option5.value as energy_protein',
        'Option6' => 'Option6.value as energy_oils',
        'Option7' => 'Option7.value as times_bought',
    );
    $default['leftJoin'] = array_merge($default['leftJoin'], $massEnergyLJ);
    $default['select'] = array_merge($default['select'], $massEnergySc);
}

// проверяем настройку на вывод списка ссылок для товара (они должны быть добавлены к товару заранее)
if ($modx->getOption("app_links_list")) {
    $massEnergyLJ = array(
        'Option8' => ['class' => 'msProductOption', 'on' => 'Option8.product_id = msProduct.id AND Option8.key = "link_audio"'],
        'Option9' => ['class' => 'msProductOption', 'on' => 'Option9.product_id = msProduct.id AND Option9.key = "link_video"'],
        'Option10' => ['class' => 'msProductOption', 'on' => 'Option10.product_id = msProduct.id AND Option10.key = "link_phone"'],
        'Option11' => ['class' => 'msProductOption', 'on' => 'Option11.product_id = msProduct.id AND Option11.key = "link_coordinates"'],
        'Option12' => ['class' => 'msProductOption', 'on' => 'Option12.product_id = msProduct.id AND Option12.key = "link_site"'],
        'Option13' => ['class' => 'msProductOption', 'on' => 'Option13.product_id = msProduct.id AND Option13.key = "link_vk"'],
        'Option14' => ['class' => 'msProductOption', 'on' => 'Option14.product_id = msProduct.id AND Option14.key = "link_fb"'],
        'Option15' => ['class' => 'msProductOption', 'on' => 'Option15.product_id = msProduct.id AND Option15.key = "link_instagram"'],
        'Option16' => ['class' => 'msProductOption', 'on' => 'Option16.product_id = msProduct.id AND Option16.key = "link_odnoklassniki"'],
        'Option17' => ['class' => 'msProductOption', 'on' => 'Option17.product_id = msProduct.id AND Option17.key = "add_address"'],
        'Option18' => ['class' => 'msProductOption', 'on' => 'Option18.product_id = msProduct.id AND Option18.key = "add_time_work"'],
    );
    $massEnergySc = array(
        'Option8' => 'Option8.value as link_audio',
        'Option9' => 'Option9.value as link_video',
        'Option10' => 'Option10.value as link_phone',
        'Option11' => 'Option11.value as link_coordinates',
        'Option12' => 'Option12.value as link_site',
        'Option13' => 'Option13.value as link_vk',
        'Option14' => 'Option14.value as link_fb',
        'Option15' => 'Option15.value as link_instagram',
        'Option16' => 'Option16.value as link_odnoklassniki',
        'Option17' => 'Option17.value as add_address',
        'Option18' => 'Option18.value as add_time_work',
    );
    $default['leftJoin'] = array_merge($default['leftJoin'], $massEnergyLJ);
    $default['select'] = array_merge($default['select'], $massEnergySc);
}

if ($search_results){
    $default["where"]["id:IN"] = $search_results;
}
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();
//return json_encode($rows);

$rows = mergeRating($rows);

//Собираем все id товаров в отдельный массив
$master_ids = array_column($rows, 'id');

// формируем список стикеров для ресурса
$stickers = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link:IN' => array(11,12), 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
        'ColorText' => ['class' => 'msProductOption', 'on' => 'ColorText.product_id = Product.id AND ColorText.key = "add_color_text"'],
        'ColorBg' => ['class' => 'msProductOption', 'on' => 'ColorBg.product_id = Product.id AND ColorBg.key = "add_color_bg"'],
        'idOrg' => ['class' => 'msProductOption', 'on' => 'idOrg.product_id = Product.id AND idOrg.key = "add_id_org"'],
    ],
    'select' => [
        'msProductLink' => 'master',
        'Product' => '`Product`.`id`,pagetitle as title',
        'ColorText' => 'ColorText.value as color_text',
        'ColorBg' => 'ColorBg.value as color_bg',
        'idOrg' => 'idOrg.value as id_org',
    ],
    'sortby' => 'Product.id',
    'sortdir' => 'ASC',
    'return' => 'data',
    'limit' => 1000
);
$pdoFetch->setConfig($stickers);
$stickers = $pdoFetch->run();
$sticerList = array();

foreach ($stickers as $sticer){
    if((int)$rest_id == (int)$sticer['id_org']) {
        $sticerList[] = array(
            "id" => $sticer['id'],
            "tagType" => "sticker",
            "title" => $sticer['title'],
            "color_text" => $sticer['color_text'],
            "color_bg" => $sticer['color_bg'],
        );
    } else {
        $sticerList[] = array(
            "id" => $sticer['id'],
            "tagType" => "tag",
            "title" => $sticer['title'],
            "color_text" => $sticer['color_text'],
            "color_bg" => $sticer['color_bg'],
        );
    }
};

// подгружаем системный параметр Добавки, если он не нулевой, то подтягиваем значения связанные с ним
if($modx->getOption("app_parameters_addpar")) {
    $adds = array(
        'class' => 'msProductLink',
        'where' => ['Product.class_key' => 'msProduct', 'link' => 3, 'master:IN' => $master_ids],
        'leftJoin' => [
            'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
        'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
    ],
    'select' => [
        'msProductLink' => 'master',
        'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
        'Data' => '`Data`.`price`',
    ],
    'sortby' => 'Product.id',
    'sortdir' => 'ASC',
    'return' => 'data',
    'limit' => 1000
    );
    $pdoFetch->setConfig($adds);
    $dobavki = $pdoFetch->run();
} else {
    $dobavki = array();
}

// подгружаем системный параметр Types, если он не нулевой, то подтягиваем значения связанные с ним
if($modx->getOption("app_parameters_type")) {
    $types = array(
        'class' => 'msProductLink',
        'where' => ['Product.class_key' => 'msProduct', 'link' => 9, 'master:IN' => $master_ids],
        'leftJoin' => [
            'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
        'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
    ],
    'select' => [
        'msProductLink' => 'master',
        'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
        'Data' => '`Data`.`price`',
    ],
    'sortby' => 'Product.id',
    'sortdir' => 'ASC',
    'return' => 'data',
    'limit' => 1000
    );
    $pdoFetch->setConfig($types);
    $types = $pdoFetch->run();
} else {
    $types = array();
}

// подгружаем системный параметр Ингредиенты, если он не нулевой, то подтягиваем значения связанные с ним
if($modx->getOption("app_parameters_ingredients")) {
    $ingredients = array(
        'class' => 'msProductLink',
        'where' => ['Product.class_key' => 'msProduct', 'link' => 8, 'master:IN' => $master_ids],
        'leftJoin' => [
            'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
        'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
    ],
    'select' => [
        'msProductLink' => 'master',
        'Product' => '`Product`.`id`, `Data`.`image` as img_url, `Product`.`pagetitle` as title',
        'Data' => '`Data`.`price`',
    ],
    'sortby' => 'Product.id',
    'sortdir' => 'ASC',
    'return' => 'data',
    'limit' => 1000
    );
    $pdoFetch->setConfig($ingredients);
    $ingredients = $pdoFetch->run();
} else {
    $ingredients = array();
}
// подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
if($modx->getOption("app_parameters_analogue")) {
    $analogs = array(
        'class' => 'msProductLink',
        'where' => ['Product.class_key' => 'msProduct', 'link' => 10, 'master:IN' => $master_ids],
        'leftJoin' => [
            'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
        'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
    ],
    'select' => [
        'msProductLink' => 'master',
        'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
        'Data' => '`Data`.`price`',
    ],
    'sortby' => 'Product.id',
    'sortdir' => 'ASC',
    'return' => 'data',
    'limit' => 1000
    );
    $pdoFetch->setConfig($analogs);
    $analogs = $pdoFetch->run();
} else {
    $analogs = array();
}

/* ================== /Подгружаем дополнительные параметры ==================== */

$json['data'] = array();
$rows = array_combine(array_column($rows, 'id'), $rows);
foreach ($analogs as $analog){
    $rows[$analog['master']]['analog'][] = $analog;
}
foreach ($ingredients as $ingredient){
    $rows[$ingredient['master']]['ing'][] = $ingredient;
}
foreach ($dobavki as $dobavka){
    $rows[$dobavka['master']]['adds'][] = $dobavka;
}
foreach ($types as $type){
    $rows[$type['master']]['types'][] = $type;
}
//return json_encode($rest_id);
foreach ($rows as $key => $row){
    $order_types = explode(",", $row['order_types']);
    if (in_array($_POST['JSON']['order_type'],$order_types)){
        $restourants = explode(",", $row['restourants']);
        if (in_array($rest_id, $restourants)) {
            
        } else {
            $json['data'][$key]['id'] = $row['id'];
            $json['data'][$key]['title'] = $row['title'];
            $json['data'][$key]['parent'] = $row['parent'];
            $json['data'][$key]['article'] = (int)$row['article'];
            $json['data'][$key]['allRating'] = ($row['item_rating'])? $row['item_rating'] : 0;
            $json['data'][$key]['rating'] = ($row['item_rating'])? $row['ratings'] : array();
            /*if ($_POST['JSON']['order_type'] == 1){
                $json['data'][$key]['price'] = (int)$row['price2'];
            } else {
                $json['data'][$key]['price'] = (int)$row['price'];    
            }*/
            $json['data'][$key]['price'] = (int)$row['price'];
            $json['data'][$key]['old_price'] = (int)$row['old_price'];
            $json['data'][$key]['desc'] = $row['description'];
            $json['data'][$key]['content'] = preg_replace('~<a\b[^>]*+>|</a\b[^>]*+>~', '', $row['content']);;
            $json['data'][$key]['new'] = (int)$row['new'];
            $json['data'][$key]['popular'] = (int)$row['popular'];
            $json['data'][$key]['favorite'] = (int)$row['favorite'];
            $json['data'][$key]['img_big'] = $image_small = $modx->runSnippet('pthumb', [
                'input' => substr($row['img_big'], 1),
                'options' => '&h=240&w=400&q=100&zc=1',
            ]);
            $json['data'][$key]['img_small'] = $row['img_small'];
            $json['data'][$key]['bonus_add'] = floor($row['price'] / 100 * $bonus_persent);
            $json['data'][$key]['times_bought'] = (int)$row['times_bought'];
            $json['data'][$key]['remains_count'] = 25;
            $json['data'][$key]['remains_text'] = "dfuvidsofov";
            $json['data'][$key]['barcode'] = "111111";
            $json['data'][$key]['pieces_per_package'] = 25;
            
            $row['card_type'] = ($row['card_type'])? $row['card_type'] : "base";
            $json['data'][$key]['card_type'] = $row['card_type'];
            /*$en_size = explode(",",$row['energy_size']);
            $l = $en_size[1]*100;
            $l = $en_size[1]*1;
            $l = $l/10;
            $k = $en_size[0];
            $k = $k*1000;
            $fin = $fin+$l;*/
            
            // Если параметр Energy равен 0, то передаём дальше "заглушку" под его значения
            if (!$modx->getOption("app_parameters_energy")) {
                $row['energy_size'] = "0";
                $row['energy_value'] = $row['energy_carbohydrates'] = $row['energy_protein'] = $row['energy_oils'] = 0;
                $row['energy_allergens'] = "-";
            }
            $json['data'][$key]['energy'] = array(
                "energy_size" => $row['energy_size'],
                "energy_value" => round($row['energy_value']),    
                "energy_allergens" => $row['energy_allergens'],
                "energy_carbohydrates" => round($row['energy_carbohydrates']),
                "energy_protein" => round($row['energy_protein']),
                "energy_oils" => round($row['energy_oils']),
            );
            // проверяем настройку на вывод списка ссылок для товара (они должны быть добавлены к товару заранее)
            if ($modx->getOption("app_links_list")) {
                
                $coordMass = explode(",",str_replace(" ","",$row['link_coordinates']));
                $json['data'][$key]["GPS"] = array(
                    'latitude' => (float)$coordMass[0],
                    'longitude' => (float)$coordMass[1],
                );
                
                // Добавляем в ответный массив перечень ссылок от объекта
                $json['data'][$key]['links_list'] = array(
                    "link_phone" => $row['link_phone'],
                    "link_site" => $row['link_site'],
                    "link_vk" => $row['link_vk'],
                    "link_fb" => $row['link_fb'],
                    "link_instagram" => $row['link_instagram'],
                    "link_odnoklassniki" => $row['link_odnoklassniki'],
                );
                
                // получаем картинку превью c ютуба
                $linkCode = explode("/",$row['link_video']);
                $linkCode = $linkCode[count($linkCode) - 1];
                if(stristr($linkCode, '?v=') === FALSE) {
                } else {
                    $linkCode = explode("?",explode("v=",$linkCode)[1])[0];
                }
                
                $json['data'][$key]['media'] = array(
                    "link_audio" => $row['link_audio'],
                    "link_video" => $row['link_video'],
                    "link_video_prev" => "https://img.youtube.com/vi/{$linkCode}/hqdefault.jpg",
                );
                $json['data'][$key]['where_is'] = array(
                    "add_address" => $row['add_address'],
                    "add_time_work" => $row['add_time_work'],
                );
            }
            
            if($sticerList) {
                $json['data'][$key]['stickers'] = $sticerList;
            }
            
            for ($i=0; $i<count($row['adds']); $i++){
                $row['adds'][$i]['price'] = (int)$row['adds'][$i]['price'];
                //$row['types'][$i]['price'] = (int)$row['types'][$i]['price'];
            }
            for ($i=0; $i<count($row['types']); $i++){
                //$row['adds'][$i]['price'] = (int)$row['adds'][$i]['price'];
                $row['types'][$i]['price'] = (int)$row['types'][$i]['price'];
            }
            if ($row['adds']){
                $json['data'][$key]['additives'] = $row['adds'];
            } else {
                $json['data'][$key]['additives'] = [];
            }
            if ($row['ing']){
                $json['data'][$key]['ingredients'] = $row['ing'];    
            } else {
                $json['data'][$key]['ingredients'] = [];
            }
            if ($row['types']){
                $json['data'][$key]['types'] = $row['types'];
            } else {
                $json['data'][$key]['types'] = [];
            }
            if ($row['analog']){
                $json['data'][$key]['analogs'] = $row['analog'];
            } else {
                $json['data'][$key]['analogs'] = [];
            }
        }
    }   
}
/*$str = implode(",",$master_ids);
$sql = "SELECT value, contentid FROM modx_site_tmplvar_contentvalues WHERE contentid in ($str) AND tmplvarid=11";
$info_user = $modx->query($sql);
$info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
$vals1 = array_column($info_user, 'value');
$vals2 = array_column($info_user, 'contentid');
$vals = array_combine($vals2, $vals1);
foreach ($vals as $key => $val){
    $product_rating = json_decode($val, true);
    $middle_rating = (floatval($product_rating[0]['sum'])+(floatval($product_rating[0]['five'])*5))/(floatval($product_rating[0]['count'])+(floatval($product_rating[0]['five'])));
    if ($product_rating[0]['count'] == 0){
        $middle_rating = 5;
    }
    if ($json['data'][$key]){
        $json['data'][$key]['rating'] = (double)$middle_rating;
        $json['data'][$key]['rating'] = round($json['data'][$key]['rating']);
    }
}
foreach ($json['data'] as $key => $js){
    if (!$js['rating']){
        if ($json['data'][$key]){
            $json['data'][$key]['rating'] = 5;
        }
    }
}*/

// Получаем название категории откуда беруться товары
$sql = "SELECT pagetitle FROM modx_site_content WHERE id = ".$category_id;
$result = $modx->query($sql);
$json1["category_title"] = $result->fetch(PDO::FETCH_ASSOC)['pagetitle'];

$json1["count_items"] = count($rows);
$json1['data'] = array_values($json['data']);
$res = $modx->getObject('modResource', (int)$rest_id);
if ($_POST['JSON']['order_type'] == 1){
    $json1['successMsgTitle'] = "Доставим за ".$res->getTVValue('waitingtimeself'); 
    $json1['successMsgSubtitle'] = $res->get("pagetitle").". Примерное время доставки - около ".$res->getTVValue('waitingtimeself');
}
return json_encode($json1);