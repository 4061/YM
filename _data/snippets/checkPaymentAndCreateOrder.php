id: 224
source: 1
name: checkPaymentAndCreateOrder
description: 'Сниппет выполняет проверку оплаты и если оплата проведена успешно, то создает заказ.'
properties: 'a:0:{}'

-----

$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$url = str_replace('http://','',$url);
$trans_id = "IOQMfydacykCQqa8I67cPKxC51U=";
$org_id = 1804;
if ($profile = $modx->getObject('modUserProfile', ['last_trans_id' => $trans_id])){
    $rest = $modx->getObject('modResource',$org_id);
    $merchant = $rest->getTVValue('merchantID');
    $params = array(
        'command' => 'c',
        'trans_id' => $profile->last_trans_id,
        'client_ip_addr'=>"5.101.157.102");
    $request = http_build_query($params);
    $curlOptions = array(
        CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
        CURLOPT_CONNECTTIMEOUT => 5,
        CURLOPT_TIMEOUT => 35,
        CURLOPT_HEADER => false,
        CURLOPT_POST => true,
        CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
        CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
        CURLOPT_SSLVERSION => 6,
        CURLOPT_POSTFIELDS => $request,
    );
        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);
        $response = curl_exec($ch);
        //echo $response;
        $OKRESPONSE = explode(" ", $response);
        $result = [];
        foreach ($OKRESPONSE as $key => $respons){
          $result[] = explode(":", $respons);
        }
        $finalRes = [];
        foreach ($result as $res){
            $finalRes[] = $res[0];
        }
        $statusCODE = $finalRes[1];
        $statusCODE = explode("\n", $statusCODE);
        if ($statusCODE[0] == "OK"){ // если оплата прошла успешно
            return 1; //здесь логика создания заказа
        } else { //если только создан
            //print_r($statusCODE).'</br>';
            if ($response == "RESULT: CREATED"){
            } else { //в противном случае - фейл
                $response = explode(" ",$response);
                $response = explode("\n",$response[1]);
                if ($response[0] == "TIMEOUT"){

                }
                if ($response[0] == "FAILED"){

                }
            } 
        }
}