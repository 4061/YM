id: 228
name: admin_tagObjects
description: 'Получение списка тэгов для админ.панели'
category: 'Шаблоны Админки'
properties: null

-----

$sql = "SELECT
            id,pagetitle as name
        FROM 
            modx_site_content
        WHERE
            parent = 3505";

$resources = $modx->query($sql);
$resources = $resources->fetchAll(PDO::FETCH_ASSOC);

$massList = $massListName = array();

$getParam = $_GET['tag_type'];
$selectList = explode(",",$getParam);

foreach ($resources as $resource) {
    $select = (in_array($resource['id'], $selectList))? "selected" : "";
    $massListName[] = array("id" => $resource['id'], "title" => $resource['name'], "selected" => $select);
}

return $massListName;