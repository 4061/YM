id: 192
source: 1
name: admin_changeIntegrations
snippet: "$_REQUEST['JSON'] = json_decode(\n             file_get_contents('php://input'), true\n    );\n    \n    if ($_REQUEST['JSON'][\"true\"]){\n        $USA = $modx->getObject('modSystemSetting', \"IIKO\");\n        $USA->set('value', true);\n        $USA->save();\n    } else {\n        $USA = $modx->getObject('modSystemSetting', \"IIKO\");\n        $USA->set('value', false);\n        $USA->save();\n    }\n    $modx->cacheManager->refresh(array('system_settings' => array()));\n    return json_encode($arr);"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_changeIntegrations.php
content: "$_REQUEST['JSON'] = json_decode(\n             file_get_contents('php://input'), true\n    );\n    \n    if ($_REQUEST['JSON'][\"true\"]){\n        $USA = $modx->getObject('modSystemSetting', \"IIKO\");\n        $USA->set('value', true);\n        $USA->save();\n    } else {\n        $USA = $modx->getObject('modSystemSetting', \"IIKO\");\n        $USA->set('value', false);\n        $USA->save();\n    }\n    $modx->cacheManager->refresh(array('system_settings' => array()));\n    return json_encode($arr);"

-----


$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    
    if ($_REQUEST['JSON']["true"]){
        $USA = $modx->getObject('modSystemSetting', "IIKO");
        $USA->set('value', true);
        $USA->save();
    } else {
        $USA = $modx->getObject('modSystemSetting', "IIKO");
        $USA->set('value', false);
        $USA->save();
    }
    $modx->cacheManager->refresh(array('system_settings' => array()));
    return json_encode($arr);