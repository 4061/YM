id: 220
source: 1
name: sendPushToAllUsers
description: 'Массовая рассылка пуш уведомлений'
properties: 'a:0:{}'

-----

$_REQUEST['JSON'] = json_decode(
         file_get_contents('php://input'), true
);
$token = getallheaders();

if ($token['Authorization']) $token2 = $token['Authorization'];
if ($token['authorization']) $token2 = $token['authorization'];
$token = substr($token2, 7);
$_POST['JSON'] = & $_REQUEST['JSON'];
$url = 'https://fcm.googleapis.com/fcm/send';
$YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
$request_headers = [
    'Content-Type: application/json',
    'Authorization: key=' . $YOUR_API_KEY,
    'apns-push-type: alert'
];
$users = $modx->getCollection("modUserProfile", array("city"));
$tokens = [];
foreach ($users as $user){
    if ($user->get("city")){
        $tokens[] = $user->get("city");
    }
}
$tokens = array_chunk($tokens, 500);
foreach ($tokens as $token){
                $request_body1['registration_ids'] = $token; 
                //$request_body1['registration_ids'] = ["cPtGlMUNTaWDFwVA6JcETD:APA91bGdLNgdgNG-EEwUhWdPo7ghsxp5TnEzN_FNc4eTVYA5tp2IRaiM4jQMb4oUHUAkKiZQk5ClI-gE-wc4UgrIDOnRRFZb7nO0aUvAs651QbDGxvWGT_XW1hi18FQTB4mJ27pn7H5z"];
                $request_body1['notification'] = array("title" => $_POST['title'], "body" => $_POST['body']);
                $request_body1['data'] = array(
                    "type" => "alert", 
                    "data" => array(
                        "title" => "Banners", 
                        "body" => "test",         
                        "type" => "product", 
                        "product_id" => 3238),
                    "mandatory" => true
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $response = curl_exec($ch);
                curl_close($ch);
                echo $response;
}