id: 243
source: 1
name: homepage-FH
category: flowerheaven
properties: 'a:0:{}'

-----

$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $profile = $modx->getObject('modUserProfile', ['website' => $token]);
    if($profile){
        $price_type = $profile->get("state");
    }
if (!$price_type){
    $price_type = 1;
}
$org = $modx->getObject("modResource", $_REQUEST['JSON']["rest_id"]);
$price_type = $org->getTVValue("deliveryTerminalId");
$modx->addPackage('softjetsync', MODX_CORE_PATH . 'components/softjetsync/model/');
$sql = "
SELECT 
    id_promo as id,
    name as title,
    description as content,
    date_end,
    id_product as product_id,
    code as promocode,
    type as type,
    child_discount as discount,
    image as img_big,
    url as url, orgs
FROM 
    modx_promo_base 
WHERE
    type = 1 and activity = 1 OR
    type = 2 and activity = 1
";
$promocodes = $modx->query($sql);
$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);
foreach ($promocodes as $key => $promocode){
    $orgs = explode(',',$promocode["orgs"]);
    if (!in_array($_REQUEST['JSON']["rest_id"], $orgs)){
        unset($promocodes[$key]);
    }
}
foreach ($promocodes as $key => $promocode){
    $image_small = $modx->runSnippet('pthumb', [
    'input' => substr($promocode["img_big"], 1),
    'options' => '&h=120&q=100',]);
    $promocodes[$key]['img_small'] = $image_small ;
    $promocodes[$key]['date_end'] = strtotime($promocode['date_end']);
}
$json["promotions"] = $promocodes;
//////
$page = $_REQUEST['JSON']["page"];
//$page = 1;
$limit = 12;
$pdoFetch = new pdoFetch($modx);
$default = array(
    'class' => 'msProduct',
    'parents' => $category_id,
    'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0, 'Price.price:IS NOT' => null],
    'leftJoin' => [
      'Data' => ['class' => 'msProductData'],
      'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type = '.$price_type],
      'Remains' => ['class' => 'SoftjetsyncRemains', 'on' => 'Remains.product_id = msProduct.id'],
    ],
    'select' => [
        'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`',
        'Price' => 'Price.price as price',
        'Remains' => 'remnant, coming',
        'Data' => '`Data`.`length` as weight,`Data`.`restourants`,`Data`.`quantityinpack`,`Data`.`order_types`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => $limit,
    'offset' => ($page-1)*$limit
);
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();
$SoftjetSync = $modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/');
$SoftjetSync->shuffle($rows, 'SoftjetsyncBarcodes', 'barcodes', 'id', 'product_id');
$master_ids = array_column($rows, 'id');
$adds = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 3, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($adds);
$dobavki = $pdoFetch->run();

$types = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 9, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($types);
$types = $pdoFetch->run();

$ingredients = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 8, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`, `Data`.`image` as img_url, `Product`.`pagetitle` as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($ingredients);
$ingredients = $pdoFetch->run();

$analogs = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 10, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($analogs);
$analogs = $pdoFetch->run();
$rows = array_combine(array_column($rows, 'id'), $rows);
foreach ($analogs as $analog){
    $rows[$analog['master']]['analog'][] = $analog;
}
foreach ($ingredients as $ingredient){
    $rows[$ingredient['master']]['ing'][] = $ingredient;
}
foreach ($dobavki as $dobavka){
    $rows[$dobavka['master']]['adds'][] = $dobavka;
}
foreach ($types as $type){
    $rows[$type['master']]['types'][] = $type;
}
foreach ($rows as $key => $row){
            $products_array["data"][$key]['id'] = $row['id'];
            $products_array["data"][$key]['title'] = $row['title'];
            $products_array["data"][$key]['parent'] = $row['parent'];
            $products_array["data"][$key]['article'] = $row['article'];
            $pr = (float)$row['price'];
            $products_array["data"][$key]['price'] =$pr;
            $products_array["data"][$key]['desc'] = $row['description'];
            $products_array["data"][$key]['new'] = (int)$row['new'];
            $products_array["data"][$key]['popular'] = (int)$row['popular'];
            $products_array["data"][$key]['favorite'] = (int)$row['favorite'];
            $products_array["data"][$key]['img_big'] = $row['img_big'];
            $products_array["data"][$key]['img_small'] = $row['img_small'];
            $products_array["data"][$key]['bonus_add'] = floor($row['price'] / 100 * $bonus_persent);
            $products_array["data"][$key]['times_bought'] = (int)$row['times_bought'];
            $products_array["data"][$key]['remains_count'] = (int)$row["remnant"];
            if ((int)$row["remnant"] != 0){
                $products_array["data"][$key]['remains_text'] = "В наличии";
            } else {
                $products_array["data"][$key]['remains_text'] = "Ожидается ".$row["coming"];
            }
            
            $products_array["data"][$key]['barcode'] = $row["barcode"];
            $products_array["data"][$key]['length'] = (int)$row["weight"]." см";
            $products_array["data"][$key]['pieces_per_package'] = (int)$row["quantityinpack"];
            $products_array["data"][$key]['energy'] = array(
                "energy_size" => (int)$row["weight"]." см",
                "energy_value" => round($row['energy_value']),    
                "energy_allergens" => $row['energy_allergens'],
                "energy_carbohydrates" => round($row['energy_carbohydrates']),
                "energy_protein" => round($row['energy_protein']),
                "energy_oils" => round($row['energy_oils']),
            );
            for ($i=0; $i<count($row['adds']); $i++){
                $row['adds'][$i]['price'] = (int)$row['adds'][$i]['price'];
                $row['types'][$i]['price'] = (int)$row['types'][$i]['price'];
            }
            if ($row['adds']){
                $products_array["data"][$key]['additives'] = $row['adds'];
            } else {
                $products_array["data"][$key]['additives'] = [];
            }
            if ($row['ing']){
                $products_array["data"][$key]['ingredients'] = $row['ing'];    
            } else {
                $products_array["data"][$key]['ingredients'] = [];
            }
            if ($row['types']){
                $products_array["data"][$key]['types'] = $row['types'];
            } else {
                $products_array["data"][$key]['types'] = [];
            }
            if ($row['analog']){
                $products_array["data"][$key]['analogs'] = $row['analog'];
            } else {
                $products_array["data"][$key]['analogs'] = [];
            }
}

$products = array_values($products_array["data"]);
$json["products"] = $products;
////
$json["catalog_first"][] = array(
    "id" => "4978",    
    "description" => "Описание",    
    "parent" => "4975",    
    "title" => "Срезанные цветы",    
    "image" => "/uploads/каталог/flowers.png",    
);
$json["catalog_first"][] = array(
    "id" => "4977",    
    "description" => "Описание",    
    "parent" => "4975",    
    "title" => "Комнатные растения",    
    "image" => "/uploads/каталог/room-flowers.png",    
);
$json["catalog_first"][] = array(
    "id" => "4976",    
    "description" => "Описание",    
    "parent" => "4975",    
    "title" => "Сопутствующие товары",    
    "image" => "/uploads/каталог/floristic.png",    
);
$json["catalog_first"][] = array(
    "id" => "4993",    
    "description" => "Описание",    
    "parent" => "4976",    
    "title" => "Флористика",    
    "image" => "/uploads/каталог/room-flowers.png",    
);
$json["catalog_second"][] = array(
    "id" => "2334",    
    "description" => "Описание",    
    "parent" => "1561",    
    "title" => "Суши и роллы",    
    "image" => "/2565/2aedf6df0f8794e554e6418befbbd90a8ba8fe4e.jpg",    
);
return json_encode($json);