id: 239
source: 1
name: cron-1c-update-users
category: flowerheaven
properties: 'a:0:{}'

-----

$SoftjetSync = $modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/', []);
$action = $SoftjetSync->getUserActions();
$sql = "SELECT internalKey FROM modx_user_attributes WHERE photo";
$c_users = $modx->query($sql);
$c_users = $c_users->fetchAll(PDO::FETCH_ASSOC);
foreach ($c_users as $c_user){
    $user_array = [];
    $profile = $modx->getObject('modUserProfile', ['internalKey' => $c_user['internalKey']]);
    $user = $action->getUser($profile);
    if ($user){
        $user_array["name"] = $user[0]["persons"][0]["name"];
        foreach ($user[0]["persons"][0]["contacts"] as $key=> $user_info){
            if ($user_info["type"] == "email"){
                $user_array["email"] = $user_info["content"];
            } elseif ($user_info["type"] == "phone"){
                $user_array["phone"] = $user_info["content"];
            } 
        }
        $price_type = $user[0]["price"]['price_type'];
        $sql = "SELECT * FROM modx_softjetsync_price_types WHERE uuid = '$price_type'";
        $info_price = $modx->query($sql);
        $info_price = $info_price->fetchAll(PDO::FETCH_ASSOC);
        $user_array["price_id"] = $info_price[0]["id"];
        if ($user_array){
            $profile->set("fullname", $user_array["name"]);
            $profile->set("email", $user_array["email"]);
            $profile->set("mobilephone", $user_array["phone"]);
            $profile->set("state", $user_array["price_id"]);
            $profile->save();
            echo "okay";
        }
    }
}