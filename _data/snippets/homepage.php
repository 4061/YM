id: 200
source: 1
name: homepage
properties: 'a:0:{}'

-----

$sql = "
SELECT 
    id_promo as id,
    name as title,
    description as content,
    date_end,
    id_product as product_id,
    code as promocode,
    type as type,
    child_discount as discount,
    image as img_big,
    url as url
FROM 
    modx_promo_base 
WHERE
    type = 1 and activity = 1 OR
    type = 2 and activity = 1
";
$promocodes = $modx->query($sql);
$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);
foreach ($promocodes as $key => $promocode){
    $image_small = $modx->runSnippet('pthumb', [
    'input' => substr($promocode["img_big"], 1),
    'options' => '&h=120&q=100',]);
    $promocodes[$key]['img_small'] = $image_small ;
    $promocodes[$key]['date_end'] = strtotime($promocode['date_end']);
}
$json["promotions"] = $promocodes;
//////
$page = 1;
$limit = 12;
$pdoFetch = new pdoFetch($modx);
$default = array(
    'class' => 'msProduct',
    'parents' => 1561,
    'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
    'leftJoin' => ['Data' => ['class' => 'msProductData'],
        'allRating' => ['class' => 'msProductOption', 'on' => 'allRating.product_id = msProduct.id AND allRating.key = "item_rating"'],
        'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
        'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
        'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
        'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
        'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
        'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
        'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "times_bought"'],
    ],
    'select' => [
        'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle`, parent, `msProduct`.`description`',
        'Data' => '`Data`.`restourants`,`Data`.`order_types`,`Data`.`price`,`Data`.`price2`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
        'allRating' => 'allRating.value as item_rating',
        'Option1' => 'Option1.value as energy_size',
        'Option2' => 'Option2.value as energy_allergens',
        'Option3' => 'Option3.value as energy_value',
        'Option4' => 'Option4.value as energy_carbohydrates',
        'Option5' => 'Option5.value as energy_protein',
        'Option6' => 'Option6.value as energy_oils',
        'Option7' => 'Option7.value as times_bought',
    ],
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'return' => 'data',
    'limit' => $limit,
    'offset' => ($page-1)*$limit
);
$pdoFetch->setConfig($default);
$rows = $pdoFetch->run();
$master_ids = array_column($rows, 'id');
$adds = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 3, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($adds);
$dobavki = $pdoFetch->run();

$types = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 9, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($types);
$types = $pdoFetch->run();

$ingredients = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 8, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`, `Data`.`image` as img_url, `Product`.`pagetitle` as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($ingredients);
$ingredients = $pdoFetch->run();

$analogs = array(
    'class' => 'msProductLink',
    'where' => ['Product.class_key' => 'msProduct', 'link' => 10, 'master:IN' => $master_ids],
    'leftJoin' => [
        'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
],
'select' => [
    'msProductLink' => 'master',
    'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
    'Data' => '`Data`.`price`',
],
'sortby' => 'Product.id',
'sortdir' => 'ASC',
'return' => 'data',
'limit' => 1000
);
$pdoFetch->setConfig($analogs);
$analogs = $pdoFetch->run();
$rows = array_combine(array_column($rows, 'id'), $rows);
foreach ($analogs as $analog){
    $rows[$analog['master']]['analog'][] = $analog;
}
foreach ($ingredients as $ingredient){
    $rows[$ingredient['master']]['ing'][] = $ingredient;
}
foreach ($dobavki as $dobavka){
    $rows[$dobavka['master']]['adds'][] = $dobavka;
}
foreach ($types as $type){
    $rows[$type['master']]['types'][] = $type;
}
foreach ($rows as $key => $row){
    $products_array["data"][$key]['id'] = $row['id'];
    $products_array["data"][$key]['title'] = $row['pagetitle'];
    $products_array["data"][$key]['parent'] = $row['parent'];
    $products_array["data"][$key]['article'] = (int)$row['article'];
    $products_array['data'][$key]['allRating'] = ($row['item_rating'])? $row['item_rating'] : 0;
    /*if ($_POST['JSON']['order_type'] == 1){
        $products_array["data"][$key]['price'] = (int)$row['price2'];
    } else {
        $products_array["data"][$key]['price'] = (int)$row['price'];    
    }*/
    $products_array["data"][$key]['price'] = (int)$row['price'];
    $products_array["data"][$key]['desc'] = $row['description'];
    $products_array["data"][$key]['new'] = (int)$row['new'];
    $products_array["data"][$key]['popular'] = (int)$row['popular'];
    $products_array["data"][$key]['favorite'] = (int)$row['favorite'];
    $products_array["data"][$key]['img_big'] = $row['img_big'];
    $products_array["data"][$key]['img_small'] = $row['img_small'];
    $products_array["data"][$key]['bonus_add'] = floor($row['price'] / 100 * $bonus_persent);
    $products_array["data"][$key]['times_bought'] = (int)$row['times_bought'];
    $products_array["data"][$key]['remains_count'] = 25;
    $products_array["data"][$key]['remains_text'] = "dfuvidsofov";
    $products_array["data"][$key]['barcode'] = "111111";
    $products_array["data"][$key]['pieces_per_package'] = 25;
    /*$en_size = explode(",",$row['energy_size']);
    $l = $en_size[1]*100;
    $l = $en_size[1]*1;
    $l = $l/10;
    $k = $en_size[0];
    $k = $k*1000;
        $fin = $fin+$l;*/
    $products_array["data"][$key]['energy'] = array(
        "energy_size" => $row['energy_size'],
        "energy_value" => round($row['energy_value']),    
        "energy_allergens" => $row['energy_allergens'],
        "energy_carbohydrates" => round($row['energy_carbohydrates']),
        "energy_protein" => round($row['energy_protein']),
        "energy_oils" => round($row['energy_oils']),
    );
            for ($i=0; $i<count($row['adds']); $i++){
                $row['adds'][$i]['price'] = (int)$row['adds'][$i]['price'];
                $row['types'][$i]['price'] = (int)$row['types'][$i]['price'];
            }
            if ($row['adds']){
                $products_array["data"][$key]['additives'] = $row['adds'];
            } else {
                $products_array["data"][$key]['additives'] = [];
            }
            if ($row['ing']){
                $products_array["data"][$key]['ingredients'] = $row['ing'];    
            } else {
                $products_array["data"][$key]['ingredients'] = [];
            }
            if ($row['types']){
                $products_array["data"][$key]['types'] = $row['types'];
            } else {
                $products_array["data"][$key]['types'] = [];
            }
            if ($row['analog']){
                $products_array["data"][$key]['analogs'] = $row['analog'];
            } else {
                $products_array["data"][$key]['analogs'] = [];
            }
}
$products = array_values($products_array["data"]);
$json["products"] = $products;
//// Получаем список категории (4 штуки из основного каталога)
$query = $modx->newQuery('modResource');
$query->sortby('menuindex', 'ASC');
$query->where(array(
    'template' => 3,
    'parent' => 1561
));
$query->limit(4);
$categoryes = $modx->getCollection("modResource",$query);

$data = array();
$num = 0;
foreach($categoryes as $key => $category) {
    $data[$num]['id'] = $category->get("id");
    $data[$num]['title'] = $category->get("pagetitle");
    $data[$num]['parent'] = $category->get("parent");
    $data[$num]['description'] = $category->get("description");
    $data[$num]['image'] = $category->getTvValue("image");
    $num++;
}
$json["catalog_first"] = $data;
/*$json["catalog_first"][] = array(
    "id" => "3497",    
    "description" => "Описание",    
    "parent" => "1561",    
    "title" => "Бест диал",    
    "image" => "/uploads/flowers/sale.png",    
);
$json["catalog_first"][] = array(
    "id" => "3495",    
    "description" => "Описание",    
    "parent" => "1561",    
    "title" => "Срезанные цветы",    
    "image" => "/uploads/flowers/flowers-3.png",    
);
$json["catalog_first"][] = array(
    "id" => "3503",    
    "description" => "Описание",    
    "parent" => "1561",    
    "title" => "Флористические товары",    
    "image" => "/uploads/flowers/floristic-1.png",    
);
$json["catalog_first"][] = array(
    "id" => "3496",    
    "description" => "Описание",    
    "parent" => "1561",    
    "title" => "Горшечные растения",    
    "image" => "/uploads/flowers/room-flowers.png",    
);*/
$json["catalog_second"][] = array(
    "id" => "2334",    
    "description" => "Описание",    
    "parent" => "1561",    
    "title" => "Суши и роллы",    
    "image" => "/2565/2aedf6df0f8794e554e6418befbbd90a8ba8fe4e.jpg",    
);
return json_encode($json);