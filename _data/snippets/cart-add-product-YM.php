id: 256
source: 1
name: cart-add-product-YM
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();
    
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    //return json_encode($_SESSION);
    if ($token){
        $_SESSION["order_type"] = $_POST['JSON']["order_type"];
        $product_id = $_POST['JSON']['id']; //id продукта
        $count = $_POST['JSON']['count']; //id продукта
        $ingredients = $_POST['JSON']['ingredients']; //информация об удаляемых продуктах
        $additives = $_POST['JSON']['additives']; //информация о добавляемых продуктах
        $size = $_POST['JSON']['size']; //информация о размере
        $type = $_POST['JSON']['type']; //тип (прожарки, лаваша и т.д.)
        for ($i=0;$i<count($additives);$i++){
            $resources = $modx->getCollection('modResource', array("id" => $additives[$i]['id']));
            foreach ($resources as $res){
                $additives[$i]['title'] = $res->get('pagetitle');
            }
        }
        for ($i=0;$i<count($type);$i++){
            $resources = $modx->getCollection('modResource', array("id" => $type[$i]['id']));
            foreach ($resources as $res){
                $type[$i]['title'] = $res->get('pagetitle');
            }
        }
        if ($ingredients == NULL){
            $ingredients = [];
        }        
        if ($additives == NULL){
            $additives = [];
        }
        if ($ingredients == NULL){
            $ingredients = [];
        }
        $option['childs'] = array(
            "ingredients"=>$ingredients, 
            "additives"=>$additives,
            "size"=>$size,
            "type"=>$type,
            "rating" => [],
            //"comment" => $_POST["JSON"]["comment"]
        );
        $option['comment'] = $_POST["JSON"]["comment"];
        $ms2 = $modx->getService('miniShop2');
        $ms2->initialize($modx->context->key);
        $cart = $ms2->cart->get();
        $order = $ms2->order->get(); // заказ
        $status = $ms2->cart->status(); // статус корзины
        //return json_encode($_SESSION['minishop2']['cart']);
        $sql = "SELECT * FROM modx_promo_base WHERE type = 4";
        $thresholds = $modx->query($sql);
        $thresholds = $thresholds->fetchAll(PDO::FETCH_ASSOC);
        foreach ($thresholds as $threshold){
            $items = explode(",", $threshold['id_product']);
            foreach ($items as $item){
                $res = $modx->getObject('modResource', $item);
                if ($res->get("id") == $product_id){
                    $selector = true;
                }
            }
        }
        if ($selector){
            foreach ($thresholds as $threshold){
                $items = explode(",", $threshold['id_product']);
                foreach ($items as $item){
                    foreach ($_SESSION['minishop2']['cart'] as $key => $prod_item){
                        if ($prod_item["id"] == $item){
                            if ($item != $product_id){
                                $cart = $ms2->cart->change($key, 0);
                            }
                        }
                    }
                }
            }
        }
        $sql = "SELECT * FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.product_id = '$product_id' AND modx_ms2_product_options.key = 'max_count'";
        $this_product_info = $modx->query($sql);
        $this_product_info = $this_product_info->fetchAll(PDO::FETCH_ASSOC);
        foreach ($_SESSION['minishop2']['cart'] as $key => $cart_item){
            if ($cart_item["id"] == $product_id){
                $this_product_key = $key;
            }
        }
        if ($this_product_key){
            if ($this_product_info[0]["value"]){
                if ((int)$this_product_info[0]["value"]>$_SESSION["minishop2"]["cart"][$this_product_key]["count"]){
                    $cart = $ms2->cart->add($product_id, $count, $option);  
                }
            } 
        } else {
            $cart = $ms2->cart->add($product_id, $count, $option);
        }
        $json["key"] = $this_product_key;
        //$cart = $ms2->cart->add($product_id, $count, $option);
        $total_cost = 0;
        $total_count = 0;
        foreach ($_SESSION['minishop2']['cart'] as $key => $cart_item){
            $dop_price = 0; // доп цена 0 
           //$_SESSION['minishop2']['cart'][$key]['price'] = 0;
            foreach ($cart_item['options']['childs']['additives'] as $additive){
                $additive_id = $additive['id'];
                $additive_count = $additive['count'];
                $resources = $modx->getCollection('modResource', array("id" => $additive_id));
                foreach ($resources as $res){
                    $dop_price =$dop_price+  ($res->get('price')*$additive_count);
                }
                //$cart = $ms2->cart->add($additive_id, $additive_count);
            }
            $_SESSION['minishop2']['cart'][$key]['additional_cost'] = $dop_price;
            //$res = $modx->getObject("modResource", $_SESSION['minishop2']['cart'][$key]['id']);
            //$_SESSION['minishop2']['cart'][$key]['total_cost'] = $res->get("price") + $dop_price;
            $_SESSION['minishop2']['cart'][$key]['total_cost'] = $_SESSION['minishop2']['cart'][$key]['price'] + $dop_price;
        }
        foreach ($_SESSION['minishop2']['cart'] as $cart_item){
            $total_cost_product = $cart_item['total_cost']*$cart_item['count'];
            $total_cost = $total_cost + $total_cost_product;
            $total_count = $total_count + $cart_item['count'];
        }
        /*------Расчет скидки---------*/
        $PH_PROGRAM = $modx->getOption('loyalty_PH_program');
        if ($PH_PROGRAM){
        $profile = $modx->getObject('modUserProfile', ['website' => $token]);
        
        $PH["OrganizationId"] = "7580c84f-3d89-ddd4-0175-26daf8845f1b";
        $PH["ParentOrderId"] = "123";
        $PH["OrderId"] = "123";
        $PH["ClientId"] = $profile->get("zip");
        $PH["OrderNumber"] = 123;
        $PH["OrderSumWithDiscount"] = $total_cost;
        $PH["OrderSum"] = $total_cost;
        $PH["Order"] = [];
        foreach ($_SESSION['minishop2']['cart'] as $prod){
            $res = $modx->getObject('modResource',$prod["id"]);
            $prodid = $prod["id"];
            $sql = "SELECT modx_ms2_product_options.value FROM modx_ms2_product_options WHERE modx_ms2_product_options.product_id = '$prodid' AND modx_ms2_product_options.key='guid'";
            $guid = $modx->query($sql);
            $guid = $guid->fetchAll(PDO::FETCH_ASSOC);
            $PH["Order"][] = array(
            "ProductId" => $guid[0]['value'],
            "ProductName" => $res->get('pagetitle'),
            "CategoryId" => "",
            "Amount" => $prod['count'],
            "Price" => $prod["price"],
            "PriceWithDiscount" => $prod["price"],
            "CategoryName" => "",
            );
        }
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://cabinet.prime-hill.com/api/iiko/v3/GetDiscount/d35-3157-4515-32a",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($PH),
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 34e4247a-7f83-4486-af51-d1a52c9f5015"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        $response = json_decode($response, true);
       // return json_encode($response);
        /*------Расчет скидки конец---------*/
        }
        
        
        $_SESSION['minishop2']['total_cost'] = $total_cost;
        $_SESSION['minishop2']['total_count'] = $total_count;
        $json['promocodes'] = $_SESSION['minishop2']['promocodes'];
        $json['total_cost'] = (int)$_SESSION['minishop2']['total_cost'];
        $json['total_count'] = (int)$_SESSION['minishop2']['total_count'];
        $cart_items = $_SESSION['minishop2']['cart'];
        foreach ($cart_items as $key=> $cart_item){
            $cart_items[$key]['key'] = $key;
            $sql = "SELECT value FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.product_id = {$cart_item['id']} AND modx_ms2_product_options.key = 'max_count'";
            $max_product_count = $modx->query($sql);
            $max_product_count = $max_product_count->fetchAll(PDO::FETCH_ASSOC);
            $max_product_count = ($max_product_count[0]['value']);
            if ($max_product_count){
                $cart_items[$key]['max_count'] = (int)$max_product_count;
            } else {
                $cart_items[$key]['max_count'] = 999999;
            }
            $res = $modx->getObject('modResource',$cart_item["id"]);
            $cart_items[$key]["img_url"] = $res->get('image');
            $cart_items[$key]["title"] = $res->get('pagetitle');
            $cart_items[$key]["desc"] = $res->get('description');
        }
        $cart_items = array_values($cart_items);
        $json['products'] = $cart_items;
        $discount_cost = 0;
        if ($_POST['JSON']["order_type"] == 1){
            $json["PrintMessage"] = "Стоимость при самовывозе со скидкой 20% (скидка не действует на Напитки)."; //- Сообщение на печать
            foreach ($cart_items as $cart_item){
                $res = $modx->getObject("modResource", $cart_item['id']);
                $discount_cost = $discount_cost + ($res->get("price2")*$cart_item["count"]);
                if ($cart_item['options']['childs']["additives"] != []){
                    foreach ($cart_item['options']['childs']["additives"] as $add){
                        $res = $modx->getObject("modResource", $add['id']);
                        $discount_cost = $discount_cost + ($res->get("price2")*$cart_item["count"]);
                    }
                }
            }
        } elseif ($_POST["JSON"]["order_type"] == 2){
            foreach ($cart_items as $cart_item){
                $res = $modx->getObject("modResource", $cart_item['id']);
                $discount_cost = $discount_cost + ($res->get("price")*$cart_item["count"]);
                if ($cart_item['options']['childs']["additives"] != []){
                    foreach ($cart_item['options']['childs']["additives"] as $add){
                        $res = $modx->getObject("modResource", $add['id']);
                        $discount_cost = $discount_cost + ($res->get("price")*$cart_item["count"]);
                    }
                }
            }
        }
        if ($_SESSION['minishop2']['promocodes']){
            //$modx->runSnippet('check-code');
            $code = $_SESSION['minishop2']['promocodes'];
            $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
            $current_code = $modx->query($sql);
            $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
            
            if ($current_code[0]['type'] == 3){
                $costfourth = $_SESSION['minishop2']['total_cost'] * 0.25;
                if ($costfourth>=200){
                    $actual_discount = 200;
                } else {
                    $actual_discount = $costfourth;
                }
                $discount_cost = $_SESSION['minishop2']['total_cost'] - $actual_discount;
                $_SESSION['minishop2']['discount_cost'] = $discount_cost;
                $_SESSION['minishop2']['actual_discount'] = $actual_discount;
            }
            if ($current_code[0]['type'] == 1){
                $discount_percent = $current_code[0]['child_discount']/100;
                $actual_discount = round($_SESSION['minishop2']['total_cost']*$discount_percent);
                $discount_cost = round($_SESSION['minishop2']['total_cost'] - $actual_discount);
                $_SESSION['minishop2']['discount_cost'] = $discount_cost;
                $_SESSION['minishop2']['actual_discount'] = $actual_discount;
            }
        }
        $json['actual_discount'] = $_SESSION['minishop2']['actual_discount']; //цена со скидкой
        $json['discount_cost'] = $discount_cost;
        //$json['actual_discount'] = $discount_cost; //цена со скидкой
        $json["SumDiscount"] = $response["SumDiscount"]; //сумма скидкой из ПХ  
        $json["BonusBalance"] = $response["BonusBalance"]; //бонусный баланс
        $json["MaxBonus"] = $response["MaxBonus"]; //бонусов для списания
        if ($_POST['JSON']["order_type"] == 2){
           $_SESSION["delivery_cost"] = 100;
        } else {
            $_SESSION["delivery_cost"] = 0;
        }
        $json["delivery_cost"] = $_SESSION["delivery_cost"];
        $json["operation_id"] = $_POST["JSON"]["operation_id"];
        //$json['order_type']  = $_SESSION['minishop2']['cart']['order_type'];
        //$json["ProductGift"] = $response["ProductGift"]; //подарок
       // $json["response"] = $response;
        $json["min_count_cost"] = $_SESSION["min_delivery_cost"];
        $json["ssess"] = session_id();
        return json_encode($json);
    } else {
        return "Нет токена";
    }
}