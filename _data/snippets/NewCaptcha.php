id: 267
source: 1
name: NewCaptcha
properties: 'a:0:{}'

-----

function gdImgToHTML( $gdImg, $format='png' ) {
    ob_start();

    if( $format == 'jpeg'){
        imagejpeg( $gdImg );
    }
    else
    if( $format == 'png' ){
        imagepng( $gdImg );
    }
    else
    if( $format == 'gif' )
    {
        imagegif( $gdImg );
    }
    // Создание цветов

    $image_data = ob_get_contents();
    ob_end_clean();

    return "<img src='data:image/$format;base64," . base64_encode( $image_data ) . "'>";
}

$text = 'TEST...';

$img_width = 100;
$img_height = 50;
 
$img = imagecreatetruecolor($img_width, $img_height);
 
$black = imagecolorallocate($img, 0, 0, 0);
$white = imagecolorallocate($img, 255, 255, 255);
 
imagefill($img, 0, 0, $black);
 
imagestring($img, 5, $img_width/2, $img_height/2, $text, $white);

echo gdImgToHTML($img);