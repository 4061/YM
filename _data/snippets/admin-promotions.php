id: 179
source: 1
name: admin_promotions
snippet: "$sql = \"SELECT *, DATE_FORMAT(date_end, '%d.%m.%Y') as new_date_end FROM modx_promo_base WHERE type = 4\";\n$promocodes = $modx->query($sql);\n$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);\nforeach ($promocodes as $key=>$promocode){\n    $products = explode(\",\",$promocode[\"id_product\"]);\n    foreach ($products as $product){\n        $sql = \"SELECT * FROM modx_ms2_products, modx_site_content WHERE modx_ms2_products.id = '$product' AND modx_ms2_products.id = modx_site_content.id\";\n        $product_info = $modx->query($sql);\n        $product_info = $product_info->fetchAll(PDO::FETCH_ASSOC);\n        $promocodes[$key][\"modificators\"][] = $product_info;\n    }\n}\nreturn($promocodes);"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_promotions.php
content: "$sql = \"SELECT *, DATE_FORMAT(date_end, '%d.%m.%Y') as new_date_end FROM modx_promo_base WHERE type = 4\";\n$promocodes = $modx->query($sql);\n$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);\nforeach ($promocodes as $key=>$promocode){\n    $products = explode(\",\",$promocode[\"id_product\"]);\n    foreach ($products as $product){\n        $sql = \"SELECT * FROM modx_ms2_products, modx_site_content WHERE modx_ms2_products.id = '$product' AND modx_ms2_products.id = modx_site_content.id\";\n        $product_info = $modx->query($sql);\n        $product_info = $product_info->fetchAll(PDO::FETCH_ASSOC);\n        $promocodes[$key][\"modificators\"][] = $product_info;\n    }\n}\nreturn($promocodes);"

-----


$sql = "SELECT *, DATE_FORMAT(date_end, '%d.%m.%Y') as new_date_end FROM modx_promo_base WHERE type = 4";
$promocodes = $modx->query($sql);
$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);
foreach ($promocodes as $key=>$promocode){
    $products = explode(",",$promocode["id_product"]);
    foreach ($products as $product){
        $sql = "SELECT * FROM modx_ms2_products, modx_site_content WHERE modx_ms2_products.id = '$product' AND modx_ms2_products.id = modx_site_content.id";
        $product_info = $modx->query($sql);
        $product_info = $product_info->fetchAll(PDO::FETCH_ASSOC);
        $promocodes[$key]["modificators"][] = $product_info;
    }
}
return($promocodes);