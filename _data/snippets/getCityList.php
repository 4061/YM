id: 245
name: getCityList
description: 'Сниппет для вывода списка городов, для возможности изменить у него какие-то данные. В частности максимальную цену заказа'
category: 'Шаблоны Админки'
properties: 'a:0:{}'

-----

$where = array(
    "parent" => 1802,
    "template" => 36
);

$cityGet = $_GET["cities"];
$cityGet = explode(",",$cityGet);

$getList = $modx->getCollection("modResource",$where);

foreach($getList as $city) {
    $maxPrice = ($city->getTvValue("tvMaxPrice"))? $city->getTvValue("tvMaxPrice") : 0;
    $selected = (in_array($city->get("id"),$cityGet))? "selected" : "";
    $cityList[] = array(
        "id" => $city->get("id"),
        "pagetitle" => $city->get("pagetitle"),
        "tvMaxPrice" => $maxPrice,
        "published" => $city->get("published"),
        "selected" => $selected,
    );
}

return $cityList;