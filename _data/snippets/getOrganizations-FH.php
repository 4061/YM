id: 235
source: 1
name: getOrganizations-FH
category: flowerheaven
properties: 'a:0:{}'

-----

$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$parent = $_GET['id'];
$sql = "SELECT id FROM modx_site_content WHERE parent = $parent";
$orgs = $modx->query($sql);
$orgs = $orgs->fetchAll(PDO::FETCH_ASSOC);
$json['orgs'] = array();

foreach ($orgs as $org){
    $payments = array();
    $deliveries = array();
    $res = $modx->getObject('modResource', $org['id']);
    $delivery = $res->getTVValue('org_dels');
    $delivery = json_decode($delivery,true);
    if ($res->getTVValue('online_payment')==1){
        $payments[] = array("id" =>1, "name"=>"Apple Pay/Samsung Pay");
    }
    if ($res->getTVValue('cash_courier_payment')==1){
        $payments[] = array("id" =>3, "name"=>"Оплата наличными при получении");
    }
    if ($res->getTVValue('cashless_courier_payment')==1){
        $payments[] = array("id" =>4, "name"=>"Оплата картой при получении");
    }
    if ($res->getTVValue('courier')==1){
        $deliveries[] = array("id" =>2, "name"=>"Курьером");
    }
    if ($res->getTVValue('selfcourier')==1){
        $deliveries[] = array("id" =>1, "name"=>"Самовывоз");
    }
    if ($res->getTVValue('restourant')==1){
        $deliveries[] = array("id" =>3, "name"=>"В ресторане");
    }
    $payments[] = array("id" =>8, "name"=>"Выписать счет");
    $payments[] = array("id" =>7, "name"=>"Оплата по QR коду");
   // $payments[] = array("id" =>6, "name"=>"Онлайн оплата с привязкой карты");
    $GPS = $res->getTVValue('GPS');
    $GPS = explode(",", $GPS);
    //$lan = $GPS[0];
    //$lan = round($lan,12);
    $json['orgs'][] = array(
        "title" => $res->get('pagetitle'),
        "id" => $res->get('id'),
        "requisites" => $url."/".$res->get("uri"),
        "deliveryTerminalId" => $res->getTVValue('deliveryTerminalId'),
        "img" => $res->getTVValue('image'),
        "waiting_time" => $res->getTVValue('waitingtime'),
        "address" => $res->get('longtitle'),
        "description" => $res->get('description'),
        "comment" => $res->get('introtext'),
        "phone" => $res->getTVValue('phone'),
        "worktime" => $res->getTVValue('mode'),
        "GPS" => array("latitude"=>($GPS[0]), "longitude"=>$GPS[1]),
        "payments"=>$payments,
        "deliveries"=>$deliveries
    );
}

return json_encode($json);