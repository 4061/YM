id: 53
source: 1
name: catalog
properties: 'a:0:{}'

-----

/**
 * @OA\Info(title="My First API", version="0.1")
 */
$sql = "SELECT id,pagetitle,parent FROM modx_site_content WHERE deleted = 0 and published = 1 and parent = '1561' and hidemenu = '0'
ORDER BY menuindex ASC";
$content_id = $modx->query($sql);
$categorys = $content_id->fetchAll(PDO::FETCH_ASSOC);
$json['data'];
$order_type = $_GET['order_type'];
foreach ($categorys as $category){
    $res = $modx->getObject('modResource', (int)$category['id']);
    $order_types = $res->getTVValue('cat_order_types');
    $order_types = explode(",", $order_types);
    if (in_array($order_type,$order_types)){
        $json['data'][] = array(
            "id"=>$category['id'],
            "parent"=>$category['parent'],
            "title"=>$category['pagetitle'],
            "distance"=>$res->getTVValue('tvDistance'),
            "time"=>$res->getTVValue('tvTime')
        );
    }
}
/**
 * @OA\Get(
 *     path="/catalogs.json", tags = {"Pages"}
 *     @OA\Response(response="200", description="Сукес")
 * )
 */
return json_encode($json);