id: 242
source: 1
name: cron-tinkoff-flowerheaven-check
category: flowerheaven
properties: 'a:0:{}'

-----

include  $_SERVER["DOCUMENT_ROOT"].'/file/tinkoff/TinkoffMerchantAPI.php'; 
$Terminal_Key = "1613647560681DEMO";
$Secret_Key = "usjbw5putnpztap7";
$sql="
SELECT 
    modx_ms2_orders.id as modx_order, 
    CURRENT_TIMESTAMP,
    modx_ms2_orders.createdon,
    DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -2 day) as current
FROM 
    modx_ms2_orders
WHERE
    modx_ms2_orders.createdon >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -2 day) AND 
    modx_ms2_orders.status = 14
";
$orders = $modx->query($sql);
$orders = $orders->fetchAll(PDO::FETCH_ASSOC);
foreach ($orders as $order){
    $order_id = $order['modx_order'];
    $ord = $modx->getObject('msOrder', $order_id);
    $current_address = $modx->getObject('msOrderAddress', $ord->get("address"));
    $paymentId = $current_address->get("trans_id");
    $api = new TinkoffMerchantAPI(
        $Terminal_Key,  //Ваш Terminal_Key
        $Secret_Key   //Ваш Secret_Key
    );
    $params = [
      'PaymentId' => $paymentId,
    ];
    $response = $api->getState($params);
    $response = json_decode($response);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=' . $YOUR_API_KEY,
            'apns-push-type: alert'
        ];
        $user = $modx->getObject("modUserProfile",array("internalKey"=>$current_address->user_id));
        $request_body1['registration_ids'] = [$user->city]; 
    if ($response->Status == "CONFIRMED"){
        $miniShop2 = $modx->getService('miniShop2');
        $miniShop2->initialize($modx->context->key, $scriptProperties);
        $miniShop2->changeOrderStatus($order_id, 1);
        $request_body1['notification'] = array("title" => 'Заказ #'.$order_id.' оплачен', "body" => 'Ваш заказ успешно оплачен.', "sound"=>"default");
        $request_body1['data'] = array(
            "type" => "alert", 
            "data" => array(
            "title" => "Banners", 
            "body" => "test",         
            "type" => "product", 
            "product_id" => 94096),
            "mandatory" => true
            );
               
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
    } elseif ($response->Status == "DEADLINE_EXPIRED"){
        $miniShop2 = $modx->getService('miniShop2');
        $miniShop2->initialize($modx->context->key, $scriptProperties);
        $miniShop2->changeOrderStatus($order_id, 15);
        $request_body1['notification'] = array("title" => 'Ошибка оплаты заказа #'.$order_id, "body" => 'Ваш заказ не был оплачен.', "sound"=>"default");
        $request_body1['data'] = array(
            "type" => "alert", 
            "data" => array(
            "title" => "Banners", 
            "body" => "test",         
            "type" => "product", 
            "product_id" => 94096),
            "mandatory" => true
            );
               
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
    }
}