id: 196
source: 1
name: cron-resend-broken-orders
description: 'переотправляет заказы которые не отправились по вине айко'
properties: 'a:0:{}'

-----

$sql = "
SELECT 
    modx_ms2_orders.id as modx_order, 
    CURRENT_TIMESTAMP,
    modx_ms2_orders.createdon,
    DATE_ADD(CURRENT_TIMESTAMP, INTERVAL +600 SECOND) as current
FROM 
    modx_ms2_orders
WHERE
    modx_ms2_orders.createdon >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL +600 SECOND) AND 
    (modx_ms2_orders.status = 8 OR modx_ms2_orders.status = 1) 
";
$orders = $modx->query($sql);
$orders = $orders->fetchAll(PDO::FETCH_ASSOC);
                    $miniShop2 = $modx->getService('miniShop2');
                    $miniShop2->initialize($modx->context->key, $scriptProperties);            
                    
foreach ($orders as $order){
    $order = $modx->getObject("msOrder", $order["modx_order"]);
    if ($order->user_id != 728 && $order->user_id != 837 && $order->user_id !=762 && $order->user_id !=202){
        
        // проверяем включена ли настройка для активации белого списка
        if($modx->getOption("app_white_list")) {
            // формируем запрос к бд, для поиска хотябы одного оплаченного заказа
            $sql = 'SELECT id FROM modx_ms2_orders WHERE user_id = '.$order->user_id.' AND status = 2';
            $result = $modx->query($sql);
            // получаем id первого полученного заказа
            $check = $result->fetch(PDO::FETCH_ASSOC)['id'];
        } else if($modx->getOption("app_black_list")) {
            // если чёрный список включён и мы сюда дошли, то для пользователя выставлено ограничение на заказ через кол-центр (2)
            // IIKO будет пропущен
            $check = true;
        } else {
            // если настройка не включена, то ставим значение false
            $check = false;
        }
        
        
        
        if ($order->get("status")){
            $iiko_id = "YaponaMP";
            $iiko_pass = "A8BVoS3h";
            $token = str_replace('"',"",file_get_contents('https://iiko.biz:9900/api/0/auth/access_token?user_id='.$iiko_id.'&user_secret='.$iiko_pass));
            $contacts = $modx->getObject('msOrderAddress', array('id'=> $order->address));
            $person =$contacts->person; // количество человек
            $products = $order->getMany('Products');
            $phone = $contacts->phone;
            $phone = str_replace("+", "", $phone);
            $org = $contacts->index;
            $res_obj = $modx->getObject('modResource', $org);
            $iiko_send_statuses[2] = $res_obj->getTVValue('courier');
            $iiko_send_statuses[1] = $res_obj->getTVValue('selfcourier');
            $iiko_send_statuses[3] = $res_obj->getTVValue('restourant');
            
            // получаем ID родителя для организации
            $cityId = $res_obj->get("parent");
            // получаем объект города для организации по ID
            $parentCity = $modx->getObject("modResource",$cityId);
            // получаем максимальную цену для заказа
            $maxPrice = $parentCity->getTVValue("tvMaxPrice");
            // если полученная цена пустая, то присваеваем её 0
            $maxPrice = ($maxPrice)? $maxPrice : 0;
            // сравниваем общую цену с максимальной для города (Максимальная цена не равна 0 и оплата не онлайн)
            if ($order->get("cost") >= $maxPrice && $maxPrice != 0 && $order->get('payment') != 1) {
                // если сумма превышена, пропускаем IIKO, идём сразу в call-центр
                $check = true;
            }
            
            // проверяем что мы получили из предыдущиъ действий.
            // - если настройка на проверку белого списка отключена, то мы в любом случае идём дальше
            // - если настройка включена, то мы идём дальше если отсуствует хотя бы один оплаченный заказ
            if(!$check){
                
                if ($iiko_send_statuses[$order->delivery] == 1){
                    $json['deliveryTerminalId'] = $res_obj->getTVValue('deliveryTerminalId');
                }
                
            }
            
            $json['organization'] = $res_obj->getTVValue('iiko_organization_key');
            $json['customer'] = array(
               // "id" => (int)$order->user_id,
                "name" => $contacts->receiver,
                "phone" => $phone
            );
            $delivery = $order->delivery;
            if ($delivery == 2){
                $delivery = false;
            } else {
                $delivery = true;
            }
            $dt = new Datetime($order->createdon);
            if ($order->delivery == 1){
                $dateint = 'PT'.$res_obj->getTVValue("integrationSelfCourierTime")."M";
            } else {
                $dateint = 'PT'.$res_obj->getTVValue("integrationCourierTime")."M";
            }
            $interval = new DateInterval($dateint);
            $dt->add($interval);
            $order_time = $dt->format('Y-m-d H:i:m');
            $json['order'] = array(
               // "id" => $order->id,
                "date" => $order_time,
                "phone" =>$phone,
                "isSelfService" =>$delivery,
                "personsCount" => $person,
            );
            if ($contacts->time_appointment) $json["order"]["date"] = $contacts->time_appointment;
            foreach ($products as $product){
                $product_id = $product->product_id;
                $sql = "
                SELECT 
                    modx_ms2_product_options.value,
                    modx_ms2_products.article
                FROM 
                    modx_ms2_product_options,modx_ms2_products
                WHERE 
                    modx_ms2_product_options.product_id = '$product_id' AND 
                    modx_ms2_product_options.key = 'guid' AND
                    modx_ms2_product_options.product_id = modx_ms2_products.id
                ";
                $info_product = $modx->query($sql);
                $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                $guid = $info_product[0]['value'];
                $code = $info_product[0]['article'];
                $item_options = json_decode($product->options, true);
                $adds = $item_options["childs"]["additives"];
                foreach ($adds as $key=> $add){
                    $adds_array[] = array("id"=>$add["id"]);
                    $this_id = $add["id"];
                    $adds[$key]['amount'] = $add['count'];
                    $adds[$key]['name'] = $add['title'];
                    unset($adds[$key]['count']);
                    unset($adds[$key]['title']);
                    $adds[$key]['groupName'] = "";
                    $sql = "
                    SELECT * 
                    FROM 
                        `modx_ms2_product_options` 
                    WHERE 
                        modx_ms2_product_options.product_id = '$this_id' AND 
                        (modx_ms2_product_options.key = 'guid' OR modx_ms2_product_options.key = 'parentGroup')
                    ";
                    $guids = $modx->query($sql);
                    $guids = $guids->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($guids as $gui){
                        if ($gui["key"] == "guid"){
                            $adds[$key]['id'] = $gui["value"];
                        }
                        if ($gui["key"] == "parentGroup"){
                            $adds[$key]['groupId'] = $gui["value"];
                        }
                    }
                }
                $types = $item_options["childs"]["type"];
                foreach ($types as $key=> $add){
                    $adds_array[] = array("id"=>$add["id"]);
                    $this_id = $add["id"];
                    $types[$key]['amount'] = $add['count'];
                    $types[$key]['name'] = $add['title'];
                    unset($types[$key]['count']);
                    unset($types[$key]['title']);
                    $types[$key]['groupName'] = "";
                    $sql = "
                    SELECT * 
                    FROM 
                        `modx_ms2_product_options` 
                    WHERE 
                        modx_ms2_product_options.product_id = '$this_id' AND 
                        (modx_ms2_product_options.key = 'guid' OR modx_ms2_product_options.key = 'parentGroup')
                    ";
                    $guids = $modx->query($sql);
                    $guids = $guids->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($guids as $gui){
                        if ($gui["key"] == "guid"){
                            $types[$key]['id'] = $gui["value"];
                        }
                        if ($gui["key"] == "parentGroup"){
                            $types[$key]['groupId'] = $gui["value"];
                        }
                    }
                }
                $json['order']['items'][$product->product_id] = array(
                    "id" => $guid,
                    "code" => $code,
                    "name" => $product->name,
                    "amount" => $product->count,
                    "sum" => 100,
                    "modifiers" => array_merge($adds, $types),
                    "comment" => $product->comment
                );
            }
            foreach ($adds_array as $add){
                unset($json['order']['items'][$add['id']]);
            }
            $json['order']['items'] = array_values($json['order']['items']);
            $json['order']['address'] = array(
                "city" =>$contacts->city,
                "street" =>$contacts->street,
                "home" => $contacts->house_num,
                "housing" => "",
                "floor" => $contacts->floor,
                "entrance" => $contacts->entrance,
                "apartment" => $contacts->flat_num,
                "comment" => $contacts->comment
            );
            if ($iiko_send_statuses[$order->delivery] == 3){
                $json['order']["comment"] = $json['order']["comment"]." Относится к ресторану: ".$res_obj->get("pagetitle");
            }
            if ($contacts->building){
                $json['order']["comment"] = "Номер столика: ".$contacts->building;
            }
            if ($contacts->room){
                $surrender = "Сдача с купюры: ".$contacts->room.", ";
            }
            $json['order']['comment'] = $json['order']['comment']." ".$surrender.$contacts->comment." Заказ в приложении №".$order->id;
            if ($order->get('payment') == 1){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '657b1ba4-0590-4193-9fbc-e9a144496a89',
            				'code' => "APSP",
            				'name' => "Аpple pay/samsung pay",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => true
            		]
            	);    
            }
            if ($order->get('payment') == 3){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '09322f46-578a-d210-add7-eec222a08871',
            				'code' => "CASH",
            				'name' => "Наличные",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => false
            		]
            	);    
            }
            if ($order->get('payment') == 4){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '1dda6a70-bdd2-4aa6-aef1-73b652796f46',
            				'code' => "CARD",
            				'name' => "Банковские карты",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => false
            		]
            	);    
            }
            if ($order->get('payment') == 6){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '03a3f513-ef0f-4378-935b-44e6c858f3d9',
            				'code' => "OLMP",
            				'name' => "Онлайн оплата МП",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => true
            		]
            	);    
            }
            if ($contacts->time_appointment){
                $json["order"]["comment"] .= " Время доставки: ".$contacts->comment_photo;
                unset($json['deliveryTerminalId']);
            }
        	/*$pay = array(
        		[
        			'sum' => $order->get("cost"),
        			'paymentType' => [
        				'id' => '1dda6a70-bdd2-4aa6-aef1-73b652796f46',
        				'code' => "CARD",
        				'name' => "Банковские карты",						
        				"deleted" => false				
        			],
        			"isProcessedExternally" => false
        		]
        	);
        	*/
        	$json['order']['paymentItems'] = $pay;
        	$modx->log(modX::LOG_LEVEL_ERROR, 'payment: '.$order->get('payment'));
            $modx->log(modX::LOG_LEVEL_ERROR, 'JSON: '.json_encode($json, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
            $modx->log(modX::LOG_LEVEL_ERROR, 'URL: '.'https://iiko.biz:9900/api/0/orders/add?access_token='.$token.'&requestTimeout=50000');
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_PORT => "9900",
              CURLOPT_URL => 'https://iiko.biz:9900/api/0/orders/add?access_token='.$token.'&requestTimeout=50000',
             //CURLOPT_URL => 'https://iiko.biz:9900/api/0/orders/add?access_token=rere&requestTimeout=10000',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 50,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($json),
              CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: ee637244-d9bb-a608-a457-e9e6a3bbe633"
              ),
            ));
            if ($modx->getOption('IIKO')){
                $response = curl_exec($curl);
                //$modx->log(xPDO::LOG_LEVEL_ERROR,'Сообщение которое хотим отправить в лог');
            }
            
            $err = curl_error($curl);
            curl_close($curl);
            $modx->log(modX::LOG_LEVEL_ERROR, 'AIKO RESPONSE: '.json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
            $response = json_decode($response,1);
            if ($response['orderId']){
                $contacts->set("id_integration_order", $response['orderId']);
                $contacts->save();
                $order->set("status",6);
                $order->save();
        
                            $url = 'https://fcm.googleapis.com/fcm/send';
                            $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
                            $request_headers = [
                                'Content-Type: application/json',
                                'Authorization: key=' . $YOUR_API_KEY,
                                'apns-push-type: alert'
                            ];
                            
                            $profile = $modx->getObject('modUserProfile', ['internalKey' => $order->user_id]);
                            $firebase_token = $profile->get('city');
                            //$firebase_token = "dg_onD6-L0ZeqZN_UKzod1:APA91bG2t1RzYHZqgeQgQbZXWQwdZ6g9_1141WDeMRpBlN2a4aBNqp4OstJHOkufLKzl1VUY_c46m0dPD8M1TPh-GHmiYEZpNG0kPw9i7iXPbGfoCCpFQq8gVk4586gCQgG5NOgWWiNB";
                            $request_body1['registration_ids'] = [$firebase_token]; 
                            $request_body1['notification'] = array("title" => "Заказ #".$order->id." оформлен.", "body" => "Заказ отправлен на кухню.");
                            $request_body1['data'] = array(
                                "type" => "alert", 
                                "data" => array(
                                    "title" => "Banners", 
                                    "body" => "test",         
                                    "type" => "change_order_status", 
                                    "order_id" => $order->id),
                                "mandatory" => true
                            );
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                            $response = curl_exec($ch);
                            curl_close($ch);
            }
            //$modx->log(modX::LOG_LEVEL_ERROR, 'id_integration_order: '.$id_integration_order);
            $id_integration_order = $contacts->get("id_integration_order");
            if (!$id_integration_order){
                $thisid = $order->id;
                $order->set("status",8);
                $order->save();
        $modx->getService('mail', 'mail.modPHPMailer');
        $modx->mail->set(modMail::MAIL_FROM, $modx->getOption('emailsender'));
        $modx->mail->set(modMail::MAIL_FROM_NAME, $modx->getOption('site_name'));
        $mails =  $modx->getOption("ms2_email_manager");
        $mails = explode(",",$mails);
        foreach ($mails as $mail){
            /*Адрес получателя нашего письма*/
            $modx->mail->address('to', $mail);
            
            /*Заголовок сообщения*/
            $modx->mail->set(modMail::MAIL_SUBJECT, "App ЯМ | Заказ №$thisid не был отправлен в IIKO.");
            
            /*Подставляем чанк с телом письма (предварительно его нужно создать)*/
            $modx->mail->set(modMail::MAIL_BODY, "<h1>Заказ №$thisid не был отправлен в IIKO.</h1>
            <p>Ввиду отсутствия доступа к IIKO заказ №$thisid не был отправлен IIKO. <br>  Обработать заказ в панели администрирования: http://ym.appsj.su/manager/?a=mgr/orders&namespace=minishop2&order=$thisid<br>Для устранения причины - обратитесь к обслуживающей организации.</p>");
            
            /*Отправляем*/
            $modx->mail->setHTML(true);
            if (!$modx->mail->send()) {
                $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
            }
            $modx->mail->reset();
            
                }
            }
        }
    
    } else {
        $modx->log(modX::LOG_LEVEL_ERROR, 'заказ с тестового аккаунта');
                $order->set("status",4);
            $order->save();
    }
    //$miniShop2->changeOrderStatus($order["modx_order"], 6);
}