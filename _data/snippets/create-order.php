id: 134
source: 1
name: create-order
properties: 'a:0:{}'

-----

//return json_encode($_SESSION["minishop2"]);
$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$url = str_replace('http://','',$url);
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();
                    //$scriptProperties = array(
                    //'status' => 14
                    //);
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        if ($profile = $modx->getObject('modUserProfile', ['website' => $token])){
            // проверяем настройку связанною с работой чёрного списка
            if($modx->getOption("app_black_list")) {
                $checkBlackList = $profile->get("black_list_type");
                switch($checkBlackList){
                    case 1:
                        $messageBlackList = "Вы не можете сделать заказ, вас добавили в чёрный список";
                    break;
                    case 2:
                        $messageBlackList = "Вам доступна только онлайн оплата";
                    break;
                    case 3:
                        $messageBlackList = "Ваш заказ принят, свами свяжутся";
                    break;
                }
            } else {
                $checkBlackList = 0;
            }
            // проверка чёрного списка
            if($checkBlackList != 1) {
                
                //проверка способа оплаты, если включён чёрный список
                if($_POST['JSON']['payment_type'] == 1 || $checkBlackList == 0) {
            
                    /*if ($_POST['JSON']['payment_type'] == 1){
                        $res = $modx->getObject('modResource',$_POST['JSON']['id_org']);
                        $merchant = $res->getTVValue('merchantID');
                        $params = array(
                            'command' => 'v',
                            'amount' => 1*100, //пока оставим рубль
                            'description' => "Новый заказ",
                            'currency' => 643,
                            'ecomm_payment_timeout' =>300,
                            'client_ip_addr'=>"5.101.157.102");
                        $request = http_build_query($params);
                        $curlOptions = array(
                            CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
                            CURLOPT_CONNECTTIMEOUT => 5,
                            CURLOPT_TIMEOUT => 35,
                            CURLOPT_HEADER => false,
                            CURLOPT_POST => true,
                            CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_SSL_VERIFYHOST => 2,
                            CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
                            CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
                            CURLOPT_SSL_VERIFYPEER => true,
                            CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
                            CURLOPT_SSLVERSION => 6,
                            CURLOPT_POSTFIELDS => $request,
                        );
                            $ch = curl_init();
                            curl_setopt_array($ch, $curlOptions);
                            $response = curl_exec($ch);
                            if (curl_errno($ch)) {
                                $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
                            }
                            curl_close($ch);
                            $trans_id = str_replace("TRANSACTION_ID: ", "", $response);
                            $paymentURL = "https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=".$trans_id;
                            $json['data'][]['paymentURL'] = $paymentURL;
                            $json['data'][]['merchant'] = $merchant;
                            $profile->set("last_trans_id", $trans_id);
                            $profile->save();
                            return json_encode($json);
                    }*/
                    $city = $_POST['JSON']['city']; //город
                    $id_restourant = $_POST['JSON']['id_org']; //id ресторана
                    $flat_num = $_POST['JSON']['flat_num']; //номер квартиры
                    $house_num = $_POST['JSON']['house_num']; //номер дома
                    $floor = $_POST['JSON']['floor']; //этаж
                    $entrance = $_POST['JSON']['entrance']; //подхезд
                    $table_num = $_POST['JSON']['table_num']; //номер столика
                    $money = $_POST['JSON']['money']; //с какой купюоы сдавать
                    $person = $_POST['JSON']['person']; // добавление количества персон
                    if($_POST['JSON']['name'] != ''){
                        $name = $_POST['JSON']['name'];
                    }else{
                        $name = $profile->get('fullname');
                    }
        
                    $phone = $_POST['JSON']['phone'];
                    
                    $comment = $_POST['JSON']['comment'];
                    $promo =  $_POST['JSON']['doorphone'];
        
                    if ($_POST['JSON']['bonus'] != 0) {
                        $bonus_count_write =  $_POST['JSON']['bonus'];
                    }
                    if (!$profile->get("fullname")){
                        $profile->set("fullname", $_POST['JSON']['name']);
                    }
                    
                    $profile->save();
                    $payment = $_POST['JSON']['payment_type'];
                    /*------ЭТО КОСТЫЛЬ ЕГО ПОТОМ НАДО БУДЕТ УБРАТЬ----------*/
                    /*if ($_POST['JSON']['order_type'] == 2){
                        $_POST['JSON']['order_type'] = 1;
                    } else {
                        $_POST['JSON']['order_type'] = 2;
                    }
                    $delivery = $_POST['JSON']['order_type'];*/
                    /*------КОСТЫЛЬ КОНЕЦ----------*/
                    $delivery = $_POST['JSON']['order_type'];
                    $address = $_POST['JSON']['street'];
                    if ($delivery == 2){
                        $restourant_id = 0;
                    }
                    if ($restourant_id !=0){
                        $sql = "SELECT value
                        FROM modx_site_tmplvar_contentvalues
                        WHERE tmplvarid = 10 and contentid = 9";
                        $order__sql = $modx->query($sql);
                        $orders = $order__sql->fetchAll(PDO::FETCH_ASSOC);
                        //echo json_encode($orders[0]);
                        $orders = $orders[0];
                        foreach ($orders as $num =>$order){
                            $order = json_decode($order, true);
                            for ($i= 0; $i < count($order); $i++){
                                if ($order[$i]["MIGX_id"] == $restourant_id){
                                    $address = $order[$i]['address'];
                                }
                            }
                        }
                    }
                    if(count($_POST['JSON']['geo']) > 0) {
                        /*$region = '
                        
                        <a href="https://maps.yandex.ru/?text='.$_POST['JSON']['geo']['lat'].'+'.$_POST['JSON']['geo']['lon'].'">
                        Ссылка на яндекс карту
                        </a>
                        ';*/
                        $region = $_POST['JSON']['geo']['lat'].','.$_POST['JSON']['geo']['lon'];
                    
                    }
                    $miniShop2 = $modx->getService('minishop2','miniShop2', MODX_CORE_PATH . 'components/minishop2/model/minishop2/', $scriptProperties);
                    if (!($miniShop2 instanceof miniShop2)) return '';
                    $miniShop2->initialize($modx->context->key, $scriptProperties);
                    if (!$name){
                        $name = "user".$phone;
                    }
                    $email = $_POST['JSON']['email'];
                    if (!$email){ //если передается пустой email, то генерим почту и вписываем в профиль
                        //$email = $phone."@mail.ru";
                        $profile->set("email", $phone."@apksite.ru");
                        $profile->save();
                        $email = $profile->get("email");
                    } else { //иначе вставляем в профиль передаваемый емэйл и передаем его в заказ
                        $profile->set("email", $email);
                        $profile->save();
                        $email = $profile->get("email");  
                    }
                    if (!$city){
                        $sql = "SELECT parent FROM modx_site_content WHERE id = '$id_restourant'";
                        $info_user = $modx->query($sql);
                        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
                        $res_city = $modx->getObject("modResource", $info_user[0]["parent"]);
                        $city = $res_city->get("pagetitle");
                    }
                    $miniShop2->order->add('receiver',$name); // Указываем имя получателя
                    $miniShop2->order->add('phone',$phone);
                    $miniShop2->order->add('email',$email);
                    $miniShop2->order->add('payment', $payment); //Указываем способ оплаты
                    $miniShop2->order->add('delivery', $delivery); //Указываем способ доставки
                    $miniShop2->order->add('street', $address);
                    $miniShop2->order->add('region', $region);
                    $miniShop2->order->add('room', $money);
                    $miniShop2->order->add('building', $table_num);
                    $miniShop2->order->add('comment', $comment);
                    $miniShop2->order->add('metro', $promo);
                    $miniShop2->order->add('flat_num', $flat_num);
                    $miniShop2->order->add('house_num', $house_num);
                    $miniShop2->order->add('floor', $floor);
                    $miniShop2->order->add('entrance', $entrance);
                    $miniShop2->order->add('city', $city);
                    $miniShop2->order->add('index', $id_restourant);
                    $miniShop2->order->add('referalDiscount', $_SESSION['minishop2']['promocodes']);
                    $miniShop2->order->add('discount', $_SESSION['minishop2']['actual_discount']);
                    $miniShop2->order->add('person', $person);
                    $cart = $miniShop2->cart->get();
                    if (!$cart){
                        $json['data'][] = array("error"=>"В корзине нет товаров");
                        return json_encode($json);
                    }
                    foreach ($cart as $item){
                        for ($i=0;$i<$item['count'];$i++){
                            foreach ($item['options']['childs']['additives'] as $additive){
                                $miniShop2->cart->add($additive['id'],$additive['count']);
                            }
                            foreach ($item['options']['childs']['type'] as $type_additive){
                                $miniShop2->cart->add($type_additive['id'],$type_additive['count']);
                            }
                        }
                    }
                    foreach ($_SESSION['minishop2']['cart'] as $key=> $item){
                        if($modx->getOption("app_pricy_type") == "userprice"){
                            $sql = 'SELECT state FROM modx_user_attributes WHERE website = "'.$token.'"';
                            $output = $modx->query($sql);
                            $price_type = $output->fetch(PDO::FETCH_ASSOC)['state'];
                        
                            if (!$price_type){
                                $price_type = $modx->getOption("add_default_type_price");
                            }
                        } else {
                            switch($delivery){
                                case 1:
                                    $tvType = 37;
                                break;
                                case 2:
                                    $tvType = 41;
                                break;
                                default:
                                    $tvType = 40;
                                break;
                            }
                            $sql = '
                                SELECT 
                                    value
                                FROM 
                                    modx_site_tmplvar_contentvalues 
                                WHERE 
                                    tmplvarid = '.$tvType.' AND contentid = '.$_POST['JSON']["id_org"];
                                        
                            $output = $modx->query($sql);
                            $city = $output->fetch(PDO::FETCH_ASSOC)['value'];
                            $price_type = ($city)? $city : 1;
                        }
                        
                        //return "id_org: ".$_POST['JSON']["id_org"].", и тип цены: ".$price_type." (".$tvType.")";
                        
                        $sql = "
                            SELECT 
                                price
                            FROM 
                                modx_softjetsync_prices
                            WHERE 
                                modx_softjetsync_prices.product_id = ".$item["id"]." AND modx_softjetsync_prices.type = '$price_type'
                            ";
                        
                        $info_product = $modx->query($sql);
                        $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                        
                        $_SESSION['minishop2']['cart'][$key]['price'] = $info_product[0]["price"];
                    }
                    //$newcart = $miniShop2->cart->get();
                    //echo json_encode($newcart);
                    $order_id = $miniShop2->order->submit();
                    unset($_SESSION['minishop2']['actual_discount']);
                    unset($_SESSION['minishop2']['promocodes']);
                    unset($_SESSION['minishop2']['discount_cost']);
                    unset($_SESSION['free_nabor']);
                    if ($payment == 1){
                        $res = $modx->getObject('modResource',$id_restourant);
                        $merchant = $res->getTVValue('merchantID');
                        $ord = $modx->getObject('msOrder', $order_id);
                        $params = array(
                            'command' => 'v',
                            'amount' => $ord->get('cost')*100,
                            'description' => 'Заказ #' .$order_id,
                            'currency' => 643,
                            'ecomm_payment_timeout' =>300,
                            'client_ip_addr'=>"5.101.157.102");
                        $request = http_build_query($params);
                        $curlOptions = array(
                            CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
                            CURLOPT_CONNECTTIMEOUT => 5,
                            CURLOPT_TIMEOUT => 35,
                            CURLOPT_HEADER => false,
                            CURLOPT_POST => true,
                            CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_SSL_VERIFYHOST => 2,
                            CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
                            CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
                            CURLOPT_SSL_VERIFYPEER => true,
                            CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
                            CURLOPT_SSLVERSION => 6,
                            CURLOPT_POSTFIELDS => $request,
                        );
                            $ch = curl_init();
                            curl_setopt_array($ch, $curlOptions);
                            $response = curl_exec($ch);
                            if (curl_errno($ch)) {
                                $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
                            }
                            curl_close($ch);
                            $trans_id = str_replace("TRANSACTION_ID: ", "", $response);
                            $paymentURL = "https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=".$trans_id;
                            $json['data'][]['paymentURL'] = $paymentURL;
                            $json['data'][]['merchant'] = $merchant;
                            $order = $modx->getObject("msOrder", $order_id);
                            $current_address = $modx->getObject('msOrderAddress', $order->get("address"));
                            $current_address->set("trans_id", $trans_id);
                            $current_address->save();
                    }
                    if ($payment == 6){
                        $res = $modx->getObject('modResource',$id_restourant);
                        $merchant = $res->getTVValue('merchantID');
                        $ord = $modx->getObject('msOrder', $order_id);
                        $this_user_id = $ord->get("user_id");
                        $sql = "SELECT * FROM modx_user_cards WHERE modx_user_cards.merchant_id = '$merchant' AND modx_user_cards.user_id = '$this_user_id' AND modx_user_cards.status = 1";
                        $user_cards = $modx->query($sql);
                        $user_cards = $user_cards->fetchAll(PDO::FETCH_ASSOC);
                        if ($user_cards){ //если уже зарегестрирован
                            $json["rsb_msg"] = "Зарегестрирован, списываем";
                            $json["user_card"] = $user_cards;
                            $params = array(
                                'command' => 'e',
                                //'amount' => 1*100,
                                'amount' => $ord->get('cost')*100,
                                'description' => 'Заказ #' .$order_id,
                                'currency' => 643,
                                'language' => "ru",
                                'biller_client_id'=> (int)$user_cards[0]["biller_client_id"],
                                'client_ip_addr'=>"5.101.157.102");
                                
                            $request = http_build_query($params);
                            $curlOptions = array(
                                CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
                                CURLOPT_CONNECTTIMEOUT => 5,
                                CURLOPT_TIMEOUT => 35,
                                CURLOPT_HEADER => false,
                                CURLOPT_POST => true,
                                CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_SSL_VERIFYHOST => 2,
                                CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
                                CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
                                CURLOPT_SSL_VERIFYPEER => true,
                                CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
                                CURLOPT_SSLVERSION => 6,
                                CURLOPT_POSTFIELDS => $request,
                            );
                                $ch = curl_init();
                                curl_setopt_array($ch, $curlOptions);
                                $response = curl_exec($ch);
                                if (curl_errno($ch)) {
                                    $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
                                }
                                curl_close($ch);
                                $json["respos"] = $response;
                                //$string = "TRANSACTION_ID: G7R39wwbwHSZWl3iouJiHBBveuE=\nRESULT: OK\nRESULT_CODE: 000\nRRN: 101209552869\nAPPROVAL_CODE: 276348";
                                $string = $response;
                                $string = explode("\n", $response);
                                $trans_id = str_replace("TRANSACTION_ID: ", "", $string[0]);
                                $order = $modx->getObject("msOrder", $order_id);
                                $current_address = $modx->getObject('msOrderAddress', $order->get("address"));
                                if ($trans_id != "error: Perspayee data expired or missing"){
                                    $current_address->set("trans_id", $trans_id);
                                    $current_address->save();   
                                } else { //если дата истекла
                                    $this_biller_client_id = $user_cards[0]["biller_client_id"];
                                    $sql = "DELETE FROM `modx_user_cards` WHERE `modx_user_cards`.`biller_client_id` = '$this_biller_client_id' AND `modx_user_cards`.merchant_id = '$merchant'";
                                    $modx->query($sql);
                                    $res = $modx->getObject('modResource',$id_restourant);
                                    $merchant = $res->getTVValue('merchantID');
                                    $ord = $modx->getObject('msOrder', $order_id);
                                    $this_user_id = $ord->get("user_id");
                                    $biller_cliend_id = rand(100000, 999999);
                                    $json["rsb_msg"] = "не зареган, регаем";
                                    $sql = "INSERT INTO `modx_user_cards` (`user_id`, `order_id`, `merchant_id`, `status`, `biller_client_id`) VALUES ('$this_user_id', '$order_id', '$merchant', '0', '$biller_cliend_id')";
                                    $modx->query($sql);
                                    $json["rsb_sql"] = $sql;
                                    $params = array(
                                        'command' => 'z',
                                        //'amount' => 1*100,
                                        'amount' => $ord->get('cost')*100,
                                        'description' => 'Заказ #' .$order_id,
                                        'currency' => 643,
                                        'perspayee_expiry' =>"0321",
                                        'ask_save_card_data'=>true,
                                        'biller_client_id' =>$biller_cliend_id,
                                        'perspayee_gen'=>1,
                                        'client_ip_addr'=>"5.101.157.102");
                                    $request = http_build_query($params);
                                    $curlOptions = array(
                                        CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
                                        CURLOPT_CONNECTTIMEOUT => 5,
                                        CURLOPT_TIMEOUT => 35,
                                        CURLOPT_HEADER => false,
                                        CURLOPT_POST => true,
                                        CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
                                        CURLOPT_RETURNTRANSFER => true,
                                        CURLOPT_SSL_VERIFYHOST => 2,
                                        CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
                                        CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
                                        CURLOPT_SSL_VERIFYPEER => true,
                                        CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
                                        CURLOPT_SSLVERSION => 6,
                                        CURLOPT_POSTFIELDS => $request,
                                    );
                                        $ch = curl_init();
                                        curl_setopt_array($ch, $curlOptions);
                                        $response = curl_exec($ch);
                                        if (curl_errno($ch)) {
                                            $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
                                        }
                                        curl_close($ch);
                                        $trans_id = str_replace("TRANSACTION_ID: ", "", $response);
                                        $paymentURL = "https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=".$trans_id;
                                        $json['data'][]['paymentURL'] = $paymentURL;
                                        $json['data'][]['response'] = $response;
                                        $json['data'][]['merchant'] = $merchant;
                                        $order = $modx->getObject("msOrder", $order_id);
                                        $current_address = $modx->getObject('msOrderAddress', $order->get("address"));
                                        $current_address->set("trans_id", $trans_id);
                                        $current_address->save();
                                }
                        } else {
                            $res = $modx->getObject('modResource',$id_restourant);
                            $merchant = $res->getTVValue('merchantID');
                            $ord = $modx->getObject('msOrder', $order_id);
                            $this_user_id = $ord->get("user_id");
                            $biller_cliend_id = rand(100000, 999999);
                            $json["rsb_msg"] = "не зареган, регаем";
                            $sql = "INSERT INTO `modx_user_cards` (`user_id`, `order_id`, `merchant_id`, `status`, `biller_client_id`) VALUES ('$this_user_id', '$order_id', '$merchant', '0', '$biller_cliend_id')";
                            $modx->query($sql);
                            $json["rsb_sql"] = $sql;
                            $params = array(
                                'command' => 'z',
                                //'amount' => 1*100,
                                'amount' => $ord->get('cost')*100,
                                'description' => 'Заказ #' .$order_id,
                                'currency' => 643,
                                'perspayee_expiry' =>"0321",
                                'ask_save_card_data'=>true,
                                'biller_client_id' =>$biller_cliend_id,
                                'perspayee_gen'=>1,
                                'client_ip_addr'=>"5.101.157.102");
                            $request = http_build_query($params);
                            $curlOptions = array(
                                CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
                                CURLOPT_CONNECTTIMEOUT => 5,
                                CURLOPT_TIMEOUT => 35,
                                CURLOPT_HEADER => false,
                                CURLOPT_POST => true,
                                CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_SSL_VERIFYHOST => 2,
                                CURLOPT_SSLKEY => "/home/s/sjeda/".$url."/ssl/".$merchant.".key",
                                CURLOPT_SSLCERT =>"/home/s/sjeda/".$url."/ssl/".$merchant.".pem",
                                CURLOPT_SSL_VERIFYPEER => true,
                                CURLOPT_CAINFO => "/home/s/sjeda/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
                                CURLOPT_SSLVERSION => 6,
                                CURLOPT_POSTFIELDS => $request,
                            );
                                $ch = curl_init();
                                curl_setopt_array($ch, $curlOptions);
                                $response = curl_exec($ch);
                                if (curl_errno($ch)) {
                                    $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
                                }
                                curl_close($ch);
                                $trans_id = str_replace("TRANSACTION_ID: ", "", $response);
                                $paymentURL = "https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=".$trans_id;
                                $json['data'][]['paymentURL'] = $paymentURL;
                                $json['data'][]['response'] = $response;
                                $json['data'][]['merchant'] = $merchant;
                                $order = $modx->getObject("msOrder", $order_id);
                                $current_address = $modx->getObject('msOrderAddress', $order->get("address"));
                                $current_address->set("trans_id", $trans_id);
                                $current_address->save();
                        }
                        
                        }
                    $json['data'][] = array("order_id"=>$order_id);
                    $miniShop2->cart->clean();
                    echo json_encode($json);
                } else {
                    $json['data'][] = array("error"=>$messageBlackList);
                    echo json_encode($json);
                    http_response_code(401);
                }
            
            } else {
                $json['data'][] = array("error"=>$messageBlackList);
                echo json_encode($json);
                http_response_code(401);
            }
        } else {
                $json['data'][] = array("error"=>"Неверный токен");
                echo json_encode($json);
                http_response_code(401);
        }
    }
}