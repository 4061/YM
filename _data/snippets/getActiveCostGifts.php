id: 261
source: 1
name: getActiveCostGifts
properties: 'a:0:{}'

-----

$pdoFetch = new pdoFetch($modx);
$gifts = array(
    'class' => 'PromoBase',
    'sortdir' => "desc",
    'return' => 'data',
    'select' => ["id_promo", "activity", "image", "date_start", "date_end", "availability", "name", "description", "id_product", "value", "products_to_gift"],
    'where' => ["type"=>4],
    'limit' => 10000
);
$pdoFetch->setConfig($gifts);
$gifts = $pdoFetch->run();
$gift_products = array(
    'class' => 'modResource',
    'sortdir' => "desc",
    'return' => 'data',
    'select' => ["id", "pagetitle"],
    'where' => ["parent"=>1739],
    'limit' => 10000
);
$pdoFetch->setConfig($gift_products);
$gift_products = $pdoFetch->run();
$gift_products = array_combine(array_column($gift_products, 'id'), $gift_products);
$master_ids = array_column($gift_products, 'id');
foreach ($gifts as $key=> $gift){
    $gifts[$key]["id_product"] = explode(",", $gift["id_product"]);
    foreach ($gift_products as $gift_product){
        if (in_array($gift_product["id"], $gifts[$key]["id_product"])){
            $product = $gift_product;
            $product["active"] = 1;
            $gifts[$key]["gifts"][] = $product;
        } else {
            $product = $gift_product;
            $product["active"] = 0;
            $gifts[$key]["gifts"][] = $product;
        }
    }
}
return($gifts);