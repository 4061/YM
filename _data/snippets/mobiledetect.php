id: 263
source: 1
name: mobiledetect
properties: 'a:0:{}'

-----

function mobile_detect() {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $ipod = strpos($user_agent,"iPod");
    $iphone = strpos($user_agent,"iPhone");
    $android = strpos($user_agent,"Android");
    $symb = strpos($user_agent,"Symbian");
    $winphone = strpos($user_agent,"WindowsPhone");
    $wp7 = strpos($user_agent,"WP7");
    $wp8 = strpos($user_agent,"WP8");
    $operam = strpos($user_agent,"Opera M");
    $palm = strpos($user_agent,"webOS");
    $berry = strpos($user_agent,"BlackBerry");
    $mobile = strpos($user_agent,"Mobile");
    $htc = strpos($user_agent,"HTC_");
    $fennec = strpos($user_agent,"Fennec/");
    $nokia = strpos($user_agent,"Nokia");
    if ($android){
        header('Location: https://play.google.com/store/apps/details?id=com.pizza_house');
    } elseif ($iphone){
        header('Location: https://apps.apple.com/us/app/pizza-house-core/id1573912500');
    } else {
        return false;
    }
}
mobile_detect();