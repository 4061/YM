id: 63
source: 1
name: cart-info
properties: 'a:0:{}'

-----

$sql = "SELECT id,name,price
FROM modx_ms2_deliveries
WHERE active = 1 ORDER BY rank ASC";
$deliveries__str= $modx->query($sql);
$deliveries = $deliveries__str->fetchAll(PDO::FETCH_ASSOC);


$sql = "SELECT id,name
FROM modx_ms2_payments
WHERE active = 1 ORDER BY rank ASC";
$payments__str= $modx->query($sql);
$payments = $payments__str->fetchAll(PDO::FETCH_ASSOC);



$sql = "SELECT value
FROM modx_site_tmplvar_contentvalues
WHERE tmplvarid = 10 and contentid = 9";
$сontacts__str= $modx->query($sql);
$сontacts = $сontacts__str->fetchAll(PDO::FETCH_ASSOC);

$сontacts__all = json_decode($сontacts[0]['value']);

/*
echo "<pre>";
print_r($сontacts__all);
echo "</pre>";
*/

$сontacts__a = [];

$contacts_array = [];

foreach ($сontacts__all as $сontact) {
    $lon = $сontact->lon;
    $lat = $сontact->lat;
   // var_dump($lon);
  //  exit;
    $contacts_array[] = array("id" => $сontact->MIGX_id, "phone" => $modx->config['phone'], "address" => $сontact->address, "coord" => array("longitude" => round($lon, 6), "latitude" => round($lat, 6)), "email" => $сontact->email);
    $adresses_array[] = array("id" => $сontact->MIGX_id, "address" => $сontact->address, "coord" => array("longitude" => round($lon, 6), "latitude" => round($lat,6)) );
}


$deliveries__a = [];
$payments__a = [];

$paymants_array = [];

// foreach ($payments as $payment) {
    
// $payments__a[] = '
//             {
//                 "id":'.$payment['id'].',
//                 "name":"'.$payment['name'].'"
//             }';

// $paymants_array[] = $payment;
    
// }


$deliveries_array = [];

// foreach ($deliveries as $deliverie) {
    
// $deliveries__a[] = '
//             {
//                 "id":'.$deliverie['id'].',
//                 "name":"'.$deliverie['name'].'",
//                 "price":'.$deliverie['price'].'
//             }';
    
// }
$org_info_card = $modx->config['organization_card'];

$org_info_card = str_replace("\t", " ", $org_info_card);

$pieces = explode("
", $org_info_card);
$i = 0;
$string_org_info = '';
foreach ($pieces as $piace)
{
    $string_org_info = $string_org_info.$piace;
    if ($i != 0) $org_info .= ',';
    $org_info .= '"'.$piace.'"';
    $i++;
}

$delivery_settings_array = [];

$delivery_settings_array[] = array("text_disabled" => $modx->config['text_delivery_disabled'],
"worktime_from" => $modx->config['delivery_worktime_from'],
"worktime_to" => $modx->config['delivery_worktime_to']);

$cart_info[] = [
       // "payments" => $payments,
        //"deliveries" => $deliveries,
       // "contacts" => array($contacts_array[0]),
        //"addresses" => $adresses_array,
        "instagram" => $modx->config['instagram'],
        "facebook"=> $modx->config['facebook'],
        "organization_card" => $pieces,
        "vk" => $modx->config['vk'],
        "whats" => $modx->config['whats'], //ватсап
        "qr_tag" =>$modx->config['qr'],
        "phone" =>$modx->config['phone'],
        "delivery_settings" => $delivery_settings_array,
        "privacy_policy" => $modx->config['privacy_policy'],
        "return_warranty" => $modx->config['return_warranty'],
        "price_free" => $modx->getOption('free_delivery_price'),
        "price_minimal" => $modx->getOption('price_minimal'),
        "work_with_us" =>$modx->getOption('work_with_us'), //ссылка работать у нас
        ];
$result = [];
$result["data"] = $cart_info;
return json_encode($cart_info, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);