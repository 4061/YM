id: 136
source: 1
name: cart-change-count-product
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $product_key = $_POST['JSON']['key']; //key продукта
        $count = $_POST['JSON']['count']; //обновленное количество
        $ms2 = $modx->getService('miniShop2');
        $ms2->initialize($modx->context->key);
        $cart = $ms2->cart->get();
        $cart = $ms2->cart->change($product_key, $count); //применяем
        /*
        if ($this_product_info["value"]){
            if ($this_product_info["value"]<$_SESSION["minishop2"]["cart"][$product_key]["count"]){
                $cart = $ms2->cart->change($product_key, $count); //применяем
                $json["ident"] = true;
            }
        } else {
            $cart = $ms2->cart->change($product_key, $count); //применяем
        }
        $json["casdc"] = $this_product_info;
        */
        $total_cost = 0;
        $total_count = 0;
        foreach ($_SESSION['minishop2']['cart'] as $cart_item){
            $total_cost_product = $cart_item['total_cost']*$cart_item['count'];
            $total_cost = $total_cost + $total_cost_product;
            $total_count = $total_count + $cart_item['count'];
        }
        $_SESSION['minishop2']['total_cost'] = $total_cost;
        $_SESSION['minishop2']['total_count'] = $total_count;
        $json['promocodes'] = $_SESSION['minishop2']['promocodes'];
        if($_SESSION['minishop2']['cart']){
            $cart_items = $_SESSION['minishop2']['cart'];
            foreach ($cart_items as $key=> $cart_item){
                $cart_items[$key]['key'] = $key;
            }
            $cart_items = array_values($cart_items);
            $json['products'] = $cart_items;
        } else {
            $json['products'] = array();
        }
        
        $sql = "SELECT * FROM modx_promo_base WHERE type = 4";
        $thresholds = $modx->query($sql);
        $thresholds = $thresholds->fetchAll(PDO::FETCH_ASSOC);
        foreach ($thresholds as $threshold){
            $arr = [];
            $items = explode(",", $threshold['id_product']);
            $arr["min_count"] = (int)$threshold["value"];
            $arr["title"] = $threshold["name"];
            $arr["imgUrl"] = $threshold["image"];
            $arr["desc"] = $threshold["description"];
            $arr["id_promo"] = $threshold["id_promo"];
            foreach ($items as $item){
                $current_item = [];
                $res = $modx->getObject('modResource', $item);
                $current_item = array(
                    "title" => $res->get('pagetitle'),
                    "imgUrl" => $res->get('image'),
                    "price" => $res->get('price'),
                    "id" => $res->get('id'),
                );
            if ($res->get('price') == 0){
                $current_item['max_gift'] = 1;
            }
            $arr["modificators"][] = $current_item;
            $json['gift_from_cost'][] = $arr;
            }
        }
        $json['gift_from_cost'] = array_combine(array_column($json['gift_from_cost'], 'id_promo'), $json['gift_from_cost']);
        $json['gift_from_cost'] = array_values($json['gift_from_cost']);
        foreach ($json['gift_from_cost'] as $gift){
            if ($_SESSION['minishop2']['total_cost']<$gift['min_count']){
                foreach ($gift['modificators'] as $gift_product){
                    foreach ($json['products'] as $cart_prod){
                        if ($cart_prod["id"] == $gift_product["id"]){
                            $cart = $ms2->cart->change($cart_prod["key"], 0);
                            $modx->log(modX::LOG_LEVEL_ERROR, 'nfsuiop');
                        } 
                    }
                }
            }
        }
        //unset($json['products']);
        if($_SESSION['minishop2']['cart']){
            $cart_items = $_SESSION['minishop2']['cart'];
            foreach ($cart_items as $key=> $cart_item){
                $cart_items[$key]['key'] = $key;
                $sql = "SELECT value FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.product_id = {$cart_item['id']} AND modx_ms2_product_options.key = 'max_count'";
                $max_product_count = $modx->query($sql);
                $max_product_count = $max_product_count->fetchAll(PDO::FETCH_ASSOC);
                $max_product_count = ($max_product_count[0]['value']);
                if ($max_product_count){
                    $cart_items[$key]['max_count'] = (int)$max_product_count;
                } else {
                    $cart_items[$key]['max_count'] = 999999;
                }
                $res = $modx->getObject('modResource',$cart_item["id"]);
                $cart_items[$key]["img_url"] = $res->get('image');
                $cart_items[$key]["title"] = $res->get('pagetitle');
                $cart_items[$key]["desc"] = $res->get('description');
            }
            $cart_items = array_values($cart_items);
            $json['products'] = $cart_items;
        } else {
            $json['products'] = array();
        }
        //unset($json['gift_from_cost']);
        $json['total_cost'] = $_SESSION['minishop2']['total_cost'];
        $json['total_count'] = $_SESSION['minishop2']['total_count'];
        $PH_PROGRAM = $modx->getOption('loyalty_PH_program');
        if ($PH_PROGRAM){
/*------Расчет скидки---------*/
        $profile = $modx->getObject('modUserProfile', ['website' => $token]);
        
        $PH["OrganizationId"] = "7580c84f-3d89-ddd4-0175-26daf8845f1b";
        $PH["ParentOrderId"] = "123";
        $PH["OrderId"] = "123";
        $PH["ClientId"] = $profile->get("zip");
        $PH["OrderNumber"] = 123;
        $PH["OrderSumWithDiscount"] = $total_cost;
        $PH["OrderSum"] = $total_cost;
        $PH["Order"] = [];
        foreach ($_SESSION['minishop2']['cart'] as $prod){
            $res = $modx->getObject('modResource',$prod["id"]);
            $prodid = $prod["id"];
            $sql = "SELECT modx_ms2_product_options.value FROM modx_ms2_product_options WHERE modx_ms2_product_options.product_id = '$prodid' AND modx_ms2_product_options.key='guid'";
            $guid = $modx->query($sql);
            $guid = $guid->fetchAll(PDO::FETCH_ASSOC);
            $PH["Order"][] = array(
            "ProductId" => $guid[0]['value'],
            "ProductName" => $res->get('pagetitle'),
            "CategoryId" => "",
            "Amount" => $prod['count'],
            "Price" => $prod["price"],
            "PriceWithDiscount" => $prod["price"],
            "CategoryName" => "",
            );
        }
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://cabinet.prime-hill.com/api/iiko/v3/GetDiscount/9e4-b57d-77a9-4da",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($PH),
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 34e4247a-7f83-4486-af51-d1a52c9f5015"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        $response = json_decode($response, true);
        /*------Расчет скидки конец---------*/
        }
        $discount_cost = 0;
        $total_cost = 0;
        if ($_SESSION["order_type"] == 1){
            $json["PrintMessage"] = "Стоимость при самовывозе со скидкой 20% (скидка не действует на Напитки).";
            foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                $res = $modx->getObject("modResource", $cart_item['id']);
                $discount_cost = $discount_cost + ($res->get("price2")*$cart_item["count"]);
                $total_cost = $total_cost + $res->get("price");
                if ($cart_item['options']['childs']["additives"] != []){
                    foreach ($cart_item['options']['childs']["additives"] as $add){
                        $res = $modx->getObject("modResource", $add['id']);
                        $discount_cost = $discount_cost + ($res->get("price2")*$cart_item["count"]);
                        $total_cost = $total_cost + $res->get("price");
                    }
                }
            }
        } elseif ($_SESSION["order_type"] == 2){
            foreach ($_SESSION['minishop2']['cart'] as $cart_item){
                $res = $modx->getObject("modResource", $cart_item['id']);
                $discount_cost = $discount_cost + ($res->get("price")*$cart_item["count"]);
                $total_cost = $total_cost + $res->get("price");
                if ($cart_item['options']['childs']["additives"] != []){
                    foreach ($cart_item['options']['childs']["additives"] as $add){
                        $res = $modx->getObject("modResource", $add['id']);
                        $discount_cost = $discount_cost + ($res->get("price")*$cart_item["count"]);
                        $total_cost = $total_cost + $res->get("price");
                    }
                }
            }
        }
        if ($_SESSION['minishop2']['promocodes']){
            //$modx->runSnippet('check-code');
            $code = $_SESSION['minishop2']['promocodes'];
            $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
            $current_code = $modx->query($sql);
            $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
            
            if ($current_code[0]['type'] == 3){
                $costfourth = $_SESSION['minishop2']['total_cost'] * 0.25;
                if ($costfourth>=200){
                    $actual_discount = 200;
                } else {
                    $actual_discount = $costfourth;
                }
                $discount_cost = $_SESSION['minishop2']['total_cost'] - $actual_discount;
                $_SESSION['minishop2']['discount_cost'] = $discount_cost;
                $_SESSION['minishop2']['actual_discount'] = $actual_discount;
            }
            if ($current_code[0]['type'] == 1){
                $discount_percent = $current_code[0]['child_discount']/100;
                $actual_discount = round($_SESSION['minishop2']['total_cost']*$discount_percent);
                $discount_cost = round($_SESSION['minishop2']['total_cost'] - $actual_discount);
                $_SESSION['minishop2']['discount_cost'] = $discount_cost;
                $_SESSION['minishop2']['actual_discount'] = $actual_discount;
            }
        }
        $json["actual_discount"] = $actual_discount;
        $json['discount_cost'] = $discount_cost; //цена со скидкой
        $json["SumDiscount"] = $response["SumDiscount"]; //сумма скидкой из ПХ  
        $json["BonusBalance"] = $response["BonusBalance"]; //бонусный баланс
        $json["MaxBonus"] = $response["MaxBonus"]; //бонусов для списания
        $json["delivery_cost"] = $_SESSION["delivery_cost"];
        $json["operation_id"] = $_POST["JSON"]["operation_id"];
        $json["ord_tpe"] = $_SESSION['order_type'];
        unset($json["gift_from_cost"]);
        //$json["ProductGift"] = $response["ProductGift"]; //подарок
        //$modx->log(MODX_LOG_LEVEL_ERROR, "CART TOTAL COST: ".$_SESSION['minishop2']['total_cost']);
        //$modx->log(MODX_LOG_LEVEL_ERROR, "CART REAL TOTAL COST: ".$real_total_cost);
        //$modx->log(MODX_LOG_LEVEL_ERROR, "CART PRODUCTS: ".json_encode($_SESSION['minishop2']['cart']));
        return json_encode($json);
    }
}