id: 249
source: 1
name: getOrderStatuses
snippet: "$pdoFetch = new pdoFetch($modx);\n$flight_statuses = array(\n    'class' => 'CourierOrderStatuses',\n    'sortdir' => \"ASC\",\n    'return' => 'data',\n    'limit' => 10000\n);\n$pdoFetch->setConfig($flight_statuses);\n$flight_statuses = $pdoFetch->run();\nreturn $flight_statuses;"
properties: 'a:0:{}'
static: 1
static_file: /core/components/courier/elements/snippets/getOrderStatuses.php
content: "$pdoFetch = new pdoFetch($modx);\n$flight_statuses = array(\n    'class' => 'CourierOrderStatuses',\n    'sortdir' => \"ASC\",\n    'return' => 'data',\n    'limit' => 10000\n);\n$pdoFetch->setConfig($flight_statuses);\n$flight_statuses = $pdoFetch->run();\nreturn $flight_statuses;"

-----


$pdoFetch = new pdoFetch($modx);
$flight_statuses = array(
    'class' => 'CourierOrderStatuses',
    'sortdir' => "ASC",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($flight_statuses);
$flight_statuses = $pdoFetch->run();
return $flight_statuses;