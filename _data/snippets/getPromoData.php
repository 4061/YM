id: 265
name: getPromoData
description: 'Получаем настройки сочетания акций и преобразуем их в таблицу'
category: admin
properties: null

-----

$arrayActions = array("Подарок от суммы","Применение промокода","Использование бонусов","Скидка на самовывоз");
$countAction = count($arrayActions);

$parameters = $modx->getOption("app_combination_of_shares");

if(!$parameters){
    foreach($arrayActions as $action) {
        for($i = 0;$i < $countAction;$i++) {
            $msssParameters[$action][] = 0;
        }
    }
} else {
    $msssParameters = json_decode($parameters);
}

$table = '<table class="table table-responsive-md table-actions"><tbody><tr>';
$tableName = '<td></td>';
$tableLine = "";
$counter = 0;
foreach($msssParameters as $key => $value){
    $tableName .= '<td>'.$key.'</td>';
    $tableLine .= '<tr class="line-'.$counter.'"><td>'.$key.'</td>';
    
    foreach($value as $num => $check){
        if($counter == $num) {
            $tableLine .= '<td class="elem-line elem-none"></td>';
        } else {
            $checked = ($check)? "checked" : "";
            $tableLine .= '<td class="elem-line"><input type="checkbox" class="" name="value-'.$counter.'[]" value="1" '.$checked.'></td>';
        }
    }
    
    $tableLine .= "<tr>";
    
    $counter++;
}

$table .= $tableName.'<tr>'.$tableLine.'</tbody></table>';

return $table;