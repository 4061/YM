id: 129
source: 1
name: getOrdersHistory
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();
    
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    
    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $profile = $modx->getObject('modUserProfile', ['website' => $token]); //получаем пользователя по token
        $user_id = $profile->get('internalKey');
        $sql = "
        SELECT 
            modx_ms2_orders.cost as cost,
            modx_ms2_orders.id as order_id,
            modx_ms2_orders.status as status_id,
            modx_ms2_order_statuses.color as status_color,
            modx_ms2_order_statuses.description as status_name,
            modx_ms2_orders.createdon as createdon,
            modx_ms2_order_addresses.street as street,
            modx_ms2_order_addresses.city as city,
            modx_ms2_order_addresses.house_num as house_num,
            modx_ms2_order_addresses.flat_num as flat_num
        FROM 
            modx_ms2_orders,modx_ms2_order_addresses,modx_ms2_order_statuses
        WHERE
            modx_ms2_order_addresses.id = modx_ms2_orders.address AND
            modx_ms2_orders.user_id = '$user_id' AND
            modx_ms2_order_statuses.id = modx_ms2_orders.status
        ORDER BY `modx_ms2_order_addresses`.`id` ASC
        ";
        $info_user= $modx->query($sql);
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        $json['orders'] = $info_user;
        foreach ($json['orders'] as $key => $order){
            $json['orders'][$key]["cost"] = (double)$order['cost'];
            $json['orders'][$key]["order_id"] = (int)$order['order_id'];
            $json['orders'][$key]["status_id"] = (int)$order['status_id'];
        }
        return json_encode($json);
    } else {
        
    }
}