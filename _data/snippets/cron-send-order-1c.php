id: 240
source: 1
name: cron-send-order-1c
category: flowerheaven
properties: 'a:0:{}'

-----

$scriptProperties = array(
                    //'status' => 14
                    );
$SoftjetSync = $modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/');
$action = $SoftjetSync->getOrderActions();
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key, $scriptProperties);  
$sql="
SELECT 
    modx_ms2_orders.id as modx_order, 
    CURRENT_TIMESTAMP,
    modx_ms2_orders.createdon,
    DATE_ADD(CURRENT_TIMESTAMP, INTERVAL +600 SECOND) as current
FROM 
    modx_ms2_orders
WHERE
    modx_ms2_orders.createdon >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL +600 SECOND) AND 
    (modx_ms2_orders.status = 1 OR modx_ms2_orders.status = 15)
";
$orders = $modx->query($sql);
$orders = $orders->fetchAll(PDO::FETCH_ASSOC);
foreach ($orders as $order){
    $order = $modx->getObject("msOrder", $order["modx_order"]);
    $contacts = $modx->getObject('msOrderAddress', array('id'=> $order->address));
    $result = $action->addOrder($order);
    if ($result[0]["id"]){
        $miniShop2->changeOrderStatus($order->id, 2);
        $contacts->set("id_integration_order", $result[0]["id"]);
        $contacts->save(); 
    } else {
        print_r($result);
    }
}