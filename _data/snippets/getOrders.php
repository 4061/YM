id: 252
source: 1
name: getOrders
snippet: "$pdoFetch = new pdoFetch($modx);\n$orders = array(\n    'class' => 'msOrder',\n    'leftJoin' => [\n        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],\n    ],\n    'sortdir' => \"desc\",\n    'return' => 'data',\n    'select' => [\n        'msOrder' => '*',\n        'Address' => 'street'\n    ],\n    'limit' => 10000\n);\n$pdoFetch->setConfig($orders);\n$orders = $pdoFetch->run();\nreturn($orders);"
properties: 'a:0:{}'
static: 1
static_file: /core/components/courier/elements/snippets/getOrders.php
content: "$pdoFetch = new pdoFetch($modx);\n$orders = array(\n    'class' => 'msOrder',\n    'leftJoin' => [\n        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],\n    ],\n    'sortdir' => \"desc\",\n    'return' => 'data',\n    'select' => [\n        'msOrder' => '*',\n        'Address' => 'street'\n    ],\n    'limit' => 10000\n);\n$pdoFetch->setConfig($orders);\n$orders = $pdoFetch->run();\nreturn($orders);"

-----


$pdoFetch = new pdoFetch($modx);
$orders = array(
    'class' => 'msOrder',
    'leftJoin' => [
        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],
    ],
    'sortdir' => "desc",
    'return' => 'data',
    'select' => [
        'msOrder' => '*',
        'Address' => 'street'
    ],
    'limit' => 10000
);
$pdoFetch->setConfig($orders);
$orders = $pdoFetch->run();
return($orders);