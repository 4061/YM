id: 264
source: 1
name: getSystemSettingsAdmin
properties: 'a:0:{}'

-----

//modSystemSetting
$pdoFetch = new pdoFetch($modx);
$system_settings = array(
    'class' => 'modSystemSetting',
    'sortdir' => "desc",
    'return' => 'data',
    'where' => ["modSystemSetting.area = 'softjet_manager' OR modSystemSetting.area = 'softjet_admin'"],
    'select' => [
        'modSystemSetting' => '*',   
    ],
    'limit' => 10000
);
$pdoFetch->setConfig($system_settings);
$system_settings = $pdoFetch->run();
$master_ids = array_column($system_settings, 'key');
foreach ($master_ids as $key=> $master_id){
    $master_ids[$key] = "setting_".$master_id;
}
foreach ($system_settings as $key=> $system_setting){
    $system_settings[$key]['key'] = "setting_".$system_setting["key"];
}
$lexicons = array(
    'class' => 'modLexiconEntry',
    'sortdir' => "desc",
    'return' => 'data',
    'where' => ["name:IN" => $master_ids],
    'select' => [
        'modLexiconEntry' => '*',   
    ],
    'limit' => 10000
);
$pdoFetch->setConfig($lexicons);
$lexicons = $pdoFetch->run();
$system_settings = array_combine(array_column($system_settings, 'key'), $system_settings);
foreach ($lexicons as $lexicon){
    $system_settings[$lexicon['name']]['name'] = $lexicon["value"];
}
return($system_settings);