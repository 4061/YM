id: 169
source: 1
name: admin_login
snippet: "$url =  $modx->config['site_url'];\n$url = str_replace('//','-',$url);\n$url = str_replace('/','',$url);\n$url = str_replace('-','//',$url);\n$mail = $_GET['param1'];\n$pass = $_GET['param2'];\n$logindata = array(\n  'username' => $mail,   // имя пользователя\n  'password' => $pass, // пароль\n  'rememberme' => true        // запомнить?\n);\n// сам процесс авторизации\n$response = $modx->runProcessor('/security/login', $logindata);\n// проверяем, успешно ли\nif ($response->isError()) {\n  // произошла ошибка, например неверный пароль\n  $modx->log(modX::LOG_LEVEL_ERROR, 'Login error. Message: '.$response->getMessage());\n  $data = 0;\n} else {\n    $data = true; \n}\nreturn $data;"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_login.php
content: "$url =  $modx->config['site_url'];\n$url = str_replace('//','-',$url);\n$url = str_replace('/','',$url);\n$url = str_replace('-','//',$url);\n$mail = $_GET['param1'];\n$pass = $_GET['param2'];\n$logindata = array(\n  'username' => $mail,   // имя пользователя\n  'password' => $pass, // пароль\n  'rememberme' => true        // запомнить?\n);\n// сам процесс авторизации\n$response = $modx->runProcessor('/security/login', $logindata);\n// проверяем, успешно ли\nif ($response->isError()) {\n  // произошла ошибка, например неверный пароль\n  $modx->log(modX::LOG_LEVEL_ERROR, 'Login error. Message: '.$response->getMessage());\n  $data = 0;\n} else {\n    $data = true; \n}\nreturn $data;"

-----


$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$mail = $_GET['param1'];
$pass = $_GET['param2'];
$logindata = array(
  'username' => $mail,   // имя пользователя
  'password' => $pass, // пароль
  'rememberme' => true        // запомнить?
);
// сам процесс авторизации
$response = $modx->runProcessor('/security/login', $logindata);
// проверяем, успешно ли
if ($response->isError()) {
  // произошла ошибка, например неверный пароль
  $modx->log(modX::LOG_LEVEL_ERROR, 'Login error. Message: '.$response->getMessage());
  $data = 0;
} else {
    $data = true; 
}
return $data;