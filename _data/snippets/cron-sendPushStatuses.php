id: 156
source: 1
name: cron-sendPushStatuses
properties: 'a:0:{}'

-----

$sql = "
SELECT * FROM 
    modx_confirmed_orders_users,
    modx_ms2_orders
WHERE 
    modx_confirmed_orders_users.status = 0 AND
    modx_ms2_orders.id = modx_confirmed_orders_users.order_id AND
    modx_ms2_orders.createdon >= DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL -'0' HOUR)";
$info_user= $modx->query($sql);
$info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
$url = 'https://fcm.googleapis.com/fcm/send';
$YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
$request_headers = [
    'Content-Type: application/json',
    'Authorization: key=' . $YOUR_API_KEY,
    'apns-push-type: alert'
];
foreach ($info_user as $user){
    $profile = $modx->getObject('modUserProfile', ['internalKey' => $user['user_id']]);
    if ($profile){
        $firebase_token = $profile->get('city');
        $request_body1['registration_ids'] = [$firebase_token]; 
                    $request_body1['notification'] = array("title" => "Пожалуйста, оцените ваш заказ", "body" => "Нам очень важно ваше мнение.");
                    $request_body1['data'] = array(
                        "type" => "alert", 
                        "data" => array(
                            "title" => "Banners", 
                            "body" => "test",         
                            "type" => "change_order_status", 
                            "order_id" => $user['order_id']),
                        "mandatory" => true
                    );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response,1);
        if ($response["success"] == 1){
            $sql = "UPDATE `modx_confirmed_orders_users` SET `status` = '1' WHERE `modx_confirmed_orders_users`.`id` = {$user['id']}";
            $modx->query($sql);
        }
    }
}