id: 181
source: 1
name: rsb-check-order-status
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $order = $_POST['JSON']['order_id'];
        $this_order = $modx->getObject("msOrder", $order);
        $order_address = $modx->getObject('msOrderAddress', $this_order->get("address"));
        $trans_id = $order_address->get("trans_id");
        $id_restourant = $order_address->get("index");
        $rest = $modx->getObject('modResource',$id_restourant);
        $merchant = $rest->getTVValue('merchantID');
        $params = array(
            'command' => 'c',
            'trans_id' => $trans_id,
            'client_ip_addr'=>"5.101.157.102");
        $request = http_build_query($params);
        $curlOptions = array(
            CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 35,
            CURLOPT_HEADER => false,
            CURLOPT_POST => true,
            CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSLKEY => "/home/s/sjeda/localmodx.appsj.su/ssl/".$merchant.".key",
            CURLOPT_SSLCERT =>"/home/s/sjeda/localmodx.appsj.su/ssl/".$merchant.".pem",
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_CAINFO => "/home/s/sjeda/localmodx.appsj.su/ssl/chain-ecomm-ca-root-ca.crt",
            CURLOPT_SSLVERSION => 6,
            CURLOPT_POSTFIELDS => $request,
        );
            $ch = curl_init();
            curl_setopt_array($ch, $curlOptions);
            $response = curl_exec($ch);
            $OKRESPONSE = explode(" ", $response);
            $result = [];
            foreach ($OKRESPONSE as $key => $respons){
              $result[] = explode(":", $respons);
            }
            $finalRes = [];
            foreach ($result as $res){
                $finalRes[] = $res[0];
            }
            $statusCODE = $finalRes[1];
            $statusCODE = explode("\n", $statusCODE);
            if ($statusCODE[0] == "OK"){ // если оплата прошла успешно
                echo "TRANS_ID: ".$trans_id." ".$statusCODE[0];
                echo "</br>";
                $miniShop2 = $modx->getService('miniShop2');
                $miniShop2->initialize($modx->context->key, $scriptProperties);            
                $miniShop2->changeOrderStatus($this_order->get("id"), 6);
                $sql = "UPDATE `modx_user_cards` SET `status` = '1' WHERE `modx_user_cards`.`order_id` = '$order'";
                $modx->query($sql);
                $json["response"] = true;
                
            $url = 'https://fcm.googleapis.com/fcm/send';
            $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
            $request_headers = [
                'Content-Type: application/json',
                'Authorization: key=' . $YOUR_API_KEY,
                'apns-push-type: alert'
            ];
                    
            $this_order = $modx->getObject("msOrder", $_POST['JSON']['order_id']);
            $user_id = $this_order->get("user_id");
            $profile = $modx->getObject('modUserProfile', ['internalKey' => $user_id]);
            $firebase_token = $profile->get('city');
            //$firebase_token = "dg_onD6-L0ZeqZN_UKzod1:APA91bG2t1RzYHZqgeQgQbZXWQwdZ6g9_1141WDeMRpBlN2a4aBNqp4OstJHOkufLKzl1VUY_c46m0dPD8M1TPh-GHmiYEZpNG0kPw9i7iXPbGfoCCpFQq8gVk4586gCQgG5NOgWWiNB";
            $request_body1['registration_ids'] = [$firebase_token]; 
            $request_body1['notification'] = array("title" => "Оплата прошла. Заказ #".$_POST['JSON']['order_id'], "body" => "Заказ #".$_POST['JSON']['order_id']." отправлен на кухню.");
            $request_body1['data'] = array(
                "type" => "alert", 
                "data" => array("title" => "Banners", "body" => "test"),
                "mandatory" => true
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($ch);
            curl_close($ch);
            $modx->log(modX::LOG_LEVEL_ERROR, 'Статус: '.$this_order->get("status").'Сообщение РЕСПОНС'.$response." fribaset token: ".$firebase_token.json_encode($request_body1));
            } else { //если только создан
                if ($response == "RESULT: CREATED"){
                    echo "TRANS_ID: ".$trans_id." ".$response;
                    echo "</br>";
                    $json["response"] = "CREATED";
                } else { //в противном случае - фейл
                    
                    $response = explode(" ",$response);
                    $response = explode("\n",$response[1]);
                    if ($response[0] == "TIMEOUT"){
                        $this_order->set("status",4);
                        $this_order->save();
                        $json["response"] = false; 
                    }
                    
                    //$json["response"] = false; 
                    //$this_order->set("status",4);
                    //$this_order->save();
                } 
            }
            return json_encode($json);
    } else {
        $json["data"] = array("error"=>"Неправильный токен.");
    }
}