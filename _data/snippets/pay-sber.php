id: 76
source: 1
name: pay-sber
properties: 'a:0:{}'

-----

$sber = $modx->getOption('ms2_sber_test');
$action = $modx->getOption('action', $scriptProperties); // получаем тип действия регистрация платежа или проверка платежа
switch ($action) {
    case "register":
        $vars = array();
        $order_id = $modx->getOption('order_id', $scriptProperties);    // Получим id заказа
        $cost = $modx->getOption('cost', $scriptProperties);    // Стоимость в рублях!!!
        $vars['userName'] = $modx->getOption('ms2_terminal_key'); 
        $vars['password'] = $modx->getOption('ms2_secret_key');
        $vars['orderNumber'] = $order_id;   // ID заказа в магазине 
        $vars['amount'] = $cost * 100;   // Сумма заказа в копейках
        $vars['returnUrl'] = 'http://freshkb.appsj.su/oplata.html?success=1'; // URL куда клиент вернется в случае успешной оплаты
        $vars['failUrl'] = 'http://freshkb.appsj.su/'; // URL куда клиент вернется в случае ошибки
        $vars['description'] = 'Заказ №' . $order_id;   
        if ($sber == 1) { $ch = curl_init('https://3dsec.sberbank.ru/payment/rest/register.do?' . http_build_query($vars)); } else {$ch = curl_init('https://securepayments.sberbank.ru/payment/rest/register.do?' . http_build_query($vars)); }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($res);
        return $res;
        //var_dump($res);
        //echo "ID в сбере: ".$res->orderId;
        //echo "<br>Ссылка на оплату: ".$res->formUrl;
        break;
    case "check":
        $vars = array();
        $paymentId = $modx->getOption('paymentId', $scriptProperties);
        $vars['userName'] = $modx->getOption('ms2_terminal_key'); 
        $vars['password'] = $modx->getOption('ms2_secret_key');
        $vars['orderId'] = $paymentId;
        if ($sber == 1) { $ch = curl_init('https://3dsec.sberbank.ru/payment/rest/getOrderStatus.do?' . http_build_query($vars)); } else {$ch = curl_init('https://securepayments.sberbank.ru/payment/rest/getOrderStatus.do?' . http_build_query($vars)); }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($res, true);
        $OrderNumber = $res['OrderNumber']; //получаем номер заказа внутри минишоп, чтоб потом через него изменить статус
        $OrderStatus = $res['OrderStatus']; //получаем статус оплаты от сбербанка
        if ($OrderStatus == 2){ //если оплата прошла успешно, меняем статус на ОПЛАЧЕН
            $miniShop2 = $modx->getService('miniShop2');
            $miniShop2->initialize($modx->context->key, $scriptProperties);            
            $order = $modx->getObject('msOrder', $OrderNumber); 
            $miniShop2->changeOrderStatus($order->get('id'), 2);
        } elseif ($OrderStatus == 0){
            $miniShop2 = $modx->getService('miniShop2');
            $miniShop2->initialize($modx->context->key, $scriptProperties);            
            $order = $modx->getObject('msOrder', $OrderNumber); 
            $miniShop2->changeOrderStatus($order->get('id'), 5);
        } else {
            $miniShop2 = $modx->getService('miniShop2');
            $miniShop2->initialize($modx->context->key, $scriptProperties);            
            $order = $modx->getObject('msOrder', $OrderNumber); 
            $miniShop2->changeOrderStatus($order->get('id'), 4);
        }
        //$modx->log(xPDO::LOG_LEVEL_ERROR,'Номер заказа is '.$OrderNumber." а статус is ".$OrderStatus);
        //$modx->log(xPDO::LOG_LEVEL_ERROR, json_encode($res));
        return $res;
        break;
}