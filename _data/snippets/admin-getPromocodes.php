id: 172
source: 1
name: admin_getPromocodes
description: 'ПХП код для получения всех промокодов из БД.'
snippet: "$sql = \"SELECT *, DATE_FORMAT(date_end, '%d.%m.%Y') as new_date_end FROM modx_promo_base WHERE (type = 1 OR type = 2 OR type = 5) AND is_referal = 0\";\n$promocodes = $modx->query($sql);\n$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);\n\nforeach ($promocodes as $promo){\n    if((stristr($promo[\"code\"], 'BD-') === FALSE)) {\n        if($action != \"BD\") $promocodesCheck[] = $promo;\n    } else if ($action == \"BD\") {\n       $promocodesCheck[] = $promo; \n    }\n}\n\nreturn $promocodesCheck;"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_getPromocodes.php
content: "$sql = \"SELECT *, DATE_FORMAT(date_end, '%d.%m.%Y') as new_date_end FROM modx_promo_base WHERE (type = 1 OR type = 2 OR type = 5) AND is_referal = 0\";\n$promocodes = $modx->query($sql);\n$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);\n\nforeach ($promocodes as $promo){\n    if((stristr($promo[\"code\"], 'BD-') === FALSE)) {\n        if($action != \"BD\") $promocodesCheck[] = $promo;\n    } else if ($action == \"BD\") {\n       $promocodesCheck[] = $promo; \n    }\n}\n\nreturn $promocodesCheck;"

-----


$sql = "SELECT *, DATE_FORMAT(date_end, '%d.%m.%Y') as new_date_end FROM modx_promo_base WHERE (type = 1 OR type = 2 OR type = 5) AND is_referal = 0";
$promocodes = $modx->query($sql);
$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);

foreach ($promocodes as $promo){
    if((stristr($promo["code"], 'BD-') === FALSE)) {
        if($action != "BD") $promocodesCheck[] = $promo;
    } else if ($action == "BD") {
       $promocodesCheck[] = $promo; 
    }
}

return $promocodesCheck;