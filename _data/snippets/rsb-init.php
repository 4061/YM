id: 171
source: 1
name: rsb-init
properties: 'a:0:{}'

-----

<?php
$params = array(
    'command' => 'v',
    'amount' => 15600,
    'description' => 'Заказ #' . 132,
    'currency' => 643,
    'client_ip_addr'=>"5.101.157.102");
$request = http_build_query($params);
$curlOptions = array(
    CURLOPT_URL => "https://testsecurepay.rsb.ru:9443/ecomm2/MerchantHandler",
    CURLOPT_CONNECTTIMEOUT => 5,
    CURLOPT_TIMEOUT => 35,
    CURLOPT_HEADER => false,
    CURLOPT_POST => true,
    CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYHOST => 2,
    CURLOPT_SSLKEY => "/home/s/sjeda/localmodx.appsj.su/ssl/9295929459.key",
    CURLOPT_SSLCERT =>"/home/s/sjeda/localmodx.appsj.su/ssl/9295929459.pem",
    CURLOPT_SSL_VERIFYPEER => true,
    CURLOPT_CAINFO => "/home/s/sjeda/localmodx.appsj.su/ssl/chain-ecomm-ca-root-ca.crt",
    CURLOPT_SSLVERSION => 6,
    CURLOPT_POSTFIELDS => $request,
);
    $ch = curl_init();
    curl_setopt_array($ch, $curlOptions);
    $response = curl_exec($ch);
    if (curl_errno($ch)) {
        $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
    }
    curl_close($ch);
    return $response;