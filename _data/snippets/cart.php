id: 62
source: 1
name: cart
properties: 'a:0:{}'

-----

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
include  $_SERVER["DOCUMENT_ROOT"].'/file/tinkoff/TinkoffMerchantAPI.php'; 
$api = new TinkoffMerchantAPI(
    '1590767212814DEMO',  //Ваш Terminal_Key
    'a8t540qqgktxw2o7'   //Ваш Secret_Key
);
$bonus_count_write = 0;
$sp = &$scriptProperties;
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/')
) {
    $modx->log(1, 'Could not load msBonus2 class!');
}
$msb2->initialize($modx->context->key);
$manager = $msb2->getManager();
$bonus_count_write = 0;
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD'])
{
    
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    //забираем токен как на кебабе
    /*if ($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) {
    $token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
    $token = substr($token, 7);
    }
    $_POST['JSON'] = & $_REQUEST['JSON'];
    $vars = $_POST;
    */
    $token = getallheaders(); //забираем токен как на вм строе
    if ($token['Authorization']){
        $token2 = $token['Authorization'];
    }
    if (isset($token['authorization'])){
        $token2 = $token['authorization'];
    }
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    $vars = $_POST;
    
    
    
    $validate_request = $modx->runSnippet('validateOrder',array(
                        'request' => $vars,
                        'required' => 'name|order_type|payment_type|restourant_id|products'
                    ));
    
    
    
    if($token) {


        if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
            $promocode = $_POST['JSON']['promocode'];
            $products = $_POST['JSON']['products'];
            
            if($_POST['JSON']['name'] != ''){
                $name = $_POST['JSON']['name'];
            }else{
                $name = $profile->get('fullname');
            }

            $phone = $_POST['JSON']['phone'];
            if ($_POST['JSON']['email']) {
                $email = $_POST['JSON']['email']; 
                $profile->set('email', $email);
                $profile->save();
            }
            
            $comment = $_POST['JSON']['comment'];
            $promo =  $_POST['JSON']['promo'];

            if ($_POST['JSON']['bonus'] != 0) {
                $bonus_count_write =  $_POST['JSON']['bonus'];
            }

            $payment = $_POST['JSON']['payment_type'];
            $delivery = $_POST['JSON']['order_type'];
            $address = $_POST['JSON']['address'];
            $restourant_id = $_POST['JSON']['restourant_id'];
            if ($delivery == 2){
                $restourant_id = 0;
                $modx->log(xPDO::LOG_LEVEL_ERROR,'Доставка КУЬЕРОМ');
            }
            if ($restourant_id !=0){
                $sql = "SELECT value
                FROM modx_site_tmplvar_contentvalues
                WHERE tmplvarid = 10 and contentid = 9";
                $order__sql = $modx->query($sql);
                $orders = $order__sql->fetchAll(PDO::FETCH_ASSOC);
                //echo json_encode($orders[0]);
                $orders = $orders[0];
                foreach ($orders as $num =>$order){
                    $order = json_decode($order, true);
                    for ($i= 0; $i < count($order); $i++){
                        if ($order[$i]["MIGX_id"] == $restourant_id){
                            $address = $order[$i]['address'];
                        }
                    }
                }
            }
            if(count($_POST['JSON']['geo']) > 0) {
                /*$region = '
                
                <a href="https://maps.yandex.ru/?text='.$_POST['JSON']['geo']['lat'].'+'.$_POST['JSON']['geo']['lon'].'">
                Ссылка на яндекс карту
                </a>
                ';*/
                $region = '
                
                href="https://maps.yandex.ru/?text='.$_POST['JSON']['geo']['lat'].'+'.$_POST['JSON']['geo']['lon'].'"';
            
            }


            if(count($products) > 0) {
                    //Готовим минишоп к запуску


                    $scriptProperties = array(
                    'status' => 5
                    );


                    $miniShop2 = $modx->getService('miniShop2');

                    // Инициализируем класс в текущий контекст
                    $miniShop2->initialize($modx->context->key, $scriptProperties);

                    $miniShop2->cart->clean(); 

                    foreach ($products as $num => $product) {
                        $modx->log(xPDO::LOG_LEVEL_ERROR, 'PRODUSCT '.json_encode($product));
                        //$arr[0]['title'] = 'test';
                    // $arr[0]['id'] = 331;
                    // $arr[0]['price'] = 200;
                    // $arr[0]['count'] = 1;
                        
                        
                        $options = [];
                        if(isset($product['options'])){
                            $option_fields = [];
                            foreach($product['options'] as $key => $option){
                                
                             /*   $option_fields[$key] = [
                                        "id" => $option["id"],
                                        "title" => $option["title"],
                                        "price" => $option["price"],
                                        "count" => $option["count"],
                                    ]; */
                                    $modx->log(xPDO::LOG_LEVEL_ERROR, json_encode($option));
                                    $miniShop2->cart->add($option["id"],$option["count"]);
                                    
                            }
                            $options['childs'][0] = json_encode($option_fields);
                            $option_ingredients = [];
                            foreach($product['ingredients'] as $key => $option){
                                
                              /*  $option_ingredients[$key] = [
                                        "id" => $option["id"],
                                        "title" => $option["title"],
                                        //"action" => $option["action"],
                                       // "count" => $option["count"],
                                        //"price" => $option["price"]
                                    ]; */
                                    $miniShop2->cart->add($option["id"],1);
                                    
                            }
                            $modx->log(xPDO::LOG_LEVEL_ERROR, 'Убираем ингредиенты '.json_encode($products[$num]['ingredients']).'Суммарно убираем '.count($products[$num]['ingredients']));
                            $testar = array();
                            foreach ($products[$num]['options'] as $arit){
                                //$modx->log(xPDO::LOG_LEVEL_ERROR, 'arit is '.($arit['id']));
                                //$testar = array($arit['id'] => "test1");
                                    $resources = $modx->getCollection('modResource', array("id" => $arit['id']));
                                    foreach ($resources as $res){
                                        $pagetitle = ($res->get('pagetitle'));
                                        $price = ($res->get('price'));
                                        $article = ($res->get('article'));
                                        $image = ($res->get('image'));
                                    }
                                array_push($testar, array(
                                    "id" => $arit['id'],
                                    "count" => $arit['count'],
                                    "pagetitle" => $pagetitle,
                                    "price" => $price,
                                    "article" => $article,
                                    "image" => $image,
                                    "status" => "+"
                                    ));
                                
                            }
                            $leftmodify = array();
                            foreach ($products[$num]['ingredients'] as $ingr){
                                    $resources = $modx->getCollection('modResource', array("id" => $ingr['id']));
                                    foreach ($resources as $res){
                                        $pagetitle = ($res->get('pagetitle'));
                                        $price = ($res->get('price'));
                                        $article = ($res->get('article'));
                                        $image = ($res->get('image'));
                                    }
                                array_push($leftmodify, array(
                                    "id" => $ingr['id'],  
                                    "pagetitle" => $pagetitle,
                                    "price" => $price,
                                    "article" => $article,
                                    "image" => $image,
                                    "status" => "-"
                                ));
                            }
                            $modx->log(xPDO::LOG_LEVEL_ERROR, 'testar is '.json_encode($testar));
                            //$options['childs'][1] = json_encode($option_ingredients);
                            //$testar = array("test1" => "test1");
                            $options['childs'][1] = json_encode($testar); // в массиве 1 лежат модификаторы, которые мы добавляем
                            $options['childs'][2] = json_encode($leftmodify); //в массиве 2 лежат модификаторы, которые мы убавляем
                           /* if ($discount){
                                $profileObj = $modx->getObject('modUserProfile', ['website' => $token]);
                                $internalKey = $profileObj -> get('internalKey');
                                $sql = "SELECT * FROM modx_promo_users WHERE type=3 AND user_id = '$internalKey' ORDER BY `modx_promo_users`.`use_time` DESC";
                                $last_use = $modx->query($sql);
                                $last_use = $last_use->fetchAll(PDO::FETCH_ASSOC);
                                $use_time = $last_use[0]['use_time'];
                                $user_id = $last_use[0]['user_id'];
                                $sql = "
                                UPDATE
                                    modx_promo_users
                                SET
                                    activation_status=2
                                WHERE   
                                    use_time = '$use_time' AND
                                    user_id = '$user_id'
                                ";
                                $modx->query($sql);
                                $options['childs'][3]=1;
                            }*/
                        }
                        $miniShop2->cart->add($product['id'],$product['count'], $options);
                    }


                    $miniShop2->order->add('receiver',$name); // Указываем имя получателя
                    $miniShop2->order->add('phone',$phone);
                    $miniShop2->order->add('email',$email);
                    $miniShop2->order->add('payment', $payment); //Указываем способ оплаты
                    $miniShop2->order->add('delivery', $delivery); //Указываем способ доставки

                    $miniShop2->order->add('street', $address);
                    $miniShop2->order->add('region', $region);
                    $miniShop2->order->add('comment', $comment);
                    $miniShop2->order->add('metro', $promo);
                    $miniShop2->order->add('referalDiscount', $promocode);
                    $order_id = $miniShop2->order->submit(); //Отправляем заказ

                     
                    /*
                    if ($payment == 1) {
                        $miniShop2->order->set('status', 5);
                    }
                    */
            
                    $modx->log(1, 'Could not load msBonus2 class!');
            
                    $miniShop2->changeOrderStatus($order_id, 6); //ставим статус 6 сначала чтоб на эквайринг, плагин у бонуса улавливает этот заказ и у него вычитает из стоимости кол-во бонусов  
            
                    
                    if($payment == 1){ // если способ оплаты картой, то при создании заказа ставится статус 5 (ожидает оплаты). Потом либо крон, либо приложуха с помощью метода pay-sber будут статус менять 
                        //$miniShop2->changeOrderStatus($order_id, 5);
                        $miniShop2->changeOrderStatus($order_id, 5);
                    } else {
                        $miniShop2->changeOrderStatus($order_id, 6);
                    }
                    
            
            
                    /*
                    $miniShop2 = $this->modx->getService('miniShop2');
                    $miniShop2->changeOrderStatus($id , 6);
                    */

                    $payment_url = "";
                    if($payment == 1) {
                        //$manager->setOrderWriteoff($id, $bonus_count_write);
                        $order = $modx->getObject('msOrder', $order_id) ;
                        $output = $modx->runSnippet('pay-sber',array(
                        'action' => 'register',
                        'order_id' => $order_id,
                        'cost' => $order->get('cost')
                        ));
                        
                        //get confirmation url
                        $confirmationUrl = $output->formUrl;    
                        $orderId = $output->orderId;
                            
                        $payment_url = ',
                                "payment_url":"'.$confirmationUrl.'",
                                "payment_id":"'.$orderId.'"
                        '; 
            
                        $profile = $modx->getObject('modUserProfile', ['internalKey' => $order->get('user_id')]);
                        $fields = $profile->get('extended');
                        
                        $fields['payment_id'] = $orderId;
                        $fields['order__id'] = $order->get('id');
                    
                        $profile->set('extended', $fields);
                        $profile->save();
                
                
                    }
                
                    echo '
                    {
                        "data": [
                            {
                            "order_id":'.$order_id.'
                            '.$payment_url.' 
                            }
                        ]
                    }';
    
            } else {
    
                echo '
                {
                    "data": [
                        {"error":"Нет товаров в корзине"}
                    ]
                }';      
         
            }

            /*******count($products)****************/

        } else {
    
            echo '
                {
                    "data": [
                        {"error":"Неверный токен"}
                    ]
                }';
                http_response_code(401);
                
        }        

            /*******user*******************/        
        
    } else {
        
        echo '
        {
            "data": [
                {"error":"Нет Токена"}
            ]
        }';  
        http_response_code(401);
    }    
    
                    
} else {
        echo '
        {
            "data": [
                {"error":"Ошибка формата"}
            ]
        }';
        http_response_code(400);
}