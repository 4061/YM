id: 206
source: 1
name: admin_statuses
properties: 'a:0:{}'

-----

$sql = "SELECT id, name, description, push, push_title, push_description FROM modx_ms2_order_statuses";
$statuses = $modx->query($sql);
$statuses = $statuses->fetchAll(PDO::FETCH_ASSOC);
return $statuses;