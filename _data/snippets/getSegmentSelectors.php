id: 115
source: 1
name: getSegmentSelectors
properties: 'a:0:{}'

-----

$id_promo= $modx->getOption('id_promo', $scriptProperties);
if ($id_promo){
    $sql = "
    SELECT 
        modx_segment_segments.segment_id,
        modx_segment_segments.settings_id,
        modx_segment_segments.segment_createdon,
        modx_segment_segments.segment_name,
        modx_segment_settings.segment_settings
    FROM 
        modx_segment_segments,modx_segment_settings
    WHERE
        modx_segment_segments.settings_id = modx_segment_settings.segment_id
    ORDER BY 
        segment_id DESC ";

    $segment_ids= $modx->query($sql);
    $segment_ids= $segment_ids->fetchAll(PDO::FETCH_ASSOC);
    $pdoFetch = new pdoFetch($modx);
    $users = array(
        'class' => 'PromoUsersSecret',
        'where' => ["id_promo" => $id_promo],
        'sortdir' => "ASC",
        'return' => 'data',
        'limit' => 10000
    );
    $pdoFetch->setConfig($users);
    $users = $pdoFetch->run();
    foreach ($users as $user){
        foreach ($segment_ids as $key=> $segment){
            if ($user["segment_id"] == $segment["segment_id"]){
                $segment_ids[$key]["active"] = 1;
            }
        }
    }
} else {
    $sql = "
    SELECT 
        modx_segment_segments.segment_id,
        modx_segment_segments.settings_id,
        modx_segment_segments.segment_createdon,
        modx_segment_segments.segment_name,
        modx_segment_settings.segment_settings
    FROM 
        modx_segment_segments,modx_segment_settings
    WHERE
        modx_segment_segments.settings_id = modx_segment_settings.segment_id
    ORDER BY 
        segment_id DESC ";

    $segment_ids= $modx->query($sql);
    $segment_ids= $segment_ids->fetchAll(PDO::FETCH_ASSOC);
}
return $segment_ids;