id: 174
source: 1
name: admin-change-promocode
properties: 'a:0:{}'

-----

$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    //return json_encode($_REQUEST['JSON']);
    $sql = "UPDATE modx_promo_base SET ";
    foreach ($_REQUEST['JSON'] as $key => $item){
        if ($key == "id_product"){
            $item = explode(",", $item);
            $item = array_unique($item);
            $item = implode(",",$item);
        }
        
        if ($key == "image"){
            if ($item){
                $sql_columns[] = "$key ='$item'" ;
            }   
        } elseif ($key == "date_end"){
            $date = date_create($item);
            $date =  date_format($date, 'Y-m-d H:i:s');
            if ($item){
                $sql_columns[] = "$key ='$date'";
            } else {
                $sql_columns[] = "$key = NULL";
            }
        } elseif ($key == "order_types" || $key = "orgs"){
            
            $string = "";
            foreach ($item as $elem) {
                $string .= $elem.",";
            }
            $string = mb_substr($string, 0, -1);
            $sql_columns[] = "$key ='$string'" ;
            //$modx->log(1,$key.": ".$string);
            
        } else {
            $sql_columns[] = "$key ='$item'";
        }
     }
     $id_promo = $_REQUEST['JSON']['id_promo'];
    $sql .= implode(', ', $sql_columns);
    $sql .= " WHERE id_promo = '$id_promo'";
        $modx->query($sql);
        return json_encode($sql);