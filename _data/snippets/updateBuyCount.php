id: 155
source: 1
name: updateBuyCount
description: 'Обновление кол-ва покупок продуктов'
properties: 'a:0:{}'

-----

$sql = "
SELECT modx_site_content.id as id FROM `modx_site_content` where modx_site_content.class_key = 'msProduct'";
$content_id = $modx->query($sql);
$all_resources = $content_id->fetchAll(PDO::FETCH_ASSOC);
foreach ($all_resources as $res){
    $all_arr[]=(int)$res['id'];
}
$sql = "
SELECT 
    modx_ms2_product_options.product_id as id 
FROM 
    `modx_ms2_product_options`
WHERE 
    modx_ms2_product_options.key = 'times_bought'";
$content_id = $modx->query($sql);
$options_resources = $content_id->fetchAll(PDO::FETCH_ASSOC);
foreach ($options_resources as $res){
    $options_arr[]=(int)$res['id'];
}
$differences = array_diff($all_arr, $options_arr);
$str = implode(",",$all_arr);
$sql = "
SELECT SUM(count) as count, product_id as id FROM `modx_ms2_order_products` where product_id in ($str) GROUP BY product_id
";
$content_id = $modx->query($sql);
$buy_count = $content_id->fetchAll(PDO::FETCH_ASSOC);
$buy_count = array_combine(array_column($buy_count, 'id'), $buy_count);
foreach ($differences as $diff){
    if ($buy_count[$diff]['count']){
        $sql = "INSERT INTO `modx_ms2_product_options` (`id`, `product_id`, `key`, `value`) VALUES (NULL, '$diff', 'times_bought', '{$buy_count[$diff]['count']}')";
        $modx->query($sql);
    }
}
foreach ($options_arr as $opt){
    if ($buy_count[$opt]['count']){
        $sql = "UPDATE `modx_ms2_product_options` SET `value` = {$buy_count[$opt]['count']} WHERE `modx_ms2_product_options`.`product_id` = '$opt' AND modx_ms2_product_options.key = 'times_bought'";
        $modx->query($sql);
    } else {
        $sql = "UPDATE `modx_ms2_product_options` SET `value` = '0' WHERE `modx_ms2_product_options`.`product_id` = '$opt' AND modx_ms2_product_options.key = 'times_bought'";
        $modx->query($sql);
    }
}