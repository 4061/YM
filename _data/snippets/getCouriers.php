id: 251
source: 1
name: getCouriers
snippet: "$pdoFetch = new pdoFetch($modx);\n$couriers = array(\n    'class' => 'modUserProfile',\n    'leftJoin' => [\n        'Members' => ['class'=> 'modUserGroupMember', 'on'=>'modUserProfile.internalKey = Members.member'],\n    ],\n    'sortdir' => \"desc\",\n    'return' => 'data',\n    'select' => [\n        'modUserProfile' => '*',\n    ],\n    'where' => [\"Members.user_group\" => 6],\n    'limit' => 10000\n);\n$pdoFetch->setConfig($couriers);\n$couriers = $pdoFetch->run();\nreturn $couriers;"
properties: 'a:0:{}'
static: 1
static_file: /core/components/courier/elements/snippets/getCouriers.php
content: "$pdoFetch = new pdoFetch($modx);\n$couriers = array(\n    'class' => 'modUserProfile',\n    'leftJoin' => [\n        'Members' => ['class'=> 'modUserGroupMember', 'on'=>'modUserProfile.internalKey = Members.member'],\n    ],\n    'sortdir' => \"desc\",\n    'return' => 'data',\n    'select' => [\n        'modUserProfile' => '*',\n    ],\n    'where' => [\"Members.user_group\" => 6],\n    'limit' => 10000\n);\n$pdoFetch->setConfig($couriers);\n$couriers = $pdoFetch->run();\nreturn $couriers;"

-----


$pdoFetch = new pdoFetch($modx);
$couriers = array(
    'class' => 'modUserProfile',
    'leftJoin' => [
        'Members' => ['class'=> 'modUserGroupMember', 'on'=>'modUserProfile.internalKey = Members.member'],
    ],
    'sortdir' => "desc",
    'return' => 'data',
    'select' => [
        'modUserProfile' => '*',
    ],
    'where' => ["Members.user_group" => 6],
    'limit' => 10000
);
$pdoFetch->setConfig($couriers);
$couriers = $pdoFetch->run();
return $couriers;