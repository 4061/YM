id: 127
source: 1
name: getUserReferalCode
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $profile = $modx->getObject('modUserProfile', ['website' => $token]);
        $internalKey = $profile -> get('internalKey');
        $sql = "SELECT code FROM modx_promo_base WHERE author_id = '$internalKey'";
        $current_code = $modx->query($sql);
        $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
        $data['data'] = array("promocode" => $current_code[0]['code']);
        return json_encode($data);
    } else {
        return ("Токен устарел");
    }
}