id: 73
source: 1
name: msBonus2Logs
category: msBonus2
properties: 'a:2:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:13:"msb2_prop_tpl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:17:"tpl.msBonus2.logs";s:7:"lexicon";s:19:"msbonus2:properties";s:4:"area";s:0:"";}s:5:"limit";a:7:{s:4:"name";s:5:"limit";s:4:"desc";s:15:"msb2_prop_limit";s:4:"type";s:11:"numberfield";s:7:"options";a:0:{}s:5:"value";i:10;s:7:"lexicon";s:19:"msbonus2:properties";s:4:"area";s:0:"";}}'
static_file: core/components/msbonus2/elements/snippets/logs.php

-----

/** @var modX $modx */
/** @var msBonus2 $msb2 */
/** @var array $scriptProperties */
$sp = &$scriptProperties;
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/', $sp)) {
    return 'Could not load msBonus2 class!';
}
$msb2->initialize($modx->context->key);
$manager = $msb2->getManager();

// Check auth
if (!$modx->user->isAuthenticated($modx->context->key)) {
    return;
}

// pdoPage
$totalVar = $modx->getOption('totalVar', $sp, 'total');
$limit = $modx->getOption('limit', $sp, 10);
$offset = $modx->getOption('offset', $sp, 0);

//
$user = ((int)$modx->getOption('user', $sp, 0)) ?: $modx->user->get('id');
$tpl = $modx->getOption('tpl', $sp, 'tpl.msBonus2.logs');

// Query
$q = $modx->newQuery('msb2Log');
$q->where([
    'user' => $user,
]);

//
$q->leftJoin('modUser', 'Creator', 'Creator.id = msb2Log.createdby');
$q->leftJoin('modUserProfile', 'CreatorProfile', 'CreatorProfile.internalKey = msb2Log.createdby');
$q->leftJoin('msOrder', 'Order', 'Order.id = msb2Log.order');

//
$q->select($modx->getSelectColumns('msb2Log', 'msb2Log'));
$q->select($modx->getSelectColumns('modUser', 'Creator', '', [
    'username',
]));
$q->select($modx->getSelectColumns('modUserProfile', 'CreatorProfile', '', [
    'fullname',
    'email',
    // 'mobilephone',
    // 'dob',
]));
$q->select($modx->getSelectColumns('msOrder', 'Order', 'order_', ['id', 'num']));


// Get total count for pdoPage
$total = $modx->getCount('msb2Log', $q);
$modx->setPlaceholder($totalVar, $total);

//
$q->sortby('id', 'DESC');
$q->limit($limit, $offset);
$rows = [];
if ($tmp = $modx->getCollection('msb2Log', $q)) {
    /** @var msb2Log $v */
    foreach ($tmp as $v) {
        $v = $v->toArray();
        $rows[] = array_merge($v, [
            'action_formatted' => $msb2->getActionType($v),
        ]);
    }
}

//
$msb2->loadFrontendScripts();

//
$msb2User = $manager->getJoinedUser($user);

return $msb2->tools->getChunk($tpl, [
    'logs' => $rows,
    'user' => [
        'points' => $msb2User->get('points'),
        'reserve' => $msb2User->get('reserve'),
        'paid_points' => $msb2User->get('paid_points'),
        'paid_money' => $msb2User->get('paid_money'),
        'level' => $manager->getUserLevel($user),
    ],
]);