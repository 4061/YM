id: 143
source: 1
name: products11
properties: 'a:0:{}'

-----

if (
    'application/json' == $_SERVER['CONTENT_TYPE']
    && 'POST' == $_SERVER['REQUEST_METHOD']
) {

    $_REQUEST['JSON'] = json_decode(
        file_get_contents('php://input'),
        true
    );
    if ($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) {
    $token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
    $token = substr($token, 7);
    }
    
    
    $_POST['JSON'] = &$_REQUEST['JSON'];
    $org_id = $_POST['JSON']['org_id'];
    if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
        $modx->log(1,json_encode($profile));
        $userid = $profile->get('id');
        $modx->log(xPDO::LOG_LEVEL_ERROR, 'id юзера: ' . $userid);
        $sql = "SELECT *
                FROM modx_msbonus2_users, modx_user_attributes, modx_msbonus2_levels
                WHERE modx_user_attributes.id = '$userid' and modx_msbonus2_users.user = modx_user_attributes.internalkey and modx_msbonus2_users.level = modx_msbonus2_levels.id";
        $info_user = $modx->query($sql);
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        if ($info_user) {

            $bonus = $info_user[0]['points'];
            $bonus_persent = $info_user[0]['bonus'];
            $paid_money = $info_user[0]['paid_money'];
            $level = $info_user[0]['level'];
            $level_name = $info_user[0]['name'];
            $modx->log(xPDO::LOG_LEVEL_ERROR, 'количество бонусов: ' . $bonus);
         

            $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = $level";
            if ($modx->query($sql2)) {
                $next_level = $modx->query($sql2);
                $next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
                $bonus_persent = $next_level[0]['bonus'];
            }
            $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
        } else {

            $sql = "SELECT * FROM `modx_msbonus2_levels` WHERE `cost` = 0";
            if ($modx->query($sql)) {
                $bonus_persent = $modx->query($sql);
                $bonus_persent = $bonus_persent->fetchAll(PDO::FETCH_ASSOC);
                $bonus_persent = $bonus_persent[0]['bonus'];
                $bonus = $bonus_persent[0]['name'];
            }
            $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
            $level = '1';
            $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = '2'";
            if ($modx->query($sql2)) {
                $next_level = $modx->query($sql2);
                $next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
                $level_name = $next_level[0]['name'];
                $next_level = '{"id":"' . $next_level[0]['id'] . '","bonus":"' . $next_level[0]['bonus'] . '","cost":"' . $next_level[0]['cost'] . '","name":"' . $next_level[0]['name'] . '"}';
            }
            $paid_money = '0';
        }
    }
} else {
    $sql = "SELECT * FROM `modx_msbonus2_levels` WHERE `cost` = 0";
    if ($modx->query($sql)) {
        $bonus_cost = $modx->query($sql);
        $bonus_cost = $bonus_cost->fetchAll(PDO::FETCH_ASSOC);
        $bonus_persent = $bonus_cost[0]['bonus'];
    }
}

$url =  $modx->config['site_url'];
$url = str_replace('//', '-', $url);
$url = str_replace('/', '', $url);
$url = str_replace('-', '//', $url);


$sql = "SELECT * FROM modx_site_content WHERE parent = 1561 and hidemenu = '0'";
$content_id = $modx->query($sql);
$categoryes = $content_id->fetchAll(PDO::FETCH_ASSOC);

$json = [];

foreach ($categoryes as $category) {

    $id = $category['id'];

$sql = "SELECT content.id,content.pagetitle,content.parent,content.description,price.price,price.image
FROM modx_site_content content , modx_ms2_products price
WHERE NOT EXISTS (SELECT * FROM modx_ms2_product_options WHERE modx_ms2_product_options.product_id = content.id and modx_ms2_product_options.key = 'invisible') and content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and content.parent = $id and content.isfolder != 1 and
    content.id IN (Select product_id FROM modx_products_org_links WHERE sell_point = $org_id) GROUP BY price.id ASC";

    $content_id = $modx->query($sql);
    $tovar = $content_id->fetchAll(PDO::FETCH_ASSOC);

    foreach ($tovar as $key => $tovar__one) {
        
        $additives_res = [];
		$ingredients_res = [];
		$sized_res = [];
		$types_res = [];
		$nutrition_res = [];
		$ingredients_type = [];
		
        $title = $modx->runSnippet('no_sk',array(
                        'str' => $tovar__one["pagetitle"],
                    ));
        

        $nutrition = '';

        $sql = "SELECT value
	FROM modx_site_tmplvar_contentvalues
	WHERE tmplvarid = 1 and contentid = " . $tovar__one['id'];
        $size = $modx->query($sql);
        $size__value = $size->fetchAll(PDO::FETCH_ASSOC);

        $sql = "SELECT *
	FROM modx_ms2_product_categories
	WHERE  product_id = " . $tovar__one['id'];
        $parenet__dop = $modx->query($sql);
        $parenet__dop__value = $parenet__dop->fetchAll(PDO::FETCH_ASSOC);



        $parenet__str = [];

        if ($parenet__dop__value) {
            foreach ($parenet__dop__value as $parenet__dop__value__one) {
                $parenet__str[] =   '"' . $parenet__dop__value__one['category_id'] . '"';
            }
        }



        $sql = "SELECT value
	FROM modx_site_tmplvar_contentvalues
	WHERE tmplvarid = 8 and contentid = " . $tovar__one['id'];
        $nutrition__value = $modx->query($sql);
        $nutrition__arryal = $nutrition__value->fetchAll(PDO::FETCH_ASSOC);


        $nutrition__massive =  json_decode($nutrition__arryal[0]['value'], true);


        if ($nutrition__massive) {
            $nutrition = '';
            $nutrition__number = 0;
            foreach ($nutrition__massive as $nutrition__one) {

                $nutrition_res[] = [
                        "title" => $nutrition__one['title'],
                        "value" =>$nutrition__one['value']
                    ];
                    
            }
        }

  /*      $sql = "SELECT * FROM modx_site_content WHERE parent = $id AND isfolder = 1";
        $additives__value = $modx->query($sql);
        $additives__value = $additives__value->fetchAll(PDO::FETCH_ASSOC);

        if ($additives__value) {

            $additives__id = $additives__value[0]['id'];

            $sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image
FROM modx_site_content content , modx_ms2_products price 
WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and content.parent = $additives__id and content.isfolder != 1 ORDER BY content.menuindex ASC";
            $additives = $modx->query($sql);

            $additives = $additives->fetchAll(PDO::FETCH_ASSOC);


            $additives__massive =  json_decode($additives[0]['id'], true);


            if ($additives) {
                $add = '';
                $additives__number = 0;
                
                foreach ($additives as $additives__one) {

                    $bonus_cost_add = floor($additives__one["price"] / 100 * $bonus_persent);
                    $pagetitle = $modx->runSnippet('no_sk',array(
                        'str' => $additives__one["pagetitle"],
                    ));
                    $additives_res[] = [
                        "id" => $additives__one["id"],
                        "parent" => [$additives__one["parent"]],    
                        "title" => $pagetitle,
                        "price" => (int) $additives__one["price"],
                        "bonus_add"=> $bonus_cost_add,
                        "imgUrl" =>  $url . $additives__one["image"],
                        "size" => $additives__one[0]["value"]
                    ];
                }
            }
            
        }
      */  
        
        
        
        $sql = "SELECT * FROM `modx_ms2_product_options` WHERE `product_id` = " .$tovar__one['id']. " AND `key` LIKE 'ms2_rk_modificators' ORDER BY `id` DESC";
        $modificators = $modx->query($sql);
        $modificators = $modificators->fetchAll(PDO::FETCH_ASSOC);
        
        if (sizeof($modificators)) {
        $modificators = explode(";", $modificators[0]['value']);
        foreach ($modificators as $modificator) {
            $info = $modx->getObject('msProductData', array('article'=> $modificator));
            $id = $info->get('id');
             $product_data = $modx->getObject('modResource', array('id'=> $id));
            $option = $modx->getObject('msProductOption', array(
                'product_id'=> $id,
                'key'=>'ms2_rk_types'
            ));
            $bonus_cost_add = floor($info->get('price') / 100 * $bonus_persent);
            if ($option) {
            $option_value = $option->get('value');
           
            $option_id = $option->get('id');
            if ($option_value == 'Доп') {
                $additives_res[] = [
                        "id" => $id,
                        "title" => $product_data->get('pagetitle'),
                        "price" => (int) $info->get('price'),
                        "bonus_add"=> $bonus_cost_add,
                        "imgUrl" =>  $url . $info->get('image')
                    ];
            } elseif ($option_value == 'Ингредиент') {
                $ingredients_res[] = [
                    "id" => $info->get('id'),
                    "price" => (int) $info->get('price'),
                    "bonus_add"=> $bonus_cost_add,
                    "title" => $product_data->get('pagetitle'),
                ];
            } elseif ($option_value == 'Лаваш') {
                $ingredients_type[] = [
                    "id" => $info->get('id'),
                    "price" => (int) $info->get('price'),
                    "bonus_add"=> $bonus_cost_add,
                    "title" => $product_data->get('pagetitle'),
                ];
            } else {
                 $modx->log(xPDO::LOG_LEVEL_ERROR, 'Ошибка проверки модификаторов id: '.$id.' option: '.$option);
            } 
        }
        }
        }
        if ($ingredients) {

            $ingredients__number = 1;
            foreach ($ingredients as $ingredients__one) {
                $ingredients_res[] = [
                    "id" => $ingredients__one['id'],
                    "title" => $ingredients__one['value'],
                ];
            }
        }

        $sql = "SELECT * FROM modx_ms2_product_options WHERE value = " . $tovar__one['id'] . " ORDER BY product_id ASC";
        $sized__value = $modx->query($sql);
        $sized__value = $sized__value->fetchAll(PDO::FETCH_ASSOC);

        if ($sized__value) {

            $sz = '';
            $sized__number = 0;
            $j = 0;
            foreach ($sized__value as $sized__one) {

                $id_parent = $sized__one['product_id'];
                $sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key = 'sized'
GROUP BY price.id ASC";
                $query = $modx->query($sql);
                $query_sized = $query->fetchAll(PDO::FETCH_ASSOC);
                $i = 0;
                foreach ($query_sized as $sized) {
                    $title_size = $modx->runSnippet('no_sk',array(
                        'str' => $sized["pagetitle"],
                    ));
                    $sized_res[] = [
                            "id" => $sized["id"],
            				"parent" => [$sized["parent"]],    
            				"title" => $title_size,
            				"price" =>  (int) $sized["price"],
            				"size" => $sized["value"]
                    ];
                    
                    if ((count($query_sized) == 1 || $i == 1) && $j == 0 ) {
                        
                        $sized_res[] = [
                            "id" => $tovar__one["id"],
            				"parent" => [$tovar__one["parent"]],    
            				"title" => "Стандарт",
            				"price" =>  0,
            				"size" => "standart"
                        ];
                    }
                    $i++;
                }
                $j++;
            }
        }

        // ******************Энергетическая ценность******************

        $sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$tovar__one['id']." AND `key` LIKE 'energy_%'";
        $energys = $modx->query($sql);
        $energys = $energys->fetchAll(PDO::FETCH_ASSOC);

        if ($energys) {
	        foreach ($energys as $key => $energy__one) {
		        if(!empty($energy__one['value'])){
        		    if ($energy__one['key'] == 'energy_carbohydrates') {
        		        
        		        $nutrition_res[0]["energy_carbohydrates"] = $energy__one['value'];
        		        
        		    } elseif ($energy__one['key'] == 'energy_oils')  {
        		        
        		        $nutrition_res[0]["energy_oils"] = $energy__one['value'];
        		        
        		    } elseif ($energy__one['key'] == 'energy_protein')  {
        		        
        		        $nutrition_res[0]["energy_protein"] = $energy__one['value'];
        		        
        		    } elseif ($energy__one['key'] == 'energy_value') {
        		        $nutrition_res[0]["energy_value"] = $energy__one['value'];
        		    } elseif ($energy__one['key'] == 'energy_size'){
        		        $nutrition_res[0]["energy_size"] = $energy__one['value'];
        		    } elseif ($energy__one['key'] == 'energy_allergens'){
        		        $nutrition_res[0]["energy_allergens"] = $energy__one['value'];
        		    }
		        }
		    
		    }
		    if(!empty($size__value[0]["value"])){
		        $nutrition_res[0]["nutrition_size"] = $size__value[0]["value"];
		    }
        }


        // ******************// Энергетическая ценность******************


        $sql = "SELECT * FROM modx_ms2_product_options WHERE value = " . $tovar__one['parent'] . " ORDER BY product_id ASC";
        $type__value = $modx->query($sql);
        $type__value = $type__value->fetchAll(PDO::FETCH_ASSOC);

        if ($type__value) {

            
            $j = 0;
            foreach ($type__value as $type__one) {

                $id_parent = $type__one['product_id'];
                $sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key LIKE 'type_%'
GROUP BY price.id ASC";
                $query = $modx->query($sql);
                $query_type = $query->fetchAll(PDO::FETCH_ASSOC);
                $i = 0;
                foreach ($query_type as $type) {
                    $title_types = $modx->runSnippet('no_sk',array(
                        'str' => $type["pagetitle"],
                    ));
                    $types_res[] = [
                        "id" => $type["id"],
        				"parent" => [$type["parent"]],    
        				"title" => $title_types,
        				"price" => (int) $type["price"],
        				"type" => $type["value"]
                    ];
                    
                    if ((count($query_type) == 1 || $i == 1)  && $j == 0) {
                        
                        $types_res[] = [
                            "id" => $tovar__one["id"],
            				"parent" => [$tovar__one["parent"]],    
            				"title" => "Лаваш обычный",
            				"price" =>  0,
            				"type" => "Обычный"
                        ];
                    }
                    
                   $i++; 
                }
                
                $j++;
            }
        }
        
        $content =  str_replace(array("\r\n", "\r", "\n"), ' ',  strip_tags($tovar__one['description']));
        $image_small = $modx->runSnippet('pthumb', [
    'input' => substr($tovar__one["image"], 1),
    'options' => '&h=120&q=100',
]);
        $image_big = $modx->runSnippet('pthumb', [
    'input' => substr($tovar__one["image"], 1),
    'options' => '&h=400&q=100',
]);
        $title_parent = $modx->getObject('modResource',$tovar__one["parent"]);
        $title_parent = $title_parent->get('pagetitle');
        $content = $modx->runSnippet('no_sk',array(
                        'str' => $content,
                    ));
        $bonus_cost = floor($tovar__one["price"] / 100 * $bonus_persent);
        $initpage = $modx->getObject('modResource', $tovar__one["id"]);
        $product_rating = $initpage->getTVValue('product_rating');
        $product_rating = json_decode($product_rating, true);
        $middle_rating = (floatval($product_rating[0]['sum'])+(floatval($product_rating[0]['five'])*5))/(floatval($product_rating[0]['count'])+(floatval($product_rating[0]['five'])));
        if ($product_rating[0]['count'] == 0){
            $middle_rating = "Нет оценок";
        }
        if (!$ingredients_type) $ingredients_type = [];
        /*Аналогичные товары*/
        $sql = "SELECT link FROM modx_ms2_product_links WHERE slave = {$tovar__one['id']} OR master = {$tovar__one['id']}";
        $links = $modx->query($sql);
        $links = $links->fetchAll(PDO::FETCH_ASSOC);
        $links_sort = array();
        foreach ($links as $link){
            $sql = "SELECT * FROM modx_ms2_links WHERE id = {$link['link']}";
            $current_link = $modx->query($sql);
            $current_link = $current_link->fetchAll(PDO::FETCH_ASSOC);
            if ($current_link[0]['type'] == "many_to_many"){
                array_push($links_sort, $current_link[0]['id']);
            }
        }
        $array_count_values = array_count_values($links_sort);
        $analog_sql_dop = "";
        $array_count_values_index = 0;
        foreach ($array_count_values as $key => $array_count_value){
            if ($array_count_values_index == 0){
                $analog_sql_dop = $analog_sql_dop." AND link = '$key'";
            } else {
                $analog_sql_dop = $analog_sql_dop." OR link = '$key'";
            }
            $array_count_values_index++;
        }
        if ($analog_sql_dop){
            $sql = "SELECT slave, link FROM modx_ms2_product_links WHERE master = {$tovar__one['id']} $analog_sql_dop";
            $product_link = $modx->query($sql);
            $product_link = $product_link->fetchAll(PDO::FETCH_ASSOC);
            for ($i=0; $i<count($product_link);$i++){
                $slave = $modx->getObject('modResource', $product_link[$i]['slave']);
                $slave_title = $slave->get('pagetitle');
                $slave_price = $slave->get('price');
                $slave_image = $slave->get('image');
                $product_link[$i]['title'] = $slave_title;
                $product_link[$i]['price'] = $slave_price;
                $product_link[$i]['image'] = $slave_image;
            }
        } else {
            $product_link = [];
        }
        /*Добавки*/
        $sql = "SELECT slave as id FROM modx_ms2_product_links WHERE master = {$tovar__one['id']} AND link = 3";
        $additives_links = $modx->query($sql);
        $additives_links = $additives_links->fetchAll(PDO::FETCH_ASSOC);
        for ($i=0; $i<count($additives_links);$i++){
            $slave = $modx->getObject('modResource', $additives_links[$i]['id']);
            $slave_title = $slave->get('pagetitle');
            $slave_price = $slave->get('price');
            $slave_image = $slave->get('image');
            $additives_links[$i]['title'] = $slave_title;
            $additives_links[$i]['price'] = $slave_price;
            $additives_links[$i]['imgUrl'] = $slave_image;
        }
        $json["data"][] = [
                "id" => $tovar__one["id"],
    			"parent" => [$tovar__one["parent"], "title" => $title_parent],    
    			"title" =>  $title,
    			"desc" => $content,
    			"price" => (int) $tovar__one["price"],
    			"img_small" =>  $image_small,
    			"img_big" =>  $image_big,
    			"size" => $size__value[0]["value"],
    			"bonus_add"=> $bonus_cost,
    			"rating" => (float) round($middle_rating,2),
    		//	"additives" => $additives_res,
    			"ingredients"=> $ingredients_res,
    			"types"=> $ingredients_type,
    		//	"sized"=> $sized_res,
    		//	"types"=> $types_res,
    			"energy"=> $nutrition_res,
    			"analogs" => $product_link,
    			"additives" => $additives_links
            ];
    }
}
return json_encode($json);