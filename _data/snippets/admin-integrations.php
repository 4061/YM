id: 191
source: 1
name: admin_integrations
snippet: "$IIKO = $modx->getOption('IIKO');\nreturn($IIKO);"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_integrations.php
content: "$IIKO = $modx->getOption('IIKO');\nreturn($IIKO);"

-----


$IIKO = $modx->getOption('IIKO');
return($IIKO);