id: 254
name: deleteProducts
properties: null

-----

// указываем родителя, в котором нужно удалять товары
$parent = 1561;
// список ID ресурсов что удалять не нужно
$skip = "";
$skip = explode(",",$skip);
// получаем список внутренних категорий с товарами (должно быть не более 1000 элементов)
$resources = $modx->getCollection("modResource",array("parent" => $parent));
// перебераем объекты полученных категорий
foreach($resources as $res){
    // если в исключениях нет данного ID, то удаляем ресурс
    if(!in_array($res->id,$skip)) {
        // удаляем категории и товары внутри полученного объекта каталога
        $modx->runProcessor('resource/delete', array('id' => $res->id));
        // получаем цены для данного товара
        $prices = $modx->getCollection("SoftjetsyncPrices",array("product_id" => $res->id));
        // если они есть
        if($prices){
            // то удаляем их
            foreach ($prices as $price) {
                $price->remove();
            }
        }
        
        // удаляем его из корзины
        $response = $modx->runProcessor('resource/emptyrecyclebin');
        if ($response->isError()) $response->getMessage();
    }
    
}
// !!внимание!!, если родительская категория нужна, то закомментируйте или удалите строки ниже
// удаляем саму родительскую категорию
/*$modx->runProcessor('resource/delete', array('id' => $parent));
// очищаем корзину
$response = $modx->runProcessor('resource/emptyrecyclebin');
if ($response->isError()) $response->getMessage();*/