id: 209
name: admin_getRatingCodes
category: 'Шаблоны Админки'
properties: 'a:0:{}'

-----

$filter = "";

$dateFromFilter = "0000-00-00";
$dateToFilter = "9999-00-00";

if($idres) { $filter .= " id_resource = ".$idres; $filter .= " AND ";}
if($rating != "") { $filter .= " rating_value = ".$rating; $filter .= " AND ";}
if($datefrom) {$dateFromFilter = $datefrom;}
if($dateto) {$dateToFilter = $dateto;}

$filter .= " date_of_creation between '".$dateFromFilter." 00:00:00' and '" .$dateToFilter. " 23:59:00' "  ; $filter .= " AND ";

$sql = "SELECT 
            id_rating,
            id_resource,
            id_user,
            rating_value,
            comment_rating,
            date_of_creation,
            date_of_editing,
            active,
            DATE_FORMAT(date_of_creation, '%d.%m.%Y') as new_date_creation,
            
            modx_user_attributes.internalKey,
            modx_user_attributes.fullname,
            modx_user_attributes.email,
            modx_user_attributes.mobilephone,
            
            modx_site_content.pagetitle
        FROM
            modx_rating,
            modx_user_attributes,
            modx_site_content
        WHERE
            ".$filter."
            modx_user_attributes.internalKey = modx_rating.id_user
            AND modx_site_content.id = modx_rating.id_resource";

$comments = $modx->query($sql);
$comments = $comments->fetchAll(PDO::FETCH_ASSOC);

return $comments;