id: 247
source: 1
name: admin_getStatisticGraphick
description: 'Спиппет для получения статистики по прибыли по месяцам'
category: 'Шаблоны Админки'
snippet: "// массив с названием месяцев на русском\n$massMonthName = array(\n    1 => \"Янв\",\n    2 => \"Фев\",\n    3 => \"Март\",\n    4 => \"Апр\",\n    5 => \"Май\",\n    6 => \"Июнь\",\n    7 => \"Июль\",\n    8 => \"Авг\",\n    9 => \"Сент\",\n    10 => \"Окт\",\n    11 => \"Ноябр\",\n    12 => \"Дек\");\n// переменные для формирования строки для вывода на странице\n$costMass = $monthMass = $orderMass = \"\";\n// текущая дата\n$dateNow = date(\"Y-m-d\");\n// номер текущего месяца\n$month = date(\"n\");\n// формируем дату начала/конца текущего месяца\n$dateCheckMonthStart = strtotime(date(\"Y-m-1\"));\n$dateCheckMonthEnd = strtotime(date(\"Y-m-t\"));\n// переменная для отслеживания текущего месяца\n$firstMonth = $totalSummMonth = $totalSummDay = 0;\n// формируем цикл для перебора 7 последних месяцев\nwhile($month != date(\"n\",strtotime(date(\"Y-m-d\") . \" - 7 month\"))){\n    // обнуляем переменную для подсчёта суммы заказов за месяц\n    $summ = $ordersCount = 0;\n    // записываем в строку с месяцами название полученного месяца\n    $monthMass = \",\".$massMonthName[$month].$monthMass;\n    // получаем список всех заказов\n    $orderCollection = $modx->getCollection(\"msOrder\");\n    // перебераем полученный список заказов\n    foreach ($orderCollection as $order){\n        // получаем дату заказа\n        $dateOrder = strtotime($order->get(\"createdon\"));\n        // если зказ был сформирован в указанном месяце, то прибавляем его сумму к общей сумме за месяц\n        if($dateCheckMonthStart <= $dateOrder && $dateOrder <= $dateCheckMonthEnd) {\n            $summ += $order->get(\"cost\");\n            $totalSummMonth += $order->get(\"cost\");\n        }\n        // обнуляем переменную дня недели\n        $dayWeek = 0;\n        // проверяем, текущий ли это месяц (для этого вводится переменная)\n        if($firstMonth != 1){\n            // перебераем цыкл по всем дням в этом месяце\n            for($day = 1; $day <= date(\"t\"); $day++) {\n                /* ============== Считаем заказы за месяц на каждый день ============== */\n                // если дата у товара совпала с полученой датой в цикле, то увеличиваем в ней переменную\n                if (date(\"Y-m-d\",$dateOrder) == date(\"Y-m-d\",strtotime(date(\"Y-m-1\") . \" + \".($day - 1).\" days\"))) {\n                    // увеличиваем переменную счётчика для дня в месяце\n                    $massMonthCount[$day]++;\n                } else if (!$massMonthCount[$day]) {\n                    // если за данный день ничего не купили, то заносим 0\n                    $massMonthCount[$day] = 0;\n                }\n            }\n            \n            // перебераем цыкл по всем дням в этом месяце\n            for($dayWeek = 0; $dayWeek < 7; $dayWeek++) {\n                \n                $checkDayWeek = date(\"Y-m-d\",strtotime(date(\"Y-m-d\") . \" - \".($dayWeek).\" days\"));\n                if (date(\"Y-m-d\",$dateOrder) == $checkDayWeek) {\n                    $massWeekCost[$checkDayWeek] += $order->get(\"cost\");\n                    $totalSummWeek += $order->get(\"cost\");\n                } else if (!$massWeekCost[$checkDayWeek]) {\n                    $massWeekCost[$checkDayWeek] = 0;\n                }\n                \n            }\n            \n            // перебераем цыкл по всем часам данного дня\n            for($dayHour = 0; $dayHour < 24; $dayHour++) {\n                \n                $checkDayHour = date(\"Y-m-d H\",strtotime(date(\"Y-m-d 00:i:s\") . \" + \".($dayHour).\" hours\"));\n                $dayHourNum = date(\"H\",strtotime(date(\"Y-m-d 00:i:s\") . \" + \".($dayHour).\" hours\"));\n                if (date(\"Y-m-d H\",$dateOrder) == $checkDayHour) {\n                    $massDayCost[$dayHourNum] += $order->get(\"cost\");\n                    $totalSummDay += $order->get(\"cost\");\n                } else if (!$massDayCost[$dayHourNum]) {\n                    $massDayCost[$dayHourNum] = 0;\n                }\n                \n            }\n        }\n        \n    };\n    \n    // заносим сумму за месяц в строку с значениями для вывода на странице\n    $costMass = \",\".$summ.$costMass;\n    // получаем дату начала и конца месяца на предыдущий месяц\n    $dateCheckMonthStart = strtotime(date(\"Y-m-1\",strtotime($dateNow . \" - 1 month\")));\n    $dateCheckMonthEnd = strtotime(date(\"Y-m-t\",strtotime($dateNow . \" - 1 month\")));\n    // получаем номер предыдущего месяца\n    $month = date(\"n\",strtotime($dateNow . \" - 1 month\"));\n    // получаем дату предыдущего месяца для расчёта\n    $dateNow = date(\"Y-m-d\",strtotime($dateNow . \" - 1 month\"));\n    // выставляем переменную текущего месяца в 1, что бы в дальнейшем по нему не велось расчётов\n    $firstMonth = 1;\n    \n}\n\n// формируем строку с перечнем количества заказов в день\nforeach($massMonthCount as $countM){\n   $orderMass .= $countM.\",\";\n}\n// формируем строку с перечнем количества заказов в день\nforeach($massWeekCost as $key => $costW){\n   $orderMassWeek = \",\".$costW.$orderMassWeek;\n   $orderMassWeekKey = \",\".$key.$orderMassWeekKey;\n}\n// формируем строку с перечнем количества заказов в день\nforeach($massDayCost as $key => $costD){\n   $orderMassDay .= $costD.\",\";\n   $orderMassDayKey .= $key.\",\";\n}\n\n$modx->log(1,\"Массив: \".print_r($massWeekCost,1));\n\n// удаляем запятые в начале из обоих полученных строк\n$costMass = substr($costMass, 1);\n$monthMass = substr($monthMass, 1);\n$orderMass = mb_substr($orderMass, 0, -1);\n$orderMassWeek = substr($orderMassWeek, 1);\n$orderMassWeekKey = substr($orderMassWeekKey, 1);\n$orderMassDay = mb_substr($orderMassDay, 0, -1);\n$orderMassDayKey = mb_substr($orderMassDayKey, 0, -1);\n// формируем массив для ответа\n$output = array(\n    \"Month\" => array(\n            \"strCost\" => $costMass,\n            \"strName\" => $monthMass,\n            \"totalCost\" => $totalSummMonth\n        ),\n    \"Week\" => array(\n            \"strCost\" => $orderMassWeek,\n            \"strName\" => $orderMassWeekKey,\n            \"totalCost\" => $totalSummWeek\n        ),\n    \"Day\" => array(\n            \"strCost\" => $orderMassDay,\n            \"strName\" => $orderMassDayKey,\n            \"totalCost\" => $totalSummDay\n        ),\n    \"graphick\" => array(\n            \"orderList\" => $orderMass\n        ),\n);\n\nreturn $output;"
properties: 'a:0:{}'
static: 1
static_file: admin/elements/snippets/admin_getStatisticGraphick.php
content: "// массив с названием месяцев на русском\n$massMonthName = array(\n    1 => \"Янв\",\n    2 => \"Фев\",\n    3 => \"Март\",\n    4 => \"Апр\",\n    5 => \"Май\",\n    6 => \"Июнь\",\n    7 => \"Июль\",\n    8 => \"Авг\",\n    9 => \"Сент\",\n    10 => \"Окт\",\n    11 => \"Ноябр\",\n    12 => \"Дек\");\n// переменные для формирования строки для вывода на странице\n$costMass = $monthMass = $orderMass = \"\";\n// текущая дата\n$dateNow = date(\"Y-m-d\");\n// номер текущего месяца\n$month = date(\"n\");\n// формируем дату начала/конца текущего месяца\n$dateCheckMonthStart = strtotime(date(\"Y-m-1\"));\n$dateCheckMonthEnd = strtotime(date(\"Y-m-t\"));\n// переменная для отслеживания текущего месяца\n$firstMonth = $totalSummMonth = $totalSummDay = 0;\n// формируем цикл для перебора 7 последних месяцев\nwhile($month != date(\"n\",strtotime(date(\"Y-m-d\") . \" - 7 month\"))){\n    // обнуляем переменную для подсчёта суммы заказов за месяц\n    $summ = $ordersCount = 0;\n    // записываем в строку с месяцами название полученного месяца\n    $monthMass = \",\".$massMonthName[$month].$monthMass;\n    // получаем список всех заказов\n    $orderCollection = $modx->getCollection(\"msOrder\");\n    // перебераем полученный список заказов\n    foreach ($orderCollection as $order){\n        // получаем дату заказа\n        $dateOrder = strtotime($order->get(\"createdon\"));\n        // если зказ был сформирован в указанном месяце, то прибавляем его сумму к общей сумме за месяц\n        if($dateCheckMonthStart <= $dateOrder && $dateOrder <= $dateCheckMonthEnd) {\n            $summ += $order->get(\"cost\");\n            $totalSummMonth += $order->get(\"cost\");\n        }\n        // обнуляем переменную дня недели\n        $dayWeek = 0;\n        // проверяем, текущий ли это месяц (для этого вводится переменная)\n        if($firstMonth != 1){\n            // перебераем цыкл по всем дням в этом месяце\n            for($day = 1; $day <= date(\"t\"); $day++) {\n                /* ============== Считаем заказы за месяц на каждый день ============== */\n                // если дата у товара совпала с полученой датой в цикле, то увеличиваем в ней переменную\n                if (date(\"Y-m-d\",$dateOrder) == date(\"Y-m-d\",strtotime(date(\"Y-m-1\") . \" + \".($day - 1).\" days\"))) {\n                    // увеличиваем переменную счётчика для дня в месяце\n                    $massMonthCount[$day]++;\n                } else if (!$massMonthCount[$day]) {\n                    // если за данный день ничего не купили, то заносим 0\n                    $massMonthCount[$day] = 0;\n                }\n            }\n            \n            // перебераем цыкл по всем дням в этом месяце\n            for($dayWeek = 0; $dayWeek < 7; $dayWeek++) {\n                \n                $checkDayWeek = date(\"Y-m-d\",strtotime(date(\"Y-m-d\") . \" - \".($dayWeek).\" days\"));\n                if (date(\"Y-m-d\",$dateOrder) == $checkDayWeek) {\n                    $massWeekCost[$checkDayWeek] += $order->get(\"cost\");\n                    $totalSummWeek += $order->get(\"cost\");\n                } else if (!$massWeekCost[$checkDayWeek]) {\n                    $massWeekCost[$checkDayWeek] = 0;\n                }\n                \n            }\n            \n            // перебераем цыкл по всем часам данного дня\n            for($dayHour = 0; $dayHour < 24; $dayHour++) {\n                \n                $checkDayHour = date(\"Y-m-d H\",strtotime(date(\"Y-m-d 00:i:s\") . \" + \".($dayHour).\" hours\"));\n                $dayHourNum = date(\"H\",strtotime(date(\"Y-m-d 00:i:s\") . \" + \".($dayHour).\" hours\"));\n                if (date(\"Y-m-d H\",$dateOrder) == $checkDayHour) {\n                    $massDayCost[$dayHourNum] += $order->get(\"cost\");\n                    $totalSummDay += $order->get(\"cost\");\n                } else if (!$massDayCost[$dayHourNum]) {\n                    $massDayCost[$dayHourNum] = 0;\n                }\n                \n            }\n        }\n        \n    };\n    \n    // заносим сумму за месяц в строку с значениями для вывода на странице\n    $costMass = \",\".$summ.$costMass;\n    // получаем дату начала и конца месяца на предыдущий месяц\n    $dateCheckMonthStart = strtotime(date(\"Y-m-1\",strtotime($dateNow . \" - 1 month\")));\n    $dateCheckMonthEnd = strtotime(date(\"Y-m-t\",strtotime($dateNow . \" - 1 month\")));\n    // получаем номер предыдущего месяца\n    $month = date(\"n\",strtotime($dateNow . \" - 1 month\"));\n    // получаем дату предыдущего месяца для расчёта\n    $dateNow = date(\"Y-m-d\",strtotime($dateNow . \" - 1 month\"));\n    // выставляем переменную текущего месяца в 1, что бы в дальнейшем по нему не велось расчётов\n    $firstMonth = 1;\n    \n}\n\n// формируем строку с перечнем количества заказов в день\nforeach($massMonthCount as $countM){\n   $orderMass .= $countM.\",\";\n}\n// формируем строку с перечнем количества заказов в день\nforeach($massWeekCost as $key => $costW){\n   $orderMassWeek = \",\".$costW.$orderMassWeek;\n   $orderMassWeekKey = \",\".$key.$orderMassWeekKey;\n}\n// формируем строку с перечнем количества заказов в день\nforeach($massDayCost as $key => $costD){\n   $orderMassDay .= $costD.\",\";\n   $orderMassDayKey .= $key.\",\";\n}\n\n$modx->log(1,\"Массив: \".print_r($massWeekCost,1));\n\n// удаляем запятые в начале из обоих полученных строк\n$costMass = substr($costMass, 1);\n$monthMass = substr($monthMass, 1);\n$orderMass = mb_substr($orderMass, 0, -1);\n$orderMassWeek = substr($orderMassWeek, 1);\n$orderMassWeekKey = substr($orderMassWeekKey, 1);\n$orderMassDay = mb_substr($orderMassDay, 0, -1);\n$orderMassDayKey = mb_substr($orderMassDayKey, 0, -1);\n// формируем массив для ответа\n$output = array(\n    \"Month\" => array(\n            \"strCost\" => $costMass,\n            \"strName\" => $monthMass,\n            \"totalCost\" => $totalSummMonth\n        ),\n    \"Week\" => array(\n            \"strCost\" => $orderMassWeek,\n            \"strName\" => $orderMassWeekKey,\n            \"totalCost\" => $totalSummWeek\n        ),\n    \"Day\" => array(\n            \"strCost\" => $orderMassDay,\n            \"strName\" => $orderMassDayKey,\n            \"totalCost\" => $totalSummDay\n        ),\n    \"graphick\" => array(\n            \"orderList\" => $orderMass\n        ),\n);\n\nreturn $output;"

-----


// массив с названием месяцев на русском
$massMonthName = array(
    1 => "Янв",
    2 => "Фев",
    3 => "Март",
    4 => "Апр",
    5 => "Май",
    6 => "Июнь",
    7 => "Июль",
    8 => "Авг",
    9 => "Сент",
    10 => "Окт",
    11 => "Ноябр",
    12 => "Дек");
// переменные для формирования строки для вывода на странице
$costMass = $monthMass = $orderMass = "";
// текущая дата
$dateNow = date("Y-m-d");
// номер текущего месяца
$month = date("n");
// формируем дату начала/конца текущего месяца
$dateCheckMonthStart = strtotime(date("Y-m-1"));
$dateCheckMonthEnd = strtotime(date("Y-m-t"));
// переменная для отслеживания текущего месяца
$firstMonth = $totalSummMonth = $totalSummDay = 0;
// формируем цикл для перебора 7 последних месяцев
while($month != date("n",strtotime(date("Y-m-d") . " - 7 month"))){
    // обнуляем переменную для подсчёта суммы заказов за месяц
    $summ = $ordersCount = 0;
    // записываем в строку с месяцами название полученного месяца
    $monthMass = ",".$massMonthName[$month].$monthMass;
    // получаем список всех заказов
    $orderCollection = $modx->getCollection("msOrder");
    // перебераем полученный список заказов
    foreach ($orderCollection as $order){
        // получаем дату заказа
        $dateOrder = strtotime($order->get("createdon"));
        // если зказ был сформирован в указанном месяце, то прибавляем его сумму к общей сумме за месяц
        if($dateCheckMonthStart <= $dateOrder && $dateOrder <= $dateCheckMonthEnd) {
            $summ += $order->get("cost");
            $totalSummMonth += $order->get("cost");
        }
        // обнуляем переменную дня недели
        $dayWeek = 0;
        // проверяем, текущий ли это месяц (для этого вводится переменная)
        if($firstMonth != 1){
            // перебераем цыкл по всем дням в этом месяце
            for($day = 1; $day <= date("t"); $day++) {
                /* ============== Считаем заказы за месяц на каждый день ============== */
                // если дата у товара совпала с полученой датой в цикле, то увеличиваем в ней переменную
                if (date("Y-m-d",$dateOrder) == date("Y-m-d",strtotime(date("Y-m-1") . " + ".($day - 1)." days"))) {
                    // увеличиваем переменную счётчика для дня в месяце
                    $massMonthCount[$day]++;
                } else if (!$massMonthCount[$day]) {
                    // если за данный день ничего не купили, то заносим 0
                    $massMonthCount[$day] = 0;
                }
            }
            
            // перебераем цыкл по всем дням в этом месяце
            for($dayWeek = 0; $dayWeek < 7; $dayWeek++) {
                
                $checkDayWeek = date("Y-m-d",strtotime(date("Y-m-d") . " - ".($dayWeek)." days"));
                if (date("Y-m-d",$dateOrder) == $checkDayWeek) {
                    $massWeekCost[$checkDayWeek] += $order->get("cost");
                    $totalSummWeek += $order->get("cost");
                } else if (!$massWeekCost[$checkDayWeek]) {
                    $massWeekCost[$checkDayWeek] = 0;
                }
                
            }
            
            // перебераем цыкл по всем часам данного дня
            for($dayHour = 0; $dayHour < 24; $dayHour++) {
                
                $checkDayHour = date("Y-m-d H",strtotime(date("Y-m-d 00:i:s") . " + ".($dayHour)." hours"));
                $dayHourNum = date("H",strtotime(date("Y-m-d 00:i:s") . " + ".($dayHour)." hours"));
                if (date("Y-m-d H",$dateOrder) == $checkDayHour) {
                    $massDayCost[$dayHourNum] += $order->get("cost");
                    $totalSummDay += $order->get("cost");
                } else if (!$massDayCost[$dayHourNum]) {
                    $massDayCost[$dayHourNum] = 0;
                }
                
            }
        }
        
    };
    
    // заносим сумму за месяц в строку с значениями для вывода на странице
    $costMass = ",".$summ.$costMass;
    // получаем дату начала и конца месяца на предыдущий месяц
    $dateCheckMonthStart = strtotime(date("Y-m-1",strtotime($dateNow . " - 1 month")));
    $dateCheckMonthEnd = strtotime(date("Y-m-t",strtotime($dateNow . " - 1 month")));
    // получаем номер предыдущего месяца
    $month = date("n",strtotime($dateNow . " - 1 month"));
    // получаем дату предыдущего месяца для расчёта
    $dateNow = date("Y-m-d",strtotime($dateNow . " - 1 month"));
    // выставляем переменную текущего месяца в 1, что бы в дальнейшем по нему не велось расчётов
    $firstMonth = 1;
    
}

// формируем строку с перечнем количества заказов в день
foreach($massMonthCount as $countM){
   $orderMass .= $countM.",";
}
// формируем строку с перечнем количества заказов в день
foreach($massWeekCost as $key => $costW){
   $orderMassWeek = ",".$costW.$orderMassWeek;
   $orderMassWeekKey = ",".$key.$orderMassWeekKey;
}
// формируем строку с перечнем количества заказов в день
foreach($massDayCost as $key => $costD){
   $orderMassDay .= $costD.",";
   $orderMassDayKey .= $key.",";
}

$modx->log(1,"Массив: ".print_r($massWeekCost,1));

// удаляем запятые в начале из обоих полученных строк
$costMass = substr($costMass, 1);
$monthMass = substr($monthMass, 1);
$orderMass = mb_substr($orderMass, 0, -1);
$orderMassWeek = substr($orderMassWeek, 1);
$orderMassWeekKey = substr($orderMassWeekKey, 1);
$orderMassDay = mb_substr($orderMassDay, 0, -1);
$orderMassDayKey = mb_substr($orderMassDayKey, 0, -1);
// формируем массив для ответа
$output = array(
    "Month" => array(
            "strCost" => $costMass,
            "strName" => $monthMass,
            "totalCost" => $totalSummMonth
        ),
    "Week" => array(
            "strCost" => $orderMassWeek,
            "strName" => $orderMassWeekKey,
            "totalCost" => $totalSummWeek
        ),
    "Day" => array(
            "strCost" => $orderMassDay,
            "strName" => $orderMassDayKey,
            "totalCost" => $totalSummDay
        ),
    "graphick" => array(
            "orderList" => $orderMass
        ),
);

return $output;