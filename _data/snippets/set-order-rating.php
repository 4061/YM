id: 124
name: set-order-rating
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);

    $_POST['JSON'] = & $_REQUEST['JSON'];
    if ($token){
        $orderId = $_POST['JSON']['orderId'];
        $taste = $_POST['JSON']['taste'];
        $beauty = $_POST['JSON']['beauty'];
        $speed = $_POST['JSON']['speed'];
        $temp = $_POST['JSON']['temp'];
        $courier = $_POST['JSON']['courier'];
        $comment = $_POST['JSON']['comment'];
        if ($taste <= 2 || $beauty <=2 || $speed <=2 || $temp <=2 || $courier <=2){
            $modx->runSnippet('sendBadRating',array(
                'type' => 'order',
                'orderId' => $orderId,
                'taste' => $taste,
                'beauty' => $beauty,
                'speed' => $speed,
                'temp' => $temp,
                'courier' => $courier,
                'comment' =>$comment
                ));
        }
        $profile = $modx->getObject('modUserProfile', ['website' => $token]); //получаем пользователя по token
        $user_id = $profile->get('internalKey'); //получаем его id
        $q = $modx->prepare("SELECT * FROM modx_ms2_orders WHERE user_id='$user_id' ORDER BY createdon");
        $q->execute();
        $orders = $q->fetchAll(PDO::FETCH_ASSOC); //по id находим список его заказов
        foreach ($orders as $key => $order){ //запускаем цикл и делаем проверку, если заказ из запроса совпадает с одним из его заказов, идем дальше
                $createdon = $order['createdon']; //получили этот заказ
                $sql = "
                UPDATE
                    modx_ms2_order_addresses
                SET
                    tasteAssessment = '$taste',
                    beautyAssessment = '$beauty', 
                    speedAssessment = '$speed', 
                    tempAssessment = '$temp', 
                    courierAssessment = '$courier',
                    commemtAssessment = '$comment'
                WHERE
                    createdon = '$createdon'
                ";
                //return $sql;
                $modx->query($sql);
        }
    } else {
        return "Нет токена";
    }
}