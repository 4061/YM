id: 216
name: admin_deliveryObjects
description: 'Массив для получения уникальных значений доставки'
category: 'Шаблоны Админки'
properties: null

-----

$sql = "SELECT ";
$sql .= "id,name";
$sql .= " FROM modx_ms2_deliveries";

$resources = $modx->query($sql);
$resources = $resources->fetchAll(PDO::FETCH_ASSOC);

$massList = $massListName = array();

if($id_promo) {
    $sql = "SELECT order_types FROM modx_promo_base WHERE id_promo = {$id_promo}";
    $result = $modx->query($sql);
    $getParam = $result->fetch(PDO::FETCH_ASSOC)['order_types'];
} else {
    $getParam = $_GET['delivery_type'];
}

$selectList = explode(",",$getParam);

foreach ($resources as $resource) {
    $select = (in_array($resource['id'], $selectList))? "selected" : "";
    $massListName[] = array("id" => $resource['id'], "title" => $resource['name'], "selected" => $select);
}

return $massListName;