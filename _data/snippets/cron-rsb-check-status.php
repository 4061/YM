id: 180
source: 1
name: cron-rsb-check-status
description: 'Этот сниппет запрашивает из базы заказы со статусом "ждет оплаты", затем проходится по каждому из них и проверяет актуальный статус внутри банковской системы. Если статус СОЗДАН, то ничего не'
properties: 'a:0:{}'

-----

$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$url = str_replace('http://','',$url);
$sql = "SELECT * FROM `modx_ms2_orders` WHERE modx_ms2_orders.status = 14 AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -1 DAY)";
$checked_orders = $modx->query($sql);
$checked_orders = $checked_orders->fetchAll(PDO::FETCH_ASSOC);
foreach ($checked_orders as $order){
    $this_order = $modx->getObject("msOrder", $order['id']);
    $order_address = $modx->getObject('msOrderAddress', $order['address']);
    $trans_id = $order_address->get("trans_id");
    $id_restourant = $order_address->get("index");
    $rest = $modx->getObject('modResource',$id_restourant);
    $merchant = $rest->getTVValue('merchantID');
    $params = array(
        'command' => 'c',
        'trans_id' => $trans_id,
        'client_ip_addr'=>"5.101.157.45");
    $request = http_build_query($params);
    $curlOptions = array(
        CURLOPT_URL => "https://securepay.rsb.ru:9443/ecomm2/MerchantHandler",
        CURLOPT_CONNECTTIMEOUT => 5,
        CURLOPT_TIMEOUT => 35,
        CURLOPT_HEADER => false,
        CURLOPT_POST => true,
        CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_SSLKEY => "/home/s/sjeda1prod/".$url."/ssl/".$merchant.".key",
        CURLOPT_SSLCERT =>"/home/s/sjeda1prod/".$url."/ssl/".$merchant.".pem",
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_CAINFO => "/home/s/sjeda1prod/".$url."/ssl/chain-ecomm-ca-root-ca.crt",
        CURLOPT_SSLVERSION => 6,
        CURLOPT_POSTFIELDS => $request,
    );
        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);
        $response = curl_exec($ch);
        //echo $response;
        $OKRESPONSE = explode(" ", $response);
        $result = [];
        foreach ($OKRESPONSE as $key => $respons){
          $result[] = explode(":", $respons);
        }
        $finalRes = [];
        foreach ($result as $res){
            $finalRes[] = $res[0];
        }
        $statusCODE = $finalRes[1];
        $statusCODE = explode("\n", $statusCODE);
        if ($statusCODE[0] == "OK"){ // если оплата прошла успешно
            echo "TRANS_ID: ".$trans_id." ".$statusCODE[0]." ORDER_ID: ".$order['id'];
            echo "</br>";
            $miniShop2 = $modx->getService('miniShop2');
            $miniShop2->initialize($modx->context->key, $scriptProperties);            
            $miniShop2->changeOrderStatus($order["id"], 1);
            $order_id = $order['id'];
            $sql = "UPDATE `modx_user_cards` SET `status` = '1' WHERE `modx_user_cards`.`order_id` = '$order_id'";
            $modx->query($sql);
            
            $url = 'https://fcm.googleapis.com/fcm/send';
            $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
            $request_headers = [
                'Content-Type: application/json',
                'Authorization: key=' . $YOUR_API_KEY,
                'apns-push-type: alert'
            ];
                    
            $this_order = $modx->getObject("msOrder", $order['id']);
            $user_id = $this_order->get("user_id");
            $profile = $modx->getObject('modUserProfile', ['internalKey' => $user_id]);
            $firebase_token = $profile->get('city');
            $request_body1['registration_ids'] = [$firebase_token]; 
            $request_body1['notification'] = array("title" => "Оплата прошла. Заказ #".$order['id'], "body" => "Заказ #".$order['id']." отправлен на кухню.");
            $request_body1['data'] = array(
                "type" => "alert", 
                "data" => array(
                    "title" => "Banners", 
                    "body" => "test",         
                    "type" => "change_order_status", 
                    "order_id" => $order['id']),
                "mandatory" => true
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($ch);
            curl_close($ch);
        } else { //если только создан
            //print_r($statusCODE).'</br>';
            if ($response == "RESULT: CREATED"){
                echo $response." TRANS_ID: ".$trans_id." ".$response;
                echo "</br>";
            } else { //в противном случае - фейл
                $response = explode(" ",$response);
                $response = explode("\n",$response[1]);
                if ($response[0] == "TIMEOUT"){
                    $miniShop2 = $modx->getService('miniShop2');
                    $miniShop2->initialize($modx->context->key, $scriptProperties);            
                    $miniShop2->changeOrderStatus($order["id"], 15);
                }
                if ($response[0] == "FAILED"){
                    $miniShop2 = $modx->getService('miniShop2');
                    $miniShop2->initialize($modx->context->key, $scriptProperties);            
                    $miniShop2->changeOrderStatus($order["id"], 15);
                    
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
                    $request_headers = [
                        'Content-Type: application/json',
                        'Authorization: key=' . $YOUR_API_KEY,
                        'apns-push-type: alert'
                    ];
                    
                    $this_order = $modx->getObject("msOrder", $order['id']);
                    $user_id = $this_order->get("user_id");
                    $profile = $modx->getObject('modUserProfile', ['internalKey' => $user_id]);
                    $firebase_token = $profile->get('city');
                    //$firebase_token = "dg_onD6-L0ZeqZN_UKzod1:APA91bG2t1RzYHZqgeQgQbZXWQwdZ6g9_1141WDeMRpBlN2a4aBNqp4OstJHOkufLKzl1VUY_c46m0dPD8M1TPh-GHmiYEZpNG0kPw9i7iXPbGfoCCpFQq8gVk4586gCQgG5NOgWWiNB";
                    $request_body1['registration_ids'] = [$firebase_token]; 
                    $request_body1['notification'] = array("title" => "Оплата не прошла. Заказ #".$order['id'], "body" => "Карта заблокирована или на вашем счету недостаточно средств. Заказ не отправлен.");
                    $request_body1['data'] = array(
                        "type" => "alert", 
                        "data" => array(
                            "title" => "Banners", 
                            "body" => "test",         
                            "type" => "change_order_status", 
                            "order_id" => $order['id']),
                        "mandatory" => true
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    $response = curl_exec($ch);
                    curl_close($ch);
                }
            } 
        }
        $statusCODE = "";
    
}