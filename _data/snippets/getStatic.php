id: 91
source: 1
name: getStatic
properties: 'a:0:{}'

-----

define('MODX_API_MODE', true);
require $_SERVER['DOCUMENT_ROOT'].'/index.php';
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_WARN);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');
function sQuery($query){
  global $modx;
  $statement = $modx->prepare($query);
  if($statement->execute()){
    $res = $statement->fetchAll(PDO::FETCH_ASSOC);
  }else{
    $res = array();
    $error = $statement->errorInfo();
    if(!empty($error[2])){
      trigger_error($error[2], E_USER_ERROR);
      return false;
    }
  }
  return $res;
}
define('FILESYSTEM_SOURCE_ID', 1); // id источника файлов Filesystem
define('NEW_DIR_PERMISSON', 0720); // Права на новые папки
$static_elems = array(
  'template' => array(
    'table' => $modx->getOption('table_prefix').'site_templates',
    'content_column' => 'content',
    'name_column' => 'templatename',
    'prepend' => '',
    'static_folder_name' => '_templates',
    'static_file_extention' => 'html',
  ),
  'plugin' => array(
    'table' => $modx->getOption('table_prefix').'site_plugins',
    'content_column' => 'plugincode',
    'name_column' => 'name',
    'prepend' => "<?php".PHP_EOL,
    'static_folder_name' => '_plugins',
    'static_file_extention' => 'php',
  ),
  'snippet' => array(
    'table' => $modx->getOption('table_prefix').'site_snippets',
    'content_column' => 'snippet',
    'name_column' => 'name',
    'prepend' => "<?php".PHP_EOL,
    'static_folder_name' => '_snippets',
    'static_file_extention' => 'php',
  ),
  'chunk' => array(
    'table' => $modx->getOption('table_prefix').'site_htmlsnippets',
    'content_column' => 'snippet',
    'name_column' => 'name',
    'prepend' => '',
    'static_folder_name' => '_chunks',
    'static_file_extention' => 'html',
  ),
);
  foreach($static_elems as $table){
      $result = sQuery("SELECT id, ".$table['name_column'].", ".$table['content_column']." FROM ".$table['table']);
          foreach($result as $row){
            $dirname = $table['static_folder_name'];
            $static_file_name = $dirname."/".$row[ $table['name_column'] ].".".$table['static_file_extention'];
            if($row['static_file'] == $static_file_name && (int)$row['static']){
              continue;
            }
            if(!file_exists($dirname)){
              mkdir($dirname, NEW_DIR_PERMISSON, true);
            }
            $cache = $modx->getCacheManager();
            $cache->writeFile(MODX_BASE_PATH .'/git/'. $static_file_name, $table['prepend'].$row[ $table['content_column']]);
          }
  }
  function endsWith($haystack, $needle) {
    return substr_compare($haystack, $needle, -strlen($needle)) === 0;
}
$cache = $modx->getCacheManager(); //инициализация кэш менеджера
$source = MODX_BASE_PATH.'/core/packages'; //путь откуда копируем пакеты
$target = MODX_BASE_PATH.'/git/packages';
$cache->copyTree($source, $target); //копируем все пакеты
$packages = scandir(MODX_BASE_PATH.'/git/packages');
$packages = array_slice($packages,2);
foreach ($packages as $package){
    $value = endsWith($package, 'zip'); //проверка зип или не зип
    if (!$value){ //удаляем папки, оставляем только установочные пакеты
        $cache->deleteTree(MODX_BASE_PATH . '/git/packages/'.$package, ['deleteTop' => true, 'extensions' => []]);
    }
}