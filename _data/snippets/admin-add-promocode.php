id: 175
source: 1
name: admin-add-promocode
properties: 'a:0:{}'

-----

$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    //return json_encode($_REQUEST['JSON']);
    $str = "INSERT INTO 
        `modx_promo_base` 
        (
        `id_promo`, 
        `availability`, 
        `code`, 
        `user_use_count`, 
        `use_count`, 
        `use_count_in_check`, 
        `date_start`, 
        `date_end`, 
        `type`, 
        `author_id`, 
        `author_bonus`, 
        `child_discount`, 
        `id_product`, 
        `value`, 
        `bonus_count`, 
        `name`, 
        `description`, 
        `stores`, 
        `image`, 
        `url`, 
        `activity`, 
        `admin_comment`) VALUES (NULL, '1', 'U-7719-6808', '1', '0', '1', '2020-10-20 11:15:46', '0000-00-00 00:00:00', '3', '761', '1', '1', '0', NULL, '0', '', '', '', '', '', '1', '')";
    $sql = "INSERT INTO `modx_promo_base` (";
    foreach ($_REQUEST['JSON'] as $key => $item){
        $sql_columns[] = "`$key`";
     }
    $sql .= implode(', ', $sql_columns);
    $sql .= ") VALUES (";
    foreach ($_REQUEST['JSON'] as $key => $item){
        if ($key == "date_end" || $key == "date_start"){
            $date = date_create($item);
            $date =  date_format($date, 'Y-m-d H:i:s');
            $sql_columns1[] = "'$date'";
        } else {
            $sql_columns1[] = "'$item'";
        }
    }
    $sql .= implode(', ', $sql_columns1);
    $sql .= ")";
        $modx->query($sql);
        return json_encode($sql);