id: 72
source: 1
name: msBonus2Form
category: msBonus2
properties: 'a:1:{s:3:"tpl";a:7:{s:4:"name";s:3:"tpl";s:4:"desc";s:13:"msb2_prop_tpl";s:4:"type";s:9:"textfield";s:7:"options";a:0:{}s:5:"value";s:17:"tpl.msBonus2.form";s:7:"lexicon";s:19:"msbonus2:properties";s:4:"area";s:0:"";}}'
static_file: core/components/msbonus2/elements/snippets/form.php

-----

/** @var modX $modx */
/** @var msBonus2 $msb2 */
/** @var array $scriptProperties */
$sp = &$scriptProperties;
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/', $sp)
) {
    return 'Could not load msBonus2 class!';
}
$msb2->initialize($modx->context->key);
$manager = $msb2->getManager();

// Check auth
if (!$modx->user->isAuthenticated($modx->context->key)) {
    return;
}

//
$tpl = $modx->getOption('tpl', $sp, 'tpl.msBonus2.form');

// Check is active
$is_active = false;
if ($amount = $manager->getCartWriteoff()) {
    $is_active = is_numeric($amount);
}
// if ($is_active === false) {
//     $manager->unsetCartWriteoff();
// }

//
$msb2->loadFrontendScripts();

//
$output = $msb2->tools->getChunk($tpl, [
    'points' => number_format($manager->getUserPoints(), 0, '.', ' '),
    'amount' => $amount,
    'is_active' => $is_active,
]);

return $output;