id: 270
source: 1
name: user_ym
properties: 'a:0:{}'

-----

//require MODX_BASE_PATH."api/api.class.php";
//$api = new ModxApi($modx,null, $headers);
//return $api->getUserYM();
require MODX_BASE_PATH."integrations/integrations.class.php";
$integrations = new Integrations($modx);
$prime_hill = $integrations->getPrimeHillActions(); //для работы с Prime Hill
$pdoFetch = new pdoFetch($modx);
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();

    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
        $profile = $modx->getObject('modUserProfile', ['website' => $token]);
        $birthday = date("d-m-Y",$profile->dob);
        $birthday = substr($birthday, 0, 10);
        if ($birthday == '01-01-1970'){
            $birthday = '';
        } else {
            $birthday = date("d-m-Y",$profile->dob);
        }
        $gender = $profile->gender;
        if ($gender == 2) {
            $gender = 'Женский';
        } elseif ($gender == 1){
            $gender = 'Мужской';
        } elseif ($gender == 0){
            $gender = 'Не указан';
        }
        $last_user_address = array(
            'class' => 'msOrderAddress',
            'where' => ["user_id" => $profile->internalKey],
            'sortdir' => "DESC",
            'return' => 'data',
            'limit' => 1,
        );
        $pdoFetch->setConfig($last_user_address);
        $last_user_address = $pdoFetch->run();
        $referal_obj = $modx->getObject("PromoBase", ["author_id"=>$profile->internalKey]);
        $bonus_obj = $modx->getObject('msb2User', array("user"=>$profile->internalKey));
        $json["data"]["name"] = $profile->fullname;
        $json["data"]["id"] = $profile->internalKey;
        $json["data"]["email"] = $profile->email;
        $json["data"]["gender"] = array("id"=>$profile->gender, "label"=>$gender);
        if ($profile->photo) $json["data"]["photo"] = $profile->photo;
        $json["data"]["bonus_card"] = "/uploads/barcodes/".$profile->internalKey."card.jpg";
        $json["data"]["birthday"] = $birthday;
        
        $json["data"]["referal"] = array();
        $json["data"]["referal"]["code"] = ($referal_obj->code)? $referal_obj->code : "";
        $json["data"]["referal"]["my_bonus"] = ($referal_obj->author_bonus)? $referal_obj->author_bonus : 0;
        $json["data"]["referal"]["friend_bonus"] = ($referal_obj->child_discount)? $referal_obj->child_discount : 0;
        $json["data"]["referal"]["description"] = "Отправьте реферальный код вашему другу. Он получит скидку на первый заказ, а вы получите бонусы на свой счет за первый оплаченный заказ вашего друга.";
        $json["data"]["referal"]["ref_link"] = "";
        
        $json["data"]["phone"] = $profile->mobilephone;
        if ($modx->getOption("loyalty_PH_program")){
            ($response = $prime_hill->getClient($profile));
            if ($response){
                $json["data"]["bonus"] = (float)$response["bonusBalance"];
            } else {
                $json["data"]["bonus"] = 0;
            }
        } elseif ($modx->getOption("tunec_loyalty_system")) {
            $tural = $this->integrations->getTuralActions(); //для работы с Prime Hill
            $response = $tural->getUserBonuses($profile);
            $json["data"]["bonus"] = $response["ОстатокБонусов"];
        } else {
            $json["data"]["bonus"] = $bonus_obj->points;
        }
        //$json["data"]["bonus"] = ($modx->getOption("loyalty_PH_program"))? $prime_hill->getClient($profile)["bonusBalance"] : $bonus_obj->points;
        $json["data"]["last_address"] = array(
            "street"=>$last_user_address[0]["street"],
            "entrance"=>$last_user_address[0]["entrance"],
            "floor"=>$last_user_address[0]["floor"],
            "house_num"=>$last_user_address[0]["house_num"],
            "flat_num"=>$last_user_address[0]["flat_num"],
            "city"=>$last_user_address[0]["city"],
            "country"=>$last_user_address[0]["country"],
        );
        if($modx->getOption("app_disable_recipient")) {
            $json["data"]["last_address"]["receiver_name"] = ($last_user_address[0]["receiver_name"])? $last_user_address[0]["receiver_name"] : "";
            $json["data"]["last_address"]["recipients_phone"] = ($last_user_address[0]["recipients_phone"])? $last_user_address[0]["recipients_phone"] : "";
            $json["data"]["last_address"]["call_the_recipient"] = ($last_user_address[0]["call_the_recipient"])? $last_user_address[0]["call_the_recipient"] : 0;
        }
        return json_encode($json);