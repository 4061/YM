id: 83
source: 1
name: test_api1
properties: 'a:0:{}'

-----

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
include  $_SERVER["DOCUMENT_ROOT"].'/file/tinkoff/TinkoffMerchantAPI.php'; 
$api = new TinkoffMerchantAPI(
    '1590767212814DEMO',  //Ваш Terminal_Key
    'a8t540qqgktxw2o7'   //Ваш Secret_Key
);
$bonus_count_write = 0;
$sp = &$scriptProperties;
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/')
) {
    $modx->log(1, 'Could not load msBonus2 class!');
}
$msb2->initialize($modx->context->key);
$manager = $msb2->getManager();
$bonus_count_write = 0;

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD'])
{
    
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    if ($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) {
    $token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
    $token = substr($token, 7);
    }

    $_POST['JSON'] = & $_REQUEST['JSON'];
    $vars = $_POST['JSON'];
    
    
    
    
    $validate_request = $modx->runSnippet('validateOrder',array(
                        'request' => $vars,
                        'required' => 'name|order_type|payment_type|restourant_id|products'
                    ));
    
    if(count($validate_request) > 0){
        
       return json_encode($validate_request);
    }
    
    if($token) {


        if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {

            $products = $_POST['JSON']['products'];

            if($_POST['JSON']['name'] != ''){
                $name = $_POST['JSON']['name'];
            }else{
                $name = $profile->get('fullname');
            }

            $phone = $profile->get('mobilephone');
            $email =  $profile->get('email');
            $comment = $_POST['JSON']['comment'];
            $promo =  $_POST['JSON']['promo'];

            if ($_POST['JSON']['bonus'] != 0) {
                $bonus_count_write =  $_POST['JSON']['bonus'];
            }

            $payment = $_POST['JSON']['payment_type'];
            $delivery = $_POST['JSON']['order_type'];
            $address = $_POST['JSON']['address'];

            if(count($_POST['JSON']['geo']) > 0) {
                
                $region = '
                
                <a href="https://maps.yandex.ru/?text='.$_POST['JSON']['geo']['lat'].'+'.$_POST['JSON']['geo']['lon'].'">
                Ссылка на yandex карту
                </a>
                ';
            
            }


            if(count($products) > 0) {
                    //Готовим минишоп к запуску


                    $scriptProperties = array(
                    'status' => 5
                    );


                    $miniShop2 = $modx->getService('miniShop2');

                    // Инициализируем класс в текущий контекст
                    $miniShop2->initialize($modx->context->key, $scriptProperties);

                    $miniShop2->cart->clean(); 

                    foreach ($products as $product) {
                        //$arr[0]['title'] = 'test';
                    // $arr[0]['id'] = 331;
                    // $arr[0]['price'] = 200;
                    // $arr[0]['count'] = 1;
                        
                        $option_fields = [];
                        $options = [];
                        if(isset($product['options'])){
                            foreach($product['options'] as $key => $option){
                                
                                $option_fields[$key] = [
                                        "id" => $option["id"],
                                        "title" => $option["title"],
                                        "price" => $option["price"],
                                        "count" => $option["count"]
                                    ];
                                    
                            }
                            
                            $options['childs'][0] = json_encode($option_fields);
                        }
                        $miniShop2->cart->add($product['id'],$product['count'],$options);
                    }


                    $miniShop2->order->add('receiver',$name); // Указываем имя получателя
                    $miniShop2->order->add('phone',$phone);
                    $miniShop2->order->add('email',$email);
                    $miniShop2->order->add('payment', $payment); //Указываем способ оплаты
                    $miniShop2->order->add('delivery', $delivery); //Указываем способ доставки

                    $miniShop2->order->add('street', $address);
                    $miniShop2->order->add('region', $region);
                    $miniShop2->order->add('comment', $comment);
                    $miniShop2->order->add('metro', $promo);
                   $order_id = $miniShop2->order->submit(); //Отправляем заказ

                     
                    /*
                    if ($payment == 1) {
                        $miniShop2->order->set('status', 5);
                    }
                    */
            
                    $modx->log(1, 'Could not load msBonus2 class!');
            
                    $miniShop2->changeOrderStatus($order_id, 6);  
            
                    /*
                    if($payment == 1)
                    $miniShop2->changeOrderStatus($order->get('id'), 5);
                    else
                    $miniShop2->changeOrderStatus($order->get('id'), 6);    
                    */
            
            
                    /*
                    $miniShop2 = $this->modx->getService('miniShop2');
                    $miniShop2->changeOrderStatus($id , 6);
                    */
                    $payment_url = "";
                    if($payment == 1) {
                        //$manager->setOrderWriteoff($id, $bonus_count_write);
                        $order = $modx->getObject('msOrder', ["id" => $order_id]) ;
                        $output = $modx->runSnippet('pay-sber',array(
                        'action' => 'register',
                        'order_id' => $order_id,
                        'cost' => $order->get('cost')
                        ));
                        //get confirmation url
                        $confirmationUrl = $output->formUrl;    
                        $orderId = $output->orderId;
                            
                        $payment_url = ',
                                "payment_url":"'.$confirmationUrl.'",
                                "payment_id":"'.$orderId.'"
                        '; 
            
                        $profile = $modx->getObject('modUserProfile', ['internalKey' => $order->get('user_id')]);
                        $fields = $profile->get('extended');
                        
                        $fields['payment_id'] = $orderId;
                        $fields['order__id'] = $order->get('id');
                    
                        $profile->set('extended', $fields);
                        $profile->save();
                
                
                    }
                
                    echo '
                    {
                        "data": [
                            {
                            "order_id":'.$order_id.'
                            '.$payment_url.' 
                            '.json_encode($output,JSON_UNESCAPED_UNICODE).'
                            }
                        ]
                    }';
    
            } else {
    
                echo '
                {
                    "data": [
                        {"error":"Нет товаров в корзине"}
                    ]
                }';      
         
            }

            /*******count($products)****************/

        } else {
    
            echo '
                {
                    "data": [
                        {"error":"Неверный токен"}
                    ]
                }';
                
        }        

            /*******user*******************/        
        
    } else {
        
        echo '
        {
            "data": [
                {"error":"Нет Токена"}
            ]
        }';  
    }    
    
                    
} else {
        echo '
        {
            "data": [
                {"error":"Ошибка формата"}
            ]
        }';
}