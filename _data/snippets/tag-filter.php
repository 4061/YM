id: 210
name: tag-filter
category: 'Шаблоны Админки'
properties: null

-----

// проверяем параметры сервера для обращения к скрипту
if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    // получаем значение запроса в формате JSON
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    // получаем все заголовки
    $token = getallheaders();
    // если есть параметр авторизации (токен), то присваиваем его значение второму токену
    if ($token['Authorization']) $token2 = $token['Authorization'];
    // вырезаем первые 7 символов второго токена и передаём значение переменной токен
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    // проверяем существует ли токен
    if ($token){
        // получаем переданную json строку с данными
        $data = $_POST['JSON']['tags_list'];
        // создаём переменную перечня id токенов, для использования её в запросе SQL
        $tagList = "";
        // перебераем полученные значения с данными
        foreach($data as $key => $tagElement){
            // формируем список значений id токенов через запятую
            $tagList .= $tagElement['tag_id'].",";
        }
        // удаляем в списке последний символ (лишняя запятая)
        $tagList = mb_substr($tagList, 0, -1);
        // формируем пустой массив для наполнения его уникальными ID полученных товаров 
        $listIdEltments = array();
        // формируем SQL запрос в базу данных с перечнем связей товаров (в нашем случае) и id нужной нам связи (link)
        $sql = 'SELECT master FROM modx_ms2_product_links WHERE slave IN ('.$tagList.') AND link = 11';
        // отправляем запрос
        $result = $modx->query($sql);
        // получаем ответ
        $list = $result->fetchAll(PDO::FETCH_ASSOC);
        // перебераем полученные значения
        foreach ($list as $key => $element) {
            // проверяем, существует ли найденный ID в массиве уникальных значений. Если нет, то добавляем его в конец массива
            if (!in_array($element['master'],$listIdEltments)) array_push($listIdEltments,$element['master']);
        }
        
        $json["result"] = $listIdEltments;

        // возвращаем результат
        return json_encode($json);
    }
}