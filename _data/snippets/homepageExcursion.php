id: 211
source: 1
name: homepageExcursion
description: 'Титульная страница с сайта GiD'
properties: 'a:0:{}'

-----

// Блок для организации JSON массива с акциями
$sql = "
SELECT 
    id_promo as id,
    name as title,
    description as content,
    date_end,
    id_product as product_id,
    code as promocode,
    type as type,
    child_discount as discount,
    image as img_big,
    url as url
FROM 
    modx_promo_base 
WHERE
    type = 1 and activity = 1 OR
    type = 2 and activity = 1
";
$promocodes = $modx->query($sql);
$promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);
foreach ($promocodes as $key => $promocode){
    $image_small = $modx->runSnippet('pthumb', [
    'input' => substr($promocode["img_big"], 1),
    'options' => '&h=120&q=100',]);
    $promocodes[$key]['img_small'] = $image_small ;
    $promocodes[$key]['date_end'] = strtotime($promocode['date_end']);
}
$json["promotions"] = $promocodes;


//* == Блок для формирования массива с "тегами" */
// указываем значения для выборки (корневая категория и ID шаблона для категорий)
$tagWhere = array("parent" => "3505","template" => "4");
// получаем выборку объектов по нашему запросу
$tags = $modx->getCollection("msProduct",$tagWhere);
// перебераем полученные значения
foreach($tags as $key => $tag){
    // формируем массив значения для взятой категории
    $tagData[$key]["id"] = $tag->get("id");
    $tagData[$key]["title"] = $tag->get("pagetitle");
    $tagData[$key]["image"] = $tag->get("image");
}
// заносим полученный общий массив с тэгами в общий массив для ответа..
// .. предварительно обработав, убрав с помощью команды array_values() ключи массива кторые нам не нужны
$json["tags"] = array_values($tagData);



/* == Блок для формирования массива с "маршрутами" (категориями где лежат товары) */
// указываем значения для выборки (корневая категория и ID шаблона для категорий)
$categoryesWhere = array("parent" => "1561","template" => "3");
// получаем выборку объектов по нашему запросу
$categories = $modx->getCollection("modResource",$categoryesWhere);
// перебераем полученные значения
foreach($categories as $key => $category){
    // формируем массив значения для взятой категории
    $categoryData[$key]["id"] = $category->get("id");
    $categoryData[$key]["title"] = $category->get("pagetitle");
    $categoryData[$key]["description"] = $category->get("introtext");
    $categoryData[$key]["img_big"] = $category->getTvValue("image");
    $categoryData[$key]["dietance"] = $category->getTvValue("tvDistance");
    $categoryData[$key]["time"] = $category->getTvValue("tvTime");
}
// заносим полученный общий массив с маршрутами в общий массив для ответа..
// .. предварительно обработав, убрав с помощью команды array_values() ключи массива кторые нам не нужны
$json["routes"] = array_values($categoryData);

// возвращаем результат
return json_encode($json);