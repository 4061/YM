id: 195
source: 1
name: get-comment-by-org
properties: 'a:0:{}'

-----

if ('application/json' == $_SERVER['CONTENT_TYPE'] && 'POST' == $_SERVER['REQUEST_METHOD']){
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $token = getallheaders();
    
    if ($token['Authorization']) $token2 = $token['Authorization'];
    if ($token['authorization']) $token2 = $token['authorization'];
    $token = substr($token2, 7);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    $rest_id = $_POST['JSON']['rest_id'];
    $res = $modx->getObject('modResource', (int)$rest_id);
    $org_timework = $res->getTVValue('org_timework');
    $org_timework = json_decode($org_timework,true);
    $current_day =  date('l'); //получаем текущий день
    $yesterday = date('l',strtotime("-1 days")); //получаем вчерашний день
    $yesterday_start_array = explode(":",$org_timework[0][$yesterday]);
    $yesterday_day_start = ($yesterday_start_array[0]*60*60)+($yesterday_start_array[1]*60);
    $yesterday_end_array = explode(":",$org_timework[1][$yesterday]);
    $yesterday_day_end = ($yesterday_end_array[0]*60*60)+($yesterday_end_array[1]*60);
    $current_time = (date('G')*60*60)+(date("i")*60);
    $day_start_array = explode(":",$org_timework[0][$current_day]);
    $this_day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60);
    $day_end_array = explode(":",$org_timework[1][$current_day]);
    $this_day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60);
    if ($yesterday_day_end < $yesterday_day_start && $current_time <= $yesterday_day_end){ //если время конца вчерашнего дня меня времени начала вчерашнего дня и настоящее время меньше времени конца вчерашнего дня, то берем временный диапазон вчерашнего дня
        $diapason_day = date('l',strtotime("-1 days"));
    } else {
        $diapason_day = date('l');
    }
    if ($yesterday_day_end > $yesterday_day_start && $this_day_start > $yesterday_day_end && $current_time < $this_day_start){
        $diapason_day = date('l',strtotime("-1 days"));
    }
    $day_start_array = explode(":",$org_timework[0][$diapason_day]);
    $day_end_array = explode(":",$org_timework[1][$diapason_day]);
    $day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60); //время начала текущего диапазона
    $day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60); //время конца текущего диапазона
    if ($day_start > $day_end && $current_time >= $day_end && $current_time < $day_start){ //если время начала больше времени конца и текущее время больше времени конца и меньше времени начала
        $hasWork = false;
    } elseif ($day_start < $day_end && $current_time < $day_end && $current_time >= $day_start){ //если время начала меньше времени конца и настоящее время попадает в этот промежуток то тру
        $hasWork = true;
    } elseif ($day_start < $day_end && $current_time < $day_start){ //если время начала меньше времени конца и настоящее время меньше времени начала, то фолс
        $hasWork = false;
    } elseif ($day_start < $day_end && $current_time >= $day_end){ //если время начала меньше времени конца и настоящее время больше времени конца, то фолс
        $hasWork = false;
    } else {
        $hasWork = true;
    }
    if ($_POST['JSON']['order_type'] == 1){
        //$json['successMsgTitle'] = "Доставим за ".$res->getTVValue('waitingtimeself'); 
        $json['successMsgTitle'] = $res->getTVValue('waitingtimeself'); 
        $json['successMsgSubtitle'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimeselfsubtitle');
    }
    if ($_POST['JSON']['order_type'] == 2){
        //$json['successMsgTitle'] = "Доставим за ".$res->getTVValue('waitingtime'); 
        $json['successMsgTitle'] = $res->getTVValue('waitingtime'); 
        $json['successMsgSubtitle'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimesubtitle');
    }
    if ($hasWork){
       $json['successMsgSubtitle'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimeselfsubtitle'); 
       $json["work_status"] = true;
    } else {
       $json['successMsgSubtitle'] = $modx->getOption('close_message'); 
       $json['successMsgTitle'] = "Рестораны закрыты :(";
       $json["work_status"] = false;
    }
    $iiko_send_statuses[2] = $res->getTVValue('courier');
    $iiko_send_statuses[1] = $res->getTVValue('selfcourier');
    $iiko_send_statuses[3] = $res->getTVValue('restourant');
    if ($iiko_send_statuses[$_POST['JSON']['order_type']] == 0){
       $json['successMsgSubtitle'] = "К сожалению, данный ресторан не может принять заказ в настоящее время по техническим причинам."; 
       $json['successMsgTitle'] = "Ресторан не может принять заказ.";
       $json["work_status"] = false;   
    }
    return json_encode($json);
}