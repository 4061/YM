id: 13
source: 1
name: ms2CustomJsCss
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    case 'msOnManagerCustomCssJs':
        switch ($page) {
            case 'orders':
            $modx->controller->addLastJavascript(MODX_ASSETS_URL . 'components/minishop2/js/mgr/custom/orders.grid.products.js');
            $modx->controller->addLastJavascript(MODX_ASSETS_URL . 'components/minishop2/js/mgr/custom/orders.window.js');
            $modx->controller->addLastJavascript(MODX_ASSETS_URL . 'components/minishop2/js/mgr/custom/orders.grid.js');
            break;
        }
        break;
}