id: 36
source: 1
name: PushOnStatusChange
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    case 'msOnChangeOrderStatus':
        if ($status != 6){
            $sql = "SELECT * FROM modx_ms2_order_statuses WHERE modx_ms2_order_statuses.id = '$status'";
            $info_status = $modx->query($sql);
            $info_status = $info_status->fetchAll(PDO::FETCH_ASSOC);
            if ($info_status[0]["push"] == 1){
                
                $url = 'https://fcm.googleapis.com/fcm/send';
                $YOUR_API_KEY = $modx->getOption('ms2_push_token'); // Server key
                $request_headers = [
                    'Content-Type: application/json',
                    'Authorization: key=' . $YOUR_API_KEY,
                    'apns-push-type: alert'
                ];
    
                $profile = $modx->getObject('modUserProfile', ['internalKey' => $order->user_id]);
                $firebase_token = $profile->get('city');
                $push_title = $info_status[0]["push_title"];
                $contacts = $modx->getObject('msOrderAddress', array('id'=> $order->address));
                $push_title = str_replace("#НОМЕР_ЗАКАЗА", $order->id, $push_title);
                $push_title = str_replace("#ИМЯ", $contacts->receiver, $push_title);
                $request_body1['registration_ids'] = [$firebase_token]; 
                $request_body1['notification'] = array("title" => $push_title, "body" => $info_status[0]["push_description"]);
                $request_body1['data'] = array(
                    "type" => "alert", 
                    "data" => array(
                        "title" => "Banners", 
                        "body" => "test",         
                        "type" => "change_order_status", 
                        "order_id" => $order->id),
                    "mandatory" => true
                );
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $response = curl_exec($ch);
                $modx->log(modX::LOG_LEVEL_ERROR, 'плагин dpf работает: '.$response);
                curl_close($ch);
            }
        }
    break;
}