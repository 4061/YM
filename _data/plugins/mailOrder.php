id: 38
source: 1
name: mailOrder
description: 'Отсылать копию письма в магазин, адрес которого был выбран в заказе'
category: miniShop2
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
  case 'msOnChangeOrderStatus':
      // проверка, включена ли настройка на отправку писем на почту менеджера
    if ($modx->getOption("app_order_to_email")) {
        if ($status == 14 || $status == 6) {
            
            // если заказ в статусе "Новый" (ID 14)
            // получаем номер заказа из объекта заказа
            $order_num = $order->get('num');
            // преобразуем объект заказа в массив
            $orderMass = $order->toArray();
            // чанк шаблона оформления письма о новом заказе для менеджера
            $tpl = 'tpl.msEmail.new.manager'; 
            
            // массив выводим в лог системы управления
            //$modx->log(1, print_r($orderMass, 1),'HTML'); 
            // получаем ID адреса заказа
            $idDelivery = $orderMass['address'];
            
            // получаем объект с адресом заказа
            $delivery = $modx->getObject('msOrderAddress',$idDelivery);
            // получаем ID организации
            $organisationId = $delivery->get('index');
            // формируем SQL запрос на получение email для отправки и выполняем его
            $sql = 'SELECT value FROM modx_site_tmplvar_contentvalues WHERE tmplvarid = 36 AND contentid = '.$organisationId;
            $resultQERY = $modx->query($sql);
            
            $emailList = "";
            
            // проверяем наличие почты у организации
            if($resultQERY){
                // добавляем к полученному списку электронную почту основного менеджера из настроек minishop2
                $emailList = $resultQERY->fetch(PDO::FETCH_ASSOC)['value'].",";
            }
            // добавляем к списку почт организации общую почту
            $emailList .= $modx->getOption("ms2_email_manager");
            
            $pdoFetch = $modx->getService('pdoFetch');
            // если полученное значение не пустое
            if($emailList != "") {
                // добавляем к строке электронных почт почту установленную для minishop2 и разбиваем её на массив
                $email_mass = explode(",",$emailList);
                
                //$modx->log(1, print_r($glCity, 1),'HTML');
                // получаем данные для письма
                $email_from = $modx->getOption('emailsender'); // эл.адрес оправителя письма
                $sitename = $modx->getOption('site_name'); // имя/название отправителя письма
                $subject = 'Новый заказ #' . $order_num; // тема письма
                // заносим данные из заказа в шаблон письма
                //$body = $modx->runSnippet('msGetOrder', array_merge($orderMass, array('tpl' => $tpl)));
                
                // формируем массивы с данными для отправки в письме
                $order = $modx->getObject('msOrder', $orderMass['id']);
                $products = $modx->getCollection('msOrderProduct',array('order_id' => $orderMass['id']));
                $address = $modx->getObject('msOrderAddress',$order->address);
                $total = $_REQUEST["JSON"];
                // получаем объект ресторана по ID
                $orgObject = $modx->getObject("modResource",$total["id_org"]);
                $userObject = $modx->getObject("modUserProfile",array("internalKey" => $address->get("user_id")));
                $userAddress = $modx->getObject("UserAddresses",array("id" => $address->get("address_id")));
                
                // формируем массив с товарами для отправки
                foreach($products as $product) {
                    $productElem = $product->toArray();
                    $productElem["mailvisible"] = 1;
                    $productsMass[] = $productElem;
                }
                
                // добавляем некторые недостающие параметры
                $total = $_REQUEST["JSON"];
                $order = $order->toArray();
                $address = $address->toArray();
                $address["discount"] = ($address["discount"])? $address["discount"] : 0;
                $address["time_appointment"] = ($address["time_appointment"])? date("H:i d-m-Y",strtotime($address["time_appointment"])) : "";
                $address["createdon"] = ($address["createdon"])? date("d-m-Y",strtotime($address["createdon"])) : "";
                $address["comment"] = str_replace(" Необходимо позвонить получателю","",$address["comment"]);
                $total["cart_cost"] = (int)$order["cart_cost"] - (int)$total["bonus"];
                $total["cost"] = (int)$order["cost"] - (int)$total["bonus"];
                $total["delivery_cost"] = $_REQUEST["delivery_cost"];
                $total["restaurant"] = $orgObject->get("pagetitle");
                $total["name"] = $userObject->get("fullname");
                $total["phone"] = $userObject->get("mobilephone");
                $total["call_the_recipient"] = $userAddress->get("call_the_recipient");
                $total["delivery_date"] = ($userAddress->get("delivery_date") ?? null) ? $userAddress->get("delivery_date") : $_REQUEST["delivery_date"] ?? null;
                $total["delivery_time"] = ($userAddress->get("delivery_time") ?? null) ? $userAddress->get("delivery_time") : $_REQUEST["delivery_time"] ?? null;
                $total["card_text"] = ($userAddress->get("card_text") ?? null) ? $userAddress->get("card_text") : $_REQUEST["card_text"] ?? null;
                // формируем массив для вставки в шаблон
                $pls = array(
                    "order" => $order,
                    "products" => $productsMass,
                    "address" => $address,
                    "total" => $total,
                    "user" => array("email" => $total["email"]),
                    "delivery" => array("name" => "Курьером"),
                    "payment" => array("name" => "Онлайн оплата"),
                    "thisOrderBonus" => ($total["bonus"])? $total["bonus"] : 0,
                );
                
                
                // собираем контент для отправки в письме
                $body = $pdoFetch->getChunk($tpl, $pls);
                
                //$modx->log(1, "получатель: ".$email_to.", отправитель: $email_from");
                //$modx->log(1, "Тест содержимого массива: ".print_r($pls,1));
                
                // отправка письма по списку почтовых ящиков
                $modx->getService('mail', 'mail.modPHPMailer');
                foreach ($email_mass as $mailtoAddress) {
                    if($mailtoAddress != ""){
                        $modx->mail->set(modMail::MAIL_BODY,$body);
                        $modx->mail->set(modMail::MAIL_FROM,$email_from);
                        $modx->mail->set(modMail::MAIL_FROM_NAME,$sitename);
                        $modx->mail->set(modMail::MAIL_SUBJECT,$subject);
                        // это отправка в лог информации о том куда отправляется письмо
                        $modx->log(1, "получатель: ".$mailtoAddress.", отправитель: $email_from");
                        $modx->mail->address('to',$mailtoAddress);
                        
                        $modx->mail->setHTML(true);
                        if (!$modx->mail->send()) {
                            $modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$modx->mail->mailer->ErrorInfo);
                        }
                        // сбрасываем указанные данные, для возможности повторной отправки
                        $modx->mail->reset();
                    }
                }
    
            }
        }
    }
    break;
}