id: 28
source: 1
name: composer
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    case 'OnMODXInit':
        $file = MODX_CORE_PATH . 'composer/autoload.php';

        if (file_exists($file)) {
            require_once $file;
        }
        break;
}