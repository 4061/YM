id: 39
source: 1
name: updateSegmentsOnInit
description: 'Обновляет таблицу сегментов, когда юзер заходит в админку'
properties: 'a:0:{}'

-----

$getRes = $modx->resource;
switch ($modx->event->name) {
    case 'OnLoadWebDocument':
        //$modx->log(1,"Страница грузится");
        
        if ($getRes->get("uri") == "segments.html"){
            $sql = 
                "SELECT 
                    MAX(us.id) as ID_us, 
                    MAX(us_atr.fullname) as ID_fullname, 
                    MIN(us.username) as ID_username, 
                    MIN(ord.createdon) as time_start,
                    MAX(ord.createdon) as time_END,
                    Count(*) as ord_count, 
                    AVG(ord.cart_cost) as  ord_avg, 
                    SUM(ord.cart_cost) as  ord_summ
                FROM modx_users as us,
                   modx_user_attributes as us_atr,
                   modx_ms2_orders as ord
                WHERE 
                    us.id= ord.user_id AND 
                    us.id= us_atr.internalKey AND
                    ord.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>0)  
                Group By us.id
                HAVING 1";
            $info_user= $modx->query($sql);
            $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC); //получаем всех, кто делал заказы и нужные поля
        
            foreach ($info_user as $key =>$user){//добавляем пользователей, которые появились недавно
                $user_id = $user['ID_us'];
                $user_name = $user['ID_fullname'];
                $time_start = $user['time_start'];
                $time_end = $user['time_END'];
                $ord_count = $user['ord_count'];
                $ord_avg = $user['ord_avg'];
                $ord_sum = $user['ord_summ'];
                $sql = "
                INSERT INTO 
                    modx_segment_users
                    (
                        `user_id`, 
                        `user_name`, 
                        `time_start`, 
                        `time_end`, 
                        `ord_count`, 
                        `ord_avg`, 
                        `ord_sum`
                    )
                    VALUES
                    (
                        '$user_id',
                        '$user_name',
                        '$time_start',
                        '$time_end',
                        '$ord_count',
                        '$ord_avg',
                        '$ord_sum'
                    )
                ";
                $modx->query($sql);
            }
            
            foreach ($info_user as $key =>$user){ //обновляем статистику
                $user_id = $user['ID_us'];
                $user_name = $user['ID_fullname'];
                $time_start = $user['time_start'];
                $time_end = $user['time_END'];
                $ord_count = $user['ord_count'];
                $ord_avg = $user['ord_avg'];
                $ord_sum = $user['ord_summ'];
                $sql = "
                UPDATE
                    modx_segment_users
                SET
                    user_name = '$user_name',
                    time_end = '$time_end', 
                    ord_count = '$ord_count', 
                    ord_avg = '$ord_avg', 
                    ord_sum = '$ord_sum'
                WHERE
                    user_id = '$user_id'
                ";
                $modx->query($sql);
            }
        
            //$modx->log(1,"Событие сработало! ".$getRes->get("uri"));
        }
        
    break;
    case 'OnWebPageComplete':
        
        //$modx->log(1,"Страница загрузилась");
        
    break;
}