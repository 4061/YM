id: 40
source: 1
name: addUserAttributes
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
    case "OnMODXInit":
        $map = array(
            'modUserProfile' => array(
                'fields' => array(
                    'last_trans_id' => '',
                    'bouns_integration_guid' => '',
                    'black_list_type' => '',
                    'max_cost' => '',
                ),
                'fieldMeta' => array(
                    'last_trans_id' => array(
                        'dbtype' => 'varchar',
                        'precision' => '100',
                        'phptype' => 'string',
                        'null' => true,
                    ),
                    'bouns_integration_guid' => array(
                        'dbtype' => 'varchar',
                        'precision' => '100',
                        'phptype' => 'string',
                        'null' => true,
                    ),
                    'black_list_type' => array(
                        'dbtype' => 'int',
                        'precision' => '',
                        'phptype' => 'int',
                        'null' => true,
                    ),
                    'max_cost' => array(
                        'dbtype' => 'varchar',
                        'precision' => '100',
                        'phptype' => 'string',
                        'null' => true,
                    ),
                ),
            ),
        );
        foreach ($map as $class => $data) {
            $modx->loadClass($class);
            foreach ($data as $tmp => $fields) {
                if ($tmp == 'fields') {
                    foreach ($fields as $field => $value) {
                        foreach (array('fields', 'fieldMeta', 'indexes') as $key) {
                            if (isset($data[$key][$field])) {
                                $modx->map[$class][$key][$field] = $data[$key][$field];
                            }
                        }
                    }
                } elseif ($tmp == 'composites' || $tmp == 'aggregates') {
                    foreach ($fields as $alias => $relation) {
                        if (!isset($modx->map[$class][$tmp][$alias])) {
                            $modx->map[$class][$tmp][$alias] = $relation;
                        }
                    }
                }
            }
        }
        break;
    case "OnUserFormPrerender":
        if (!isset($user) || $user->get('id') < 1) {
            return;
        }
        if ($user->get('id') > 0) {
            $data['last_trans_id'] = htmlspecialchars($user->Profile->last_trans_id);
            $data['max_cost'] = htmlspecialchars($user->Profile->max_cost);
            $data['bouns_integration_guid'] = htmlspecialchars($user->Profile->bouns_integration_guid);
            $modx->controller->addHtml("
                <script type='text/javascript'>
                    Ext.ComponentMgr.onAvailable('modx-user-tabs', function() {
                        this.on('beforerender', function() {
                            // Получаем колонки первой вкладки
                            var leftCol = this.items.items[0].items.items[0].items.items[0];
                            // Добавляем новое поле в левую колонку 4ым по счёту полем (перед полем 'Email')
                            leftCol.items.insert(3, 'last_trans_id', new Ext.form.TextField({
                                id: 'modx-user-last_trans_id',
                                name: 'last_trans_id',
                                fieldLabel: 'Id последней банковской транзакции',
                                xtype: 'textfield',
                                anchor: '100%',
                                maxLength: 255,
                                value: '{$data['last_trans_id']}',
                            }));
                            leftCol.items.insert(4, 'max_cost', new Ext.form.TextField({
                                id: 'modx-user-max_cost',
                                name: 'max_cost',
                                fieldLabel: 'Максимальная цена корзины',
                                xtype: 'textfield',
                                anchor: '100%',
                                maxLength: 255,
                                value: '{$data['max_cost']}',
                            }));
                            leftCol.items.insert(5, 'bouns_integration_guid', new Ext.form.TextField({
                                id: 'modx-user-bouns_integration_guid',
                                name: 'bouns_integration_guid',
                                fieldLabel: 'ID юзера в интеграционной бонусной системы',
                                xtype: 'textfield',
                                anchor: '100%',
                                maxLength: 255,
                                value: '{$data['bouns_integration_guid']}',
                            }));
                        });
                    });
                </script>
            ");
        }
        break;
}