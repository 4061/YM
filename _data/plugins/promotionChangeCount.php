id: 41
source: 1
name: promotionChangeCount
description: 'Плагин на товары при добавлении и смене их количества'
properties: 'a:0:{}'
disabled: 1

-----

$cartElements = $cart->get(); //Получаем информацию о козине - т.е. список товаров

$totalCount = $actionCount = 0;
$itemIdPrice = $sortMassPrice = array();
// перебераем все товары в корзине
foreach ($cartElements as $key => $item) {
    
    if(!$item['options']["action"]){
        // считаем общее число товаров в корзине
        $totalCount += $item['count'];
        
        // составляем массив с уникальными ценами, для того что бы посчитать минимальную цену
        if(!in_array($key,array_keys($itemIdPrice))) {
            $itemIdPrice[$key] = $item['price'];
        }
    } else if (!$item['options']["newPrice"]) {
        $cartElements[$key]['old_price'] = $cartElements[$key]['price'];
        $cartElements[$key]['price'] = $cartElements[$key]['price']/2;
        $cartElements[$key]['options']["newPrice"] = 1;
        //$modx->log(1,"Есть элемент - меняем цену: ".$cartElements[$key]['price']);
        
        $cart->set($cartElements);
        $actionCount++;
    }
    
}
// сортируем полученный масси, что бы цены шли по возростанию в нём (от меньшего к большему)
asort($itemIdPrice);
// формируем новый массив, что бы каждый его элемент содержал ключ товара и его цену
foreach($itemIdPrice as $key => $item) {
    $sortMassPrice[] = array("key" => $key, "price" => $item, "id" => $cartElements[$key]["id"]);
}
//$modx->log(1,print_r($sortMassPrice,1), 'HTML');

$option["action"] = "promo50";
$itemCountAction = floor($totalCount/3);
$numCheck = 0;

$ms2 = $modx->getService('miniShop2');

if(($actionCount > $itemCountAction && $itemCountAction >= 3) || $actionCount == 0) {
    for($i = $totalCount; $i >= 3; $i = $i - 3){
        
        if($cartElements[$sortMassPrice[$numCheck]["key"]]['count'] == 0) {
            $ms2->cart->remove($sortMassPrice[$numCheck]["key"]);
            $numCheck++;
        }
            
        $cartElements[$sortMassPrice[$numCheck]["key"]]['count'] = $cartElements[$sortMassPrice[$numCheck]["key"]]['count'] - 1;
        
        $cart->set($cartElements);
        
        $ms2->initialize($modx->context->key);
        $ms2->cart->add($sortMassPrice[$numCheck]["id"], 1, $option);
    }
}


/*switch ($modx->event->name) {

  case "msOnAddToCart":
    
  break;
  case "msOnChangeInCart":
    
  break;
  case "msOnRemoveFromCart":
    
  break;

}*/