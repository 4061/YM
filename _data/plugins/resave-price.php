id: 23
source: 1
name: resave_price
properties: 'a:0:{}'

-----

//$modx->log(modX::LOG_LEVEL_ERROR, 'проверка пост в плагине '.json_encode($_POST));
switch ($modx->event->name) {
    case 'msOnAddToCart':
        $tmp = $cart->get();
        $keys = (array_keys($tmp));
        
        foreach ($keys as $key){
            
            $currentPrice = $tmp[$key]['price'];
            $tmp[$key]['original_price'] = $tmp[$key]['price'];
            $id = $tmp[$key]['id'];
            $token = getallheaders();
            
            //$modx->log(1, "Ключ: ".$key);
            
            if ($token['Authorization']) $token2 = $token['Authorization'];
            
            $token = substr($token2, 7);
            
            if($modx->getOption("app_pricy_type") == "userprice"){
                $sql = 'SELECT state FROM modx_user_attributes WHERE website = "'.$token.'"';
                $output = $modx->query($sql);
                $price_type = $output->fetch(PDO::FETCH_ASSOC)['state'];
            
                if (!$price_type){
                    $price_type = $modx->getOption("add_default_type_price");
                }
            } else if($modx->getOption("app_pricy_type") == "cityprice"){
                switch($_POST['JSON']["order_type"]){
                    case 1:
                        $tvType = 37;
                    break;
                    case 2:
                        $tvType = 41;
                    break;
                    case 4:
                        $tvType = 41;
                    break;
                    case 3:
                        $tvType = 40;
                    break;
                }
                $sql = '
                    SELECT 
                        value
                    FROM 
                        modx_site_tmplvar_contentvalues 
                    WHERE 
                        tmplvarid = '.$tvType.' AND contentid = '.$_POST['JSON']["id_org"];
                            
                $output = $modx->query($sql);
                $city = $output->fetch(PDO::FETCH_ASSOC)['value'];
                $price_type = ($city)? $city : 1;
                
                $sql = '
                    SELECT 
                        value
                    FROM 
                        modx_site_tmplvar_contentvalues 
                    WHERE 
                        tmplvarid = 41 AND contentid = '.$_POST['JSON']["id_org"];
                            
                $output = $modx->query($sql);
                $city = $output->fetch(PDO::FETCH_ASSOC)['value'];
                $price_type_original = ($city)? $city : 1;
            } else {
                $price_type = $price_type_original = 0;
            }
            
            //$modx->log(1,"id_org: ".$_POST['JSON']["id_org"].", и тип цены: ".$price_type." (".$tvType.")");
            
            if($price_type != 0) {
                $sql = "
                    SELECT 
                        price
                    FROM 
                        modx_softjetsync_prices
                    WHERE 
                        modx_softjetsync_prices.product_id = '$id' AND modx_softjetsync_prices.type = '$price_type'
                    ";
                $info_product = $modx->query($sql);
                $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                $newprice = $info_product[0]["price"];
                $tmp[$key]['price'] = round((int)$newprice);
                
                $sql = "
                    SELECT 
                        price
                    FROM 
                        modx_softjetsync_prices
                    WHERE 
                        modx_softjetsync_prices.product_id = '$id' AND modx_softjetsync_prices.type = '$price_type_original'
                    ";
                $info_product = $modx->query($sql);
                $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                $newprice = $info_product[0]["price"];
                $tmp[$key]['original_price'] = round((int)$newprice);
                
                if($_POST['JSON']["order_type"] == 1 && ($tmp[$key]['price'] == $tmp[$key]['original_price'])){
                    // получаем переменную товара которая показывает, является ли товар "комбо"
                    if($itemObject->get("is_comobo")) {
                        // если да, то цена без изменений
                    } else {
                        // если нет, то для самовывоза пересчитываем цену
                        $tmp[$key]['price'] = round($tmp[$key]['price'] - (($tmp[$key]['price'] * (int)$modx->getOption("app_discount_percent"))/100));
                    }
                    
                }
                
                if(count($tmp[$key]["options"]["childs"]["size"])) {
                    $modyfication = $tmp[$key]["options"]["childs"]["size"][0]["price"];
                    $modyfication += (count($tmp[$key]["options"]["childs"]["type"]))? (int)$tmp[$key]["options"]["childs"]["type"][0]["price"] : 0;
                    $tmp[$key]['price'] = $tmp[$key]['original_price'] - $modyfication;
                    $tmp[$key]['price'] = round((int)$tmp[$key]['price'] - (((int)$tmp[$key]['price'] * (int)$modx->getOption("app_discount_percent"))/100)) + $modyfication;
                }
                
            } else {
                // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
                if($_POST['JSON']["order_type"] == 1 && $modx->getOption("app_discount_percent") && $modx->getOption("app_pricy_type") == "price") {
                    $itemObject = $modx->getObject("msProduct",$id);
                    // получаем переменную товара которая показывает, является ли товар "комбо"
                    if($itemObject->get("is_comobo")) {
                        // если да, то цена без изменений
                        $tmp[$key]["price"] = (int)$itemObject->get("price");
                    } else {
                        $tmp[$key]["original_price"] = (int)$itemObject->get("price");
                        // если нет, то для самовывоза пересчитываем цену
                        $tmp[$key]["price"] = round((int)$itemObject->get("price") - (((int)$itemObject->get("price") * (int)$modx->getOption("app_discount_percent"))/100));
                    }
                } else {
                    $itemObject = $modx->getObject("msProduct",$id);
                    $tmp[$key]["original_price"] = $tmp[$key]["price"] = (int)$itemObject->get("price");
                }
            }
            
        }
        //$modx->log(1, "Ключ: ".json_encode($tmp));
        $cart->set($tmp);
    break;
}