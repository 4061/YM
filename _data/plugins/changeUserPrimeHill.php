id: 45
source: 1
name: changeUserPrimeHill
properties: 'a:0:{}'

-----

if ($modx->getOption("loyalty_PH_program")) {
    
    $token = $modx->getOption("prime_hill_token");
    $profile = $userprofile;
    
    $birthday = ($profile->dob)? date("Y-m-d", $profile->dob) : "";
    $user_array["clients"][] = array(
        "clientId" => $profile->zip,
        "lastName" => "",
        "firstName" => $profile->fullname,
        "patronymic" => "",
        "email" => $profile->email,
        "tags" => [],
        "sex" => 1,
        "cardBarcode" => "",
        "cardNumber" => "",
        "birthday"=> $birthday , 
        "templateId"=>4374, //айди шаблона. пока непонятно что это, но очень важно
        "phone"=>$profile->mobilephone,
        "comment" => "Нет данных"
        );
    //return $user_array;
    //return json_encode($user_array,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    $vars["token"] = $token;
    $response = $modx->client->request('POST', 'https://cabinet.prime-hill.com/api/v2/updateClients?'.http_build_query($vars), ['json'=>$user_array]);

}