id: 44
source: 1
name: usingPromo
description: 'Использование промокода пользователем'
category: miniShop2
properties: 'a:0:{}'

-----

switch ($modx->event->name) {
  case 'msOnChangeOrderStatus':
      // проверка, включена ли настройка на отправку писем на почту менеджера
    if ($status==14 || $status==1) {
        // получаем данные заказа и преобразуем полученный объект в массив
        $addressData = $modx->getObject('msOrderAddress',$order->address);
        $addressData = $addressData->toArray();
        // получаем код промокода из массива данных
        $code = $addressData["referalDiscount"];
        //$modx->log(1,"Реферальный код: ".$addressData["referalDiscount"]);
        // получаем объект промокода
        if($promoObject = $modx->getObject("PromoBase",array("code" => $code))){
        
            $idUser = $addressData["user_id"];              // ID пользователя
            $idPromo = $promoObject->get("id_promo");       // ID промокода
            $use_count = $promoObject->get("use_count");    // количество использований
            $promo_availability = $promoObject->get("availability");        // Тип промокода
            $all_using = $promoObject->get("already_used"); // сколько раз уже использовали промокод пользователи
            $userOnly = $promoObject->get("user_only"); // параметр отвечающий за то считаем мы индивидуально для пользователя или для всех сразу
            
            //$modx->log(1,"Только для пользователя: ".$userOnly);
            
            // проверяем, является ли данный промокод личным (тип = 2)
            if($promo_availability == 2 || $userOnly == 1) {
                
                // формируем массив для выборки
                $where = array("id_user" => $idUser,"id_promo" => $idPromo);
                // получаем объект по указанным параметрам
                if($checkObject = $modx->getObject("PromoUsingUsers",$where)) {
                    // если он найден, то увеличиваем количество применений
                    $count = $checkObject->get("using_count");
                    $count++;
                    $checkObject->set("using_count",$count);
                    //echo "Редактируем (".$checkObject->get("id").")";
                } else {
                    // если он не найден, то создаём новый элемент в таблице БД
                    $checkObject = $modx->newObject("PromoUsingUsers");
                    $checkObject->set("id_user",$idUser);
                    $checkObject->set("id_promo",$idPromo);
                    $checkObject->set("using_count",1);
                    //echo "Создаём";
                }
                // сохраняем данные для объекта
                $checkObject->save();
                
            } else {
                // если промокод общий
                // увеличиваем счётчик и сохраняем изменения
                $all_using++;
                $promoObject->set("already_used",$all_using);
                $promoObject->save();
            }
        }
    }
    break;
}