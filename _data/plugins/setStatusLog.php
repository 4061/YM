id: 29
source: 1
name: setStatusLog
description: 'Логирует изменение статусов'
properties: 'a:0:{}'

-----

$item = $modx->newObject('Ms2OrderStatusesLogs');
$item->set("order_id", $order->id);
$item->set("next_status_id", $order->get("status"));
$this_status = $order->get("status");
$sql = "SELECT name FROM `modx_ms2_order_statuses` WHERE id = '$this_status'";
$this_status_name = $modx->query($sql);
$this_status_name = $this_status_name->fetchAll(PDO::FETCH_ASSOC);
$this_status_name = $this_status_name[0]['name'];
$item->set("next_status_name", $this_status_name);
$item->save();