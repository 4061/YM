id: 27
source: 1
name: PushChangeOrderStatus
description: 'Записывает пользователя в БД если заказ проведен успешно, а так же начисляем бонусы реферальному пользователю'
properties: 'a:0:{}'

-----

// Switch Event
switch ($modx->event->name) {
    case 'msOnChangeOrderStatus':
        // событие на статус заказа "ГОТОВ"
        if ($status == 13) {
            $user_id = $order->get('user_id');
            $order_id = $order->get('id');
           // $modx->log(modX::LOG_LEVEL_ERROR, 'плагин работает: '.$id);
            $sql="INSERT INTO `modx_confirmed_orders_users` (`id`, `user_id`, `order_id`, `status`) VALUES (NULL, '$user_id', '$order_id', '0')";
            $modx->query($sql);
            
            // ======== Блок отвечающий за начисление бонусов по реферальному промокоду
            // получаем адрес покупателя и формируем из него массив
            $addressData = $modx->getObject('msOrderAddress',$order->address);
            $addressData = $addressData->toArray();
            // получаем код промокода из массива данных
            $code = $addressData["referalDiscount"];
            
            // получаем объект промокода
            if($promoObject = $modx->getObject("PromoBase",array("code" => $code))){
                // если промокод является реферальным
                if($promoObject->get("is_referal")){
                    // получаем настройку для типа начисления бонусов для реферальной ссылки
                    switch($modx->getOption("app_add_referal_bonuses")){
                        // начисление внутри системы
                        case 1:
                            // получаем ID хозяена реферальной ссылки и количество начисляемых бонусов
                            $userReferal = $promoObject->get("author_id");
                            $referalBonus = $promoObject->get("author_bonus");
                            
                            //$modx->log(1,"Статус заказа ГОТОВ! - ".$code."/".$promoObject->get("author_bonus"));
                            // получаем сервис для начисления бонусов
                            $msb2 = $modx->getService('msbonus2', 'msBonus2', MODX_CORE_PATH . 'components/msbonus2/model/msbonus2/');
                            $msb2->initialize($modx->context->key);
                            $manager = $msb2->getManager();
                            // начисляем бонусы пользователю
                            $manager->setPlus('Начисление по реферальному коду', $referalBonus, $userReferal, 11, $userReferal);
                        break;
                        // начисления через ПраймХилл
                        case 2:
                            /*$token = 1;
                            $id = 2;
                            
                            // тело запроса
                            $query = array();
                            
                            $url = 'https://cabinet.prime-hill.com/api/v2/createOrder?token='.$token.'&type=clientId&id='.$id;
                            
                            $myCurl = curl_init();
                            curl_setopt_array($myCurl, array(
                                CURLOPT_URL => $url,
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_POST => true,
                                CURLOPT_POSTFIELDS => http_build_query($query)
                            ));
                            $response = curl_exec($myCurl);
                            curl_close($myCurl);*/
                        break;
                    }
                    
                    // ====== push-уедомление хозяина реферальной ссылки =====
                    // получаем ID пользователя
                    $userReferal = $promoObject->get("author_id");
                    // получаем объект пользователя реферала
                    $userObject = $this->modx->getObject("modUserProfile",array("internalKey"=>$userReferal));
                    // получаем ключ для отправки push-уведомления данному пользователю
                    $fireToken = $userObject->get("city");
                    // если ключ есть..
                    if($fireToken) {
                        // основые настройки запроса к сервису
                        $url = 'https://fcm.googleapis.com/fcm/send';
                        $YOUR_API_KEY = $this->modx->getOption('ms2_push_token'); // Server key
                        $request_headers = [
                            'Content-Type: application/json',
                            'Authorization: key=' . $YOUR_API_KEY,
                            'apns-push-type: alert'
                        ];
                        
                        // запускаем метод для подгрузки шаблона, если он есть
                        $letter = array(
                            "subject" => "Уведомление",
                            "body" => "Вашим реферальным кодом воспользовались, вам начислено ".$promoObject->get("author_bonus")." баллов",
                        );
                        
                        // JSON массив для запроса PUSH
                        $JSONcode = '{
                            "to":"'.$fireToken.'",
                            "notification": {
                                "sound":"default",
                                "title": "'.$letter["subject"].'",
                                "body":"'.$letter['body'].'",
                                "content_available": true,
                                "priority": "high",
                                "high_priority": "high",
                                "show_in_foreground": true
                            },
                            "adnroid": {
                                "tag": "location",
                                "priority": "high"
                            },
                            "data": {
                                "id": 4,
                                "action": "logout",
                                "sound": "default",
                                "body": "test body",
                                "title": "test title",
                                "content_available": true,
                                "priority": "high"
                            },
                            "apns": {
                                "headers": {
                                    "apns-push-type": "background",
                                    "apns-priority": "10",
                                    "apns-collapse-id": "location"
                                },
                                "payload": {
                                    "aps": {
                                        "apns-collapse-id": "location",
                                        "contentAvailable": true
                                    }
                                }
                            }
                        }';
                        
                        $request_body1 = json_decode($JSONcode);
                        
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                        $response = curl_exec($ch);
                        curl_close($ch);
                        $this->modx->log(1,"рассылка на токены: ".$fireToken.", заголовок: ".$letter["subject"].", текст: ".$letter['body'].", ответ сервера:".print_r($response,1));
                            

                    }
                    
                }
            }
        }
    break;
}