id: 22
source: 1
name: usePromoCode
description: 'Логика промокодов'
properties: 'a:0:{}'
disabled: 1

-----

$contacts = $modx->getObject('msOrderAddress', array('id'=> $msOrder->address));
$orderId = $msOrder ->get('id');
$delivery_cost = $msOrder->delivery_cost;
$codes = $contacts->get('referalDiscount');
$codes = explode(", ", $codes); //получаем все промокоды, активированные в заказе
foreach ($codes as $code){ //проходимся по каждому из них
    if ($code != ""){
        $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
        $current_code = $modx->query($sql);
        $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
        $type = $current_code[0]['type'];
        if ($type == 3){
            $msOrder->set('cost',$_SESSION['minishop2']['discount_cost']+$delivery_cost); //делаем скидку
            $msOrder->save();
            $current_code = $modx->getObject("PromoBase", $current_code[0]["id_promo"]);
            $current_code->set("use_count", $current_code->get("use_count") - 1); //вычитаем одно использование
            $current_code->save(); //сейвим
            $bonus = $modx->getObject('msb2User', array("user"=>$current_code->get("author_id")));
            $bonus_actual_count = $bonus->get('points');
            $bonus_new_count = $bonus_actual_count + 50;
            $bonus->set('points', $bonus_new_count); 
            $bonus->save(); //начисляем бонусы автору
            /*
            $sql = "SELECT user_id FROM modx_ms2_orders WHERE id = '$orderId'";
            $user_id = $modx->query($sql);
            $user_id = $user_id->fetchAll(PDO::FETCH_ASSOC);
            $user_id = $user_id[0]['user_id']; //получаем юзера, который сделал заказ
            $sql = "SELECT * FROM modx_promo_users WHERE type=3 AND user_id = '$user_id' ORDER BY `modx_promo_users`.`use_time` DESC";
            $last_use = $modx->query($sql);
            $last_use = $last_use->fetchAll(PDO::FETCH_ASSOC);
            $use_time = $last_use[0]['use_time'];
            $promo_id = $last_use[0]['promo_id'];
            $sql = "
                UPDATE
                    modx_promo_users
                SET
                    activation_status=1,
                    order_id = '$orderId'
                WHERE   
                    user_id = '$user_id' AND
                    type = 3 AND
                    use_time = '$use_time'
            ";
            $modx->query($sql); //делаем так, чтоб скидкой он больше не воспользовался
            $sql = "SELECT author_id,use_count FROM modx_promo_base WHERE id_promo='$promo_id'";
            $author_id = $modx->query($sql);
            $author_id = $author_id->fetchAll(PDO::FETCH_ASSOC);
            $use_count = $author_id[0]['use_count'];
            $author_id = $author_id[0]['author_id'];
            $bonus = $modx->getObject('msb2User', array("user"=>$author_id));
            $bonus_actual_count = $bonus->get('points');
            $bonus_new_count = $bonus_actual_count + 50;
            $bonus->set('points', $bonus_new_count); 
            $bonus->save(); //начисляем бонусы автору
            $use_count = $use_count-1;
            $sql = "
                UPDATE
                    modx_promo_base
                SET 
                    use_count = '$use_count'
                WHERE
                    id_promo = '$promo_id' 
                ";
            $modx->query($sql);*/
        }
        if ($type == 1 || $type == 5){
            $msOrder->set('cost',$_SESSION['minishop2']['discount_cost']); //делаем скидку
            $msOrder->save();
        }
    }
}
/*$contacts = $modx->getObject('msOrderAddress', array('id'=> $msOrder->address));
$orderId = $msOrder ->get('id');
$codes = $contacts->get('referalDiscount');
$codes = explode(", ", $codes); //получаем все промокоды, активированные в заказе
foreach ($codes as $code){ //проходимся по каждому из них
    $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
    $current_code = $modx->query($sql);
    $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
    $type = $current_code[0]['type'];
    if ($type == 2){
        $sql = "SELECT user_id FROM modx_ms2_orders WHERE id = '$orderId'";
        $user_id = $modx->query($sql);
        $user_id = $user_id->fetchAll(PDO::FETCH_ASSOC);
        $user_id = $user_id[0]['user_id']; //получаем юзера, который сделал заказ
        $max_use  = $current_code[0]['user_use_count']; // получаем макисмальное кол-во использований этого промокода пользователем..
        $promo_id = $current_code[0]['id_promo'];
        $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$user_id' AND promo_id = '$promo_id'";
        $countCheck = $modx->query($sql);
        $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
        if (count($countCheck) < $max_use){ //проверяем, не исчерпан ли лимит использования
            $sql = "SELECT * FROM modx_promo_users WHERE type=2 AND user_id = '$user_id' ORDER BY `modx_promo_users`.`use_time` DESC";
            $last_use = $modx->query($sql);
            $last_use = $last_use->fetchAll(PDO::FETCH_ASSOC);
            $use_time = $last_use[0]['use_time'];
            $activation_status = $last_use[0]['activation_status'];
            if ($activation_status != 1){
                $sql = "
                    UPDATE
                        modx_promo_users
                    SET
                        activation_status=1,
                        order_id = '$orderId'
                    WHERE   
                        user_id = '$user_id' AND
                        type = 2 AND
                        use_time = '$use_time'
                ";
                $modx->query($sql);   
            }
        }
    }
    if ($type == 1){ //если тип промокода "начисление скидки"
        $sql = "SELECT user_id FROM modx_ms2_orders WHERE id = '$orderId'";
        $user_id = $modx->query($sql);
        $user_id = $user_id->fetchAll(PDO::FETCH_ASSOC);
        $user_id = $user_id[0]['user_id']; //получаем юзера, который сделал заказ
        $max_use  = $current_code[0]['user_use_count']; // получаем макисмальное кол-во использований этого промокода пользователем..
        $promo_id = $current_code[0]['id_promo'];
        $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND user_id = '$user_id' AND promo_id = '$promo_id'";
        $countCheck = $modx->query($sql);
        $countCheck = $countCheck->fetchAll(PDO::FETCH_ASSOC);
        if (count($countCheck) < $max_use){ //проверяем, не исчерпан ли лимит использования
            $sql = "SELECT * FROM modx_promo_users WHERE type=1 AND user_id = '$user_id' ORDER BY `modx_promo_users`.`use_time` DESC";
            $last_use = $modx->query($sql);
            $last_use = $last_use->fetchAll(PDO::FETCH_ASSOC);
            $use_time = $last_use[0]['use_time'];
            $activation_status = $last_use[0]['activation_status'];
            if ($activation_status != 1){
                $cost = $msOrder->get('cost');
                $discount = $current_code[0]['child_discount'];
                $discount_percent = $discount/100; //переводим скидку в проценты
                $newcost = $cost-($cost*$discount_percent); //новая цена с учетом скидки
                $msOrder->set('cost',$newcost); //устанавливаем новую цену в заказ
                $msOrder->save(); //сохраняем
                $contacts->set('discount',$cost-$newcost);
                $contacts->save();
                $sql = "
                    UPDATE
                        modx_promo_users
                    SET
                        activation_status=1,
                        order_id = '$orderId'
                    WHERE   
                        user_id = '$user_id' AND
                        type = 1 AND
                        use_time = '$use_time'
                ";
                $modx->query($sql);
            }
        }
    }
    if ($type == 3){ //если это реферальная скидка
        $sql = "SELECT user_id FROM modx_ms2_orders WHERE id = '$orderId'";
        $user_id = $modx->query($sql);
        $user_id = $user_id->fetchAll(PDO::FETCH_ASSOC);
        $user_id = $user_id[0]['user_id']; //получаем юзера, который сделал заказ
        $sql = "SELECT user_id FROM modx_promo_users WHERE activation_status = 1 AND type = 3 AND user_id = '$user_id'";
        $referalCheck = $modx->query($sql);
        $referalCheck = $referalCheck->fetchAll(PDO::FETCH_ASSOC);
        if (count($referalCheck) == 0){ //проверка на всякий случай
            $cost = $msOrder->get('cost');
            $costfourth = $cost * 0.25;
            if ($costfourth>=200){
                $actual_discount = 200;
            } else {
                $actual_discount = $costfourth;
            }
            $cost = $cost - $actual_discount;
            $msOrder->set('cost',$cost); //делаем скидку
            $msOrder->save();
            $contacts->set('discount',$actual_discount);
            $contacts->save();
            $orderId = $msOrder->get('id');
            $sql = "SELECT * FROM modx_promo_users WHERE type=3 AND user_id = '$user_id' ORDER BY `modx_promo_users`.`use_time` DESC";
            $last_use = $modx->query($sql);
            $last_use = $last_use->fetchAll(PDO::FETCH_ASSOC);
            $use_time = $last_use[0]['use_time'];
            $promo_id = $last_use[0]['promo_id'];
            $sql = "
                UPDATE
                    modx_promo_users
                SET
                    activation_status=1,
                    order_id = '$orderId'
                WHERE   
                    user_id = '$user_id' AND
                    type = 3 AND
                    use_time = '$use_time'
            ";
            $modx->query($sql); //делаем так, чтоб скидкой он больше не воспользовался
            $sql = "SELECT author_id,use_count FROM modx_promo_base WHERE id_promo='$promo_id'";
            $author_id = $modx->query($sql);
            $author_id = $author_id->fetchAll(PDO::FETCH_ASSOC);
            $use_count = $author_id[0]['use_count'];
            $author_id = $author_id[0]['author_id'];
            $bonus = $modx->getObject('msb2User', array("user"=>$author_id));
            $bonus_actual_count = $bonus->get('points');
            $bonus_new_count = $bonus_actual_count + 50;
            $bonus->set('points', $bonus_new_count); 
            $bonus->save(); //начисляем бонусы автору
            $use_count = $use_count-1;
            $sql = "
            UPDATE
                modx_promo_base
            SET 
                use_count = '$use_count'
            WHERE
                id_promo = '$promo_id' 
            ";
            $modx->query($sql);
        } else {
            $contacts->set('referalDiscount','');
            $contacts->save();
        }
    }
}*/