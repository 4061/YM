<?php
require MODX_BASE_PATH.'vendor/autoload.php'; //Подключаем библиотеку
use Telegram\Bot\Api;
class TigrusBot {
    public function __construct($modx){
        $this->modx = $modx;
    }
    public function sendMessage($id){
        $address_object = $this->modx->getObject("UserAddresses", $_GET["id"]);
        $profile_object = $this->modx->getObject("modUserProfile", array("internalKey"=>$address_object->user_id));
        $message = $address_object->toArray();
        $message = json_encode($message);
        $obj = $this->modx->newObject("TigrusMessages");
        $obj->save();

$message = 
"ЗАЯВКА #$obj->id
--------------
Страна: $address_object->country
Город: $address_object->city
Улица: $address_object->street
Номер дома: $address_object->house_num
Номер квартиры: $address_object->flat_num
Подъезд: $address_object->entrance
Этаж: $address_object->floor

Пользователь: $profile_object->fullname
Номер: $profile_object->mobilephone
Email: $profile_object->email
";
        $telegram = new Api('1795550037:AAGgdHICnwUU6w5ulNXz39B9PSy8sGN8CLg');
        $info_user = $this->modx->query("SELECT * FROM `modx_tigrusbot`");
        $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
        $collection = $this->modx->getCollection("TigrusMessages", array("status"=>0));
        foreach ($collection as $item){
            $keyboard[] = ["Принять заказ #".$item->id];
        }
        $reply_markup = $telegram->replyKeyboardMarkup([ 'keyboard' => $keyboard,  'resize_keyboard' => true]);
        foreach ($info_user as $chat_id){
            $id = $chat_id['chat_id'];
            $telegram->sendMessage([ 'chat_id' => $id, 'parse_mode'=> 'HTML', 'text' => $message, 'reply_markup' => $reply_markup]);
        }
        return $info_user;
    }
}