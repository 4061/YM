<?php
class User extends ModxApi{
    public function getBonusScreen($profile){
        if ($this->modx->getOption("loyalty_PH_program")){
            $response = $this->prime_hill->getClient($profile);
            if ($response){
                $json['total_count'] = $response["bonusBalance"];
            } else {
                $json['total_count'] = $bonus_obj->points;
            }
        } else {
            $json['total_count'] = $bonus_obj->points;
        }
        $json["description"] = "Бонусы можно использовать в течение 60 дней после начисления. По истечении 60 дней они сгорают.";
        $json["cancelling"] = array(
            "date" => "26 июн",
            "value" => 420
        );
        $json["data"][] = array(
            "date" => "25 июня",
            "items" => array(
                array(
                    "title" => "ЯМ на Опалихинской",
                    "description" => "Заказ 12331 на 329 руб",
                    "value" => "+32.50",
                    "status" => "Accrual"
                ),
                array(
                    "title" => "ЯМ на Опалихинской",
                    "description" => "Заказ 12331 на 329 руб",
                    "value" => "-32.50",
                    "status" => "Writeoff"
                )  
            ),
        );
        return $json;
    }
    //сохраняет адрес
    public function saveUserAddress(){
        // получаем профиль пользователя
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        // формируем параметры запроса для проверки существования адреса
        $where = array(
            "country" => $this->modx->request["country"],
            "city" => $this->modx->request["city"],
            "street" => $this->modx->request["street"],
            "house_num" => $this->modx->request["house_num"],
            //"flat_num" => $this->modx->request["flat_num"],
            //"entrance" => $this->modx->request["entrance"],
            //"floor" => $this->modx->request["floor"],
            //"doorphone" => $this->modx->request["doorphone"],
            "user_id" => $profile->internalKey,
        );
        // получаем адрес по параметрам
        $addresses = $this->modx->getObject("UserAddresses",$where);
        // если адрес отсутствует, то создаём его
        if(!$addresses){
            $new_user_address = $this->modx->newObject("UserAddresses"); 
            $new_user_address->set("country", $this->modx->request["country"]);
            $new_user_address->set("city", $this->modx->request["city"]);
            $new_user_address->set("street", $this->modx->request["street"]);
            $new_user_address->set("house_num", $this->modx->request["house_num"]);
            $new_user_address->set("flat_num", $this->modx->request["flat_num"]);
            $new_user_address->set("entrance", $this->modx->request["entrance"]);
            $new_user_address->set("floor", $this->modx->request["floor"]);
            $new_user_address->set("doorphone", $this->modx->request["doorphone"]);
            $new_user_address->set("user_id", $profile->internalKey);
            $new_user_address->set("id_org", $this->modx->request["id_org"]);
            $new_user_address->set("delivery_exists", $this->modx->request["delivery_exists"]);
            if($this->modx->getOption("app_disable_recipient")) {
                $new_user_address->set("receiver_name", $this->modx->request["receiver_name"]);
                $new_user_address->set("recipients_phone", $this->modx->request["recipients_phone"]);
                $new_user_address->set("call_the_recipient", $this->modx->request["call_the_recipient"]);
                $new_user_address->set("delivery_date", $this->modx->request["delivery_date"]);
                $new_user_address->set("delivery_time", $this->modx->request["delivery_time"]);
                $new_user_address->set("card_text", $this->modx->request["card_text"]);
            }
            $hidden = ($this->modx->request["delivery_exists"])? false : true ;
            $new_user_address->set("hidden", $hidden);
            $new_user_address->save();
        } else {
            // адрес присутствует, то меняем в нём данные
            $addresses->set("flat_num", $this->modx->request["flat_num"]);
            $addresses->set("entrance", $this->modx->request["entrance"]);
            $addresses->set("floor", $this->modx->request["floor"]);
            $addresses->set("doorphone", $this->modx->request["doorphone"]);
            $addresses->set("user_id", $profile->internalKey);
            $addresses->set("id_org", $this->modx->request["id_org"]);
            $addresses->set("delivery_exists", $this->modx->request["delivery_exists"]);
            if($this->modx->getOption("app_disable_recipient")) {
                $addresses->set("receiver_name", $this->modx->request["receiver_name"]);
                $addresses->set("recipients_phone", $this->modx->request["recipients_phone"]);
                $addresses->set("call_the_recipient", $this->modx->request["call_the_recipient"]);
                $addresses->set("delivery_date", $this->modx->request["delivery_date"]);
                $addresses->set("delivery_time", $this->modx->request["delivery_time"]);
                $addresses->set("card_text", $this->modx->request["card_text"]);
            }
            
            $hidden = ($this->modx->request["delivery_exists"])? false : true ;
            $addresses->set("hidden", $hidden);
            
            $addresses->save();
            
            $new_user_address = $addresses;
        }
        // возвращаем добавленный адрес
        $arr = $new_user_address->toArray();
        $arr["hidden"] = (bool)$arr["hidden"];
        $arr["delivery_exists"] = (bool)$arr["delivery_exists"];
        $arr["id"] = (int)$arr["id"];
        $arr["id_org"] = (int)$arr["id_org"];
        $arr["user_id"] = (int)$arr["user_id"];
        return $arr;
    }
    public function getUserAddresses(){
        // получаем токен для поиска пользователя
        $where = array(
            'website' => $this->modx->token
        );
        // получаем профиль пользователя по токену
        $profile = $this->modx->getObject('modUserProfile', $where);
        // формируем параметры для выборки адресов
        $user_addresses = array(
            'class' => 'UserAddresses',
            'where' => ["user_id" => $profile->internalKey],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        // проверяем системную настройку, нужно ли нам подгружать только те товары на которые осуществляется доставка
        if($this->modx->getOption("app_delivery_exists")) $user_addresses['delivery_exists'] = 1;
        // получаем адреса
        $this->pdoFetch->setConfig($user_addresses);
        $user_addresses = $this->pdoFetch->run();
        foreach ($user_addresses as $key => $user_address){
            $user_addresses[$key]["hidden"] = (bool)$user_addresses[$key]["hidden"];
            $user_addresses[$key]["id"] = (int)$user_addresses[$key]["id"];
            $user_addresses[$key]["id_org"] = (int)$user_addresses[$key]["id_org"];
            $user_addresses[$key]["user_id"] = (int)$user_addresses[$key]["user_id"];
            $user_addresses[$key]["delivery_exists"] = (bool)$user_addresses[$key]["delivery_exists"];
            if(!$this->modx->getOption("app_disable_recipient")) {
                unset($user_addresses[$key]["receiver_name"],$user_addresses[$key]["recipients_phone"],$user_addresses[$key]["call_the_recipient"],$user_addresses[$key]["delivery_date"],$user_addresses[$key]["delivery_time"],$user_addresses[$key]["card_text"]);
            }
        }
        return $user_addresses;
    }
    //изменить адрес юзера
    public function changeUserAddress(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $user_address = $this->modx->getObject("UserAddresses", $this->modx->request["id"]);
        $user_address->set("country", $this->modx->request["country"]);
        $user_address->set("city", $this->modx->request["city"]);
        $user_address->set("street", $this->modx->request["street"]);
        $user_address->set("house_num", $this->modx->request["house_num"]);
        $user_address->set("flat_num", $this->modx->request["flat_num"]);
        $user_address->set("entrance", $this->modx->request["entrance"]);
        $user_address->set("floor", $this->modx->request["floor"]);
        $user_address->set("doorphone", $this->modx->request["doorphone"]);
        if($this->modx->getOption("app_disable_recipient")) {
            $user_address->set("receiver_name", $this->modx->request["receiver_name"]);
            $user_address->set("recipients_phone", $this->modx->request["recipients_phone"]);
            $user_address->set("call_the_recipient", $this->modx->request["call_the_recipient"]);
            $user_address->set("delivery_date", $this->modx->request["delivery_date"]);
            $user_address->set("delivery_time", $this->modx->request["delivery_time"]);
            $user_address->set("card_text", $this->modx->request["card_text"]);
        }
        $user_address->save();
        $arr = $user_address->toArray();
        $arr["hidden"] = (bool)$arr["hidden"];
        $arr["delivery_exists"] = (bool)$arr["delivery_exists"];
        $arr["id"] = (int)$arr["id"];
        $arr["id_org"] = (int)$arr["id_org"];
        $arr["user_id"] = (int)$arr["user_id"];
        return $arr;
    }
    public function removeUserAddress(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $user_address = $this->modx->getObject("UserAddresses", $this->modx->request["id"]);
        $user_address->set("hidden", true);
        $user_address->save();
        return $this->getUserAddresses();
    }
    public function getUser(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $birthday = date("d.m.Y",$profile->dob);
        $birthday = substr($birthday, 0, 10);
        if ($birthday == '01.01.1970'){
            $birthday = '';
        } else {
            $birthday = date("d.m.Y",$profile->dob);
        }
        $gender = $profile->gender;
        if ($gender == 2) {
            $gender = 'Женский';
        } elseif ($gender == 1){
            $gender = 'Мужской';
        } elseif ($gender == 0){
            $gender = 'Не указан';
        }
        $last_user_address = array(
            'class' => 'msOrderAddress',
            'where' => ["user_id" => $profile->internalKey],
            'sortdir' => "DESC",
            'return' => 'data',
            'limit' => 1,
        );
        $this->pdoFetch->setConfig($last_user_address);
        $last_user_address = $this->pdoFetch->run();
        $referal_obj = $this->modx->getObject("PromoBase", ["author_id"=>$profile->internalKey]);
        $bonus_obj = $this->modx->getObject('msb2User', array("user"=>$profile->internalKey));
        $json["data"]["name"] = $profile->fullname;
        $json["data"]["id"] = $profile->internalKey;
        $json["data"]["email"] = (explode("@",$profile->email)[1] == "apksite.ru")? "" : $profile->email;
        $json["data"]["gender"] = array("id"=>$profile->gender, "label"=>$gender);
        if ($profile->photo) $json["data"]["photo"] = $profile->photo;
        $json["data"]["bonus_card"] = "/uploads/barcodes/".$profile->internalKey."card.jpg";
        $json["data"]["birthday"] = $birthday;
        
        $json["data"]["referal"] = array();
        $json["data"]["referal"]["code"] = ($referal_obj->code)? $referal_obj->code : "";
        $json["data"]["referal"]["my_bonus"] = ($referal_obj->author_bonus)? $referal_obj->author_bonus : 0;
        $json["data"]["referal"]["friend_bonus"] = ($referal_obj->child_discount)? $referal_obj->child_discount : 0;
        $json["data"]["referal"]["description"] = "Отправьте реферальный код вашему другу. Он получит скидку на первый заказ, а вы получите бонусы на свой счет за первый оплаченный заказ вашего друга.";
        $json["data"]["referal"]["ref_link"] = "";
        
        $json["data"]["phone"] = $profile->mobilephone;
        if ($this->modx->getOption("loyalty_PH_program")){
            $response = $this->prime_hill->getClient($profile);
            if ($response){
                $json["data"]["bonus"] = $response["bonusBalance"];
                $json["data"]["qr_string"] = $response["cardBarcode"];
            } else {
                $json["data"]["bonus"] = $bonus_obj->points;
            }
        } elseif ($this->modx->getOption("tunec_loyalty_system")) {
            $tural = $this->integrations->getTuralActions(); //для работы с Prime Hill
            $response = $tural->getUserBonuses($profile);
            $json["data"]["bonus"] = $response["ОстатокБонусов"];
        } else {
            $json["data"]["bonus"] = $bonus_obj->points;
        }
        //$json["data"]["bonus"] = ($this->modx->getOption("loyalty_PH_program"))? $this->prime_hill->getClient($profile)["bonusBalance"] : $bonus_obj->points;
        $json["data"]["last_address"] = array(
            "street"=>$last_user_address[0]["street"],
            "entrance"=>$last_user_address[0]["entrance"],
            "floor"=>$last_user_address[0]["floor"],
            "house_num"=>$last_user_address[0]["house_num"],
            "flat_num"=>$last_user_address[0]["flat_num"],
            "city"=>$last_user_address[0]["city"],
            "country"=>$last_user_address[0]["country"],
        );
        if($this->modx->getOption("app_disable_recipient")) {
            $json["data"]["last_address"]["receiver_name"] = ($last_user_address[0]["receiver_name"])? $last_user_address[0]["receiver_name"] : "";
            $json["data"]["last_address"]["recipients_phone"] = ($last_user_address[0]["recipients_phone"])? $last_user_address[0]["recipients_phone"] : "";
            $json["data"]["last_address"]["call_the_recipient"] = ($last_user_address[0]["call_the_recipient"])? $last_user_address[0]["call_the_recipient"] : 0;
            $json["data"]["last_address"]["delivery_date"] = ($last_user_address[0]["delivery_date"])? $last_user_address[0]["delivery_date"] : "";
            $json["data"]["last_address"]["delivery_time"] = ($last_user_address[0]["delivery_time"])? $last_user_address[0]["delivery_time"] : "";
            $json["data"]["last_address"]["card_text"] = ($last_user_address[0]["card_text"])? $last_user_address[0]["card_text"] : "";
        }
        if (!$json["data"]["qr_string"]) $json["data"]["qr_string"] = "";
        return $json;
    }
    
    // Метод для получения картинки из BASE64
    public function getImageBase64($cobeBase64,$directory,$typeFile,$fileName){
        
        $data = str_replace('data:image/'.$typeFile.';base64,', '', $cobeBase64);
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $fileName = substr(str_shuffle($permitted_chars), 0, 10);
        $file = $directory . $fileName . '.'.$typeFile;
        
        // формируем директорию для сохранения картинки
    	$category = MODX_BASE_PATH.$directory;
    	// создаём директорию
    	if (!mkdir($category, 0766, true));
        
        $success = file_put_contents(MODX_BASE_PATH.$file, $data);
        $data = base64_decode($data);
        $source_img = imagecreatefromstring($data);
        $rotated_img = imagerotate($source_img, 90, 0);
        $imageSave = imagejpeg($rotated_img, MODX_BASE_PATH.$file, 10);
        imagedestroy($source_img);
        
        return $file;
    }
    
    public function changeUser(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $birthday = $this->modx->request['birthday'];
        $birthday = strtotime($birthday);
        $gender = $this->modx->request['gender'];
        if ($gender == "Мужской"){
            $gender=1;
        } elseif ($gender == "Женский"){
            $gender=2;
        }
        $image = $this->modx->request['image'];
        
        if($image){
            $directory = "uploads/users_img/";
            $image = $this->getImageBase64($image,$directory,"png",$profile->get("internalKey"));
            $profile->set('photo',$image);
        }
        
        $profile->set('fullname',$this->modx->request['name']);
        if($this->modx->request['email']) $profile->set('email',$this->modx->request['email']);
        $profile->set('gender', $gender);
        if ($birthday){
            $profile->set('dob', $birthday);
        }
        $profile->save(); //сохранили, осталось вернуть норм массив
        return $this->getUser();
    }
    public function setFirebaseToken(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $profile->set('city',$this->modx->request['fcm_token']); 
        $profile->save();
        $json["data"][] = [];
        return $json;
    }
    public function getOrdersHistory(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $sql = "
        SELECT 
            modx_ms2_orders.cost as total_cost,
            modx_ms2_orders.delivery_cost,
            modx_ms2_orders.id as order_id,
            modx_ms2_orders.delivery,
            modx_ms2_orders.status as status_id,
            modx_ms2_order_statuses.color as status_title_color,
            modx_ms2_order_statuses.general_wrapper_color as general_wrapper_color,
            modx_ms2_order_statuses.color_wrapper as status_color_wrapper,
            modx_ms2_order_statuses.description as status_name,
            modx_ms2_orders.createdon as createdon,
            modx_ms2_orders.rating as rating,
            modx_ms2_order_addresses.street as street,
            modx_ms2_order_addresses.city as city,
            modx_ms2_order_addresses.house_num as house_num,
            modx_ms2_order_addresses.flat_num as flat_num,
            modx_ms2_order_addresses.index as id_org,
            modx_ms2_order_addresses.commemtAssessment as comment
        FROM 
            modx_ms2_orders,modx_ms2_order_addresses,modx_ms2_order_statuses
        WHERE
            modx_ms2_order_addresses.id = modx_ms2_orders.address AND
            modx_ms2_orders.user_id = '$profile->internalKey' AND
            modx_ms2_order_statuses.id = modx_ms2_orders.status
        ORDER BY `modx_ms2_order_addresses`.`id` DESC
        ";
        $orders_history = $this->modx->query($sql);
        $json["orders"] = $orders_history->fetchAll(PDO::FETCH_ASSOC);
        foreach ($json['orders'] as $key => $order){
            $json['orders'][$key]["total_cost"] = (int)$order['total_cost'];
            $json['orders'][$key]["delivery_cost"] = (int)$order['delivery_cost'];
            unset($json['orders'][$key]["delivery_cost"]);
            $json['orders'][$key]["order_id"] = (int)$order['order_id'];
            $json['orders'][$key]["status_id"] = (int)$order['status_id'];
            $json['orders'][$key]["container_color"] = $order["general_wrapper_color"];
            $json['orders'][$key]["status_wrapper_color"] = $order["status_color_wrapper"];
            $json['orders'][$key]["status_title_color"] = "#FFFFFF";
            $json['orders'][$key]["createdon"] = date("d.m.Y H:i:s",strtotime($order['createdon']));
            if($this->modx->getOption("app_pass_rating")){
                $json['orders'][$key]["rating"] = ((int)$order['rating'])?(int)$order['rating']: 0;
                $json['orders'][$key]["comment"] = ($order['comment'])? $order['comment'] : "";
            } else {
                $json['orders'][$key]["rating"] = 0;
                $json['orders'][$key]["comment"] = "";
            }
            
            $json['orders'][$key]["street"] = ($json['orders'][$key]["street"])? $json['orders'][$key]["street"] : "";
            $json['orders'][$key]["city"] = ($json['orders'][$key]["city"])? $json['orders'][$key]["city"] : "";
            $json['orders'][$key]["house_num"] = ($json['orders'][$key]["house_num"])? $json['orders'][$key]["house_num"] : "";
            $json['orders'][$key]["flat_num"] = ($json['orders'][$key]["flat_num"])? $json['orders'][$key]["flat_num"] : "";
            $json['orders'][$key]["id_org"] = ($json['orders'][$key]["id_org"])? (int)$json['orders'][$key]["id_org"] : 0;
            
            if($json['orders'][$key]["delivery"] == 1 || $json['orders'][$key]["delivery"] == 3) {
                $org = $this->modx->getObject("modResource",$json['orders'][$key]["id_org"]);
                $json['orders'][$key]["address"] = ($org)? $org->longtitle : "";
            } else {
                $json['orders'][$key]["address"] = "";
                $json['orders'][$key]["address"] .= ($json['orders'][$key]["city"])? $json['orders'][$key]["city"] : "";
                $json['orders'][$key]["address"] .= ($json['orders'][$key]["address"] && $json['orders'][$key]["street"])? ", " : "";
                
                $json['orders'][$key]["address"] .= ($json['orders'][$key]["street"])? $json['orders'][$key]["street"] : "";
                $json['orders'][$key]["address"] .= ($json['orders'][$key]["address"] && $json['orders'][$key]["house_num"])? ", " : "";
                
                $json['orders'][$key]["address"] .= ($json['orders'][$key]["house_num"])? $json['orders'][$key]["house_num"] : "";
            }
            
        }
        return $json;
    }
    public function getUserOrder(){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $order_id = $_GET['order_id'];
        $sql = "
        SELECT 
            modx_ms2_orders.cost as cost,
            modx_ms2_orders.id as order_id,
            modx_ms2_orders.status as status_id,
            modx_ms2_order_statuses.color as status_color,
            modx_ms2_order_statuses.description as status_name,
            modx_ms2_orders.payment as payment_id,
            modx_ms2_payments.name as payment_name,
            modx_ms2_orders.createdon as createdon,
            modx_ms2_orders.comment as comment,
            modx_ms2_order_addresses.city as city,
            modx_ms2_order_addresses.index as id_org,
            modx_ms2_order_addresses.street as street,
            modx_ms2_order_addresses.floor as floor,
            modx_ms2_order_addresses.address_id,
            modx_ms2_order_addresses.region as region,
            modx_ms2_order_addresses.house_num as house_num,
            modx_ms2_order_addresses.flat_num as flat_num,
            modx_ms2_order_addresses.entrance as entrance,
            modx_ms2_order_addresses.referalDiscount as promocode,
            modx_ms2_order_addresses.discount as discount,
            modx_ms2_orders.rating as rating,
            modx_msbonus2_orders.writeoff as bonus_writeoff,
            modx_msbonus2_orders.accrual as bonus_accrual,
            modx_site_content.id as city_id,
            modx_ms2_deliveries.name as order_type_title,
            modx_ms2_orders.delivery_cost,
            modx_ms2_deliveries.id as order_type_id,
            modx_ms2_deliveries.map as order_type_is_remoted
        FROM 
            modx_ms2_orders,modx_ms2_order_addresses,modx_msbonus2_orders,modx_ms2_order_statuses,modx_ms2_payments,modx_ms2_deliveries,modx_site_content
        WHERE
            modx_site_content.pagetitle = modx_ms2_order_addresses.city AND
            modx_ms2_order_addresses.id = modx_ms2_orders.address AND
            modx_msbonus2_orders.order = modx_ms2_orders.id AND
            modx_ms2_order_statuses.id = modx_ms2_orders.status AND
            modx_ms2_payments.id = modx_ms2_orders.payment AND
            modx_ms2_deliveries.id = modx_ms2_orders.delivery AND
            modx_ms2_orders.id = '$order_id'
        ";
        $order = $this->modx->query($sql);
        $order = $order->fetchAll(PDO::FETCH_ASSOC);
        $additives = array();
        $original_total_cost = 0;
        $org_object = $this->modx->getObject("modResource",$order[0]["id_org"]);
        
        
        //$price_type = $org_object->getTVValue("tvTypeDelivery");
        
        
        if($this->modx->getOption("app_pricy_type") == "userprice"){
            if ($profile){
                $price_type = $profile->get("state");
            } else {
                $price_type = $this->modx->getOption("add_default_type_price");;
            }
            $price_type_original = $price_type;
        } else if($this->modx->getOption("app_pricy_type") == "cityprice") {
            if($this->modx->getOption("app_always_courier_price")) {
                $order_type_tv = "tvTypeDelivery";
            } else {
                switch ($_GET["order_type"]){
                    case '1': //самовывоз
                        $order_type_tv = "tvType";
                        break;
                    case '2': //курьер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '4': //менеджер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '3': //ресторан
                        $order_type_tv = "tvTypeRestourant";
                        break;
                }
            }
            
            $price_type = $org_object->getTVValue($order_type_tv);
            $price_type_original = $org_object->getTVValue("tvTypeDelivery");
        } else {
            $price_type = $price_type_original = 0;
        }
        
        
        for ($i = 0; $i<count($order);$i++){
            $order_id = $order[$i]['order_id'];
                        $sql = "SELECT
                        modx_ms2_order_products.name as title, 
                        modx_ms2_order_products.product_id, 
                        modx_ms2_order_products.count, 
                        modx_ms2_order_products.options,
                        modx_ms2_order_products.price,
                        modx_ms2_products.image as img_url
                    FROM 
                        modx_ms2_order_products, modx_ms2_products
                    WHERE 
                        modx_ms2_order_products.product_id = modx_ms2_products.id AND
                        modx_ms2_order_products.order_id = '$order_id'
                    "; // GROUP BY modx_ms2_order_products.name
            $info_product = $this->modx->query($sql);
            $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
            foreach ($info_product as $key=> $info_produc){
                $prod_id = (int)$info_product[$key]['product_id'];
                $info_product[$key]['product_id'] = (int)$info_product[$key]['product_id'];
                $info_product[$key]['count'] = (int)$info_product[$key]['count'];
                
                
                $info_product[$key]['img_url'] = $url.$info_product[$key]['img_url'];
                    $info_product[$key]['img_url'] = ($info_product[$key]['img_url'])? $this->modx->runSnippet('pthumb', [
                                            'input' => substr($info_product[$key]['img_url'], 1),
                                            'options' => '&h=300&w=300&q=100&bg=FFFFFF',
                                        ]) : '/assets/template/images/content/fri.jpg';
                $sql = "
                SELECT 
                    modx_ms2_product_options.value
                FROM 
                    modx_ms2_product_options 
                WHERE 
                    modx_ms2_product_options.key = 'energy_size' AND 
                    modx_ms2_product_options.product_id = '$prod_id'
                ";
                    $info_weight = $this->modx->query($sql);
                    $info_weight = $info_weight->fetchAll(PDO::FETCH_ASSOC);
                    $info_product[$key]['weight'] = (int)$info_weight[0]["value"];
                    
                    if($price_type != 0) {
                        $price_object = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$info_product[$key]['product_id'], "type"=>$price_type));
                        $price = $price_object->price;
                    } else {
                        $price = $info_product[$key]['price'];
                    }
                    
                    //$price_object = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$info_product[$key]['product_id'], "type"=>$price_type));
                    $original_total_cost += $price * $info_product[$key]['count'];
                    $info_product[$key]['price'] = $price;
            }
            $arr = array();
            foreach ($info_product as $key => $product){
                $productOptions = json_decode($product['options'], true);
                $adds1 = $productOptions['childs']['additives'];
                $adds2 = $productOptions['childs']['type'];
                $adds3 = $productOptions['childs']['size'];
                foreach ($adds3 as $size){
                    $original_total_cost = ($original_total_cost - ($size["price"]*$product["count"]));
                }
                $adds = array_merge($adds1, $adds2, $adds3);
                foreach ($adds as $add){
                    array_push($additives, $add["id"]);
                }
            }
            for ($j = 0; $j<count($info_product);$j++){
                $info_product[$j]['options'] = json_decode($info_product[$j]['options'],true);
                array_push($arr,$info_product[$j]);
            }
            foreach ($arr as $key=> $order_product_item){
                if (!$order_product_item["options"]["comment"]) $arr[$key]["options"]["comment"] = "";
            }
            $order[$i]['products'] = $arr;
        }
        foreach ($order[0]['products'] as $key => $product){
            if (in_array($product['product_id'],$additives)){
                unset($order[0]['products'][$key]);
            }
        }
        $order[0]["order_type"] = array(
            "title"=>$order[0]["order_type_title"],
            "id"=>(int)$order[0]["order_type_id"],
            "is_remoted"=>(bool)$order[0]["order_type_is_remoted"],
        );
        $order[0]['createdon'] = date("d.m.Y H:i:s",strtotime($order[0]['createdon']));
        $order[0]['products'] = array_values($order[0]['products']);
        $order[0]['delivery_cost'] = (int)$order[0]['delivery_cost'];
        $order[0]['rating'] = ($order[0]['rating'])? (int)$order[0]['rating'] : 0;
        $order[0]['bonus_writeoff'] = (int)$order[0]['bonus_writeoff'];
        $order[0]['bonus_accrual'] = (int)$order[0]['bonus_accrual'];
        $order[0]['discount'] = (int)$order[0]['discount'];
        if($order[0]['order_type_id'] == 4 && $order[0]['discount'] == 0) {
            $order[0]['cost'] = (int)$order[0]['cost'] + (int)$order[0]['bonus_writeoff'] + (int)$order[0]['discount'];
            $order[0]['total_cost'] = (int)$order[0]['cost'] + (int)$order[0]['delivery_cost'] - (int)$order[0]['bonus_writeoff'] - (int)$order[0]['discount'];
        } else {
           $order[0]['cost'] = (int)$order[0]['cost'] - (int)$order[0]['delivery_cost'] + (int)$order[0]['bonus_writeoff'] + (int)$order[0]['discount'];
           $order[0]['total_cost'] = (int)$order[0]['cost'] + (int)$order[0]['delivery_cost'] - (int)$order[0]['bonus_writeoff'] - (int)$order[0]['discount'];
        }
        $order[0]["original_total_cost"] = $original_total_cost;
        $selfPickupDiscount = $order[0]["original_total_cost"]-$order[0]["total_cost"]-$order[0]["discount"]-$order[0]['bonus_writeoff'];
        $order[0]["self_pickup_discount"] = ($selfPickupDiscount < 0)? 0 : $selfPickupDiscount;
        $order[0]['order_id'] = (int)$order[0]['order_id'];
        $order[0]['status_id'] = (int)$order[0]['status_id'];
        $order[0]['order_type_id'] = (int)$order[0]['order_type_id'];
        $order[0]['payment_id'] = (int)$order[0]['payment_id'];
        $order[0]['address_id'] = (int)$order[0]['address_id'];
        $order[0]['city_id'] = (int)$order[0]['city_id'];
        $order[0]['id_org'] = (int)$order[0]['id_org'];
        if ($order[0]['payment_id'] == 1){
            $order[0]['order_title'] = "Заказ ".$order[0]["order_id"]." оплачен!";
           // $order[0]['order_subtitle'] = "Мы начали приготовление вашего заказа.";
        } else {
            $order[0]['order_title'] = "Заказ ".$order[0]["order_id"]." оформлен!";
           // $order[0]['order_subtitle'] = "Пожалуйста, приготовьте к оплате";            
        }
        $region =  explode(",", $order[0]['region']);
        $order[0]['region'] = array(
            "latitude" => $region[0],
            "longitude" => $region[1],
        );
        $organisationObj = $this->modx->getObject("modResource",$order[0]['id_org']);
        $order[0]['organisation_name'] = $organisationObj->pagetitle;
        foreach ($order[0]['products'] as $key => $product){
            if(!array_key_exists("size",$product["options"]["childs"])) {
                $order[0]['products'][$key]["options"]["childs"]["size"] = [];
            }
            
            if(count($product["options"]["childs"]["size"] > 0)) {
                $order[0]['products'][$key]["price"] -= (int)$product["options"]["childs"]["size"][0]["price"];
            }
            if(count($product["options"]["childs"]["type"] > 0)) {
                $order[0]['products'][$key]["price"] -= (int)$product["options"]["childs"]["type"][0]["price"];
            }
            if(count($product["options"]["childs"]["additives"] > 0)) {
                foreach($product["options"]["childs"]["additives"] as $additive) {
                    $order[0]['products'][$key]["price"] += (int)$additive["price"] * (int)$additive["count"];
                }
                
            }
        }
        return ($order[0]);
    }
}