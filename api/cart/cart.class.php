<?php
//test
class Cart extends ModxApi{
    //новые подарки
    public function getGiftsNew(){
        $sql = "SELECT * FROM modx_promo_base WHERE type = 4 and activity = 1 ORDER BY value ASC";
        $thresholds = $this->modx->query($sql);
        $thresholds = $thresholds->fetchAll(PDO::FETCH_ASSOC);
        foreach (array_column($thresholds, 'gift_product_wrapper_id') as $id){
            $result[] = array("id"=>(int)$id);
        }
        $result = array_combine(array_column($result, 'id'), $result);
        foreach ($thresholds as $threshold){
            //$this->modx->log(1,$_POST['order_type']."/".$_POST['id_org'].": ".print_r($threshold,1));
            
            if (in_array($_GET['order_type'], explode(",",$threshold["order_types"])) || $threshold["order_types"] == ""){
                if(in_array($_GET['id_org'], explode(",",$threshold["orgs"])) || $threshold["orgs"] == ""){
                    
                    
                    $arr = [];
                    $items = explode(",", $threshold['id_product']);
                    $arr["min_count"] = (int)$threshold["value"];
                    $arr["min_cost"] = (int)$threshold["value"];
                    $arr["title"] = $threshold["name"];
                    $arr["imgUrl"] = ($threshold["image"])? $threshold["image"] : "";
                    $arr["desc"] = $threshold["description"];
                    $arr["id_promo"] = (int)$threshold["id_promo"];
                    $first_res = $this->modx->getObject('modResource', $items[0]);
                    //$arr["imgUrl"] = $first_res->get('image');
                    foreach ($items as $item){
                        $current_item = [];
                        $res = $this->modx->getObject('modResource', $item);
                        $current_item = array(
                            "title" => $res->get('pagetitle'),
                            "imgUrl" => ($res->get('image'))? $res->get('image') : "",
                            "price" => $res->get('price'),
                            "id" => $res->get('id'),
                        );
                    /*if ($res->get('price') == 0){
                        $current_item['max_gift'] = 1;
                    }*/
                    $arr["modificators"][] = $current_item;
                
                    
                    }
                    $result[$threshold["gift_product_wrapper_id"]]["gifts"][] = $arr;
                    
                }
                
            }
        }
        //$json['gift_from_cost'] = array_combine(array_column($json['gift_from_cost'], 'id_promo'), $json['gift_from_cost']);
        //$json['gift_from_cost'] = array_values($json['gift_from_cost']);
        $result = array_values($result);
        return $result;
    }
    //получить информацию о добавке
    private function addInfo($array){
        if ($array != []){
            foreach ($array as $key => $product){
                $product_object = $this->modx->getObject("msProduct",$product["id"]);
                $product_data = $this->modx->getObject("msProductData",$product["id"]);
                $array[$key]["title"] = $product_object->pagetitle;
                $array[$key]["desc"] = $product_object->description;
                $array[$key]["img_url"] = (string)$product_object->image;
                $array[$key]["price"] = (float)$product_data->price;
            }
        }
        return $array;
    }
    //Добавлят в корзину новый айтем
    public function add($request) {
        //
        $product_id = $request['id']; //id продукта
        $count = $request['count']; //id продукта
        $ingredients = $this->addInfo($request['ingredients']);
        $additives = $this->addInfo($request['additives']); //информация о добавляемых продуктах
        $sizes = $this->addInfo($request['size']); //информация о размере
        $types = $this->addInfo($request['type']); //тип (прожарки, лаваша и т.д.)
        if ($this->modx->getOption("app_pricy_type") == "userprice"){
            if ($profile->state){
                $price_type = $profile->state;
            } else {
                $price_type = $this->modx->getOption("add_default_type_price");
            }
            $price_type_original = $price_type;
            
            $org = $this->modx->getObject("modResource", $request["id_org"]);
            if (!$org) ModxApi::HTTPStatus(409);
            $price_type = $org->getTVValue($order_type_tv);
            $price_type_original = $org->getTVValue("tvTypeDelivery");
        } elseif ($this->modx->getOption("app_pricy_type") == "cityprice") {
            switch ($request["order_type"]){
                case '1': //самовывоз
                    $order_type_tv = "tvType";
                    break;
                case '2': //курьер
                    $order_type_tv = "tvTypeDelivery";
                    break;
                case '4': //менеджер
                    $order_type_tv = "tvTypeDelivery";
                    break;
                case '3': //ресторан
                    $order_type_tv = "tvTypeRestourant";
                    break;
            }
            $org = $this->modx->getObject("modResource", $request["id_org"]);
            if (!$org) ModxApi::HTTPStatus(409);
            $price_type = $org->getTVValue($order_type_tv);
            $price_type_original = $org->getTVValue("tvTypeDelivery");
        } else {
            $price_type = 0;
        }
        
        foreach ($additives as $key=> $additive){
            $res = $this->modx->getObject("modResource", $additive["id"]);
            if ($price_type != 0) {
                $price_obj = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$additive["id"], "type"=>$price_type));
                $price_obj_opiginal = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$additive["id"], "type"=>$price_type_original));
                $additives[$key]["original_price"];
            } else {
                $price_obj = $this->modx->getObject("msProduct",$additive["id"]);
                $additives[$key]["original_price"] = $price_obj->get("price");
            }
            $additives[$key]["title"] = $res->pagetitle;
            $additives[$key]["price"] = $price_obj->get("price");
            // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
            if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "price") {
                $additives[$key]["price"] = round($additives[$key]["price"] - (($additives[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            } if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "cityprice") {
                if($additives[$key]["price"] == $additives[$key]["original_price"]) $additives[$key]["price"] = round($additives[$key]["price"] - (($additives[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            }
        }
        foreach ($ingredients as $key=> $ingredient){
            $res = $this->modx->getObject("modResource", $ingredient["id"]);
            if ($price_type != 0) {
                $price_obj = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$ingredient["id"], "type"=>$price_type));
                $price_obj_opiginal = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$additive["id"], "type"=>$price_type_original));
                $ingredient[$key]["original_price"];
            } else {
                $price_obj = $this->modx->getObject("msProduct",$ingredient["id"]);
                $ingredient[$key]["original_price"] = $price_obj->get("price");
            }
            $ingredient[$key]["title"] = $res->pagetitle;
            $ingredient[$key]["price"] = $price_obj->get("price");
            // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
            if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "price") {
                $ingredient[$key]["price"] = round($ingredient[$key]["price"] - (($ingredient[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            } if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "cityprice") {
                if($ingredient[$key]["price"] == $ingredient[$key]["original_price"]) $ingredient[$key]["price"] = round($ingredient[$key]["price"] - (($ingredient[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            }
        }
        foreach ($sizes as $key=> $size){
            $res = $this->modx->getObject("modResource", $size["id"]);
            /*if ($price_type != 0) {
                $price_obj = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$size["id"], "type"=>$price_type));
                $price_obj_opiginal = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$additive["id"], "type"=>$price_type_original));
                $size[$key]["original_price"];
            } else {*/
                $price_obj = $this->modx->getObject("msProduct",$size["id"]);
                $size[$key]["original_price"] = $price_obj->get("price");
            //}
            $size[$key]["title"] = $res->pagetitle;
            $size[$key]["price"] = $price_obj->get("price");
            // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
            if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "price") {
                $size[$key]["price"] = round($size[$key]["price"] - (($size[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            } if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "cityprice") {
                if($size[$key]["price"] == $size[$key]["original_price"]) $size[$key]["price"] = round($size[$key]["price"] - (($size[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            }
        }
        foreach ($types as $key=> $type){
            $res = $this->modx->getObject("modResource", $type["id"]);
            /*if ($price_type != 0) {
                $price_obj = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$type["id"], "type"=>$price_type));
                $price_obj_opiginal = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$additive["id"], "type"=>$price_type_original));
                $type[$key]["original_price"];
            } else {*/
                $price_obj = $this->modx->getObject("msProduct",$type["id"]);
                $type[$key]["original_price"] = $price_obj->get("price");
            //}
            $type[$key]["title"] = $res->pagetitle;
            $type[$key]["price"] = $price_obj->get("price");
            // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
            if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "price") {
                $type[$key]["price"] = round($type[$key]["price"] - (($type[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            } if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "cityprice") {
                if($type[$key]["price"] == $type[$key]["original_price"]) $type[$key]["price"] = round($type[$key]["price"] - (($type[$key]["price"] * $this->modx->getOption("app_discount_percent"))/100));
            }
        }
        if ($ingredients == NULL){
            $ingredients = [];
        }        
        if ($additives == NULL){
            $additives = [];
        }
        if ($ingredients == NULL){
            $ingredients = [];
        }
        $option['childs'] = array(
            "ingredients"=>$ingredients, 
            "additives"=>$additives,
            "size"=>$sizes,
            "type"=>$types,
            "rating" => []
        );
        $option['comment'] = $request["comment"];
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        $cart = $this->ms2->cart->get();
        $order = $this->ms2->order->get(); // заказ
        //не даем добавить товар, если лимит товара установлен
        $sql = "SELECT * FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.product_id = '$product_id' AND modx_ms2_product_options.key = 'max_count'";
        $this_product_info = $this->modx->query($sql);
        $this_product_info = $this_product_info->fetchAll(PDO::FETCH_ASSOC);
        foreach ($_SESSION['minishop2']['cart'] as $key => $cart_item){
            if ($cart_item["id"] == $product_id){
                $this_product_key = $key;
                $checkOption = $cart_item["options"];
            }
            if ($cart_item["id"] == $product_id && $this->productIsGift($product_id)){
                return $this->getCart($request);
            }
        }
        if ($this_product_key && $option['childs'] == $checkOption){
            if ($this_product_info[0]["value"]){
                if ((int)$this_product_info[0]["value"]>$_SESSION["minishop2"]["cart"][$this_product_key]["count"]){
                    $cart = $this->ms2->cart->add($product_id, $count, $option);  
                }
            } 
        } else {
            $cart = $this->ms2->cart->add($product_id, $count, $option);
        }
        return $this->getCart($request);
    }
    //Изменение количества товара
    public function change($request){
        $product_key = $request['key']; //key продукта
        $count = $request['count']; //обновленное количество
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        if ($count !=0){
            $cart = $this->ms2->cart->get();
            $cart[$request['key']]['options']['comment'] = $request["comment"];
            $this->ms2->cart->set($cart);
        }
        $cart = $this->ms2->cart->get();
        $cart = $this->ms2->cart->change($product_key, $count); //применяем
        return $this->getCart($request);
    }
    //получить эквайринг для организации
    public function getAcquiring(){
        $org_obj = $this->modx->getObject("modResource", $this->modx->request["id_org"]);
        $acquiring_types = json_decode($org_obj->getTVValue("acquiring_types"),1);
        foreach ($acquiring_types as $key => $acquiring_type){
            if ($acquiring_type["activity"] == 1){
                $acquiring = $acquiring_type;
            }
        }
        return $acquiring;
    }
    //проверяет статус оплаты и если ок, то создает заказ
    public function checkPayment($request){
        if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])) return $this->returnError(1); //если нет такого токена, возвращаем ошибку
        //получаем эквайринг
        $acquiring = $this->getAcquiring();
        //модуль эквайринга сбербанк проверка транзакции
        if ($acquiring["type"] == "SBERBANK"){
            $vars['userName'] = $acquiring["terminal_key"]; 
            $vars['password'] = $acquiring["secret_key"];
            $vars['orderId'] = $profile->last_trans_id;
            if ($acquiring['mode'] == 0) $ch = curl_init('https://3dsec.sberbank.ru/payment/rest/getOrderStatusExtended.do?' . http_build_query($vars)); //тестовая среда
            if ($acquiring['mode'] == 1) $ch = curl_init('https://securepayments.sberbank.ru/payment/rest/getOrderStatusExtended.do?' . http_build_query($vars)); //боевая среда
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $response = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($response, true);
        } else if ($acquiring["type"] == "TINKOFF"){
            include  $_SERVER["DOCUMENT_ROOT"].'/file/tinkoff/TinkoffMerchantAPI.php'; 
            $Terminal_Key = $acquiring["terminal_key"];
            $Secret_Key = $acquiring["secret_key"];
            $api = new TinkoffMerchantAPI(
                $Terminal_Key,  //Ваш Terminal_Key
                $Secret_Key   //Ваш Secret_Key
            );
            $params = [
                  'PaymentId' => $profile->last_trans_id,
            ];
            $response = $api->getState($params);
            $response = json_decode($response, true);
            //$this->modx->log(1,print_r($params,1));
            //$this->modx->log(1,print_r($response,1));
            if($response["Status"] == "CONFIRMED") $response["orderStatus"] = 2;
        }
        if ($response["orderStatus"] == 2) $success = true;
        if ($success){
            return $this->createOrder(true,$request);
        } else {
            return $this->returnError(1001);
        }
    }
    //создает транзакцию
    public function createTransaction($request){
        $cart = $this->getCart($request);
        //получаем эквайринг
        $acquiring = $this->getAcquiring();
        //модуль эквайринга сбербанк создание транзакции
        if ($acquiring["type"] == "SBERBANK"){
            $vars['userName'] = $acquiring["terminal_key"]; 
            $vars['password'] = $acquiring["secret_key"];
            $vars['orderNumber'] = mt_rand() . "\n";   // ID заказа в магазине 
            $vars['amount'] = ($cart['cost_with_delivery'] - $request["bonus"]) * 100;   // Сумма заказа в копейках
            $vars['returnUrl'] = $this->modx->getOption("site_url").$this->modx->getOption('payment_success_url'); // URL куда клиент вернется в случае успешной оплаты
            $vars['failUrl'] = $this->modx->getOption("site_url").$this->modx->getOption('payment_fail_url'); // URL куда клиент вернется в случае ошибки
            $vars['description'] = 'Новый заказ';
            if ($acquiring['mode'] == 0) $ch = curl_init('https://3dsec.sberbank.ru/payment/rest/register.do?' . http_build_query($vars)); //тестовая среда
            if ($acquiring['mode'] == 1) $ch = curl_init('https://securepayments.sberbank.ru/payment/rest/register.do?' . http_build_query($vars)); //боевая среда
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $res = curl_exec($ch);
            curl_close($ch);
            $response = json_decode($res, true);
        } else if ($acquiring["type"] == "TINKOFF") {
            include  $_SERVER["DOCUMENT_ROOT"].'/file/tinkoff/TinkoffMerchantAPI.php'; 
            $Terminal_Key = $acquiring["terminal_key"]; //"1607960680360DEMO";
            $Secret_Key = $acquiring["secret_key"]; //"tx8dbcn4lj29la10";
            $ord = mt_rand();// $this->modx->getObject('msOrder', $order_id);
            $api = new TinkoffMerchantAPI(
                $Terminal_Key,  //Ваш Terminal_Key
                $Secret_Key   //Ваш Secret_Key
            );
            $enabledTaxation = true;
            $params = [
                'OrderId' => $ord,
                'Amount'  =>  ($cart['cost_with_delivery'] - $request["bonus"]) * 100,
                'SuccessURL' => $this->modx->getOption("site_url").$this->modx->getOption('payment_success_url'),
                'FailURL' => $this->modx->getOption("site_url").$this->modx->getOption('payment_fail_url'),
            ];
            
            if ($enabledTaxation) {
                $params['Receipt'] = $receipt;
            }
            $response = $api->init($params);
            $response = json_decode($response, true);
            $response["orderId"] = $response["PaymentId"];
            $response["formUrl"] = $response["PaymentURL"];
            
            //$this->modx->log(1,print_r($response,1));
            /*$confirmationUrl = $response->PaymentURL;
            $json['data'][]['paymentURL'] = $confirmationUrl;
            $order = $modx->getObject("msOrder", $order_id);
            $current_address = $modx->getObject('msOrderAddress', $order->get("address"));
            $current_address->set("trans_id", $response->PaymentId);
            $current_address->save();*/
        }
        return $response;
    }
    //Создает заказ
    public function createOrder($payment_status, $request){
        if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])) return $this->returnError(1); //если нет такого токена, возвращаем ошибку
        if (!$this->ms2->cart->get()) return $this->returnError(105); //если корзина пустая, возвращаем ошибку // ранее было (!$this->cart)
        $status = $this->status;
        $discount = $this->checkCodes($status["total_cost"]); //получаем информацию о скидке
        if (!$payment_status){
            if ($this->modx->request["payment_type"] == 1){
                //создаем транзакцию
                $response = $this->createTransaction($request);
                //Логика по записыванию айди транзакции в профиль пользователя
                $trans_id = $response["orderId"]; //айди транзакции
                $profile->set("last_trans_id", $trans_id);
                $profile->save();
                if ($response["formUrl"]){
                    $json['payment_url'] = $response["formUrl"];  
                } else {
                    $json['payment_url'] = "";
                }
                $json['order_id'] = 0;
                return $json;
            }
        }
        // проверяем настройку связанною с работой чёрного списка
        if($this->modx->getOption("app_black_list")) {
            $checkBlackList = $profile->get("black_list_type");
        } else {
            $checkBlackList = 0;
        }
        // проверка чёрного списка
        //if($checkBlackList == 1) return $this->returnError(10);
        //проверка способа оплаты, если включён чёрный список
        //if($checkBlackList == 2 && $this->modx->request['payment_type'] != 1) return $this->returnError(11);
        $user_address = $this->modx->getObject("UserAddresses", $this->modx->request["address_id"]);
        if ($this->modx->request['city']){ //город
            $city = $this->modx->request['city'];
        }  else {
            $city = $user_address->city;
        }
        if ($this->modx->request['flat_num']){//номер квартиры
            $flat_num = $this->modx->request['flat_num']; 
        } else {
            $flat_num = $user_address->flat_num; 
        }
        if ($this->modx->request['house_num']){//номер дома
            $house_num = $this->modx->request['house_num'];
        } else {
            $house_num = $user_address->house_num;
        }
        if ($this->modx->request['floor']){//этаж
            $floor = $this->modx->request['floor'];
        } else {
            $floor = $user_address->floor;
        }
        if ($this->modx->request['entrance']){//подъезд
            $entrance = $this->modx->request['entrance'];
        } else {
            $entrance = $user_address->entrance;
        }
        if ($this->modx->request['street']){//улица
            $address = $this->modx->request['street'];
        } else {
            $address = $user_address->street;
        }
        if ($this->modx->request['doorphone']){//код домофона
            $promo = $this->modx->request['doorphone'];
        } else {
            $promo = $user_address->doorphone;
        }
        // проверяем настройку, нужно ли использовать данные получателя
        if(!$this->modx->getOption("app_disable_recipient")) {
            if ($this->modx->request['phone']) {
                $phone = $this->modx->request['phone']; //Номер телефона
            } else {
                $phone = $profile->mobilephone;
            }
        } else {
            if ($profile->get('recipients_phone')) {
                $phone = $profile->get('recipients_phone'); //Номер телефона
            } else {
                $phone = $profile->mobilephone;
            }
        }
        
        if ($this->modx->request['email']){//email
            $email = $this->modx->request['email'];
        } else {
            $email = $profile->email;
        }
        // проверяем настройку, нужно ли использовать данные получателя
        if(!$this->modx->getOption("app_disable_recipient")) {
            if ($profile->get('fullname')){
                $name = $profile->get('fullname');
            } else {
                $name = $profile->get('email');
            }
        } else {
            if ($user_address->get("receiver_name")) {
                $name = $user_address->get("receiver_name");
            } else {
                $name = $profile->get('fullname');
            }
        }
        
        // массив для преобразования в дату для SQL
        $massMonth = array(
            "ЯНВ" => "01",
            "ФЕВ" => "02",
            "МАР" => "03",
            "АПР" => "04",
            "МАЙ" => "05",
            "ИЮНЬ" => "06",
            "ИЮЛЬ" => "07",
            "АВГ" => "08",
            "СЕН" => "09",
            "ОКТ" => "10",
            "НОЯ" => "11",
            "ДЕК" => "12");
        // разбиваем строку с полученным временем бронирования на части для обработки
        $time_edit = explode(" ",$this->modx->request['time_appointment']);
        // формируем нужный формат даты
        $time_appointment_sql = date("Y")."-".$massMonth[$time_edit[1]]."-".$time_edit[0]." ".explode("-",$time_edit[2])[0].":00"; //время записи
        
        //$this->modx->log(1,"Данные: ".$user_address->get("receiver_name")."/".$user_address->get("recipients_phone"));
        $comment = $this->modx->request['comment']; //коммент
        if($this->modx->getOption("app_disable_recipient") && $user_address->get('call_the_recipient')) $comment .= " Необходимо позвонить получателю";
        $delivery = $this->modx->request['order_type']; //тип доставки
        $payment = $this->modx->request['payment_type']; //тип оплаты
        $time_appointment = $this->modx->request['time_appointment']; //время записи
        $id_restourant = $this->modx->request['id_org']; //id ресторана
        $table_num = $this->modx->request['table_num']; //номер столика
        $money = $this->modx->request['bill']; //с какой купюоы сдавать
        $person = $this->modx->request['person']; // добавление количества персон
        $polygone_id = $this->modx->request["polygon_id"];
        $bonus_count_write = $this->modx->request['bonus'];
        if ($delivery == 1){
            $org_obj = $this->modx->getObject("modResource",$id_restourant);
            $org_obj_parent = $this->modx->getObject("modResource",$org_obj->parent);
            $city = $org_obj_parent->pagetitle;
            $address = $org_obj->longtitle;
        }
        $this->ms2->order->add('receiver',$name); // Указываем имя получателя
        $this->ms2->order->add('phone',$phone);
        $this->ms2->order->add('email',$email);
        $this->ms2->order->add('payment', $payment); //Указываем способ оплаты
        $this->ms2->order->add('delivery', $delivery); //Указываем способ доставки
        $this->ms2->order->add('street', $address);
        $this->ms2->order->add('region', $region);
        $this->ms2->order->add('room', $money);
        $this->ms2->order->add('building', $table_num);
        $this->ms2->order->add('comment', $comment);
        $this->ms2->order->add('metro', $promo);
        $this->ms2->order->add('flat_num', $flat_num);
        $this->ms2->order->add('house_num', $house_num);
        $this->ms2->order->add('floor', $floor);
        $this->ms2->order->add('entrance', $entrance);
        $this->ms2->order->add('city', $city);
        $this->ms2->order->add('index', $id_restourant);
        $this->ms2->order->add('referalDiscount', $_SESSION['minishop2']['promocodes']);
        $this->ms2->order->add('discount', $discount["actual_discount"]); 
        $this->ms2->order->add('person', $person);
        $this->ms2->order->add("user_id", $profile->internalKey);
        $this->ms2->order->add('time_appointment', $time_appointment_sql);
        $this->ms2->order->add('polygone_id', $polygone_id);
        $this->ms2->order->add('address_id', $this->modx->request["address_id"]);
        
        //$this->modx->log(1,"asdasdasdasd2: ".$_SESSION["minishop2"]["order"]["delivery_cost"]);
        $delivery_cost = $_REQUEST["delivery_cost"] = $_SESSION["minishop2"]["order"]["delivery_cost"];
        
        // если тип оплаты "Онлайе" и доставка "Менеджер"
        if($delivery == 4 && $payment == 2) {
            // то вызываем метод отправки заказа менеджеру
            $this->sendAnOrderToTheManager($request);
        }
        
        $cart = $this->ms2->cart->get();//$cart = $this->cart;
        foreach ($cart as $item){ //добавляем в корзину все добавки
            for ($i=0;$i<$item['count'];$i++){
                foreach ($item['options']['childs']['additives'] as $additive){
                    $this->ms2->cart->add($additive['id'],$additive['count']);
                }
                foreach ($item['options']['childs']['type'] as $type_additive){
                    $this->ms2->cart->add($type_additive['id'],$type_additive['count']);
                }
                foreach ($item['options']['childs']['size'] as $type_additive){
                    $this->ms2->cart->add($type_additive['id'],$type_additive['count']);
                }
            }
        }
        
        // получаем параметр что передаётся из приложения, отвечающий за замену цен на курьерские (original_price)
        if($this->modx->request["enabled_delivery_sale"] == false) {
            //$this->modx->log(1,"Корзина перед обработкой: ".print_r($this->ms2->cart->get(),1)); // проверка того что было до изменения цен в корзине
            // получаем корзину с товарами
            $cart = $this->ms2->cart->get();
            // перебераем товары в корзине
            foreach($cart as $keyCart => $cartItem){
                // если у товара есть "курьерская цена", то заносим её в цену товара в корзине
                if($cartItem["original_price"]) $cart[$keyCart]["price"] = $cartItem["original_price"];
            }
            // сохраняем изменения с ценами
            $this->ms2->cart->set($cart);
            
            //$this->modx->log(1,"Корзина перед обработкой (переделка): ".print_r($this->ms2->cart->get(),1)); // проверка того что получилось после изменения цен в корзине
        }
        
        $order_id = $this->ms2->order->submit(); //создаем заказ
        //return $order_id;
        $order = $this->modx->getObject("msOrder", $order_id); //получаем заказ
        $order->set("comment",$comment);
        $order->save();
        if ($payment == 1){
            $current_address = $this->modx->getObject('msOrderAddress', $order->get("address"));
            $current_address->set("trans_id", $profile->last_trans_id);
            $current_address->save();
        }

        // добавляем время заказа в отдельную таблицу, если указано время бронирования и дата для заказа
        if($time_appointment) {
            // разбиваем дату и время на дату и часы поотдельности
            $timeOrder = explode(" ",$time_appointment);
            // выполняем функцию по по добавлению бронирования заказа
            $this->setTimeOrder($timeOrder[0],date("H:i",strtotime($timeOrder[1])),$id_restourant,$order_id);
        }
        
        unset($_SESSION['minishop2']['actual_discount']); //чистим сессию
        unset($_SESSION['minishop2']['promocodes'],$_SESSION['minishop2']['promocode_left_to_use']);//чистим сессию
        unset($_SESSION['minishop2']['discount_cost']);//чистим сессию
        unset($_SESSION['free_nabor']);//чистим сессию
        unset($_SESSION["getting_gifts"]);//чистим сессию
        $this->setDiscount($order); //начисляем скидку
        if (!$msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/')
        ) {
            $this->modx->log(1, 'Could not load msBonus2 class!');
        }
        $msb2->initialize($this->modx->context->key);
        $manager = $msb2->getManager();
        $manager->setOrderWriteoff($order_id, $bonus_count_write);
        $writeOff = ($bonus_count_write)? $bonus_count_write : 0;
        $json = array("order_id"=>$order_id, "payment_url"=>"", "cost"=>((int)$order->cost - (int)$writeOff)); // туту было вложение в массив []
        return $json;
    }
    //Активирует промокод
    public function promocode($request){
        $this->modx->log(1,'promocode start '.print_r([
            '$this->modx->request'=>$this->modx->request,
        ],1));
        $today = date("Y-m-d H:i:s"); //получаем актуальное время 
        $code = $request['promocode']; //получаем промокод
        $code_obj = $this->modx->getObject("PromoBase", array("code"=>$code));
        if ($code_obj->activity != 1) return $this->returnError(100); //если промокод не активен, возвращаем 100 ошибку
        if (((int)$code_obj->use_count - (int)$code_obj->already_used) < 1) return $this->returnError(101); //если количество использований закончилось, возвращаем 101 ошибку
        
        // проверяем, не является ли данный промокод реферальным и количество пользователей что его могут применить не выставлено в 0
        if($code_obj->get("is_referal") && $this->modx->getOption("app_referal_count_user") != 0){
            // если он реферальный, то получаем по его ID перечень пользователей которые его применили
            $referalObject = $this->modx->getCollection("PromoUsingUsers",array("id_promo" => $code_obj->id));
            // обнуляем счётчик
            $count = 0;
            // если в полученном списке что-то есть, то считаем количество пользователей что применили промокод
            if($referalObject) {
                foreach($referalObject as $usedUsers) $count++;
                if($count - (int)$this->modx->getOption("app_referal_count_user") <= 0) return $this->returnError(101); //если количество использований закончилось, возвращаем 101 ошибку
            }
        }
        
        if ($today > $code_obj->date_end){ //если время действия закончилось, то возвращаем ошибку
            if ($code_obj->date_end != NULL) return $this->returnError(102); //но проверяем на NULL, если NULL - то все окей, значит промокод не имеет конца действия.
        }
        // считаем общий остаток применений по промокоду
        $checkUseCount = (int)$code_obj->use_count - (int)$code_obj->already_used;
        
        if ($code_obj->availability == 2 || $code_obj->user_only == 1){// если тип промокода "личный" или промокод для всех
            
            // получаем пользователя по токену, если пользователь отсуствует, то возвращаем ошибку
            if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])) return $this->returnError(100);
            if($code_obj->availability == 2){
                // получаем по ID пользователя и ID промокода данные, прикреплён ли промокод к пользователю, если нет, то возвращаем ошибку
                if (!$promoUser = $this->modx->getObject('PromoUsersSecret',['id_promo'=>$code_obj->id_promo, 'id_user' => $profile->id])) return $this->returnError(100);
            }
            // формируем массив для выборки
            $where = array("id_user" => $profile->internalKey,"id_promo" => $code_obj->id_promo);
            // получаем объект промокода для юзера
            $checkObject = $this->modx->getObject("PromoUsingUsers",$where);
            // получаем количество применений пользователем промокода, если их нет, то присваиваем 0
            $usingCount = ($checkObject)? $checkObject->get("using_count") : 0;
            //$this->modx->log(1,"Количество: ".$usingCount);
            // если общее число применений минус число применений пользователя меньше 1, то возвращаем ошибку
            if(((int)$code_obj->use_count - (int)$usingCount) < 1) return $this->returnError(101);
            // считаем индивидуальный остаток по применению промокода для конкретного пользователя
            $checkUseCount = (int)$code_obj->use_count - (int)$usingCount;
            //$this->modx->log(1,$checkUseCount."=".(int)$code_obj->use_count."-".(int)$usingCount.print_r($where,1));

        } else {
            
        }
        $_SESSION['minishop2']['promocodes'] = $code;
        $_SESSION['minishop2']['promocode_left_to_use'] = $checkUseCount;
        if ($code_obj->type == 2){
            $option['childs'] = array(
                "ingredients"=>[], 
                "additives"=>[],
                "size"=>[],
                "type"=>[],
                "rating" => [],
                "promoitem" => true
            );
            //проверяем, можно ли еще раз положить в корзину
            $cart = $this->ms2->cart->get();// $this->cart;
            $cart = array_combine(array_column($cart, 'id'), $cart);
            if ($cart[$code_obj->id_product]["count"] < $code_obj->use_count_in_check){
                $this->ms2->cart->add($code_obj->id_product, 1, $option);
            } else {
                return $this->returnError(103); // возвращаем ошибку что больше этот код в корзине уже нельзя использовать
            }
        }
        $this->modx->log(1,'promocode start '.print_r([
            '$this->modx->request'=>$this->modx->request,
            '$_SESSION[minishop2]'=>$_SESSION['minishop2']
        ],1));
        return $this->getCart($request);
    }
    //Очищает корзину
    public function clean($request){
        //$this->modx->log(1,"До очистки (this->cart): ".print_r($this->cart,1));
        
        // получаем содержимое корзины
        //$cart = $this->ms2->cart->get();
        
        //$this->modx->log(1,"Проверка (this->ms2->cart->get): ".print_r($this->ms2->cart->get(),1));
        
        /*foreach($cart as $key => $item) {
            $this->ms2->cart->change($key, 0);
            $cart[$key]["count"] = 0;
        }*/
        $ms2 = $this->modx->getService('miniShop2');
        $ms2->initialize($modx->context->key);
        $cart = $ms2->cart->get();
         foreach ($_SESSION['minishop2']['cart'] as $key => $prod_item){
           // $cart = $ms2->cart->add(3274, 1, $option);
            $cart = $ms2->cart->change($key, 0);
            //$cart = $ms2->cart->change($key, 0);
        }
        //$this->modx->log(1,"Проверка после обнуления товаров: ".print_r($this->ms2->cart->get(),1));
        
        //$this->ms2->cart->set($cart);
        //$this->ms2->cart->clean();
        //unset($_SESSION["minishop2"]);
        $session = session_id();
        //session_destroy();
        //$sql = "DELETE FROM `modx_session` WHERE `modx_session`.`id` = '$session'";
        //$info_user = $this->modx->query($sql);
        //$this->modx->log(1,"Проверка в конце: ".print_r($this->ms2->cart->get(),1));
        
        //$this->modx->log(1,"После очистки (this->cart): ".print_r($this->cart,1));
        
        unset($_SESSION['minishop2']['promocodes'],$_SESSION['minishop2']['promocode_left_to_use'], $_SESSION["getting_gifts"]);
        return $this->getCart($request);
    }
    //Удаляем промокод из корзины
    public function removePromo($request){
        // получаем промокод
        $code = $_SESSION['minishop2']['promocodes'];
        // находим его в базе
        $promo = $this->modx->getObject("PromoBase",array("code" => $code));
        // проверяем, существует ли промокод
        if($promo) {
            // получаем ID товара привязанного к промокоду
            if($promo->get("id_product")) {
                // если он есть, то получаем корзину с твоарами
                $cart = $this->ms2->cart->get();
                // перебераем элементы корзины
                foreach($cart as $key => $item){
                    // если находим тот что участвовал в акции, то удаляем его
                    if($item["id"] == $promo->get("id_product")) {
                        unset($cart[$key]);
                    }
                }
                // сохраняем обновлённую корзину
                $this->ms2->cart->set($cart);
            }
        }
        $_SESSION['minishop2']['promocodes'] = "";
        $_SESSION['minishop2']['promocode_left_to_use'] = 0;
        return $this->getCart($request);
    }
    // Выслать заказ мэнеджеру на почту или WhatsApp
    public function sendAnOrderToTheManager($request){
        // получаем содержимое корзины
        $order = $this->getCart($request);
        // получаем данные к заказу
        $orderData = $this->ms2->order->get();
        
        //$this->modx->log(1,"КорзинаMAIL: ".print_r($order,1));
        //$this->modx->log(1,"ЗаказMAIL: ".print_r($this->ms2->order->get(),1));
        
        // получаем тип заказа и id организации
        $orderType = $this->modx->request['order_type'];
        $idOrg = $this->modx->request['id_org'];
        
        //$this->modx->log(1,"Данные: ".$orderType." - ".$idOrg);
        
        // если тип заказа не самовывоз
        if ($orderType != 1) {
            // получаем объект организации
            $organisationObject = $this->modx->getObject("modResource",$idOrg);
            // получаем выбранный способ отправки заказа
            $sendOrder = $organisationObject->getTVValue("tvCheckSend");
            // проверяем что выбрано для доставки и выполняем метод для неё
            switch($sendOrder){
                // отправляем на почту
                case "1":
                    $emailTo = $organisationObject->getTVValue("tvEmailManager");
                    $emailTo = explode(",",$emailTo);
                    
                    if($this->emailOrder($emailTo,array_merge($order,$orderData))) $output = "На почту отправим";
                break;
                // отправляем на whatsApp
                case "2":
                    $emailTo = $organisationObject->getTVValue("tvWhatsApp");
                    $output = "На WhatsApp отправим";
                break;
            }
        }
        
        return $output;
    }
    
    // Отправка данных на почту
    public function emailOrder($emails,$order){
        
        $this->modx->log(1,"Данные заказа: ".print_r($order,1));
        
        // получаем данные отправки
        $subject = "У вас новый заказ";
        //$body = json_encode($order);
        // переменная где будет хранится список товаров из корзины
        $elementList = "";
        
        // Обрабатываем список товары для вставки в шаблон
        foreach($order["products"] as $element) {
            $itemsPar = array(
                "id" => $element["id"],
                "title" => $element["title"],
                "count" => $element["count"],
                "price" => $element["price"],
                "weight" => $element["weight"],
                "options" => json_encode($element["options"]),
            );
            $elementList .= $this->modx->getChunk("tpl.email.order.row",$itemsPar);
        }
        
        foreach($order as $key => $elementValue) {
            if($key != "products") {
                switch($key){
                    case "total_cost":
                        $nameParam = "Общая цена заказа";
                    break;
                    case "total_count":
                        $nameParam = "Общее количество товаров";
                    break;
                    case "min_count_cost":
                        $nameParam = "Минимальная цена заказа";
                    break;
                    case "delivery_cost":
                        $nameParam = "Цена доставки";
                    break;
                    case "cost_with_delivery":
                        $nameParam = "Общая цена с доставкой";
                    break;
                    case "max_bonus":
                        $nameParam = "Максимальное количество бонусов для оплаты";
                    break;
                    case "name":
                        $nameParam = "Имя заказчика";
                    break;
                    case "phone":
                        $nameParam = "Телефон";
                    break;
                    case "address":
                        $nameParam = "Адрес доставки";
                    break;
                    
                    case "receiver":
                        $nameParam = "Заказчик";
                    break;
                    case "phone":
                        $nameParam = "Телефон";
                    break;
                    case "payment":
                        $nameParam = "Тип оплаты";
                    break;
                    case "delivery":
                        $nameParam = "Тип подачи";
                    break;
                    case "street":
                        $nameParam = "Улица";
                    break;
                    case "region":
                        $nameParam = "Область";
                    break;
                    case "room":
                        $nameParam = "Сдача с суммы";
                    break;
                    case "building":
                        $nameParam = "Код домофона";
                    break;
                    case "comment":
                        $nameParam = "Комментарий к заказу";
                    break;
                    case "entrance":
                        $nameParam = "Подъезд";
                    break;
                    case "city":
                        $nameParam = "Город";
                    break;
                    case "metro":
                        $nameParam = "Промо";
                    break;
                    case "flat_num":
                        $nameParam = "Номер квартиры";
                    break;
                    case "house_num":
                        $nameParam = "Номер дома";
                    break;
                    case "floor":
                        $nameParam = "Этаж";
                    break;
                    case "index":
                        $nameParam = "ID организации";
                    break;
                    case "referalDiscount":
                        $nameParam = "Реферальная скидка";
                    break;
                    case "discount":
                        $nameParam = "Скидка";
                    break;
                    case "person":
                        $nameParam = "Количество человек";
                    break;
                    case "time_appointment":
                        $nameParam = "Время бронирования";
                    break;
                    
                    default:
                        $nameParam = $key;
                    break;
                    
                }
                
                if($elementValue) $orderData .= '<li class="element-list">'.$nameParam.': '.$elementValue.'</li>';
            }
        }
        
        // параметры что будут добавляться в шаблон
        $parameters = array(
            "elements" => $elementList,
            "order-data" => $orderData,
        );

        // формируем письмо с указанными параметрами
        $body = $this->modx->getChunk("tpl.email.order.outer",$parameters);
        
        $this->modx->log(1,"Заказ: ".$body);
        
        foreach ($emails as $email) {
            // получаем данные для письма
            $email_from = $this->modx->getOption('emailsender'); // эл.адрес оправителя письма
            $sitename = $this->modx->getOption('site_name'); // имя/название отправителя письма
            
            // отправка письма по списку почтовых ящиков
            $this->modx->getService('mail', 'mail.modPHPMailer');
            
            if($email && !(stristr($email, '@') === FALSE)) {
                $this->modx->mail->set(modMail::MAIL_BODY,$body);
                $this->modx->mail->set(modMail::MAIL_FROM,$email_from);
                $this->modx->mail->set(modMail::MAIL_FROM_NAME,$sitename);
                $this->modx->mail->set(modMail::MAIL_SUBJECT,$subject);
                // это отправка в лог информации о том куда отправляется письмо
                $this->modx->log(1, "получатель: ".$email.", отправитель: $email_from");
                $this->modx->mail->address('to',$email);
                
                $this->modx->mail->setHTML(true);
                if (!$this->modx->mail->send()) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$this->modx->mail->mailer->ErrorInfo);
                }
                // сбрасываем указанные данные, для возможности повторной отправки
                $this->modx->mail->reset();
            }
            
            // задержка в отправке писем
            sleep(0.5);
        }
        
        return true;
        
    }
    
    //Возвращает корзину
    public function getCart($request){
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        
        // получаем ID типа заказа и сравниваем с тем что есть в корзине, а так же проверяем есть ли в корзине товары
        if((!array_key_exists("id_org", $_SESSION["minishop2"]["order"]) || $request["id_org"] != $_SESSION["minishop2"]["order"]["id_org"]) || (!array_key_exists("order_type", $_SESSION["minishop2"]["order"]) || $request["order_type"] != $_SESSION["minishop2"]["order"]["order_type"]) && count($_SESSION["minishop2"]["cart"])) {
            //$this->modx->log(1,"Сейчас меняем цены! Было: ".$_SESSION["minishop2"]["order"]["id_org"].", стало: ".$request["id_org"]);
            // обращаемся к методу который пересчитает цены под изменившейся тип подачи
            $newPriceCart = $this->changeThePrice($this->ms2->cart->get(),$request);
            // заносим новые значения цен в корзину
            $this->ms2->cart->set($newPriceCart);
        
            // присваеваем новое значение ID типа подачи
            $_SESSION["minishop2"]["order"]["id_org"] = $request["id_org"];
            $_SESSION["minishop2"]["order"]["order_type"] = $request["order_type"];
            $_SESSION['minishop2']['promocodes'] = "";
            $_SESSION['minishop2']['promocode_left_to_use'] = 0;
            $_SESSION['minishop2']["actual_discount"] = 0;
        }
        
        // если настройка на получение столовых приборов включена, то проверяем не является ли данный товар единственным в корзине
        if($this->modx->getOption("app_add_cutlery")) {
            // вызываем метод проверки и удаления столовых приборов из корзины
            $this->removeCutlery();
        }
        // проверяем включаена ли настройка на активацию акции "каждая третья пицца со скидкой"
        if($this->modx->getOption("app_cart_action_on")) {
            $this->resetAShare(); // удаляем товары по акции 50% на каждый третий и добавляем удалённое количество к существующим соответствующим товарам
            $this->discountOnFavoriteProduct(); // меняем количество товара относительно акции (натретий самый дешовый товар 50% скидка)
            $this->checkPriceFavoriteProduct(); // меняем саму цену относительно акции выше
        }
        foreach ($_SESSION["minishop2"]["cart"] as $item){
            $status["total_cost"] += $item["original_price"]*$item["count"];
            $status["total_cost"] -= $item["options"]["childs"]["size"][0]["price"]*$item["count"];
        }
        $disabled_options = $this->checkPromoWithCost($status);
        $discount = $this->checkCodes($status["total_cost"]);
        //return $this->ms2->cart->status();
        $json = $this->updateCart($this->ms2->cart->status());
        if ($discount) $json = array_merge($json, $discount);
        $delivery_cost = $this->checkDeliveryCost($request["order_type"], $request["id_org"], $request["polygon_id"], $json["total_cost"]);
        $_SESSION["minishop2"]["order"]["delivery_cost"] = $delivery_cost["delivery_cost"];
        $json = array_merge($json,$delivery_cost);
        if (!$json["discount_cost"]) {//сумма корзины с доставкой
            $json["cost_with_delivery"] = $json["total_cost"]+$delivery_cost["delivery_cost"];
            $json["original_cost_with_delivery"] = $json["original_total_cost"]+$delivery_cost["delivery_cost"];
        } else {
            $json["cost_with_delivery"] = $json["discount_cost"]+$delivery_cost["delivery_cost"];
            $json["original_cost_with_delivery"] = $json["original_discount_cost"]+$delivery_cost["delivery_cost"];
        }
        $json["self_pickup_discount"] = (int)$json["original_total_cost"] - (int)$json["total_cost"];
        $_SESSION["minishop2"]["self_pickup_discount"] = $json["self_pickup_discount"];
        
        if(!array_key_exists("actual_discount",$json)) {
            $json["actual_discount"] = 0;
        }
        $json["max_bonus"] = $this->getMaxBonus($json["total_cost"]);
        $summBonus = ($json['actual_discount'] >= $json["total_cost"])? $json["discount_cost"] : $json["total_cost"] - $json['actual_discount'];
        $json["max_bonus"] = ($json["max_bonus"] >= $summBonus)? $summBonus : $json["max_bonus"];
        
        //if(array_key_exists("products", $json)) $json["products"] = array();
        if ($json["products"] == NULL) $json["products"] = []; 
        //if (sizeof($json["products"])) return 
        if ($this->modx->getOption("loyalty_PH_program")){
            //если пх включен, то отправляем корзину к ним 
            $prime_hill_response = $this->prime_hill->GetDiscount($this->modx->getObject('modUserProfile', ['website' => $this->modx->token]), $json);
            //если у них есть для пользователя подарок, то добавляем его
            if ($prime_hill_response["ProductGift"]){
                foreach ($prime_hill_response["ProductGift"] as $gift){
                    $gift_object = $this->modx->getObject("modResource", array("alias"=>$gift["ProductId"]));
                    if (!in_array($gift_object->id, $_SESSION["getting_gifts"])){
                        $_SESSION["getting_gifts"][] = $gift_object->id;
                        $request_array["id"] = $gift_object->id;
                        $request_array["count"] = 1;
                        $request_array["order_type"] = $request["order_type"];
                        $request_array["id_org"] = $request["id_org"];
                        $request_array["size"] = [];
                        $request_array["type"] = [];
                        $request_array["additives"] = [];
                        $request_array["ingredients"] = [];
                        $json = $this->add($request_array); //переписываем json новой корзиной
                    }   
                }
            }
        }
        //$json["getting_gifts"] = $_SESSION["getting_gifts"];
        //$product_ids = array_column($json["products"], 'id');
        $get_gift_info = $this->getGiftCost($json["products"], $json["total_cost"]);
        $json["gifts_status"] = $get_gift_info["gift_status"];
        $json["gifts_category_statuses"] = $get_gift_info["gifts_category_statuses"];
        $json["gifts_remains_cost"] = $get_gift_info["gifts_remains_cost"];
        $json["gifts_category_remains_cost"] = $get_gift_info["gifts_category_remains_cost"];
        $json["next_opened_gift_ids"] = $get_gift_info["next_opened_gift_ids"];
        $json["selected_gift_modificator_ids"] = $get_gift_info["selected_gift_modificator_ids"];
        $json["disabled_options"] = $this->checkDisabledOptions($json["products"], $disabled_options);
        if ($json["promocodes"]) $json["discount_cost"] = $json["original_discount_cost"];
        if ($json["promocodes"] && $request["order_type"] != 1) {
            $json["self_pickup_discount"] = 0;
            $_SESSION["minishop2"]["self_pickup_discount"] = 0;
        }
        //$json["disabled_options"] = $disabled_options;
        //return $_SESSION["minishop2"];
        if ($request["repeat_order"]){
            $json["repeat_order"] = $request["repeat_order"];
        }
        $this->modx->log(1,'getCart response '.print_r([
            '$this->modx->request'=>$this->modx->request,
            '$discount'=>$discount,
            '$json'=>$json,
        ],1));
        return $json;
        
    }
    private function checkDisabledOptions($products, $disabled_options){
        if ($disabled_options) return $disabled_options;
        $tmblr = false;
        foreach ($products as $product){
            if ($product["is_combo"] == 1){
                $tmblr = true;
            }
        }
        return $tmblr;
    }
    private function getGiftCost($products, $cart_total_cost){
        $gifts = array(
            'class' => 'PromoBase',
            'sortdir' => "desc",
            'return' => 'data',
            'select' => ["id_promo", "value", "products_to_gift", "id_product", "gift_product_wrapper_id"],
            'where' => ["type"=>4],
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($gifts);
        $gifts = $this->pdoFetch->run();
        $result = array_combine(array_column($result, 'id'), $result);
        if (!$_SESSION["promocodes"]){
            foreach ($gifts as $key => $gift){
                $total_cost = 0;
                $gifts[$key]["products_to_gift"] = explode(",", $gift["products_to_gift"]);
                $products_to_gift = explode(",", $gift["products_to_gift"]);
                $id_products = explode(",", $gift["id_product"]);
                    if ($products_to_gift != [""]){ //если подарок от цены по определенным товарам
                        $testar[] = $gift;
                        foreach ($products as $product){
                            if (in_array($product["id"],$products_to_gift)){
                                $total_cost += $product["price"]*$product["count"];
                            }
                            if (in_array($product["id"],$id_products)){
                                $response["selected_gift_modificator_ids"][] = $product["id"];
                            } 
                        }
                        if ($total_cost>=$gift["value"]){
                            $gift_statuses[$gift["id_promo"]] = true;
                            $gifts_remains_cost[$gift["id_promo"]] = 0;
                        } else {
                            $gift_statuses[$gift["id_promo"]] = false;
                            $gifts_remains_cost[$gift["id_promo"]] = $gift["value"] - $total_cost;
                        }
                    } else { //если обычный подарок от цены
                        if ($cart_total_cost>=$gift["value"]){
                            $gift_statuses[$gift["id_promo"]] = true;
                            $gifts_remains_cost[$gift["id_promo"]] = 0;
                        } else {
                            $gift_statuses[$gift["id_promo"]] = false;
                            $gifts_remains_cost[$gift["id_promo"]] = $gift["value"] - $total_cost;
                        }
                    }
            }
        } else {
            foreach ($gifts as $key => $gift){
                $gift_statuses[$gift["id_promo"]] = false;
            }
        }
        $gifts = array_combine(array_column($gifts, 'id_promo'), $gifts);
        foreach ($gift_statuses as $key => $gift_status){
            $test1[$gifts[$key]["gift_product_wrapper_id"]] = false;
            if ($gift_status){
                $test1[$gifts[$key]["gift_product_wrapper_id"]] = true;
            }
        }
        foreach ($gifts_remains_cost as $key=> $gifts__remains_cost){
            $test2[$gifts[$key]["gift_product_wrapper_id"]][] = $gifts__remains_cost;
        }
        foreach ($test2 as $key=> $gift_category_remains_cost){
            $time_array = [];
            foreach ($gift_category_remains_cost as $gift_cat){
                if ($gift_cat != 0){
                    $time_array[] = $gift_cat;
                }
            }
            //$test2[$key] = min($gift_category_remains_cost);
            //$test2[$key] = array_map('removeNull', $gift_category_remains_cost);
            $test2[$key] = min($time_array);
        }
        foreach ($gifts_remains_cost as $key=> $gifts__remains_cost){
            $test3[$gifts[$key]["gift_product_wrapper_id"]][] = array("cost" => $gifts__remains_cost, "id" => $key, "min_value" => (int)$gifts[$key]["value"]);
        }
        foreach ($test3 as $key=> $items){
            $min_value = 100000000;
            $id = null;
            foreach ($items as $key1=> $item){
                if ($item["cost"]<$min_value) {
                    if ($item["cost"] != 0){
                        $min_value = $item["cost"];
                        $id = $item["id"];
                    }
                }
            }
            if ($id != null) {
                $test3[$key] = $id;  
            } else {
                //array_column($items, 'min_value')
                $max_value = 0;
                foreach ($items as $item){
                    if ($item["min_value"]>$max_value){
                        $max_value = $item["min_value"];
                        $id = $item["id"];
                    }
                }
                $test3[$key] = $id;
            }
        }
        if (!$response["selected_gift_modificator_ids"]) $response["selected_gift_modificator_ids"] = [];
        $response["gift_status"] = $gift_statuses;
        $response["gifts_remains_cost"] = $gifts_remains_cost;
        $response["gifts_category_statuses"] = $test1;
        $response["gifts_category_remains_cost"] = $test2;
        $response["next_opened_gift_ids"] = $test3;
        return $response;
    }
    // получаем total_cost для корзины под разные цены
    public function getTotalCost(){
        // получаем объект корзины и саму корзину
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        $cart = $this->ms2->cart->get();
        //$order = $ms2->order->get();
        
        // массив с ТВ полями где содержатся типы цен
        $typeMass = array(
            1 => "tvType",
            2 => "tvTypeDelivery",
            4 => "tvTypeDelivery",
            3 => "tvTypeRestourant");
        // перебераем типы цен для получения их ID
        foreach($typeMass as $deliveryKey => $typePriceName) {
            $org = $this->modx->getObject("modResource", $this->modx->request["id_org"]);
            $priceTypeIds[$deliveryKey] = $org->getTVValue($typePriceName);
        }
        // пустой массив для наполнения ценами
        $totalCost = [];
        
        // перебераем товары в корзине
        foreach ($cart as $key => $item) {
            // получаем id товара и его количество
            $itemId = $item["id"];
            $count = $item["count"];
            
            // получаем список цен из прайса по ID организации
            $priceObjects = $this->modx->getCollection("SoftjetsyncPrices",array("product_id" => $itemId));
            // перебераем полученные объекты цен
            foreach($priceObjects as $priceObject){
                // получаем тип цены для данного объекта цены
                $typeCheck = $priceObject->get("type");
                // получаем цену для данного объекта цены
                $price = $priceObject->get("price");
                // перебераем список ID интересующих нас типов цен
                foreach($priceTypeIds as $priceKey => $priceTypeId){
                    // если полученный тип совпал с нужным нам
                    if ($typeCheck == $priceTypeId){
                        $discount = ($priceKey == 1)? ($this->modx->getOption("app_discount_for_pickup")/100) * $price : 0;
                        // считаем сумму total_cost для данного типа
                        $totalCost[$priceKey]["total_cost"] += ($price - $discount) * $count;
                    }
                }
            }
            
        }
        
        $objectTest = [];
        
        //$this->modx->log(1,print_r($cart,1));
        return (object)$totalCost;
    }
    //Приводит корзину к удобному формату и каждому товару добавляет поля фото, тайтл, макс кол-во и описание
    public function updateCart($status){
        $json['promocodes'] = $_SESSION['minishop2']['promocodes'];
        $json['promocode_left_to_use'] = (array_key_exists("promocode_left_to_use",$_SESSION['minishop2'])? $_SESSION['minishop2']['promocode_left_to_use'] : 0);
        $cart_items = $_SESSION['minishop2']['cart'];
        $addtives_cost = $origin_addtives_cost = $originalTotalPrice = $size_cost = $type_cost = 0;
        foreach ($cart_items as $key=> $cart_item){
            if($key && array_key_exists("id",$cart_item)){
                $cart_items[$key]['key'] = $key;
                $sql = "SELECT value FROM `modx_ms2_product_options` WHERE modx_ms2_product_options.product_id = {$cart_item['id']} AND modx_ms2_product_options.key = 'max_count'";
                $max_product_count = $this->modx->query($sql);
                $max_product_count = $max_product_count->fetchAll(PDO::FETCH_ASSOC);
                $max_product_count = ($max_product_count[0]['value']);
                if ($max_product_count && !array_key_exists('promoitem', $cart_items[$key]["options"]["childs"])){
                    $cart_items[$key]['max_count'] = (int)$max_product_count;
                } else if (array_key_exists('promoitem', $cart_items[$key]["options"]["childs"])){
                    $cart_items[$key]['max_count'] = 1;
                } else {
                    $cart_items[$key]['max_count'] = 999999;
                }
                $res = $this->modx->getObject('modResource',$cart_item["id"]);
                $product_data = $this->modx->getObject('msProductData',$cart_item["id"]);
                $cart_items[$key]["is_combo"] = $product_data->is_comobo;
                $cart_items[$key]["id"] = (int)$cart_item["id"];
                if ($res->get('image')){
                    $cart_items[$key]["img_url"] = $this->modx->runSnippet('pthumb', [
                                            'input' => substr($res->get('image'), 1),
                                            'options' => '&h=300&w=300&q=100&bg=FFFFFF',
                                        ]);
                } else {
                    $cart_items[$key]["img_url"] = '/assets/template/images/content/fri.jpg';
                }
                $cart_items[$key]["title"] = $res->get('pagetitle');
                $cart_items[$key]["desc"] = $res->get('description');
                $cart_items[$key]["uuid"] = $res->get('alias');
                $card_type = $this->modx->getObject("msProductOption", array("product_id"=>$cart_item["id"], "key"=>"card_type"));
                foreach ($cart_item["options"]["childs"]["additives"] as $additive){
                    if ($card_type->value != "construct") {
                        $addtives_cost = $addtives_cost + ($additive["count"]*$additive["price"])*$cart_items[$key]['count'];
                        $origin_addtives_cost = $origin_addtives_cost + ($additive["count"]*$additive["original_price"])*$cart_items[$key]['count'];
                        $cart_items[$key]['price'] =  $cart_items[$key]['price'] + ($additive["count"]*$additive["price"]);
                        $cart_items[$key]['original_price'] =  $cart_items[$key]['original_price'] + ($additive["count"]*$additive["original_price"]);
                    }
                }
                foreach ($cart_item["options"]["childs"]["size"] as $size){
                    $size_cost = $size_cost - $size["price"]*$cart_items[$key]['count'];
                    $cart_items[$key]['price'] = $cart_items[$key]['price'] - $size["price"];
                    $cart_items[$key]['original_price'] = $cart_items[$key]['original_price'] - $size["price"];
                }
                foreach ($cart_item["options"]["childs"]["type"] as $type){
                    $type_cost = $type_cost - $type["price"]*$cart_items[$key]['count'];
                    $cart_items[$key]['price'] = $cart_items[$key]['price'] + $type["price"];
                    $cart_items[$key]['original_price'] = $cart_items[$key]['original_price'] + $type["price"];
                }
                
                $originalTotalPrice += $cart_items[$key]['original_price'] * $cart_items[$key]['count'];
                //return $cart_items[$key]['original_price'];
                $cart_items[$key]['price'] = $cart_items[$key]['original_price'];
            }
        }
        $cart_items = array_values($cart_items);
        //$discount_percent = (100 - $this->modx->getOption("app_discount_percent"))/100;
        //$size_cost = $size_cost*$discount_percent;
        //$addtives_cost = $addtives_cost;
        $json['products'] = $cart_items;
        $json["total_cost"] = $status["total_cost"]+$addtives_cost+$size_cost;
        $json["original_total_cost"] = $originalTotalPrice;
        $json["total_count"] = $status["total_count"];
        
        return $json;
    }
    //Проверяет есть ли промокод и выводит скидку, которая будет применена в последствии
    public function checkCodes($total_cost){
        //$_SESSION['minishop2']['promocodes'] = 'test-promo-3';
        //$_SESSION['minishop2']['promocodes'] = "U-1429-8244";
        //unset($_SESSION['minishop2']['promocodes']);
        /*$this->modx->log(1,'checkCodes start '.print_r([
            '$this->modx->request'=>$this->modx->request,
            '$total_cost'=>$total_cost,
            '$_SESSION[minishop2][promocodes]'=>$_SESSION['minishop2']['promocodes'],
            '$_SESSION[minishop2]'=>$_SESSION['minishop2'],
        ],1));*/
        
        $json = [];
        
        if ($_SESSION['minishop2']['promocodes']){
            // получаем товары из корзины
            $cart = $this->ms2->cart->get();
            $origin_price = $comboPrice = $origin_comboPrice = $discount_cost = $origin_discount_cost = $actual_discount = $origin_actual_discount = 0;
            // получаем цену курьера для товара
            foreach($cart as $item) {
                $origin_price += (int)$item["original_price"] * (int)$item["count"];
                $origin_price -= (int)$item["options"]["childs"]["size"][0]["price"] * (int)$item["count"];
                // получаем товар и проверяем не является ли он комбо
                $itemObject = $this->modx->getObject("msProduct",$item["id"]);
                if($itemObject->get("is_comobo")){
                    // если является, то считаем общую цену товаров типа "Комбо"
                    $comboPrice += (int)$item["price"] * (int)$item["count"];
                    $comboPrice -= (int)$item["options"]["childs"]["size"][0]["price"] * (int)$item["count"];
                    
                    $origin_comboPrice += (int)$item["original_price"] * (int)$item["count"];
                    $origin_comboPrice -= (int)$item["options"]["childs"]["size"][0]["price"] * (int)$item["count"];
                }
            }
            // общая цена с учётом вычета цена на "Комбо"
            $total_cost = $total_cost - $comboPrice;
            $origin_price = $origin_price - $origin_comboPrice;
            
            /*$this->modx->log(1,'checkCodes общая цена с учётом вычета цена на "Комбо" '.print_r([
                '$total_cost'=>$total_cost,
                '$origin_price'=>$origin_price,
            ],1));*/
            // проверяем их наличие
            if(count($cart)) {
                // если товары есть в карзине, то применяем промокод
                $code = $_SESSION['minishop2']['promocodes'];
                $sql = "SELECT * FROM modx_promo_base WHERE code = '$code'";
                $code_obj = $this->modx->getObject("PromoBase", array("code"=>$code));
                $current_code = $this->modx->query($sql);
                $current_code = $current_code->fetchAll(PDO::FETCH_ASSOC);
                
                
                $min_cart_cost = $current_code[0]['min_cart_cost'];
                $check_min_cart_cost = ((int)$total_cost >= (int)$min_cart_cost);
                
                if (!$check_min_cart_cost && ($current_code[0]['type'] == 1 || $current_code[0]['type']  == 5)){
                    $json['min_cart_cost'] = [
                        'check' => false,
                        'message' => 'Промокод не применен, вы не достигли нужной суммы',
                        'min_sum' => $min_cart_cost,
                    ];
                }
                
                //type 1 скидка в процентах
                if ($current_code[0]['type'] == 1 && $check_min_cart_cost){
                    $discount_percent = $current_code[0]['child_discount']/100;
                    
                    $actual_discount = round($total_cost*$discount_percent);
                    $discount_cost = round($total_cost - $actual_discount);
                    
                    $origin_actual_discount = round($origin_price*$discount_percent);
                    $origin_discount_cost = round($origin_price - $origin_actual_discount);
                    
                    $json["discount_percent"] = $current_code[0]['child_discount'];
                } else if ($current_code[0]['type'] == 5 && $check_min_cart_cost){
                    //type 5 скидка в рублях
                    $discount_rub = $current_code[0]['child_discount_rub'];
                    
                    $discount_rub = ($discount_rub <= $total_cost)? $discount_rub : $origin_price; // $total_cost
                    $discount_cost = round($total_cost - $discount_rub);
                    $discount_cost = ($discount_cost < 0)? 0 : $discount_cost;
                    $actual_discount = (double) $discount_rub;
                    
                    $discount_rub_or = ($discount_rub <= $origin_price)? $discount_rub : $origin_price;
                    $origin_discount_cost = round($origin_price - $discount_rub_or);
                    $origin_discount_cost = ($origin_discount_cost < 0)? 0 : $origin_discount_cost;
                    $origin_actual_discount = (double) $discount_rub_or;
                } else {
                    $discount_cost = $total_cost;
                    $origin_discount_cost = $origin_price;
                }
            } else {
                // если товаров нет в корзине, то удаляем промокод
                $discount_cost = $total_cost + $comboPrice;
                $actual_discount = 0;
                $_SESSION['minishop2']['promocodes'] = "";
                $_SESSION['minishop2']['promocode_left_to_use'] = 0;
            }
        }
        $json["promocodes"] = ($_SESSION['minishop2']['promocodes'])? $_SESSION['minishop2']['promocodes'] : "";
        $json["discount_cost"] = $discount_cost + $comboPrice;
        $json['actual_discount'] = $origin_actual_discount;
        $json["original_discount_cost"] = $origin_discount_cost + $origin_comboPrice;
        $json['original_actual_discount'] = $origin_actual_discount;
        $this->modx->log(1,'checkCodes response '.print_r([
            '$json'=>$json,
        ],1));
        return $json;
    }
    //Начисляет скилдку
    public function setDiscount($order){
        $contacts = $this->modx->getObject('msOrderAddress', array('id'=> $order->address));
        $discount = $contacts->discount; //получаем скидку
        if ($discount){
            $discount_cart_cost = $order->cart_cost - $contacts->discount; //цена покупок с учетом скидки
            $cost = $discount_cart_cost + $order->delivery_cost; //цена заказа. доставка+покупки
            $order->set("cart_cost",$discount_cart_cost);
            $order->set("cost",$cost);
            $order->save();
        }
    }
    //Удаляет промо-продукт, если кост ниже порога
    public function checkPromoWithCost($status){
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        $sql = "SELECT * FROM modx_promo_base WHERE type = 4 and activity = 1 ORDER BY value ASC"; // добавил сортировку по полю value, что бы промо товары выводились по акции на возростание
        $thresholds = $this->modx->query($sql);
        $thresholds = $thresholds->fetchAll(PDO::FETCH_ASSOC);
        $gifts = array_combine(array_column($thresholds, 'id_promo'), $thresholds);
        
        foreach ($thresholds as $threshold){
            $arr = [];
            $items = explode(",", $threshold['id_product']);
            $arr["min_count"] = (int)$threshold["value"];
            $arr["min_cost"] = (int)$threshold["value"];
            $arr["remainder_cost"] = (int)$arr["min_cost"] - (int)$status["total_cost"];
            $arr["remainder_cost"] = ($arr["remainder_cost"] < 0)? 0 : $arr["remainder_cost"];
            $arr["title"] = $threshold["name"];
            $arr["imgUrl"] = $threshold["image"];
            $arr["desc"] = $threshold["description"];
            $arr["id_promo"] = $threshold["id_promo"];
            foreach ($items as $item){
                $current_item = [];
                $res = $this->modx->getObject('modResource', $item);
                $current_item = array(
                    "title" => $res->get('pagetitle'),
                    "imgUrl" => $res->get('image'),
                    "price" => $res->get('price'),
                    "id" => $res->get('id'),
                );
                if ($res->get('price') == 0){
                    $current_item['max_gift'] = 1;
                }
                $arr["modificators"][] = $current_item;
                $json['gift_from_cost'][] = $arr;
            }
        }
        $json['gift_from_cost'] = array_combine(array_column($json['gift_from_cost'], 'id_promo'), $json['gift_from_cost']);
        $combine =  array_combine(array_column($json['gift_from_cost'], 'id_promo'), $json['gift_from_cost']);
        $json['gift_from_cost'] = array_values($json['gift_from_cost']);
        foreach ($json['gift_from_cost'] as $gift){
            if ($status['total_cost']<$gift['min_cost']){
                $cart = $this->ms2->cart->get();
                foreach ($gift['modificators'] as $gift_product){
                    foreach ($cart as $key=> $cart_prod){
                        if ($cart_prod["id"] == $gift_product["id"]){
                            $this->ms2->cart->change($key, 0);
                        } 
                    }
                }
            }
        }
        $tubmler = false;
        $master_ids = array_column($this->ms2->cart->get(), 'id');
        foreach ($json['gift_from_cost'] as $gift_product){
            foreach ($gift_product["modificators"] as $gift_object){
                if (in_array((int)$gift_object["id"], $master_ids)){
                    $tubmler = true;
                }
            }
        }
        foreach ($thresholds as $gift){
            $products = explode(',',$gift['id_product']);
            foreach ($products as $product){
                $array[$product] = $gift['gift_product_wrapper_id'];
            }
        }
        $actual_cart = $this->ms2->cart->get();
        foreach ($json['gift_from_cost'] as $gift){
            foreach ($gift['modificators'] as $gift_product){
                foreach ($actual_cart as $cart_item){
                    if ($gift_product["id"] == $cart_item["id"]){
                        $gifts_in_cart[] = $cart_item["id"];
                    }
                }
            }
        }
        foreach ($array as $key=> $item){
            if (in_array($key, $gifts_in_cart)){
                $wrappers[$item][] = $key;
            }
        }
        foreach ($wrappers as $wrapper){
            if (count($wrapper)>1){
                foreach ($wrapper as $wrap){
                    if ($wrap != $this->modx->request["id"]){
                        $kek = $wrap;
                    }
                }
                //$kek = min($wrapper);
                foreach ($actual_cart as $key=> $cart_item){
                    if ($cart_item["id"] == $kek){
                        $this->ms2->cart->change($key, 0);
                    }
                }
            }
        }
        $this->modx->log(1,"JESSAAA ".$kek);
        return $tubmler;
    }
    //проверяем стоимость доставки
    public function checkDeliveryCost($order_type, $id_org, $polygon_id, $total_cost){
        if ($order_type == 2 || $order_type == 4){ //если тип доставки курьером или "менеджер"
            // проверяем включена ли настройка получения данные из организации а не из полигона
            if ($this->modx->getOption("add_disabled_polygons")) {
                // получаем данные из организации
                $organisation = $this->modx->getObject("modResource",$id_org);
                $json["min_count_cost"] = ($organisation->getTVValue("tvMinCountCost"))? (int)$organisation->getTVValue("tvMinCountCost") : 0;
                if ($total_cost < (int)$organisation->getTVValue("tvMinFreeDeliveryCost") || (int)$organisation->getTVValue("tvMinFreeDeliveryCost") < 1){
                    $delivery_cost = ($organisation->getTVValue("tvFieldGeoDeliveryPrice"))? (int)$organisation->getTVValue("tvFieldGeoDeliveryPrice") : 0;
                } else {
                    $delivery_cost = 0;
                }
                $json["delivery_cost"] = $delivery_cost;
                $json["min_free_delivery_cost"] = ($organisation->getTVValue("tvMinFreeDeliveryCost"))? (int)$organisation->getTVValue("tvMinFreeDeliveryCost") : 0;
            } else {
                $this->modx->log(1,"ID полигона".$polygon_id);
                // получаем данные из полигона
                $polygon = $this->modx->getObject("PolygonsBase", array("Id" => $polygon_id));
                
                if($polygon) {
                    if (((int)$total_cost < (int)$polygon->get("free_shipping")) || (int)$polygon->get("free_shipping") == 0){
                        $delivery_cost = ($polygon->delivery_price)? (int)$polygon->delivery_price : 0;;
                    } else {
                        $delivery_cost = 0;
                    }
                }
                
                $json["min_count_cost"] = ($polygon->min_count_cost)? (int)$polygon->min_count_cost : 0;
                $json["delivery_cost"] = ($delivery_cost)? $delivery_cost : 0;
                $json["min_free_delivery_cost"] = ($polygon->free_shipping)? (int)$polygon->free_shipping : 0;
            }
            
        } else {
            $json["min_count_cost"] = 0;
            $json["delivery_cost"] = 0;
            $json["min_free_delivery_cost"] = 0;
        }
        return $json;
    }
    // удаляем столвые приборы, если в корзине кроме них больше ничего нет
    public function removeCutlery(){
        // получаем содержимое корзины
        $cartElements = $this->ms2->cart->get();
        // перебераем элементы корзины
        foreach ($cartElements as $key => $item) {
            // получаем количество товаров в корзине, и если там лежит один товар
            if(count($cartElements) == 1) {
                // сравниваем ID этого товара с ID указанным в системных настройках ID столовых приборов
                if($item["id"] == $this->modx->getOption("app_add_cutlery_id")) {
                    // если ID совпадает, то удаляем товар
                    $this->ms2->cart->change($key, 0);
                }
            }
        }
    }
    //Сбрасываем акцию на 50% для каждого третьего товара, для последующего пересчёта
    public function resetAShare(){
        // инициируем класс для работы с корзиной
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        // получаем содержимое корзины
        $cartElements = $this->ms2->cart->get();
        //$this->modx->log(1,"Корзина в методе: ".print_r($cartElements,1));
        
        foreach ($cartElements as $key => $item) {
            if ($item['options']["action"] && $item['options']["newPrice"]) {
                //$this->modx->log(1,"Удаляем элемент: ".$key);
                $this->ms2->cart->change($key, 0);
                //$ms2->cart->remove($key);
                
                if($cartElements[$item['options']['code']]['count'] > 0) {
                    $countOldItem = $cartElements[$item['options']['code']]['count'] + $item['count'];
                    $this->ms2->cart->change($item['options']['code'], $countOldItem);
                } else {
                    $this->ms2->cart->add($item['id'], $item['count']);
                }
                
                //$this->modx->log(1,"ключ товара: ".$item['options']['code'].", количество ".$countOldItem);
            }
        }
    }
    //Работа с акцией, на каждый третий товар скидка
    public function discountOnFavoriteProduct(){
        // инициируем класс для работы с корзиной
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        // получаем содержимое корзины
        $cartElements = $this->ms2->cart->get();
        //$this->modx->log(1,"Корзина в методе: ".print_r($cartElements,1));
        // создаём нулевые переменные количества товаров в корзине и количестов товаров с акцией
        $totalCount = $totalActionCount = 0;
        // создаём пустые массивы для цен (мы будем его сортировать и искать минимальную цену) и массив где будем хранить уникальные товары
        $itemIdPrice = $sortMassPrice = array();
        // перебераем все товары в корзине
        foreach ($cartElements as $key => $item) {
            
            if($key && array_key_exists("id",$item)) {
                // считаем общее число товаров в корзине
                $totalCount += $item['count'];
                
                // получаем товар по его id
                $prod = $this->modx->getObject("msProduct",$item["id"]);
                //$this->modx->log(1,"Проверка: ".$item["id"]);
                
                // условие на проверку опции товара
                if ($prod->get("third_action")) {
                    
                    $this->modx->log(1,"Перебор элемнтов в методе - ".$prod->get("third_action"));
                    
                    // считаем количестов товаров подходящий под кретерий по размеру
                    $totalActionCount += $item['count'];
                
                    // составляем массив с уникальными ценами, для того что бы посчитать минимальную цену
                    if(!in_array($key,array_keys($itemIdPrice))) {
                        $itemIdPrice[$key] = $item['price'];
                    }
                }
            }
            
        }
        //$this->modx->log(1,"Зашли в метод 3 товара: ".print_r($itemIdPrice,1));
        // сортируем полученный масси, что бы цены шли по возростанию в нём (от меньшего к большему)
        asort($itemIdPrice);
        // формируем новый массив, что бы каждый его элемент содержал ключ товара и его цену
        foreach($itemIdPrice as $key => $item) {
            $sortMassPrice[] = array("key" => $key, "price" => $item, "id" => $cartElements[$key]["id"]);
        }
        //$this->modx->log(1,print_r($sortMassPrice,1), 'HTML');
        
        // назначаем метку для товара который будет акционным
        $option["action"] = "promo50";
        
        $numCheck = $keyAddPromMass = $addItems = 0;
        
        $addPromMass = array();
        
        for($i = $totalActionCount; $i >= 3; $i = $i - 3){
            
            $addItems++;
            $addPromMass[$keyAddPromMass] = array(
                "id" => $sortMassPrice[$numCheck]["id"],
                "count" => $addItems
            );
            
            if($cartElements[$sortMassPrice[$numCheck]["key"]]['count'] - 1 == 0) {
                $cartElements[$sortMassPrice[$numCheck]["key"]]['count'] = $cartElements[$sortMassPrice[$numCheck]["key"]]['count'] - 1;
                unset($cartElements[$sortMassPrice[$numCheck]["key"]]);
                $numCheck++;
                $keyAddPromMass++;
                $addItems = 0;
            } else {
                $cartElements[$sortMassPrice[$numCheck]["key"]]['count'] = $cartElements[$sortMassPrice[$numCheck]["key"]]['count'] - 1;
            }
            
            $option['childs'] = $cartElements[$sortMassPrice[$numCheck]["key"]]['options']['childs'];
            $option['code'] = $sortMassPrice[$numCheck]["key"];
            
            $this->ms2->cart->set($cartElements);
            
        }
        
        foreach($addPromMass as $count){
            $this->ms2->cart->add($count['id'], $count['count'], $option);
        }

    }
    //Работа с акцией, на каждый третий товар скидка
    public function checkPriceFavoriteProduct(){
        // инициируем класс для работы с корзиной
        //$ms2 = $this->modx->getService('miniShop2');
        //$ms2->initialize($this->modx->context->key);
        // получаем содержимое корзины
        $cartElements = $this->ms2->cart->get();
        foreach ($cartElements as $key => $item) {
            // если товар отмечен как акционный, меняем ему цену    
            if ($item['options']["action"] && !$item['options']["newPrice"]) {
                $cartElements[$key]['old_price'] = $cartElements[$key]['price'];
                $sale = $cartElements[$key]['price']*($this->modx->getOption("app_cart_action3")/100);
                $cartElements[$key]['price'] = round(($cartElements[$key]['price'] - $sale), 2);
                $cartElements[$key]['options']["newPrice"] = 1;
            }
        }
        // сохраняем корзину с изменёнными ценами
        $this->ms2->cart->set($cartElements);
    }
    public function getMaxBonus($total_cost){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
        $msb2_cart_maximum_percent = $this->modx->getOption("msb2_cart_maximum_percent")/100;
        
        // проверяем включены ли бонусы ПраймХил
        if ($this->modx->getOption("loyalty_PH_program")){
            // если да, то получаем бонусы от праймХилл для пользователя
            $response = $this->prime_hill->getClient($profile);
            $user_points = ($response)? (int)$response["bonusBalance"] : 0 ;
        } else {
            // если нет, то получаем бонусы из модуля
            $bonus_obj = $this->modx->getObject('msb2User', array("user"=>$profile->internalKey));
            $user_points = $bonus_obj->points;
        }
        
        if ($user_points>=$total_cost*$msb2_cart_maximum_percent){
            $max_bonus = round((double)($total_cost*$msb2_cart_maximum_percent));
        } else {
            $max_bonus = (double)$user_points;
        }
        return $max_bonus;
    }
    // Метод на получение свободного времени бронирования на 3 дня вперёд
    public function getFreeTime(){
        
        $massMonth = array(
            1 => "ЯНВ",
            2 => "ФЕВ",
            3 => "МАР",
            4 => "АПР",
            5 => "МАЙ",
            6 => "ИЮНЬ",
            7 => "ИЮЛЬ",
            8 => "АВГ",
            9 => "СЕН",
            10 => "ОКТ",
            11 => "НОЯ",
            12 => "ДЕК");
        
        // формируем объект для запроса в бд
        $query = $this->modx->newQuery('OrgBookingTime');
        
        // передаваемая дата
        $dateNow = date("Y-m-d");//$this->modx->request['dateNow']; //"2021-04-22"; // текущий день date("Y-m-d"), можно передать дату вида "2021-04-23"
        // id организации
        $orgId = $this->modx->request['id_org']; //3691;
        // получаем объект организации
        $ordanisation = $this->modx->getObject("modResource",$orgId);
        // получаем параметр показывающий сколько временных промежутков нужно скрыть
        $hiddenTime = $ordanisation->getTVValue("tvHiddenTime");
        // получаем временной шаг для организации
        $timeStep = $ordanisation->getTVValue("tvTimeStep");
        // получаем количестово часов в указанном временном шаге
        $timeStepHour = explode(",",(String)$timeStep/60)[0];
        // получаем количество часов в указанном временном шаге
        $timeStepMin = $timeStep - $timeStepHour * 60;
        // получаем часы работы организации на неделю
        $timeWork = json_decode($ordanisation->getTVValue("org_timework"));
        // получаем количество бронирований для данной организации, которое можно совершить на данный промежуток времени
        $restrictionOfOrders = json_decode($ordanisation->getTVValue("tvRestrictionOfOrders"));
        // переводим указанную дату в UNIX формат
        $dayNow = strtotime($dateNow); 
        
        // получаем время начала для текущего рабочего дня
        $timeWorkStart = (array)$timeWork[0];
        // получаем время окончания для текущего рабочего дня
        $timeWorkStop = (array)$timeWork[1];
        
        // создаём массив для хранения свободного времени бронирования
        $freeTime = array();
        // подтягиваем системную настройку где указано количество дней достуных дня бронирования
        $daysAvailable = $this->modx->getOption("app_order_days_ahead");
        
        // если указанное значение на количество дней не равно 0
        if($daysAvailable){
            // переменная на перебор дней
            $day = 1;
            // получаем текущее время
            $timeNow = time();
            // создаём цикл на 3 дня вперёд
            while ($day <= $daysAvailable){
                // получаем текущий день - словом
                $dayWeek = date("l",$dayNow); // N - число, l - полное название
                //echo $dayWeek." (".$dayNow.")";
                // получаем время начала рабочего дня в UNIX формате (в секундах)
                $timeCheck = strtotime($dateNow ." ". $timeWorkStart[$dayWeek]);
                // получаем время окончания рабочего дня в UNIX формате (в секундах)
                $timeMax = strtotime($dateNow ." ". date("H:i", strtotime("02-02-2020".$timeWorkStop[$dayWeek]." - 1 hours"))); // минус 1 час к максимальному времени бронирования, изначально было $timeWorkStop[$dayWeek]
                
                // проверяем время окончания работчего дня, если оно меньше чем время начала дня, то нужно добавить день
                if ($timeMax < $timeCheck) {
                    $timeMax = strtotime($dateNow . " + 1 days" . $timeWorkStop[$dayWeek]);
                    //echo date("Y-m-d H:i",$timeMax)."<hr>";
                }
                
                // формируем запрос в БД по дате и времени
                $query->where(
                    array(
                        'booking_date:>=' => date("Y-m-d",$timeCheck),
                        'AND:booking_date:<=' => date("Y-m-d",$timeMax),
                        'AND:id_organisation:=' => $orgId,
                    )
                );
                // формируем пустой массив с уже занятым временем
                $timeTaken = $timeTakenCheck = array();
                // получаем список заказов по указанным датам
                $timeOrders = $this->modx->getCollection("OrgBookingTime",$query);
                // перебераем полученные заказы и формируем из них массив
                foreach($timeOrders as $timeOrder) {
                    //echo date("H:i",strtotime($timeOrder->get("booking_time")))."<br>";
                    //$timeTaken[$timeOrder->get("booking_date")][] = date("H:i",strtotime($timeOrder->get("booking_time"))); // просто выводим в массив сравнения все заказы на данный день
                    
                    // получаем время заказа, и по данному времени увеличиваем счётчик в массиве
                    $timeTakenCheck[$timeOrder->get("booking_date")][date("H:i",strtotime($timeOrder->get("booking_time")))]++;
                    // слудующим шагом проверяем не привысил ли счётчик по данному времени ограничения на количество бронирований, если превысил, то 
                    if($timeTakenCheck[$timeOrder->get("booking_date")][date("H:i",strtotime($timeOrder->get("booking_time")))] >= $restrictionOfOrders && $restrictionOfOrders != 0) {
                        $timeTaken[$timeOrder->get("booking_date")][] = date("H:i",strtotime($timeOrder->get("booking_time")));
                    } else if ($restrictionOfOrders == 0) {
                        $timeTaken = array();
                    }
                }
                
                //$this->modx->log(1,"Счётчик (".$restrictionOfOrders."): ".print_r($timeTakenCheck,1));
                //$this->modx->log(1,"Массив с недоступным временем: ".print_r($timeTaken,1));
                
                //echo "<hr>";
                $checkTimeHidden = $checkTimeHiddenNext = 1;
                
                //$this->modx->log(1,"Дата: ".date("Y-m-d H:i",$timeCheck));
                // переменная проверки наличия времени в бронировании
                $checkInArray = false;
                
                // формируем список из доступных дат
                while ($timeCheck < $timeMax) {
                    
                    //$this->modx->log(1,"Часы: ".$checkHiddenTime.", день ".$checkHiddenDay.", дата: ".date("Y-m-d",$timeNow));
                
                    //echo date("H:i",$timeCheck)." - ".date("H:i",$timeMax). " == ";
                    if(array_key_exists(date("Y-m-d",$timeCheck),$timeTaken)) {
                        if(in_array(date("H:i",$timeCheck),$timeTaken[date("Y-m-d",$timeCheck)])) {
                            $checkInArray = true;
                        } else {
                            $checkInArray = false;
                        }
                    }
                    // проверяем сформированное время по шагу с уже забронированным временем
                    if(!$checkInArray) {
                        if(((date("H",$timeNow) - date("H",$timeCheck)) < 0) && (date("d",$timeNow) == date("d",$timeCheck)) && $checkTimeHidden <= $hiddenTime) {
                            // увеличиваем текущее время на 
                            $timeNow = strtotime(date("Y-m-d H:i",$timeNow) . " + " .$timeStep. " minutes");
                            $checkTimeHidden++;
                        } else if (
                            (((date("H",$timeNow) - date("H",$timeCheck)) < 0) && (date("d",$timeNow) == date("d",$timeCheck)) && $checkTimeHidden > $hiddenTime) ||
                            ((date("d",$timeNow) != date("d",$timeCheck)) && $checkTimeHiddenNext > 1)) {
                            
                            //$this->modx->log(1,"Часы: ".(int)date("H",$timeNow)." - " .(int)date("H",$timeCheck). ", день: ".date("d",$timeNow)." - " .date("d",$timeCheck). ", дата: ".date("Y-m-d",$timeNow));
                            //$freeTime[date("Y-m-d",$timeCheck)][] = date("H:i",$timeCheck);
                            $freeTime[date("d",$timeCheck)." ".$massMonth[date("n",$timeCheck)]][] = date("H:i",$timeCheck)."-".date("H:i",strtotime(date("Y-m-d H:i",$timeCheck) . " + " .$timeStep. " minutes"));
                        } else {
                            $checkTimeHiddenNext++;
                        }
                        
                    }
                    // увеличиваем время для цикла
                    $timeCheck = strtotime(date("Y-m-d H:i",$timeCheck) . " + ".$timeStepHour." hours + ".$timeStepMin." minutes");
                }
                
                //echo " [".$dayWeek." = ".$timeWorkStart[$dayWeek]."] ";
                //echo " - ".date("Y-m-d",$dayNow)."<br>";
                
                // выставляем следующий день
                $dayNow = strtotime(date("Y-m-d",$dayNow) . " + 1 days");
                // получаем день недели по следующему дню
                $dayWeek = date("l",$dayNow);
                // меняем дату по следующему дню
                $dateNow = date("Y-m-d",$dayNow);
                
                // если следующий день не содержет часов начала дня, то ..
                while($timeWorkStart[$dayWeek] == ""){
                    // выставляем следующий день
                    $dayNow = strtotime(date("Y-m-d",$dayNow) . " + 1 days");
                    $dayWeek = date("l",$dayNow);
                    $dateNow = date("Y-m-d",$dayNow);
                }
                // меням день для цикла
                $day++;
                
            }
        } else {
            // если указано количество дней равное 0, то возвращаем пустой массив
            $freeTime = array();
        }
        // добавить в начала массива пустой массив "заказать сейчас"
        //$freeTime = array_merge(array("Заказать сейчас" => array()),$freeTime);
        
        // возвращаем результат
        return array("freeTime" => $freeTime);
    }
    
    // Метод на сохранение времени заказа
    public function setTimeOrder($dateOrder,$timeOrder,$idOrg,$idOrder){
        // получаем параметры для занесения в БД
        /*$dateOrder = $this->modx->request['dateOrder'];
        $timeOrder = $this->modx->request['timeOrder'];
        $idOrg = $this->modx->request['id_org'];
        $idOrder = $this->modx->request['id_order'];*/
        
        $timeSet = $this->modx->newObject("OrgBookingTime");
        $timeSet->set("booking_time",$timeOrder);
        $timeSet->set("booking_date",$dateOrder);
        $timeSet->set("id_organisation",$idOrg);
        $timeSet->set("id_order",$idOrder);
        $timeSet->set("active",1);
        $timeSet->save();
    }
    // Метод акций
    public function cartAdditives(){
        $sql = "SELECT * FROM modx_promo_base WHERE type = 4 and activity = 1 ORDER BY value ASC";
        $thresholds = $this->modx->query($sql);
        $thresholds = $thresholds->fetchAll(PDO::FETCH_ASSOC);
        foreach ($thresholds as $threshold){
            //$this->modx->log(1,$_POST['order_type']."/".$_POST['id_org'].": ".print_r($threshold,1));

            if (in_array($_GET['order_type'], explode(",",$threshold["order_types"])) || $threshold["order_types"] == ""){
                if(in_array($_GET['id_org'], explode(",",$threshold["orgs"])) || $threshold["orgs"] == ""){
                    
                    $status = $this->ms2->cart->status();
                    
                    $arr = [];
                    $items = explode(",", $threshold['id_product']);
                    $arr["min_count"] = (int)$threshold["value"];
                    $arr["min_cost"] = (int)$threshold["value"];
                    $arr["remainder_cost"] = (int)$arr["min_cost"] - (int)$status["total_cost"];
                    $arr["remainder_cost"] = ($arr["remainder_cost"] < 0)? 0 : $arr["remainder_cost"];
                    $arr["title"] = $threshold["name"];
                    $arr["imgUrl"] = ($threshold["image"])? $threshold["image"] : "";
                    $arr["desc"] = $threshold["description"];
                    $arr["id_promo"] = (int)$threshold["id_promo"];
                    $first_res = $this->modx->getObject('modResource', $items[0]);
                    //$arr["imgUrl"] = $first_res->get('image');
                    foreach ($items as $item){
                        $current_item = [];
                        $res = $this->modx->getObject('modResource', $item);
                        $current_item = array(
                            "title" => $res->get('pagetitle'),
                            "imgUrl" => ($res->get('image'))? $res->get('image') : "",
                            "price" => $res->get('price'),
                            "id" => $res->get('id'),
                        );
                    /*if ($res->get('price') == 0){
                        $current_item['max_gift'] = 1;
                    }*/
                    
                    $arr["modificators"][] = $current_item;
                    $json['gift_from_cost'][] = $arr;
                    }
                    
                }
                
            }
        }
        $json['gift_from_cost'] = array_combine(array_column($json['gift_from_cost'], 'id_promo'), $json['gift_from_cost']);
        $json['gift_from_cost'] = array_values($json['gift_from_cost']);
        if (!$json['gift_from_cost']){
            unset($json['gift_from_cost']);
        }
        
        return $json;
    }
    public function repeatOrder($request){
        //$this->clean(); //очищаем корзину
        $tumbler = false;
        $order = $this->modx->getObject("msOrder", $request["order_id"]);
        $order_address = $this->modx->getObject("msOrderAddress", $order->address);
        $products = $order->getMany('Products');
        $product_count = count($products);
        $start_count = 0;
        foreach ($products as $product){
            if ($product_object = $this->modx->getObject("msProduct", $product->product_id)){
                $product = $product->toArray();
                $json[] = $product;
            } else {
                $start_count++;
            }
        }
        foreach ($json as $product){
            $add_request["id"] = $product["product_id"];
            $add_request["comment"] = $product["comment"];
            $add_request["count"] = $product["count"];
            $add_request["id_org"] = $order_address->index;
            $add_request["order_type"] = $order->delivery;
            $add_request["additives"] = $product["additives"];
            $add_request["size"] = $product["size"];
            $add_request["type"] = $product["type"];
            $add_request["ingredients"] = $product["ingredients"];
            $this->add($add_request);
        }
        if ($start_count == $product_count){
            $request["repeat_order"]["code"] = 1;
            $request["repeat_order"]["title"] = "Таких товаров больше не существует.";
            $request["repeat_order"]["subtitle"] = "Таких товаров больше не существует.";
        } elseif ($start_count>0 && $start_count<$product_count){
            $request["repeat_order"]["code"] = 2;
            $request["repeat_order"]["title"] = "Товары могут отличаться.";
            $request["repeat_order"]["subtitle"] = "Некоторых товаров больше нет в продаже, поэтому меню может отличаться.";
        } else {
            $request["repeat_order"]["code"] = 3;
            $request["repeat_order"]["title"] = "Все гуд";
            $request["repeat_order"]["subtitle"] = "Все гуд";
        }
        return $this->getCart($request);
    }
    // меняем цены товара в зависимости от города и типа подачи
    public function changeThePrice($tmp,$request) {
        
        $keys = (array_keys($tmp));
        
        foreach ($keys as $key){
            if($tmp[$key]['id']){
                $getObject = $this->modx->getObject("msProduct",$tmp[$key]['id']);
                //$this->modx->log(1,"Удаляем (".$tmp[$key]['id'].")".$getObject->getTVValue("restourants"));
                if(in_array($request["id_org"],explode(",",$getObject->get("restourants")))) {
                    unset($tmp[$key]);
                } else {
                    
                    $currentPrice = $tmp[$key]['price'];
                    $tmp[$key]['original_price'] = $tmp[$key]['price'];
                    $id = $tmp[$key]['id'];
                    
                    if($this->modx->getOption("app_pricy_type") == "userprice"){
                        $sql = 'SELECT state FROM modx_user_attributes WHERE website = "'.$this->modx->token.'"';
                        $output = $this->modx->query($sql);
                        $price_type = $output->fetch(PDO::FETCH_ASSOC)['state'];
                    
                        if (!$price_type){
                            $price_type = $this->modx->getOption("add_default_type_price");
                        }
                    } else if($this->modx->getOption("app_pricy_type") == "cityprice"){
                        switch($request["order_type"]){
                            case 1:
                                $tvType = 37;
                            break;
                            case 2:
                                $tvType = 41;
                            break;
                            case 4:
                                $tvType = 41;
                            break;
                            case 3:
                                $tvType = 40;
                            break;
                        }
                        $sql = '
                            SELECT 
                                value
                            FROM 
                                modx_site_tmplvar_contentvalues 
                            WHERE 
                                tmplvarid = '.$tvType.' AND contentid = '.$request["id_org"];
                                    
                        $output = $this->modx->query($sql);
                        $city = $output->fetch(PDO::FETCH_ASSOC)['value'];
                        $price_type = ($city)? $city : 1;
                        
                        $sql = '
                            SELECT 
                                value
                            FROM 
                                modx_site_tmplvar_contentvalues 
                            WHERE 
                                tmplvarid = 41 AND contentid = '.$request["id_org"];
                                    
                        $output = $this->modx->query($sql);
                        $city = $output->fetch(PDO::FETCH_ASSOC)['value'];
                        $price_type_original = ($city)? $city : 1;
                    } else {
                        $price_type = $price_type_original = 0;
                    }
                    
                    if($price_type != 0) {
                        $sql = "
                            SELECT 
                                price
                            FROM 
                                modx_softjetsync_prices
                            WHERE 
                                modx_softjetsync_prices.product_id = '$id' AND modx_softjetsync_prices.type = '$price_type'
                            ";
                        $info_product = $this->modx->query($sql);
                        $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                        $newprice = $info_product[0]["price"];
                        $tmp[$key]['price'] = round((int)$newprice);
                        
                        $sql = "
                            SELECT 
                                price
                            FROM 
                                modx_softjetsync_prices
                            WHERE 
                                modx_softjetsync_prices.product_id = '$id' AND modx_softjetsync_prices.type = '$price_type_original'
                            ";
                        $info_product = $this->modx->query($sql);
                        $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                        $newprice = $info_product[0]["price"];
                        $tmp[$key]['original_price'] = round((int)$newprice);
                        
                        if($request["order_type"] == 1 && ($tmp[$key]['price'] == $tmp[$key]['original_price'])) {
                            $itemObject = $this->modx->getObject("msProduct",$id);
                            if($itemObject->get("is_comobo")) {
                                // если да, то цена без изменений
                            } else {
                                // если нет, то для самовывоза пересчитываем цену
                                $tmp[$key]['price'] = round($tmp[$key]['price'] - (($tmp[$key]['price'] * (int)$this->modx->getOption("app_discount_percent"))/100));
                            }
                        }
                        
                        if(count($tmp[$key]["options"]["childs"]["size"])) {
                            $modyfication = $tmp[$key]["options"]["childs"]["size"][0]["price"];
                            $modyfication += (count($tmp[$key]["options"]["childs"]["type"]))? (int)$tmp[$key]["options"]["childs"]["type"][0]["price"] : 0;
                            $tmp[$key]['price'] = $tmp[$key]['original_price'] - $modyfication;
                            $tmp[$key]['price'] = round((int)$tmp[$key]['price'] - (((int)$tmp[$key]['price'] * (int)$this->modx->getOption("app_discount_percent"))/100)) + $modyfication;
                        }
                        
                    } else {
                        // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
                        if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "price") {
                            $itemObject = $this->modx->getObject("msProduct",$id);
                            // получаем переменную товара которая показывает, является ли товар "комбо"
                            if($itemObject->get("is_comobo")) {
                                // если да, то цена без изменений
                                $tmp[$key]["price"] = (int)$itemObject->get("price");
                            } else {
                                $tmp[$key]["original_price"] = (int)$itemObject->get("price");
                                // если нет, то для самовывоза пересчитываем цену
                                $tmp[$key]["price"] = round((int)$itemObject->get("price") - (((int)$itemObject->get("price") * (int)$this->modx->getOption("app_discount_percent"))/100));
                            }
                        } else {
                            $itemObject = $this->modx->getObject("msProduct",$id);
                            $tmp[$key]["price"] = (int)$itemObject->get("price");
                        }
                    }
                    
                    // метод для пересчёта цены модификаторов
                    $keyModifyMass = array("additives","size","type");
                    foreach($keyModifyMass as $keyModify){
                        if(count($tmp[$key]["options"]["childs"][$keyModify])) {
                            foreach($tmp[$key]["options"]["childs"][$keyModify] as $keyAdd => $addItem) {
                                if($price_type != 0) {
                                    $sql = "
                                        SELECT 
                                            price
                                        FROM 
                                            modx_softjetsync_prices
                                        WHERE 
                                            modx_softjetsync_prices.product_id = '".$addItem["id"]."' AND modx_softjetsync_prices.type = '$price_type'
                                        ";
                                    $info_product = $this->modx->query($sql);
                                    $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                                    $newprice = $info_product[0]["price"];
                                    $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'] = round((int)$newprice);
                                    
                                    $sql = "
                                        SELECT 
                                            price
                                        FROM 
                                            modx_softjetsync_prices
                                        WHERE 
                                            modx_softjetsync_prices.product_id = '".$addItem["id"]."' AND modx_softjetsync_prices.type = '$price_type_original'
                                        ";
                                    $info_product = $this->modx->query($sql);
                                    $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                                    $newprice = $info_product[0]["price"];
                                    $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['original_price'] = round((int)$newprice);
                                    
                                    if($request["order_type"] == 1 && ($tmp[$key]['price'] == $tmp[$key]['original_price'])) {
                                        $itemObject = $this->modx->getObject("msProduct",$addItem["id"]);
                                        $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'] = round($tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'] - (($tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'] * (int)$this->modx->getOption("app_discount_percent"))/100));
                                    }
                                    
                                } else {
                                    // если тип подачи "самовывоз" и величина процента скидки по самовывозу существует, то высчитываем скидку и округляем
                                    if($request["order_type"] == 1 && $this->modx->getOption("app_discount_percent") && $this->modx->getOption("app_pricy_type") == "price") {
                                        $itemObject = $this->modx->getObject("msProduct",$addItem["id"]);
                                        
                                        $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]["original_price"] = (int)$itemObject->get("price");
                                        // если нет, то для самовывоза пересчитываем цену
                                        $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'] = round((int)$itemObject->get("price") - (((int)$itemObject->get("price") * (int)$this->modx->getOption("app_discount_percent"))/100));
    
                                    } else {
                                        $itemObject = $this->modx->getObject("msProduct",$addItem["id"]);
                                        $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'] = (int)$itemObject->get("price");
                                        $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]["original_price"] = $tmp[$key]["options"]["childs"][$keyModify][$keyAdd]['price'];
                                    }
                                }
                            }
                        }
                        
                    }
                    
                }
            }
            
        }
        //$this->modx->log(1, "Ключ: ".json_encode($tmp));
        return $tmp;
        
    }
    
    // список доступных акций, а так же их применение
    public function getOptions($request) {
        // массив с доступными акциями для корзины
        $arrayActions = array("Применение промокода","Использование бонусов","Скидка на самовывоз");
        // получаем доступное количество применений в корзине
        $maxSelectCount = $this->modx->getOption("app_max_select_count");
        $maxSelectCount = ($maxSelectCount)? (int)$maxSelectCount : 1;
        // минимальная стоимость заказа, после торой можно применять бонусы, промокоды и акции
        $minPriceForShares = $this->modx->getOption("app_min_price_for_shares");
        $minPriceForShares = ($minPriceForShares)? (int)$minPriceForShares : 500;
        
        $json["max_select_count"] = $maxSelectCount;
        $json["options"]["min_cost"] = $minPriceForShares;
        
        $max_bonus = $this->getMaxBonus($request["total_cost"]);
        
        $json["options"]["items"] = array(
            array("id" => 1,"title"=>"Применяем бонусы","value"=>$max_bonus,"type"=>"bonus"),
            array("id" => 3,"title"=>"Применить промокод","value"=>0,"type"=>"promo"),
            array("id" => 2,"title"=>"Скидка на самовывоз","value"=>10,"type"=>"delivery_sale"),
        );
        
        return $json;
    }
    
    // список доступных акций, а так же их применение
    public function logOut($request) {
        // получаем данные пользователя по токену
        if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])) return $this->returnError(1); //если нет такого токена, возвращаем ошибку
        // получаем телефон пользователя и кодируем его
        $base64code = hash('ripemd160', $profile->get("mobilephone"));
        // проверяем существует ли сессия с указанным ключом
        if(!$sessionUser = $this->modx->getObject("modSession",$base64code)) {
            $sessionUser = $this->modx->newObject("modSession");
            $sessionUser->set("id",$base64code);
        }
        
        // удаляем токен пользователя перед выходом из приложения, он будет сгенерирован при входе
        $profile->set("website","");
        $profile->save();
        
        // получаем сессию
        $request["data"]["session"] = $_SESSION["minishop2"];
        $request["data"]["cart"] = $this->getCart($request);
        unset($request["cart"]);
        
        //$this->modx->log(1,print_r($request["data"]["session"],1));
        
        $sessionUser->set("data","minishop2|".json_encode($request["data"]));
        $sessionUser->set("access",time());
        $sessionUser->save();
        
        $this->clean($request);
        
        return $request;
    }
    
    // список доступных акций, а так же их применение
    public function loadCart($request) {
        // получаем данные пользователя по токену
        if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])) return $this->returnError(1); //если нет такого токена, возвращаем ошибку
        // получаем телефон пользователя и кодируем его
        $base64code = hash('ripemd160', $profile->get("mobilephone"));
        // проверяем существует ли сессия с указанным ключом
        if($sessionUser = $this->modx->getObject("modSession",$base64code)) {
            $result = json_decode(str_replace("minishop2|","",$sessionUser->get("data")),1);
            $_SESSION["minishop2"] = $result["session"];
            $this->getCart($result["cart"]);
            $sessionUser->remove();
            //return $result["cart"];
        }
    }
    
    protected function productIsGift($product_id)
    {
        $product_id = (int)$product_id;
        
        $sql = "SELECT id_product FROM modx_promo_base WHERE type = 4 and activity = 1"
            //. " and id_product in ($product_id)"
            //. " limit 1"
            . "";
        
        $thresholds = $this->modx->query($sql);
        if(!$thresholds) {
            return false;
        }
        $thresholds = $thresholds->fetchAll(PDO::FETCH_ASSOC);
        if(!$thresholds) {
            return false;
        }
        
        foreach ($thresholds ?? [] as $threshold) {
            
            $id_products = explode(",", $threshold['id_product']);
            if(!$id_products) {
                continue;
            }
            foreach ($id_products ?? [] as $id_product) {
                if((int)$id_product === $product_id) {
                    return true;
                }
            }
        }
        
        return false;
    }
}
