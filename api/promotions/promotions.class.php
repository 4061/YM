<?php
class Promotions extends ModxApi{
    public function getPromotions(){
    $sql = "
    SELECT 
        id_promo as id,
        name as title,
        description as content,
        date_end,
        code as promocode,
        type as type,
        id_product as product_id,
        image as img_big,
        url as url,
        orgs as organisations,
        order_types,
        use_count,
        already_used,
        user_only,
        availability
    FROM 
        modx_promo_base 
    WHERE
        (type = 1 and activity = 1 OR
        type = 2 and activity = 1 OR
        type = 5 and activity = 1) AND
        (availability <> 2 AND availability <> 3) AND
        user_hidden = 0
    ";
    $promocodes = $this->modx->query($sql);
    $promocodes = $promocodes->fetchAll(PDO::FETCH_ASSOC);
    
    if($promocodes && $this->modx->token) {
        // проверяем относится ли данный промокод к указанному городу и указанному типу подачи
        if($_GET["id_org"] != 0 || $_GET["id_org"] != "") {
            $checkPromocodes = array();
            foreach($promocodes as $promocode) {
                if(in_array($_GET["id_org"],explode(",",$promocode["organisations"])) || $promocode["organisations"] == "") {
                    if(in_array($_GET["order_type"],explode(",",$promocode["order_types"])) || $promocode["order_types"] == "") {
                        $checkPromocodes[] = $promocode;
                    }
                }
            }
            $promocodes = $checkPromocodes;
        }
    } else {
       $promocodes = array(); 
    }
    
    $pdoFetch = $this->pdoFetch;
    if ($profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])){
        $ids = [];
        $categories = $this->modx->getCollection("PromoUsersSecret",array("id_user" => $profile->get("internalKey")));
        foreach ($categories as $cat){
            $ids[] = $cat->get("id_promo");
        }
        $ids = array_unique($ids);
        $secret_promotions = array(
            'class' => 'PromoBase',
            'where' => ["id_promo:IN" => $ids],
            'select' => ["
                `PromoBase`.`id_promo` as id, 
                name as title, description as content,
                date_end,
                id_product as product_id,
                code as promocode,
                type as type,
                child_discount as discount,
                image as img_big,
                url as url,
                orgs as organisations,
                order_types,
                use_count,
                already_used,
                user_only,
                availability"],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $pdoFetch->setConfig($secret_promotions);
        $secret_promotions = $pdoFetch->run();
        
        if(!$secret_promotions) $secret_promotions = array();
        
        // проверяем относится ли данный промокод к указанному городу и указанному типу подачи
        $checkSecretPromotions = array();
        foreach($secret_promotions as $secret_promotion) {
            if(in_array($_GET["id_org"],explode(",",$secret_promotion["organisations"])) || $secret_promotion["organisations"] == "") {
                if(in_array($_GET["order_type"],explode(",",$secret_promotion["order_types"])) || $secret_promotion["order_types"] == "") {
                   $checkSecretPromotions[] = $secret_promotion;
                }
            }
        }
        $secret_promotions = $checkSecretPromotions;
        
        if (is_array($secret_promotions)){
            $promocodes = array_merge($promocodes,$secret_promotions);
        }
    }
    // промокод для неавторизованных пользователей
    if (!$this->modx->token) {
        $secret_promotions = array(
            'class' => 'PromoBase',
            'where' => ["availability" => 3],
            'select' => ["
                `PromoBase`.`id_promo` as id, 
                name as title, description as content,
                date_end,
                id_product as product_id,
                code as promocode,
                type as type,
                child_discount as discount,
                image as img_big,
                url as url,
                orgs as organisations,
                order_types,
                use_count,
                already_used,
                user_only,
                availability"],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $pdoFetch->setConfig($secret_promotions);
        $secret_promotions = $pdoFetch->run();
        
        // проверяем относится ли данный промокод к указанному городу и указанному типу подачи
        $checkSecretPromotions = array();
        foreach($secret_promotions as $secret_promotion) {
            if(in_array($_GET["id_org"],explode(",",$secret_promotion["organisations"])) || $secret_promotion["organisations"] == "") {
                if(in_array($_GET["order_type"],explode(",",$secret_promotion["order_types"])) || $secret_promotion["order_types"] == "") {
                   $checkSecretPromotions[] = $secret_promotion;
                }
            }
        }
        $secret_promotions = $checkSecretPromotions;
        
        if (is_array($secret_promotions)){
            $promocodes = array_merge($promocodes,$secret_promotions);
        }
    }
    foreach ($promocodes as $key => $promocode){
            $image_small = str_replace("/assets/","assets/",$this->modx->runSnippet('pthumb', [
                'input' => $promocode["img_big"],
                'options' => '&h=140&q=200&far=1',]));
            $promocodes[$key]['img_small'] = $this->modx->getOption("site_url").$image_small;
            $promocodes[$key]['img_big'] = $this->modx->getOption("site_url").str_replace("/assets/","assets/",$this->modx->runSnippet('pthumb', [
                        'input' => $promocode["img_big"],
                        'options' => '&h=240&w=400&q=100&far=1',
                    ]));
            $promocodes[$key]['id'] = (int)$promocodes[$key]['id'];
            $promocodes[$key]['promocode'] = ($promocodes[$key]['promocode'])? $promocodes[$key]['promocode'] : "";
            $promocodes[$key]['product_id'] = (int)$promocodes[$key]['product_id'];
            $promocodes[$key]['type'] = (int)$promocodes[$key]['type'];
            if ($promocodes[$key]['date_end']){
                $promocodes[$key]['date_end'] = strtotime($promocode['date_end']);
            } else {
                $promocodes[$key]['date_end'] = "";
            }
            
            if(($promocode["availability"] == 2 || $promocode["user_only"] == 1) && $this->modx->token) {
                $userCheck = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token]);
                if($userCheck){
                    $countUse = $this->modx->getObject("PromoUsingUsers",array("id_user" => $userCheck->get("id")));
                    if((int)($promocode["use_count"] - (int)$countUse) < 1) unset($promocodes[$key]);
                }
            } else if ((int)($promocode["use_count"] - (int)$promocode["already_used"]) < 1){
                unset($promocodes[$key]);
            }
    }
    $promocodes = array_values($promocodes);
    return ($promocodes);
    }
}