<?php
class Courier extends ModxApi{
    //добавляем рейс
    public function addFlight($orders, $courier_id, $comment, $status_id){
        $flight = $this->modx->newObject("CourierFlights", array("comment"=>$comment, "courier_id"=>$courier_id, "status_id"=>1, "date_start"=>date("Y-m-d H:i:s")));
        $i = 0;
        foreach ($orders as $order){
            $orders[] = $this->modx->newObject("CourierOrders", array(
                "order_id"=>$order,
                "status_id"=>$status_id,
                "menuindex"=>$i,
                )
            );
            $i++;
        }
        $flight->addMany($orders);
        $flight->save();
        return $this->getFlight($flight->id);
    }
    //удалить рейс
    public function removeFlight($id){
        $flight = $this->modx->getObject("CourierFlights",$id);
        $flight->remove();
        return true;
    }
    //изменить рейс
    public function changeFlight($orders, $flight_id, $status_id, $date_end, $courier_id, $comment){
        $flight = $this->modx->getObject("CourierFlights", $flight_id);
        if ($status_id) $flight->set("status_id", $status_id);
        if ($courier_id) $flight->set("courier_id", $courier_id);
        if ($comment) $flight->set("comment", $comment);
        //if ($date_end) $flight->set("date_end", $date_end);
        $status = $this->modx->getObject("CourierOrderStatuses", $status_id);
        if ($status->final) $flight->set("date_end", date("Y-m-d H:i:s"));
        $max_order = array(
            'class' => 'CourierOrders',
            'sortdir' => "DESC",
            'sortby' => "menuindex",
            'where' => ['flight_id' => $flight_id],
            'return' => 'data',
            'limit' => 1
        );
        $this->pdoFetch->setConfig($max_order);
        $max_order = $this->pdoFetch->run();
        $i = $max_order[0]['menuindex']+1;
        foreach ($orders as $order){
            if (!$ord = $this->modx->getObject("CourierOrders", array("order_id"=>$order))){
                $orders[] = $this->modx->newObject("CourierOrders", array(
                    "order_id"=>$order,
                    "status_id"=>1,
                    "menuindex"=>$i,
                    )
                );
                $i++;
            }
        }
        $flight->addMany($orders);
        $flight->save();
        return $this->getFlight($flight_id);
    }
    //получить рейс
    public function getFlight($flight_id){
        $orders = array(
            'class' => 'CourierOrders',
            'leftJoin' => [
                'msOrder' => ['class'=> 'msOrder', 'on'=>'CourierOrders.order_id = msOrder.id'],
                'msOrderAddress' => ['class'=>'msOrderAddress', 'on' => 'msOrder.address = msOrderAddress.id'],
                'CourierOrderStatuses'=> ['class'=> 'CourierOrderStatuses', 'on'=> 'CourierOrderStatuses.id = CourierOrders.status_id']
                ],
            'select' => [
                'msOrder' => 'msOrder.id as order_id',
                'msOrderAddress' => '
                    msOrderAddress.country, 
                    msOrderAddress.city,
                    msOrderAddress.street, 
                    msOrderAddress.house_num, 
                    msOrderAddress.flat_num,
                    msOrderAddress.entrance,
                    msOrderAddress.metro as doorphone,
                    msOrderAddress.floor',
                'CourierOrders' => 'CourierOrders.flight_id, menuindex',
                'CourierOrderStatuses' => '
                    CourierOrderStatuses.name as status_name,
                    CourierOrderStatuses.description as status_description,
                    CourierOrderStatuses.text_color as status_text_color,
                    CourierOrderStatuses.wrapper_color as status_text_wrapper'
                ],
            'where' => ["flight_id" => $flight_id],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($orders);
        $orders = $this->pdoFetch->run();
        $flights = array(
            'class' => 'CourierFlights',
            'leftJoin' => [
                'Status' => ['class'=> 'CourierFlightStatuses', 'on'=>'Status.id = CourierFlights.status_id'],
                ],
            'select' => [
                'CourierFlights'=>'CourierFlights.id, CourierFlights.status_id, CourierFlights.comment, CourierFlights.courier_id, CourierFlights.date_start, CourierFlights.date_end', 
                'Status' => 'Status.name as status_name, Status.description as status_description, Status.text_color as status_text_color, Status.wrapper_color as status_wrapper_color'
                ],
            'where' => ["id" => $flight_id],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($flights);
        $flights = $this->pdoFetch->run();
        $json = $flights[0];
        $json["orders"] = $orders;
        return $json;
    }
    //получить историю рейсов для курьера
    public function getFlightsHistory($token){
        $profile = $this->modx->getObject('modUserProfile', ['website' => $token]);
        $flights = array(
            'class' => 'CourierFlights',
            'leftJoin' => [
                'Status' => ['class'=> 'CourierFlightStatuses', 'on'=>'Status.id = CourierFlights.status_id'],
                ],
            'select' => [
                'CourierFlights'=>'CourierFlights.id, CourierFlights.status_id, CourierFlights.comment, CourierFlights.courier_id, CourierFlights.date_start, CourierFlights.date_end', 
                'Status' => 'Status.name as status_name, Status.description as status_description, Status.text_color as status_text_color, Status.wrapper_color as status_wrapper_color'
                ],
            'where' => ["courier_id" => $profile->internalKey],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($flights);
        $flights = $this->pdoFetch->run();
        $master_ids = array_column($flights, 'id');
        $orders = array(
            'class' => 'CourierOrders',
            'leftJoin' => [
                'msOrder' => ['class'=> 'msOrder', 'on'=>'CourierOrders.order_id = msOrder.id'],
                'msOrderAddress' => ['class'=>'msOrderAddress', 'on' => 'msOrder.address = msOrderAddress.id']
                ],
            'select' => [
                'msOrder' => 'msOrder.id',
                'msOrderAddress' => '
                    msOrderAddress.country, 
                    msOrderAddress.city,
                    msOrderAddress.street, 
                    msOrderAddress.house_num, 
                    msOrderAddress.flat_num,
                    msOrderAddress.entrance,
                    msOrderAddress.metro as doorphone,
                    msOrderAddress.floor',
                'CourierOrders' => 'CourierOrders.flight_id'
                ],
            'where' => ["flight_id:IN" => $master_ids],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($orders);
        $orders = $this->pdoFetch->run();
        //return $orders;
        $flights = array_combine(array_column($flights, 'id'), $flights);
        foreach ($orders as $key=> $order){
            $flights[$order["flight_id"]]['order_addresses'][] = $order;
        }
        $flights = array_values($flights);
        return $flights;
    }
    //получить статусы рейсов
    public function getFlightStatuses(){
        $flight_statuses = array(
            'class' => 'CourierFlightStatuses',
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($flight_statuses);
        $flight_statuses = $this->pdoFetch->run();
        return $flight_statuses;
    }
    //получить статусы заказов 
    public function getOrderStatuses(){
        $flight_statuses = array(
            'class' => 'CourierOrderStatuses',
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($flight_statuses);
        $flight_statuses = $this->pdoFetch->run();
        return $flight_statuses;
    }
    //изменить параметры заказа в рейсе
    public function changeOrder($flight_id, $order_id, $menuindex, $status_id){
        $order = $this->modx->getObject("CourierOrders", array("order_id"=>$order_id, "flight_id"=>$flight_id));
        $order->set("menuindex", $menuindex);
        $order->set("status_id", $status_id);
        $order->save();
        $order_array = $order->toArray();
        return $order_array;
    }
    //создать статус курьера
    public function addOrderStatus($name, $description, $text_color, $wrapper_color, $final){
        $order_status = $this->modx->newObject('CourierOrderStatuses', array(
            "name" => $name,
            "description" => $description,
            "text_color" => $text_color,
            "description" => $description,
            "wrapper_color" => $wrapper_color,
            "final"=> $final
        ));
        $order_status->save();
        return $this->getOrderStatuses();
    }
    //создать статус рейса
    public function addFlightStatus($name, $description, $text_color, $wrapper_color, $final){
        $flight_status = $this->modx->newObject('CourierFlightStatuses', array(
            "name" => $name,
            "description" => $description,
            "text_color" => $text_color,
            "description" => $description,
            "wrapper_color" => $wrapper_color,
            "final" => $final
        ));
        $flight_status->save();
        return $this->getFlightStatuses();
    }
    
    //удалить статус курьера
    public function removeOrderStatus($id){
        $order_status = $this->modx->getObject("CourierOrderStatuses", $id);
        $order_status->remove();
        return $this->getOrderStatuses();
    }
    //удалить статус рейса
    public function removeFlightStatus($id){
        $flight_status = $this->modx->getObject("CourierFlightStatuses", $id);
        $flight_status->remove();
        return $this->getFlightStatuses();
    }
    //получить статус курьера
    public function getOrderStatus($id){
        $order_status = $this->modx->getObject("CourierOrderStatuses", $id);
        $status = $order_status->toArray();
        return $status;
    }
    //получить статус курьера
    public function getFlightStatus($id){
        $flight_status = $this->modx->getObject("CourierFlightStatuses", $id);
        $status = $flight_status->toArray();
        return $status;
    }
    //изменить статус курьера
    public function changeOrderStatus($id, $name, $description, $text_color, $wrapper_color,$final){
        $order_status = $this->modx->getObject("CourierOrderStatuses", $id);
        $order_status->set("name", $name);
        $order_status->set("description", $description);
        $order_status->set("text_color", $text_color);
        $order_status->set("wrapper_color", $wrapper_color);
        $order_status->set("final", $final);
        $order_status->save();
        return $this->getOrderStatus($id);
    }
    //изменить статус рейса
    public function changeFlightStatus($id, $name, $description, $text_color, $wrapper_color, $final){
        $flight_status = $this->modx->getObject("CourierFlightStatuses", $id);
        $flight_status->set("name", $name);
        $flight_status->set("description", $description);
        $flight_status->set("text_color", $text_color);
        $flight_status->set("wrapper_color", $wrapper_color);
        $flight_status->set("final", $final);
        $flight_status->save();
        return $this->getFlightStatus($id);
    }
}