<?php
class Rates extends ModxApi{
    public function rateOrder(){
        $order = $this->modx->getObject("msOrder", $this->modx->request["order_id"]);
        $order_addr = $this->modx->getObject("msOrderAddress", $order->address);
        $order->set("rating", $this->modx->request["general_rating"]);
        $order->save();
        $answer_options = $this->modx->getCollection("AnswerOptions", array("id:IN"=>$this->modx->request["answer_options"]));
        $string = "";
        foreach ($answer_options as $answer_option){
            $string .= $answer_option->answer_option."; ";
        }
        
        // если артинку передали
        if($this->modx->request["photo"]) {
        	// получаем картинку из приложения
        	$photo = 'data:image/png;base64,'.$this->modx->request["photo"];
        	
        	// получаем и преобразуем код в картинку
        	list($type, $photo) = explode(';', $photo);
        	list(, $photo) = explode(',', $photo);
        	$photo = base64_decode($photo);
        	
        	$directory = "/assets/images/orders/";
        	// сохраняем картинку для отзыва
        	$category = $_SERVER['DOCUMENT_ROOT'].$directory.$this->modx->request["order_id"];
        	// создаём директорию
        	if (!mkdir($category, 0766, true)) $this->modx->log(1,"Ошибка создания директории: ".$category);
        	// формируем название картинки
        	$file = "photo-".$order->get("user_id").".png";
        	// сохраняем картинку
        	if(!file_put_contents($category."/".$file, $photo)) $this->modx->log(1,"Ошибка создания файла: ".$category."/".$file);
        	
        	$filePath = $directory.$this->modx->request["order_id"]."/".$file;
        	
        	//$this->modx->log(1,"Старая картинка: ".$filePath);
        	
        	$phpthumbImg = $this->modx->runSnippet('pthumb', [
                    'input' => substr($filePath, 1),
                    'options' => '&w=840&q=100&bg=FFFFFF',
                ]);
                
            //$this->modx->log(1,"Новая картинка: ".$phpthumbImg);
        	
        	$order_addr->set("comment_photo", $phpthumbImg); // $filePath
        	//unlink($category."/".$file);
        }
        
        $order_addr->set("commemtAssessment", $this->modx->request["comment"]);
        $order_addr->set("courierAssessment", $string);
        $order_addr->save();
        /*if ($this->modx->request["general_rating"]<=2){
            $this->modx->runSnippet('sendBadRating',array(
                'type' => 'order',
                'orderId' => $this->modx->request["order_id"],
                'taste' => $taste,
                'beauty' => $beauty,
                'speed' => $speed,
                'temp' => $temp,
                'courier' => $courier,
                'comment' => $this->modx->request["comment"],
                'rating' => $this->modx->request["general_rating"]
            ));
        }*/
        // если артинку передали
        if($this->modx->request["photo"]) unlink($category."/".$file);
    }
    public function rateObject(){
        $sql='SELECT internalKey FROM modx_user_attributes WHERE website = "'.$token.'"';
        $sqlOutput = $this->modx->query($sql);
        $idUser = $sqlOutput->fetch(PDO::FETCH_ASSOC)['internalKey'];
        
        if($idUser) {
        
            // получаем переданную json строку с данными
            $dataList = $this->modx->request['ratings'];
            
            //return json_encode($dataList);
            
            foreach($dataList as $key => $data){
                
                // проверяем указан ли был рейтинг, а так же не является ли комментарий пустым
                if ($data["rating"] > 0 && $data["comment"] != "") {
                    // формируем SQL запрос, что бы понять создан ли уже комментарий текущий пользователь для указанного объекта
                    $sql = 'SELECT id_rating FROM modx_rating WHERE id_resource = ' .$data['id']. ' AND id_user = ' .$idUser;
                    //return $sql;
                   
                    $elementQuery = $this->modx->query($sql);
                    $id_rating = $elementQuery->fetch(PDO::FETCH_ASSOC)['id_rating'];
                    // если создал ..
                    if($id_rating){
                        
                        $sql = 'UPDATE modx_rating SET `comment_rating` = "' .$data['comment']. '", `rating_value` = '.$data['rating'].', `date_of_editing` = "' .date("Y-m-d H:i:s"). '" WHERE id_rating = ' .$id_rating;
                        $elementQuery = $this->modx->query($sql);
                        /*// .. то получаем его объект и меняем данные
                        $rating = $this->modx->getObject("Rating",$element['id_rating']);
                        // устанавливаем дату и время изменения комментария
                        $rating->set("date_of_editing",date("Y-m-d H:i:s"));*/
                        
                        // параметры для ответа
                        $result[$key]["id_rating"] = $id_rating;
                        $result[$key]["action"] = "edit";
                        
                    } else {
        
                        // если комментарий отсутствует, то создаём его
                        $rating = $this->modx->newObject("Rating");
                        $rating->set("active",1);
                        
                        // заносим нужные нам зачения
                        $rating->set("id_resource",$data['id']);
                        $rating->set("id_user",$idUser);
                        $rating->set("comment_rating",$data['comment']);
                        $rating->set("rating_value",$data['rating']);
                        $rating->save();
                        
                        //return $data["rating"];
                        
                        // получаем id толкьо что созданного комментария
                        $sql = 'SELECT id_rating FROM modx_rating WHERE id_resource = ' .$data['id']. ' AND id_user = ' .$idUser;
                        //return $sql;
                        $elementQueryLast = $this->modx->query($sql);
                        $result[$key]["id_rating"] = $elementQueryLast->fetch(PDO::FETCH_ASSOC)["id_rating"];
                        
                        $result[$key]["action"] = "created";
                    }
                    
                    
                    // формируем запрос для получения всех оценок по данному ресурсу
                    $sql = 'SELECT rating_value FROM modx_rating WHERE id_resource = ' .$data['id']. " AND active = 1";
                    $allRatingElements = $this->modx->query($sql);
                    // обнуляем счётчик и сумму для рейтинга
                    $count = $summRating = 0;
                    // перебераем полученные значения
                    foreach($allRatingElements as $value) {
                        // считаем сумму рейтинга
                        $summRating += $value['rating_value'];
                        // увеличиваем счётчик
                        $count++;
                    }
                    // проверяем не равна ли сумма рейтинга 0
                    if($summRating) {
                        // если не равна, то рассчитываем рейтинг по формуле
                        $allRating = (float)(round(($summRating/$count), 1));
                    } else {
                        // если равна 0, то присваиваем 0 для общего рейтинга
                        $allRating = 0;
                    }
                    
                    $sql = 'SELECT id FROM modx_ms2_product_options WHERE product_id = ' .$data['id']. ' AND `key` = "item_rating"';
                    $ratingAllQuery = $this->modx->query($sql);
                    $id = $ratingAllQuery->fetch(PDO::FETCH_ASSOC)["id"];
                    
                    if($id) {
                        $sql = "UPDATE modx_ms2_product_options SET value = '" .$allRating. "' WHERE product_id = " .$data['id']. " AND `key` = 'item_rating'";
                        $this->modx->query($sql);
                    } else {
                        $sql = "INSERT INTO modx_ms2_product_options (`product_id`,`key`,`value`) VALUES (".$data['id'].",'item_rating','".$allRating."')";
                        $this->modx->query($sql);
                    }
                    
                    // заносим новый рейтинг в ответ
                    $result[$key]["allRating"] = round($allRating,4); // number_format($allRating, 1, '.', '');
                    $result[$key]["countRating"] = $count;
                
                // если комментарий есть, а рейтинг не выставлен, возвращаем ошибку    
                } else if ($data["rating"] > 0) {
                    $result["error"] = "Вы не указали комментарий для объекта";
                    break;
                // если рейтинг есть, а комментарий отсутствует, возвращаем ошибку    
                } else if ($data["comment"] != "") {
                    $result["error"] = "Вы не указали рейтинг для объекта";
                    break;
                // если ничего не указано, возвращаем ошибку
                } else {
                    $result["error"] = "Вы не указали ни комментарий, ни рейтинг для объекта";
                    break;
                }
                
            }
        
        } else {
            $result["error"] = "Пользователь с таким токеном отсуствует";
        }
        
        $json["result"] = $result;

        // возвращаем результат
        return ($json);
    }
    //public function sendBadRating
}