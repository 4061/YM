<?php
define('MODX_API_MODE', true);
require '../index.php';
// Включаем обработку ошибок
/** @var modX $modx */
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
//$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');
$_REQUEST['JSON'] = json_decode(
         file_get_contents('php://input'), true
);
$_POST['JSON'] = & $_REQUEST['JSON'];
$token = getallheaders();
$headers = getallheaders();

if ($token['Authorization']) $token2 = $token['Authorization'];
$token = substr($token2, 7);

$_POST['JSON'] = & $_REQUEST['JSON'];
require 'api.class.php';
$api = new ModxApi($modx,$_POST["JSON"], $headers);
$api->run($_SERVER['REQUEST_URI'], $_POST["JSON"], $headers);
echo $api->getResponse();