<?php
class Content extends ModxApi{
    public function getParents(){
        $org = $this->modx->getObject("modResource", $_GET["id_org"]);
        function getCatalog($parent_id_cat,$modx){
            //global $modx;
            $sql = "
            SELECT 
                modx_site_content.id, 
                modx_site_content.pagetitle as title, 
                modx_site_content.description,
                modx_site_content.menuindex        
            FROM 
                `modx_site_content`
            WHERE 
                modx_site_content.parent = '$parent_id_cat' AND 
                modx_site_content.class_key = 'msCategory' AND 
                modx_site_content.deleted = 0 AND 
                modx_site_content.published = 1";
            $content_id = $modx->query($sql);
            $resources = $content_id->fetchAll(PDO::FETCH_ASSOC);
            foreach ($resources as $key => $res){
                $res_obj = $modx->getObject('modResource', $res['id']);
                $resources[$key]['id'] = (int)$resources[$key]['id'];
                $resources[$key]['menuindex'] = (int)$resources[$key]['menuindex'];
                $resources[$key]['image']=$res_obj->getTVValue('image');
                $resources[$key]['parent']=(int)$res_obj->get('parent');
            }
            foreach ($resources as $key => $res){
                $res = getCatalog($res['id'],$modx);
                if ($res) $resources[$key]['data'] = $res;
            } 
            return $resources;
        }
        function mysort1($a, $b){
            return $a['menuindex'] <=> $b['menuindex'];
        }
        if ($this->modx->getOption("get_categ_by_orgs")){
            $category_id = $org->getTVValue("category_parents");
        } else {
            $category_id = $this->modx->getOption('content_parent');
        }
        $json["data"] = getCatalog($category_id,$this->modx);
        usort($json["data"], 'mysort1');
        return $json;
    }
    public function getProduct(){
        //if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])) return $this->returnError(1); //если нет такого пользователя - ошибка
        //проверяем какой тип цены надо использовать
        if ($this->modx->getOption("app_pricy_type") == "userprice"){
            if ($profile->state){
                $price_type = $profile->state;
            } else {
                $price_type = $this->modx->getOption("add_default_type_price");;
            }
            $price_type_original = $price_type;
        } elseif ($this->modx->getOption("app_pricy_type") == "cityprice") {
            if($this->modx->getOption("app_always_courier_price")) {
                $order_type_tv = "tvTypeDelivery";
            } else {
                switch ($_GET["order_type"]){
                    case '1': //самовывоз
                        $order_type_tv = "tvType";
                        break;
                    case '2': //курьер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '4': //менеджер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '3': //ресторан
                        $order_type_tv = "tvTypeRestourant";
                        break;
                }
            }
            
            $org = $this->modx->getObject("modResource", $_GET["id_org"]);
            $price_type = $org->getTVValue($order_type_tv);
            $price_type_original = $org->getTVValue("tvTypeDelivery");
        } else {
            $price_type = $price_type_original = 0;
        }
        
        $default = array(
            'class' => 'msProduct',
            'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0, 'id' => $_GET["product_id"]],
            'leftJoin' => [
              'Data' => ['class' => 'msProductData'],
              'allRating' => ['class' => 'msProductOption', 'on' => 'allRating.product_id = msProduct.id AND allRating.key = "item_rating"'],
              'Option19' => ['class' => 'msProductOption', 'on' => 'Option19.product_id = msProduct.id AND Option19.key = "card_type"'],
                'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
                'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
                'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
                'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
                'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
                'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
            ],
            'select' => [
                'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`, `msProduct`.`content`',
                'Data' => '`Data`.`restourants`,`Data`.`order_types`,`Data`.`article`, `Data`.`image` as img_url, `Data`.`thumb` as img_small',
                'allRating' => 'allRating.value as item_rating',
                'Option19' => 'Option19.value as card_type',
                'Option1' => 'Option1.value as energy_size',
                'Option2' => 'Option2.value as energy_allergens',
                'Option3' => 'Option3.value as energy_value',
                'Option4' => 'Option4.value as energy_carbohydrates',
                'Option5' => 'Option5.value as energy_protein',
                'Option6' => 'Option6.value as energy_oils',
            ],
            'sortby' => 'msProduct.id',
            'sortdir' => 'ASC',
            'groupby' => 'msProduct.id',
            'return' => 'data'
        );
        
        if($price_type != 0) {
            $default['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type];
            $default['select']['Price'] = 'Price.price, Price.old_price';
            $default['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = msProduct.id AND PriceOriginal.type ='.$price_type_original];
            $default['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
        } else {
            $default['select']['msProduct'] = $default['select']['msProduct'].', price, old_price, price as original_price';
        }
        
        $this->pdoFetch->setConfig($default);
        $row = $this->pdoFetch->run();
        $product = $row[0];
        $product["energy"] = array(
            "energy_size" => $product["energy_size"],  
            "energy_allergens" => $product["energy_allergens"],  
            "energy_value" => $product["energy_value"],  
            "energy_carbohydrates" => $product["energy_carbohydrates"],
            "energy_protein" => $product["energy_protein"],
            "energy_oils" => $product["energy_oils"],    
        );
        unset($product["energy_size"]);
        unset($product["energy_allergens"]);
        unset($product["energy_value"]);
        unset($product["energy_carbohydrates"]);
        unset($product["energy_protein"]);
        unset($product["energy_oils"]);;
        $product["id"] = (int)$product["id"];
        $product["article"] = (int)$product["article"];
        $product["parent"] = (int)$product["parent"];
        $product["desc"] = $product["description"];
        $product["price"] = (double)$product["price"];
        $product["original_price"] = (double)$product["original_price"];
        unset($product["restourants"]);
        unset($product["order_types"]);
        unset($product["description"]);
        $modifiers = $this->modx->getCollection("msProductLink", array("master"=>$_GET["product_id"]));
        //return $product;
        foreach ($modifiers as $modifier){
            switch ($product["card_type"]){
                case "halves":
                    if ($modifier->link == 13){
                        $modifier_obj = $this->modx->getObject("msProduct", $modifier->slave);
                        $modifier_ids[] = $modifier->slave;
                        if ($par = $this->modx->getObject("modResource", $modifier_obj->parent)) $parents[] = $modifier_obj->parent;
                    }
                    break;
                case "construct":
                    if ($modifier->link != 14){
                        $modifier_obj = $this->modx->getObject("msProduct", $modifier->slave);
                        $modifier_ids[] = $modifier->slave;
                        if ($par = $this->modx->getObject("modResource", $modifier_obj->parent)) $parents[] = $modifier_obj->parent;
                    }
                    break;
            }
            //return $modifier->toArray();
        }
        $parents = array_values(array_unique($parents));
        foreach ($parents as $parent){
            $modifier_parent = $this->modx->getObject("modResource",$parent);
            $title_action = $modifier_parent->getTVValue("title_action");
            $min_quantity = (int)$modifier_parent->getTVValue("min_quantity");
            $max_quantity = (int)$modifier_parent->getTVValue("max_quantity");
            $default_id = (int)$modifier_parent->getTVValue("default_id");
            if (!$title_action) $title_action = $modifier_parent->description;
            if (!$min_quantity) $min_quantity = 1;
            if (!$max_quantity) $max_quantity = 1;
            $arr = array(
                "title"=> $modifier_parent->pagetitle,
                "description"=> $modifier_parent->description, 
                "title_action"=> $title_action,
                "min_quantity"=> (int)$min_quantity ,
                "max_quantity"=>(int) $max_quantity ,
                "default_id"=> (int)$default_id,
            );
            $product["modificators"][$modifier_parent->id]=$arr;
            $arr = [];
        }
        //return $modifier_ids;
        //return $price_type;
        $product_modifiers = array(
            'class' => 'msProduct',
            'parents' => 2,
            'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0, 'id:IN' => $modifier_ids],
            'leftJoin' => [
              'Data' => ['class' => 'msProductData'],
            ],
            'select' => [
                'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, `msProduct`.`description`, parent',
                'Data' => '`Data`.`restourants`,`Data`.`order_types`, `Data`.`image` as img_url',
            ],
            'sortby' => 'msProduct.id',
            'sortdir' => 'ASC',
            'groupby' => 'msProduct.id',
            'return' => 'data',
            'limit' => 1000
        );
        
        if($price_type != 0) {
            $product_modifiers['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type];
            $product_modifiers['select']['Price'] = 'Price.price, Price.old_price';
            $product_modifiers['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = msProduct.id AND PriceOriginal.type ='.$price_type_original];
            $product_modifiers['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
        } else {
            $product_modifiers['select']['msProduct'] = $default['select']['msProduct'].', price, old_price, price as original_price';
        }
        
        $massEnergyLJ = array(
            'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
            'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
            'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
            'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
            'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
            'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
        );
        $massEnergySc = array(
            'Option1' => 'Option1.value as energy_size',
            'Option2' => 'Option2.value as energy_allergens',
            'Option3' => 'Option3.value as energy_value',
            'Option4' => 'Option4.value as energy_carbohydrates',
            'Option5' => 'Option5.value as energy_protein',
            'Option6' => 'Option6.value as energy_oils',
        );
        $product_modifiers['leftJoin'] = array_merge($product_modifiers['leftJoin'], $massEnergyLJ);
        $product_modifiers['select'] = array_merge($product_modifiers['select'], $massEnergySc);
        $this->pdoFetch->setConfig($product_modifiers);
        $product_modifiers = $this->pdoFetch->run();
        foreach ($product_modifiers as $key=> $product_modifier){
            $restourants = explode(",", $product_modifiers['restourants']);
            $this_product = $this->modx->getObject("msProduct", $product_modifier["id"]);
            if (in_array($_GET["id_org"], $restourants) || $this_product->getTVValue("technicalModifier")){
                unset($product_modifiers[$key]);
            } else {
                $product_modifiers[$key]["price"] = (double)$product_modifiers[$key]["price"];
                $min_quantity = (int)$this_product->getTVValue("min_quantity");
                $max_quantity = (int)$this_product->getTVValue("max_quantity");
                if (!$min_quantity) $min_quantity = 1;
                if (!$max_quantity) $max_quantity = 1;
                $product_modifiers[$key]["min_quantity"] = $min_quantity;
                $product_modifiers[$key]["max_quantity"] = $max_quantity;
                $product_modifiers[$key]["energy"] = array(
                    "energy_size" =>(float) round($product_modifier["energy_size"],1),  
                    "energy_allergens" => $product_modifier["energy_allergens"],  
                    "energy_value" => $product_modifier["energy_value"],  
                    "energy_carbohydrates" => $product_modifier["energy_carbohydrates"],
                    "energy_protein" => $product_modifier["energy_protein"],
                    "energy_oils" => $product_modifier["energy_oils"],
                );
                unset($product_modifiers[$key]["energy_size"]);
                unset($product_modifiers[$key]["energy_allergens"]);
                unset($product_modifiers[$key]["energy_value"]);
                unset($product_modifiers[$key]["energy_carbohydrates"]);
                unset($product_modifiers[$key]["energy_protein"]);
                unset($product_modifiers[$key]["energy_oils"]);
                unset($product_modifiers[$key]["restourants"]);
                unset($product_modifiers[$key]["order_types"]);
            }
        }
        foreach ($product_modifiers as $product_modifier){
            $product["modificators"][$product_modifier["parent"]]["products"][] = $product_modifier;
        }
        $product["modificators"] = array_values($product["modificators"]);
        foreach ($product["modificators"] as $key=> $modifi_cat){
            if (!$product["modificators"][$key]["products"]) unset($product["modificators"][$key]);
        }
        //return $product_modifiers;
        $product["img_small"] = ($product["img_small"])? $product["img_small"] : '/assets/template/images/content/fri.jpg';
        $product["img_url"] = ($product["img_url"])? $product["img_url"] : '/assets/template/images/content/fri.jpg';
        $sizes = '{
          "title": "Размер",
          "description": "выберите что-то там (описание)",
          "type": "size",
          "min_quantity": "1",
          "max_quantity": "1",
          "default_id": 102,
          "products": [
            {
              "id": "102",
              "img_url": "",
              "title": "Стандарт",
              "price": 0,
              "min_quantity": 0,
              "max_quantity": 0,
              "energy": {
                "energy_size": 0,
                "energy_value": 0,
                "energy_allergens": 0,
                "energy_carbohydrates": 0,
                "energy_protein": 0,
                "energy_oils": 0
              }
            },

            {
              "id": "24",
              "img_url": "",
              "title": "Мини",
              "price": -50,
              "min_quantity": 0,
              "max_quantity": 0,
              "energy": {
                "energy_size": -20,
                "energy_value": -20,
                "energy_allergens": -20,
                "energy_carbohydrates": -20,
                "energy_protein": -20,
                "energy_oils": -20
              }
            }
          ]
        }';
        $product["sizes"] = json_decode($sizes);
        $json["data"][] = $product;
        return $json;

    }
    //получить продукты
    public function getProducts(){
        $pdoFetch = $this->pdoFetch;
        $rest_id = $_GET['id_org'];
        $category_id = $_GET["category_id"];
        $search_query = $_GET["search_string"];
        $page = $_GET["page"];
        if (!$category_id){
            $category_id = $this->modx->getOption('content_parent');
        }
        if ($search_query){
            $search_results = $this->modx->runSnippet("apiSearch", ["parents" => 1561, "query" => $search_query, "returnIds" => true, "limit" => 10000]);
            $search_results = explode(",",$search_results);
        }
        if($this->modx->getOption("app_pricy_type") == "userprice"){
            if ($profile){
                $price_type = $profile->get("state");
            } else {
                $price_type = $this->modx->getOption("add_default_type_price");;
            }
            $price_type_original = $price_type;
        } else if($this->modx->getOption("app_pricy_type") == "cityprice") {
            if($this->modx->getOption("app_always_courier_price")) {
                $order_type_tv = "tvTypeDelivery";
            } else {
                switch ($_GET["order_type"]){
                    case '1': //самовывоз
                        $order_type_tv = "tvType";
                        break;
                    case '2': //курьер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '4': //менеджер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '3': //ресторан
                        $order_type_tv = "tvTypeRestourant";
                        break;
                }
            }
            
            $org = $this->modx->getObject("modResource", $_GET["id_org"]);
            if (!$org) ModxApi::HTTPStatus(409);
            $price_type = $org->getTVValue($order_type_tv);
            $price_type_original = $org->getTVValue("tvTypeDelivery");
        } else {
            $price_type = $price_type_original = 0;
        }
        function mergeRating($rows){
            global $modx;
            $pdoFetch = new pdoFetch($modx);
            $master_ids = array_column($rows, 'id');
            $ratings = array(
                'class' => 'Rating',
                'where' => ["id_resource:IN" => $master_ids, "active" => 1],
                'leftJoin' => [
                    'User' => ['class' => 'modUserProfile', 'on' => 'User.internalKey = Rating.id_user'],
                ],
                'select' => [
                   'Rating' => '`Rating`.`id_rating`,
                       `Rating`.`id_resource`,
                       `Rating`.`id_user`,
                       `Rating`.`comment_rating`,
                       `Rating`.`rating_value`,
                       `Rating`.`date_of_creation`,
                       `Rating`.`date_of_editing`',
                    'User' => '`User`.`fullname`',
                ],
                'sortdir' => "ASC",
                'sortby' => "id_rating",
                'return' => 'data',
                'limit' => 1000
            );
            $pdoFetch->setConfig($ratings);
            $ratings = $pdoFetch->run();
            $rows_array = array_combine(array_column($rows, 'id'), $rows);
            foreach ($ratings as $rating){
                $rows_array[$rating["id_resource"]]["ratings"][] = $rating;
            }
            return $rows_array;
        }
        /*$pdoFetch->setConfig([
            'class' => 'msCategory',
            'select' => ['msCategory' => 'id'],
            'parents' => 1561,
            'return' => 'data',
            'where' => ['class_key' => 'msCategory', 'published' =>1, 'deleted' => 0],
            'limit' => 100000
            ]);
        $parents = $pdoFetch->run();
        $parents = array_column($parents, 'id');*/

        $default = array(
            'class' => 'msProduct',
            'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
            'leftJoin' => [
              'Data' => ['class' => 'msProductData'],
              'allRating' => ['class' => 'msProductOption', 'on' => 'allRating.product_id = msProduct.id AND allRating.key = "item_rating"'],
              'Option19' => ['class' => 'msProductOption', 'on' => 'Option19.product_id = msProduct.id AND Option19.key = "card_type"'],
            ],
            'select' => [
                'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`, `msProduct`.`content`,restourants,order_types',
                'Data' => '`Data`.`restourants`,`Data`.`order_types`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
                'allRating' => 'allRating.value as item_rating',
                'Option19' => 'Option19.value as card_type',
            ],
            'sortby' => 'msProduct.id',
            'sortdir' => 'ASC',
            'groupby' => 'msProduct.id',
            'return' => 'data',
            'limit' => 1000,
            'offset' => ($page-1)*$limit
        );
        if ($this->modx->getOption("get_categ_by_orgs")){
            $default['parents'] = $org->getTVValue("category_parents");
        } else {
            $default['parents'] = $category_id;
        }
        if($price_type != 0) {
            $default['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type];
            $default['select']['Price'] = 'Price.price, Price.old_price';
            $default['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = msProduct.id AND PriceOriginal.type ='.$price_type_original];
            $default['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
        } else {
            $default['select']['msProduct'] = $default['select']['msProduct'].', price, old_price, price as original_price';
        }
        
        /* ================== Подгружаем дополнительные параметры ==================== */
        // подгружаем системный параметр energy, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_energy")) {
            $massEnergyLJ = array(
                'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
                'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
                'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
                'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
                'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
                'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
                'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "times_bought"'],
            );
            $massEnergySc = array(
                'Option1' => 'Option1.value as energy_size',
                'Option2' => 'Option2.value as energy_allergens',
                'Option3' => 'Option3.value as energy_value',
                'Option4' => 'Option4.value as energy_carbohydrates',
                'Option5' => 'Option5.value as energy_protein',
                'Option6' => 'Option6.value as energy_oils',
                'Option7' => 'Option7.value as times_bought',
            );
            $default['leftJoin'] = array_merge($default['leftJoin'], $massEnergyLJ);
            $default['select'] = array_merge($default['select'], $massEnergySc);
        }
        
        // проверяем настройку на вывод списка ссылок для товара (они должны быть добавлены к товару заранее)
        if ($this->modx->getOption("app_links_list")) {
            $massEnergyLJ = array(
                'Option8' => ['class' => 'msProductOption', 'on' => 'Option8.product_id = msProduct.id AND Option8.key = "link_audio"'],
                'Option9' => ['class' => 'msProductOption', 'on' => 'Option9.product_id = msProduct.id AND Option9.key = "link_video"'],
                'Option10' => ['class' => 'msProductOption', 'on' => 'Option10.product_id = msProduct.id AND Option10.key = "link_phone"'],
                'Option11' => ['class' => 'msProductOption', 'on' => 'Option11.product_id = msProduct.id AND Option11.key = "link_coordinates"'],
                'Option12' => ['class' => 'msProductOption', 'on' => 'Option12.product_id = msProduct.id AND Option12.key = "link_site"'],
                'Option13' => ['class' => 'msProductOption', 'on' => 'Option13.product_id = msProduct.id AND Option13.key = "link_vk"'],
                'Option14' => ['class' => 'msProductOption', 'on' => 'Option14.product_id = msProduct.id AND Option14.key = "link_fb"'],
                'Option15' => ['class' => 'msProductOption', 'on' => 'Option15.product_id = msProduct.id AND Option15.key = "link_instagram"'],
                'Option16' => ['class' => 'msProductOption', 'on' => 'Option16.product_id = msProduct.id AND Option16.key = "link_odnoklassniki"'],
                'Option17' => ['class' => 'msProductOption', 'on' => 'Option17.product_id = msProduct.id AND Option17.key = "add_address"'],
                'Option18' => ['class' => 'msProductOption', 'on' => 'Option18.product_id = msProduct.id AND Option18.key = "add_time_work"'],
            );
            $massEnergySc = array(
                'Option8' => 'Option8.value as link_audio',
                'Option9' => 'Option9.value as link_video',
                'Option10' => 'Option10.value as link_phone',
                'Option11' => 'Option11.value as link_coordinates',
                'Option12' => 'Option12.value as link_site',
                'Option13' => 'Option13.value as link_vk',
                'Option14' => 'Option14.value as link_fb',
                'Option15' => 'Option15.value as link_instagram',
                'Option16' => 'Option16.value as link_odnoklassniki',
                'Option17' => 'Option17.value as add_address',
                'Option18' => 'Option18.value as add_time_work',
            );
            $default['leftJoin'] = array_merge($default['leftJoin'], $massEnergyLJ);
            $default['select'] = array_merge($default['select'], $massEnergySc);
        }
        
        if ($search_results){
            $default["where"]["id:IN"] = $search_results;
        }
        
        //return $default;
        $pdoFetch->setConfig($default);
        $rows = $pdoFetch->run();
        
        //Собираем все id товаров в отдельный массив
        $master_ids = array_column($rows, 'id');
        $msCategoryMember = array(
            'class' => 'msCategoryMember',
            'sortdir' => "desc",
            'return' => 'data',
            'limit' => 10000,
            'where' => ['product_id:IN' => $master_ids]
            
        );
        $pdoFetch->setConfig($msCategoryMember);
        $msCategoryMember = $pdoFetch->run();
        //return $rows;
        // подгружаем системный параметр Добавки, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_addpar")) {
            $adds = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 3, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original]
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $adds['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $adds['select']['Price'] = 'Price.price, Price.old_price';
                $adds['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $adds['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $adds['select']['Product'] = $adds['select']['Product'].', price, old_price, price as original_price';
            }
            
            $pdoFetch->setConfig($adds);
            $dobavki = $pdoFetch->run();
        } else {
            $dobavki = array();
        }
        
        // подгружаем системный параметр Types, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_type")) {
            $types = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 9, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original]
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`,`Data`.`price`,`Data`.`image` as img_url, pagetitle as title',
                //'Price' => 'Price.price as price_type, Price.old_price  as old_price_type',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $types['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $types['select']['Price'] = 'Price.price, Price.old_price';
                $types['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $types['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $types['select']['Product'] = $types['select']['Product'].', price, old_price, price as original_price';
            }
            
            $pdoFetch->setConfig($types);
            $types = $pdoFetch->run();
        } else {
            $types = array();
        }
        
        // подгружаем системный параметр Ингредиенты, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_ingredients")) {
            $ingredients = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 8, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original],
                    //'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Data`.`image` as img_url, `Product`.`pagetitle` as title',
                //'Data' => '`Data`.`price`',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $ingredients['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $ingredients['select']['Price'] = 'Price.price, Price.old_price';
                $ingredients['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $ingredients['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $ingredients['leftJoin']['Data'] = ['class' => 'msProductData', 'on' => 'Data.id = Product.id'];
                $ingredients['select']['Data'] = '`Data`.`price`,`Data`.`old_price`,`Data`.`price` as original_price';
            }
            
            $pdoFetch->setConfig($ingredients);
            $ingredients = $pdoFetch->run();
        } else {
            $ingredients = array();
        }
        // подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_analogue")) {
            $analogs = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 10, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original],
                    //'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
                //'Data' => '`Data`.`price`',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $analogs['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $analogs['select']['Price'] = 'Price.price, Price.old_price';
                $analogs['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $analogs['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $analogs['leftJoin']['Data'] = ['class' => 'msProductData', 'on' => 'Data.id = Product.id'];
                $analogs['select']['Data'] = '`Data`.`price`,`Data`.`old_price`,`Data`.`price` as original_price';
            }
            
            $pdoFetch->setConfig($analogs);
            $analogs = $pdoFetch->run();
        } else {
            $analogs = array();
        }
        // подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_related_products")) {
            $relatedProducts = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 15, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
                'Data' => '`Data`.`price`',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $relatedProducts['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $relatedProducts['select']['Price'] = 'Price.price, Price.old_price';
                $relatedProducts['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $relatedProducts['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $relatedProducts['leftJoin']['Data'] = ['class' => 'msProductData', 'on' => 'Data.id = Product.id'];
                $relatedProducts['select']['Data'] = '`Data`.`price`,`Data`.`old_price`,`Data`.`price` as original_price';
            }
            
            $pdoFetch->setConfig($relatedProducts);
            $relatedProducts = $pdoFetch->run();
            //$this->modx->log(1,"С ним покупают: $price_type - $price_type_origina".Print_r($relatedProducts,1));
        } else {
            $relatedProducts = array();
        }
        
        // подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_sizes")) {
            $sizes = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 16, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
                    
                    'enSize' => ['class' => 'msProductOption', 'on' => 'enSize.product_id = Product.id AND enSize.key = "energy_size"'],
                    'enAllergens' => ['class' => 'msProductOption', 'on' => 'enAllergens.product_id = Product.id AND enAllergens.key = "energy_allergens"'],
                    'enValue' => ['class' => 'msProductOption', 'on' => 'enValue.product_id = Product.id AND enValue.key = "energy_value"'],
                    'enCarbohydrates' => ['class' => 'msProductOption', 'on' => 'enCarbohydrates.product_id = Product.id AND enCarbohydrates.key = "energy_carbohydrates"'],
                    'enProtein' => ['class' => 'msProductOption', 'on' => 'enProtein.product_id = Product.id AND enProtein.key = "energy_protein"'],
                    'enOils' => ['class' => 'msProductOption', 'on' => 'enOils.product_id = Product.id AND enOils.key = "energy_oils"'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Product`.`pagetitle` as title, price, old_price',
                'Data' => '`Data`.`price`',
                
                'enSize' => 'enSize.value as energy_size',
                'enAllergens' => 'enAllergens.value as energy_allergens',
                'enValue' => 'enValue.value as energy_value',
                'enCarbohydrates' => 'enCarbohydrates.value as energy_carbohydrates',
                'enProtein' => 'enProtein.value as energy_protein',
                'enOils' => 'enOils.value as energy_oils',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            $pdoFetch->setConfig($sizes);
            $sizes = $pdoFetch->run();
            //$this->modx->log(1,"С ним покупают: $price_type - $price_type_origina".Print_r($sizes,1));
        } else {
            $sizes = array();
        }
        
        //return $rows;
        /* ================== /Подгружаем дополнительные параметры ==================== */
        // формируем список стикеров для ресурса
        $stickers = array(
            'class' => 'msProductLink',
            'where' => ['Product.class_key' => 'msProduct', 'link' => 12, 'master:IN' => $master_ids],
            'leftJoin' => [
                'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                'ColorText' => ['class' => 'msProductOption', 'on' => 'ColorText.product_id = Product.id AND ColorText.key = "add_color_text"'],
                'ColorBg' => ['class' => 'msProductOption', 'on' => 'ColorBg.product_id = Product.id AND ColorBg.key = "add_color_bg"'],
                'idOrg' => ['class' => 'msProductOption', 'on' => 'idOrg.product_id = Product.id AND idOrg.key = "add_id_org"'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`,pagetitle as title',
                'ColorText' => 'ColorText.value as color_text',
                'ColorBg' => 'ColorBg.value as color_bg',
                'idOrg' => 'idOrg.value as id_org',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
        );
        $pdoFetch->setConfig($stickers);
        $stickers = $pdoFetch->run();
        foreach ($stickers as $key=> $sticker){
            $stickers[$key]["master"] = (int)$stickers[$key]["master"];
            $stickers[$key]["id"] = (int)$stickers[$key]["id"];
            $stickers[$key]["id_org"] = (int)$stickers[$key]["id_org"];
        }
        //return $dobavki;
        //return $stickers;
        $sticerList = array();
        $json['data'] = array();
        $rows = array_combine(array_column($rows, 'id'), $rows);
        foreach ($msCategoryMember as $parent){
            $rows[$parent["product_id"]]['parents'][] = $parent['category_id'];
        }
        foreach ($analogs as $analog){
            $analog["price"] = ($analog["price"])? (int)$analog["price"] : 0;
            $analog["old_price"] = ($analog["old_price"])? (int)$analog["old_price"] : 0;
            $analog["original_price"] = ($analog["original_price"])? (int)$analog["original_price"] : 0;
            
            $analog["master"] = (int)$analog["master"];
            $analog["id"] = (int)$analog["id"];
            $rows[$analog['master']]['analog'][] = $analog;
        }
        foreach ($relatedProducts as $relatedProduct){
            $relatedProduct["price"] = ($relatedProduct["price"])? (int)$relatedProduct["price"] : 0;
            $relatedProduct["old_price"] = ($relatedProduct["old_price"])? (int)$relatedProduct["old_price"] : 0;
            $relatedProduct["original_price"] = ($relatedProduct["original_price"])? (int)$relatedProduct["original_price"] : 0;
            
            $relatedProduct["master"] = (int)$relatedProduct["master"];
            $relatedProduct["id"] = (int)$relatedProduct["id"];
            $rows[$relatedProduct['master']]['relatedProduct'][] = $relatedProduct;
        }
        foreach ($sizes as $size){
            $size["master"] = (int)$size["master"];
            $size["id"] = (int)$size["id"];
            $size["price"] = ($size["price"])? (int)$size["price"] : 0;
            $size["old_price"] = ($size["old_price"])? (int)$size["old_price"] : 0;
            $size["original_price"] = ($size["original_price"])? (int)$size["original_price"] : 0;
            
            $size["energy"]["energy_size"] = ($size["energy_size"])? $size["energy_size"] : 0;
            $size["energy"]["energy_allergens"] = ($size["energy_allergens"])? $size["energy_allergens"] : 0;
            $size["energy"]["energy_value"] = ($size["energy_value"])? $size["energy_value"] : 0;
            $size["energy"]["energy_carbohydrates"] = ($size["energy_carbohydrates"])? $size["energy_carbohydrates"] : 0;
            $size["energy"]["energy_protein"] = ($size["energy_protein"])? $size["energy_protein"] : 0;
            $size["energy"]["energy_oils"] = ($size["energy_oils"])? $size["energy_oils"] : 0;
            
            unset($size["energy_size"],$size["energy_allergens"],$size["energy_value"],$size["energy_carbohydrates"],$size["energy_protein"],$size["energy_oils"]);
            
            //$rows[$size['master']]['size'][] = $size;
            //if(!array_key_exists('default_id',$rows[$size['master']]['size'])) $rows[$size['master']]['size']['default_id'] = $size["id"];
            //if($size["price"] == 0) $rows[$size['master']]['size']['default_id'] = $size["id"];
            /*$rows[$size['master']]['size']['ids'][] = $size["id"];
            $rows[$size['master']]['size']['data'][$size["id"]] = $size;*/
            $rows[$size['master']]['size']['data'][] = $size;
        }
        foreach ($ingredients as $ingredient){
            $ingredient["master"] = (int)$ingredient["master"];
            $ingredient["id"] = (int)$ingredient["id"];
            $ingredient["img_url"] = ($ingredient["img_url"])? $ingredient["img_url"] : "";
            $ingredient["price"] = ($ingredient["price"])? (int)$ingredient["price"] : 0;
            $ingredient["old_price"] = ($ingredient["old_price"])? (int)$ingredient["old_price"] : 0;
            $ingredient["original_price"] = ($ingredient["original_price"])? (int)$ingredient["original_price"] : 0;
            $rows[$ingredient['master']]['ing'][] = $ingredient;
        }
        foreach ($dobavki as $dobavka){
            $dobavka["master"] = (int)$dobavka["master"];
            $dobavka["id"] = (int)$dobavka["id"];
            $dobavka["img_url"] = ($dobavka["img_url"])? $dobavka["img_url"] : "";
            $dobavka["price"] = ($dobavka["price"])? (int)$dobavka["price"] : 0;
            $dobavka["old_price"] = ($dobavka["old_price"])? (int)$dobavka["old_price"] : 0;
            $dobavka["original_price"] = ($dobavka["original_price"])? (int)$dobavka["original_price"] : 0;
            $rows[$dobavka['master']]['adds'][] = $dobavka;
        }
        $minPrice = 0;
        foreach ($types as $type){
            $type["master"] = (int)$type["master"];
            $type["id"] = (int)$type["id"];
            $type["img_url"] = ($type["img_url"])? $type["img_url"] : "";
            $type["price"] = ($type["price_type"])? (int)$type["price_type"] : (int)$type["price"];
            $type["old_price"] = ($type["old_price_type"])? (int)$type["old_price_type"] : (int)$type["old_price"];
            $type["original_price"] = ($type["original_price"])? (int)$type["original_price"] : 0;
            unset($type["price_type"],$type["old_price_type"]);
            //$rows[$type['master']]['types'][] = $type;
            
            if(!array_key_exists('default_index',$rows[$type['master']]['types'])) {
                $rows[$type['master']]['types']['default_index'] = $type["id"];
                $minPrice = $type["price"];
            }
            if($type["price"] < $minPrice) {
                $rows[$type['master']]['types']['default_index'] = $type["id"];
                $minPrice = $type["price"];
            }
            /*$rows[$type['master']]['types']['ids'][] = $type["id"];
            $rows[$type['master']]['types']['data'][$type["id"]] = $type;*/
            $rows[$type['master']]['types']['data'][] = $type;
        }
        foreach ($stickers as $sticker){
            $rows[$sticker['master']]['stickers'][] = $sticker;
        }
        $rows = mergeRating($rows);
        foreach ($rows as $key => $row){
            $order_types = explode(",", $row['order_types']);
            if (in_array($_GET['order_type'],$order_types)){
                $restourants = explode(",", $row['restourants']);
                if (in_array($rest_id, $restourants)) {
                    
                } else {
                    $row['parents'][] = $row['parent'];
                    
                    $json['data'][$key]['category_ids'] = $row['parents'];
                    foreach ($json['data'][$key]['category_ids'] as $sub_key => $sub_parent){
                        $json['data'][$key]['category_ids'][$sub_key] = (int)$sub_parent;
                    }
                    $json['data'][$key]['id'] = (int)$row['id'];
                    $json['data'][$key]['title'] = $row['title'];
                    $json['data'][$key]['parent'] = (int)$row['parent'];
                    $json['data'][$key]['article'] = (int)$row['article'];
                    $json['data'][$key]['allRating'] = ($row['item_rating'])? $row['item_rating'] : 0;
                    $json['data'][$key]['rating'] = (array_key_exists("ratings", $row))? $row['ratings'] : array(); // если в массиве присутствует параметр rating, то добавляем его содержимое
                    $json['data'][$key]['price'] = (double)$row['price'];
                    $json['data'][$key]['old_price'] = (double)$row['old_price'];
                    $json['data'][$key]['original_price'] = (double)$row['original_price'];
                    $json['data'][$key]['desc'] = $row['description'];
                    $json['data'][$key]['content'] = $row['content'];
                    $json['data'][$key]['restourants'] = (int)$row['restourants'];
                    $json['data'][$key]['order_types'] = (int)$row['order_types'];
                    $image_small = ($row['img_big'])? $this->modx->runSnippet('pthumb', [
                                            'input' => substr($row['img_big'], 1),
                                            'options' => '&h=300&w=300&q=100&bg=FFFFFF',
                                        ]) : '/assets/template/images/content/fri.jpg';
                    $json['data'][$key]['img_small'] = $image_small;
                    $json['data'][$key]['img_big'] = $image_small;
                    $json['data'][$key]['bonus_add'] = ($bonus_persent)? floor($row['price'] / 100 * $bonus_persent) : 0; // если есть параметр бонуса, то считаем его
                    $json['data'][$key]['times_bought'] = (int)$row['times_bought'];
                    $json['data'][$key]['remains_count'] = 25;
                    $json['data'][$key]['remains_text'] = "dfuvidsofov";
                    $json['data'][$key]['barcode'] = "111111";
                    $json['data'][$key]['pieces_per_package'] = 25;
                    $json['data'][$key]['stickers'] = (array_key_exists("stickers", $row))? $row["stickers"] : array(); // если в массиве есть параметр stickers, то получаем его содержимое
                    //$json['data'][$key]['sizes'] = ($sizes)? json_decode($sizes) : array();
                    $row['card_type'] = ($row['card_type'])? $row['card_type'] : "base";
                    $json['data'][$key]['card_type'] = $row['card_type'];
                    
                    // Если параметр Energy равен 0, то передаём дальше "заглушку" под его значения
                    if (!$this->modx->getOption("app_parameters_energy")) {
                        $row['energy_size'] = "0";
                        $row['energy_value'] = $row['energy_carbohydrates'] = $row['energy_protein'] = $row['energy_oils'] = 0;
                        $row['energy_allergens'] = "-";
                    }
                    $json['data'][$key]['energy'] = array(
                        "energy_size" => $row['energy_size'],
                        "energy_value" => round($row['energy_value']),    
                        "energy_allergens" => $row['energy_allergens'],
                        "energy_carbohydrates" => round($row['energy_carbohydrates']),
                        "energy_protein" => round($row['energy_protein']),
                        "energy_oils" => round($row['energy_oils']),
                    );
                    // проверяем настройку на вывод списка ссылок для товара (они должны быть добавлены к товару заранее)
                    if ($this->modx->getOption("app_links_list")) {
                        
                        $coordMass = explode(",",str_replace(" ","",$row['link_coordinates']));
                        $json['data'][$key]["GPS"] = array(
                            'latitude' => (float)$coordMass[0],
                            'longitude' => (float)$coordMass[1],
                        );
                        
                        // Добавляем в ответный массив перечень ссылок от объекта
                        $json['data'][$key]['links_list'] = array(
                            "link_phone" => $row['link_phone'],
                            "link_site" => $row['link_site'],
                            "link_vk" => $row['link_vk'],
                            "link_fb" => $row['link_fb'],
                            "link_instagram" => $row['link_instagram'],
                            "link_odnoklassniki" => $row['link_odnoklassniki'],
                        );
                        
                        // получаем картинку превью c ютуба
                        $linkCode = explode("/",$row['link_video']);
                        $linkCode = $linkCode[count($linkCode) - 1];
                        if(stristr($linkCode, '?v=') === FALSE) {
                        } else {
                            $linkCode = explode("?",explode("v=",$linkCode)[1])[0];
                        }
                        
                        $json['data'][$key]['media'] = array(
                            "link_audio" => $row['link_audio'],
                            "link_video" => $row['link_video'],
                            "link_video_prev" => "https://img.youtube.com/vi/{$linkCode}/hqdefault.jpg",
                        );
                        $json['data'][$key]['where_is'] = array(
                            "add_address" => $row['add_address'],
                            "add_time_work" => $row['add_time_work'],
                        );
                    }
                    if (array_key_exists("adds", $row) && sizeof($row['adds'])){
                        $json['data'][$key]['additives'] = $row['adds'];
                    } else {
                        $json['data'][$key]['additives'] = [];
                    }
                    if (array_key_exists("ing", $row) && sizeof($row['ing'])){
                        $json['data'][$key]['ingredients'] = $row['ing'];    
                    } else {
                        $json['data'][$key]['ingredients'] = [];
                    }
                    if (array_key_exists("types", $row) && sizeof($row['types'])){
                        $minPrice = 0;
                        foreach($row['types']['data'] as $keyType => $type){
                            if(!array_key_exists('default_index',$row['types'])) {
                                $row['types']['default_index'] = 0;
                                $minPrice = $type["price"];
                            }
                            if((int)$type["price"] < (int)$minPrice) {
                                $row['types']['default_index'] = $keyType;
                                $minPrice = $type["price"];
                            }
                        }
                        $json['data'][$key]['types'] = $row['types'];
                    } else {
                        $json['data'][$key]['types'] = [];
                    }
                    if (array_key_exists("analog", $row) && sizeof($row['analog'])){
                        $json['data'][$key]['analogs'] = $row['analog'];
                    } else {
                        $json['data'][$key]['analogs'] = [];
                    }
                    if (array_key_exists("relatedProduct", $row) && sizeof($row['relatedProduct'])){
                        $json['data'][$key]['relatedProducts'] = $row['relatedProduct'];
                    } else {
                        $json['data'][$key]['relatedProducts'] = [];
                    }
                    if (array_key_exists("size", $row) && sizeof($row['size'])){
                        $minPrice = 0;
                        foreach($row['size']['data'] as $keySize => $size){
                            if(!array_key_exists('default_index',$row['size'])) {
                                $row['size']['default_index'] = 0;
                                $minPrice = $size["price"];
                            }
                            if((int)$size["price"] < (int)$minPrice) {
                                $row['size']['default_index'] = $keySize;
                                $minPrice = $size["price"];
                            }
                        }
                        $json['data'][$key]['sizes'] = $row['size'];
                        $json['data'][$key]['max_price'] = $json['data'][$key]['price']-min(array_column($json['data'][$key]['sizes']['data'], 'price'));
                    } else {
                        $json['data'][$key]['sizes'] = [];
                        $json['data'][$key]['max_price'] = $json['data'][$key]['price'];
                    }
                    
                }
            }   
        }
        $sql = "SELECT pagetitle FROM modx_site_content WHERE id = ".$category_id;
        $result = $this->modx->query($sql);
        $json["category_title"] = $result->fetch(PDO::FETCH_ASSOC)['pagetitle'];
        $json["count_items"] = count($json["data"]);
        $json["data"] = array_values($json["data"]);
        return $json;
    }
    public function getHomepage(){
        $promotions = new Promotions($this->modx, $request, $headers);
        //return $this->getProducts();
        $json["promotions"] = $promotions->getPromotions();
        $json["products"] = $this->getProducts();
        
        //// Получаем список категории (4 штуки из основного каталога)
        $query = $this->modx->newQuery('modResource');
        $query->sortby('menuindex', 'ASC');
        $query->where(array(
            'template' => 3,
            'parent' => 1561,
            'published' => 1,
            'deleted' => 0
        ));
        $query->limit(4);
        $categoryes = $this->modx->getCollection("modResource",$query);
        
        $data = array();
        $num = 0;
        foreach($categoryes as $key => $category) {
            $data[$num]['id'] = $category->get("id");
            $data[$num]['title'] = $category->get("pagetitle");
            $data[$num]['parent'] = $category->get("parent");
            $data[$num]['description'] = $category->get("description");
            $data[$num]['image'] = $category->getTvValue("image");
            $num++;
        }
        $json["catalog_first"] = $data;
        $json["catalog_second"][] = array(
            "id" => "2334",    
            "description" => "Описание",    
            "parent" => "1561",    
            "title" => "Суши и роллы",    
            "image" => "/2565/2aedf6df0f8794e554e6418befbbd90a8ba8fe4e.jpg",    
        );
        
        return $json;
    }
    
    // получение списка ID ресурсов по тэгам
    public function getResourceByTags(){
        // получаем переданную json строку с данными
        $data = $_GET['tags_list'];//$this->modx->request['tags_list'];
        // создаём переменную перечня id токенов, для использования её в запросе SQL
        $tagList = array();
        // перебераем полученные значения с данными
        foreach(explode(",",$data) as $key => $tagElement){
            // формируем список значений id токенов через запятую
            $tagList[] = $tagElement;
        }
        
        // формируем пустой массив для наполнения его уникальными ID полученных товаров 
        $listIdEltments = array();
        
        foreach ($tagList as $tagId) {
            $list = $this->modx->getCollection("msProductLink",array("link" => 11, "slave" => $tagId));
            // перебераем полученные значения
            foreach ($list as $key => $element) {
                // проверяем, существует ли найденный ID в массиве уникальных значений. Если нет, то добавляем его в конец массива
                if (!in_array($element->master,$listIdEltments)) array_push($listIdEltments,$element->master);
            }
        }
        
        $json["result"] = $listIdEltments;

        // возвращаем результат
        return $json;
    }
    
    //получить продукты - оптимизация
    public function getCatalog(){
        $pdoFetch = $this->pdoFetch;
        $rest_id = $_GET['id_org'];
        $category_id = $_GET["category_id"];
        $search_query = $_GET["search_string"];
        $page = $_GET["page"];
        if (!$category_id){
            $category_id = $this->modx->getOption('content_parent');
        }
        if ($search_query){
            $search_results = $this->modx->runSnippet("apiSearch", ["parents" => $category_id, "query" => $search_query, "returnIds" => true, "limit" => 10000]);
            $search_results = explode(",",$search_results);
        }
        if($this->modx->getOption("app_pricy_type") == "userprice"){
            if ($profile){
                $price_type = $profile->get("state");
            } else {
                $price_type = $this->modx->getOption("add_default_type_price");;
            }
            $price_type_original = $price_type;
        } else if($this->modx->getOption("app_pricy_type") == "cityprice") {
            if($this->modx->getOption("app_always_courier_price")) {
                $order_type_tv = "tvTypeDelivery";
            } else {
                switch ($_GET["order_type"]){
                    case '1': //самовывоз
                        $order_type_tv = "tvType";
                        break;
                    case '2': //курьер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '4': //менеджер
                        $order_type_tv = "tvTypeDelivery";
                        break;
                    case '3': //ресторан
                        $order_type_tv = "tvTypeRestourant";
                        break;
                }
            }
            
            $org = $this->modx->getObject("modResource", $_GET["id_org"]);
            if (!$org) ModxApi::HTTPStatus(409);
            $price_type = $org->getTVValue($order_type_tv);
            $price_type_original = $org->getTVValue("tvTypeDelivery");
        } else {
            $price_type = $price_type_original = 0;
        }
        function mergeRating($rows){
            global $modx;
            $pdoFetch = new pdoFetch($modx);
            $master_ids = array_column($rows, 'id');
            $ratings = array(
                'class' => 'Rating',
                'where' => ["id_resource:IN" => $master_ids, "active" => 1],
                'leftJoin' => [
                    'User' => ['class' => 'modUserProfile', 'on' => 'User.internalKey = Rating.id_user'],
                ],
                'select' => [
                   'Rating' => '`Rating`.`id_rating`,
                       `Rating`.`id_resource`,
                       `Rating`.`id_user`,
                       `Rating`.`comment_rating`,
                       `Rating`.`rating_value`,
                       `Rating`.`date_of_creation`,
                       `Rating`.`date_of_editing`',
                    'User' => '`User`.`fullname`',
                ],
                'sortdir' => "ASC",
                'sortby' => "id_rating",
                'return' => 'data',
                'limit' => 1000
            );
            $pdoFetch->setConfig($ratings);
            $ratings = $pdoFetch->run();
            $rows_array = array_combine(array_column($rows, 'id'), $rows);
            foreach ($ratings as $rating){
                $rows_array[$rating["id_resource"]]["ratings"][] = $rating;
            }
            return $rows_array;
        }
        /*$pdoFetch->setConfig([
            'class' => 'msCategory',
            'select' => ['msCategory' => 'id'],
            'parents' => 1561,
            'return' => 'data',
            'where' => ['class_key' => 'msCategory', 'published' =>1, 'deleted' => 0],
            'limit' => 100000
            ]);
        $parents = $pdoFetch->run();
        $parents = array_column($parents, 'id');*/

        $default = array(
            'class' => 'msProduct',
            'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
            'leftJoin' => [
              'Data' => ['class' => 'msProductData'],
              'allRating' => ['class' => 'msProductOption', 'on' => 'allRating.product_id = msProduct.id AND allRating.key = "item_rating"'],
              'Option19' => ['class' => 'msProductOption', 'on' => 'Option19.product_id = msProduct.id AND Option19.key = "card_type"'],
            ],
            'select' => [
                'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`, `msProduct`.`content`,restourants,order_types',
                'Data' => '`Data`.`restourants`,`Data`.`order_types`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
                'allRating' => 'allRating.value as item_rating',
                'Option19' => 'Option19.value as card_type',
            ],
            'sortby' => 'msProduct.id',
            'sortdir' => 'ASC',
            'groupby' => 'msProduct.id',
            'return' => 'data',
            'limit' => 1000,
            'offset' => ($page-1)*$limit
        );
        if ($this->modx->getOption("get_categ_by_orgs")){
            $default['parents'] = $org->getTVValue("category_parents");
        } else {
            $default['parents'] = $category_id;
        }
        if($price_type != 0) {
            $default['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type];
            $default['select']['Price'] = 'Price.price, Price.old_price';
            $default['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = msProduct.id AND PriceOriginal.type ='.$price_type_original];
            $default['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
        } else {
            $default['select']['msProduct'] = $default['select']['msProduct'].', price, old_price, price as original_price';
        }
        
        /* ================== Подгружаем дополнительные параметры ==================== */
        // подгружаем системный параметр energy, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_energy")) {
            $massEnergyLJ = array(
                'Option1' => ['class' => 'msProductOption', 'on' => 'Option1.product_id = msProduct.id AND Option1.key = "energy_size"'],
                'Option2' => ['class' => 'msProductOption', 'on' => 'Option2.product_id = msProduct.id AND Option2.key = "energy_allergens"'],
                'Option3' => ['class' => 'msProductOption', 'on' => 'Option3.product_id = msProduct.id AND Option3.key = "energy_value"'],
                'Option4' => ['class' => 'msProductOption', 'on' => 'Option4.product_id = msProduct.id AND Option4.key = "energy_carbohydrates"'],
                'Option5' => ['class' => 'msProductOption', 'on' => 'Option5.product_id = msProduct.id AND Option5.key = "energy_protein"'],
                'Option6' => ['class' => 'msProductOption', 'on' => 'Option6.product_id = msProduct.id AND Option6.key = "energy_oils"'],
                'Option7' => ['class' => 'msProductOption', 'on' => 'Option7.product_id = msProduct.id AND Option7.key = "times_bought"'],
            );
            $massEnergySc = array(
                'Option1' => 'Option1.value as energy_size',
                'Option2' => 'Option2.value as energy_allergens',
                'Option3' => 'Option3.value as energy_value',
                'Option4' => 'Option4.value as energy_carbohydrates',
                'Option5' => 'Option5.value as energy_protein',
                'Option6' => 'Option6.value as energy_oils',
                'Option7' => 'Option7.value as times_bought',
            );
            $default['leftJoin'] = array_merge($default['leftJoin'], $massEnergyLJ);
            $default['select'] = array_merge($default['select'], $massEnergySc);
        }
        
        // проверяем настройку на вывод списка ссылок для товара (они должны быть добавлены к товару заранее)
        if ($this->modx->getOption("app_links_list")) {
            $massEnergyLJ = array(
                'Option8' => ['class' => 'msProductOption', 'on' => 'Option8.product_id = msProduct.id AND Option8.key = "link_audio"'],
                'Option9' => ['class' => 'msProductOption', 'on' => 'Option9.product_id = msProduct.id AND Option9.key = "link_video"'],
                'Option10' => ['class' => 'msProductOption', 'on' => 'Option10.product_id = msProduct.id AND Option10.key = "link_phone"'],
                'Option11' => ['class' => 'msProductOption', 'on' => 'Option11.product_id = msProduct.id AND Option11.key = "link_coordinates"'],
                'Option12' => ['class' => 'msProductOption', 'on' => 'Option12.product_id = msProduct.id AND Option12.key = "link_site"'],
                'Option13' => ['class' => 'msProductOption', 'on' => 'Option13.product_id = msProduct.id AND Option13.key = "link_vk"'],
                'Option14' => ['class' => 'msProductOption', 'on' => 'Option14.product_id = msProduct.id AND Option14.key = "link_fb"'],
                'Option15' => ['class' => 'msProductOption', 'on' => 'Option15.product_id = msProduct.id AND Option15.key = "link_instagram"'],
                'Option16' => ['class' => 'msProductOption', 'on' => 'Option16.product_id = msProduct.id AND Option16.key = "link_odnoklassniki"'],
                'Option17' => ['class' => 'msProductOption', 'on' => 'Option17.product_id = msProduct.id AND Option17.key = "add_address"'],
                'Option18' => ['class' => 'msProductOption', 'on' => 'Option18.product_id = msProduct.id AND Option18.key = "add_time_work"'],
            );
            $massEnergySc = array(
                'Option8' => 'Option8.value as link_audio',
                'Option9' => 'Option9.value as link_video',
                'Option10' => 'Option10.value as link_phone',
                'Option11' => 'Option11.value as link_coordinates',
                'Option12' => 'Option12.value as link_site',
                'Option13' => 'Option13.value as link_vk',
                'Option14' => 'Option14.value as link_fb',
                'Option15' => 'Option15.value as link_instagram',
                'Option16' => 'Option16.value as link_odnoklassniki',
                'Option17' => 'Option17.value as add_address',
                'Option18' => 'Option18.value as add_time_work',
            );
            $default['leftJoin'] = array_merge($default['leftJoin'], $massEnergyLJ);
            $default['select'] = array_merge($default['select'], $massEnergySc);
        }
        
        if ($search_results){
            $default["where"]["id:IN"] = $search_results;
        }
        
        //return $default;
        $pdoFetch->setConfig($default);
        $rows = $pdoFetch->run();
        
        $rowsNEW = array();
        foreach($rows as $row) {
            $rowsNEW[$row["id"]] = $row;
        }
        
        $rows = $rowsNEW;
        
        //Собираем все id товаров в отдельный массив
        $master_ids = array_column($rows, 'id');
        $msCategoryMember = array(
            'class' => 'msCategoryMember',
            'sortdir' => "desc",
            'return' => 'data',
            'limit' => 10000,
            'where' => ['product_id:IN' => $master_ids]
            
        );
        $pdoFetch->setConfig($msCategoryMember);
        $msCategoryMember = $pdoFetch->run();
        //return $rows;
        // подгружаем системный параметр Добавки, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_addpar")) {
            $adds = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 3, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original]
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`,`Data`.`image` as img_url, pagetitle as title',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $adds['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $adds['select']['Price'] = 'Price.price, Price.old_price';
                $adds['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $adds['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $adds['select']['Product'] = $adds['select']['Product'].', price, old_price, price as original_price';
            }
            
            $pdoFetch->setConfig($adds);
            $dobavki = $pdoFetch->run();
        } else {
            $dobavki = array();
        }
        
        // подгружаем системный параметр Types, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_type")) {
            $types = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 9, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original]
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`,`Data`.`price`,`Data`.`image` as img_url, pagetitle as title',
                //'Price' => 'Price.price as price_type, Price.old_price  as old_price_type',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $types['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $types['select']['Price'] = 'Price.price, Price.old_price';
                $types['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $types['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $types['select']['Product'] = $types['select']['Product'].', price, old_price, price as original_price';
            }
            
            $pdoFetch->setConfig($types);
            $types = $pdoFetch->run();
        } else {
            $types = array();
        }
        
        // подгружаем системный параметр Ингредиенты, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_ingredients")) {
            $ingredients = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 8, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original],
                    //'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Data`.`image` as img_url, `Product`.`pagetitle` as title',
                //'Data' => '`Data`.`price`',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $ingredients['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $ingredients['select']['Price'] = 'Price.price, Price.old_price';
                $ingredients['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $ingredients['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $ingredients['leftJoin']['Data'] = ['class' => 'msProductData', 'on' => 'Data.id = Product.id'];
                $ingredients['select']['Data'] = '`Data`.`price`,`Data`.`old_price`,`Data`.`price` as original_price';
            }
            
            $pdoFetch->setConfig($ingredients);
            $ingredients = $pdoFetch->run();
        } else {
            $ingredients = array();
        }
        // подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_analogue")) {
            $analogs = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 10, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original],
                    //'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
                //'Data' => '`Data`.`price`',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $analogs['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $analogs['select']['Price'] = 'Price.price, Price.old_price';
                $analogs['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $analogs['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $analogs['leftJoin']['Data'] = ['class' => 'msProductData', 'on' => 'Data.id = Product.id'];
                $analogs['select']['Data'] = '`Data`.`price`,`Data`.`old_price`,`Data`.`price` as original_price';
            }
            
            $pdoFetch->setConfig($analogs);
            $analogs = $pdoFetch->run();
        } else {
            $analogs = array();
        }
        // подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_related_products")) {
            $relatedProducts = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 15, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    //'Price' => ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type = '.$price_type],
                    //'PriceOriginal' => ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type = '.$price_type_original],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Product`.`pagetitle` as title',
                'Data' => '`Data`.`price`',
                //'Price' => 'Price.price, Price.old_price',
                //'PriceOriginal' => 'PriceOriginal.price as original_price',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            
            if($price_type != 0) {
                $relatedProducts['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = Product.id AND Price.type ='.$price_type];
                $relatedProducts['select']['Price'] = 'Price.price, Price.old_price';
                $relatedProducts['leftJoin']['PriceOriginal'] = ['class' => 'SoftjetsyncPrices', 'on' => 'PriceOriginal.product_id = Product.id AND PriceOriginal.type ='.$price_type_original];
                $relatedProducts['select']['PriceOriginal'] = 'PriceOriginal.price as original_price';
            } else {
                $relatedProducts['leftJoin']['Data'] = ['class' => 'msProductData', 'on' => 'Data.id = Product.id'];
                $relatedProducts['select']['Data'] = '`Data`.`price`,`Data`.`old_price`,`Data`.`price` as original_price';
            }
            
            $pdoFetch->setConfig($relatedProducts);
            $relatedProducts = $pdoFetch->run();
            //$this->modx->log(1,"С ним покупают: $price_type - $price_type_origina".Print_r($relatedProducts,1));
        } else {
            $relatedProducts = array();
        }
        
        // подгружаем системный параметр Аналоги, если он не нулевой, то подтягиваем значения связанные с ним
        if($this->modx->getOption("app_parameters_sizes")) {
            $sizes = array(
                'class' => 'msProductLink',
                'where' => ['Product.class_key' => 'msProduct', 'link' => 16, 'master:IN' => $master_ids],
                'leftJoin' => [
                    'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                    'Data' => ['class' => 'msProductData', 'on' => 'Data.id = Product.id'],
                    
                    'enSize' => ['class' => 'msProductOption', 'on' => 'enSize.product_id = Product.id AND enSize.key = "energy_size"'],
                    'enAllergens' => ['class' => 'msProductOption', 'on' => 'enAllergens.product_id = Product.id AND enAllergens.key = "energy_allergens"'],
                    'enValue' => ['class' => 'msProductOption', 'on' => 'enValue.product_id = Product.id AND enValue.key = "energy_value"'],
                    'enCarbohydrates' => ['class' => 'msProductOption', 'on' => 'enCarbohydrates.product_id = Product.id AND enCarbohydrates.key = "energy_carbohydrates"'],
                    'enProtein' => ['class' => 'msProductOption', 'on' => 'enProtein.product_id = Product.id AND enProtein.key = "energy_protein"'],
                    'enOils' => ['class' => 'msProductOption', 'on' => 'enOils.product_id = Product.id AND enOils.key = "energy_oils"'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`, `Product`.`pagetitle` as title, price, old_price',
                'Data' => '`Data`.`price`',
                
                'enSize' => 'enSize.value as energy_size',
                'enAllergens' => 'enAllergens.value as energy_allergens',
                'enValue' => 'enValue.value as energy_value',
                'enCarbohydrates' => 'enCarbohydrates.value as energy_carbohydrates',
                'enProtein' => 'enProtein.value as energy_protein',
                'enOils' => 'enOils.value as energy_oils',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
            );
            $pdoFetch->setConfig($sizes);
            $sizes = $pdoFetch->run();
            //$this->modx->log(1,"С ним покупают: $price_type - $price_type_origina".Print_r($sizes,1));
        } else {
            $sizes = array();
        }
        
        //return $rows;
        /* ================== /Подгружаем дополнительные параметры ==================== */
        // формируем список стикеров для ресурса
        $stickers = array(
            'class' => 'msProductLink',
            'where' => ['Product.class_key' => 'msProduct', 'link' => 12, 'master:IN' => $master_ids],
            'leftJoin' => [
                'Product' => ['class' => 'msProduct', 'on' => 'Product.id = msProductLink.slave'],
                'ColorText' => ['class' => 'msProductOption', 'on' => 'ColorText.product_id = Product.id AND ColorText.key = "add_color_text"'],
                'ColorBg' => ['class' => 'msProductOption', 'on' => 'ColorBg.product_id = Product.id AND ColorBg.key = "add_color_bg"'],
                'idOrg' => ['class' => 'msProductOption', 'on' => 'idOrg.product_id = Product.id AND idOrg.key = "add_id_org"'],
            ],
            'select' => [
                'msProductLink' => 'master',
                'Product' => '`Product`.`id`,pagetitle as title',
                'ColorText' => 'ColorText.value as color_text',
                'ColorBg' => 'ColorBg.value as color_bg',
                'idOrg' => 'idOrg.value as id_org',
            ],
            'sortby' => 'Product.id',
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
        );
        $pdoFetch->setConfig($stickers);
        $stickers = $pdoFetch->run();
        foreach ($stickers as $key=> $sticker){
            $stickers[$key]["master"] = (int)$stickers[$key]["master"];
            $stickers[$key]["id"] = (int)$stickers[$key]["id"];
            $stickers[$key]["id_org"] = (int)$stickers[$key]["id_org"];
        }
        //return $dobavki;
        //return $stickers;
        $sticerList = array();
        $json['data'] = array();
        $rows = array_combine(array_column($rows, 'id'), $rows);
        foreach ($msCategoryMember as $parent){
            $rows[$parent["product_id"]]['parents'][] = $parent['category_id'];
        }
        foreach ($analogs as $analog){
            $analog["price"] = ($analog["price"])? (int)$analog["price"] : 0;
            $analog["old_price"] = ($analog["old_price"])? (int)$analog["old_price"] : 0;
            $analog["original_price"] = ($analog["original_price"])? (int)$analog["original_price"] : 0;
            
            $analog["master"] = (int)$analog["master"];
            $analog["id"] = (int)$analog["id"];
            $rows[$analog['master']]['analog'][] = $analog;
        }
        foreach ($relatedProducts as $relatedProduct){
            $relatedProduct["price"] = ($relatedProduct["price"])? (int)$relatedProduct["price"] : 0;
            $relatedProduct["old_price"] = ($relatedProduct["old_price"])? (int)$relatedProduct["old_price"] : 0;
            $relatedProduct["original_price"] = ($relatedProduct["original_price"])? (int)$relatedProduct["original_price"] : 0;
            
            $relatedProduct["master"] = (int)$relatedProduct["master"];
            $relatedProduct["id"] = (int)$relatedProduct["id"];
            $rows[$relatedProduct['master']]['relatedProduct'][] = $relatedProduct;
        }
        foreach ($sizes as $size){
            $size["master"] = (int)$size["master"];
            $size["id"] = (int)$size["id"];
            $size["price"] = ($size["price"])? (int)$size["price"] : 0;
            $size["old_price"] = ($size["old_price"])? (int)$size["old_price"] : 0;
            $size["original_price"] = ($size["original_price"])? (int)$size["original_price"] : 0;
            
            $size["energy"]["energy_size"] = ($size["energy_size"])? $size["energy_size"] : 0;
            $size["energy"]["energy_allergens"] = ($size["energy_allergens"])? $size["energy_allergens"] : 0;
            $size["energy"]["energy_value"] = ($size["energy_value"])? $size["energy_value"] : 0;
            $size["energy"]["energy_carbohydrates"] = ($size["energy_carbohydrates"])? $size["energy_carbohydrates"] : 0;
            $size["energy"]["energy_protein"] = ($size["energy_protein"])? $size["energy_protein"] : 0;
            $size["energy"]["energy_oils"] = ($size["energy_oils"])? $size["energy_oils"] : 0;
            
            unset($size["energy_size"],$size["energy_allergens"],$size["energy_value"],$size["energy_carbohydrates"],$size["energy_protein"],$size["energy_oils"]);
            
            //$rows[$size['master']]['size'][] = $size;
            //if(!array_key_exists('default_id',$rows[$size['master']]['size'])) $rows[$size['master']]['size']['default_id'] = $size["id"];
            //if($size["price"] == 0) $rows[$size['master']]['size']['default_id'] = $size["id"];
            /*$rows[$size['master']]['size']['ids'][] = $size["id"];
            $rows[$size['master']]['size']['data'][$size["id"]] = $size;*/
            $rows[$size['master']]['size']['data'][] = $size;
        }
        foreach ($ingredients as $ingredient){
            $ingredient["master"] = (int)$ingredient["master"];
            $ingredient["id"] = (int)$ingredient["id"];
            $ingredient["img_url"] = ($ingredient["img_url"])? $ingredient["img_url"] : "";
            $ingredient["price"] = ($ingredient["price"])? (int)$ingredient["price"] : 0;
            $ingredient["old_price"] = ($ingredient["old_price"])? (int)$ingredient["old_price"] : 0;
            $ingredient["original_price"] = ($ingredient["original_price"])? (int)$ingredient["original_price"] : 0;
            $rows[$ingredient['master']]['ing'][] = $ingredient;
        }
        foreach ($dobavki as $dobavka){
            $dobavka["master"] = (int)$dobavka["master"];
            $dobavka["id"] = (int)$dobavka["id"];
            $dobavka["img_url"] = ($dobavka["img_url"])? $dobavka["img_url"] : "";
            $dobavka["price"] = ($dobavka["price"])? (int)$dobavka["price"] : 0;
            $dobavka["old_price"] = ($dobavka["old_price"])? (int)$dobavka["old_price"] : 0;
            $dobavka["original_price"] = ($dobavka["original_price"])? (int)$dobavka["original_price"] : 0;
            $rows[$dobavka['master']]['adds'][] = $dobavka;
        }
        $minPrice = 0;
        foreach ($types as $type){
            $type["master"] = (int)$type["master"];
            $type["id"] = (int)$type["id"];
            $type["img_url"] = ($type["img_url"])? $type["img_url"] : "";
            $type["price"] = ($type["price_type"])? (int)$type["price_type"] : (int)$type["price"];
            $type["old_price"] = ($type["old_price_type"])? (int)$type["old_price_type"] : (int)$type["old_price"];
            $type["original_price"] = ($type["original_price"])? (int)$type["original_price"] : 0;
            unset($type["price_type"],$type["old_price_type"]);
            //$rows[$type['master']]['types'][] = $type;
            
            if(!array_key_exists('default_index',$rows[$type['master']]['types'])) {
                $rows[$type['master']]['types']['default_index'] = $type["id"];
                $minPrice = $type["price"];
            }
            if($type["price"] < $minPrice){
                $rows[$type['master']]['types']['default_index'] = $type["id"];
                $minPrice = $type["price"];
            } 
            /*$rows[$type['master']]['types']['ids'][] = $type["id"];
            $rows[$type['master']]['types']['data'][$type["id"]] = $type;*/
            $rows[$type['master']]['types']['data'][] = $type;
        }
        foreach ($stickers as $sticker){
            $rows[$sticker['master']]['stickers'][] = $sticker;
        }
        $rows = mergeRating($rows);
        foreach ($rows as $key => $row){
            $order_types = explode(",", $row['order_types']);
            if (in_array($_GET['order_type'],$order_types)){
                $restourants = explode(",", $row['restourants']);
                if (in_array($rest_id, $restourants)) {
                    
                } else {
                    
                    $json['data'][$key]['id'] = (int)$row['id'];
                    $json['data'][$key]['title'] = $row['title'];
                    $json['data'][$key]['parent'] = (int)$row['parent'];
                    $json['data'][$key]['article'] = (int)$row['article'];
                    $json['data'][$key]['allRating'] = ($row['item_rating'])? $row['item_rating'] : 0;
                    $json['data'][$key]['rating'] = (array_key_exists("ratings", $row))? $row['ratings'] : array(); // если в массиве присутствует параметр rating, то добавляем его содержимое
                    $json['data'][$key]['price'] = (double)$row['price'];
                    $json['data'][$key]['old_price'] = (double)$row['old_price'];
                    $json['data'][$key]['original_price'] = (double)$row['original_price'];
                    $json['data'][$key]['desc'] = $row['description'];
                    $json['data'][$key]['content'] = $row['content'];
                    $json['data'][$key]['restourants'] = (int)$row['restourants'];
                    $json['data'][$key]['order_types'] = (int)$row['order_types'];
                    $image_small = ($row['img_big'])? $this->modx->runSnippet('pthumb', [
                                            'input' => substr($row['img_big'], 1),
                                            'options' => '&h=300&w=300&q=100&bg=FFFFFF',
                                        ]) : '/assets/template/images/content/fri.jpg';
                    $json['data'][$key]['img_small'] = $image_small;
                    $json['data'][$key]['img_big'] = $image_small;
                    $json['data'][$key]['bonus_add'] = ($bonus_persent)? floor($row['price'] / 100 * $bonus_persent) : 0; // если есть параметр бонуса, то считаем его
                    $json['data'][$key]['times_bought'] = (int)$row['times_bought'];
                    $json['data'][$key]['remains_count'] = 25;
                    $json['data'][$key]['remains_text'] = "dfuvidsofov";
                    $json['data'][$key]['barcode'] = "111111";
                    $json['data'][$key]['pieces_per_package'] = 25;
                    $json['data'][$key]['stickers'] = (array_key_exists("stickers", $row))? $row["stickers"] : array(); // если в массиве есть параметр stickers, то получаем его содержимое
                    //$json['data'][$key]['sizes'] = ($sizes)? json_decode($sizes) : array();
                    $row['card_type'] = ($row['card_type'])? $row['card_type'] : "base";
                    $json['data'][$key]['card_type'] = $row['card_type'];
                    
                    // Если параметр Energy равен 0, то передаём дальше "заглушку" под его значения
                    if (!$this->modx->getOption("app_parameters_energy")) {
                        $row['energy_size'] = "0";
                        $row['energy_value'] = $row['energy_carbohydrates'] = $row['energy_protein'] = $row['energy_oils'] = 0;
                        $row['energy_allergens'] = "-";
                    }
                    $json['data'][$key]['energy'] = array(
                        "energy_size" => $row['energy_size'],
                        "energy_value" => round($row['energy_value']),    
                        "energy_allergens" => $row['energy_allergens'],
                        "energy_carbohydrates" => round($row['energy_carbohydrates']),
                        "energy_protein" => round($row['energy_protein']),
                        "energy_oils" => round($row['energy_oils']),
                    );
                    // проверяем настройку на вывод списка ссылок для товара (они должны быть добавлены к товару заранее)
                    if ($this->modx->getOption("app_links_list")) {
                        
                        $coordMass = explode(",",str_replace(" ","",$row['link_coordinates']));
                        $json['data'][$key]["GPS"] = array(
                            'latitude' => (float)$coordMass[0],
                            'longitude' => (float)$coordMass[1],
                        );
                        
                        // Добавляем в ответный массив перечень ссылок от объекта
                        $json['data'][$key]['links_list'] = array(
                            "link_phone" => $row['link_phone'],
                            "link_site" => $row['link_site'],
                            "link_vk" => $row['link_vk'],
                            "link_fb" => $row['link_fb'],
                            "link_instagram" => $row['link_instagram'],
                            "link_odnoklassniki" => $row['link_odnoklassniki'],
                        );
                        
                        // получаем картинку превью c ютуба
                        $linkCode = explode("/",$row['link_video']);
                        $linkCode = $linkCode[count($linkCode) - 1];
                        if(stristr($linkCode, '?v=') === FALSE) {
                        } else {
                            $linkCode = explode("?",explode("v=",$linkCode)[1])[0];
                        }
                        
                        $json['data'][$key]['media'] = array(
                            "link_audio" => $row['link_audio'],
                            "link_video" => $row['link_video'],
                            "link_video_prev" => "https://img.youtube.com/vi/{$linkCode}/hqdefault.jpg",
                        );
                        $json['data'][$key]['where_is'] = array(
                            "add_address" => $row['add_address'],
                            "add_time_work" => $row['add_time_work'],
                        );
                    }
                    if (array_key_exists("adds", $row) && sizeof($row['adds'])){
                        $json['data'][$key]['additives'] = $row['adds'];
                    } else {
                        $json['data'][$key]['additives'] = [];
                    }
                    if (array_key_exists("ing", $row) && sizeof($row['ing'])){
                        $json['data'][$key]['ingredients'] = $row['ing'];    
                    } else {
                        $json['data'][$key]['ingredients'] = [];
                    }
                    if (array_key_exists("types", $row) && sizeof($row['types'])){
                        $minPrice = 0;
                        foreach($row['types']['data'] as $keyType => $type){
                            if(!array_key_exists('default_index',$row['types'])) {
                                $row['types']['default_index'] = 0;
                                $minPrice = $type["price"];
                            } 
                            if((int)$type["price"] < (int)$minPrice) {
                                $row['types']['default_index'] = $keyType;
                                $minPrice = $type["price"];
                            }
                        }
                        $json['data'][$key]['types'] = $row['types'];
                    } else {
                        $json['data'][$key]['types'] = [];
                    }
                    if (array_key_exists("analog", $row) && sizeof($row['analog'])){
                        $json['data'][$key]['analogs'] = $row['analog'];
                    } else {
                        $json['data'][$key]['analogs'] = [];
                    }
                    if (array_key_exists("relatedProduct", $row) && sizeof($row['relatedProduct'])){
                        $json['data'][$key]['relatedProducts'] = $row['relatedProduct'];
                    } else {
                        $json['data'][$key]['relatedProducts'] = [];
                    }
                    if (array_key_exists("size", $row) && sizeof($row['size'])){
                        $minPrice = 0;
                        foreach($row['size']['data'] as $keySize => $size){
                            if(!array_key_exists('default_index',$row['size'])){
                                $row['size']['default_index'] = 0;
                                $minPrice = $type["price"];
                            }  
                            if((int)$size["price"] < (int)$minPrice){
                               $row['size']['default_index'] = $keySize;
                               $minPrice = $type["price"];
                            } 
                        }
                        $json['data'][$key]['sizes'] = $row['size'];
                    } else {
                        $json['data'][$key]['sizes'] = [];
                    }
                }
            }   
        }
        // получаем список связей товаров с другими категориями
        $sql = "SELECT product_id,category_id FROM modx_ms2_product_categories";
        $categoryIdLinks = $this->modx->query($sql);
        $categoryIdLinks = $categoryIdLinks->fetchAll(PDO::FETCH_ASSOC);
        // получаем обхекты всех категорий
        $categoryesObjects = $this->modx->getCollection("modResource",array("template"=>3,"parent" => $category_id));
        // перебераем объекты категорий
        foreach($categoryesObjects as $org) {
            // заносим в массив категорий новые элементы
            $categoryMass[] = array("id" => $org->get("id"),"title" => $org->get("pagetitle"));
            // получаем перечень объектов товаров которые лежат в данной категории
            $itemsParent = $this->modx->getCollection("msProduct",array("parent"=>$org->get("id"),"published" => 1,"deleted" => 0));
            // перебераем полученные товары
            foreach($itemsParent as $item){
                // получаем ID товара и заносим его массив
                $categoryIds[$org->get("id")][] = (int)$item->id;
            }
        }
        // перебераем полученный массив с ID товаров и добавлем в него ID товаров из связей с другими категориями
        foreach($categoryIdLinks as $elem) {
           array_push($categoryIds[$elem["category_id"]], (int)$elem["product_id"]);
        }
        
        $json["categories"] = $categoryMass;
        
        $json["entities"] = $categoryIds;
        
        $json["products"] = $json["data"];
        unset($json["data"]);
        return $json;
    }
    //получить вилки ложки
    public function getCartAddivies(){
        // проверяем выключена ли настройка для показа "сопутствующих товаров"
        if(!$this->modx->getOption("app_hidden_related_products")){
            
            if ($this->modx->getOption("app_pricy_type") == "userprice"){
                if ($profile->state){
                    $price_type = $profile->state;
                } else {
                    $price_type = $this->modx->getOption("add_default_type_price");;
                }
                $price_type_original = $price_type;
            } elseif ($this->modx->getOption("app_pricy_type") == "cityprice") {
                if($this->modx->getOption("app_always_courier_price")) {
                    $order_type_tv = "tvTypeDelivery";
                } else {
                    switch ($_GET["order_type"]){
                        case '1': //самовывоз
                            $order_type_tv = "tvType";
                            break;
                        case '2': //курьер
                            $order_type_tv = "tvTypeDelivery";
                            break;
                        case '4': //менеджер
                            $order_type_tv = "tvTypeDelivery";
                            break;
                        case '3': //ресторан
                            $order_type_tv = "tvTypeRestourant";
                            break;
                    }
                }
                
                $org = $this->modx->getObject("modResource", $_GET["id_org"]);
                $price_type = $org->getTVValue($order_type_tv);
                $price_type_original = $org->getTVValue("tvTypeDelivery");
            } else {
                $price_type = $price_type_original = 0;
            }
            $default = array(
                'class' => 'msProduct',
                'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0, "Data.additive"=>1],
                'leftJoin' => [
                  'Data' => ['class' => 'msProductData'],
                ],
                'select' => [
                    'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`',
                    'Data' => '`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small',
                ],
                'sortby' => 'msProduct.id',
                'sortdir' => 'ASC',
                'groupby' => 'msProduct.id',
                'return' => 'data',
                'limit' => 100000,
            );
            $this->pdoFetch->setConfig($default);
            $row = $this->pdoFetch->run();
            
            if($price_type) {
                $default['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type];
                $default['select']['Price'] = 'Price.price, Price.old_price';
            } else {
                $default['select']['msProduct'] .= ', price';
            }
            
            $this->pdoFetch->setConfig($default);
            $row = $this->pdoFetch->run();
            
            foreach($row as $key => $item){
                $row[$key]["price"] = (int)$item["price"];
                $row[$key]["original_price"] = $row[$key]["price"];
            }
        
            /* Добавление в список товаров "сопутствующие товары" списка "с данным товаром покупают"*/
            // ============================
        
            // получаем список товаров
            if($cartItems = $this->ms2->cart->get()){
                foreach($cartItems as $cartItem){
                    $massCartItem .= $cartItem['id'].",";
                }
                // удаляем последнюю запятую
                $massCartItem = substr($massCartItem,0,-1);
                // формируем SQL запрос для получения товаров "с этим товаром покупают" для корзины
                $sql = "SELECT modx_site_content.id FROM modx_site_content, modx_ms2_product_links WHERE modx_site_content.id = modx_ms2_product_links.slave AND modx_ms2_product_links.master IN (".$massCartItem.") AND modx_ms2_product_links.link = 15";
                $result = $this->modx->query($sql);
                // если мы получили результат запроса
                if($result) {
                    // то обрабатываем ответ
                    $resources = $result->fetchAll(PDO::FETCH_ASSOC);
                    // создаём пустые массивы
                    $massAdd = $massAddDesc = array();
                    // формируем массив для сравнения, из тех товаров что уже есть в корзине
                    $massIdes = explode(",",$massCartItem);
                    // перебераем полученные товары по связи
                    foreach ($resources as $res) {
                        // проверяем нет ли уже добавленного товара в массиве, проверяем что бы полученный товар не совпадал с товарами из корзины
                        if(!in_array($res["id"],$massAdd) && !in_array($res["id"],$massIdes)) {
                            // добавляем ID в массив
                            $massAdd[] = $res["id"];
                        }
                    }
                    // формируем запрос для получения всех данных по полученному списку ID товаров
                    $default = array(
                        'class' => 'msProduct',
                        'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0, "id:IN"=>$massAdd],
                        'leftJoin' => [
                          'Data' => ['class' => 'msProductData'],
                        ],
                        'select' => [
                            'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle` as title, parent, `msProduct`.`description`',
                            'Data' => '`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small',
                        ],
                        'sortby' => 'msProduct.id',
                        'sortdir' => 'ASC',
                        'groupby' => 'msProduct.id',
                        'return' => 'data',
                        'limit' => 100000,
                    );
                    
                    if($price_type) {
                        $default['leftJoin']['Price'] = ['class' => 'SoftjetsyncPrices', 'on' => 'Price.product_id = msProduct.id AND Price.type ='.$price_type];
                        $default['select']['Price'] = 'Price.price, Price.old_price';
                    } else {
                        $default['select']['msProduct'] .= ', price';
                    }
            
                    // получаем данные
                    $this->pdoFetch->setConfig($default);
                    $rowAdd = $this->pdoFetch->run();
                    if($rowAdd) {
                        // перебераем полученные данные, меняем тип данных для цен у товара
                        foreach($rowAdd as $key => $rowItem) {
                            $rowAdd[$key]["price"] = (int)$rowItem["price"];
                            $rowAdd[$key]["original_price"] = $rowAdd[$key]["price"];
                        }
                        
                        // объединяем массивы
                        $row = array_merge($row, $rowAdd);
                    }
                }
            }
        } else {
            $row = [];
        }
        
        // ============================
        
        return $row;
    }
}