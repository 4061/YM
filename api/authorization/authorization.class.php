<?php
class Authorization extends ModxApi{
    public function auth(){
        $profile = $this->modx->getObject('modUserProfile', ['phone' => $this->modx->request["request_id"]]);
        if ($profile->fax != $this->modx->request["code"]) return $this->returnError(8);
        if (!$this->modx->request["code"]) return $this->returnError(9);
            $token = md5($this->modx->request["code"].time().$this->modx->request["code"]);
            $profile->set('website', $token);
            $profile->save();
            $json["token"] = $token;
            //$json["data"]["id"] = $profile->internalKey;
            return $json;
    }
    public function getSms(){
        //require MODX_CORE_PATH."vendor/autoload.php";
        $phone = str_replace("+", "", $_GET['phone']);
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace("(", "", $phone);
        $phone = str_replace(")", "", $phone);
        $phone = str_replace("-", "", $phone);
        function generateCodes($profile){ //функция генерации  кодов для авторизации
            $code_sms = rand(1000, 9999); //Генерируем код смс
            $code_request = rand(100000, 999999); //Генерируем code_request
            $profile->set('phone', $code_request);  //Записываем в профиль
            $profile->set('fax', $code_sms); //Записываем в профиль
            $profile->save();
            return $code_request;
        }
        function sendSms($profile,$modx){
            //Отправка смс
            include  $_SERVER["DOCUMENT_ROOT"].'/file/sms/sms.ru.php'; 
            $smsru_api_key = $modx->getOption('smsru_api_key');
            $smsru = new SMSRU($smsru_api_key); // Ваш уникальный программный ключ, который можно получить на главной странице
            $data = new stdClass();
            $data->to = $profile->mobilephone;
            $data->text = "Код ".$profile->fax.". Для регистрации в приложении."; // Текст сообщения
            $smsru->send_one($data);
        }
        if($phone == '71111111111'){
            $json = array("request_id"=>670532);
            return $json;
        }
        if ($phone == '72222222222'){
            $json = array("request_id"=>222222);
            return $json;
        }
        if ($phone == '73333333333'){
            $json = array("request_id"=>290061);
            return $json;
        }
        if ($phone == '75555555555'){
            $json = array("request_id"=>555555);
            return $json;
        }
        if ($phone == '76666666666'){
            $json = array("request_id"=>666666);
            return $json;
        }
        if ($profile = $this->modx->getObject('modUserProfile', ['mobilephone' => $phone])) { //если юзер уже зареган
            //$json = array("request_id"=>generateCodes($profile)); // Генерируем новый код
        } else {
            $email = $phone."@apksite.ru"; 
            $user = $this->modx->newObject('modUser');
            $user->set('username', $email);
            $profile=$this->modx->newObject('modUserProfile');
            $profile->set('fullname', '');
            $profile->set('email', $email);
            $profile->set('mobilephone', $phone);
            $user->addOne($profile);
            $profile->save();
            $user->save();
            /* Регистрация пользователя в Prime Hill Начало */
            if ($this->modx->getOption('loyalty_PH_program')){ //если ПХ включен, то регаем пользователя
                $this->prime_hill->createClient($profile);
            }
            /* Регистрация пользователя в Prime Hill Конец */
            $user_id = $profile->get('internalKey');
            //$generator = new Picqer\Barcode\BarcodeGeneratorJPG();
            //file_put_contents(MODX_BASE_PATH.'/uploads/barcodes/'.$user_id."card.jpg", $generator->getBarcode($user_id."softjetcard", $generator::TYPE_CODE_128)); //сохраняем штрихкод бонус-карты
            $start_count = $this->modx->getOption('msb2_bonus_for_registration');
            $bonus = $this->modx->getObject('msb2User', array("user"=>$user_id));
            $bonus->set('points', $start_count); //начисляем стартовые баллы
            $bonus->save();
            //Генерация промкода
            $first_code_part = rand(1000, 9999);
            $second_code_part = rand(1000, 9999);
            $promocode = "U-".$first_code_part."-".$second_code_part;
            $code_obj = $this->modx->newObject("PromoBase");
            $code_obj->set("code", $promocode); //Сам промокод
            $code_obj->set("availability", 1); //Доступен для всех
            $code_obj->set("user_use_count", 1); //Пользователь может его использовать только один раз
            $code_obj->set("use_count", $this->modx->getOption("app_referal_use_count")); //Суммарное количество использований
            $code_obj->set("use_count_in_check", 1); //Сколько раз можно использовать в корзине
            $code_obj->set("type", $this->modx->getOption("app_referal_type")); //Тип 3 (реферальный код) 
            $code_obj->set("is_referal", 1);
            if($this->modx->getOption("app_referal_time_used")) $code_obj->set("date_end", date("Y-m-d 00:00:00",strtotime(date("Y-m-d H:i:s")." + ".$this->modx->getOption("app_referal_type")." days")));
            $code_obj->set("author_id", $profile->internalKey); //Автор айди
            $code_obj->set("author_bonus", $this->modx->getOption("app_referal_bonus")); //Сколько бонусов будет начислено автору, если его промокод будет активирован
            $code_obj->set("child_discount", 200); //Какую максимальную скидку в корзине за активацию получит пользователь, который юханет его код
            $code_obj->set("activity", 1); //активен
            $code_obj->set("description", "Реферальный код, созданный при регистрации."); //Коммент для нас
            
            switch((int)$this->modx->getOption("app_referal_type")){
                case 1: // товар в подарок
                    $code_obj->set("child_discount", $this->modx->getOption("app_referal_type_value"));
                    $code_obj->set("id_product", "");
                    $code_obj->set("child_discount_rub", 0);
                break;
                case 2: // скидка в процентах
                    $code_obj->set("child_discount", 0);
                    $code_obj->set("id_product", $this->modx->getOption("app_referal_type_value"));
                    $code_obj->set("child_discount_rub", 0);
                break;
                case 5: // скидка в рублях
                    $code_obj->set("child_discount", 0);
                    $code_obj->set("id_product", "");
                    $code_obj->set("child_discount_rub", $this->modx->getOption("app_referal_type_value"));
                break;
            }
            
            $code_obj->save();

            //если передается параметр courier == true назначаем роль курьера
            if ($_GET["courier"] == true) {
                $modUserGroupMember = $this->modx->newObject("modUserGroupMember", array("role"=>1, "member"=>$profile->internalKey, "user_group"=>6));
                $modUserGroupMember->save();
            }
        }
        $sql = "
        SELECT * FROM modx_sms_requests 
        WHERE 
            modx_sms_requests.phone = '$phone' AND 
            modx_sms_requests.last_request >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -20 SECOND)";
        $last_request = $this->modx->query($sql);
        $last_request = $last_request->fetchAll(PDO::FETCH_ASSOC);
        $sql = "
        SELECT Count(*) as count FROM modx_sms_requests 
        WHERE 
            modx_sms_requests.phone = '$phone' AND 
            modx_sms_requests.last_request >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL -1 DAY)";
        $day_requests = $this->modx->query($sql);
        $day_requests = $day_requests->fetchAll(PDO::FETCH_ASSOC);
        $day_requests = $day_requests[0]["count"];
        //$sendsms = false;
        if ($last_request) return $this->returnError(13);
        if ($day_requests >= 155) return $this->returnError(14);
        $sql = "INSERT INTO modx_sms_requests (id, phone, last_request) VALUES (NULL, '$phone', CURRENT_TIMESTAMP)";
        $this->modx->query($sql);
        $json = array("request_id"=>generateCodes($profile)); // Генерируем новый код
        sendSms($profile, $this->modx); // отправляем смс
        return $json;
    }
}