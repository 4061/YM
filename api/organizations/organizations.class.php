<?php
class Organizations extends ModxApi{
    public function getCities(){
        $sql = "SELECT id FROM modx_site_content WHERE parent = 1802 and published = 1 ORDER BY pagetitle";
        $orgs = $this->modx->query($sql);
        $orgs = $orgs->fetchAll(PDO::FETCH_ASSOC);
        foreach ($orgs as $org){
            $res = $this->modx->getObject('modResource', $org['id']);
            
            // следом, мы получаем id первой организации по списку
            $sql = 'SELECT id FROM modx_site_content WHERE parent = "'.$org['id'].'" AND template = 18 ORDER BY menuindex LIMIT 1';
            $resultQuery = $this->modx->query($sql);
            
            $json['towns'][] = array(
                "title" => $res->get('pagetitle'),
                "id" => (int)$res->get('id'),
                "default_id_org" => (int)$resultQuery->fetch(PDO::FETCH_ASSOC)['id'],
            );
        }
        return $json;
    }
    public function getOrganizations(){
        //session_abort();
        $url =  $this->modx->config['site_url'];
        $url = str_replace('//','-',$url);
        $url = str_replace('/','',$url);
        $url = str_replace('-','//',$url);
        $parent = $_GET['city_id'];
        $sql = "SELECT id FROM modx_site_content WHERE parent = $parent and published = 1";
        $orgs = $this->modx->query($sql);
        $orgs = $orgs->fetchAll(PDO::FETCH_ASSOC);
        $json['orgs'] = array();
        if ($orgs){
            foreach ($orgs as $org){
                $payments = array();
                $deliveries = array();
                $res = $this->modx->getObject('modResource', $org['id']);
                $delivery = $res->getTVValue('org_dels');
                $delivery = json_decode($delivery,true);
                if ($res->getTVValue('online_payment')==1){
                    $payments[] = array("id" =>1, "name"=>"Оплата онлайн", "is_online" => true, "is_cash" => false);
                }
                if ($res->getTVValue('cash_courier_payment')==1){
                    $payments[] = array("id" =>3, "name"=>"Оплата наличными при получении", "is_online" => false, "is_cash" => true);
                }
                if ($res->getTVValue('cashless_courier_payment')==1){
                    $payments[] = array("id" =>4, "name"=>"Оплата картой при получении", "is_online" => false, "is_cash" => false);
                }
                if ($res->getTVValue('courier')==1){
                    $deliveries[] = array("id" =>2, "name"=>"Курьером","payment_methods" => explode(",",$res->getTVValue('payment_types_delivery')));
                }
                if ($res->getTVValue('selfcourier')==1){
                    $deliveries[] = array("id" =>1, "name"=>"Самовывоз","payment_methods" => explode(",",$res->getTVValue('payment_types_pickup')));
                }
                if ($res->getTVValue('restourant')==1){
                    $deliveries[] = array("id" =>3, "name"=>"В ресторане","payment_methods" => explode(",",$res->getTVValue('payment_types_restaurant')));
                }
                // получаем из MIGX таблицы данные по банкам
                $banksTable = json_decode($res->getTVValue('acquiring_types'));
                $pymentOnline = array();
                // перебераем полученный список
                foreach($banksTable as $key => $bank) {
                    $bank = (array)$bank;
                    // если элемент в перечне не отключён, то добпавляем его в массив основного списка
                    if($bank["activity"]){
                        $pymentOnline[] = $bank;
                        unset($pymentOnline[$key]["MIGX_id"],$pymentOnline[$key]["terminal_key"],$pymentOnline[$key]["secret_key"],$pymentOnline[$key]["activity"]);
                    }
                }
                
                //$payments[] = array("id" =>6, "name"=>"Онлайн оплата с привязкой карты");
                $GPS = $res->getTVValue('GPS');
                $GPS = explode(",", $GPS);
                $GPS[0] = ($GPS[0])? $GPS[0] : "";
                $GPS[1] = ($GPS[1])? $GPS[1] : "";
                //$lan = $GPS[0];
                //$lan = round($lan,12);
                $timework = json_decode($res->getTVValue("time_zones"));
                $time_zones = [];
                foreach ($timework as $key => $time){
                    $time_zone = array(
                        "start" => $time->start,
                        "end" => $time->end,
                    );
                    $time_zones[] = $time_zone;
                }
                // создаём пустой массив стикеров
                $stickerMass = array();
                // получаем список стикеров
                $stickersOdjects = $this->modx->getCollection('msProduct',array('template' => 42));
                // перебераем полученный список стикеров
                foreach($stickersOdjects as $sticker) {
                    // получаем опции где хранится ID города
                    $options = $sticker->loadData()->get('options');
                    // разбиваем полученный строку с ID на массив
                    $optionMass = explode(",",$options["add_id_org"][0]);
                    // проверяем есть ли в полученном массиве указанный город
                    if(in_array($res->get('id'),$optionMass)) {
                        // если есть, то заносим его в резултирующий массив
                        $stickerMass[] = array(
                            "id" => $sticker->get("id"),
                            "title" => $sticker->get("pagetitle"),
                            "color_text" => $sticker->get("add_color_text")[0],
                            "color_bg" => $sticker->get("add_color_bg")[0],
                        );
                    }
                }
                
                $json['orgs'][] = array(
                    "title" => $res->get('pagetitle'),
                    "id" => $res->get('id'),
                    "requisites" => $url."/".$res->get("uri"),
                    "deliveryTerminalId" => $res->getTVValue('deliveryTerminalId'),
                    "img" => $res->getTVValue('image'),
                    "waiting_time" => $res->getTVValue('waitingtime'),
                    "address" => $res->get('longtitle')? $res->get('longtitle') : $res->get('pagetitle'),
                    "description" => $res->get('description'),
                    "comment" => (string)$res->get('introtext'),
                    "phone" => $res->getTVValue('phone'),
                    "phone_name" => $res->getTVValue('tvPhoneName'),
                    "worktime" => $res->getTVValue('mode'),
                    "GPS" => array("latitude"=>($GPS[0]), "longitude"=>$GPS[1]),
                    "pymentOnline"=>$pymentOnline,
                    "payments"=>$payments,
                    "deliveries"=>$deliveries,
                    "order_days" => (int)$res->getTVValue('tvOrderDays'),
                    "time_zones" => $time_zones,
                    "stickers" => ($stickerMass)? $stickerMass : array(),
                );
            }
        }
        return $json;
    }
    public function getCityStreets(){
        return "В процессе";
    }
    public function getOrganizationByAddress(){
        function getLineEquation($coor){ //получаем уравнение прямой
            $x1 = $coor[0][0];
            $y1 = $coor [0][1];
            $x2 = $coor[1][0];
            $y2 = $coor[1][1];
            $resolve1 = ($x2-$x1); //знаменатель 1
            $resolve2 = ($y2-$y1); //знаменатель 2
            $resolve3 = ($resolve1*(-$y1));
            $resolve4 = ($resolve2*(-$x1));
            $newx = $resolve2;
            $newy = $resolve1;
            $newx = -$newx;
            $resolve5 = $resolve4-$resolve3;
            $coorY = $newy;
            $coorX = $newx;
            $C = $resolve5;
            if ($C <0){
                $C = ($C*-1);
                $coorY = ($coorY*-1);
                $coorX = ($coorX*-1);
            }
            //echo($coorX.'x'.$coorY.'y'.'='.$C);  //какое уравнение мы получаем
            $equation = array("X" =>$coorX, "Y" =>$coorY, "=" =>$C);
            return($equation);
        }
        function getRayEquation($ray){
            $result = array($ray[1], $ray);
            return $result;
        }
        function getCoor($coor, $ray){
            $ray = getRayEquation($ray);
            $equation = getLineEquation($coor);
            $resultY = $ray[0];
            $ray_coors = $ray[1];
            $y = $resultY * $equation['Y'];
            $resultX = ($equation['='] - $y)/$equation['X'];
            $result = array($resultX, $resultY); //точка пересечения
            $x_coor = $ray[1][0];
            //echo($result[0]);
            //echo('    '.$x_coor);
            //echo('</br>'.'Координата X '.$ray[0].', Координата Y '.$x_coor);
            //echo('</br>');
            //echo json_encode($coor);
            //echo('</br>');
            $y1 = $coor[0][1];
            $y2 = $coor[1][1];
            if ($y1 > $y2){
                if ($ray[0] >= $y2 && $ray[0] <=$y1){
                    if ($x_coor <= $result[0]){
                            
                    } else {
                        return($result);
                    }
                }
            } else {
                if ($ray[0] >= $y1 && $ray[0] <=$y2){
                    if ($x_coor <= $result[0]){
                           
                    } else {
                        return($result);
                    }
                }
            }
        }
        $x_coordinate = $_GET['x'];
        $y_coordinate = $_GET['y'];
        $country = $_GET['country'];
        $city = $_GET['city'];
        $cityId = $_GET['city_id'];
        $street = $_GET['street'];
        $house = $_GET['house'];
        
        // формируем массив из адресов
        $streetArray = array($country,$city,$street,$house);
        
        if (!$x_coordinate){
            $search_string = $country."+".$city."+".$street."+".$house;
            $curl = curl_init();
            $city = urlencode($search_string);
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://geocode-maps.yandex.ru/1.x/?apikey=152b2615-c5be-45e5-aed4-501faef5b5ba&geocode='$city'&format=json",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            // ===== Проверка на корректность указанного адреса ======
            // переводим ответ от Яндекс в массив
            $response_arr = json_decode($response,1);
            // флаг проверки на корректность адреса
            $checkAddressHidden = 0;
    
            //$this->modx->log(1,"Мы в методе!: ".print_r($response_arr["response"]["GeoObjectCollection"]["featureMember"],1).", coord_x ".$x_coordinate);
            // проверяем наличие количества полученныйх результатов
            //if($response_arr["response"]["GeoObjectCollection"]["metaDataProperty"]["GeocoderResponseMetaData"]["found"] == 1) {
                // если в массиве присутствует откорректированный яндексом адрес
                if($response_arr["response"]["GeoObjectCollection"]["metaDataProperty"]["GeocoderResponseMetaData"]["suggest"]) {
                    //$this->modx->log(1,"Улица от Яндекс: ".$response_arr["response"]["GeoObjectCollection"]["metaDataProperty"]["GeocoderResponseMetaData"]["suggest"]);
                    // удаляем в откорректированном яндексом адресе ковычки в начале и в конце
                    $streetArray = $response_arr["response"]["GeoObjectCollection"]["metaDataProperty"]["GeocoderResponseMetaData"]["suggest"];
                    $streetArray = substr(substr($streetArray, 1),0,-1);
                    // разбиваем строку на массив
                    $streetArray = explode("+",$streetArray);
                    // удаляем улицу
                    unset($streetArray[2]);
                } else {
                    $search_string = str_replace(" ","+",$search_string);
                    $streetArray = explode("+",$search_string);
                }
                
                //print_r($streetArray);
                // перебераем массив улицы
                //return $streetArray;
                foreach($streetArray as $addressElement) {
                    // проверяем наличие элемента с чатью адреса
                    if($addressElement) {
                        //$this->modx->log(1,"Элементы для поиска: ".$addressElement);
                        $checkAddress = str_replace("ё","е",mb_strtolower($response_arr["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["metaDataProperty"]["GeocoderMetaData"]["text"]));
                        //$this->modx->log(1,"Сравнение: ".$checkAddress);
                        // проверяем входят ли элементы массива улицы в строку адреса найденную яндексом
                        if(stristr($checkAddress, str_replace("ё","е",mb_strtolower($addressElement))) === FALSE) {
                            // тут у нас должен быть флаг для использования далее по коду
                            // с помощью него нужно будет вернуть в приложение что адрес указан неверный
                            //return $addressElement;
                            $checkAddressHidden = 1;
                            break;
                        } else {
        
                        }
                    }
                }
            /*} else {
                // если у нас яндекс вернул несколкьо значений вместо одного, то флаг проверки адрес переводим в 1
    
                $checkAddressHidden = 1;
            }*/
            // ===== /Проверка на корректность указанного адреса ======
            
            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              $response = json_decode($response, 1);
              $coors = ($response["response"]["GeoObjectCollection"]["featureMember"][0]["GeoObject"]["Point"]["pos"]);
              $coors = explode(" ", $coors);
              $x_coordinate = $coors[0];
              $y_coordinate = $coors[1];
            }
            // если у нас адрес не корректный, то задаём нулевые координаты, что бы не попасть в полигоны
            if($checkAddressHidden) {
                $x_coordinate = "0.000000";
                $y_coordinate = "0.000000";
            }
        }
        
        $sql = "SELECT * FROM modx_polygons_base WHERE status = 1";
        $polygons= $this->modx->query($sql);
        $polygons = $polygons->fetchAll(PDO::FETCH_ASSOC);

        $ray = array($x_coordinate,$y_coordinate);

        foreach ($polygons as $polygon){
            $coords = $polygon["Polygon_coords"];
            $coords = explode(" ", $coords);
            $new_coords = array();
            $main_coords = array();
            foreach ($coords as $coord){
                $coord = explode(",", $coord);
                unset($coord[2]);
                array_push($new_coords, $coord);
            }
            $count =  count($new_coords);
            for ($i=0; $i<$count;$i++){
                $new_coords[$i][0] = round($new_coords[$i][0],7);
                $new_coords[$i][1] = round($new_coords[$i][1],7);
            }
            $array = array();
            for ($i=0; $i<$count-1;$i++){
               $arr[0] = $new_coords[$i];
               $arr[1] = $new_coords[$i+1];
               array_push($array, $arr);
            }
            $coors = $array;
            $touches = array();
            foreach ($coors as $coor){
                $result = getCoor($coor, $ray);
                if ($result != false){
                    array_push($touches, $result); //пушим в массив touches все пересечения
                }
            }
            $result = array();
            foreach ($touches as $touch){
                if (in_array($touch, $result)){
                
                } else {
                    array_push($result, $touch);
                }
            }
            $touch_count =  count($result);
            if (($touch_count % 2) == 0){
            } else {
                if($this->modx->getOption("app_we_use_brands")) {
                    $keyRestoran = (int)$polygon['Restourant_id'];
                    $dataList[$keyRestoran] = array(
                        "id_org" => $keyRestoran,
                        "polygon_id" => (int)$polygon['Id'],
                    );
                    //$dataList[$keyRestoran]["min_delivery_cost"] = (int)$polygon['min_count_cost'];
                    //$_SESSION[$keyRestoran]["min_delivery_cost"] = (int)$polygon['min_count_cost'];
                    //$dataList[$keyRestoran]["min_free_delivery_cost"] = (int)$polygon['free_shipping'];
                    //$_SESSION[$keyRestoran]["min_free_delivery_cost"] = (int)$polygon['free_shipping'];
                } else {
                    $data = array("id_org"=>(int)$polygon['Restourant_id']);
                    //$data["min_delivery_cost"] = (int)$polygon['min_count_cost'];
                    //$_SESSION["min_delivery_cost"] = (int)$polygon['min_count_cost'];
                    //$data["min_free_delivery_cost"] = (int)$polygon['free_shipping'];
                    //$_SESSION["min_free_delivery_cost"] = (int)$polygon['free_shipping'];
                    //$_SESSION["min_delivery_cost"] = (int)$polygon['min_count_cost'];
                    //$_SESSION["min_delivery_cost"] = 1500;
                }
                $data["polygon_id"] = (int)$polygon['Id'];
            }
        }
        
        // проверка настройик на получения организации
        if(!$this->modx->getOption("app_org_change")) {
            // следом, мы получаем id первой организации по списку
            $sql = 'SELECT id FROM modx_site_content WHERE parent = "'.$cityId.'" AND template = 18 ORDER BY menuindex LIMIT 1';
            $resultQuery = $this->modx->query($sql);
            // заносим в массив data id организации
            $data = array("id_org"=>(int)$resultQuery->fetch(PDO::FETCH_ASSOC)['id']);
        }
        // проверяем, включена ли настройка на получение брендов для организаций
        if($this->modx->getOption("app_we_use_brands")){
            $citysGet = $this->modx->getCollection("modResource",array("pagetitle" => $_GET['city']));
            foreach($citysGet as $cityGet){
                $cityGetId = $cityGet->get("id");
            }
            // Получаем список объектов брэндов
            $brandsList = $this->modx->getCollection("modResource",array("parent" => $this->modx->getOption("app_brand_id_category")));
            foreach($brandsList as $brand) {
                // получаем массив перечисленных городов у бренда
                $cityCheck =  explode(",",$brand->getTVValue("tvCity"));
                //$this->modx->log(1,$cityCheck." - ".$cityGetId." (".utf8_decode($city).")");
                // проверяем, есть ли переданный нами город в списке городов бренда
                if(in_array($cityGetId,$cityCheck)) {
                    // получаем через SQL запрос id одной из организаций
                    $sql = 'SELECT contentid FROM modx_site_tmplvar_contentvalues WHERE value = '.$brand->id;
                    $query = $this->modx->query($sql);
                    $dataOrg = $query->fetch(PDO::FETCH_ASSOC);
                    // формируем общий массив имеющихся брендов
                    $brands[] = array(
                        "id_brand" => $brand->id,
                        "title" => $brand->pagetitle,
                        "image" => $brand->getTVValue("image"),
                        "message_title" => "К вам пока не доставляем :(",
                        "message" => "По данному адресу не осуществляем доставку. Попробуйте ввести другой адрес.",
                        "data" => array(
                                "id_org" => ($dataOrg["contentid"])? $dataOrg["contentid"] : 0,
                                "polygon_id" => NULL,
                                "brand" => $brand->id,
                                "work_status" => true
                            )
                    );
                }
            }
        } else {
            
        }
        
                    
        //$this->modx->log(1, "ДанныеНовые:".print_r($dataList,1));
        //$this->modx->log(1, "Брэнды:".print_r($brands,1));
        
        // проверка на хотя бы одно попадание в полигон
        $checkPoligonsYes = 0;
        
        if($this->modx->getOption("app_we_use_brands")){
            
            foreach ($dataList as $data) {
    
                $res = $this->modx->getObject('modResource', (int)$data["id_org"]);
                $data['title'] = $res->getTVValue('waitingtime'); // старое название successMsgTitle
                $data['brand'] = $res->getTVValue('tvBrands');
                $data['polygon_id'] = (int)$data["polygon_id"];
                $org_timework = $res->getTVValue('org_timework');
                $org_timework = json_decode($org_timework,true);
                $current_day =  date('l'); //получаем текущий день
                $yesterday = date('l',strtotime("-1 days")); //получаем вчерашний день
                $yesterday_start_array = explode(":",$org_timework[0][$yesterday]);
                $yesterday_day_start = ($yesterday_start_array[0]*60*60)+($yesterday_start_array[1]*60);
                $yesterday_end_array = explode(":",$org_timework[1][$yesterday]);
                $yesterday_day_end = ($yesterday_end_array[0]*60*60)+($yesterday_end_array[1]*60);
                $current_time = (date('G')*60*60)+(date("i")*60);
                $day_start_array = explode(":",$org_timework[0][$current_day]);
                $this_day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60);
                $day_end_array = explode(":",$org_timework[1][$current_day]);
                $this_day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60);
                if ($yesterday_day_end < $yesterday_day_start && $current_time <= $yesterday_day_end){ //если время конца вчерашнего дня меня времени начала вчерашнего дня и настоящее время меньше времени конца вчерашнего дня, то берем временный диапазон вчерашнего дня
                    $diapason_day = date('l',strtotime("-1 days"));
                } else {
                    $diapason_day = date('l');
                }
                if ($yesterday_day_end > $yesterday_day_start && $this_day_start > $yesterday_day_end && $current_time < $this_day_start){
                    $diapason_day = date('l',strtotime("-1 days"));
                }
                $day_start_array = explode(":",$org_timework[0][$diapason_day]);
                $day_end_array = explode(":",$org_timework[1][$diapason_day]);
                $day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60); //время начала текущего диапазона
                $day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60); //время конца текущего диапазона
                if ($day_start > $day_end && $current_time >= $day_end && $current_time < $day_start){ //если время начала больше времени конца и текущее время больше времени конца и меньше времени начала
                    $hasWork = false;
                } elseif ($day_start < $day_end && $current_time < $day_end && $current_time >= $day_start){ //если время начала меньше времени конца и настоящее время попадает в этот промежуток то тру
                    $hasWork = true;
                } elseif ($day_start < $day_end && $current_time < $day_start){ //если время начала меньше времени конца и настоящее время меньше времени начала, то фолс
                    $hasWork = false;
                } elseif ($day_start < $day_end && $current_time >= $day_end){ //если время начала меньше времени конца и настоящее время больше времени конца, то фолс
                    $hasWork = false;
                } else {
                    $hasWork = true;
                }
                if ($hasWork){
                   $data['description'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimesubtitle');  // старое название successMsgSubtitle
                   $data["work_status"] = true;
                   $checkPoligonsYes++;
                } else {
                   $data['description'] = $this->modx->getOption('close_message'); 
                   $data['title'] = "Рестораны закрыты :(";
                   $data["work_status"] = false;
                   $checkPoligonsYes++;
                }
                
                if ($res){
                    if ($res->getTVValue('courier') == 0){
                       $data['description'] = "К сожалению, данный ресторан не может принять заказ в настоящее время по техническим причинам."; 
                       $data['title'] = "Ресторан не может принять заказ.";
                       $data["work_status"] = false;   
                    }
                }
                
                //$this->modx->log(1, "Данные (".$data["id_org"]."):".print_r($data,1));
                
                //$output['brands'] = $brands;
                foreach($brands as $key => $brandElement){
                    if($data['brand'] == $brandElement['id_brand']) {
                        $brands[$key]["message_title"] = $data['title']; // старое название errorMsgTitle
                        $brands[$key]["message"] = $data['description']; // старое название errorMsgSubtitle
                        unset($data['title'],$data['description']);
                        $brands[$key]['data'] = $data;
                    }
                }
            }
            
            $data = array("organisation" => (object)[]);
            $data["brands"] = $brands;
            $data["polygon_id"] = 0;
            
        } else {
            //$data = 
            if ($data){
                $res = $this->modx->getObject('modResource', (int)$data['id_org']);
                $data['title'] = $res->getTVValue('waitingtime'); // старое название successMsgTitle
                $org_timework = $res->getTVValue('org_timework');
                $org_timework = json_decode($org_timework,true);
                $current_day =  date('l'); //получаем текущий день
                $yesterday = date('l',strtotime("-1 days")); //получаем вчерашний день
                $yesterday_start_array = explode(":",$org_timework[0][$yesterday]);
                $yesterday_day_start = ($yesterday_start_array[0]*60*60)+($yesterday_start_array[1]*60);
                $yesterday_end_array = explode(":",$org_timework[1][$yesterday]);
                $yesterday_day_end = ($yesterday_end_array[0]*60*60)+($yesterday_end_array[1]*60);
                $current_time = (date('G')*60*60)+(date("i")*60);
                $day_start_array = explode(":",$org_timework[0][$current_day]);
                $this_day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60);
                $day_end_array = explode(":",$org_timework[1][$current_day]);
                $this_day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60);
                if ($yesterday_day_end < $yesterday_day_start && $current_time <= $yesterday_day_end){ //если время конца вчерашнего дня меня времени начала вчерашнего дня и настоящее время меньше времени конца вчерашнего дня, то берем временный диапазон вчерашнего дня
                    $diapason_day = date('l',strtotime("-1 days"));
                } else {
                    $diapason_day = date('l');
                }
                if ($yesterday_day_end > $yesterday_day_start && $this_day_start > $yesterday_day_end && $current_time < $this_day_start){
                    $diapason_day = date('l',strtotime("-1 days"));
                }
                $day_start_array = explode(":",$org_timework[0][$diapason_day]);
                $day_end_array = explode(":",$org_timework[1][$diapason_day]);
                $day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60); //время начала текущего диапазона
                $day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60); //время конца текущего диапазона
                if ($day_start > $day_end && $current_time >= $day_end && $current_time < $day_start){ //если время начала больше времени конца и текущее время больше времени конца и меньше времени начала
                    $hasWork = false;
                } elseif ($day_start < $day_end && $current_time < $day_end && $current_time >= $day_start){ //если время начала меньше времени конца и настоящее время попадает в этот промежуток то тру
                    $hasWork = true;
                } elseif ($day_start < $day_end && $current_time < $day_start){ //если время начала меньше времени конца и настоящее время меньше времени начала, то фолс
                    $hasWork = false;
                } elseif ($day_start < $day_end && $current_time >= $day_end){ //если время начала меньше времени конца и настоящее время больше времени конца, то фолс
                    $hasWork = false;
                } else {
                    $hasWork = true;
                }
                if ($hasWork){
                   $data['description'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimesubtitle');  // старое название successMsgSubtitle
                   $data["work_status"] = true;
                   $checkPoligonsYes++;
                } else {
                   $data['description'] = $this->modx->getOption('close_message'); 
                   $data['title'] = "Рестораны закрыты :(";
                   $data["work_status"] = false;
                   $checkPoligonsYes++;
                }
            } else {
                $data["title"] = "К вам пока не доставляем :("; // старое название errorMsgTitle
                $data["description"] = "По данному адресу не осуществляем доставку. Попробуйте ввести другой адрес."; // старое название errorMsgSubtitle
            }
            if ($res){
                if ($res->getTVValue('courier') == 0){
                   $data['description'] = "К сожалению, данный ресторан не может принять заказ в настоящее время по техническим причинам."; 
                   $data['title'] = "Ресторан не может принять заказ.";
                   $data["work_status"] = false;   
                }
            }
            
            $outputMass = $data;
            $data = array("organisation" => $outputMass);
            $data["brands"] = (object)[];
        }
        $data["polygon_id"] = ($outputMass["polygon_id"])? $outputMass["polygon_id"] : 0;
        //unset($data["organisation"]["polygon_id"]);
        $data["success"] = ($checkPoligonsYes)? true : false;
        //$data = array("polygon_id"=>(int)$polygon['id']);
        return ($data);
    }
    public function getOrganizationComment(){
        if (!$_GET["id_org"]) {
            $rest_id = $_GET['rest_id'];   
        } else {
            $rest_id = $_GET['id_org'];
        }
        $res = $this->modx->getObject('modResource', (int)$rest_id);
        $org_timework = $res->getTVValue('org_timework');
        $org_timework = json_decode($org_timework,true);
        $current_day =  date('l'); //получаем текущий день
        $yesterday = date('l',strtotime("-1 days")); //получаем вчерашний день
        $yesterday_start_array = explode(":",$org_timework[0][$yesterday]);
        $yesterday_day_start = ($yesterday_start_array[0]*60*60)+($yesterday_start_array[1]*60);
        $yesterday_end_array = explode(":",$org_timework[1][$yesterday]);
        $yesterday_day_end = ($yesterday_end_array[0]*60*60)+($yesterday_end_array[1]*60);
        $current_time = (date('G')*60*60)+(date("i")*60);
        $day_start_array = explode(":",$org_timework[0][$current_day]);
        $this_day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60);
        $day_end_array = explode(":",$org_timework[1][$current_day]);
        $this_day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60);
        if ($yesterday_day_end < $yesterday_day_start && $current_time <= $yesterday_day_end){ //если время конца вчерашнего дня меня времени начала вчерашнего дня и настоящее время меньше времени конца вчерашнего дня, то берем временный диапазон вчерашнего дня
            $diapason_day = date('l',strtotime("-1 days"));
        } else {
            $diapason_day = date('l');
        }
        if ($yesterday_day_end > $yesterday_day_start && $this_day_start > $yesterday_day_end && $current_time < $this_day_start){
            $diapason_day = date('l',strtotime("-1 days"));
        }
        $day_start_array = explode(":",$org_timework[0][$diapason_day]);
        $day_end_array = explode(":",$org_timework[1][$diapason_day]);
        $day_start = ($day_start_array[0]*60*60)+($day_start_array[1]*60); //время начала текущего диапазона
        $day_end = ($day_end_array[0]*60*60)+($day_end_array[1]*60); //время конца текущего диапазона
        if ($day_start > $day_end && $current_time >= $day_end && $current_time < $day_start){ //если время начала больше времени конца и текущее время больше времени конца и меньше времени начала
            $hasWork = false;
        } elseif ($day_start < $day_end && $current_time < $day_end && $current_time >= $day_start){ //если время начала меньше времени конца и настоящее время попадает в этот промежуток то тру
            $hasWork = true;
        } elseif ($day_start < $day_end && $current_time < $day_start){ //если время начала меньше времени конца и настоящее время меньше времени начала, то фолс
            $hasWork = false;
        } elseif ($day_start < $day_end && $current_time >= $day_end){ //если время начала меньше времени конца и настоящее время больше времени конца, то фолс
            $hasWork = false;
        } else {
            $hasWork = true;
        }
        if ($_GET['order_type'] == 1){
            //$json['successMsgTitle'] = "Доставим за ".$res->getTVValue('waitingtimeself'); 
            $json['message'] = $res->getTVValue('waitingtimeself'); 
            $json['message_title'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimeselfsubtitle');
        }
        if ($_GET['order_type'] == 2){
            //$json['successMsgTitle'] = "Доставим за ".$res->getTVValue('waitingtime'); 
            $json['message'] = $res->getTVValue('waitingtime'); 
            $json['message_title'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimesubtitle');
        }
        if ($hasWork){
           $json['message'] = $res->getTVValue('waitingtime'); 
           $json["work_status"] = true;
           $json['message_title'] = $res->get("pagetitle").". ".$res->getTVValue('waitingtimeselfsubtitle'); 
        } else {
           $json['message_title'] = $this->modx->getOption('close_message'); 
           $json['message'] = "Рестораны закрыты :(";
           $json["work_status"] = false;
        }
        $iiko_send_statuses[2] = $res->getTVValue('courier');
        $iiko_send_statuses[1] = $res->getTVValue('selfcourier');
        $iiko_send_statuses[3] = $res->getTVValue('restourant');
        if ($_GET['order_type']){
            if ($iiko_send_statuses[$_GET['order_type']] == 0){
               $json['message'] = "К сожалению, данный ресторан не может принять заказ в настоящее время по техническим причинам."; 
               $json['message_title'] = "Ресторан не может принять заказ.";
               $json["work_status"] = false;   
            }
        }
        return $json;
    }
}