<?php
class Info extends ModxApi{
    //получить способы оплаты
    public function getPaymentTypes(){
        $paymentTypes = array(
            'class' => 'msPayment',
            'select' => ["id", "name", "description"],
            'sortdir' => "ASC",
            'sortby' => "rank",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($paymentTypes);
        $paymentTypes = $this->pdoFetch->run();
        foreach ($paymentTypes as $key=> $type){
            $paymentTypes[$key]["id"] = (int)$paymentTypes[$key]["id"];
        }
        return $paymentTypes;
    }
    //получить способы доставки
    public function getOrderTypes(){
        $orderTypes = array(
            'class' => 'msDelivery',
            'select' => ["id", "name", "description", "logo", "map as isRemoted", "show_on_main", "show_in_modals", "type"],
            'where' => ["active" => true],
            'sortdir' => "ASC",
            'sortby' => "rank",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($orderTypes);
        $orderTypes = $this->pdoFetch->run();
        $msDeliveryMember = array(
            'class' => 'msDeliveryMember',
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($msDeliveryMember);
        $msDeliveryMember = $this->pdoFetch->run();
        foreach ($orderTypes as $key=> $orderType){
            $orderTypes[$key]['id'] = (int)$orderTypes[$key]['id'];
            $orderTypes[$key]['isRemoted'] = (bool)$orderTypes[$key]['isRemoted'];
            $orderTypes[$key]['show_on_main'] = (bool)$orderTypes[$key]['show_on_main'];
            $orderTypes[$key]['show_in_modals'] = (bool)$orderTypes[$key]['show_in_modals'];
        }
        $orderTypes = array_combine(array_column($orderTypes, 'id'), $orderTypes); 
        foreach ($msDeliveryMember as $delivery_member){
            if(array_key_exists("id",$orderTypes[$delivery_member['delivery_id']])) {
                $orderTypes[$delivery_member['delivery_id']]["payment_methods"][] = (int)$delivery_member['payment_id'];
            }
        }
        return array_values($orderTypes);
    }
    public function getAppSettings(){
        if ((int)$this->modx->getOption('app_state_status') == 0){
            $json["app_state_status"] = false; //Приложение активно / не активно.
        } else {
            $json["app_state_status"] = true; //Приложение активно / не активно.
        }
        if ((int)$this->modx->getOption('app_cashback_status') == 0){
            $json["app_cashback_status"] = false; //Статус кешбек системы
        } else {
            $json["app_cashback_status"] = true; //Статус кешбек системы
        }
        if ((int)$this->modx->getOption('app_promocode_status') == 0){
            $json["app_promocode_status"] = false; //Активность показывать ли строку ввода промо-кода
        } else {
            $json["app_promocode_status"] = true; //Активность показывать ли строку ввода промо-кода
        }
        if ((int)$this->modx->getOption('app_referal_status') == 0){
            $json["app_referal_status"] = false; //Активность показывать ли строку "Скидка для друга"
        } else {
            $json["app_referal_status"] = true; //Активность показывать ли строку "Скидка для друга"
        }
        if ((int)$this->modx->getOption('app_reviews_status') == 0){
            $json["app_reviews_status"] = false; //Показывать ли отзывы
        } else {
            $json["app_reviews_status"] = true; //Показывать ли отзывы
        }
        if ((int)$this->modx->getOption('app_org_change') == 0){
            $json["app_org_change"] = false; //Показывать ли точки продаж
        } else {
            $json["app_org_change"] = true; //Показывать ли точки продаж
        }
        if ((int)$this->modx->getOption('app_phone_check') == 0){
            $json["app_phone_check"] = false; //Проверять ли телефон на титульной странице приложения
        } else {
            $json["app_phone_check"] = true; //Проверять ли телефон на титульной странице приложения
        }
        if ((int)$this->modx->getOption('app_modal_type_order') == 0){
            $json["app_modal_type_order"] = false; //Выводить ли модальное с выбором типа подачи
        } else {
            $json["app_modal_type_order"] = true; //Выводить ли модальное с выбором типа подачи
        }
        if ((int)$this->modx->getOption('app_repeat_order') == 0){
            $json["app_repeat_order"] = false; //Давать ли возможность повтороного заказа
        } else {
            $json["app_repeat_order"] = true; //Давать ли возможность повтороного заказа
        }
        if ((int)$this->modx->getOption('app_white_list') == 0){
            $json["app_white_list"] = false; //Использовать ли "белый список"
        } else {
            $json["app_white_list"] = true; //Использовать ли "белый список"
        }
        if ((int)$this->modx->getOption('app_black_list') == 0){
            $json["app_black_list"] = false; //Использовать ли "белый список"
        } else {
            $json["app_black_list"] = true; //Использовать ли "белый список"
        }
        if ((int)$this->modx->getOption('requiredStreetSelection') == 0){
            $json["app_required_street_selection"] = false; //Использовать ли "белый список"
        } else {
            $json["app_required_street_selection"] = true; //Использовать ли "белый список"
        }
        if ((int)$this->modx->getOption('app_delivery_exists') == 0){
            $json["app_delivery_exists"] = false; // не выводить адреса на которые доставка не осуществляется
        } else {
            $json["app_delivery_exists"] = true; // выводить все адреса, даже те на которые доставка не осуществляется
        }
        if ((int)$this->modx->getOption('app_order_evaluation') == 0){
            $json["app_order_evaluation"] = false; // Оценка заказа недоступна
        } else {
            $json["app_order_evaluation"] = true; // Оценка заказа доступна
        }
        if ((int)$this->modx->getOption('app_original_price') == 0){
            $json["app_original_price"] = false; // Вывод оригинальной цены без скидок в корзине
        } else {
            $json["app_original_price"] = true; // Вывод цены со скидками и перерасчётами
        }
        if ((int)$this->modx->getOption('app_hidden_referal') == 0){
            $json["app_hidden_referal"] = false; // Скрыть реферальный экран
        } else {
            $json["app_hidden_referal"] = true; // Не скрывать реферальный экран
        }
        if ((int)$this->modx->getOption('app_item_evaluation') == 0){
            $json["app_item_evaluation"] = false; // Оценка товара отключена
        } else {
            $json["app_item_evaluation"] = true; // Оценка товара включена
        }
        if ((int)$this->modx->getOption('app_gift_ladder') == 0){
            $json["app_gift_ladder"] = false; // Не выводить "Лестницу подарков"
        } else {
            $json["app_gift_ladder"] = true; // Вывести "Лестницу подарков"
        }
        if ((int)$this->modx->getOption('app_bonus_hidden') == 0){
            $json["app_bonus_hidden"] = false; // Отключить вывод бонусов в приложении
        } else {
            $json["app_bonus_hidden"] = true; // Включить вывод бонусов в приложении
        }
        
        $json["app_screen_type"] = $this->modx->getOption('app_screen_type'); // Какой тип экрана будет отображатся в приложении
        $json["app_state_disable_text"] = $this->modx->getOption('app_state_disable_text');//Какой текст вывести если приложение не активно
        //$json["app_cashback_status"] = (int)$this->modx->getOption('app_cashback_status'); //Статус кешбек системы
        //$json["app_promocode_status"] =(int) $this->modx->getOption('app_promocode_status'); //Активность показывать ли строку ввода промо-кода
        //$json["app_referal_status"] = (int)$this->modx->getOption('app_referal_status'); //Активность показывать ли строку "Скидка для друга"
        $json["app_ver_api"] = $this->modx->getOption('app_ver_api');//версия апи
        return $json;
    }
    public function getAppInfo(){
        $sql = "SELECT id,name,price
        FROM modx_ms2_deliveries
        WHERE active = 1 ORDER BY rank ASC";
        $deliveries__str= $this->modx->query($sql);
        $deliveries = $deliveries__str->fetchAll(PDO::FETCH_ASSOC);
        
        
        $sql = "SELECT id,name
        FROM modx_ms2_payments
        WHERE active = 1 ORDER BY rank ASC";
        $payments__str= $this->modx->query($sql);
        $payments = $payments__str->fetchAll(PDO::FETCH_ASSOC);
        
        
        
        $sql = "SELECT value
        FROM modx_site_tmplvar_contentvalues
        WHERE tmplvarid = 10 and contentid = 9";
        $сontacts__str= $this->modx->query($sql);
        $сontacts = $сontacts__str->fetchAll(PDO::FETCH_ASSOC);
        
        $сontacts__all = json_decode($сontacts[0]['value']);
        
        /*
        echo "<pre>";
        print_r($сontacts__all);
        echo "</pre>";
        */
        
        $сontacts__a = [];
        
        $contacts_array = [];
        
        foreach ($сontacts__all as $сontact) {
            $lon = $сontact->lon;
            $lat = $сontact->lat;
           // var_dump($lon);
          //  exit;
            $contacts_array[] = array("id" => $сontact->MIGX_id, "phone" => $this->modx->config['phone'], "address" => $сontact->address, "coord" => array("longitude" => round($lon, 6), "latitude" => round($lat, 6)), "email" => $сontact->email);
            $adresses_array[] = array("id" => $сontact->MIGX_id, "address" => $сontact->address, "coord" => array("longitude" => round($lon, 6), "latitude" => round($lat,6)) );
        }
        
        
        $deliveries__a = [];
        $payments__a = [];
        
        $paymants_array = [];
        
        // foreach ($payments as $payment) {
            
        // $payments__a[] = '
        //             {
        //                 "id":'.$payment['id'].',
        //                 "name":"'.$payment['name'].'"
        //             }';
        
        // $paymants_array[] = $payment;
            
        // }
        
        
        $deliveries_array = [];
        
        // foreach ($deliveries as $deliverie) {
            
        // $deliveries__a[] = '
        //             {
        //                 "id":'.$deliverie['id'].',
        //                 "name":"'.$deliverie['name'].'",
        //                 "price":'.$deliverie['price'].'
        //             }';
            
        // }
        $org_info_card = $this->modx->config['organization_card'];
        
        $org_info_card = str_replace("\t", " ", $org_info_card);
        
        $pieces = explode("
        ", $org_info_card);
        $i = 0;
        $string_org_info = '';
        foreach ($pieces as $piace)
        {
            $string_org_info = $string_org_info.$piace;
            if ($i != 0) $org_info .= ',';
            $org_info .= '"'.$piace.'"';
            $i++;
        }
        
        $info = $this->modx->getObject("modResource",2);
        
        $delivery_settings_array = [];
        
        $delivery_settings_array[] = array("text_disabled" => $this->modx->config['text_delivery_disabled'],
        "worktime_from" => $this->modx->config['delivery_worktime_from'],
        "worktime_to" => $this->modx->config['delivery_worktime_to']);
        $objects = $this->modx->getCollection("AnswerOptions");
        foreach ($objects as $obj){
            $AnswerOptions[] = ($obj->toArray());    
        }
        
        /* ------------ Конструкция для использования плейсхолдера в строке ---------*/
        // получаем параметры со строкой ответа приложению, содержащую плейсхолдер
        $tpl = $this->modx->getOption("app_discount_message_pickup");
        // получаем процент для подстановки в плейсхолдер
        $props = array('percent' => $this->modx->getOption("app_discount_percent"));
        
        // Создание временного чанка
        $uniqid = uniqid();
        $chunk = $this->modx->newObject('modChunk', array('name' => "{tmp}-{$uniqid}"));
        $chunk->setCacheable(false);
        /* ----------- /Конструкция для использования плейсхолдера в строке ---------*/
        
        $cart_info = [
               // "payments" => $payments,
                //"deliveries" => $deliveries,
               // "contacts" => array($contacts_array[0]),
                //"addresses" => $adresses_array,
                "answer_options" => $AnswerOptions,
                "instagram" => $this->modx->config['instagram'],
                "facebook"=> $this->modx->config['facebook'],
                "organization_card" => $pieces,
                "vk" => $this->modx->config['vk'],
                "odnoklassniki" => $this->modx->config['odnoklassniki'],
                "whats" => $this->modx->config['whats'], //ватсап
                "qr_tag" =>$this->modx->config['qr'],
                "phone" =>$this->modx->config['phone'],
                "time_work" => $this->modx->getOption('time_work'),
                "delivery_settings" => $delivery_settings_array,
                "privacy_policy" => $this->modx->config['privacy_policy'], // $info->getTVValue("tvPrivacyPolicy"),//
                "terms_of_use" => $this->modx->config['terms_of_use'],
                "delivery_and_payment" => $info->getTVValue("tvDeliveryAndPayment"),
                "return_warranty" => $this->modx->config['return_warranty'],
                "price_free" => $this->modx->getOption('free_delivery_price'),
                "price_minimal" => $this->modx->getOption('price_minimal'),
                "work_with_us" =>$this->modx->getOption('work_with_us'), //ссылка работать у нас
                'profile_background' => $this->modx->config['profile_background'],
                "payment_urls" => array(
                    "fail_url" => $this->modx->getOption('payment_fail_url'),
                    "success_url" => $this->modx->getOption('payment_success_url'),
                ),
                "discount_message_pickup" => $chunk->process($props, $tpl),
                ];
        return $cart_info;
    }
}