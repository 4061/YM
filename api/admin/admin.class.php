<?php
class Admin extends ModxApi{
    public function login(){
        $mail = $this->modx->request['mail'];
        $pass = $this->modx->request['password'];
        $logindata = array(
          'username' => $mail,   // имя пользователя
          'password' => $pass, // пароль
          'rememberme' => true        // запомнить?
        );
        // сам процесс авторизации
        $response = $this->modx->runProcessor('/security/login', $logindata);
        // проверяем, успешно ли
        if ($response->isError()) {
          // произошла ошибка, например неверный пароль
          $data = $response->getMessage();
        } else {
            $data = true; 
        }
        return $data;
    }
    public function changePromo(){
        $promo_obj = $this->modx->getObject("PromoBase", $this->modx->request["id_promo"]);
        $promo_data = [];
        foreach ($this->modx->request as $key => $item){
             if ($key == "date_end"){
                $date =  date_create($item);
                $date =  date_format($date, 'Y-m-d H:i:s');
                if ($item){
                    $promo_obj->set($key,$date);
                    
                } else {
                    $promo_obj->set($key, NULL);
                }
            } elseif($key == "id_product") {
                if(count($item)) $promo_obj->set($key,$item);//implode(",",$item)
            } elseif ($key == "order_types"){
                $this_str_types = implode(",",$item);
                $promo_obj->set($key,$this_str_types);
            } elseif ($key == "orgs"){
                $this_str_orgs = implode(",",$item);
                $promo_obj->set($key,$this_str_orgs);
            } elseif ($key == "image"){
                if ($item){
                    $promo_obj->set($key,$item);
                }
            }  elseif ($key == "use_count_in_check" || $key == "user_use_count"){
                if ($item){
                    $promo_obj->set($key,1);
                } else {
                    $promo_obj->set($key,99);
                }
            }  elseif ($key == "user_only"){
                if ($item){
                    $promo_obj->set($key,1);
                } else {
                    $promo_obj->set($key,0);
                }
            } elseif ($key == "min_cart_cost") {
                $promo_data['min_cart_cost'] = ((int)$item > 0) ? (int)$item : 0;
                $promo_obj->set($key, $promo_data['min_cart_cost']);
            } else {
                $promo_obj->set($key,$item);
            }
            
        }
        
        $promo_obj->save();
        return 1;
    }
    public function addPhoto(){
        if( isset( $_POST['my_file_upload'] ) ){  

        	// ВАЖНО! тут должны быть все проверки безопасности передавемых файлов и вывести ошибки если нужно
        
        	$uploaddir = './uploads'; // . - текущая папка где находится submit.php
        
        	// cоздадим папку если её нет
        	if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
        
        	$files      = $_FILES; // полученные файлы
        	$done_files = array();
        
        	// переместим файлы из временной директории в указанную
        	foreach( $files as $file ){
        		$file_name = $file['name'];
        		if( move_uploaded_file( $file['tmp_name'], "$uploaddir/$file_name" ) ){
        			$done_files[] = realpath( "$uploaddir/$file_name" );
        		}
        	}
        
        	$data = $done_files ? array('files' => $done_files ) : array('error' => 'Ошибка загрузки файлов.');
        
        	die( $data );
        } 
    }
    public function addPromo(){
        $checkAvailability = false;
        $new_promo = $this->modx->newObject("PromoBase");
        $promo_data = [];
        foreach ($this->modx->request as $key=> $item){
            if ($key != "segments") {
                if ($key == "activity") $activity = true;
                if ($key == "date_end" || $key == "date_start"){
                    $date =  date_create($item);
                    $date =  date_format($date, 'Y-m-d H:i:s');
                    if ($item){
                        $new_promo->set($key,$date);
                    } else {
                        $new_promo->set($key, NULL);
                    }
                } elseif ($key == "use_count_in_check" || $key == "user_use_count"){
                    if ($item){
                        $new_promo->set($key,1);
                    } else {
                        $new_promo->set($key,99);
                    }
                }  elseif ($key == "user_only"){
                    if ($item){
                        $new_promo->set($key,1);
                    }
                } elseif ($key == "image"){
                    $siteUrl = $this->modx->getOption("site_url");
                    $new_promo->set($key,"/".str_replace($siteUrl,"",$item));
                } elseif ($key == "order_types"){
                    $this_str_types = implode(",",$item);
                    $new_promo->set($key,$this_str_types);
                } elseif ($key == "orgs"){
                    $this_str_orgs = implode(",",$item);
                    $new_promo->set($key,$this_str_orgs);
                }  elseif ($key == "min_cart_cost") {
                    $promo_data['min_cart_cost'] = ((int)$item > 0) ? (int)$item : 0;
                    $new_promo->set($key, $promo_data['min_cart_cost']);
                } else {
                    $new_promo->set($key, $item);
                    
                    if($key == "availability" && $item == 2) $checkAvailability = true;
                }
            }
        }
        if (!$activity) $new_promo->set("activity", 0);
        $new_promo->save();
        
        if($checkAvailability) {
            $this->modx->log(1,"ID созданного: ".$new_promo->get("id_promo"));
            
            $this->modx->request["segments"] = implode(",", $this->modx->request["segments"]);
            $this->modx->request["old_segments"] = "";
            $this->modx->request["promo_id"] = $new_promo->get("id_promo");
            
            $this->addSecretPromoToSegment();
        }
        
        return json_encode($this->modx->request);
    }
    public function addGiftFromCostPromo(){
        $new_promo = $this->modx->newObject("PromoBase");
        foreach ($this->modx->request as $key=> $item){
            if ($key == "date_end" || $key == "date_start"){
                $date =  date_create($item);
                $date =  date_format($date, 'Y-m-d H:i:s');
                if ($item){
                    $new_promo->set($key,$date);
                } else {
                    $new_promo->set($key, NULL);
                }
            } elseif ($key == "use_count_in_check" || $key == "user_use_count"){
                if ($item){
                    $new_promo->set($key,1);
                } else {
                    $new_promo->set($key,99);
                }
            } elseif ($key == "order_types"){
                $this_str_types = implode(",",$item);
                $new_promo->set($key,$this_str_types);
            } elseif ($key == "orgs"){
                $this_str_orgs = implode(",",$item);
                $new_promo->set($key,$this_str_orgs);
            } elseif ($key == "id_product" || $key == "products_to_gift"){
                $new_promo->set($key, implode(",",$item));
            } else {
                $new_promo->set($key, $item);
            }
        }
            $new_promo->set('active', 1);
            $new_promo->set('availability', 2);
            $new_promo->set('type', 4);
        $new_promo->save();
        return 1;
    }
    public function removePromo(){
        $promo = $this->modx->getObject("PromoBase", $this->modx->request['id']);
        $promo->remove();
        return $this->modx->request['id'];
    }
    public function changeBonusSystem(){
        foreach ($this->modx->request as $key => $system){
            $USA = $this->modx->getObject('modSystemSetting', $key);
            if (!$system){
                $system = 0;
            }
            $USA->set('value', $system);
            $USA->save();
            $arr[$key] = $system;
        }
        $this->modx->cacheManager->refresh(array('system_settings' => array()));
        return $arr;
    }
    public function changeIntegrationSystem(){
        if ($this->modx->request["true"]){
            $USA = $this->modx->getObject('modSystemSetting', "IIKO");
            $USA->set('value', true);
            $USA->save();
        } else {
            $USA = $this->modx->getObject('modSystemSetting', "IIKO");
            $USA->set('value', false);
            $USA->save();
        }
        $this->modx->cacheManager->refresh(array('system_settings' => array()));
        return $arr;
    }
    public function changePushInfo(){
        $status = $this->modx->getObject("msOrderStatus", $this->modx->request["id"]);
        $status->set("push",$this->modx->request["push"]);
        $status->set("push_title",$this->modx->request["push_title"]);
        $status->set("push_description",$this->modx->request["push_description"]);
        $status->set("name",$this->modx->request["name"]);
        $status->set("description",$this->modx->request["description"]);
        $status->save();
        return 1;
    }
    public function addSecretPromoToSegment(){
        
        $str = $this->modx->request["segments"];
        $old_str = $this->modx->request["old_segments"]; // параметр для удаления старых значений сегмента, список элементов который был ранее
        $promo_id = $this->modx->request["promo_id"];
        
        $this->modx->log(1,"Список: ".$str);
        
        $str = str_replace(", ",",",$str);
        $arr = explode(",",$str);
        
        if($old_str != "") {
            $old_str = explode(",",$old_str);
            
            $old_str = array_diff($old_str,$arr);
        
            $segmentsOld = array(
                'class' => 'SegmentSegments',
                'where' => ["segment_name:IN" => $old_str],
                'sortdir' => "ASC",
                'sortby' => "segment_id",
                'return' => 'data',
                'limit' => 10000
            );
            $this->pdoFetch->setConfig($segmentsOld);
            $segmentsOld = $this->pdoFetch->run();
            $master_ids_old = implode(",",array_column($segmentsOld, 'segment_id'));
            
            $sql = 'DELETE FROM modx_promo_users_secret WHERE id_promo = '.$promo_id.' AND segment_id IN('.$master_ids_old.')';
            $this->modx->query($sql);
            //$this->modx->log(1,$sql);
        }
        
        
        $segments = array(
            'class' => 'SegmentSegments',
            'where' => ["segment_name:IN" => $arr],
            'sortdir' => "ASC",
            'sortby' => "segment_id",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($segments);
        $segments = $this->pdoFetch->run();
        $master_ids = array_column($segments, 'segment_id');
        $users = array(
            'class' => 'SegmentBase',
            'where' => ["segment_id:IN" => $master_ids],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($users);
        $users = $this->pdoFetch->run();
        foreach ($users as $user){
            if (!$res=$this->modx->getObject("PromoUsersSecret", array("id_user"=>$user["user_id"], "id_promo"=>$promo_id))){
                $secret_user = $this->modx->newObject("PromoUsersSecret");
                $secret_user->set("id_user", $user["user_id"]);
                $secret_user->set("id_promo", $promo_id);
                $secret_user->set("segment_id", $user["segment_id"]);
                $secret_user->save();
            }
        }
        return 1;
    }
    public function adminRemoveComment(){
        $id = $this->modx->request["id"];
        $obj = $this->modx->getObject("Rating", $id);
        $obj->remove();
        return $id;
    }
    public function adminChangeComment(){
        $id = $this->modx->request["id_rating"];
        $obj = $this->modx->getObject("Rating", $id);
        $obj->set("active", $this->modx->request["active"]);
        $obj->save();
        return $id;
    }
    public function adminNotyficationAdd(){
        $data = $this->modx->request;
        
        if($data["checkId"]) {
            $notyfication = $this->modx->getObject("SegmentNotifications",$data["checkId"]);
        } else {
            $notyfication = $this->modx->newObject("SegmentNotifications");
            $notyfication->set("date_of_editing",date('Y-m-d H:i:s'));
        }
        
        foreach($data as $key => $notyficationProp){
            //$this->modx->log(1,"параметр: ".$key." - ".$notyficationProp);
            $notyfication->set($key,$notyficationProp);
        };
        
        
        if($notyfication->save()) {
            $newId = $notyfication->get("id");
            
            $output = array(
                "answer" => "add",
                "data" => array(
                    "id" => $newId
                ),
                "message" => "Рассылка успешно создана"
            );
        } else {
            $output = array(
                "answer" => "error",
                "message" => "Ошибка создания рассылки"
            );
        }
        return $output;
    }
    public function adminEditCell(){
        // {"bd":"segment_notifications","callName":"title_notification","elemenetId":"22","newText":"вапвап"}
        (array)$data = $this->modx->request;
        
        //return $data["elemenetId"].' + '.$data["callName"] . " + " . $data["newText"];
        
        $editCall = $this->modx->getObject($data["table"],$data["elemenetId"]);
        switch($data["table"]){
            case "modUser":
                $profile = $editCall->getOne('Profile');
                $data["toggle"] = (!$data["toggle"])? 1 : 0;
                $profile->set($data["callName"],$data["newText"]);
                $profile->save();
            break;
            default:
                if ($data["callName"] == "tvMaxPrice") {
                    $editCall->setTvValue($data["callName"],$data["newText"]);
                } else {
                    $editCall->set($data["callName"],$data["newText"]);
                }
                
            break;
        }
        
        $editCall->save();
        
        $output = array(
            "answer" => "edit",
            "message" => "Поле успешно отредактировано (".$data["elemenetId"].")"
        );
        
        return $output;
    }
    public function adminDeleteRow(){
        // {"bd":"segment_notifications","callName":"title_notification","elemenetId":"22","newText":"вапвап"}
        (array)$data = $this->modx->request;
        
        //return $data["elemenetId"].' + '.$data["callName"] . " + " . $data["newText"];
        
        $editCall = $this->modx->getObject("SegmentNotifications",$data["elemenetId"]);
        $editCall->remove();
        
        $output = array(
            "answer" => "delete",
            "message" => "Удалил строку из таблицы (".$data["elemenetId"].")"
        );
        
        return $output;
    }
    public function adminDesabledRow(){
        // {"bd":"segment_notifications","callName":"title_notification","elemenetId":"22","newText":"вапвап"}
        (array)$data = $this->modx->request;
        
        $editCall = $this->modx->getObject($data["table"],$data["elemenetId"]);
        switch($data["table"]){
            case "modUser":
                $profile = $editCall->getOne('Profile');
                $data["toggle"] = (!$data["toggle"])? 1 : 0;
                $profile->set("blocked",$data["toggle"]);
                $profile->save();
            break;
            case "PolygonsBase":
                $editCall->set("status",$data["toggle"]);
            break;
            default:
                $editCall->set("active",$data["toggle"]);
            break;
        }
        
        $editCall->save();
        
        $message = ($data["toggle"])? "Элемент включён" : "Элемент выключен";
        
        $output = array(
            "answer" => "Видимость",
            "message" => $message." (".$data["elemenetId"].")"
        );
        
        return $output;
    }
    public function adminEditItem(){
        // {"bd":"segment_notifications","callName":"title_notification","elemenetId":"22","newText":"вапвап"}
        (array)$data = $this->modx->request;
        
        $dataMass = array();
        
        $pdoFetch = new pdoFetch($this->modx);
        $dataQuery = array(
            'class' => 'SegmentNotifications',
            'where' => ["id" => $data['id']],
            'leftJoin' => [
                'sSettings' => ['class' => 'SegmentSettings', 'on' => 'sSettings.segment_id = SegmentNotifications.segment_setting_id'],
            ],
            'select' => [
               'SegmentNotifications' => '`SegmentNotifications`.`id`,
                   `SegmentNotifications`.`title_notification`,
                   `SegmentNotifications`.`segment_setting_id`,
                   `SegmentNotifications`.`type_notification`,
                   `SegmentNotifications`.`promo`,
                   `SegmentNotifications`.`only_new_users`,
                   `SegmentNotifications`.`time_minutes`,
                   `SegmentNotifications`.`time_hours`,
                   `SegmentNotifications`.`time_days`,
                   `SegmentNotifications`.`time_months`,
                   `SegmentNotifications`.`time_days_week`,
                   `SegmentNotifications`.`timezone`',
                'sSettings' => '`sSettings`.`segment_id`,
                    `sSettings`.`segment_name`,
                    `sSettings`.`segment_settings`,
                    `sSettings`.`segment_comment`'
            ],
            'sortdir' => "ASC",
            'sortby' => "id",
            'return' => 'data',
            'limit' => 1000
        );
        $pdoFetch->setConfig($dataQuery);
        $dataMass = $pdoFetch->run();
        
        $output = array(
            "answer" => "Редактирование элемента",
            "message" => "Элемент отредактирован",
            "data" => $dataMass
        );
        
        return $output;
    }
    // Добавить шаблон к рассылке
    public function adminTemplateAdd(){
        // {id: "22", template: "0", template_title: "sdfsdf", template_content: "sdfsdfsdf"}
        (array)$data = $this->modx->request;
        
        $where = array(
            "id_notification" => $data["id"],
        );
        
        if(!$template = $this->modx->getCollection("SegmentNotificationsTemplate",$where)){
            $template = $this->modx->newObject("SegmentNotificationsTemplate");
            $template->set("id_notification",$data["id"]);
            $template->set("template_name",$data["template"]);
            $template->set("title",$data["template_title"]);
            $template->set("content",$data["template_content"]);
            
            $template->save();
            $id = $template->get("id");
        } else {
            foreach($template as $templateElem) {
                $templateElem->set("template_name",$data["template"]);
                $templateElem->set("title",$data["template_title"]);
                $templateElem->set("content",$data["template_content"]);
                
                $templateElem->save();
                $id = $templateElem->get("id");
            }
        }
        
        $output = array(
            "answer" => "Шаблон добавлен/отредактирован",
            "message" => "Шаблон добавлен/отредактирован - рассылка ($id)",
        );
        
        return $output;
        
    }
    
    // Добавить шаблон к рассылке
    public function getTemplate(){
        // {id: "22", template: "0", template_title: "sdfsdf", template_content: "sdfsdfsdf"}
        (array)$data = $this->modx->request;
        
        $where = array(
            "id_notification" => $data["id"],
        );
        
        $templates = $this->modx->getCollection("SegmentNotificationsTemplate",$where);
        foreach($templates as $template) {
            $dataMass = array(
                "id" => $template->get("id"),
                "template" => $template->get("template_name"),
                "template_title" => $template->get("title"),
                "template_content" => $template->get("content")
            );
        }
        
        $output = array(
            "answer" => "Шаблон добавлен/отредактирован",
            "message" => "Шаблон добавлен/отредактирован - рассылка ($id)",
            "data" => $dataMass
        );
        
        return $output;
        
    }
    
    // Редактирование системной опции
    public function setOption(){
        // {id: "22", template: "0", template_title: "sdfsdf", template_content: "sdfsdfsdf"}
        $option = $this->modx->request["option"];
        $value = ($this->modx->request["value"])? $this->modx->request["value"] : 0;
        
        $optionObject= $this->modx->getObject('modSystemSetting', $option);
        $optionObject->set("value", $value);
        $optionObject->save();
        
        $output = array(
            "answer" => "Опция изменена",
            "message" => "Опция изменена, новое значение = $value"
        );
        
        return $output;
    }
    
    public function getSegment(){
        $segment_id = $this->modx->request['segment_id'];
        $delivery_type = $this->modx->request['delivery_type'];
        $restaurants = $this->modx->request['restaurants'];
        $tags = $this->modx->request['tag_type'];
        $phone = $this->modx->request['phone'];
        $cities = $this->modx->request['cities'];

        if($cities) {
            $citiesMass = explode(",",$cities);
            $cities = "";
            foreach($citiesMass as $cityId) {
                $city = $this->modx->getObject("modResource",$cityId);
                $cities .= "'".$city->get("pagetitle")."',";
            }
            $cities = mb_substr($cities, 0, -1);
        }
        
        if (!$segment_id){
            $birthday_1 = $this->modx->request['birthday_1'];
            $birthday_2 = $this->modx->request['birthday_2'];
            $min_dob = $this->modx->request['min_dob'];
            $max_dob = $this->modx->request['max_dob'];
            $last_buy1 = $this->modx->request['last_buy1'];
            $last_buy2 = $this->modx->request['last_buy2'];
            $date1 = $this->modx->request['date_1'];
            $date2 = $this->modx->request['date_2'];
            $min_cartcost = $this->modx->request['min_cartcost'];
            $max_cartcost = $this->modx->request['max_cartcost'];
            $gender = $this->modx->request['gender'];
            $max_avg = $this->modx->request['max_avg']; //максимальный средний чек
            $min_avg = $this->modx->request['min_avg']; //минимальный средний чек
            $lastdays = $this->modx->request['days']; //за сколько последних дней брать информацию
            $min_sum = $this->modx->request['min_sum']; //минимальная сумма покупок от (price)
            $max_sum = $this->modx->request['max_sum']; //максимальная сумма покупок до (price)
            $min_count = $this->modx->request['min_count']; //минимальное количество заказов
            $max_count = $this->modx->request['max_count']; //максимальное количество заказов
            $products = $this->modx->request['products'];
            $products = explode(",", $products);
            $product_keys_placeholder = '';
            
            if($min_count == "" || $max_count == "" || $min_count === null || $max_count === null || $min_count > 0 || $max_count > 0 || (($min_count != 0) && ($max_count != 0))){
                
                //$this->modx->log(1,"Я тут");
            
                foreach ($products as $product){
                    if ($product){
                        $product_keys_placeholder = $product_keys_placeholder." AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
                    }
                }
                if($tags) {
                    $tag_keys_placeholder = "AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products,modx_ms2_product_links WHERE modx_ms2_product_links.master = modx_ms2_order_products.product_id AND modx_ms2_product_links.slave IN (".$tags."))";
                }
                if ($delivery_type){
                    $delivery_type_placeholder = "AND modx_ms2_orders.delivery IN ($delivery_type)";
                }
                if ($restaurants){
                    $restaurants_placeholder = "AND modx_ms2_order_addresses.id = modx_ms2_orders.address AND modx_ms2_order_addresses.index IN ($restaurants)";
                }
                if ($cities){
                    $cities_placeholder = "AND modx_ms2_order_addresses.id = modx_ms2_orders.address AND modx_ms2_order_addresses.city IN ($cities)";
                }
                if ($birthday_1){
                    $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
                }
                if ($birthday_2){
                    $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
                }
                if ($min_dob>0 or $max_dob>0){
                    $dobswither = " AND dob <> 0";
                    $this->modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
                }
                if ($min_dob){
                    $min_dob = $min_dob*365*24*60*60;
                    $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
                }
                if ($max_dob){
                    $max_dob = $max_dob*365*24*60*60;
                    $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
                }
                if ($last_buy1){
                    $last_buy1_placeholder = " AND time_end <= DATE_ADD(CURDATE(), INTERVAL -'$last_buy1' DAY)";
                }
                if ($last_buy2){
                    $last_buy2_placeholder = " AND time_end >= DATE_ADD(CURDATE(), INTERVAL -'$last_buy2' DAY)";
                }
                if ($date1){
                    $date1_placeholder = " AND modx_ms2_orders.createdon >= '$date1'";
                }
                if ($date2){
                    $date2_placeholder = " AND modx_ms2_orders.createdon <= '$date2'";
                }
                if ($gender){
                    $gender_placeholder = " AND modx_segment_users.user_id IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
                }
                if ($max_cartcost>0){
                    $max_cartcost_placeholder = "AND modx_ms2_orders.cart_cost<='$max_cartcost'";
                }
                if ($min_cartcost>0){
                    $min_cartcost_placeholder = " AND modx_ms2_orders.cart_cost>='$min_cartcost'";
                }
                if ($min_sum>0){
                    $min_sum_placeholder = " AND ord_sum>='$min_sum'";
                }
                if ($max_sum>0){
                    $max_sum_placeholder = " AND ord_sum<='$max_sum'";
                }
                if ($min_count>0){
                    $min_count_placeholder = " AND ord_count>='$min_count'";
                }
                if ($max_count>0){
                    $max_count_placeholder = " AND ord_count<='$max_count'";
                }
                if ($min_avg>0){
                    $min_avg_placeholder = " AND ord_avg>='$min_avg'";
                }
                if ($max_avg>0){
                    $max_avg_placeholder = " AND ord_avg<='$max_avg'";
                }
                if ($lastdays){
                    $lastdays_placeholder = " AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'$lastdays' DAY)";
                }
                if ($phone){
                    $phone_placeholder = " AND modx_user_attributes.mobilephone = '$phone'";
                }
                $sql = "
                SELECT 
                    modx_segment_users.user_id as user_id,
                    modx_segment_users.user_name as user_name,
                    modx_user_attributes.email as email,
                    modx_user_attributes.mobilephone as mobilephone,
                    modx_segment_users.time_start as time_start,
                    modx_segment_users.time_end as time_end,
                    modx_segment_users.ord_count as ord_count,
                    modx_segment_users.ord_avg as ord_avg,
                    modx_segment_users.ord_sum as ord_sum,
                    modx_ms2_deliveries.name as delivery_name,
                    modx_ms2_orders.address,
                    SUM(modx_ms2_orders.cart_cost) as  ord_sum2
                FROM 
                    modx_segment_users,modx_ms2_orders,modx_user_attributes,modx_ms2_deliveries,modx_ms2_order_addresses
                WHERE 
                    modx_segment_users.user_id = modx_ms2_orders.user_id AND
                    modx_segment_users.user_id = modx_user_attributes.internalKey AND
                    modx_ms2_deliveries.id = modx_ms2_orders.delivery AND 
                    modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>=0 $lastdays_placeholder $date1_placeholder $date2_placeholder $last_buy1_placeholder $last_buy2_placeholder) AND
                    modx_segment_users.user_id IN (Select internalKey FROM modx_user_attributes WHERE id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder)
                    $min_avg_placeholder 
                    $max_avg_placeholder
                    $min_sum_placeholder
                    $max_sum_placeholder
                    $gender_placeholder
                    $min_count_placeholder
                    $max_count_placeholder
                    $product_keys_placeholder
                    $min_cartcost_placeholder
                    $max_cartcost_placeholder
                    $delivery_type_placeholder
                    $restaurants_placeholder
                    $cities_placeholder
                    $tag_keys_placeholder
                    $phone_placeholder
                Group By 
                    modx_segment_users.user_id";
                    
                $info_user= $this->modx->query($sql);
                $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
                    
            } else {
                $sql = "SELECT user_id as id FROM modx_segment_users";
                $buyerQuery = $this->modx->query($sql);
                $buyers = $buyerQuery->fetchAll(PDO::FETCH_ASSOC);
                
                $buyersMass = array(0 => 1);
                foreach($buyers as $buyer){
                  $buyersMass[] = $buyer['id'];
                }
                
                if ($birthday_1){
                    $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
                }
                if ($birthday_2){
                    $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
                }
                if ($min_dob>0 or $max_dob>0){
                    $dobswither = " AND dob <> 0";
                    $this->modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
                }
                if ($min_dob){
                    $min_dob = $min_dob*365*24*60*60;
                    $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
                }
                if ($max_dob){
                    $max_dob = $max_dob*365*24*60*60;
                    $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
                }
                if ($gender){
                    $gender_placeholder = " AND ".$table.".".$idUser." IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
                }
                if ($phone){
                    $phone_placeholder = " AND modx_user_attributes.mobilephone = '$phone'";
                }
                
                $sql = "
                SELECT 
                    modx_user_attributes.internalKey as user_id,
                    modx_user_attributes.fullname as user_name,
                    modx_user_attributes.email as email,
                    modx_user_attributes.mobilephone as mobilephone
                FROM 
                    modx_user_attributes
                WHERE
                    id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder
                    $gender_placeholder
                    $phone_placeholder
                Group By 
                    modx_user_attributes.id";
                    
                $this->modx->log(1,$sql);
                
                $getuser = $this->modx->query($sql);
                $getuser = $getuser->fetchAll(PDO::FETCH_ASSOC);
                
                $info_user = array();
                $keyMass = 0;
                foreach($getuser as $key => $user) {
                    $test .= $user['user_id'].", "; 
                    if(!in_array($user['user_id'],$buyersMass)){
                        $info_user[$keyMass] = $user;
                        $info_user[$keyMass]['ord_count'] = $info_user[$keyMass]['ord_avg'] = $info_user[$keyMass]['ord_sum'] = 0;
                        $keyMass++;
                    }
                }
                //$this->modx->log(1,$test);
            }
            
            return($info_user);
        } else {
            $birthday_1 = $this->modx->request['birthday_1'];
            $birthday_2 = $this->modx->request['birthday_2'];
            $min_dob = $this->modx->request['min_dob'];
            $max_dob = $this->modx->request['max_dob'];
            $last_buy1 = $this->modx->request['last_buy1'];
            $last_buy2 = $this->modx->request['last_buy2'];
            $date1 = $this->modx->request['date_1'];
            $date2 = $this->modx->request['date_2'];
            $min_cartcost = $this->modx->request['min_cartcost'];
            $max_cartcost = $this->modx->request['max_cartcost'];
            $gender = $this->modx->request['gender'];
            $max_avg = $this->modx->request['max_avg']; //максимальный средний чек
            $min_avg = $this->modx->request['min_avg']; //минимальный средний чек
            $lastdays = $this->modx->request['days']; //за сколько последних дней брать информацию
            $min_sum = $this->modx->request['min_sum']; //минимальная сумма покупок от (price)
            $max_sum = $this->modx->request['max_sum']; //максимальная сумма покупок до (price)
            $min_count = $this->modx->request['min_count']; //минимальное количество заказов
            $max_count = $this->modx->request['max_count']; //максимальное количество заказов
            $products = $this->modx->request['products'];
            $products = explode(",", $products);
            $product_keys_placeholder = '';
            foreach ($products as $product){
                if ($product){
                    $product_keys_placeholder = $product_keys_placeholder." AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
                }
            }
            if ($birthday_1){
                $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
            }
            if ($birthday_2){
                $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
            }
            if ($min_dob>0 or $max_dob>0){
                $dobswither = " AND dob <> 0";
                $this->modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
            }
            if ($min_dob){
                $min_dob = $min_dob*365*24*60*60;
                $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
            }
            if ($max_dob){
                $max_dob = $max_dob*365*24*60*60;
                $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
            }
            if ($last_buy1){
                $last_buy1_placeholder = " AND time_end <= DATE_ADD(CURDATE(), INTERVAL -'$last_buy1' DAY)";
            }
            if ($last_buy2){
                $last_buy2_placeholder = " AND time_end >= DATE_ADD(CURDATE(), INTERVAL -'$last_buy2' DAY)";
            }
            if ($date1){
                $date1_placeholder = " AND modx_ms2_orders.createdon >= '$date1'";
            }
            if ($date2){
                $date2_placeholder = " AND modx_ms2_orders.createdon <= '$date2'";
            }
            if ($gender){
                $gender_placeholder = " AND modx_segment_users.user_id IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
            }
            if ($max_cartcost>0){
                $max_cartcost_placeholder = "AND modx_ms2_orders.cart_cost<='$max_cartcost'";
            }
            if ($min_cartcost>0){
                $min_cartcost_placeholder = " AND modx_ms2_orders.cart_cost>='$min_cartcost'";
            }
            if ($min_sum>0){
                $min_sum_placeholder = " AND ord_sum>='$min_sum'";
            }
            if ($max_sum>0){
                $max_sum_placeholder = " AND ord_sum<='$max_sum'";
            }
            if ($min_count>0){
                $min_count_placeholder = " AND ord_count>='$min_count'";
            }
            if ($max_count>0){
                $max_count_placeholder = " AND ord_count<='$max_count'";
            }
            if ($min_avg>0){
                $min_avg_placeholder = " AND ord_avg>='$min_avg'";
            }
            if ($max_avg>0){
                $max_avg_placeholder = " AND ord_avg<='$max_avg'";
            }
            if ($lastdays){
                $lastdays_placeholder = " AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'$lastdays' DAY)";
            }
            if ($phone){
                $phone_placeholder = " AND modx_user_attributes.mobilephone = '$phone'";
            }
            $sql = "
            SELECT 
                modx_segment_users.user_id as user_id,
                modx_segment_users.user_name as user_name,
                modx_user_attributes.email as email,
                modx_user_attributes.mobilephone as mobilephone,
                modx_segment_users.time_start as time_start,
                modx_segment_users.time_end as time_end,
                modx_segment_users.ord_count as ord_count,
                modx_segment_users.ord_avg as ord_avg,
                modx_segment_users.ord_sum as ord_sum,
                modx_ms2_deliveries.name as delivery_name,
                SUM(modx_ms2_orders.cart_cost) as  ord_sum2
            FROM 
                modx_segment_users,modx_ms2_orders,modx_user_attributes,modx_ms2_deliveries
            WHERE 
                modx_segment_users.user_id = modx_ms2_orders.user_id AND
                modx_segment_users.user_id = modx_user_attributes.internalKey AND
                modx_ms2_deliveries.id = modx_ms2_orders.delivery AND 
                modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>=0 $lastdays_placeholder $date1_placeholder $date2_placeholder $last_buy1_placeholder $last_buy2_placeholder) AND
                modx_segment_users.user_id IN (Select internalKey FROM modx_user_attributes WHERE id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder)
                $min_avg_placeholder 
                $max_avg_placeholder
                $min_sum_placeholder
                $max_sum_placeholder
                $gender_placeholder
                $min_count_placeholder
                $max_count_placeholder
                $phone_placeholder
                $product_keys_placeholder
                $min_cartcost_placeholder
                $max_cartcost_placeholder AND
                modx_segment_users.user_id IN (Select user_id FROM modx_segment_base WHERE segment_id = '$segment_id')
            Group By 
                modx_segment_users.user_id";
            
            $info_user= $this->modx->query($sql);
            $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
            return($info_user);
        }
    }
    // получение данных по промокоду
    public function getPromoData(){
        $promoId = $this->modx->request['promo_id'];
        
        $promo = $this->modx->getObject("PromoBase",$promoId);
        
        $output = array(
            "id" => $promoId,
            "code" => $promo->get("code"),
            "availability" => $promo->get("availability"), // доступность
            "user_use_count" => $promo->get("user_use_count"), // количестов использований
            "use_count" => $promo->get("use_count"), // сколько раз можно использовать
            "use_count_in_check" => $promo->get("use_count_in_check"), 
            "date_start" => $promo->get("date_start"), // дата начала использования
            "date_end" => $promo->get("date_end"), // дата окончания использования
            "author_id" => $promo->get("author_id"), // 
            "author_bonus" => $promo->get("author_bonus"),
            "child_discount" => $promo->get("child_discount"),
            "child_discount_rub" => $promo->get("child_discount_rub"),
            "id_product" => $promo->get("id_product"),
            "value" => $promo->get("value"),
            "bonus_count" => $promo->get("bonus_count"),
            "name" => $promo->get("name"),
            "description" => $promo->get("description"),
            "stores" => $promo->get("stores"),
            "image" => $promo->get("image"),
            "url" => $promo->get("url"),
            "activity" => $promo->get("activity"),
            "admin_comment" => $promo->get("admin_comment"),
            "order_types" => $promo->get("order_types"),
            "orgs" => $promo->get("orgs"),
        );
        
        return $output;
    }
    // Добавление нового стикера (маркера)
    public function addMarker(){
        $data = $this->modx->request;
        // получаем стикеры с подобным названием
        $checkId = $this->modx->getCollection("msProduct",array("pagetitle" => $data[0]["value"]));
        // если не создан
        if(!$checkId) {
            // моздаём новый объект
            $newMaeker = $this->modx->newObject("msProduct");
            $newMaeker->set("pagetitle",$data[0]["value"]);
            $newMaeker->set("alias",$data[0]["value"]);
            $newMaeker->set("template",42);
            $newMaeker->set("parent",3505);
            $newMaeker->set("show_in_tree",0);
            $newMaeker->set("published",1);
            $newMaeker->save();
            // получаем его ID
            $idMarker = $newMaeker->get("id");
            $newMaeker = $this->modx->getObject("msProduct",$idMarker);
            // задаём для него опции
            $options = array(
            	'add_color_text' => array($data[1]["value"]), // задаём цвет текста
            	'add_color_bg' => array($data[2]["value"]), // задаём цвет фона
            	'add_id_org' => array(implode(",", $data[3]["value"])), // задаём id ораганизаций (преобразуем массив значений в строку)
            );
            // сохраняем опции
            $newMaeker->set('options', $options);
            $newMaeker->save();
            // положительный ответ в на запрос
            $output = array(
                "answer" => "Новый стикер создан",
                "message" => 'Стикер с названием "'.$data[0]["value"].'" ('.$idMarker.') создан'
            );
        } else {
            // возвращаем ошибку на запрос, если подобный стикер уже создан
            $output = array(
                "answer" => "error",
                "message" => "Стикер с таким названием уже существует!"
            );
        }
        // возвращаем ответ
        return $output;
    }
    
    // получение списка маркеров для товара
    public function getMarkerList(){
        $data = $this->modx->request;
        // получаем список стикеров-русурсов которые принадлежат указанному товару
        $stickerAdd = $this->modx->getCollection("msProductLink",array("master" => $data["id_item"]));
        
        // формируем массив из ID стикеров для данного товара
        foreach($stickerAdd as $stickerObj) {
            if($stickerObj->get("slave") != 0) {
                $stickerAddMass[] = $stickerObj->get("slave");
            }
        }
        
        // получаем массив стикеров
        $stickers = $this->modx->getCollection("msProduct",array("template" => 42));
        // перебераем стикеры
        foreach ($stickers as $stycker) {
            // получаем опцию где хранится id организации к которой он относится (их может быть несколько)
            $option = $this->modx->getObject("msProductOption",array("key" => "add_id_org","product_id" => $stycker->id));
            $orgList = explode(",",$option->value);
            if (in_array($data["id_org"],$orgList)){
                // формируем массив с перечнем стикеров, в котором хранятся названия и организации к которым они относятся
                $stickerMass[$stycker->id] = array(
                    "name" => $stycker->pagetitle,
                    "id_org" => $orgList,
                    "selected" => ""
                );
                // если ID стикера присутствует в списке стикеров указанных у данного товара, то помечаем его выделенным
                if(in_array($stycker->id,$stickerAddMass)) $stickerMass[$stycker->id]["selected"] = "checked";
            }
        }
        $outerList = "";
        foreach($stickerMass as $key => $sticker) {
            $outerList .= '<input type="checkbox" name="stickersList" value="'.$key.'" id="sticker-'.$key.'-'.$data["id_item"].'" '.$sticker["selected"].'><label for="sticker-'.$key.'-'.$data["id_item"].'">'.$sticker["name"].'</label><br>';
        }
        
        if($outerList == "") {
            $output = array(
                "answer" => "Стикеры отсутствуют",
                "message" => "Список стикеров пуст",
                "list" => "Для данного города у товара стикеры отсутствуют",
                "id_item" => $data["id_item"]
            );
        } else {
            $output = array(
                "answer" => "Список стикеров присутствует",
                "message" => "Список стикеров присутствует",
                "list" => $outerList,
                "id_item" => $data["id_item"]
            );
        }

        // возвращаем ответ
        return $output;
    }
    
    // добавляем стикеры к элементу
    public function addStickerToElement(){
        $data = $this->modx->request;
        // получаем коллекцию всех стиккеров для данного товара
        $stickerCollection = $this->modx->getCollection("msProductLink",array("link" => 12,"master" => $data["id_item"]));
        // перебераем их и формируем массив с ID
        foreach($stickerCollection as $sticker) {
            $stickerMass[] = $sticker->slave;
        }

        // если массив с переданными со страницы новыми ID не пустой
        if ($data["id_stickers"] != "") {
            // перебераем присланные элементы
            foreach (explode(",",$data["id_stickers"]) as $idSticker) {
                // проверяем существоание элемента, если его нет, то добавляем новую связь со стикеров
                if(!$this->modx->getObject("msProductLink",array("link" => 12, "master" => $data["id_item"], "slave" => $idSticker))) {
                    $newSticker = $this->modx->newObject("msProductLink");
                    $newSticker->set("master",$data["id_item"]);
                    $newSticker->set("slave",$idSticker);
                    $newSticker->set("link",12);
                    $newSticker->save();
                }
                // удаляем данный стикер из массива всех стикеров что есть у элемента (в дальнейшем по нему будем удалять теч то не выбраны)
                $slaveDelete = array_search($idSticker,$stickerMass);
                unset($stickerMass[$slaveDelete]);
            }
        }
        // перебераем оставшиеся элементы массива из всех стиккеров, удаялем их
        foreach ($stickerMass as $element){
            $deleteObject = $this->modx->getObject("msProductLink",array("link" => 12,"master" => $data["id_item"], "slave" => $element));
            if($deleteObject->remove()) $removeMass[] = $element;
        }
        
        // возвращаем ответ
        return $stickerMass;
    }
    
    // Поиск и вывод списка
    public function getElementsList(){
        $data = $this->modx->request;
        $list = "";
        
        $sql = 'SELECT id,pagetitle FROM modx_site_content WHERE '.$data["search"].' LIKE "%'.$data["text"].'%"';

        // Вариант 1.
        $statement = $this->modx->query($sql);
        $products = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($products as $product) {
            $list .= '<li class="product-list__element" data-elementid="'.$product["id"].'">'.$product["pagetitle"]."</li>";
        }
        
        // возвращаем ответ
        return "<ul>".$list."</ul>";
    }
    // Получение списка пользователей для которых доступен данный промокод
    public function getPromoUserList(){
        
        // порядковый номер
        $num = 1;
        // получаем список пользователей которые воспользовались промокодом
        $userList = $this->modx->getCollection("PromoUsingUsers",array("id_promo" => $this->modx->request["promo_id"]));
        if($userList){
            $list = "<tr><th>№</th><th>ID</th><th>Имя</th><th>Применено</th><th>Общее количество</th></tr>";
            // перебераем пользователей
            foreach($userList as $user) {
                // формируем будущую строку содержащую таблицу пользователей
                $list .= "<tr>";
                // добавляем порядковый номер пользователя
                $list .= "<td>".$num."</td>";
                // получаем ID пользователя
                $userId = $user->get("id_user");
                $list .= "<td>".$userId."</td>";
                // получаем объект профиля пользователя
                $userObject = $this->modx->getObject("modUserProfile",array("internalKey" => $userId));
                // заносим полное имя пользователя в строку с таблицей
                $list .= "<td>".$userObject->fullname."</td>";
                
                // заносим количество применений промокода данным пользователем
                $list .= "<td>".$user->using_count."</td>";
                
                // получаем сам промокод
                $promoObject = $this->modx->getObject("PromoBase",array("id_promo" => $this->modx->request["promo_id"]));
                // получаем общее количество применений
                $list .= "<td>".$promoObject->get("use_count")."</td>";
                $list .= "</tr>";
                $num++;
            }
        } else {
          $list = "Промокод пока никто не использовал";
        }
        
        return $list;
        
    }
    public function noSession($request){
        //session_abort();
        //$_SESSION["test"] = session_id();
        //$_SESSION["tes1"] = 'vals';
        //return session_module_name();
        return $_SESSION;
    }
    public function updateSystem($request){
        $tural = $this->integrations->getTuralActions();
        switch ($request["action"]){
            case "photos":
                    $tural->updatePhotos();
                break;
            case "category":
                    $tural->updateCategories();
                break;
            case "prices":
                    $tural->updatePrices();
                break;
            case "products":
                    $tural->updateProducts();
                break;
        }
    }
    
    // удаляем сегмент через страницу с сегментами пользователей
    public function deleteSegment($request){
        $segmentId = $request["segment_id"];
        
        $segment = $this->modx->getObject("SegmentSegments",array("segment_id" => $segmentId));
        if($segment->remove()){
            $data["message"] = "Сегмент удалён";
            $data["saccess"] = true;
            $data["id"] = $segmentId;
        } else {
            $data["message"] = "Произошла ошибка удаления сегмента";
            $data["saccess"] = false;
            $data["id"] = 0;
        }
        
        return $data;
    }
    
    // удаляем сегмент через страницу с сегментами пользователей
    public function saveOptionsBD($request){

        foreach($request as $key => $systemOpt) {
            //$this->modx->log(1,$key."=".$systemOpt);
            $opt = $this->modx->getObject('modSystemSetting', $key);
            $opt->set('value', $systemOpt);
            $opt->save();
        }
        
        //Чистим кеш
        $this->modx->cacheManager->refresh(array('system_settings' => array()));
        
        $data = array(
            "message" => "Данные сохранены",
            "saccess" => "info"
        );
        
        return $data;
    }
}