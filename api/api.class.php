<?php
include __DIR__.'/cart/cart.class.php';
include __DIR__.'/admin/admin.class.php';
include __DIR__.'/content/content.class.php';
include __DIR__.'/user/user.class.php';
include __DIR__.'/authorization/authorization.class.php';
include __DIR__.'/promotions/promotions.class.php';
include __DIR__.'/organizations/organizations.class.php';
include __DIR__.'/info/info.class.php';
include __DIR__.'/rates/rates.class.php';
include __DIR__.'/courier/courier.class.php';
include __DIR__.'/telegram_debugger/bot.class.php';
include MODX_BASE_PATH."tigrus_bot/tigrus.class.php";
include MODX_BASE_PATH."integrations/integrations.class.php";
class ModxApi {
    
    public $modx;
    public $response;
    public function __construct($modx,$request,$headers)
    {
        $this->modx = $modx;
        $this->integrations = new Integrations($this->modx);
        $this->prime_hill = $this->integrations->getPrimeHillActions(); //для работы с Prime Hill
        $ms2 = $this->modx->getService('miniShop2');
        $ms2->initialize($this->modx->context->key);
        $this->ms2 = $ms2;
        $this->cart = $ms2->cart->get(); //корзина
        $this->status = $ms2->cart->status(); // статус корзины
        $this->modx->request = $request;
        $this->modx->headers = $headers;
        $url = $this->modx->config['site_url'];
        $url = str_replace('//','-',$url);
        $url = str_replace('/','',$url);
        $url = str_replace('-','//',$url);
        $this->modx->url = $url;
        if ($headers["Authorization"]) {
            $this->modx->token = substr($headers["Authorization"], 7);
        } else if ($headers["authorization"]) $this->modx->token = substr($headers["authorization"], 7);
        $this->pdoFetch = new pdoFetch($modx);
       //parent::__construct();
    }
    //записать лог
    public function setLogs($request, $response, $url){
        require_once MODX_CORE_PATH . 'model/modx/modx.class.php';
        //ini_set('display_errors', '1');
        //ini_set('display_startup_errors', '1');
        //error_reporting(E_ALL);
        define('MODX_CORE_PATH', '/core');
        define('MODX_CONFIG_KEY','/config');
         
        // Criteria for foreign Database
        $host = 'localhost';
        $username = 'sjeda_logs';
        $password = 'e*49OEOs';
        $dbname = 'sjeda_logs';
        $port = 3306;
        $charset = 'utf8';
         
        $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
        $xpdo = new xPDO($dsn, $username, $password);
         
        // Test your connection
        //echo $o = ($xpdo->connect()) ? 'Есть контакт' : 'Нихрена не работает';
        $profile = $this->modx->getObject("modUserProfile", ['website' => $this->modx->token]);
        $internalKey = $profile->internalKey;
        $explode_uri = explode("?", $this->modx->uri);
        $request = json_encode($request, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        $response = json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        $request_get = json_encode($_GET);
        $method = $explode_uri[0];
        //return "
        //INSERT INTO `sjeda_project_logs` (`id`, `url`, `method`, `time`, `user_id`, `request_post`, `request_get`, `response`) VALUES (NULL, '$url', '$method', CURRENT_TIMESTAMP, '$internalKey', '$request', '$request_get', '$response')";
        $results = $xpdo->query("
        INSERT INTO `sjeda_project_logs` (`id`, `url`, `method`, `time`, `user_id`, `request_post`, `request_get`, `response`) VALUES (NULL, '$url', '$method', CURRENT_TIMESTAMP, '$internalKey', '$request', '$request_get', '$response')"); 
        $recordCount = $results->rowCount();
        $results = $results->fetchAll(PDO::FETCH_ASSOC);
    }
    //вернуть ответ
    public function getResponse()
    {
        $this->setLogs($this->modx->request, $this->response, $this->modx->url);
        header('Content-Type: application/json');
        //header('HTTP/1.1 401 Unauthorized');
        //ModxApi::HTTPStatus(401);
        //return json_encode($this->response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        return json_encode($this->response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
    public function testrun(){
        $content = new Content($this->modx, $request, $headers);
        return  $content->getParents();
    }
    public function run($uri, $request, $headers)
    {
        
        $this->modx->request = $request;
        $this->modx->headers = $headers;
        $this->modx->uri = $uri;
        $explode_uri = explode("?", $uri);
        $mistake = $this->checkRequiredFields(); //проверяем заполение всех хэдеров/пост полей
        //$mistake = $this->checkOrganisation(); //проверяем существует ли организация
        if ($mistake) return $this->response = $this->returnError($mistake);
        $action = new Cart($this->modx, $request, $headers);
        $admin = new Admin($this->modx, $request, $headers);
        $content = new Content($this->modx, $request, $headers);
        $user_action = new User($this->modx, $request, $headers);
        $user_authorization = new Authorization($this->modx, $request, $headers);
        $promotions = new Promotions($this->modx, $request, $headers);
        $organizations = new Organizations($this->modx, $request, $headers);
        $info = new Info($this->modx, $request, $headers);
        $rates = new Rates($this->modx, $request, $headers);
        $courier = new Courier($this->modx, $request, $headers);
        $bot = new Bot($this->modx, $request, $headers);
        switch ($explode_uri[0]) {
            case '/api/tg/bot':
                $this->response = $bot->run($this->modx->request["id"]);
                break;
            case '/api/admin/noSession':
                $this->response = $admin->noSession($this->modx->request["id"]);
                break;
            case '/api/admin/addGiftFromCostPromo':
                $this->response = $admin->addGiftFromCostPromo($this->modx->request["id"]);
                break;
            case '/api/courier/removeFlight':
                $this->response = $courier->removeFlight($this->modx->request["id"]);
                break;
            case '/api/courier/addFlightStatus':
                $this->response = $courier->addFlightStatus($this->modx->request['name'], $this->modx->request['description'], $this->modx->request['text_color'], $this->modx->request['wrapper_color'], $this->modx->request['final']);
                break;
            case '/api/courier/changeOrderStatus':
                $this->response = $courier->changeOrderStatus($this->modx->request["id"], $this->modx->request['name'], $this->modx->request['description'], $this->modx->request['text_color'], $this->modx->request['wrapper_color'], $this->modx->request['final']);
                break;
            case '/api/courier/changeFlightStatus':
                $this->response = $courier->changeFlightStatus($this->modx->request["id"], $this->modx->request['name'], $this->modx->request['description'], $this->modx->request['text_color'], $this->modx->request['wrapper_color'], $this->modx->request["final"]);
                break;
            case '/api/courier/removeOrderStatus':
                $this->response = $courier->removeOrderStatus($this->modx->request["id"]);
                break;
            case '/api/courier/removeFlightStatus':
                $this->response = $courier->removeFlightStatus($this->modx->request["id"]);
                break;
            case '/api/courier/getOrderStatus':
                $this->response = $courier->getOrderStatus($this->modx->request["id"]);
                break;
            case '/api/courier/getFlightStatus':
                $this->response = $courier->getFlightStatus($this->modx->request["id"]);
                break;
            case '/api/courier/addOrderStatus':
                $this->response = $courier->addOrderStatus($this->modx->request['name'], $this->modx->request['description'], $this->modx->request['text_color'], $this->modx->request['wrapper_color'], $this->modx->request['final']);
                break;
            case '/api/courier/changeOrder':
                $this->response = $courier->changeOrder($this->modx->request['flight_id'], $this->modx->request['order_id'], $this->modx->request['menuindex'], $this->modx->request['status_id']);
                break;
            case '/api/courier/getOrderStatuses':
                $this->response = $courier->getOrderStatuses($this->modx->token);
                break;
            case '/api/courier/getFlightStatuses':
                $this->response = $courier->getFlightStatuses($this->modx->token);
                break;
            case '/api/courier/getFlight':
                $this->response = $courier->getFlight($this->modx->request["flight_id"]);
                break;
            case '/api/courier/getFlightsHistory':
                $this->response = $courier->getFlightsHistory($this->modx->token);
                break;
            case '/api/courier/addFlight':
                $this->response = $courier->addFlight($this->modx->request["orders"],$this->modx->request["courier_id"],$this->modx->request["comment"], $this->modx->request["status_id"]);
                break;
            case '/api/courier/changeFlight':
                $this->response = $courier->changeFlight($this->modx->request["orders"], $this->modx->request['flight_id'], $this->modx->request['status_id'], $this->modx->request['date_end'], $this->modx->request['courier_id'], $this->modx->request['comment']);
                break;
            case '/api/orders/rateOrder':
                $this->response = $rates->rateOrder();
                break;
            case '/api/orders/rateObject':
                $this->response = $rates->rateObject();
                break;
            case '/api/info/getAppInfo':
                $this->response = $info->getAppInfo();
                break;
            case '/api/info/getAppSettings':
                $this->response = $info->getAppSettings();
                break;
            case '/api/info/getOrderTypes':
                $this->response = $info->getOrderTypes();
                break;
            case '/api/info/getPaymentTypes':
                $this->response = $info->getPaymentTypes();
                break;
            case '/api/organizations/getCities':
                $this->response = $organizations->getCities();
                break;
            case '/api/organizations/getCityStreets':
                $this->response = $organizations->getCityStreets();
                break;
            case '/api/organizations/getOrganizations':
                $this->response = $organizations->getOrganizations();
                break;
            case '/api/organizations/getOrganizationByAddress':
                $this->response = $organizations->getOrganizationByAddress();
                break;
            case '/api/organizations/getOrganizationComment':
                $this->response = $organizations->getOrganizationComment();
                break;
            case '/api/promotions/getPromotions':
                //$mistake = $this->checkUser();
                //if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $promotions->getPromotions();
                break;
            case '/api/auth/authorization':
                $this->response = $user_authorization->auth();
                break;
            case '/api/auth/getSms':
                $this->response = $user_authorization->getSms();
                break;
            case '/api/user/setFirebaseToken':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->setFirebaseToken();
                break;
            case '/api/user/getOrdersHistory':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->getOrdersHistory();
                break;
            case '/api/user/getUserOrder':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->getUserOrder();
                break;
            case '/api/user/getUser':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->getUser();
                break;
            case '/api/user/changeUser':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->changeUser();
                break;
            case '/api/user/saveUserAddress':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->saveUserAddress();
                break;
            case '/api/user/changeUserAddress':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->changeUserAddress();
                break;
            case '/api/user/getUserAddresses':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->getUserAddresses();
                break;
            case '/api/user/removeUserAddress':
                $mistake = $this->checkUser();
                if ($mistake) return $this->response = $this->returnError($mistake);
                $this->response = $user_action->removeUserAddress();
                break;
            case '/api/content/getParents':
                $this->response = $content->getParents();
                break;
            case '/api/content/getCartAddivies':
                $this->response = $content->getCartAddivies();
                break;
            case '/api/content/getProduct':
                $this->response = $content->getProduct();
                break;
            case '/api/content/getProductsTest':
                $this->response = $content->getProductsTest();
                break;
            case '/api/content/getProducts':
                $this->response = $content->getProducts();
                break;
            case '/api/content/getCatalog':
                $this->response = $content->getCatalog();
                break;
            case '/api/content/getHomepage':
                $this->response = $content->getHomepage();
                break;
            case '/api/content/getResourceByTags':
                $this->response = $content->getResourceByTags();
                break;
            case '/api/cart/add':
                $result = $action->add($this->modx->request);
                $this->response = $result;
                break;
            case '/api/order/reorder':
                $result = $action->repeatOrder($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/change':
                $result = $action->change($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/promocode':
                $result = $action->promocode($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/clean':
                $result = $action->clean($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/removePromo':
                $result = $action->removePromo($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/getCart':
                $result = $action->getCart($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/createOrder':
                $result = $action->createOrder(false, $this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/checkPayment':
                $result = $action->checkPayment($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/getFreeTime':
                $result = $action->getFreeTime();
                $this->response = $result;
                break;
            case '/api/cart/cartAdditives':
                $result = $action->cartAdditives();
                $this->response = $result;
                break;
            case '/api/cart/getTotalCost':
                $result = $action->getTotalCost();
                $this->response = $result;
                break;
            case '/api/cart/sendAnOrderToTheManager':
                $result = $action->sendAnOrderToTheManager();
                $this->response = $result;
                break;
            case '/api/cart/getGifts':
                $result = $action->getGifts($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/logOut':
                $result = $action->logOut($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/loadCart':
                $result = $action->loadCart($this->modx->request);
                $this->response = $result;
                break;
            case '/api/cart/getOptions':
                $result = $action->getOptions($this->modx->request);
                $this->response = $result;
                break;
            
            //админка
            case '/api/admin/login':
                $result = $admin->login();
                $this->response = $result;
                break;
            case '/api/admin/addPhoto':
                $result = $admin->addPhoto();
                $this->response = $result;
                break;
            case '/api/admin/changePromo':
                $result = $admin->changePromo();
                $this->response = $result;
                break;
            case '/api/admin/addPromo':
                $this->response = $admin->addPromo();
                break;
            case '/api/admin/getSegment':
                $this->response = $admin->getSegment();
                break;
            case '/api/admin/removePromo':
                $this->response = $admin->removePromo();
                break;
            case '/api/admin/changeBonusSystem':
                $this->response = $admin->changeBonusSystem();
                break;
            case '/api/admin/changeIntegrationSystem':
                $this->response = $admin->changeIntegrationSystem();
                break;
            case '/api/admin/changePushInfo':
                $this->response = $admin->changePushInfo();
                break;            
            case '/api/admin/addSecretPromoToSegment':
                $this->response = $admin->addSecretPromoToSegment();
                break;
            case '/api/admin/adminRemoveComment':
                $this->response = $admin->adminRemoveComment();
                break;
            case '/api/admin/adminChangeComment':
                $this->response = $admin->adminChangeComment();
                break;
            case '/api/admin/adminNotyficationAdd':
                $this->response = $admin->adminNotyficationAdd();
                break;
            case '/api/admin/adminEditCell':
                $this->response = $admin->adminEditCell();
                break;
            case '/api/admin/adminDeleteRow':
                $this->response = $admin->adminDeleteRow();
                break;
            case '/api/admin/adminDesabledRow':
                $this->response = $admin->adminDesabledRow();
                break;
            case '/api/admin/adminEditItem':
                $this->response = $admin->adminEditItem();
                break;
            case '/api/admin/adminTemplateAdd':
                $this->response = $admin->adminTemplateAdd();
                break;
            case '/api/admin/getTemplate':
                $this->response = $admin->getTemplate();
                break;
            case '/api/admin/getPromoData':
                $this->response = $admin->getPromoData();
                break;
            case '/api/admin/setOption':
                $this->response = $admin->setOption();
                break;
            case '/api/admin/addMarker':
                $this->response = $admin->addMarker();
                break;
            case '/api/admin/getMarkerList':
                $this->response = $admin->getMarkerList();
                break;
            case '/api/admin/addStickerToElement':
                $this->response = $admin->addStickerToElement();
                break;
            case '/api/admin/getElementsList':
                $this->response = $admin->getElementsList();
                break;
            case '/api/admin/getPromoUserList':
                $this->response = $admin->getPromoUserList();
                break;
            case '/api/primehill/getLoyaltyCard':
                $profile = $this->modx->getObject("modUserProfile", ['website' => $this->modx->token]);
                $this->response = $this->prime_hill->getLoyaltyCard($profile);
                break;
            case '/api/primehill/getBonusScreen':
                $profile = $this->modx->getObject("modUserProfile", ['website' => $this->modx->token]);
                $this->response = $user_action->getBonusScreen($profile);
                break;
            case '/api/admin/updateIntegrationSystem':
                $this->response = $admin->updateSystem($this->modx->request);
                break;
            case '/api/admin/deleteSegment':
                $this->response = $admin->deleteSegment($this->modx->request);
                break;
            case '/api/admin/saveOptionsBD':
                $this->response = $admin->saveOptionsBD($this->modx->request);
                break;
            case '/api/tigrus/sendMessage':
                $Bot = new TigrusBot($this->modx);
                $this->response = $Bot->sendMessage($this->modx->request["id"]);
                break;
            
        }
    }
    //---------ПРОВЕРКА НА ОШИБКИ ЗАПРОСА ------------//
    public function returnError($error){ //возвращает ошибку
        $error_obj = $this->modx->getObject("ResponseErrors", array("result_code" => $error));
        $error_response["result_code"] = (int)$error_obj->result_code;
        $error_response["error_msg"] = $error_obj->error_msg;
        if ($error_obj->error) $error_response["error"] = true;
        return $error_response;
    }
    public function checkRequiredFields(){//проверяет передаваемые в апи поля, возвращает ошибку если чего-то не хватает
        $uri_arr = explode("/", $this->modx->uri);
        if (!$this->modx->headers["x-api-token"]) return 3;
        if ($this->modx->getOption('x-api-token') != $this->modx->headers['x-api-token']) return 2;
        // список методов cart для которых не нужно проверять токен
        $cartMetodsNonToken = array("add","change","promocode","getCart","clean","createOrder","checkPayment","getTotalCost","getUser");
        // список методов user для которых не нужно проверять токен
        $cartMetodsNonUser = array("getUser");
        if ($_SERVER['REQUEST_METHOD'] == "POST" &&
            $uri_arr[2] != "admin" &&
            $uri_arr[2] != "auth" &&
            $uri_arr[2] != "organizations" &&
            $uri_arr[2] != "promotions" &&
            $uri_arr[2] != "courier" &&
            $uri_arr[2] != "info" &&
            ($uri_arr[2] == "cart" && !in_array($uri_arr[3],$cartMetodsNonToken)) &&
            ($uri_arr[2] == "user" && !in_array($uri_arr[3],$cartMetodsNonUser))){
                
            if (!$this->modx->headers["Authorization"] && !$this->modx->headers["authorization"]){
                ModxApi::HTTPStatus(401);
                return 4;
            }
            if ($this->modx->token == ""){
                ModxApi::HTTPStatus(401);
                return 4;
            }
        }
        $cart_additives_uri = explode("?",$uri_arr[3]);
        if ($uri_arr[2] == "cart" && $uri_arr[3] != "getFreeTime" && $cart_additives_uri[0] != "cartAdditives"){ //проверка для корзины
            $required_field_mistake = $this->checkRequiredFieldsInCart();
            if ($required_field_mistake) return $required_field_mistake;
        }
    }
    public function checkRequiredFieldsInCart(){//проверяет передаваемые в апи поля, возвращает ошибку если чего-то не хватает
        if (!$this->modx->request["id_org"]) return 5;
        if (!$this->modx->request["order_type"]) return 6;
    }
    public function checkOrganisation(){
        if (!$this->modx->getObject("modResource", $this->modx->request["id_org"])) return 7; //если организации не существует
    }
    public function checkUser(){
        if (!$profile = $this->modx->getObject('modUserProfile', ['website' => $this->modx->token])){
            ModxApi::HTTPStatus(401);
            return 1;
        }
    }
    //---------ПРОВЕРКА НА ОШИБКИ ЗАПРОСА КОНЕЦ ------------//
    public static function HTTPStatus($num) {
        $http = array(
            100 => 'HTTP/1.1 100 Continue',
            101 => 'HTTP/1.1 101 Switching Protocols',
            200 => 'HTTP/1.1 200 OK',
            201 => 'HTTP/1.1 201 Created',
            202 => 'HTTP/1.1 202 Accepted',
            203 => 'HTTP/1.1 203 Non-Authoritative Information',
            204 => 'HTTP/1.1 204 No Content',
            205 => 'HTTP/1.1 205 Reset Content',
            206 => 'HTTP/1.1 206 Partial Content',
            300 => 'HTTP/1.1 300 Multiple Choices',
            301 => 'HTTP/1.1 301 Moved Permanently',
            302 => 'HTTP/1.1 302 Found',
            303 => 'HTTP/1.1 303 See Other',
            304 => 'HTTP/1.1 304 Not Modified',
            305 => 'HTTP/1.1 305 Use Proxy',
            307 => 'HTTP/1.1 307 Temporary Redirect',
            400 => 'HTTP/1.1 400 Bad Request',
            401 => 'HTTP/1.1 401 Unauthorized',
            402 => 'HTTP/1.1 402 Payment Required',
            403 => 'HTTP/1.1 403 Forbidden',
            404 => 'HTTP/1.1 404 Not Found',
            405 => 'HTTP/1.1 405 Method Not Allowed',
            406 => 'HTTP/1.1 406 Not Acceptable',
            407 => 'HTTP/1.1 407 Proxy Authentication Required',
            408 => 'HTTP/1.1 408 Request Time-out',
            409 => 'HTTP/1.1 409 Conflict',
            410 => 'HTTP/1.1 410 Gone',
            411 => 'HTTP/1.1 411 Length Required',
            412 => 'HTTP/1.1 412 Precondition Failed',
            413 => 'HTTP/1.1 413 Request Entity Too Large',
            414 => 'HTTP/1.1 414 Request-URI Too Large',
            415 => 'HTTP/1.1 415 Unsupported Media Type',
            416 => 'HTTP/1.1 416 Requested Range Not Satisfiable',
            417 => 'HTTP/1.1 417 Expectation Failed',
            500 => 'HTTP/1.1 500 Internal Server Error',
            501 => 'HTTP/1.1 501 Not Implemented',
            502 => 'HTTP/1.1 502 Bad Gateway',
            503 => 'HTTP/1.1 503 Service Unavailable',
            504 => 'HTTP/1.1 504 Gateway Time-out',
            505 => 'HTTP/1.1 505 HTTP Version Not Supported',
        );
     
        header($http[$num]);
    }
}