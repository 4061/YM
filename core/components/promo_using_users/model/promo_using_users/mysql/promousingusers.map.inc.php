<?php
$xpdo_meta_map['PromoUsingUsers']= array (
  'package' => 'promo_using_users',
  'version' => '1.1',
  'table' => 'promo_using_users',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'id_user' => NULL,
    'id_promo' => NULL,
    'using_count' => NULL,
  ),
  'fieldMeta' => 
  array (
    'id_user' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'id_promo' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'using_count' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
  ),
);
