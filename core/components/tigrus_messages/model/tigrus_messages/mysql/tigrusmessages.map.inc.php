<?php
$xpdo_meta_map['TigrusMessages']= array (
  'package' => 'tigrus_messages',
  'version' => '1.1',
  'table' => 'tigrus_messages',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'status' => 0,
  ),
  'fieldMeta' => 
  array (
    'status' => 
    array (
      'dbtype' => 'int',
      'precision' => '1',
      'phptype' => 'integer',
      'null' => false,
      'default' => 0,
    ),
  ),
);
