<?php
// получаем список всех заказов
$orders = $modx->getCollection("msOrder");
// формируем пустой массив для последующих расчётов
$bayersMassAll = $bayersMassDay = $bayersMassLastDay = $bayersMassWeek = $bayersMassLastWeek = $bayersMassMonth = $bayersMassLastMonth = array();
// переменные для подсчёта стоимости заказов
$totalPrices = $totalPricesDay = $totalPricesWeek = $totalPricesMonth = $totalPricesDayLast = $totalPricesWeekLast = $totalPricesMonthLast = 0;
// Переменные для подсчёта количества заказов
$totalCount = $totalCountDay = $totalCountWeek = $totalCountMonth = $totalCountDayLast = $totalCountWeekLast = $totalCountMonthLast = 0;
// переменные расчёта количества товаров
$countItems = $countItemsDay = $countItemsWeek = $countItemsMonth = $countItemsDayLast = $countItemsWeekLast = $countItemsMonthLast = 0;
// переменные для расчёта количества закрытых заказов, отправленных и отменённых
$countNewOrdDay = $countDelivOrdDay = $countCloseOrdDay = $countCanselOrdDay = $countPayOrdDay = $countWaitOrdDay = $countNewOrdWeek = $countDelivOrdWeek = $countCloseOrdWeek = $countCanselOrdWeek = $countPayOrdWeek = $countWaitOrdWeek = $countNewOrdMonth = $countDelivOrdMonth = $countCloseOrdMonth = $countCanselOrdMonth = $countPayOrdMonth = $countWaitOrdMonth = 0;
// дата сейчас и вчера
$dateNow = date("Y-m-d");
$dateYesterday = date("Y-m-d",strtotime(date("Y-m-d") . " - 1 days"));

// Текущая неделя (дата начала и окончания)
$dateWeekStert = date("Y-m-d",strtotime(date("Y-m-d") . " - ".(date("N") - 1)." days"));
$dateWeekEnd = date("Y-m-d",strtotime(date("Y-m-d") . " + ".(7 - date("N"))." days"));
// Прошлая неделя (дата начала и окончания)
$dateLastWeekStert = date("Y-m-d",strtotime(date("Y-m-d") . " - 1 week - ".(date("N") - 1)." days"));
$dateLastWeekEnd = date("Y-m-d",strtotime(date("Y-m-d") . " - 1 week  + ".(7 - date("N"))." days"));

// месяц назад (дата начала и окончания)
$dateMonthStart = date("Y-m-1");
$dateMonthEnd = date("Y-m-t");
// месяц назад (дата начала и окончания)
$dateLastMonthStart = date("Y-m-1",strtotime(date("Y-m-d") . " - 1 month"));
$dateLastMonthEnd = date("Y-m-t",strtotime(date("Y-m-d") . " - 1 month"));

// функция для подсчёта процентов, по наличию обоих нужных нам переменных
function getDinamic($count1,$count2){
    if($count1 != 0 && $count2 != 0) {
        $dinamic = (100 - (($count1/$count2) * 100)) * (-1);
    } else if ($count2 == 0) {
        $dinamic = $count1 * 100;
    } else {
        $dinamic = $count1 * 100 * (-1);
    }
    
    return round($dinamic);
}
// функция для подсчёта количества товаров в заказе
function getCountItems($OrderItems){
    $count = 0;
    foreach($OrderItems as $item){
        $count += $item->get("count");
    }
    return $count;
}

// перебераем заказы
foreach($orders as $order) {
    // получаем дату заказа
    $dateOrder = strtotime($order->get("createdon"));
    // получаем дату изменения заказа (предполагается что меняется и статус)
    $dateUpdate = strtotime($order->get("updatedon"));
    /* ========= Уникальные пользователи всего ========= */
    // Получаем количество всех пользователей
    $bayersMassAll[] = $order->get("user_id");
    // Получаем общее количество цены заказов
    $totalPrices += $order->get("cost");
    // получаем общее количество заказов
    $totalCount++;

    /* ========= Уникальные пользователи за день ========= */
    // получаем уникальных пользователей за сегодняшний день
    if(!in_array($order->get("user_id"),$bayersMassDay) && date("Y-m-d",$dateOrder) == $dateNow) {
        $bayersMassDay[] = $order->get("user_id");
    }
    // получаем другие данные за сегодняшний день
    if(date("Y-m-d",$dateOrder) == $dateNow) {
        $totalPricesDay += $order->get("cost");
        $totalCountDay++;
        $countItemsDay += getCountItems($modx->getCollection("msOrderProduct",array("order_id" => $order->get("id"))));
    }
    // получаем изменённые заказы за день
    if(date("Y-m-d",$dateOrder) == $dateNow) { // изменение - $dateUpdate
        switch($order->get("status")){
            case "1": // Новый
                $countNewOrdDay++;
            break;
            case "2": // Оплачен
                $countPayOrdDay++;
            break;
            case "14": // Ждёт оплаты
                $countWaitOrdDay++;
            break;
            case "3": // в пути
                $countDelivOrdDay++;
            break;
            case "9": // закрыт
                $countCloseOrdDay++;
            break;
            case "4": // отменён
                $countCanselOrdDay++;
            break;
            
        }
    }
    
    // получаем уникальных пользователей за вчерашний день
    if(!in_array($order->get("user_id"),$bayersMassLastDay) && date("Y-m-d",$dateOrder) == $dateYesterday) {
        $bayersMassLastDay[] = $order->get("user_id");
    }
    // получаем другие данные за вчерашний день
    if(date("Y-m-d",$dateOrder) == $dateYesterday) {
        $totalPricesDayLast += $order->get("cost");
        $totalCountDayLast++;
        $countItemsDayLast += getCountItems($modx->getCollection("msOrderProduct",array("order_id" => $order->get("id"))));
    }
    
    /* ========= Уникальные пользователи за неделю ========= */
    // получаем уникальных пользователей за текущую неделю
    if(!in_array($order->get("user_id"),$bayersMassWeek) && (strtotime($dateWeekStert) <= $dateOrder && $dateOrder <= strtotime($dateWeekEnd))) {
        $bayersMassWeek[] = $order->get("user_id");
    }
    // получаем другие данные за текущую неделю
    if((strtotime($dateWeekStert) <= $dateOrder && $dateOrder <= strtotime($dateWeekEnd))) {
        $totalPricesWeek += $order->get("cost");
        $totalCountWeek++;
        $countItemsWeek += getCountItems($modx->getCollection("msOrderProduct",array("order_id" => $order->get("id"))));
    }
    // получаем изменённые заказы за текущую неделю
    if((strtotime($dateWeekStert) <= $dateOrder && $dateOrder <= strtotime($dateWeekEnd))) {
        switch($order->get("status")){
            case "1": // Новый
                $countNewOrdWeek++;
            break;
            case "2": // Оплачен
                $countPayOrdWeek++;
            break;
            case "14": // Ждёт оплаты
                $countWaitOrdWeek++;
            break;
            case "3": // в пути
                $countDelivOrdWeek++;
            break;
            case "9": // закрыт
                $countCloseOrdWeek++;
            break;
            case "4": // отменён
                $countCanselOrdWeek++;
            break;
        }
    }
    
    // получаем уникальных пользователей за прошлую неделю
    if(!in_array($order->get("user_id"),$bayersMassLastWeek) && (strtotime($dateLastWeekStert) <= $dateOrder && $dateOrder <= strtotime($dateLastWeekEnd))) {
        $bayersMassLastWeek[] = $order->get("user_id");
    }
    // получаем другие данные за прошлую неделю
    if((strtotime($dateLastWeekStert) <= $dateOrder && $dateOrder <= strtotime($dateLastWeekEnd))) { // уникальные пользователи !in_array($order->get("user_id"),$bayersMassLastWeek) && 
        $totalPricesWeekLast += $order->get("cost");
        $totalCountWeekLast++;
        $countItemsWeekLast += getCountItems($modx->getCollection("msOrderProduct",array("order_id" => $order->get("id"))));
    }
    
    /* ========= Уникальные пользователи за месяц ========= */
    // получаем уникальных пользователей за текущий месяц
    if(!in_array($order->get("user_id"),$bayersMassMonth) && (strtotime($dateMonthStart) <= $dateOrder && $dateOrder <= strtotime($dateMonthEnd))) {
        $bayersMassMonth[] = $order->get("user_id");
    }
    // получаем другие данные за текущий месяц
    if((strtotime($dateMonthStart) <= $dateOrder && $dateOrder <= strtotime($dateMonthEnd))) {
        $totalPricesMonth += $order->get("cost");
        $totalCountMonth++;
        $countItemsMonth += getCountItems($modx->getCollection("msOrderProduct",array("order_id" => $order->get("id"))));
    }
    // получаем изменённые заказы за текущий месяц
    if((strtotime($dateMonthStart) <= $dateOrder && $dateOrder <= strtotime($dateMonthEnd))) {
        switch($order->get("status")){
            case "1": // Новый
                $countNewOrdMonth++;
            break;
            case "2": // Оплачен
                $countPayOrdMonth++;
            break;
            case "14": // Ждёт оплаты
                $countWaitOrdMonth++;
            break;
            case "3": // в пути
                $countDelivOrdMonth++;
            break;
            case "9": // закрыт
                $countCloseOrdMonth++;
            break;
            case "4": // отменён
                $countCanselOrdMonth++;
            break;
        }
    }
    
    // получаем уникальных пользователей за прошлый месяц
    if(!in_array($order->get("user_id"),$bayersMassLastMonth) && (strtotime($dateLastMonthStart) <= $dateOrder && $dateOrder <= strtotime($dateLastMonthEnd))) {
        $bayersMassLastMonth[] = $order->get("user_id");
    }
    // получаем другие данные за прошлый месяц
    if((strtotime($dateLastMonthStart) <= $dateOrder && $dateOrder <= strtotime($dateLastMonthEnd))) {
        $totalPricesMonthLast += $order->get("cost");
        $totalCountMonthLast++;
        $countItemsMonthLast += getCountItems($modx->getCollection("msOrderProduct",array("order_id" => $order->get("id"))));
    }
}
// общее число заказов (в пути, закрыт, отменён)
$summAllStatusesMonth = $countDelivOrdMonth + $countCloseOrdMonth + $countCanselOrdMonth + $countWaitOrdMonth + $countPayOrdMonth;
$summAllStatusesWeek = $countDelivOrdWeek + $countCloseOrdWeek + $countCanselOrdWeek + $countWaitOrdWeek + $countPayOrdWeek;
$summAllStatusesDay = $countDelivOrdDay + $countCloseOrdDay + $countCanselOrdDay + $countWaitOrdDay + $countPayOrdDay;

$massKey = array("Day","Week","Month");

// Создаём массив для ответа
$output = array(
    "paymentsAll" => count($bayersMassAll)
);
// формируем данные в массиве под день, неделю и месяц
foreach($massKey as $key) {
    $output[$key] = array(
        "bayerCount" => count(${bayersMass.$key}),
        "bayerLast" => count(${bayersMassLast.$key}),
        "dinamic" => getDinamic(count(${bayersMass.$key}),count(${bayersMassLast.$key})),
        "cost" => ${totalPrices.$key},
        "costLast" => ${totalPrices.$key.Last},
        "costDinamic" => getDinamic(${totalPrices.$key},${totalPrices.$key.Last}),
        "count" => ${totalCount.$key},
        "countLast" => ${totalCount.$key.Last},
        "countDinamic" => getDinamic(${totalCount.$key},${totalCount.$key.Last}),
        "items" => ${countItems.$key},
        "itemsLast" => ${countItems.$key.Last},
        "itemsDinamic" => getDinamic(${countItems.$key},${countItems.$key.Last}),
        "countTypeOrders" => array(
            "new" => ${countNewOrd.$key},
            "newPercent" => round((${countNewOrd.$key}/${totalCount.$key}) * 100),
            "newPercentGr" => round((${countNewOrd.$key}/${summAllStatuses.$key}) * 10),
            
            "delivery" => ${countDelivOrd.$key},
            "deliveryPercent" => round((${countDelivOrd.$key}/${totalCount.$key}) * 100),
            "deliveryPercentGr" => round((${countDelivOrd.$key}/${summAllStatuses.$key}) * 10),
            
            "close" => ${countCloseOrd.$key},
            "closePercent" => round((${countCloseOrd.$key}/${totalCount.$key}) * 100),
            "closePercentGr" => round((${countCloseOrd.$key}/${summAllStatuses.$key}) * 10),
            
            "cansel" => ${countCanselOrd.$key},
            "canselPercent" => round((${countCanselOrd.$key}/${totalCount.$key}) * 100),
            "canselPercentGr" => round((${countCanselOrd.$key}/${summAllStatuses.$key}) * 10),
            
            "pay" => ${countPayOrd.$key},
            "payPercent" => round((${countPayOrd.$key}/${totalCount.$key}) * 100),
            "payPercentGr" => round((${countPayOrd.$key}/${summAllStatuses.$key}) * 10),
            
            "wait" => ${countWaitOrd.$key},
            "waitPercent" => round((${countWaitOrd.$key}/${totalCount.$key}) * 100),
            "waitPercentGr" => round((${countWaitOrd.$key}/${summAllStatuses.$key}) * 10),
        ),
    );
}

//print_r($output);
return $output;