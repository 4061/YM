<?php
return array(
    'map' => array(
        'msProductData' => array(
            'fields' => array (
                'quantityinpack' => NULL,
                'length' => NULL,
                'measure' => NULL,
            ),
            'fieldMeta' => array(
                'quantityinpack' => array (
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
                'length' => array (
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                ),
                'measure' => array (
                    'dbtype' => 'varchar',
                    'precision' => '255',
                    'phptype' => 'string',
                    'null' => true,
                )
            ),
        ),
    ),
);