<?php
class SoftjetUserUpdater extends SoftjetUpdater
{
    private $getMethod = "contacts/info/";
    private $addMethod = "contacts/register/";
    private $updateMethod = "contacts/update/";
    private $addToCompanyMethod = "contacts/connecttocompany/";

    /**
     * contacts/info
     * @param $user
     * @return array|bool|mixed|null
     */
    public function getUser($user)
    {
        if (!($phone = $this->getPhone($user))) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no phone");
            return false;
        }
        $user = $this->guzzle->getEntities($this->getMethod . $phone, [
            'headers' => [
                'city' => $this->config['city_id'],
                ]
        ]);
        if ($user)
            return $user;
        return null;
    }

    /**
     * POST contacts/register
     * @param modUser $user
     * @return array|bool|mixed
     */
    public function addUser(modUser $user)
    {
        /**
         * @var modUserProfile $profile
         */
        $profile = $user->getOne('Profile');
        if (!empty($profile->{$this->config["user_field"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User already have uuid with uuid {$profile->{$this->config["user_field"]}}");
            return false;
        }
        if (empty($profile->{$this->config["contact_key"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no phone number");
            return false;
        }
        $contact_key = $profile->{$this->config["contact_key"]};
        $userData = $this->collectUserData($profile);
        $newUser = $this->guzzle->postEntity($this->addMethod . $contact_key, [
            'headers' => [
                'city' => $this->config['city_id'],
            ],
            'json' => $userData
        ]);
        if (!$newUser) {
            return false;
        }
        $profile->set($this->config["user_field"], $newUser[0]['persons'][0]["person_id"]);
        $priceTypeId = $this->getPriceTypeId($newUser[0]['price']["price_type"]);
        $profile->set($this->config["priceType_key"], $priceTypeId);
        $profile->save();
        return $newUser;
    }

    /**
     * POST contacts/update
     * @param modUser $user
     * @return array|bool|mixed
     */
    public function updateUser(modUser $user)
    {
        /**
         * @var modUserProfile $profile
         */
        $profile = $user->getOne('Profile');
        if (empty($profile->{$this->config["user_field"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no uuid");
            return false;
        }
        if (empty($profile->{$this->config["contact_key"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no phone number");
            return false;
        }
        $contact_key = $profile->{$this->config["contact_key"]};
        $userData = $this->collectUserData($profile);
        $updatedUser = $this->guzzle->postEntity($this->updateMethod . $contact_key, [
            'headers' => [
                'city' => $this->config['city_id'],
            ],
            'json' => $userData
        ]);
        if (!$updatedUser) {
            return false;
        }
        $profile->set($this->config["user_field"], $updatedUser[0]['persons'][0]["person_id"]);
        $priceTypeId = $this->getPriceTypeId($updatedUser[0]['price']["price_type"]);
        $profile->set($this->config["priceType_key"], $priceTypeId);
        $profile->save();
        return $updatedUser;
    }

    /**
     * POST contacts/connecttocompany
     * @param modUser $user
     * @param $inn
     * @param $name
     * @return array|bool|mixed
     */
    public function addUserToCompany(modUser $user, $inn, $name)
    {
        /**
         * @var modUserProfile $profile
         */
        $profile = $user->getOne('Profile');
        if (empty($profile->{$this->config["user_field"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no uuid");
            return false;
        }
        if (empty($profile->{$this->config["contact_key"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no phone number");
            return false;
        }
        $contact_key = $profile->{$this->config["contact_key"]};
        $response = $this->guzzle->postEntity($this->addToCompanyMethod . $contact_key, [
            'headers' => [
                'city' => $this->config['city_id'],
            ],
            'json' => [
                'inn' => $inn,
                'name' => $name
            ]
        ]);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function collectUserData(modUserProfile $profile)
    {
        $requiredFileds = ["fullname" => "name", "email" => "email", "gender" => "gender", "dob" => "birth_date"];
        $userData = [];
        foreach ($requiredFileds as $modxFiledKey => $oneSfieldKey){
            if (empty($profile->{$modxFiledKey}))
                continue;
            switch ($modxFiledKey){
                case "gender":
                    switch ($profile->{$modxFiledKey}){
                        case 1:
                            $value = "male";
                            break;
                        case 2:
                            $value = "female";
                            break;
                        default:
                            continue 3;
                    }
                    break;
                case "dob":
                    $value = date("d.m.Y", $profile->dob);
                    break;
                default:
                    $value = $profile->{$modxFiledKey};
            }
            /** @var string $value */
            $userData[$oneSfieldKey] = $value;
        }
        return $userData;
    }

    public function getPhone($user)
    {
        if (gettype($user) == "string")
            return $user;
        if (gettype($user) == "object"){
            switch (get_class($user)){
                case "modUser_mysql":
                    /**
                     * @var modUser $user
                     * @var modUserProfile $profile
                     */
                    $profile = $user->getOne('Profile');
                    if (!empty($profile->{$this->config["contact_key"]}))
                        return $profile->{$this->config["contact_key"]};
                    break;
                case "modUserProfile_mysql":
                    /**
                     * @var modUserProfile $user
                     */
                    if (!empty($user->{$this->config["contact_key"]}))
                        return $user->{$this->config["contact_key"]};
                    break;
            }
        }
        return false;
    }

    public function getPriceTypeId($uuid)
    {
        if ($priceType = $this->modx->getObject('SoftjetsyncPriceTypes', ['uuid' => $uuid]))
            return $priceType->id;
        $priceType = $this->modx->getObject('SoftjetsyncPriceTypes', ['uuid' => $this->config['default_priceType']]);
        return $priceType->id;
    }
}