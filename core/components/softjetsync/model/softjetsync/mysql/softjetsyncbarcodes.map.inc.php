<?php
$xpdo_meta_map['SoftjetsyncBarcodes']= array (
  'package' => 'softjetsync',
  'version' => '1.1',
  'table' => 'softjetsync_barcodes',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'product_id' => NULL,
    'product_uuid' => NULL,
    'barcode' => NULL,
  ),
  'fieldMeta' => 
  array (
    'product_id' => 
    array (
      'dbtype' => 'int',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'index',
    ),
    'product_uuid' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '36',
      'phptype' => 'string',
      'null' => false,
      'index' => 'index',
    ),
    'barcode' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
      'index' => 'index',
    ),
  ),
  'indexes' => 
  array (
    'barcodes_UNIQUE' => 
    array (
      'alias' => 'barcodes_UNIQUE',
      'primary' => false,
      'unique' => true,
      'type' => 'BTREE',
      'columns' => 
      array (
        'barcode' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'modx_softjetsync_barcodes_id_IDX' => 
    array (
      'alias' => 'modx_softjetsync_barcodes_id_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'modx_softjetsync_barcodes_barcode_IDX' => 
    array (
      'alias' => 'modx_softjetsync_barcodes_barcode_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'barcode' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'modx_softjetsync_barcodes_product_id_IDX' => 
    array (
      'alias' => 'modx_softjetsync_barcodes_product_id_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'product_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'modx_softjetsync_barcodes_product_uuid_IDX' => 
    array (
      'alias' => 'modx_softjetsync_barcodes_product_uuid_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'product_uuid' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
);
