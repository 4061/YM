<?php

class SoftjetPriceTypesUpdater extends SoftjetUpdater
{
    protected $method = '/prices/types';
    /**
     * @var array
     */
    protected $remapped;
    public function getPriceTypes(){
        $pdoFetch = new pdoFetch($this->modx);
        $orgs = array(
            'class' => 'modResource',
            'parents' => 1802,
            'where' => ['template' => 18],
            'sortdir' => "ASC",
            //'sortby' => "segment_id",
            'return' => 'data',
            'limit' => 10000
        );
        $pdoFetch->setConfig($orgs);
        $orgs = $pdoFetch->run();
        foreach ($orgs as $org){
            $org = $this->modx->getObject("modResource", $org["id"]);
            $orgPriceTypes = $this->guzzle->getEntities($this->method,['headers' => [
                'city' => $org->getTVValue("iiko_organization_key"),
                ]
            ]);
            foreach ($orgPriceTypes as $priceType){
                $priceTypes[] = $priceType;
            }
        }
        return $priceTypes;
    }
    public function process()
    {
        $pdoFetch = new pdoFetch($this->modx);
        $orgs = array(
            'class' => 'modResource',
            'parents' => 1802,
            'where' => ['template' => 18],
            'sortdir' => "ASC",
            'return' => 'data',
            'limit' => 10000
        );
        $pdoFetch->setConfig($orgs);
        $orgs = $pdoFetch->run();
        foreach ($orgs as $org){
            $org = $this->modx->getObject("modResource", $org["id"]);
            $orgPriceTypes = $this->guzzle->getEntities($this->method,['headers' => [
                'city' => $org->getTVValue("iiko_organization_key"),
                ]
            ]);
            foreach ($orgPriceTypes as $priceType){
                $priceTypes[] = $priceType;
            }
        }
        $priceTypes = array_values(array_combine(array_column($priceTypes, 'id'), $priceTypes));
        
        //$priceTypes = $this->guzzle->getEntities($this->method,['headers' => [
        //    'city' => $this->config['city_id'],
        //    ]
        //]);
        if (empty($priceTypes)) {
            return false;
        }
        $this->getCurrentTypes();
        foreach ($priceTypes as $priceType){
            if (isset($this->remapped[$priceType['id']])){
                $this->updateType($this->remapped[$priceType['id']], $priceType);
            }else{
                $this->addType($priceType);
            }
        }

    }

    public function getCurrentTypes()
    {
        $this->pdo->setConfig(array(
            'class' => 'SoftjetsyncPriceTypes',
            'return' => 'data',
            'limit' => $this->defaultLimit
        ));
        $currentTypes = $this->pdo->run();
        if (empty($currentTypes))
            return [];
        $this->remapped = array_combine(array_column($currentTypes, 'uuid'), $currentTypes);
        return $this->remapped;
    }

    private function updateType($dbPriceType, $priceType)
    {
        if ($dbPriceType['name'] != $priceType['name'] ||
            $dbPriceType['currency'] != $priceType['currency']
        ){
            $objectPrice = $this->modx->getObject('SoftjetsyncPriceTypes', $dbPriceType['id']);
            $objectPrice->fromArray([
                'name' => $priceType['name'],
                'currency' => $priceType['currency'],
            ]);
            $objectPrice->save();
        }
    }

    private function addType($priceType)
    {
        $newType = $this->modx->newObject('SoftjetsyncPriceTypes');
        $newType->fromArray([
            'uuid' => $priceType['id'],
            'name' => $priceType['name'],
            'currency' => $priceType['currency'],
        ]);
        $newType->save();
    }
}
