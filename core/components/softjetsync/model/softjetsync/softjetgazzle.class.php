<?php
use GuzzleHttp\Client;
class SoftjetGazzle
{
    /**
     * @var array
     */
    private $config;
    /**
     * @var Client
     */
    public $client;
    /**
     * @var modX
     */
    private $modx;


    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX &$modx, array $config = [])
    {
        $this->modx = $modx;
        $this->config = array_merge([
            'timeout' => $config['request_timeout'],
            'headers'  => [
                'api_key' => $config['sync_key']
            ]
        ],$config);

        $this->client = new Client([
            'timeout'  => $this->config['timeout'],
            'base_uri'  => $this->config['api_url'],
            'headers'  => $this->config['headers']
        ]);
    }


    /**
     * Получем сущности
     * @param $method
     * @param array $options
     * @return array|bool|mixed
     */
    public function getEntities($method, $options = [])
    {
        try {
            $response = $this->client->request('GET', $this->config['api_path'] . $method, $options);
            if ($response->getStatusCode() == 200){
                if ($data = $this->validateResponse($response))
                    return $data;

            }else{
                $this->modx->log(modX::LOG_LEVEL_INFO, 'Response from api got bad code: ' . $response->getStatusCode());
            }
        } catch (\Throwable $e) {
            $this->modx->log(modX::LOG_LEVEL_INFO, $e->getMessage());
            return [];
        }
        return [];
    }

    /**
     * Создаем сущность
     * @param $method
     * @param array $options
     * @return array|bool|mixed
     */
    public function postEntity($method, $options = [])
    {
        try {
            $response = $this->client->request('POST', $this->config['api_path'] . $method, $options);
            if ($response->getStatusCode() == 200){
                if ($data = $this->validateResponse($response))
                    return $data;

            }else{
                $this->modx->log(modX::LOG_LEVEL_INFO, 'Response from api got bad code: ' . $response->getStatusCode());
            }
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $this->modx->log(modX::LOG_LEVEL_INFO, $e->getMessage());
            return false;
        }
        return false;
    }

    /**
     * Валидируем ответ
     * @param $response
     * @return bool|mixed
     */
    private function validateResponse($response){
        $body = $response->getBody();
        $body = json_decode($body, true);
        if (!$body) {
            $this->modx->log(modX::LOG_LEVEL_INFO, 'Cant parse response to json');
            return false;
        }
        if ($body['status'] === 'Err'){
            foreach ($body['errors'] as $error){
                $this->modx->log(modX::LOG_LEVEL_INFO, $error['text']);
            }
            return false;
        }

        return $body['data'];
    }

}