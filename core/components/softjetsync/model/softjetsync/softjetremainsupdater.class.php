<?php

class SoftjetRemainsUpdater extends SoftjetUpdater
{
    protected $method = '/goods/remains';
    /**
     * @var array|mixed
     */
    private $products;
    /**
     * @var array|bool|string
     */
    private $remapped = [];

    public function updateRemains(){
        $pdoFetch = new pdoFetch($this->modx);
        $orgs = array(
            'class' => 'modResource',
            'parents' => 1802,
            'where' => ['template' => 18],
            'sortdir' => "ASC",
            //'sortby' => "segment_id",
            'return' => 'data',
            'limit' => 10000
        );
        $pdoFetch->setConfig($orgs);
        $orgs = $pdoFetch->run();
        foreach ($orgs as $org){
            $org = $this->modx->getObject("modResource", $org["id"]);
            $orgPriceTypes = $this->guzzle->getEntities($this->method,['headers' => [
                'city' => $org->getTVValue("iiko_organization_key"),
                ]
            ]);
            foreach ($orgPriceTypes as $priceType){
                $product = $this->modx->getObject('modResource', array("alias"=> $priceType["prod"]));
                $priceType['org_id'] = $org->id;
                $priceType['product_id'] = $product->id;
                $priceTypes[] = $priceType;
            }
        }
        $remains = $priceTypes;
        //return $remains;
        foreach ($remains as $key=> $remnant){
            $this->newUpdateRemnant($remnant);
        }
    }
    public function newUpdateRemnant($remnant){
        if (!$updateRemnant = $this->modx->getObject('SoftjetsyncRemains', array("product_id"=>$remnant['product_id']))) $updateRemnant = $this->modx->newObject('SoftjetsyncRemains');
        $updateRemnant->set("remnant", $remnant["remains"]);
        $updateRemnant->set("product_id", $remnant["product_id"]);
        $updateRemnant->set("coming", $remnant["coming"]);
        $updateRemnant->set("id_org", $remnant["org_id"]);
        $updateRemnant->save();
    }
    public function process()
    {
        $remains = $this->guzzle->getEntities($this->method,['headers' => [
            'city' => $this->config['city_id'],
        ]
        ]);
        $prodClass = new SoftjetProductsUpdater($this->guzzle, $this->modx, $this->config);
        $this->products = $prodClass->getProductsFromModx();
        if (empty($remains)) {
            return false;
        }
        $this->getCurrentRemains();
        foreach ($remains as $remnant){
            if (!isset($this->products[$remnant['prod']]))
                continue;
            if (isset($this->remapped[$this->products[$remnant['prod']]['id']])) {
                $this->updateRemnant($this->remapped[$this->products[$remnant['prod']]['id']], $remnant);
            }else{
                $this->addRemnant($this->products[$remnant['prod']], $remnant);
            }
        }

    }

    protected function getCurrentRemains()
    {
        $this->pdo->setConfig(array(
            'class' => 'SoftjetsyncRemains',
            'return' => 'data',
            'limit' => $this->defaultLimit
        ));
        $currentRemains = $this->pdo->run();
        if (empty($currentRemains))
            return [];
        $this->remapped = array_combine(array_column($currentRemains, 'product_id'), $currentRemains);
        return $this->remapped;
    }

    private function updateRemnant($dbRemant, $remnant)
    {
        if ($dbRemant['remant'] != $remnant['remains'] || $dbRemant['coming'] != $remnant['coming']
        ){
            $updateRemnant = $this->modx->getObject('SoftjetsyncRemains', $dbRemant['id']);
            $updateRemnant->fromArray([
                'remant' => $remnant['remains'],
                'coming' => $remnant['coming'],
            ]);
            $updateRemnant->save();
        }
    }

    private function addRemnant($product, $remnant)
    {
        $newRemnant = $this->modx->newObject('SoftjetsyncRemains');
        $newRemnant->fromArray([
            'product_id' => $product['id'],
            'remant' => $remnant['remains'],
            'coming' => $remnant['coming'],
        ]);
        $newRemnant->save();
    }
}