<?php

/**
 * Class SoftjetSync
 * Для доступа к классу, в сниппите достаточно указать:
 * $SoftjetSync = $modx->getService('SoftjetSync','SoftjetSync',MODX_CORE_PATH . 'components/softjetsync/model/');
 */
class SoftjetSync
{
    /** @var modX $modx */
    public $modx;

    public $sjgazzle;
    public $groupUpdater;
    public $productsUpdater;
    public $priceTypesUpdater;
    public $pricesUpdater;
    public $remainsUpdater;
    public $config;
    /**
     * @var SoftjetUserUpdater
     */
    private $userActionsClass;
    private $orderActionsClass;
    public $pdo;
    public static $init = false;

    /**
     * @param modX $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = [])
    {
        $modx->exec("SET SESSION wait_timeout = 1000");
        $this->modx =& $modx;
        $this->pdo = $modx->getService('pdoFetch');
        $corePath = MODX_CORE_PATH . 'components/softjetsync/';
        $assetsUrl = MODX_ASSETS_URL . 'components/softjetsync/';
        
        $this->config = array_merge([
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/',
            'sync_key' => $this->modx->getOption('softjetsync_key'),
            'api_url' => $this->modx->getOption('softjetsync_api_url'),
            'api_path' => $this->modx->getOption('softjetsync_api_path'),

            'tv_sj_id' => $this->modx->getOption('softjetsync_tv_sj_id'),
            'catalog_parent' => $this->modx->getOption('softjetsync_catalog_parent'),
            'modx_category_template_id' => $this->modx->getOption('ms2_template_category_default', null, 0),
            'modx_product_template_id' => $this->modx->getOption('ms2_template_product_default', null,0),
            'city_id' => $this->modx->getOption('softjetsync_city_id', null,''),
            'user_field' => $this->modx->getOption('softjetsync_default_user_field', null,'website'),
            'contact_key' => $this->modx->getOption('softjetsync_contact_key', null,'phone'),
            'priceType_key' => $this->modx->getOption('softjetsync_priceType_key', null,'fax'),
            'request_timeout' => $this->modx->getOption('softjetsync_request_timeout', null,3*60),
            'default_priceType' => 'efaf2a75-1a08-11eb-80d6-005056aebb19',

            'ftp_url' => $this->modx->getOption('softjetsync_ftp_url', null,''),
            'ftp_user' => $this->modx->getOption('softjetsync_ftp_user', null,''),
            'ftp_pass' => $this->modx->getOption('softjetsync_ftp_pass', null,''),

            'connectorUrl' => $assetsUrl . 'connector.php',
            'assetsUrl' => $assetsUrl,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
        ], $config);

        $this->modx->addPackage('softjetsync', $this->config['modelPath']);
        $this->modx->lexicon->load('softjetsync:default');
        $this->init();
    }

    private function init()
    {
        if (self::$init)
            return;
        require_once 'vendor/autoload.php';
        $classes = [
            'softjetupdater',
            'softjetgazzle',
            'softjetuserupdater',
            'softjetgroupupdater',
            'softjetproductsupdater',
            'softjetremainsupdater',
            'softjetpricetypesupdater',
            'softjetpricesupdater',
            'softjetorderupdater'];
        foreach ($classes as $class){
            require_once 'softjetsync/' . $class . '.class.php';
        }
        $this->sjgazzle  = new SoftjetGazzle($this->modx, $this->config);
        self::$init = true;
    }

    public function updateGroups()
    {
        $this->groupUpdater = new SoftjetGroupUpdater($this->sjgazzle, $this->modx, $this->config);
        $this->groupUpdater->process();
    }

    public function updateProducts()
    {
        $this->registerFtpClient();
        $this->productsUpdater = new SoftjetProductsUpdater($this->sjgazzle, $this->modx, $this->config);
        $this->productsUpdater->process();
    }
    public function newUpdateImages(){
        $this->productsUpdater = new SoftjetProductsUpdater($this->sjgazzle, $this->modx, $this->config);
        return $this->productsUpdater;
    }
    public function newUpdateRemains(){
        $this->remainsUpdater = new SoftjetRemainsUpdater($this->sjgazzle, $this->modx, $this->config);
        return $this->remainsUpdater;
    }
    public function updatePriceTypes()
    {
        $this->priceTypesUpdater = new SoftjetPriceTypesUpdater($this->sjgazzle, $this->modx, $this->config);
        $this->priceTypesUpdater->process();
    }
    public function getPriceActions(){
        $this->pricesUpdater = new SoftjetPricesUpdater($this->sjgazzle, $this->modx, $this->config);
        return $this->pricesUpdater;
    }
    public function getPriceTypesActions()
    {
        $this->priceTypesUpdater = new SoftjetPriceTypesUpdater($this->sjgazzle, $this->modx, $this->config); 
        return $this->priceTypesUpdater;
    }
    public function updatePrices()
    {
        $this->pricesUpdater = new SoftjetPricesUpdater($this->sjgazzle, $this->modx, $this->config);
        $this->pricesUpdater->process();
    }
    public function updateRemains()
    {
        $this->remainsUpdater = new SoftjetRemainsUpdater($this->sjgazzle, $this->modx, $this->config);
        $this->remainsUpdater->process();
    }

    /**
     * @return SoftjetUserUpdater
     */
    public function getUserActions()
    {
        if (empty($this->userActionsClass))
            $this->userActionsClass = new SoftjetUserUpdater($this->sjgazzle, $this->modx, $this->config);
        return $this->userActionsClass;
    }

    /**
     * @return SoftjetUserUpdater
     */
    public function getOrderActions()
    {
        if (empty($this->orderActionsClass))
            $this->orderActionsClass = new SoftjetOrderUpdater($this->sjgazzle, $this->modx, $this->config);
        return $this->orderActionsClass;
    }

    public function shuffle(& $source, $class, $newKey, $sourceKey, $classKey, $where = [])
    {
        $this->pdo->setConfig(array(
            'class' => $class,
            'return' => 'data',
            'where' => array_merge($where, [

            ]),
            'limit' => 100000000
        ));
        $objects = $this->pdo->run();
        foreach ($source as &$obj){
            $obj[$newKey] = [];
        }
        if (empty($objects))
            return $source;
        $remapped = array_combine(array_column($source, $sourceKey), array_keys($source));
        foreach ($objects as $object) {
            if (isset($remapped[$object[$classKey]])){
                $source[$remapped[$object[$classKey]]][$newKey][] = $object;
            }
        }
        return $source;
    }

    public function registerFtpClient()
    {
        spl_autoload_register(function($class) {
            if (strstr($class, 'Suin_') !== false) {
                require_once MODX_CORE_PATH . 'components/softjetsync/model/' . str_replace(array('\\', '_'), '/', $class) . '.php';
                return true;
            }
            return false;
        });
    }
}
