<?php

/**
 * The home manager controller for SoftjetSync.
 *
 */
class SoftjetSyncHomeManagerController extends modExtraManagerController
{
    /** @var SoftjetSync $SoftjetSync */
    public $SoftjetSync;


    /**
     *
     */
    public function initialize()
    {
        $this->SoftjetSync = $this->modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/');
        parent::initialize();
    }


    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return ['softjetsync:default'];
    }


    /**
     * @return bool
     */
    public function checkPermissions()
    {
        return true;
    }


    /**
     * @return null|string
     */
    public function getPageTitle()
    {
        return $this->modx->lexicon('softjetsync');
    }


    /**
     * @return void
     */
    public function loadCustomCssJs()
    {
        $this->addCss($this->SoftjetSync->config['cssUrl'] . 'mgr/main.css');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/softjetsync.js');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/misc/utils.js');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/misc/combo.js');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/widgets/items.grid.js');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/widgets/items.windows.js');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/widgets/home.panel.js');
        $this->addJavascript($this->SoftjetSync->config['jsUrl'] . 'mgr/sections/home.js');

        $this->addHtml('<script type="text/javascript">
        SoftjetSync.config = ' . json_encode($this->SoftjetSync->config) . ';
        SoftjetSync.config.connector_url = "' . $this->SoftjetSync->config['connectorUrl'] . '";
        Ext.onReady(function() {MODx.load({ xtype: "softjetsync-page-home"});});
        </script>');
    }


    /**
     * @return string
     */
    public function getTemplateFile()
    {
        $this->content .= '<div id="softjetsync-panel-home-div"></div>';

        return '';
    }
}