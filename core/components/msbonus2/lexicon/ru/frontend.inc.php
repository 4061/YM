<?php

// Ошибки
$_lang['msb2_front_err_required_amount'] = 'Укажите количество бонусов';
$_lang['msb2_front_err_negative_amount'] = 'Нельзя указывать отрицательное число';
$_lang['msb2_front_err_less_points'] = 'Доступных бонусов меньше, чем вы указали';
$_lang['msb2_front_err_cart_empty'] = 'В корзине нет товаров, к которым можно применить бонусы';

// Успехи
$_lang['msb2_front_success_set'] = 'Бонусы применены';
$_lang['msb2_front_success_unset'] = 'Бонусы отменены';