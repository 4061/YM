<?php

// Поля
$_lang['msb2_ms2_field_writeoff'] = 'Оплата бонусами';
$_lang['msb2_ms2_field_points'] = 'Бонусов у пользователя';

// Кнопки
$_lang['msb2_ms2_window_set'] = 'Применить';
$_lang['msb2_ms2_window_unset'] = 'Отменить';

// Ошибки конкретизированные
$_lang['msb2_ms2_err_required_order'] = 'Необходимо указать ID заказа';
$_lang['msb2_ms2_err_required_writeoff'] = 'Укажите количество бонусов';
$_lang['msb2_ms2_err_order_writeoff'] = 'Предположительно, в корзине нет товаров, к которым можно применить бонусы';
$_lang['msb2_ms2_err_order_paid'] = 'После оплаты или отмены заказа нельзя использовать бонусы';

// Сообщения
$_lang['msb2_ms2_message_loading'] = 'Подождите...';
$_lang['msb2_ms2_message_disabled'] = 'После оплаты или отмены заказа нельзя использовать бонусы.';
