<?php
require 'common.inc.php';
require 'setting.inc.php';
require 'frontend.inc.php';
require 'minishop2.inc.php';
require 'logs.inc.php';

// Основное
$_lang['msbonus2'] = 'msBonus2';
$_lang['msb2_menu_desc'] = 'Бонусная система';

// Табы
$_lang['msb2_tab_users'] = 'Пользователи';
$_lang['msb2_tab_levels'] = 'Уровни';
$_lang['msb2_tab_info'] = 'Информация';
$_lang['msb2_tab_action'] = 'Действия с бонусами';
$_lang['msb2_tab_log'] = 'История';

// Названия столбцов
$_lang['msb2_grid_points'] = 'Бонусов';
$_lang['msb2_grid_reserve'] = 'В резерве';
$_lang['msb2_grid_paid_money'] = '<div style="margin: -8px 0 -7px;">Оплачено<br>деньгами</div>';
$_lang['msb2_grid_paid_points'] = '<div style="margin: -8px 0 -7px;">Оплачено<br>бонусами</div>';
$_lang['msb2_grid_bonus'] = 'Бонусы';
$_lang['msb2_grid_cost'] = 'Сумма';
$_lang['msb2_grid_level'] = 'Уровень';
$_lang['msb2_grid_user'] = 'Пользователь';
$_lang['msb2_grid_username'] = 'Логин';
$_lang['msb2_grid_fullname'] = 'Имя';
$_lang['msb2_grid_email'] = 'Email';
$_lang['msb2_grid_mobilephone'] = 'Телефон';
$_lang['msb2_grid_dob'] = '<div style="margin: -8px 0 -7px;">Дата<br>рождения</div>';
$_lang['msb2_grid_order'] = 'Заказ';
$_lang['msb2_grid_log_user'] = 'Кто';
$_lang['msb2_grid_log_amount'] = 'Сколько';
$_lang['msb2_grid_log_action'] = 'За что';
$_lang['msb2_grid_log_createdon'] = 'Когда';
$_lang['msb2_grid_name'] = 'Название';
$_lang['msb2_grid_description'] = 'Описание';
$_lang['msb2_grid_createdon'] = 'Создано';
$_lang['msb2_grid_active'] = 'Вкл';

// Название полей
$_lang['msb2_field_label_points'] = '<span class="msb2-field_display-label">Бонусов:</span>';
$_lang['msb2_field_label_reserve'] = '<span class="msb2-field_display-label">В резерве:</span>';
$_lang['msb2_field_paid_money'] = 'Оплачено деньгами';
$_lang['msb2_field_paid_points'] = 'Оплачено бонусами';
$_lang['msb2_field_bonus'] = 'Бонусы %';
$_lang['msb2_field_cost'] = 'Сумма заказов';
$_lang['msb2_field_level'] = 'Уровень';
$_lang['msb2_field_username'] = 'Логин';
$_lang['msb2_field_fullname'] = 'Имя';
$_lang['msb2_field_email'] = 'Email';
$_lang['msb2_field_mobilephone'] = 'Телефон';
$_lang['msb2_field_dob'] = 'Дата рождения';
$_lang['msb2_field_name'] = 'Название';
$_lang['msb2_field_description'] = 'Описание';
$_lang['msb2_field_createdon'] = 'Создано';
$_lang['msb2_field_active'] = 'Включено';

$_lang['msb2_field_form_type'] = 'Действие';
$_lang['msb2_field_form_cost'] = 'Стоимость заказа';
$_lang['msb2_field_form_points'] = 'Баллов';

// Заголовки
$_lang['msb2_title_form_writeoff'] = 'Списать баллы';
$_lang['msb2_title_form_accrual'] = 'Начислить баллы';

// Заголовки окон
$_lang['msb2_window_'] = '';

// Кнопки
$_lang['msb2_button_form_calculate'] = 'Рассчитать';
$_lang['msb2_button_form_submit'] = 'Отправить';
$_lang['msb2_button_form_writeoff'] = 'Списать';
$_lang['msb2_button_form_accrual'] = 'Начислить';

// Подтверждения
$_lang['msb2_confirm_form_writeoff'] = 'Списать [[+points]] баллов?';
$_lang['msb2_confirm_form_accrual'] = 'Начислить [[+points]] баллов?';

// Ошибки конкретизированные
$_lang['msb2_err_required_name'] = 'Необходимо указать название';
$_lang['msb2_err_required_type'] = 'Укажите действие';
$_lang['msb2_err_required_cost'] = 'Укажите стоимость заказа';
$_lang['msb2_err_required_points'] = 'Укажите количество баллов';
$_lang['msb2_err_unique_name'] = 'Название должно быть уникальным';

// Успехи
$_lang['msb2_success_form_writeoff'] = 'Успешно списано [[+points]] баллов,<br>к оплате [[+cost]] [[%ms2_frontend_currency]]';
$_lang['msb2_success_form_accrual'] = 'Успешно начислено [[+points]] баллов';

// Сообщения
$_lang['msb2_message_'] = '';

// ComboBox
$_lang['msb2_combo_'] = '';

// Другое
$_lang['msb2_'] = '';