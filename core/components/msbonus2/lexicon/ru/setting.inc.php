<?php

/**
 * Areas
 */
$_lang['area_msb2_main'] = 'Основные';
$_lang['area_msb2_usage'] = 'Применение';
$_lang['area_msb2_cart'] = 'Корзина';
$_lang['area_msb2_order'] = 'Заказы';
$_lang['area_msb2_bonus'] = 'Бонусы';
$_lang['area_msb2_backend'] = 'Бек-энд';
$_lang['area_msb2_frontend'] = 'Фронт-энд';

/**
 * Main
 */
$_lang['setting_msb2_form_action_types'] = 'Действия для ручного пополнения/списания';
$_lang['setting_msb2_form_action_types_desc'] = 'Для выбора действия в формах ручного пополнения или списания баллов у пользователя.<br>Указывается в формате JSON. Пример:<br>{"key_1":{"+":"Text for write to balance", "-":"Text for write off balance"}, "key_2":{"+":"Text for write to balance", "-":"Text for write off balance"}}';

/**
 * Main
 */
$_lang['setting_msb2_categories_for_accrual_of_points'] = 'Разделы для начисления баллов';
$_lang['setting_msb2_categories_for_accrual_of_points_desc'] = 'Список ID разделов через запятую, при покупке товаров из которых, баллы на стоимость оных будут начисляться. Пусто = весь каталог.';

$_lang['setting_msb2_categories_for_writing_off_points'] = 'Разделы для списания баллов';
$_lang['setting_msb2_categories_for_writing_off_points_desc'] = 'Список ID разделов через запятую, на товары которых, можно применить накопленные бонусы. Пусто = весь каталог.';

$_lang['setting_msb2_writing_off_with_old_price'] = 'Списывать с товаров со старой ценой';
$_lang['setting_msb2_writing_off_with_old_price_desc'] = 'Отметьте, если баллы нужно списывать с товаров, не зависимо от того, заполнено у них поле old_price или пусто.';

/**
 * Bonus
 */
$_lang['setting_msb2_activation_time_for_bonus'] = 'Время активации баллов после начисления';
$_lang['setting_msb2_activation_time_for_bonus_desc'] = 'Кол-во секунд времени для каждого типа начисления, спустя которое баллы могут начислиться на бонусный счёт пользователя и он сможет их использовать. До этого они будут в резерве.<br>Указывается в формате JSON. Пример:<br>{"order_accrual": 0,"signup": 0,"dob": 0,"offline": 0}';

$_lang['setting_msb2_lifetime_for_bonus'] = 'Время жизни баллов после начисления';
$_lang['setting_msb2_lifetime_for_bonus_desc'] = 'Кол-во секунд времени для каждого типа начисления, спустя которое баллы сгорают на бонусном счёте пользователя.<br><b>Обратите внимание, чтобы значения в данной настройке были выше (либо по нулям), чем в msb2_activation_time_for_bonus!</b><br>Указывается в формате JSON. Пример:<br>{"order_accrual": 0,"signup": 0,"dob": 0,"offline": 0}';

$_lang['setting_msb2_bonus_for_registration'] = 'Бонус за регистрацию';
$_lang['setting_msb2_bonus_for_registration_desc'] = 'Кол-во бонусов, назначаемых пользователю при регистрации.';

$_lang['setting_msb2_bonus_for_birthday'] = 'Бонус за ДР';
$_lang['setting_msb2_bonus_for_birthday_desc'] = 'Кол-во бонусов, назначаемых пользователю в честь дня рождения.';

$_lang['setting_msb2_bonus_for_birthday_days'] = 'Бонус за ДР / Дней для начисления';
$_lang['setting_msb2_bonus_for_birthday_days_desc'] = 'Кол-во дней для начисления бонусов в честь дня рождения. Можно указать отрицательное число.<br>Например:<br><b>2</b> - начислить через два дня после ДР<br><b>-1</b> - начислить за день до ДР<br><br><b>Будьте аккуратны с этой настройкой! Частая смена данной настройки на рабочем проекте может повлечь повторное начисление бонусов некоторым юзерам.</b>';

/**
 * Cart
 */
$_lang['setting_msb2_cart_maximum_percent'] = 'Максимальный процент корзины';
$_lang['setting_msb2_cart_maximum_percent_desc'] = 'Указывается максимальный процент стоимости корзины, который можно оплатить бонусами.';

/**
 * Order
 */
$_lang['setting_msb2_order_status_new'] = 'Статус "Новый"';
$_lang['setting_msb2_order_status_new_desc'] = 'ID статуса заказа, который устанавливается при создании и по которому резервировать баллы до оплаты (списать полностью) или отмены (вернуть).';

$_lang['setting_msb2_order_status_paid'] = 'Статус "Оплачено"';
$_lang['setting_msb2_order_status_paid_desc'] = 'ID статуса заказа, по которому списывать баллы. Обычно это статус "Оплачен".';

$_lang['setting_msb2_order_status_cancel'] = 'Статус "Отменён"';
$_lang['setting_msb2_order_status_cancel_desc'] = 'ID статуса заказа, по которому возвращать баллы. Обычно это статус "Отменён".';

/**
 * Frontend
 */
$_lang['setting_msb2_frontend_js'] = 'JS фронтенда';
$_lang['setting_msb2_frontend_js_desc'] = 'Путь к JS файлу компонента. Если вы хотите использовать собственные скрипты - укажите путь к ним здесь, или очистите параметр и загрузите их вручную через шаблон сайта.';

$_lang['setting_msb2_frontend_css'] = 'CSS фронтенда';
$_lang['setting_msb2_frontend_css_desc'] = 'Путь к CSS файлу компонента. Если вы хотите использовать собственные стили - укажите путь к ним здесь, или очистите параметр и загрузите их вручную через шаблон сайта.';
