<?php

abstract class msb2Plugin
{
    /** @var modX $modx */
    protected $modx;
    /** @var msBonus2 $msb2 */
    protected $msb2;
    /** @var array $sp */
    protected $sp;

    /**
     * @param msBonus2 $msb2
     * @param array    $sp
     */
    public function __construct(msBonus2 &$msb2, array &$sp)
    {
        $this->msb2 = &$msb2;
        $this->modx = &$this->msb2->modx;
        $this->sp = &$sp;
        $this->msb2->initialize($this->modx->context->key);
    }

    // abstract public function run();
}