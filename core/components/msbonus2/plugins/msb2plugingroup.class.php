<?php

/**
 *
 */
class msb2PluginGroup extends msb2Plugin
{
    /**
     * @var msb2Manager $manager
     */
    protected $manager;

    /**
     * @param msBonus2 $msb2
     * @param array    $sp
     */
    public function __construct(msBonus2 &$msb2, array &$sp)
    {
        parent::__construct($msb2, $sp);

        $this->manager = $this->msb2->getManager();
    }

    /**
     * Reload points amount in session
     */
    public function reloadPointsAmount()
    {
        if ($amount = $this->manager->getCartWriteoff()) {
            if (is_numeric($amount)) {
                $this->manager->setCartWriteoff($amount);
            }
        }
    }

    /**
     *
     */
    public function msOnChangeInCart()
    {
        $this->reloadPointsAmount();
    }

    /**
     *
     */
    public function msOnRemoveFromCart()
    {
        $this->reloadPointsAmount();
    }

    /**
     * Unset points amount on cart clean
     */
    public function msOnEmptyCart()
    {
        $this->manager->unsetCartWriteoff();
    }

    /**
     * Remember old costs on before save order
     */
    public function msOnBeforeSaveOrder()
    {
        /** @var msOrder $msOrder */
        $msOrder = &$this->sp['msOrder'];
        $msOrder->set('cart_cost_old', $msOrder->get('cart_cost'));
        $msOrder->set('cost_old', $msOrder->get('cost'));
    }

    /**
     *
     */
    public function msOnSaveOrder()
    {
        /** @var msOrder $msOrder */
        $msOrder = &$this->sp['msOrder'];
        $msb2Order = $this->manager->getJoinedOrder($msOrder);

        //
        if ($msOrder->get('update_products')) {
            if ($writeoff = $msb2Order->get('writeoff')) {
                $msOrder->set('cart_cost', ($msOrder->get('cart_cost') - $writeoff));
                $msOrder->set('cost', ($msOrder->get('cost') - $writeoff));

                // Save
                $msOrder->set('update_products', false);
                $msOrder->save();
                $msOrder->set('update_products', true);
            }

            // Refresh write off points
            $this->manager->refreshOrderWriteoff($msOrder);
        }
    }

    /**
     *
     */
    public function msOnCreateOrder()
    {
        /** @var msOrder $msOrder */
        $msOrder = &$this->sp['msOrder'];
        $msb2Order = $this->manager->getJoinedOrder($msOrder);

        //
        if ($writeoff = $this->manager->getCartWriteoff()) {
            if (is_numeric($writeoff)) {
                $msb2Order->set('writeoff_tmp', $writeoff);
            }
        }

        $msb2Order->save();
    }

    /**
     * Remember old status on before change order status
     */
    public function msOnBeforeChangeOrderStatus()
    {
        /** @var msOrder $msOrder */
        $msOrder = &$this->sp['order'];
        $msOrder->set('status_old', $msOrder->get('status'));
    }

    /**
     *
     */
    public function msOnChangeOrderStatus()
    {
        /** @var msOrder $msOrder */
        $msOrder = &$this->sp['order'];
        $msb2Order = $this->manager->getJoinedOrder($msOrder);

        // Get order status ids
        $status = (int)$msOrder->get('status');
        $status_old = (int)$msOrder->get('status_old') ?: 0;

        // Get other status ids
        $order_status_new = (int)$this->modx->getOption('msb2_order_status_new');
        $order_status_paid = (int)$this->modx->getOption('msb2_order_status_paid');
        $order_status_cancel = (int)$this->modx->getOption('msb2_order_status_cancel');

        /**
         * Status "New"
         */
        if ($status === $order_status_new) {
            // Set write off points
            $writeoff = $this->manager->setOrderWriteoff($msOrder, $msb2Order->get('writeoff_tmp'));
        }

        /**
         * Status "Paid"
         */
        if ($status === $order_status_paid) {
            // переменная для проверки того нужно ли списывать бонусы
            $checkPaymenBonus = true;
            // проверка настройки, будут ли начилсяться бонусы, если мы их списали
            if(!$this->modx->getOption('app_bonus_writing')) {
                //$this->modx->log(1,"ID заказа: ".$msOrder->get('id'));
                // если статус "выключена", то получаем объект заказа с бонусами
                $orderObject = $this->modx->getObject("msb2Order",array("order" => $msOrder->get('id')));
                //$this->modx->log(1,"Списано баллов: ".$orderObject->get("writeoff"));
                // проверяем есть ли списание бонусов в заказе
                if($orderObject->get("writeoff")) {
                    //$this->modx->log(1,"Отменяем начисление баллов");
                    // если есть, то выставляем переменную для проверки в false
                    $checkPaymenBonus = false;
                }
            }
            // проверяем переменную, узнаём можно ли списывать баллы
            if($checkPaymenBonus) {
                // Set accrual points
                $accrual = $this->manager->setOrderAccrual($msOrder);
    
                // Refresh write off points
                $this->manager->refreshOrderWriteoff($msOrder);
            }
            
        }

        /**
         * Status "Cancel"
         */
        if ($status === $order_status_cancel) {
            // Unset write off points
            $this->manager->unsetOrderWriteoff($msOrder);

            // Unset accrual points
            $this->manager->unsetOrderAccrual($msOrder);
        }
    }

    /**
     *
     */
    public function msOnBeforeRemoveOrder()
    {
        /** @var msOrder $msOrder */
        $msOrder = &$this->sp['msOrder'];

        // Unset write off points
        $this->manager->unsetOrderWriteoff($msOrder);

        // Unset accrual points
        $this->manager->unsetOrderAccrual($msOrder);
    }

    /**
     *
     */
    public function msOnManagerCustomCssJs()
    {
        /** @var modManagerController $controller */
        $controller = &$this->sp['controller'];
        $page = $this->sp['page'];

        if ($page === 'orders') {
            $this->msb2->loadManagerScripts($controller, [
                'css/main',
                'js/vendor',
                'js/misc',
                'js/minishop2',
            ]);
        }
    }
}