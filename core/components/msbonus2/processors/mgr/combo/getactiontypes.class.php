<?php

class msb2ComboActionTypeGetListProcessor extends modProcessor
{
    /** @var msBonus2 $msb2 */
    protected $msb2;

    /**
     * @return bool
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        return parent::initialize();
    }

    /**
     * @return string
     */
    public function process()
    {
        $output = [];

        //
        $form = $this->getProperty('form');

        //
        $types = $this->modx->getOption('msb2_form_action_types');
        if ($this->msb2->tools->isJSON($types)) {
            $types = $this->modx->fromJSON($types);
        }
        if (is_array($types)) {
            foreach ($types as $k => $v) {
                if (empty($v[$form])) {
                    continue;
                }
                $output[] = [
                    'display' => $v[$form],
                    'value' => $k,
                ];
            }
        }

        return $this->outputArray($output);
    }

    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('msbonus2:default');
    }
}

return 'msb2ComboActionTypeGetListProcessor';