<?php

class msb2LevelUpdateProcessor extends modObjectUpdateProcessor
{
    public $objectType = 'msb2Level';
    public $classKey = 'msb2Level';
    public $languageTopics = array('msbonus2:default');
    public $permission = 'save';
    /** @var msBonus2 $msb2 */
    protected $msb2;

    /**
     * @return bool
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        return parent::initialize();
    }

    /**
     * @return bool|string
     */
    public function beforeSave()
    {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return parent::beforeSave();
    }

    /**
     * @return bool
     */
    public function beforeSet()
    {
        if (!$id = (int)$this->getProperty('id')) {
            return $this->modx->lexicon('msb2_err_ns');
        }
        if (($tmp = $this->prepareProperties()) !== true) {
            return $tmp;
        }
        unset($tmp);

        // Проверяем на заполненность
        $required = array(
            // 'bonus',
            'name:msb2_err_required_name',
        );
        $this->msb2->tools->checkProcessorRequired($this, $required, 'msb2_err_required');

        // Проверяем на уникальность
        $unique = array(
            'cost',
            'name:msb2_err_unique_name',
        );
        $this->msb2->tools->checkProcessorUnique('', 0, $this, $unique, 'msb2_err_unique');

        return parent::beforeSet();
    }

    /**
     * @return string|bool
     */
    public function prepareProperties()
    {
        $properties = $this->getProperties();
        $this->setProperties($properties);

        return true;
    }
}

return 'msb2LevelUpdateProcessor';