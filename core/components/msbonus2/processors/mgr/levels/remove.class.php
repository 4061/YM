<?php

class msb2LevelRemoveProcessor extends modObjectProcessor
{
    public $objectType = 'msb2Level';
    public $classKey = 'msb2Level';
    public $languageTopics = array('msbonus2:default');
    public $permission = 'remove';

    /**
     * @return array|string
     */
    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

        if ($ids = $this->getProperty('id')) {
            $ids = array($ids);
        } else {
            $ids = $this->modx->fromJSON($this->getProperty('ids'));
            if (empty($ids)) {
                return $this->failure($this->modx->lexicon('msb2_err_ns'));
            }
        }

        foreach ($ids as $id) {
            /** @var msb2Level $object */
            if (!$object = $this->modx->getObject($this->classKey, $id)) {
                return $this->failure($this->modx->lexicon('msb2_err_nf'));
            }
            $object->remove();
        }

        return $this->success();
    }
}

return 'msb2LevelRemoveProcessor';