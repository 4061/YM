<?php

class msb2LevelGetListProcessor extends modObjectGetListProcessor
{
    public $objectType = 'msb2Level';
    public $classKey = 'msb2Level';
    public $defaultSortField = 'cost';
    public $defaultSortDirection = 'ASC';
    public $permission = 'list';

    /**
     * @return boolean|string
     */
    public function initialize()
    {
        return parent::initialize();
    }

    /**
     * @return boolean|string
     */
    public function beforeQuery()
    {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        $this->setProperty('sort', str_replace('_formatted', '', $this->getProperty('sort')));

        return parent::beforeQuery();
    }

    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        //
        if ($query = trim($this->getProperty('query'))) {
            $c->where(array(
                $this->classKey . '.name:LIKE' => "%{$query}%",
                'OR:' . $this->classKey . '.description:LIKE' => "%{$query}%",
            ));
        }

        return $c;
    }

    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();

        //
        $data['cost_formatted'] = number_format($data['cost'], 0, '.', ' ');

        // Buttons
        $data['actions'] = $this->getActions($data);

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function getActions(array $data)
    {
        $actions = array();
        $actions[] = array(
            'cls' => '',
            'icon' => 'icon icon-edit',
            'title' => $this->modx->lexicon('msb2_button_update'),
            'action' => 'updateObject',
            'button' => true,
            'menu' => true,
        );
        if (!in_array($data['id'], [1/*, 2*/], true)) {
            $actions[] = array(
                'cls' => '',
                'icon' => 'icon icon-trash-o action-red',
                'title' => $this->modx->lexicon('msb2_button_remove'),
                'multiple' => $this->modx->lexicon('msb2_button_remove_multiple'),
                'action' => 'removeObject',
                'button' => true,
                'menu' => true,
            );
        }

        return $actions;
    }
}

return 'msb2LevelGetListProcessor';