<?php

class msb2UserOrderDoItProcessor extends modProcessor
{
    /**
     * @var msBonus2 $msb2
     */
    protected $msb2;
    /**
     * @var miniShop2 $ms2
     */
    protected $ms2;
    /**
     * @var msb2Manager $manager
     */
    protected $manager;

    /**
     * @return bool
     */
    public function initialize()
    {
        //
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        //
        $this->ms2 = $this->msb2->getMiniShop2();
        if (empty($this->ms2->cart)) {
            $this->ms2->loadServices();
        }

        //
        $this->manager = $this->msb2->getManager();

        return parent::initialize();
    }

    /**
     * @return string
     */
    public function process()
    {
        $action = preg_replace('/^.+\/([a-z]+)$/', '$1', $this->getProperty('action', ''));
        if (empty($action)) {
            return $this->msb2->tools->failure('msb2_err_unexpected');
        }

        //
        $msOrder = $this->manager->getOrder((int)$this->getProperty('order', 0));
        $msb2Order = $this->manager->getJoinedOrder($msOrder);
        if (empty($msOrder) || empty($msb2Order)) {
            return $this->msb2->tools->failure('msb2_ms2_err_required_order');
        }

        /** @var int|float $writeoff */
        $writeoff = (float)$this->getProperty('writeoff', '');
        if ($action === 'set' && empty($writeoff)) {
            return $this->msb2->tools->failure('msb2_ms2_err_required_writeoff');
        }

        //
        $data = [
            'order' => null,
            'writeoff' => $writeoff,
            'points' => 0,
            'status' => ($action === 'set'),
            'disabled' => false,
        ];

        switch ($action) {
            /**
             * Get points of order
             */
            case 'get':
                // Check order paid
                if ($msb2Order->get('cost') || $this->manager->isOrderStatus($msOrder, 'cancel')) {
                    $data['disabled'] = true;
                }

                if ($data['writeoff'] = $msb2Order->get('writeoff')) {
                    $data['status'] = true;
                }
                break;

            /**
             * Set points to order
             */
            case 'set':
                // Check order paid
                if ($msb2Order->get('cost') || $this->manager->isOrderStatus($msOrder, 'cancel')) {
                    return $this->msb2->tools->failure('msb2_ms2_err_order_paid');
                }

                // Unset write off points
                $this->manager->unsetOrderWriteoff($msOrder);

                // Set write off points
                $writeoff = $this->manager->setOrderWriteoff($msOrder, $writeoff);
                if (empty($writeoff)) {
                    return $this->msb2->tools->failure('msb2_ms2_err_order_writeoff');
                }
                $data['writeoff'] = $writeoff;
                break;

            /**
             * Unset points from order
             */
            case 'unset':
                // Check order paid
                if ($msb2Order->get('cost') || $this->manager->isOrderStatus($msOrder, 'cancel')) {
                    return $this->msb2->tools->failure('msb2_ms2_err_order_paid');
                }

                // Unset write off points
                $this->manager->unsetOrderWriteoff($msOrder);

                //
                $msb2Order = $this->manager->getJoinedOrder($msOrder);
                $msb2Order->set('writeoff_tmp', 0);
                $msb2Order->save();
                break;
        }

        //
        $data['points'] = $this->manager->getUserPoints($msOrder->get('user_id'));

        // Get order data
        if ($msOrder = $this->manager->getOrder($msOrder)) {
            $data['order'] = $msOrder->toArray();
        }

        //
        return $this->modx->toJSON([
            'success' => true,
            'object' => $data,
        ]);
    }

    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('msbonus2:default');
    }
}

return 'msb2UserOrderDoItProcessor';