<?php

class msb2UserFormDoItProcessor extends modProcessor
{
    /**
     * @var msBonus2 $msb2
     */
    protected $msb2;
    /**
     * @var miniShop2 $ms2
     */
    protected $ms2;
    /**
     * @var msb2Manager $manager
     */
    protected $manager;

    /**
     * @return bool
     */
    public function initialize()
    {
        //
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        //
        $this->ms2 = $this->msb2->getMiniShop2();
        if (empty($this->ms2->cart)) {
            $this->ms2->loadServices();
        }

        //
        $this->manager = $this->msb2->getManager();

        return parent::initialize();
    }

    /**
     * @return string
     */
    public function process()
    {
        $action = preg_replace('/^.+\/([a-z]+)$/', '$1', $this->getProperty('action', ''));
        if (empty($action)) {
            return $this->msb2->tools->failure('msb2_err_unexpected');
        }

        //
        $form = $this->getProperty('form', '');
        $msb2User = $this->manager->getJoinedUser((int)$this->getProperty('user', 0));
        if (empty($msb2User) || empty($form)) {
            return $this->msb2->tools->failure('msb2_err_unexpected');
        }

        //
        $type = $this->getProperty('type', '');
        if (empty($type)) {
            return $this->msb2->tools->failure('msb2_err_required_type');
        }

        /** @var int|float $cost */
        $cost = (float)$this->getProperty('cost', 0);

        //
        $message = null;
        $data = [
            'user' => [
                'points' => 0,
                'reserve' => 0,
                'paid_money' => 0,
                'paid_points' => 0,
            ],
            'points' => null,
        ];

        switch ($action) {
            /**
             * Calculate points from order cost
             */
            case 'calculate':
                if (empty($cost)) {
                    return $this->msb2->tools->failure('msb2_err_required_cost');
                }

                switch ($form) {
                    case '-':
                        $user_points = $this->manager->getUserPoints($msb2User->get('user'));
                        $data['points'] = $this->manager->getOrderWriteoffAmount($cost, $user_points);
                        break;

                    case '+':
                        $data['points'] = $this->manager->getOrderAccrualAmount($msb2User->get('user'), $cost);
                        break;
                }
                break;

            /**
             * Submit points to user
             */
            case 'submit':
                /** @var int|float $points */
                $points = (float)$this->getProperty('points', '');
                if (empty($points)) {
                    return $this->msb2->tools->failure('msb2_err_required_points');
                }
                $user_points = $this->manager->getUserPoints($msb2User->get('user'));

                switch ($form) {
                    case '-':
                        if ($points > $user_points) {
                            $points = $user_points;
                        }
                        $data['cost'] = $cost - $points;
                        if ($data['cost'] < 0) {
                            $data['cost'] = 0;
                        }

                        //
                        $paid_points = $msb2User->get('paid_points');
                        $msb2User->set('paid_points', ($paid_points + $points));
                        $msb2User->save();

                        //
                        $this->manager->setMinus(
                            $type,
                            $points,
                            $msb2User->get('user'),
                            0,
                            0,
                            true
                        );

                        //
                        $message = $this->msb2->tools->getChunk(
                            ('@INLINE ' . $this->modx->lexicon('msb2_success_form_writeoff')), [
                                'points' => $points,
                                'cost' => $data['cost'],
                            ]);
                        break;

                    case '+':
                        //
                        $paid_money = $msb2User->get('paid_money');
                        $msb2User->set('paid_money', ($paid_money + $cost));
                        $msb2User->save();

                        //
                        $this->manager->setPlus(
                            $type,
                            $points,
                            $msb2User->get('user'),
                            0,
                            0,
                            true
                        );

                        //
                        $message = $this->msb2->tools->getChunk(
                            ('@INLINE ' . $this->modx->lexicon('msb2_success_form_accrual')), [
                            'points' => $points,
                            'cost' => $cost,
                        ]);
                        break;
                }
                break;
        }

        //
        if ($msb2User = $this->manager->getJoinedUser((int)$msb2User->get('user'))) {
            $data['user']['points'] = $this->manager->getUserPoints($msb2User->get('user'));
            $data['user']['reserve'] = $msb2User->get('reserve');
            $data['user']['paid_money'] = $msb2User->get('paid_money');
            $data['user']['paid_points'] = $msb2User->get('paid_points');

            foreach ($data['user'] as &$v) {
                $v = number_format($v, 0, '.', ' ');
            }
            unset($v);
        }

        //
        return $this->modx->toJSON([
            'success' => true,
            'message' => $message,
            'object' => $data,
        ]);
    }

    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('msbonus2:default', 'minishop2:default');
    }
}

return 'msb2UserFormDoItProcessor';