<?php

class msb2UserGetListProcessor extends modObjectGetListProcessor
{
    public $objectType = 'msb2User';
    public $classKey = 'msb2User';
    public $defaultSortField = 'createdon';
    public $defaultSortDirection = 'DESC';
    public $permission = 'view_user';

    /**
     * @return boolean|string
     */
    public function initialize()
    {
        return parent::initialize();
    }

    /**
     * @return boolean|string
     */
    public function beforeQuery()
    {
        $this->setProperty('sort', str_replace('_formatted', '', $this->getProperty('sort')));

        return parent::beforeQuery();
    }

    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        //
        $c->innerJoin('modUser', 'User', 'User.id = msb2User.user');
        $c->innerJoin('modUserProfile', 'Profile', 'Profile.internalKey = msb2User.user');
        $c->innerJoin('msb2Level', 'Level', 'Level.id = msb2User.level');

        //
        $c->select($this->modx->getSelectColumns('modUser', 'User', '', ['id', 'class_key'], true));
        $c->select($this->modx->getSelectColumns('modUserProfile', 'Profile', '', [
            'fullname',
            'email',
            'mobilephone',
            'dob',
            'blocked',
        ]));
        $c->select($this->modx->getSelectColumns('msb2User', 'msb2User', ''));
        $c->select($this->modx->getSelectColumns('msb2Level', 'Level', 'level_', ['id'], true));

        //
        foreach (array('level') as $v) {
            if (${$v} = $this->getProperty($v)) {
                if (${$v} == '_') {
                    $c->where(array(
                        '(' . $this->classKey . '.' . $v . ' = "" OR ' . $this->classKey . '.' . $v . ' IS NULL)',
                    ));
                } else {
                    $c->where(array(
                        $this->classKey . '.' . $v => ${$v},
                    ));
                }
            }
        }

        //
        if ($query = trim($this->getProperty('query'))) {
            $c->where(array(
                'User.username:LIKE' => "%{$query}%",
                'OR:Profile.fullname:LIKE' => "%{$query}%",
                'OR:Profile.email:LIKE' => "%{$query}%",
                'OR:Profile.mobilephone:LIKE' => "%{$query}%",
            ));
        }

        $c->groupby('msb2User.user');

        return $c;
    }

    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();

        // Phone
        // $data['phone'] = $data['phone'] ?: $data['mobilephone'];

        //
        $data['points_formatted'] = number_format($data['points'], 0, '.', ' ');
        $data['reserve_formatted'] = number_format($data['reserve'], 0, '.', ' ');

        //
        $data['paid_money_formatted'] = number_format($data['paid_money'], 0, '.', ' ');
        $data['paid_points_formatted'] = number_format($data['paid_points'], 0, '.', ' ');

        //
        $data['level_cost_formatted'] = number_format($data['level_cost'], 0, '.', ' ');

        //
        $data['blocked'] = $data['blocked'] ? true : false;
        unset($data['password'], $data['cachepwd'], $data['salt']);

        // Buttons
        $data['actions'] = $this->getActions($data);

        return $data;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function getActions(array $data)
    {
        $actions = array();
        $actions[] = array(
            'cls' => '',
            'icon' => 'icon icon-eye',
            'title' => $this->modx->lexicon('msb2_button_update'),
            'action' => 'updateObject',
            'button' => true,
            'menu' => true,
        );

        // if (!$data['active']) {
        //     $actions[] = array(
        //         'cls' => '',
        //         'icon' => 'icon icon-toggle-on action-green',
        //         'title' => $this->modx->lexicon('msb2_button_enable'),
        //         'multiple' => $this->modx->lexicon('msb2_button_enable_multiple'),
        //         'action' => 'enableObject',
        //         'button' => true,
        //         'menu' => true,
        //     );
        // } else {
        //     $actions[] = array(
        //         'cls' => '',
        //         'icon' => 'icon icon-toggle-off action-red',
        //         'title' => $this->modx->lexicon('msb2_button_disable'),
        //         'multiple' => $this->modx->lexicon('msb2_button_disable_multiple'),
        //         'action' => 'disableObject',
        //         'button' => true,
        //         'menu' => true,
        //     );
        // }
        // $actions[] = array(
        //     'cls' => '',
        //     'icon' => 'icon icon-trash-o action-red',
        //     'title' => $this->modx->lexicon('msb2_button_remove'),
        //     'multiple' => $this->modx->lexicon('msb2_button_remove_multiple'),
        //     'action' => 'removeObject',
        //     'button' => true,
        //     'menu' => true,
        // );

        return $actions;
    }
}

return 'msb2UserGetListProcessor';