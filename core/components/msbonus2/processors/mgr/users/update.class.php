<?php

class msb2UserUpdateProcessor extends modObjectUpdateProcessor
{
    public $objectType = 'msb2User';
    public $classKey = 'msb2User';
    public $languageTopics = array('msbonus2:default');
    public $permission = 'save';
    /** @var msBonus2 $msb2 */
    protected $msb2;

    /**
     * @return bool
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        return parent::initialize();
    }

    /**
     * @return bool|string
     */
    public function beforeSave()
    {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        return parent::beforeSave();
    }

    /**
     * @return bool
     */
    public function beforeSet()
    {
        if (!$id = (int)$this->getProperty('id')) {
            return $this->modx->lexicon('msb2_err_ns');
        }
        if (($tmp = $this->prepareProperties()) !== true) {
            return $tmp;
        }
        unset($tmp);

        // Check on require
        $required = [
            'username',
            'fullname',
            'email',
            'level',
        ];
        $this->msb2->tools->checkProcessorRequired($this, $required, 'msb2_err_required');

        // Check on unique
        $unique = [
            'username',
        ];
        if ($this->modx->getOption('allow_multiple_emails') === false) {
            $unique[] = 'email';
        }
        $this->msb2->tools->checkProcessorUnique('', 0, $this, $unique, 'msb2_err_unique');

        return parent::beforeSet();
    }

    /**
     * @return boolean
     */
    public function afterSave()
    {
        $properties = $this->getProperties();

        $user = $this->object->getOne('User');
        foreach ([
            'username',
            // 'createdon',
            // 'active',
        ] as $key) {
            if (isset($properties[$key])) {
                $user->set($key, $properties[$key]);
            }
        }

        $profile = $user->getOne('Profile');
        foreach ([
            'fullname',
            'email',
            'mobilephone',
            'dob',
            'gender',
            // 'blocked',
        ] as $key) {
            if (isset($properties[$key])) {
                $profile->set($key, $properties[$key]);
            }
        }

        $user->save();

        return true;
    }

    /**
     * @return string|bool
     */
    public function prepareProperties()
    {
        $properties = $this->getProperties();

        // Level frozen
        if ((int)$this->object->get('level') !== (int)$this->getProperty('level')) {
            $this->setProperty('level_frozen', true);
        }

        $this->setProperties($properties);

        return true;
    }
}

return 'msb2UserUpdateProcessor';