<?php

class msb2LogGetListProcessor extends modObjectGetListProcessor
{
    public $objectType = 'msb2Log';
    public $classKey = 'msb2Log';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';
    public $permission = 'list';
    /** @var msBonus2 $msb2 */
    protected $msb2;

    /**
     * @return bool
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        return parent::initialize();
    }

    /**
     * @return boolean|string
     */
    public function beforeQuery()
    {
        if (!$this->checkPermissions()) {
            return $this->modx->lexicon('access_denied');
        }

        $this->setProperty('sort', str_replace('_formatted', '', $this->getProperty('sort')));

        return parent::beforeQuery();
    }

    /**
     * @param xPDOQuery $c
     *
     * @return xPDOQuery
     */
    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        //
        $c->leftJoin('modUser', 'Creator', 'Creator.id = msb2Log.createdby');
        $c->leftJoin('modUserProfile', 'CreatorProfile', 'CreatorProfile.internalKey = msb2Log.createdby');
        $c->leftJoin('msOrder', 'Order', 'Order.id = msb2Log.order');

        //
        $c->select($this->modx->getSelectColumns('msb2Log', 'msb2Log'));
        $c->select($this->modx->getSelectColumns('modUser', 'Creator', '', [
            'username',
        ]));
        $c->select($this->modx->getSelectColumns('modUserProfile', 'CreatorProfile', '', [
            'fullname',
            'email',
            // 'mobilephone',
            // 'dob',
        ]));
        $c->select($this->modx->getSelectColumns('msOrder', 'Order', 'order_', ['id', 'num']));

        //
        if ($user_id = (int)$this->getProperty('user')) {
            $c->where([
                $this->classKey . '.user' => $user_id,
            ]);
        }

        // //
        // if ($query = trim($this->getProperty('query'))) {
        //     $c->where(array(
        //         $this->classKey . '.name:LIKE' => "%{$query}%",
        //         'OR:' . $this->classKey . '.description:LIKE' => "%{$query}%",
        //     ));
        // }

        return $c;
    }

    /**
     * @param array $rows
     *
     * @return array
     */
    public function beforeIteration(array $rows)
    {
        return $rows;
    }

    /**
     * @param xPDOObject $object
     *
     * @return array
     */
    public function prepareRow(xPDOObject $object)
    {
        $data = $object->toArray();

        //
        $data['action_formatted'] = $this->msb2->getActionType($data);

        //
        $data['amount_formatted'] = number_format($data['amount'], 0, '.', ' ');

        return $data;
    }
}

return 'msb2LogGetListProcessor';