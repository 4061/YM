<?php
$xpdo_meta_map['msb2Level']= array (
  'package' => 'msbonus2',
  'version' => '1.1',
  'table' => 'msbonus2_levels',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'bonus' => 0,
    'cost' => 0,
    'name' => '',
    'description' => '',
    'properties' => NULL,
  ),
  'fieldMeta' => 
  array (
    'bonus' => 
    array (
      'dbtype' => 'int',
      'precision' => '3',
      'phptype' => 'integer',
      'attributes' => 'unsigned',
      'null' => false,
      'default' => 0,
    ),
    'cost' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'attributes' => 'unsigned',
      'null' => false,
      'default' => 0,
    ),
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '100',
      'phptype' => 'string',
      'null' => false,
      'default' => '',
    ),
    'description' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => true,
      'default' => '',
    ),
    'properties' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'json',
      'null' => true,
    ),
  ),
  'aggregates' => 
  array (
    'Users' => 
    array (
      'class' => 'msb2User',
      'local' => 'id',
      'foreign' => 'level',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
