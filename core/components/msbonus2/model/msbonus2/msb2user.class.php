<?php

class msb2User extends xPDOSimpleObject
{
    /** @var msBonus2 $msb2 */
    protected $msb2;
    /** @var bool $_skipUpdateLevel */
    protected $_skipUpdateLevel = false;

    /**
     * @param xPDO $xpdo
     */
    public function __construct(xPDO &$xpdo)
    {
        parent::__construct($xpdo);

        //
        $this->msb2 = $this->xpdo->getService('msbonus2', 'msBonus2',
            $this->xpdo->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize();
    }

    /**
     * @param null|bool|int $cacheFlag
     *
     * @return bool
     */
    public function save($cacheFlag = null)
    {
        $is_new = $this->isNew();
        if ($saved = parent::save($cacheFlag)) {
            if ($is_new) {
                // Set points for registration
                $q = $this->xpdo->newQuery('modSystemSetting', [
                    'key' => 'msb2_bonus_for_registration'
                ])->select('value');
                if ($q->prepare()->execute()) {
                    if ($amount = (float)$q->stmt->fetchColumn()) {
                        if ($manager = $this->msb2->getManager()) {
                            if (!$manager->isExists('+', 'signup', $this, 0)) {
                                $this->_skipUpdateLevel = true;
                                $manager->setPlus('signup', $amount, $this, 0, $this->get('user'));
                                $this->_skipUpdateLevel = false;
                            }
                        }
                    }
                }
            }

            // Set new level
            if ($this->_skipUpdateLevel === false) {
                // Get all levels
                $q = $this->xpdo->newQuery('msb2Level')
                    ->select(['id', 'cost'])
                    ->sortby('cost', 'ASC');
                if ($q->prepare()->execute()) {
                    if ($tmp = $q->stmt->fetchAll(PDO::FETCH_ASSOC)) {
                        $levels = [];
                        foreach ($tmp as $v) {
                            $levels[$v['id']] = $v;
                        }
                        unset($tmp, $v);

                        // Get current level
                        $user_level_old = $this->get('level');
                        $user_paid_money = $this->get('paid_money');

                        $user_level_new = 0;
                        foreach ($levels as $level) {
                            if ($user_paid_money >= $level['cost']) {
                                $user_level_new = $level['id'];

                                // If old level lower than new level, then reset freeze
                                if (!empty($user_level_old) && isset($levels[$user_level_old])) {
                                    if ($levels[$user_level_old]['cost'] <= $level['cost']) {
                                        $this->set('level_frozen', false);
                                    }
                                }
                            } else {
                                break;
                            }
                        }

                        if (!empty($user_level_new) && $this->get('level_frozen') === false) {
                            $this->set('level', $user_level_new);
                        }
                    }
                    unset($levels, $level);
                }
            }

            $saved = parent::save($cacheFlag);
        }

        return $saved;
    }
}