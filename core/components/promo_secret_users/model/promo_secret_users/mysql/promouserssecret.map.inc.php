<?php
$xpdo_meta_map['PromoUsersSecret']= array (
  'package' => 'promo_secret_users',
  'version' => '1.1',
  'table' => 'promo_users_secret',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'id_user' => NULL,
    'id_promo' => NULL,
    'segment_id' => NULL,
  ),
  'fieldMeta' => 
  array (
    'id_user' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'id_promo' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'segment_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
  ),
);
