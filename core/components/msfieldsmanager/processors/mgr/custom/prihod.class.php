<?php
class MsfmPrihodGetListProcessor extends modObjectGetListProcessor {
    public $classKey = 'ATest';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'ASC';
    public $checkListPermission = true;
}
return 'MsfmPrihodGetListProcessor';