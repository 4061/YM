<?php
$xpdo_meta_map['ATest']= array (
  'package' => 'msfieldsmanager',
  'version' => '1.1',
  'table' => 'a_test',
  'extends' => 'xPDOSimpleObject',
  'fields' => 
  array (
    'name' => NULL,
  ),
  'fieldMeta' => 
  array (
    'name' => 
    array (
      'dbtype' => 'longtext',
      'phptype' => 'string',
      'null' => true,
    ),
  ),
);
