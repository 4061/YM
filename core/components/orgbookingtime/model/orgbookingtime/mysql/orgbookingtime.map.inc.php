<?php
$xpdo_meta_map['OrgBookingTime']= array (
  'package' => 'orgbookingtime',
  'version' => '1.1',
  'table' => 'org_booking_time',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'id_organisation' => NULL,
    'id_order' => NULL,
    'booking_time' => NULL,
    'booking_date' => NULL,
    'comment' => NULL,
    'active' => NULL,
  ),
  'fieldMeta' => 
  array (
    'id_organisation' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'id_order' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'booking_time' => 
    array (
      'dbtype' => 'time',
      'phptype' => 'string',
      'null' => false,
    ),
    'booking_date' => 
    array (
      'dbtype' => 'date',
      'phptype' => 'date',
      'null' => false,
    ),
    'comment' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'active' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'phptype' => 'integer',
      'null' => false,
    ),
  ),
);
