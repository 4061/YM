<?php
$xpdo_meta_map['SegmentNotificationsTemplate']= array (
  'package' => 'segment_notifications_template',
  'version' => '1.1',
  'table' => 'segment_notifications_template',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'id_notification' => NULL,
    'template_name' => NULL,
    'title' => NULL,
    'content' => NULL,
  ),
  'fieldMeta' => 
  array (
    'id_notification' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'template_name' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'title' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'content' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
