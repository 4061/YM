<?php
$xpdo_meta_map['ResponseErrors']= array (
  'package' => 'errors',
  'version' => '1.1',
  'table' => 'response_errors',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'result_code' => NULL,
    'error' => 1,
    'error_msg' => NULL,
  ),
  'fieldMeta' => 
  array (
    'result_code' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
    'error' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'phptype' => 'integer',
      'null' => false,
      'default' => 1,
    ),
    'error_msg' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
