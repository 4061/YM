<?php
 return array (
  'fields' => 
  array (
    'additive' => 0,
    'price2' => 0.0,
    'restourants' => NULL,
    'order_types' => NULL,
    'gift' => 0,
    'mailvisible' => 1,
    'third_action' => 0,
  ),
  'fieldMeta' => 
  array (
    'additive' => 
    array (
      'dbtype' => 'boolean',
      'precision' => '',
      'phptype' => 'boolean',
      'default' => 0,
      'null' => false,
    ),
    'price2' => 
    array (
      'dbtype' => 'double',
      'precision' => '',
      'phptype' => 'integer',
      'default' => 0.0,
      'null' => false,
    ),
    'restourants' => 
    array (
      'dbtype' => 'text',
      'precision' => '',
      'phptype' => 'string',
      'default' => NULL,
      'null' => false,
    ),
    'order_types' => 
    array (
      'dbtype' => 'text',
      'precision' => '',
      'phptype' => 'string',
      'default' => NULL,
      'null' => false,
    ),
    'gift' => 
    array (
      'dbtype' => 'boolean',
      'precision' => '',
      'phptype' => 'boolean',
      'default' => 0,
      'null' => false,
    ),
    'mailvisible' => 
    array (
      'dbtype' => 'boolean',
      'precision' => '',
      'phptype' => 'boolean',
      'default' => 1,
      'null' => false,
    ),
    'third_action' => 
    array (
      'dbtype' => 'boolean',
      'precision' => '',
      'phptype' => 'boolean',
      'default' => 0,
      'null' => false,
    ),
  ),
);
