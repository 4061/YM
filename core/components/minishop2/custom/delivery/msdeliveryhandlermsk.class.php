<?php
if(!class_exists('msDeliveryInterface')) {
    require_once dirname(dirname(dirname(__FILE__))) . '/model/minishop2/msdeliveryhandler.class.php';
}
class msDeliveryHandlerMsk extends msDeliveryHandler implements msDeliveryInterface{

    public function getCost(msOrderInterface $order, msDelivery $delivery, $cost = 0) {
        $cart = $order->ms2->cart->status();
        $cart_cost = $cart['total_cost'];
        $order_array = $order->get();
        if ($_SERVER["REQUEST_URI"] != "/create-order.json"){
            $action = new Cart($this->modx, $request, $headers);
            $delivery_cost = $action->checkDeliveryCost($order_array['delivery'], $order_array['index'], $order_array['polygone_id'], $cart_cost);
            $this->modx->log(modX::LOG_LEVEL_ERROR, '$delivery_cost'.json_encode($delivery_cost));
        } else {
            $org_obj = $this->modx->getObject("modResource", $order_array['index']);
            $delivery_cost["delivery_cost"] = $org_obj->getTVValue("tvFieldGeoDeliveryPrice");
        }
        $delivery_cost = parent::getCost($order, $delivery, (double)$delivery_cost["delivery_cost"]);
        return $delivery_cost+$cost;
        
    }
}