<?php
if(!class_exists('msOrderInterface')) {
    require_once dirname(dirname(dirname(__FILE__))) . '/model/minishop2/msorderhandler.class.php';
}
class myOrderhandler extends msOrderHandler {
    public function submit($data = array())
    {
        
        //askjfga
        $response = $this->ms2->invokeEvent('msOnSubmitOrder', array(
            'data' => $data,
            'order' => $this,
        ));
        if (!$response['success']) {
            return $this->error($response['message']);
        }
        if (!empty($response['data']['data'])) {
            $this->set($response['data']['data']);
        }

        $response = $this->getDeliveryRequiresFields();
        if ($this->ms2->config['json_response']) {
            $response = json_decode($response, true);
        }
        if (!$response['success']) {
            return $this->error($response['message']);
        }
        $requires = $response['data']['requires'];

        $errors = array();
        foreach ($requires as $v) {
            if (!empty($v) && empty($this->order[$v])) {
                $errors[] = $v;
            }
        }
        if (!empty($errors)) {
            return $this->error('ms2_order_err_requires', $errors);
        }
        if (!$this->order["user_id"]){
            $user_id = $this->ms2->getCustomerId();
        } else {
            $user_id = (int)$this->order["user_id"];
        }
        if (empty($user_id) || !is_int($user_id)) {
            return $this->error(is_string($user_id) ? $user_id : 'ms2_err_user_nf');
        }

        $cart_status = $this->ms2->cart->status();
        $delivery_cost = $this->getCost(false, true);
        $cart_cost = $this->getCost(true, true) - $delivery_cost;
        $createdon = date('Y-m-d H:i:s');
        /** @var msOrder $order */
        $order = $this->modx->newObject('msOrder');
        $order->fromArray(array(
            'user_id' => $user_id,
            'createdon' => $createdon,
            'num' => $this->getNum(),
            'delivery' => $this->order['delivery'],
            'payment' => $this->order['payment'],
            'cart_cost' => $cart_cost,
            'weight' => $cart_status['total_weight'],
            'delivery_cost' => $delivery_cost,
            'cost' => $cart_cost + $delivery_cost,
            'status' => 0,
            'context' => $this->ms2->config['ctx'],
        ));
        

        // Adding address
        /** @var msOrderAddress $address */
        $address = $this->modx->newObject('msOrderAddress');
        $address->fromArray(array_merge($this->order, array(
            'user_id' => $user_id,
            'createdon' => $createdon,
        )));
        $order->addOne($address);
        // Adding products
        $cart = $this->ms2->cart->get();
        $products = array();
        //$this->modx->log(MODX_LOG_LEVEL_ERROR, json_encode($cart));
        foreach ($cart as $v) {
            if ($tmp = $this->modx->getObject('msProduct', array('id' => $v['id']))) {
                $name = $tmp->get('pagetitle');
            } else {
                $name = '';
            }
            /** @var msOrderProduct $product */
            
            $product = $this->modx->newObject('msOrderProduct');
            $discount_percent = (100 - $this->modx->getOption("app_discount_percent"))/100; //процент скидки на самовывоз
            switch ($this->order['delivery']){
                case 2:
                    $size_price = $v["options"]["childs"]["size"][0]["price"];
                    break;
                case 1:
                    if ($_POST["enabled_delivery_sale"] == false){
                        $size_price = $v["options"]["childs"]["size"][0]["price"];
                    } else {
                        $size_price = $v["options"]["childs"]["size"][0]["price"]*$discount_percent;
                    }
                    break;
                default:
                    $size_price = $v["options"]["childs"]["size"][0]["price"];
            }
            $this->modx->log(MODX_LOG_LEVEL_ERROR, 'ЦЕНА РАЗМЕРА '.$size_price);
            $product->fromArray(array_merge($v, array(
                'product_id' => $v['id'],
                'name' => $name,
                'cost' => ($v['price']-$size_price) * $v['count'],
                'comment' => $v["options"]["comment"]
            )));
            $new_cart_cost += ($v['price']-$size_price) * $v['count'];
            $products[] = $product;
        }
        $order->addMany($products);
        $order->set("cart_cost", $new_cart_cost);
        $order->set("cost", $new_cart_cost+$delivery_cost);
        $response = $this->ms2->invokeEvent('msOnBeforeCreateOrder', array(
            'msOrder' => $order,
            'order' => $this,
        ));
        if (!$response['success']) {
            return $this->error($response['message']);
        }
        
        if ($order->save()) {
            $response = $this->ms2->invokeEvent('msOnCreateOrder', array(
                'msOrder' => $order,
                'order' => $this,
            ));
            if (!$response['success']) {
                return $this->error($response['message']);
            }

            $this->ms2->cart->clean();
            $this->clean();
            if (empty($_SESSION['minishop2']['orders'])) {
                $_SESSION['minishop2']['orders'] = array();
            }
            $_SESSION['minishop2']['orders'][] = $order->get('id');

            // Trying to set status "new"
            //$response = $this->ms2->changeOrderStatus($order->get('id'), 6);    
            /*if ($order->get('payment') == '3') {
                $response = $this->ms2->changeOrderStatus($order->get('id'), 6);    
            } else {
                $response = $this->ms2->changeOrderStatus($order->get('id'), 5); 
            }*/
            if ($order->get('payment') == 1 || $order->get('payment') == 6) {
                $response = $this->ms2->changeOrderStatus($order->get('id'), 14); 
            } else {
                $response = $this->ms2->changeOrderStatus($order->get('id'), 1); 
            }
            
            if ($response !== true) {
                
                return $this->error($response, array('msorder' => $order->get('id')));
            } /** @var msPayment $payment */
            elseif ($payment = $this->modx->getObject('msPayment',
                array('id' => $order->get('payment'), 'active' => 1))
            ) {
                $response = $payment->send($order);
               // return $this->success('', array('redirect' => 'https://3dsec.sberbank.ru/payment/merchants/sbersafe/payment_ru.html?mdOrder=39de63f0-7c46-74e9-abfe-13065e390dfe'));
                
                if ($this->config['json_response']) {
                    @session_write_close();
                    
                    exit(is_array($response) ? json_encode($response) : $response);
                } else {
                    
                    if (!empty($response['data']['redirect'])) {
                       // $this->modx->sendRedirect($response['data']['redirect']);
                    } elseif (!empty($response['data']['msorder'])) {
                        
                        return $response['data']['msorder'];
                        //$modx->log(1,'заказззззззззззззз2222222');
                        // $this->modx->sendRedirect(
                        //     $this->modx->context->makeUrl(
                        //         $this->modx->resource->id,
                        //         array('msorder' => $response['data']['msorder'])
                        //     )
                        // );
                    } else {
                       // $this->modx->sendRedirect($this->modx->context->makeUrl($this->modx->resource->id));
                    }

                    //return $this->success();
                }
            } else {
                if ($this->ms2->config['json_response']) {
                   // return $this->success('', array('msorder' => $order->get('id')));
                } else {
                    // $this->modx->sendRedirect(
                    //     $this->modx->context->makeUrl(
                    //         $this->modx->resource->id,
                    //         array('msorder' => $response['data']['msorder'])
                    //     )
                    // );

                    // return $this->success();
                }
            }
        }

        return $this->error();
    }
}