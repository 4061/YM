<?php
$xpdo_meta_map['AnswerOptions']= array (
  'package' => 'answer_options',
  'version' => '1.1',
  'table' => 'answer_options',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'answer_option' => NULL,
  ),
  'fieldMeta' => 
  array (
    'answer_option' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
