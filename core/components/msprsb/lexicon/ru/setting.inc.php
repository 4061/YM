<?php

$_lang['area_msprsb_common'] = 'Общие';

$_lang['setting_msprsb_merchand_id'] = 'Merchant ID';
$_lang['setting_msprsb_merchand_id_desc'] = 'Ваш идентификатор в системе банка';

$_lang['setting_msprsb_api_url'] = 'Merchant Handler URL';
$_lang['setting_msprsb_api_url_desc'] = 'Адрес для обращения к api банка. Обычное значение настройки: <br>- в тестовой среде: <em>https://testsecurepay.rsb.ru:9443/ecomm2/MerchantHandler</em> <br>- в боевом режиме: <em>https://securepay.rsb.ru:9443/ecomm2/MerchantHandler</em>';

$_lang['setting_msprsb_checkout_url'] = 'Client Handler URL';
$_lang['setting_msprsb_checkout_url_desc'] = 'Ссылка для перенаправления клиента на страницу оплаты. <strong>Внимание:</strong> в конце ссылки должен быть указан get-параметр <em>trans_id</em>. Обычное значение настройки: <br>- в тестовой среде: <em>https://testsecurepay.rsb.ru/ecomm2/ClientHandler?trans_id=</em> <br>- в боевом режиме: <em>https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=</em>';

$_lang['setting_msprsb_currency_code'] = 'Код валюты';
$_lang['setting_msprsb_currency_code_desc'] = 'Код валюты транзакции (ISO 4217). На территории РФ - 643';

$_lang['setting_msprsb_client_language'] = 'Язык платежной страницы';
$_lang['setting_msprsb_client_language_desc'] = 'Код языка платёжной страницы (латиницей, в нижнем регистре)';

$_lang['setting_msprsb_success_id'] = 'Страница успешной оплаты';
$_lang['setting_msprsb_success_id_desc'] = 'ID страницы, на которую будет перенаправлен пользователь в случае успешной оплаты. Если не указано, пользователь будет перенаправлен на главную страницу сайта';

$_lang['setting_msprsb_failure_id'] = 'Страница неуспешной оплаты';
$_lang['setting_msprsb_failure_id_desc'] = 'ID страницы, на которую будет перенаправлен пользователь в случае неуспешной оплаты. Если не указано, пользователь будет перенаправлен на главную страницу сайта';