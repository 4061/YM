<?php

$_lang['area_msprsb_common'] = 'Common';

$_lang['setting_msprsb_merchand_id'] = 'Merchant ID';
$_lang['setting_msprsb_merchand_id_desc'] = 'Your Merchant Identifier in the Bank';

$_lang['setting_msprsb_api_url'] = 'Merchant Handler URL';
$_lang['setting_msprsb_api_url_desc'] = 'Url for api-requests. Usually: <br>- Test mode: <em>https://testsecurepay.rsb.ru:9443/ecomm2/MerchantHandler</em> <br>- Production mode: <em>https://securepay.rsb.ru:9443/ecomm2/MerchantHandler</em>';

$_lang['setting_msprsb_checkout_url'] = 'Client Handler URL';
$_lang['setting_msprsb_checkout_url_desc'] = 'Url of payment page. <strong>Warning:</strong> get-parameter <em>trans_id</em> is required. Usually: <br>- Test mode: <em>https://testsecurepay.rsb.ru/ecomm2/ClientHandler?trans_id=</em> <br>- Production mode: <em>https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=</em>';

$_lang['setting_msprsb_currency_code'] = 'Currency Code';
$_lang['setting_msprsb_currency_code_desc'] = 'The code of transaction currency (ISO 4217). Usually in Russia - 643';

$_lang['setting_msprsb_client_language'] = 'Payment Page Language';
$_lang['setting_msprsb_client_language_desc'] = 'The code of payment page language (latin lowercase)';

$_lang['setting_msprsb_success_id'] = 'Success page';
$_lang['setting_msprsb_success_id_desc'] = 'ID of page to redirect in case of successfull payment. If ID is not specified, user will be redirected to Site Start Page';

$_lang['setting_msprsb_failure_id'] = 'Failure page';
$_lang['setting_msprsb_failure_id_desc'] = 'ID of page to redirect in case of unsuccessfull payment. If ID is not specified, user will be redirected to Site Start Page';