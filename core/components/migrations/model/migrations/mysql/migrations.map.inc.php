<?php
$xpdo_meta_map['Migrations']= array (
  'package' => 'migrations',
  'version' => '1.1',
  'table' => 'migrations',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'modx_table' => NULL,
    'modx_field' => NULL,
    'modx_sql' => NULL,
  ),
  'fieldMeta' => 
  array (
    'modx_table' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
    'modx_field' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
    'modx_sql' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
);
