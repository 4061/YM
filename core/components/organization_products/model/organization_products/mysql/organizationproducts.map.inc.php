<?php
$xpdo_meta_map['OrganizationProducts']= array (
  'package' => 'organization_products',
  'version' => '1.1',
  'table' => 'organization_products',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'id_org' => NULL,
    'id_product' => NULL,
  ),
  'fieldMeta' => 
  array (
    'id_org' => 
    array (
      'dbtype' => 'int',
      'precision' => '255',
      'phptype' => 'integer',
      'null' => true,
    ),
    'id_product' => 
    array (
      'dbtype' => 'int',
      'precision' => '255',
      'phptype' => 'integer',
      'null' => true,
    ),
  ),
);
