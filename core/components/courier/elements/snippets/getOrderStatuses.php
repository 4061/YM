<?php
$pdoFetch = new pdoFetch($modx);
$flight_statuses = array(
    'class' => 'CourierOrderStatuses',
    'sortdir' => "ASC",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($flight_statuses);
$flight_statuses = $pdoFetch->run();
return $flight_statuses;