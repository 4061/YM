<?php
$pdoFetch = new pdoFetch($modx);
$orders = $pdoFetch->run();
$flights = array(
    'class' => 'CourierFlights',
    'leftJoin' => [
        'Status' => ['class'=> 'CourierFlightStatuses', 'on'=>'Status.id = CourierFlights.status_id'],
        'modUserProfile' => ['class'=> 'modUserProfile', 'on'=> 'CourierFlights.courier_id = modUserProfile.internalKey'],
        ],
    'select' => [
        'CourierFlights'=>'CourierFlights.id, CourierFlights.status_id, CourierFlights.comment, CourierFlights.courier_id, CourierFlights.date_start, CourierFlights.date_end', 
        'Status' => 'Status.name as status_name, Status.description as status_description, Status.text_color as status_text_color, Status.wrapper_color as status_wrapper_color',
        'modUserProfile' => 'modUserProfile.fullname as courier'
        ],
    'sortdir' => "ASC",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($flights);
$flights = $pdoFetch->run();
return $flights;