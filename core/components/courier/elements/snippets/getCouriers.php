<?php
$pdoFetch = new pdoFetch($modx);
$couriers = array(
    'class' => 'modUserProfile',
    'leftJoin' => [
        'Members' => ['class'=> 'modUserGroupMember', 'on'=>'modUserProfile.internalKey = Members.member'],
    ],
    'sortdir' => "desc",
    'return' => 'data',
    'select' => [
        'modUserProfile' => '*',
    ],
    'where' => ["Members.user_group" => 6],
    'limit' => 10000
);
$pdoFetch->setConfig($couriers);
$couriers = $pdoFetch->run();
return $couriers;