<?php
$pdoFetch = new pdoFetch($modx);
$orders = array(
    'class' => 'msOrder',
    'leftJoin' => [
        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],
    ],
    'sortdir' => "desc",
    'return' => 'data',
    'select' => [
        'msOrder' => '*',
        'Address' => 'street'
    ],
    'limit' => 10000
);
$pdoFetch->setConfig($orders);
$orders = $pdoFetch->run();
return($orders);