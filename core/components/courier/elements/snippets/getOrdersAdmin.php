<?php
$pdoFetch = new pdoFetch($modx);
$orders = array(
    'class' => 'msOrder',
    'leftJoin' => [
        'Address' => ['class'=> 'msOrderAddress', 'on'=>'msOrder.address = Address.id'],
        'Receiver' => ['class' => 'modUserProfile', 'on' => 'msOrder.user_id = Receiver.internalKey'],
        'CourierOrders' => ['class' => 'CourierOrders', 'on' => 'msOrder.id = CourierOrders.order_id'],
        'Flight' => ['class' => 'CourierFlights', 'on' => 'CourierOrders.flight_id = Flight.id'],
        'Courier' => ['class' => 'modUserProfile', 'on' => 'Flight.courier_id = Courier.internalKey'],
        'Payment' => ['class'=> 'msPayment', 'on'=> 'Payment.id = msOrder.payment'],
        'CourierOrderStatuses' => ['class'=> 'CourierOrderStatuses', 'on'=> 'CourierOrderStatuses.id = CourierOrders.status_id']
    ],
    'sortdir' => "desc",
    'return' => 'data',
    'select' => [
        'msOrder' => '
            msOrder.id,
            msOrder.cost',
        'Address' => '
            Address.country, 
            Address.city, 
            Address.street, 
            Address.house_num, 
            Address.street, 
            Address.entrance, 
            Address.floor, 
            Address.flat_num, 
            Address.metro as doorphone',
        'Receiver' => '
            Receiver.fullname as receiver_name,
            Receiver.mobilephone as receiver_mobilephone',
        'CourierOrders' => 'flight_id',
        'Courier' => 'Courier.fullname as courier_name',
        'Payment' => 'Payment.name as payment',
        'CourierOrderStatuses' => '
            CourierOrderStatuses.name as status_name,
            CourierOrderStatuses.description as status_description'
    ],
    'limit' => 10000,
    'where' => ["flight_id <> 0"]
);
$pdoFetch->setConfig($orders);
$orders = $pdoFetch->run();
return $orders;