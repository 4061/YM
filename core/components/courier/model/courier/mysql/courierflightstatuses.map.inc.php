<?php
$xpdo_meta_map['CourierFlightStatuses']= array (
  'package' => 'courier',
  'version' => '1.1',
  'table' => 'courier_flight_statuses',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'name' => NULL,
    'description' => NULL,
    'text_color' => NULL,
    'wrapper_color' => NULL,
    'final' => 0,
  ),
  'fieldMeta' => 
  array (
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'description' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'text_color' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'wrapper_color' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'final' => 
    array (
      'dbtype' => 'tinyint',
      'precision' => '1',
      'phptype' => 'integer',
      'null' => true,
      'default' => 0,
    ),
  ),
  'aggregates' => 
  array (
    'CourierFlights' => 
    array (
      'class' => 'CourierFlights',
      'local' => 'id',
      'foreign' => 'status_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
