<?php
$xpdo_meta_map['CourierOrerStatuses']= array (
  'package' => 'courier',
  'version' => '1.1',
  'table' => 'courier_orer_statuses',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'name' => NULL,
    'description' => NULL,
    'text_color' => NULL,
    'wrapper_color' => NULL,
  ),
  'fieldMeta' => 
  array (
    'name' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'description' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'text_color' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => true,
    ),
    'wrapper_color' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '255',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
  'aggregates' => 
  array (
    'CourierOrders' => 
    array (
      'class' => 'CourierOrders',
      'local' => 'id',
      'foreign' => 'status_id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
