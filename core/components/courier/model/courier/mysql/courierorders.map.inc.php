<?php
$xpdo_meta_map['CourierOrders']= array (
  'package' => 'courier',
  'version' => '1.1',
  'table' => 'courier_orders',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'menuindex' => NULL,
    'flight_id' => NULL,
    'order_id' => NULL,
    'status_id' => NULL,
  ),
  'fieldMeta' => 
  array (
    'menuindex' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'flight_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'order_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
    'status_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '11',
      'phptype' => 'integer',
      'null' => false,
    ),
  ),
  'aggregates' => 
  array (
    'CourierFlights' => 
    array (
      'class' => 'CourierFlights',
      'local' => 'flight_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
    'CourierOrderStatuses' => 
    array (
      'class' => 'CourierOrderStatuses',
      'local' => 'status_id',
      'foreign' => 'id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
);
