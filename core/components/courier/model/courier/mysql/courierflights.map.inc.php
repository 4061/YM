<?php
$xpdo_meta_map['CourierFlights']= array (
  'package' => 'courier',
  'version' => '1.1',
  'table' => 'courier_flights',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'MyISAM',
  ),
  'fields' => 
  array (
    'status_id' => NULL,
    'date_start' => NULL,
    'date_end' => NULL,
    'courier_id' => NULL,
    'comment' => NULL,
  ),
  'fieldMeta' => 
  array (
    'status_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'attributes' => 'unsigned',
      'phptype' => 'integer',
      'null' => true,
    ),
    'date_start' => 
    array (
      'dbtype' => 'datetime',
      'phptype' => 'datetime',
      'null' => true,
    ),
    'date_end' => 
    array (
      'dbtype' => 'datetime',
      'phptype' => 'datetime',
      'null' => true,
    ),
    'courier_id' => 
    array (
      'dbtype' => 'int',
      'precision' => '10',
      'phptype' => 'integer',
      'null' => false,
    ),
    'comment' => 
    array (
      'dbtype' => 'text',
      'phptype' => 'string',
      'null' => false,
    ),
  ),
  'composites' => 
  array (
    'CourierOrders' => 
    array (
      'class' => 'CourierOrders',
      'local' => 'id',
      'foreign' => 'flight_id',
      'cardinality' => 'many',
      'owner' => 'local',
    ),
  ),
  'aggregates' => 
  array (
    'CourierFlightStatuses' => 
    array (
      'class' => 'CourierFlightStatuses',
      'local' => 'status_id',
      'foreign' => 'id',
      'cardinality' => 'one',
      'owner' => 'local',
    ),
  ),
);
