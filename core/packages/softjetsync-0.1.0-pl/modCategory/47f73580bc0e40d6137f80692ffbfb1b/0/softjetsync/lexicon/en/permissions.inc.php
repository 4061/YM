<?php
/**
 * Russian permissions Lexicon Entries for SoftjetSync
 *
 * @package SoftjetSync
 * @subpackage lexicon
 */
$_lang['softjetsync_save'] = 'Permission for save/update data.';