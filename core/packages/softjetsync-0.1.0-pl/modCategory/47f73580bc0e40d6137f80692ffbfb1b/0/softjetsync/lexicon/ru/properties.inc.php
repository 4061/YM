<?php

$_lang['softjetsync_prop_limit'] = 'Ограничение вывода Предметов на странице.';
$_lang['softjetsync_prop_outputSeparator'] = 'Разделитель вывода строк.';
$_lang['softjetsync_prop_sortby'] = 'Поле сортировки.';
$_lang['softjetsync_prop_sortdir'] = 'Направление сортировки.';
$_lang['softjetsync_prop_tpl'] = 'Чанк оформления каждого ряда Предметов.';
$_lang['softjetsync_prop_toPlaceholder'] = 'Если указан этот параметр, то результат будет сохранен в плейсхолдер, вместо прямого вывода на странице.';
