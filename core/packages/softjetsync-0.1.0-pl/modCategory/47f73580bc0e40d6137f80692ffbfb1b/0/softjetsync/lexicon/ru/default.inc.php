<?php
include_once 'setting.inc.php';

$_lang['softjetsync'] = 'SoftjetSync';
$_lang['softjetsync_menu_desc'] = 'Пример расширения для разработки.';
$_lang['softjetsync_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['softjetsync_items'] = 'Предметы';
$_lang['softjetsync_item_id'] = 'Id';
$_lang['softjetsync_item_name'] = 'Название';
$_lang['softjetsync_item_description'] = 'Описание';
$_lang['softjetsync_item_active'] = 'Активно';

$_lang['softjetsync_item_create'] = 'Создать предмет';
$_lang['softjetsync_item_update'] = 'Изменить Предмет';
$_lang['softjetsync_item_enable'] = 'Включить Предмет';
$_lang['softjetsync_items_enable'] = 'Включить Предметы';
$_lang['softjetsync_item_disable'] = 'Отключить Предмет';
$_lang['softjetsync_items_disable'] = 'Отключить Предметы';
$_lang['softjetsync_item_remove'] = 'Удалить Предмет';
$_lang['softjetsync_items_remove'] = 'Удалить Предметы';
$_lang['softjetsync_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['softjetsync_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['softjetsync_item_active'] = 'Включено';

$_lang['softjetsync_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['softjetsync_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['softjetsync_item_err_nf'] = 'Предмет не найден.';
$_lang['softjetsync_item_err_ns'] = 'Предмет не указан.';
$_lang['softjetsync_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['softjetsync_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['softjetsync_grid_search'] = 'Поиск';
$_lang['softjetsync_grid_actions'] = 'Действия';