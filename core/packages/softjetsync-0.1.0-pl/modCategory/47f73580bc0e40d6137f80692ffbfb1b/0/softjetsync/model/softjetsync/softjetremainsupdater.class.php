<?php

class SoftjetRemainsUpdater extends SoftjetUpdater
{
    protected $method = '/goods/remains';
    /**
     * @var array|mixed
     */
    private $products;
    /**
     * @var array|bool|string
     */
    private $remapped = [];


    public function process()
    {
        $remains = $this->guzzle->getEntities($this->method,['headers' => [
            'city' => $this->config['city_id'],
        ]
        ]);
        $prodClass = new SoftjetProductsUpdater($this->guzzle, $this->modx, $this->config);
        $this->products = $prodClass->getProductsFromModx();
        if (empty($remains)) {
            return false;
        }
        $this->getCurrentRemains();
        foreach ($remains as $remnant){
            if (!isset($this->products[$remnant['prod']]))
                continue;
            if (isset($this->remapped[$this->products[$remnant['prod']]['id']])) {
                $this->updateRemnant($this->remapped[$this->products[$remnant['prod']]['id']], $remnant);
            }else{
                $this->addRemnant($this->products[$remnant['prod']], $remnant);
            }
        }

    }

    protected function getCurrentRemains()
    {
        $this->pdo->setConfig(array(
            'class' => 'SoftjetsyncRemains',
            'return' => 'data',
            'limit' => $this->defaultLimit
        ));
        $currentRemains = $this->pdo->run();
        if (empty($currentRemains))
            return [];
        $this->remapped = array_combine(array_column($currentRemains, 'product_id'), $currentRemains);
        return $this->remapped;
    }

    private function updateRemnant($dbRemant, $remnant)
    {
        if ($dbRemant['remant'] != $remnant['remains'] || $dbRemant['coming'] != $remnant['coming']
        ){
            $updateRemnant = $this->modx->getObject('SoftjetsyncRemains', $dbRemant['id']);
            $updateRemnant->fromArray([
                'remant' => $remnant['remains'],
                'coming' => $remnant['coming'],
            ]);
            $updateRemnant->save();
        }
    }

    private function addRemnant($product, $remnant)
    {
        $newRemnant = $this->modx->newObject('SoftjetsyncRemains');
        $newRemnant->fromArray([
            'product_id' => $product['id'],
            'remant' => $remnant['remains'],
            'coming' => $remnant['coming'],
        ]);
        $newRemnant->save();
    }
}