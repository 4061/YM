<?php

class SoftjetOrderUpdater extends SoftjetUpdater
{

    private $addOrderMethod  = "orders/create/";
    private $getOrderMethod  = "orders/info/";
    private $getOrdersMethod = "orders/list/";

    public function addOrder(msOrder $order, $person_phone = false)
    {
        $user = $this->modx->getObject('modUser', $order->user_id);
        /**
         * @var modUserProfile $profile
         */
        $profile = $user->getOne('Profile');
        if (empty($profile->{$this->config["user_field"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no uuid");
            return false;
        }
        if (empty($profile->{$this->config["contact_key"]})) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "User have no phone number");
            return false;
        }
        $userData = $this->getUserData($profile->{$this->config["contact_key"]});
        
        if (empty($userData)){
            $this->modx->log(modX::LOG_LEVEL_ERROR, "No registred user in 1C");
            return false;
        }
        $orderData = [
            'person_id' => $userData[0]['persons'][0]["person_id"],
            'client_id' => $userData[0]['client_id'],
            'contragent_id' => $userData[0]['contragents'][0]["contragent_id"],
            'order_number' => $order->id
        ];
        if ($person_phone) {
            $orderData['person_phone'] = $person_phone;
        }
        $this->pdo->setConfig(array(
            'class' => 'msOrderProduct',
            'leftJoin' => ['modResource' => ['class' => 'modResource', 'on' => 'modResource.id = msOrderProduct.product_id']],
            'select' => ['msOrderProduct' => '*', 'modResource' => 'alias'],
            'return' => 'data',
            'where' => ['msOrderProduct.order_id' => $order->id],
            'limit' => $this->defaultLimit
        ));
        $products = $this->pdo->run();
        foreach ($products as $product) {
            $orderData['goods'][] = [
                "good" => $product['alias'],
                "quantity" => $product['count'],
            ];
        }
        $contacts = $this->modx->getObject('msOrderAddress', array('id'=> $order->address)); //получаем объект с подрообной информацией о заказе
        if ($order->delivery == 1){
            $orderData["delivery_type"] = "Pickup"; // самовывоз
            $org = $this->modx->getObject("modResource", $contacts->get("index"));
            $orderData["delivery_address"] = $org->get("pagetitle");
        } elseif ($order->delivery == 2){
            $orderData["delivery_type"] = "Delivery"; // доставка
            $orderData["delivery_address"] = 
            "Город: ".$contacts->city.", ".
            "Улица: ".$contacts->street.", ".
            "Дом: ".$contacts->house_num.", ".
            "Подъезд: ".$contacts->entrance.", ".
            "Код домофона: ".$contacts->metro.", ".
            "Этаж: ".$contacts->floor.", ".
            "Квартира: ".$contacts->flat_num;
        }
        switch ($order->payment){
            case 1:
                $orderData["payment_type"] = "card";
                $orderData["comment"] = "\r\nТип оплаты: онлайн оплата \r\n";
            break;
            case 4:
                $orderData["payment_type"] = "cashless";
                $orderData["comment"] = "\r\nТип оплаты: безналичная оплата \r\n";
            break;
            case 3:
                $orderData["payment_type"] = "cash";
                $orderData["comment"] = "\r\nТип оплаты: наличная оплата \r\n";
        }
        if ($order->payment == 7){
            $orderData["comment"] = "\r\nТип оплаты: Оплата по QR коду\r\n";
        }
        if ($order->payment == 8){
            $orderData["comment"] = "\r\nТип оплаты: Выписать счет\r\n";
        }
        if ($contacts->trans_id){
            $orderData["comment"] .= "ID транзакции в банке: ".$contacts->trans_id.", ID терминала: 1613647560681DEMO"; 
        }
        if ($order->status == 14){ //если оплата не прошла
            $orderData["comment"] .= ", онлайн оплата не прошла";
        }
        if ($order->status == 6 && $order->payment == 1){
            $orderData["comment"] .= "\r\nСумма фактической оплаты в банке: ".$order->cost;
        }
        $orderData["comment"] .= "\r\n".$contacts->comment;
        $log_path = MODX_BASE_PATH."1c.log"; //путь к файлу логов
        $this_date = date("Y-m-d H:i:s"); //дата
        $log_request = "[".$this_date."] "."ЗАПРОС В 1С:".json_encode($orderData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)."\n"; //записываем в строку запрос
        $logs_before = file_get_contents($log_path); //получаем то, что было в лог-файле
        file_put_contents($log_path, $log_request.$logs_before);
        $this->modx->log(modX::LOG_LEVEL_ERROR, '1C REQUEST: '.json_encode($orderData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
        $org_object = $this->modx->getObject("modResource", $contacts->index);
        //$this->modx->log(modX::LOG_LEVEL_ERROR, 'iiko_organization_key: '.$org_object->getTVValue("iiko_organization_key"));
        $response = $this->guzzle->postEntity($this->addOrderMethod, [
            'headers' => [
                'city' => $org_object->getTVValue("iiko_organization_key"),
            ],
            'json' => $orderData
        ]);
        if ($response){
            file_put_contents($log_path, "[".date("Y-m-d H:i:s")."] ОТВЕТ 1С: ".json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK)."\n".file_get_contents($log_path));
            return $response;
        } else {
            return null;
        }
    }


    public function getOrders(array $requestData = [])
    {
        $orders = $this->guzzle->getEntities($this->getOrdersMethod, ['query' => $requestData]);
        return $orders;
    }

    public function getOrder($order_id)
    {
        $order = $this->guzzle->getEntities($this->getOrderMethod,['query' => ['id' => $order_id]]);
        return $order;
    }


    public function getUserData($userPhone)
    {
        $SoftjetSync = $this->modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/', []);
        $action = $SoftjetSync->getUserActions();
        $this->modx->log(modX::LOG_LEVEL_ERROR, "userData ".$action->getUser($userPhone));
        return $action->getUser($userPhone);
    }
}
