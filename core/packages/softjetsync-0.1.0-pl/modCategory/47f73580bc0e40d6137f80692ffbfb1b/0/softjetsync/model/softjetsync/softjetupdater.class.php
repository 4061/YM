<?php

class SoftjetUpdater
{
    /**
     * @var array
     */
    protected $config;
    /**
     * @var SoftjetGazzle
     */
    protected $guzzle;
    /**
     * @var modX
     */
    protected $modx;
    /**
     * @var pdoFetch|null
     */
    protected $pdo;

    protected $reIndexKey;
    protected $defaultLimit = 10000000;

    public function __construct(SoftjetGazzle $guzzle, modX &$modx, array $config = [])
    {
        $this->pdo = $modx->getService('pdoFetch');
        $this->config = array_merge([

        ],$config);
        $this->guzzle = $guzzle;
        $this->modx = &$modx;
    }

    /**
     * Синхронизирует
     */
    public function process()
    {
    }


}