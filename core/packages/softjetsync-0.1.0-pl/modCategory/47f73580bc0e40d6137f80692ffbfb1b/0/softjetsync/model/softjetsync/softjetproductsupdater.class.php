<?php
class SoftjetProductsUpdater extends SoftjetUpdater
{
    protected $reIndexKey       = 'alias';
    protected $method           = 'goods/list';
    public    $dbProducts         = [];
    public    $categories         = [];
    protected $notFoundParent   = [];
    protected $barcodes = [];
    protected $imagesCacheDir = MODX_CORE_PATH . 'cache/images/';
    /**
     * @var array
     */
    private $newImagesArray = [];

    /**
     * Синхронизирует товары
     */
    public function process()
    {
        $products = $this->guzzle->getEntities($this->method);
        if (empty($products)) {
            return false;
        }
        $groupClass = new SoftjetGroupUpdater($this->guzzle, $this->modx, $this->config);
        $this->categories = $groupClass->getGroupsFromModx();
        $this->getProductsFromModx();
        foreach ($products as $product){
            if (!empty($this->dbProducts[$product['id']])){
                $this->updateProduct($this->dbProducts[$product['id']], $product);
            }else{
                $this->addProduct($product);
            }
        }
        $this->insertBarcodes();
        //$this->updateImages();
    }
    public function newUpdateImages(){
        /*
            $res = $this->modx->getObject("msProduct", 93571);
            if (!$res->get("image"))
            $product["img"]= "http://31.13.130.63/softjet_pic/products/63feeead-63b8-11eb-80dd-005056aebb19/Cactus Gemengd Showdoos.jpg";
            $basename = basename($product["img"]);
            $fileImage = urlencode(basename($product["img"]));
            $fileImage = str_replace("+","%20",$fileImage);
            $encodedFileName = str_replace($basename, $fileImage, $product["img"]);
            $response = $this->modx->runProcessor('gallery/upload',
                [
                    'id' => 93571,
                    'file' => $encodedFileName,
                ],
                ['processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/']
            );
            $response->getAllErrors();
            $jej = $response->getAllErrors();
            foreach ($jej as $je){
                echo $je;
                echo '</br>';
            }
            return 1;
        */
        $products = $this->guzzle->getEntities($this->method);
        foreach ($products as $product){
            if ($product["img"] != ""){
                $res = $this->modx->getObject("msProduct", array("alias"=>$product["id"]));
                if (!$res->get("image")){
                    $basename = basename($product["img"]);
                    $fileImage = urlencode(basename($product["img"]));
                    $fileImage = str_replace("+","%20",$fileImage);
                    $encodedFileName = str_replace($basename, $fileImage, $product["img"]);
                    $response = $this->modx->runProcessor('gallery/upload',
                        [
                            'id' => $res->id,
                            'file' => "http://31.13.130.63/softjet_pic".$encodedFileName,
                        ],
                        ['processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/']
                    );
                    $jej = $response->getAllErrors();
                    foreach ($jej as $je){
                        echo $je." file: ".$encodedFileName.'         ';
                    }
                }
            }
        }
    }
    /**
     * Возвращает товары, которые уже есть в modx
     * @return mixed
     */
    public function getProductsFromModx()
    {
        $this->pdo->setConfig(array(
            'class' => 'msProduct',
            'leftJoin' => array('Data' => array('class' => 'msProductData')),
            'select' => array(
                'Data' => $this->modx->getSelectColumns('msProductData', 'Data', '', array('id'), true),
                'msProduct' => $this->modx->getSelectColumns('msProduct', 'msProduct', '', array('content'), true)
            ),
            'return' => 'data',
            'where' => ['class_key' => 'msProduct'],
            'limit' => $this->defaultLimit
        ));
        $products = $this->pdo->run();
        if (empty($products))
            return [];
        $this->dbProducts = array_combine(array_column($products, 'alias'), $products);
        return $this->dbProducts;
    }

    /**
     * Создает новую категорию
     * @param $data
     */
    private function addProduct($data)
    {
        /**
         * @var msProduct $newProduct
         */
        $newProduct = $this->modx->newObject('msProduct');
        $newProduct->fromArray([
            'pagetitle' => $data['title'],
            'description' => $data['desc'],
            'published' => 1,
            'alias' => $data['id'],
            'hidemenu' => true,
            'link_attributes' => $data['parent'],
            'template' => $this->config['modx_product_template_id'],
            'measure' => $data['measure'],
            'quantityinpack' => $data['quantityinpack'],
            'length' => $data['length'],
        ]);
        $parent = $this->config['catalog_parent'];
        if (!empty($data['parent']) && isset($this->categories[$data['parent']])){
            $parent = $this->categories[$data['parent']]['id'];
        }
        $newProduct->set('parent',$parent);

        $newProduct->save();
        $this->pushImages($newProduct, $data['img']);
        $this->pushBarcodes($newProduct, $data['barcodes']);
    }

    /**
     * Создает новый товар
     * @param msProduct $product
     * @param $data
     */
    private function updateProduct($product, $data)
    {
        if ($product['pagetitle'] != $data['title']
            || $product['link_attributes'] != $data['parent']
            || $product['measure'] != $data['measure']
            || $product['quantityinpack'] != $data['quantityinpack']
            || $product['length'] != $data['length']
            || $product['description'] != $data['desc']
        ){
            /**
             * @var msProduct $product
             */
            $product = $this->modx->getObject('msProduct', $product['id']);
            $product->fromArray([
                'pagetitle' => $data['title'],
                'link_attributes' => $data['parent'],
                'measure' => $data['measure'],
                'quantityinpack' => $data['quantityinpack'],
                'length' => $data['length'],
                'description' => $data['desc'],
            ]);
            if ($product->link_attributes != $data['parent'] && isset($this->categories[$data['parent']])){
                $product->set('parent', $this->categories[$data['parent']]['id']);
            }
            $product->save();
        }
        $this->pushImages($product, $data['img']);
        $this->pushBarcodes($product, $data['barcodes']);
    }

    protected function pushBarcodes($product, $barcodes)
    {
        $productId = is_object($product) ? $product->id : $product['id'];
        $productUuid = is_object($product) ? $product->alias : $product['alias'];
        if (empty($barcodes) || !isset($this->dbProducts[$productUuid]))
            return false;
        foreach ($barcodes as $barcode){
            if (empty($barcode))
                continue;
            $this->barcodes[] = [
                'product_id' => $productId,
                'product_uuid' => $productUuid,
                'barcode' => $barcode
            ];
        }
    }

    protected function insertBarcodes()
    {
        if (empty($this->barcodes))
            return;
        $barcodeTable = $this->modx->getTableName('SoftjetsyncBarcodes');
        $sql = "INSERT IGNORE INTO {$barcodeTable} (product_id,product_uuid,barcode) VALUES ";
        $sqlBarcodes = [];
        foreach ($this->barcodes as $barcode){
            $sqlBarcodes[] = "({$barcode['product_id']}, '{$barcode['product_uuid']}', '{$barcode['barcode']}')";
        }
        $sql .= implode(',', $sqlBarcodes) . ';';
        $this->modx->exec($sql);
    }

    public static function customUrlEncode($string)
    {
        $arr = ["~" => "%7E","`" => "60%","'" => "27%","\"" => "22%","@" => "40%","?" => "%3F","!" => "21%","#" => "23%","№" => "%B9","$" => "24%","%" => "25%","^" => "%5E","&amp;" => "26%","+" => "%2B","*" => "%2A",":" => "%3A","," => "%2C","(" => "28%",")" => "29%","{" => "%7B","}" => "%7D","[" => "%5B","]" => "%5D","&lt;" => "%3C","&gt;" => "%3E","/" => "%2F","" => "%5C","А" => "%C0","а" => "%E0","Б" => "%C1","б" => "%E1","В" => "%C2","в" => "%E2","Г" => "%C3","г" => "%E3","Д" => "%C4","д" => "%E4","Е" => "%C5","е" => "%E5","Ё" => "%A8","ё" => "%B8","Ж" => "%C6","ж" => "%E6","З" => "%C7","з" => "%E7","И" => "%C8","и" => "%E8","Й" => "%C9","й" => "%E9","К" => "%CA","к" => "%EA","Л" => "%CB","л" => "%EB","М" => "%CC","м" => "%EC","Н" => "%CD","н" => "%ED","О" => "%CE","о" => "%EE","П" => "%CF","п" => "%EF","Р" => "%D0","р" => "%F0","С" => "%D1","с" => "%F1","Т" => "%D2","т" => "%F2","У" => "%D3","у" => "%F3","Ф" => "%D4","ф" => "%F4","Х" => "%D5","х" => "%F5","Ц" => "%D6","ц" => "%F6","Ч" => "%D7","ч" => "%F7","Ш" => "%D8","ш" => "%F8","Щ" => "%D9","щ" => "%F9","Ъ" => "%DA","ъ" => "%FA","Ы" => "%DB","ы" => "%FB","Ь" => "%DC","ь" => "%FC","Э" => "%DD","э" => "%FD","Ю" => "%DE","ю" => "%FE","Я" => "%DF","я" => "%FF"];
        $bn = basename($string);
        return str_replace($bn, str_replace(array_keys($arr),array_values($arr),$bn), $string);
    }

    protected function pushImages($product, $image)
    {
        $productId = is_object($product) ? $product->id : $product['id'];
        $productImage = is_object($product) ? basename($product->image) : basename($product['image']);
        $productUuid = is_object($product) ? $product->alias : $product['alias'];
        if (basename($image) != $productImage){
            $this->newImagesArray[] = [
                'product_id' => $productId,
                'uuid' => $productUuid,
                'ftp_image_path' => $image,
                'name' => basename($image)
            ];
        }
    }

    protected function updateImages()
    {
        $cacheDir = $this->imagesCacheDir;
        if (!file_exists($cacheDir)){
            mkdir($cacheDir);
        }
        $client = new Suin_FTPClient_FTPClient($this->config['ftp_url']);
        if ($client->login($this->config['ftp_user'], $this->config['ftp_pass']) === false) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "Cannot login to FTP!");
            return false;
        }
        foreach ($this->newImagesArray as $image){
            $tmp = pathinfo($image['ftp_image_path']);
            if ( ($dir = $client->getList($tmp['dirname'])) === false )
            {
                $this->modx->log(modX::LOG_LEVEL_ERROR, "Not found image for product id = {$image['product_id']}");
                continue;
            }
            if (!$client->download($dir[0], $cacheDir . $image['name'], Suin_FTPClient_FTPClient::MODE_BINARY)){
                $this->modx->log(modX::LOG_LEVEL_ERROR, "Cannot download image for product id = {$image['product_id']}");
                continue;
            }

            $this->modx->runProcessor('gallery/upload',
                [
                    'id' => $image['product_id'],
                    'file' => $cacheDir . $image['name'],
                    'name'=> $image['name'],
                ],
                ['processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/']
            );
        }
        $client->disconnect();
        $this->cleanCachedImages();
    }

    protected function cleanCachedImages()
    {
        $cacheDir = $this->imagesCacheDir;
        if (!file_exists($cacheDir)){
            return true;
        }
        foreach (scandir($cacheDir) as $file){
            if (in_array($file, ['.', '..']))
                continue;
            @unlink($cacheDir . $file);
        }
    }
}
