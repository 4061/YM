<?php
$xpdo_meta_map['SoftjetsyncPrices']= array (
  'package' => 'softjetsync',
  'version' => '1.1',
  'table' => 'softjetsync_prices',
  'extends' => 'xPDOSimpleObject',
  'tableMeta' => 
  array (
    'engine' => 'InnoDB',
  ),
  'fields' => 
  array (
    'product_id' => NULL,
    'product_uuid' => NULL,
    'type' => NULL,
    'price' => 0.0,
  ),
  'fieldMeta' => 
  array (
    'product_id' => 
    array (
      'dbtype' => 'int',
      'phptype' => 'integer',
      'null' => false,
      'index' => 'index',
    ),
    'product_uuid' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '36',
      'phptype' => 'string',
      'null' => false,
      'index' => 'index',
    ),
    'type' => 
    array (
      'dbtype' => 'varchar',
      'precision' => '36',
      'phptype' => 'string',
      'null' => false,
    ),
    'price' => 
    array (
      'dbtype' => 'decimal',
      'precision' => '12,2',
      'phptype' => 'float',
      'null' => false,
      'default' => 0.0,
    ),
  ),
  'indexes' => 
  array (
    'softjetsync_prices_product_uuid_IDX' => 
    array (
      'alias' => 'softjetsync_prices_product_uuid_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'product_uuid' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'modx_softjetsync_prices_product_id_IDX' => 
    array (
      'alias' => 'modx_softjetsync_prices_product_id_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'product_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'modx_softjetsync_prices_product_uuid_IDX' => 
    array (
      'alias' => 'modx_softjetsync_prices_product_uuid_IDX',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'product_uuid' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
        'type' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
    'combo' => 
    array (
      'alias' => 'combo',
      'primary' => false,
      'unique' => false,
      'type' => 'BTREE',
      'columns' => 
      array (
        'product_id' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
        'type' => 
        array (
          'length' => '',
          'collation' => 'A',
          'null' => false,
        ),
      ),
    ),
  ),
  'aggregates' => 
  array (
    'Type' => 
    array (
      'class' => 'SoftjetsyncPriceTypes',
      'local' => 'product_uuid',
      'foreign' => 'uuid',
      'cardinality' => 'one',
      'owner' => 'foreign',
    ),
  ),
);
