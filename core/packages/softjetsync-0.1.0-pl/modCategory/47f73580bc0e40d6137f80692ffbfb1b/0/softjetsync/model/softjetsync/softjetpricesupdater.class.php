<?php

class SoftjetPricesUpdater extends SoftjetUpdater
{
    protected $method = '/prices/list';
    /**
     * @var array
     */
    protected $remapped;
    /**
     * @var array
     */
    private $priceTypes;
    //новая функция обновления 
    public function priceUpdateNew(){
        $pdoFetch = new pdoFetch($this->modx);
        $orgs = array(
            'class' => 'modResource',
            'parents' => 1802,
            'where' => ['template' => 18],
            'sortdir' => "ASC",
            //'sortby' => "segment_id",
            'return' => 'data',
            'limit' => 10000
        );
        $pdoFetch->setConfig($orgs);
        $orgs = $pdoFetch->run();
        foreach ($orgs as $org){
            $org = $this->modx->getObject("modResource", $org["id"]);
            $orgPrices = $this->guzzle->getEntities($this->method,['headers' => [
                'city' => $org->getTVValue("iiko_organization_key"),
                ]
            ]);
            foreach ($orgPrices as $price){
                $prices[] = $price;
            }
        }
        //$prices1 = $this->guzzle->getEntities($this->method,['headers' => [
            //'city' => $this->config['city_id'],
        //    'city' => "c59aed87-9785-11eb-80e3-005056aebb19",
        //]]);
        //return $prices;
        foreach ($prices as $price){
            foreach ($price["prices"] as $product_price){
                $type = $this->modx->getObject("SoftjetsyncPriceTypes", array("uuid"=>$product_price["type"]));
                if ($price_obj = $this->modx->getObject("SoftjetsyncPrices", array("type"=>$type->id, "product_uuid"=>$price["prod"]))){
                    $price_obj->set("price", $product_price["price"]);
                } else {
                    $prod = $this->modx->getObject("modResource", array("alias"=>$price["prod"]));
                    $price_obj = $this->modx->newObject("SoftjetsyncPrices");
                    $price_obj->set("price", $product_price["price"]);
                    $price_obj->set("type", $type->id);
                    $price_obj->set("product_uuid", $price["prod"]);
                    $price_obj->set("product_id", $prod->id);
                    //echo $price["prod"]." TYPE ".$type->id."</br>";
                }
                $price_obj->save();
            }
            //$product_obj = $this->modx->getObject("modResource", array("alias"=>$price["prod"]));
            
        }
    }
    
    public function process()
    {
        $products = $this->guzzle->getEntities($this->method,['headers' => [
            'city' => $this->config['city_id'],
        ]
        ]);
        $priceTypesClass = new SoftjetPriceTypesUpdater($this->guzzle, $this->modx, $this->config);
        $prodClass = new SoftjetProductsUpdater($this->guzzle, $this->modx, $this->config);
        $this->priceTypes = $priceTypesClass->getCurrentTypes();
        $this->products = $prodClass->getProductsFromModx();
        if (empty($products)) {
            return false;
        }
        $this->getCurrentPrices();
        foreach ($products as $product){
            foreach ($product['prices'] as $price) {
                $key = $product['prod'] . '_' . $this->priceTypes[$price['type']]['id'];
                if (isset($this->remapped[$key])) {
                    $this->updateType($this->remapped[$key], $price, $key);
                } else {
                    $this->addPrice($price, $product['prod']);
                }
            }
        }

    }

    protected function getCurrentPrices()
    {
        $this->pdo->setConfig(array(
            'class' => 'SoftjetsyncPrices',
            'return' => 'data',
            'limit' => $this->defaultLimit
        ));
        $currentPrices = $this->pdo->run();
        if (empty($currentPrices))
            return [];
        $keys = array_map(function ($data) {
            return $data['product_uuid'] . '_' . $data['type'];
        }, $currentPrices);
        $this->remapped = array_combine($keys, $currentPrices);
        return $this->remapped;
    }

    private function updateType($dbPrice, $price, $key)
    {
        if ($dbPrice['price'] != $this->remapped[$key]['price']
        ){
            $objectPrice = $this->modx->getObject('SoftjetsyncPrices', $dbPrice['id']);
            $objectPrice->fromArray([
                'price' => $price['price']
            ]);
            $objectPrice->save();
        }
    }

    private function addPrice($priceType, $productUuid)
    {
        $newPrice = $this->modx->newObject('SoftjetsyncPrices');
        $newPrice->fromArray([
            'product_id' => $this->products[$productUuid]['id'],
            'product_uuid' => $productUuid,
            'price' => $priceType['price'],
            'type' => $this->priceTypes[$priceType['type']]['id']
        ]);
        $newPrice->save();
    }
}
