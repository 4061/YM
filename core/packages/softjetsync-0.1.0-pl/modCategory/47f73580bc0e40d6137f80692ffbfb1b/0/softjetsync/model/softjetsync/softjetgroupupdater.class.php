<?php
class SoftjetGroupUpdater extends SoftjetUpdater
{
    protected $reIndexKey   = 'alias';
    protected $method       = 'goods/groups';
    public $remapped       = [];
    private $notFoundParent = [];


    /**
     * Синхронизирует группы/категории
     */
    public function process()
    {
        $groups = $this->guzzle->getEntities($this->method);
        if (empty($groups)) {
            return false;
        }
        $this->getGroupsFromModx();
        foreach ($groups as $group){
            if (!empty($this->remapped[$group['id']])){
                $this->updateCategory($this->remapped[$group['id']], $group);
            }else{
                $this->addCategory($group);
            }
        }
        $this->reNewParent();
    }

    /**
     * Возвращает группы, которые уже есть в modx
     * @return mixed
     */
    public function getGroupsFromModx()
    {
        $this->pdo->setConfig(array(
            'class' => 'msCategory',
            'return' => 'data',
            'where' => ['class_key' => 'msCategory'],
            'limit' => $this->defaultLimit
        ));
        $categories = $this->pdo->run();
        if (empty($categories))
            return [];
        $this->remapped = array_combine(array_column($categories, 'alias'), $categories);
        return $this->remapped;
    }

    /**
     * Создает новую категорию
     * @param $data
     */
    private function addCategory($data)
    {
        /**
         * @var msCategory $newCategory
         */
        $newCategory = $this->modx->newObject('msCategory');
        $newCategory->fromArray([
            'pagetitle' => $data['name'],
            'alias' => $data['id'],
            'link_attributes' => $data['parent'],
            'published' => 1,
            'template' => $this->config['modx_category_template_id']
        ]);
        $parent = $this->config['catalog_parent'];
        if (!empty($data['parent']) && isset($this->remapped[$data['parent']])){
            $parent = $this->remapped[$data['parent']]['id'];
        }elseif (!empty($data['parent'])){
            /**
             * В конце синхронизации если еще не создан родитель,
             * мы пройдемся по этому массиву и изменим родителя
             */
            $this->notFoundParent[] = &$newCategory;
        }
        $newCategory->set('parent',$parent);

        if ($newCategory->save()){
            $this->remapped[$data['id']] = $newCategory->toArray();
        }
    }

    /**
     * Создает новую категорию
     * @param msCategory $category
     * @param $data
     */
    private function updateCategory($category, $data)
    {
        if ($category['link_attributes'] != $data['parent'] || $category['pagetitle'] != $data['name']) {
            /**
             * @var $category msCategory
             */
            $category = $this->modx->getObject('msCategory', $category['id']);
            $category->fromArray([
                'pagetitle' => $data['name']
            ]);
            $category->set('link_attributes', $data['parent']);
            if (!empty($data['parent']) && !isset($this->remapped[$data['parent']])){
                $this->notFoundParent[] = &$category;
            }else{
                $category->set('parent', $this->remapped[$data['parent']]['id']);
            }
            return $category->save();
        }
        return false;
    }

    /**
     * Обновляет родитилей
     */
    public function reNewParent(){
        foreach ($this->notFoundParent as $category){
            /**
             * @var msCategory $category
             */
            $parentSjId = $category->link_attributes;
            if (isset($this->remapped[$parentSjId])){
                $category->set('parent', $this->remapped[$parentSjId]['id']);
                $category->save();
            }else{
                $this->modx->log(modX::LOG_LEVEL_INFO, 'Cant\'t find parent to ' . $category->id);
            }
        }
    }

}