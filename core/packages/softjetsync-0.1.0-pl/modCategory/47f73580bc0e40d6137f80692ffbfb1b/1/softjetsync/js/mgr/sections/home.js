SoftjetSync.page.Home = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        components: [{
            xtype: 'softjetsync-panel-home',
            renderTo: 'softjetsync-panel-home-div'
        }]
    });
    SoftjetSync.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(SoftjetSync.page.Home, MODx.Component);
Ext.reg('softjetsync-page-home', SoftjetSync.page.Home);