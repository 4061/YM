SoftjetSync.panel.Home = function (config) {
    config = config || {};
    Ext.apply(config, {
        baseCls: 'modx-formpanel',
        layout: 'anchor',
        hideMode: 'offsets',
        items: [{
            html: '<h2>' + _('softjetsync') + '</h2>',
            cls: '',
            style: {margin: '15px 0'}
        }, {
            xtype: 'modx-tabs',
            defaults: {border: false, autoHeight: true},
            border: true,
            hideMode: 'offsets',
            items: [{
                title: 'Синхронизация',
                layout: 'anchor',
                items: [ {
                    xtype: 'button',
                    style:'margin:20px;',
                    margin: 20,
                    text: '<i class=\"icon icon-sitemap\"></i>&nbsp; ' + ' Синхронизировать категории',
                    handler: function (btn, e) {
                        var w = this;

                        MODx.Ajax.request({
                            url: '/assets/components/softjetsync/connector.php',
                            params: {
                                action: 'mgr/synccategories',
                            },
                            listeners: {
                                success: {
                                    fn: function (r) {
                                        // console.log('success email r', r);

                                        MODx.msg.alert('Готово!', r['message']);
                                    }, scope: this
                                },
                                failure: {
                                    fn: function (r) {
                                        MODx.msg.alert('Ошибка', r['message']);
                                        console.error(r);
                                    }, scope: this
                                },
                            }
                        });
                    }
                }, {
                    xtype: 'button',
                    style:'margin:20px;',
                    text: '<i class=\"icon icon-vcard\"></i>&nbsp; ' + ' Синхронизировать товары',
                    handler: function (btn, e) {
                        var w = this;

                        MODx.Ajax.request({
                            url: '/assets/components/softjetsync/connector.php',
                            params: {
                                action: 'mgr/syncproducts',
                            },
                            listeners: {
                                success: {
                                    fn: function (r) {
                                        // console.log('success email r', r);

                                        MODx.msg.alert('Готово!', r['message']);
                                    }, scope: this
                                },
                                failure: {
                                    fn: function (r) {
                                        MODx.msg.alert('Ошибка', r['message']);
                                        console.error(r);
                                    }, scope: this
                                },
                            }
                        });
                    }
                }, {
                    xtype: 'button',
                    style:'margin:20px;',
                    text: '<i class=\"icon icon-braille\"></i>&nbsp; ' + ' Синхронизировать типы цен',
                    handler: function (btn, e) {
                        var w = this;

                        MODx.Ajax.request({
                            url: '/assets/components/softjetsync/connector.php',
                            params: {
                                action: 'mgr/syncpricetypes',
                            },
                            listeners: {
                                success: {
                                    fn: function (r) {
                                        // console.log('success email r', r);

                                        MODx.msg.alert('Готово!', r['message']);
                                    }, scope: this
                                },
                                failure: {
                                    fn: function (r) {
                                        MODx.msg.alert('Ошибка', r['message']);
                                        console.error(r);
                                    }, scope: this
                                },
                            }
                        });
                    }
                }, {
                    xtype: 'button',
                    style:'margin:20px;',
                    text: '<i class=\"icon icon-cloud-download\"></i>&nbsp; ' + ' Синхронизировать остатки',
                    handler: function (btn, e) {
                        var w = this;

                        MODx.Ajax.request({
                            url: '/assets/components/softjetsync/connector.php',
                            params: {
                                action: 'mgr/syncremains',
                            },
                            listeners: {
                                success: {
                                    fn: function (r) {
                                        // console.log('success email r', r);

                                        MODx.msg.alert('Готово!', r['message']);
                                    }, scope: this
                                },
                                failure: {
                                    fn: function (r) {
                                        MODx.msg.alert('Ошибка', r['message']);
                                        console.error(r);
                                    }, scope: this
                                },
                            }
                        });
                    }
                }, {
                    xtype: 'button',
                    style:'margin:20px;',
                    text: '<i class=\"icon icon-dollar\"></i>&nbsp; ' + ' Синхронизировать цены',
                    handler: function (btn, e) {
                        var w = this;

                        MODx.Ajax.request({
                            url: '/assets/components/softjetsync/connector.php',
                            params: {
                                action: 'mgr/syncprices',
                            },
                            listeners: {
                                success: {
                                    fn: function (r) {
                                        // console.log('success email r', r);

                                        MODx.msg.alert('Готово!', r['message']);
                                    }, scope: this
                                },
                                failure: {
                                    fn: function (r) {
                                        MODx.msg.alert('Ошибка', r['message']);
                                        console.error(r);
                                    }, scope: this
                                },
                            }
                        });
                    }
                }]
            },{
                title: 'Логи синхронихации',
                layout: 'anchor',
            }]
        }]
    });
    SoftjetSync.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(SoftjetSync.panel.Home, MODx.Panel);
Ext.reg('softjetsync-panel-home', SoftjetSync.panel.Home);
