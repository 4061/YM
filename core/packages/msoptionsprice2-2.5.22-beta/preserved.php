<?php return array (
  'ae3336f2839073e5c351cd33c27671bd' => 
  array (
    'criteria' => 
    array (
      'name' => 'msoptionsprice',
    ),
    'object' => 
    array (
      'name' => 'msoptionsprice',
      'path' => '{core_path}components/msoptionsprice/',
      'assets_path' => '',
    ),
  ),
  '6eb4de349822c2ebbd28d0369b7fbf64' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_working_templates',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_working_templates',
      'value' => '1,2,3,4',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '47b8b0eaf8cfae8e821f58bbbcc1aaa2' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_zero_cost',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_zero_cost',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '38ee102a6dd422b8e2129e2d72331cd8' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_zero_old_cost',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_zero_old_cost',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '34f097c55fa885ae60dc1c2448ee40be' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_zero_mass',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_zero_mass',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'a8eda123214c85816b4789263b17376a' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_zero_article',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_zero_article',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'f7d0b2fc6e572d627e2782f6ec8d9208' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_zero_count',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_zero_count',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '21ebbe92c243df69a43e795f80236bb3' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_zero_modification',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_zero_modification',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '13467a0ba7090d9f6802b857608bb963' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_allow_remains',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_allow_remains',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '3febff98c6a86f82ee953262e865236f' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_status_pickup_remains',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_status_pickup_remains',
      'value' => '1',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '187d5b6b1a9e212bbcae48c8ff6809ce' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_status_return_remains',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_status_return_remains',
      'value' => '4',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '63d4e1320c72984048878add6b9332ff' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_modification_thumbs',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_modification_thumbs',
      'value' => 'small',
      'xtype' => 'textarea',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'c369ccd37f72c666d3eb981856c23b79' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_modification_gallery_class',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_modification_gallery_class',
      'value' => 'msProductFile',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'dbe1337a2009f3ebfacafc7c89fb9ee8' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_exclude_modification_options',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_exclude_modification_options',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '70de656cdeee51703f809bfa50196614' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_include_modification_options',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_include_modification_options',
      'value' => '',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '98398f660b4cfacb620b77c0e8a529a1' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_sort_modification_option_values',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_sort_modification_option_values',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'd96a6291b0c39bacf564ccb8e2f87eca' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_search_modification_strict',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_search_modification_strict',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '7746ff06ef17b024a6ec31fd2115a2c1' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_search_modification_by_image_strict',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_search_modification_by_image_strict',
      'value' => '',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'd6118128be0414e445805cdb198e33f4' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_create_modification_with_duplicate',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_create_modification_with_duplicate',
      'value' => '1',
      'xtype' => 'combo-boolean',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '51c530098313776281da69464e8a5e43' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_number_format',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_number_format',
      'value' => '[2,1]',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'ca48cd1e1ac0654c11b72775ad4b6edb' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_frontendCss',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_frontendCss',
      'value' => '[[+assetsUrl]]css/web/default.css',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  'b1a8cdd8f2b35eb5dda7dfe7b09c32ef' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_frontendJs',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_frontendJs',
      'value' => '[[+assetsUrl]]js/web/default.js',
      'xtype' => 'textfield',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '42f02c4908c6485e10a2ba573b3430fd' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_window_modification_tabs',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_window_modification_tabs',
      'value' => 'modification',
      'xtype' => 'textarea',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '2c3ca6bc890d29da341822d4239b0a5c' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_grid_modification_fields',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_grid_modification_fields',
      'value' => 'id,type,price,old_price,article,weight,count,image',
      'xtype' => 'textarea',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '63573e3e4b89affb0b8b2806c92efab9' => 
  array (
    'criteria' => 
    array (
      'key' => 'msoptionsprice_window_modification_fields',
    ),
    'object' => 
    array (
      'key' => 'msoptionsprice_window_modification_fields',
      'value' => 'name,price,old_price,image,article,weight,count',
      'xtype' => 'textarea',
      'namespace' => 'msoptionsprice',
      'area' => 'msoptionsprice_main',
      'editedon' => NULL,
    ),
  ),
  '222bebef72c8a9d9f949e11bab1d6769' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnBeforeGetModification',
    ),
    'object' => 
    array (
      'name' => 'msopOnBeforeGetModification',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '7b9977dba34110021bf0192cd4281ea1' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnAfterGetModification',
    ),
    'object' => 
    array (
      'name' => 'msopOnAfterGetModification',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'dd7f070547b1267760ab12af909f448e' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnBeforeGetCost',
    ),
    'object' => 
    array (
      'name' => 'msopOnBeforeGetCost',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '53ecdc9258dfc24838746dca3cb19b18' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnAfterGetCost',
    ),
    'object' => 
    array (
      'name' => 'msopOnAfterGetCost',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'b3ee7967973a76ed3025c7bd6d35aa37' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnBeforeGetMass',
    ),
    'object' => 
    array (
      'name' => 'msopOnBeforeGetMass',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '5cde73b1f3385f51186a7af9e8541668' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnAfterGetMass',
    ),
    'object' => 
    array (
      'name' => 'msopOnAfterGetMass',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'af145ece01e9fb9716e03f8f234a7f01' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnGetFullCost',
    ),
    'object' => 
    array (
      'name' => 'msopOnGetFullCost',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'aaf249e4d2ec426e4317edb5b25ff526' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnGetFullMass',
    ),
    'object' => 
    array (
      'name' => 'msopOnGetFullMass',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'f0047aa4981787587d813e257c7d915e' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnGetModificationById',
    ),
    'object' => 
    array (
      'name' => 'msopOnGetModificationById',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'ee9a5b033f4236c0fd3709af160e513d' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnModificationNotFound',
    ),
    'object' => 
    array (
      'name' => 'msopOnModificationNotFound',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '5b1c8100e21f39bf2b0458a7f06d9ad0' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnModificationBeforeSave',
    ),
    'object' => 
    array (
      'name' => 'msopOnModificationBeforeSave',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'cfcf737b560c8df96c203ce1ae87ab24' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnModificationSave',
    ),
    'object' => 
    array (
      'name' => 'msopOnModificationSave',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '143d43348df871299ee8b5f93520f846' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnModificationBeforeRemove',
    ),
    'object' => 
    array (
      'name' => 'msopOnModificationBeforeRemove',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  'd46fca653b49c2e7c52f2a94d1bdfe6a' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnModificationRemove',
    ),
    'object' => 
    array (
      'name' => 'msopOnModificationRemove',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '4e0b55460cbd30d6086e88272141501c' => 
  array (
    'criteria' => 
    array (
      'name' => 'msopOnManagerPrepareObjectData',
    ),
    'object' => 
    array (
      'name' => 'msopOnManagerPrepareObjectData',
      'service' => 6,
      'groupname' => 'msOptionsPrice2',
    ),
  ),
  '7ed04aac6580cb44f2136d877c2fa001' => 
  array (
    'criteria' => 
    array (
      'name' => 'tpl.msOptionsPrice.modification',
    ),
    'object' => 
    array (
      'id' => 49,
      'source' => 1,
      'property_preprocess' => 0,
      'name' => 'tpl.msOptionsPrice.modification',
      'description' => '',
      'editor_type' => 0,
      'category' => 0,
      'cache_type' => 0,
      'snippet' => '<div class="row ms2_product">
	<div class="col-md-8">
		<form method="post" class="ms2_form">
			<a href="{$rid | url}">{$product_pagetitle}</a>

			{if $_pls[\'small\']?}
				<img src="{$_pls[\'small\']}" alt="{$product_pagetitle}" title="{$product_pagetitle}"/>
			{else}
				<img src="{\'assets_url\' | option}components/minishop2/img/web/ms2_small.png"
					 srcset="{\'assets_url\' | option}components/minishop2/img/web/ms2_small@2x.png 2x"
					 alt="{$product_pagetitle}" title="{$product_pagetitle}"/>
			{/if}

            <span class="flags">
                {if $data_new?}
					<i class="glyphicon glyphicon-flag" title="{\'ms2_frontend_new\' | lexicon}"></i>
				{/if}
				{if $data_popular?}
					<i class="glyphicon glyphicon-star" title="{\'ms2_frontend_popular\' | lexicon}"></i>
				{/if}
				{if $data_favorite?}
					<i class="glyphicon glyphicon-bookmark" title="{\'ms2_frontend_favorite\' | lexicon}"></i>
				{/if}
            </span>

			<span class="options">
				{if $options?}
					<span class="small">
						{$options | join : \'; \'}
					</span>
				{/if}
			</span>

            <span class="price">
                {$price} {\'ms2_frontend_currency\' | lexicon}
            </span>
			{if $old_price?}
				<span class="old_price">{$old_price} {\'ms2_frontend_currency\' | lexicon}</span>
			{/if}
			{if $article?}
				<span class="article">{\'ms2_product_article\' | lexicon}: {$article}</span>
			{/if}
			{if $weight?}
				<span class="weight">{\'ms2_product_weight\' | lexicon}
					: {$weight} {\'ms2_frontend_weight_unit\' | lexicon}</span>
			{/if}

			<input type="hidden" name="id" value="{$rid}">
			<input type="hidden" name="count" value="1">
			<input type="hidden" name="options" value="[]">
			{foreach $options as $name => $value}
				<input type="hidden" name="options[{$name}]" value="{$value}">
			{/foreach}

			<button class="btn btn-default btn-sm pull-right" type="submit" name="ms2_action" value="cart/add">
				<i class="glyphicon glyphicon-barcode"></i> {\'ms2_frontend_add_to_cart\' | lexicon}
			</button>

		</form>
	</div>
</div>',
      'locked' => 0,
      'properties' => NULL,
      'static' => 0,
      'static_file' => 'core/components/msoptionsprice/elements/chunks/chunk.modification.tpl',
      'content' => '<div class="row ms2_product">
	<div class="col-md-8">
		<form method="post" class="ms2_form">
			<a href="{$rid | url}">{$product_pagetitle}</a>

			{if $_pls[\'small\']?}
				<img src="{$_pls[\'small\']}" alt="{$product_pagetitle}" title="{$product_pagetitle}"/>
			{else}
				<img src="{\'assets_url\' | option}components/minishop2/img/web/ms2_small.png"
					 srcset="{\'assets_url\' | option}components/minishop2/img/web/ms2_small@2x.png 2x"
					 alt="{$product_pagetitle}" title="{$product_pagetitle}"/>
			{/if}

            <span class="flags">
                {if $data_new?}
					<i class="glyphicon glyphicon-flag" title="{\'ms2_frontend_new\' | lexicon}"></i>
				{/if}
				{if $data_popular?}
					<i class="glyphicon glyphicon-star" title="{\'ms2_frontend_popular\' | lexicon}"></i>
				{/if}
				{if $data_favorite?}
					<i class="glyphicon glyphicon-bookmark" title="{\'ms2_frontend_favorite\' | lexicon}"></i>
				{/if}
            </span>

			<span class="options">
				{if $options?}
					<span class="small">
						{$options | join : \'; \'}
					</span>
				{/if}
			</span>

            <span class="price">
                {$price} {\'ms2_frontend_currency\' | lexicon}
            </span>
			{if $old_price?}
				<span class="old_price">{$old_price} {\'ms2_frontend_currency\' | lexicon}</span>
			{/if}
			{if $article?}
				<span class="article">{\'ms2_product_article\' | lexicon}: {$article}</span>
			{/if}
			{if $weight?}
				<span class="weight">{\'ms2_product_weight\' | lexicon}
					: {$weight} {\'ms2_frontend_weight_unit\' | lexicon}</span>
			{/if}

			<input type="hidden" name="id" value="{$rid}">
			<input type="hidden" name="count" value="1">
			<input type="hidden" name="options" value="[]">
			{foreach $options as $name => $value}
				<input type="hidden" name="options[{$name}]" value="{$value}">
			{/foreach}

			<button class="btn btn-default btn-sm pull-right" type="submit" name="ms2_action" value="cart/add">
				<i class="glyphicon glyphicon-barcode"></i> {\'ms2_frontend_add_to_cart\' | lexicon}
			</button>

		</form>
	</div>
</div>',
    ),
  ),
);