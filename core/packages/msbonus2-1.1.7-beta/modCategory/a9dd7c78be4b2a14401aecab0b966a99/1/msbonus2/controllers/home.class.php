<?php

class msBonus2HomeManagerController extends modExtraManagerController
{
    /** @var msBonus2 $msb2 */
    public $msb2;

    /**
     *
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');

        parent::initialize();
    }

    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('msbonus2:default');
    }

    /**
     * @return bool
     */
    public function checkPermissions()
    {
        return true;
    }

    /**
     * @return null|string
     */
    public function getPageTitle()
    {
        return $this->modx->lexicon('msbonus2');
    }

    /**
     * @return void
     */
    public function loadCustomCssJs()
    {
        $this->msb2->loadManagerScripts();
    }

    /**
     * @return string
     */
    public function getTemplateFile()
    {
        return $this->msb2->config['templatesPath'] . 'home.tpl';
    }
}