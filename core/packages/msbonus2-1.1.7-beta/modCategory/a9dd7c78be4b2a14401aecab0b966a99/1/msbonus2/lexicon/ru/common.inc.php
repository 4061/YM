<?php

// Табы
$_lang['msb2_tab_main'] = 'Основное';

// // Названия столбцов
$_lang['msb2_grid_id'] = 'ID';
$_lang['msb2_grid_idx'] = 'idx';
$_lang['msb2_grid_actions'] = 'Действия';

// Кнопки
$_lang['msb2_button_create'] = 'Добавить';
$_lang['msb2_button_update'] = 'Редактировать';
$_lang['msb2_button_enable'] = 'Включить';
$_lang['msb2_button_enable_multiple'] = 'Включить выбранное';
$_lang['msb2_button_disable'] = 'Выключить';
$_lang['msb2_button_disable_multiple'] = 'Выключить выбранное';
$_lang['msb2_button_remove'] = 'Удалить';
$_lang['msb2_button_remove_multiple'] = 'Удалить выбранное';

// Заголовки окон
$_lang['msb2_window_create'] = 'Создать';
$_lang['msb2_window_update'] = 'Редактировать';

// Подтверждения
$_lang['msb2_confirm_other'] = 'Вы уверены?';
$_lang['msb2_confirm_remove'] = 'Вы уверены, что хотите удалить объект(-ы)?';

// Ошибки общие
$_lang['msb2_err_unexpected'] = 'Непредвиденная ошибка.';
$_lang['msb2_err_required'] = 'Это поле необходимо заполнить.';
$_lang['msb2_err_unique'] = 'Это поле должно быть уникальным.';
$_lang['msb2_err_ns'] = 'Объект не указан.';
$_lang['msb2_err_nf'] = 'Объект не найден.';

// ComboBox
$_lang['msb2_combo_list_empty'] = 'Выпадающий список пуст...';