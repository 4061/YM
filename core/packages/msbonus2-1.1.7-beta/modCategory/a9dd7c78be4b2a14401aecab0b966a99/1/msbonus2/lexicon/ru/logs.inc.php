<?php

//
$_lang['msb2_logs_-order_writeoff'] = 'Списание по заказу [[+order_num]]';
$_lang['msb2_logs_+order_writeoff'] = 'Возврат по заказу [[+order_num]]';

//
$_lang['msb2_logs_+order_accrual'] = 'Начисление по заказу [[+order_num]]';
$_lang['msb2_logs_-order_accrual'] = 'Отмена начисления по заказу [[+order_num]]';
$_lang['msb2_logs_-order_accrual_expired'] = 'Списание устаревших баллов за заказ [[+order_num]]';

//
$_lang['msb2_logs_+dob'] = 'В честь дня рождения';
$_lang['msb2_logs_-dob_expired'] = 'Списание устаревших баллов за день рождения';

//
$_lang['msb2_logs_+signup'] = 'Стартовые баллы';
$_lang['msb2_logs_-signup_expired'] = 'Списание устаревших баллов за регистрацию';

//
$_lang['msb2_logs_-expired'] = 'Списание устаревших баллов';
