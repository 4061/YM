<?php

/**
 *
 */
class msb2OnMODXInit extends msb2Plugin
{
    /**
     * @var msb2Manager $manager
     */
    protected $manager;

    // /**
    //  * @param msBonus2 $msb2
    //  * @param array    $sp
    //  */
    // public function __construct(msBonus2 &$msb2, array &$sp)
    // {
    //     parent::__construct($msb2, $sp);
    // }

    /**
     *
     */
    public function run()
    {
        //
        $this->modx->loadClass('msb2User');
        $this->modx->loadClass('msb2Level');
        $this->modx->loadClass('msb2Log');
        $this->modx->loadClass('msb2Order');
        if (!isset($this->modx->map['msb2User'])) {
            return;
        }

        /**
         *
         */
        $this->map([
            'modUser' => [
                'composites' => [
                    'msb2User' => [
                        'class' => 'msb2User',
                        'local' => 'id',
                        'foreign' => 'user',
                        'cardinality' => 'many',
                        'owner' => 'local',
                    ],
                ],
            ],
            'msOrder' => [
                'composites' => [
                    'msb2Order' => [
                        'class' => 'msb2Order',
                        'local' => 'id',
                        'foreign' => 'order',
                        'cardinality' => 'one',
                        'owner' => 'local',
                    ],
                    'msb2Log' => [
                        'class' => 'msb2Log',
                        'local' => 'id',
                        'foreign' => 'order',
                        'cardinality' => 'many',
                        'owner' => 'local',
                    ],
                ],
            ],
        ]);

        //
        $this->manager = $this->msb2->getManager();

        /**
         * Set points for birthday
         */
        $this->accrualPointsForBirthday();

        /**
         * Reserved points to bonus balance
         */
        $q = $this->modx->newQuery('msb2Log')
            ->where([
                'user:!=' => 0,
                'activatedon:!=' => 0,
                'activatedon:<' => time(),
            ])
        ;
        if ($msb2Logs = $this->modx->getCollection('msb2Log', $q)) {
            /** @var msb2Log $msb2Log */
            foreach ($msb2Logs as $msb2Log) {
                if ($msb2User = $this->manager->getJoinedUser($msb2Log->get('user'))) {
                    $amount = $msb2Log->get('amount');
                    $points = $msb2User->get('points') + $amount;
                    $reserve = $msb2User->get('reserve') - $amount;

                    $msb2User->set('points', $points);
                    $msb2User->set('reserve', $reserve);
                    $msb2User->save();

                    $msb2Log->set('activatedon', 0);
                    $msb2Log->save();
                }
            }
        }

        /**
         * Write off expired bonus points
         */
        // todo: add field "used" with amount of used bonus... with expired = 0
        $lifetime_points = $this->modx->getOption('msb2_lifetime_for_bonus');
        $lifetime_points = $this->msb2->tools->isJSON($lifetime_points) ? $this->modx->fromJSON($lifetime_points) : [];
        if (!empty($lifetime_points)) {
            foreach ($lifetime_points as $lifetime_type => $lifetime_value) {
                if (empty($lifetime_type) || empty($lifetime_value)) {
                    continue;
                }
                $q = $this->modx->newQuery('msb2Log')
                    ->where([
                        'user:!=' => 0,
                        'action' => '+',
                        'type' => $lifetime_type,
                        'createdon:<' => time() - $lifetime_value,
                        'expiredon' => 0,
                    ])
                ;
                if ($msb2Logs = $this->modx->getCollection('msb2Log', $q)) {
                    /** @var msb2Log $msb2Log */
                    foreach ($msb2Logs as $msb2Log) {
                        if ($msb2User = $this->manager->getJoinedUser($msb2Log->get('user'))) {
                            $amount = $msb2Log->get('amount');
                            $points = $msb2User->get('points') - $amount;
                            if ($points < 0) {
                                $points = 0;
                            }
                            $msb2User->set('points', $points);
                            $msb2User->save();

                            $msb2Log->set('expiredon', time());
                            $msb2Log->save();

                            //
                            $this->modx->newObject('msb2Log', [
                                'user' => $msb2Log->get('user'),
                                'order' => $msb2Log->get('order'),
                                'action' => '-',
                                'type' => $msb2Log->get('type') . '_expired',
                                'amount' => $amount,
                                'createdon' => time(),
                                'createdby' => $msb2Log->get('createdby'),
                                'activatedon' => 0,
                            ])->save();
                        }
                    }

                    // $this->modx->log(1, print_r($lifetime_type . ': ' . count($msb2Logs), 1));
                }
            }
        }
    }

    /**
     * ...
     *
     * @return mixed|void
     */
    protected function accrualPointsForBirthday()
    {
        //
        $amount = (float)$this->modx->getOption('msb2_bonus_for_birthday') ?: 0;
        if (empty($amount)) {
            return;
        }
        $accrual_days = (int)$this->modx->getOption('msb2_bonus_for_birthday_days') ?: 0;
        $accrual_seconds = ($accrual_days * -1) * 86400;

        //
        $condition = [];
        $condition_part_dob = 'IF(Profile.dob != 0, DATE_FORMAT(FROM_UNIXTIME(Profile.dob), "%m%d"), "0000")';
        $monthday_accrual = date('md', time() + $accrual_seconds); // месяцдень начисления
        $monthday_current = date('md'); // месяцдень текущий
        if ($accrual_days < 0) {
            $condition[] = $condition_part_dob . ' <= "' . $monthday_accrual . '"';
            $condition[] = $condition_part_dob . ' >= "' . $monthday_current . '"';
            $condition[] = '(Log.createdon IS NULL OR Log.createdon <= ' . strtotime(date('Y-m-d 23:59:59', time() + $accrual_seconds) . ' -1 year') . ')';
        } else {
            $condition[] = $condition_part_dob . ' = "' . $monthday_accrual . '"';
            $condition[] = '(Log.createdon IS NULL OR Log.createdon <= ' . strtotime(date('Y-m-d 23:59:59') . ' -1 year') . ')';
        }

        // Get users with birthday and set birthday-bonus for each user
        $q = $this->modx->newQuery('modUser')
            ->innerJoin('modUserProfile', 'Profile')
            ->leftJoin('msb2Log', 'Log', join(' AND ', [
                'Log.user = modUser.id',
                'Log.type = "dob"',
                'Log.createdon > ' . strtotime(date('Y-m-d 23:59:59') . ' -1 year'),
            ]))
            ->select([
                'modUser.id',
                'Profile.email',
                // 'Log.id as log_id',
            ])
            ->where($condition)
            ->groupby('modUser.id')
        ;
        if ($q->prepare()->execute()) {
            if ($users = $q->stmt->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($users as $user) {
                    if (!$this->manager->isExists('+', 'dob', $user['id'], 0, [
                        'createdon:>=' => strtotime(date('Y-m-d 00:00:00')),
                        'createdon:<=' => strtotime(date('Y-m-d 23:59:59')),
                    ])) {
                        $this->manager->setPlus('dob', $amount, $user['id'], 0, $user['id']);
                        // todo: Send email to user
                    }
                }
            }
        }

        // // Get users with birthday and set birthday-bonus for each user
        // $q = $this->modx->newQuery('modUser')
        //     ->innerJoin('modUserProfile', 'Profile')
        //     ->leftJoin('msb2Log', 'Log', join(' AND ', [
        //         'Log.user = modUser.id',
        //         'Log.type = "dob"',
        //         'Log.createdon > ' . strtotime(date('Y-m-d 23:59:59') . ' -1 year'),
        //     ]))
        //     ->select([
        //         'modUser.id',
        //         'Profile.email',
        //     ])
        //     ->where([
        //         'IF(Profile.dob != 0, DATE_FORMAT(FROM_UNIXTIME(Profile.dob), "%m%d"), "0000") = "' . date('md') . '"',
        //         '(Log.createdon IS NULL OR Log.createdon <= ' . strtotime(date('Y-m-d 23:59:59') . ' -1 year') . ')',
        //     ])
        //     ->groupby('modUser.id')
        // ;
        // if ($q->prepare()->execute()) {
        //     if ($users = $q->stmt->fetchAll(PDO::FETCH_ASSOC)) {
        //         foreach ($users as $user) {
        //             if (!$this->manager->isExists('+', 'dob', $user['id'], 0, [
        //                 'createdon:>=' => strtotime(date('Y-m-d 00:00:00')),
        //                 'createdon:<=' => strtotime(date('Y-m-d 23:59:59')),
        //             ])) {
        //                 $this->manager->setPlus('dob', $amount, $user['id'], 0, $user['id']);
        //                 // todo: Send email to user
        //             }
        //         }
        //     }
        // }
    }

    /**
     * Extended MODX map data
     *
     * @param array $map
     *
     * @return bool
     */
    public function map(array $map = [])
    {
        foreach ($map as $class => $data) {
            $this->modx->loadClass($class);

            foreach ($data as $tmp => $fields) {
                if ($tmp == 'fields') {
                    foreach ($fields as $field => $value) {
                        foreach (array('fields', 'fieldMeta', 'indexes') as $key) {
                            if (isset($data[$key][$field])) {
                                $this->modx->map[$class][$key][$field] = $data[$key][$field];
                            }
                        }
                    }
                } elseif ($tmp == 'composites' || $tmp == 'aggregates') {
                    foreach ($fields as $alias => $relation) {
                        if (!isset($this->modx->map[$class][$tmp][$alias])) {
                            $this->modx->map[$class][$tmp][$alias] = $relation;
                        }
                    }
                }
            }
        }

        return true;
    }
}