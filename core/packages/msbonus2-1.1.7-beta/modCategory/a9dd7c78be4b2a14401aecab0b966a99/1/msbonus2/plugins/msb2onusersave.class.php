<?php

/**
 * Create msb2User object for new users
 */
class msb2OnUserSave extends msb2Plugin
{
    /**
     * @var msb2Manager $manager
     */
    protected $manager;

    /**
     * @param msBonus2 $msb2
     * @param array    $sp
     */
    public function __construct(msBonus2 &$msb2, array &$sp)
    {
        parent::__construct($msb2, $sp);

        $this->manager = $this->msb2->getManager();
    }

    /**
     *
     */
    public function run()
    {
        // Only new users
        if ($this->sp['mode'] === 'new') {
            /** @var modUser $user */
            $user = &$this->sp['user'];
            if (!is_object($user)) {
                return;
            }
            $msb2User = $this->manager->getJoinedUser($user->get('id'));
        }
    }
}