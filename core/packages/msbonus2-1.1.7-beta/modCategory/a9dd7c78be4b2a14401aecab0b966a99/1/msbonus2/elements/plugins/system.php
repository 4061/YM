<?php
/** @var modX $modx */
/** @var msBonus2 $msb2 */
/** @var array $scriptProperties */
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/')) {
    return;
}

//
$exists_group = false;
$exists_single = false;

//
$className = 'msb2' . $modx->event->name;
$modx->loadClass('msb2Plugin', $msb2->config['pluginsPath'], true, true);
$modx->loadClass('msb2PluginGroup', $msb2->config['pluginsPath'], true, true);
if (file_exists($msb2->config['pluginsPath'] . strtolower($className) . '.class.php')) {
    $modx->loadClass($className, $msb2->config['pluginsPath'], true, true);
}

//
if (class_exists('msb2PluginGroup')) {
    /** @var msb2PluginGroup $handlerGroup */
    $handlerGroup = new msb2PluginGroup($msb2, $scriptProperties);
    if ($exists_group = method_exists($handlerGroup, $modx->event->name)) {
        $handlerGroup->{$modx->event->name}();
    }
    unset($handlerGroup);
}

//
if ($exists_single = class_exists($className)) {
    /** @var msb2Plugin $handlerSingle */
    $handlerSingle = new $className($msb2, $scriptProperties);
    $handlerSingle->run();
    unset($handlerSingle);
}

// Удаляем событие у плагина, если такого класса не существует
if ($exists_group === false && $exists_single === false) {
    if ($event = $modx->getObject('modPluginEvent', array(
        'pluginid' => $modx->event->plugin->get('id'),
        'event' => $modx->event->name,
    ))) {
        $event->remove();
    }
}
return;