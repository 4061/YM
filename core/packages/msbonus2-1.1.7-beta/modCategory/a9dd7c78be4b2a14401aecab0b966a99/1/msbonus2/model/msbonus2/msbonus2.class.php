<?php

class msBonus2
{
    public $version = '1.1.7';
    public $config = [];
    public $initialized = [];
    /**
     * @var modX $modx
     */
    public $modx;
    /**
     * @var msb2Tools $tools
     */
    public $tools;
    /**
     * @var pdoTools $pdoTools
     */
    public $pdoTools;
    /**
     * @var pdoFetch $pdoFetch
     */
    public $pdoFetch;
    /**
     * @var miniShop2 $miniShop2
     */
    public $miniShop2;
    /**
     * @var msOptionsPrice $msOptionsPrice
     */
    public $msOptionsPrice;
    /**
     * @var msb2Manager $manager
     */
    public $manager;

    /**
     * @param modX  $modx
     * @param array $config
     */
    function __construct(modX &$modx, array $config = [])
    {
        $this->modx = &$modx;

        $corePath = $this->modx->getOption('msb2_core_path', $config, MODX_CORE_PATH . 'components/msbonus2/');
        $assetsUrl = $this->modx->getOption('msb2_assets_url', $config, MODX_ASSETS_URL . 'components/msbonus2/');
        $assetsPath = $this->modx->getOption('msb2_assets_path', $config, MODX_ASSETS_PATH . 'components/msbonus2/');

        $this->config = array_merge([
            'assetsUrl' => $assetsUrl,
            'assetsPath' => $assetsPath,
            'cssUrl' => $assetsUrl . 'css/',
            'jsUrl' => $assetsUrl . 'js/',
            'imagesUrl' => $assetsUrl . 'images/',
            'connectorUrl' => $assetsUrl . 'connector.php',
            'actionUrl' => $assetsUrl . 'action.php',

            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'vendorPath' => $corePath . 'vendor/',
            'pluginsPath' => $corePath . 'plugins/',
            'handlersPath' => $corePath . 'handlers/',
            'templatesPath' => $corePath . 'elements/templates/',
            'processorsPath' => $corePath . 'processors/',

            'prepareResponse' => false,
            'jsonResponse' => false,
        ], $config);

        $this->modx->addPackage('msbonus2', $this->config['modelPath']);
        $this->modx->lexicon->load('msbonus2:default');
    }

    /**
     * @param string $ctx
     * @param array  $sp
     *
     * @return boolean
     */
    public function initialize($ctx = 'web', $sp = [])
    {
        $this->config = array_merge($this->config, $sp, ['ctx' => $ctx]);

        $this->getTools();
        if ($pdoTools = $this->getPdoTools()) {
            // $pdoTools->setConfig($this->config);
        }
        if ($pdoFetch = $this->getPdoFetch()) {
            // $pdoFetch->setConfig($this->config);
        }

        if (empty($this->initialized[$ctx])) {
            switch ($ctx) {
                case 'mgr':
                    break;
                default:
                    // if (!defined('MODX_API_MODE') || !MODX_API_MODE) {
                    //     $this->loadFrontendScripts();
                    // }
                    break;
            }
        }

        return ($this->initialized[$ctx] = true);
    }

    /**
     * @param string $objectName
     * @param array  $sp
     *
     * @return bool
     */
    public function loadFrontendScripts($objectName = '', array $sp = [])
    {
        $version = time(); // $this->version;
        if (empty($objectName)) {
            $objectName = 'msBonus2';
        }
        $objectName = trim($objectName);

        if (empty($this->modx->loadedjscripts[$objectName]) && (!defined('MODX_API_MODE') || !MODX_API_MODE)) {
            $pls = $this->tools->makePlaceholders($this->config);
            if ($css = trim($this->modx->getOption('msb2_frontend_css'))) {
                $this->modx->regClientCSS(str_replace($pls['pl'], $pls['vl'], $css . '?v=' . $version));
            }
            if ($js = trim($this->modx->getOption('msb2_frontend_js'))) {
                $this->modx->regClientScript(str_replace($pls['pl'], $pls['vl'], $js . '?v=' . $version));
            }

            $params = $this->modx->toJSON(array_merge([
                // 'assetsUrl' => $this->config['assetsUrl'],
                'actionUrl' => $this->config['actionUrl'],
                'ctx' => $this->config['ctx'],
            ], $sp));

            $this->modx->regClientScript('<script>
                if (typeof(' . $objectName . 'Cls) === "undefined") {
                    var ' . $objectName . 'Cls = new ' . $objectName . '(' . $params . ');
                }
            </script>', true);

            $this->modx->loadedjscripts[$objectName] = true;
        }

        return !empty($this->modx->loadedjscripts[$objectName]);
    }

    /**
     * @param modManagerController $controller
     * @param null|array $services
     *
     * @return bool
     */
    public function loadManagerScripts(
        modManagerController $controller = null,
        $services = [
            'css/main',
            'js/vendor',
            'js/misc',
            'js/main',
            'js/minishop2',
        ]
    ) {
        $version = time(); // $this->version;

        //
        $controller = is_null($controller) ? $this->modx->controller : $controller;
        if (!(is_object($controller) && ($controller instanceof modManagerController))) {
            return false;
        }

        // Lexicon
        $controller->addLexiconTopic('msbonus2:default');

        // CSS
        if (in_array('css/main', $services)) {
            $controller->head['css'][] = $this->config['cssUrl'] . 'mgr/main.css?v=' . $version;
            $controller->head['css'][] = $this->config['cssUrl'] . 'mgr/bootstrap.buttons.css?v=' . $version;
        }

        // JS / Vendors
        if (in_array('js/vendor', $services)) {
            $controller->head['js'][] = $this->config['jsUrl'] . 'vendor/strftime.min.js';
        }

        // JS / Class
        $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/msbonus2.js?v=' . $version;

        // JS / Config
        $controller->addHtml('
            <script>
                msBonus2.config = ' . json_encode($this->config) . ';
                msBonus2.config[\'connector_url\'] = "' . $this->config['connectorUrl'] . '";
            </script>
        ');

        // JS / Other
        if (in_array('js/misc', $services)) {
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/ux.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/utils.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/renderer.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/combo.js?v=' . $version;

            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/default/grid.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/default/window.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/default/formpanel.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/misc/default/panel.js?v=' . $version;
        }

        // JS / Main
        if (in_array('js/main', $services)) {
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/widgets/levels/grid.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/widgets/levels/window.js?v=' . $version;

            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/widgets/logs/grid.js?v=' . $version;

            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/widgets/users/grid.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/widgets/users/window.js?v=' . $version;

            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/widgets/home.panel.js?v=' . $version;
            $controller->head['js'][] = $this->config['jsUrl'] . 'mgr/sections/home.js?v=' . $version;

            // Config
            $controller->addHtml('
                <script>
                    Ext.onReady(function() {
                        MODx.load({
                            xtype: "msbonus2-page-home",
                        });
                    });
                </script>
            ');
        }

        // JS / miniShop2
        if (in_array('js/minishop2', $services)) {
            $controller->head['lastjs'][] = $this->config['jsUrl'] . 'mgr/extends/ms2/orders.window.js?v=' . $version;
        }

        return true;
    }

    /**
     * @param array $config
     *
     * @return msb2Tools
     */
    public function getTools(array $config = [])
    {
        if (!is_object($this->tools)) {
            if ($class = $this->modx->loadClass('tools.msb2Tools', $this->config['handlersPath'], true, true)) {
                $this->tools = new $class($this, $config);
            }
        }

        return $this->tools;
    }

    /**
     * @return pdoTools
     */
    public function getPdoTools()
    {
        if (class_exists('pdoTools') && !is_object($this->pdoTools)) {
            $this->pdoTools = $this->modx->getService('pdoTools');
        }

        return $this->pdoTools;
    }

    /**
     * @return pdoFetch
     */
    public function getPdoFetch()
    {
        if (class_exists('pdoFetch') && !is_object($this->pdoFetch)) {
            $this->pdoFetch = $this->modx->getService('pdoFetch');
        }

        return $this->pdoFetch;
    }

    /**
     * @return miniShop2
     */
    public function getMiniShop2()
    {
        if (!is_object($this->miniShop2)) {
            $this->miniShop2 = $this->modx->getService('miniShop2');
        }

        return $this->miniShop2;
    }

    // /**
    //  * @return msOptionsPrice
    //  */
    // public function getMsOptionsPrice()
    // {
    //     $path = MODX_CORE_PATH . 'components/msoptionsprice/model/msoptionsprice/';
    //     if (file_exists($path) && !is_object($this->msOptionsPrice)) {
    //         $this->msOptionsPrice = $this->modx->getService('msoptionsprice', 'msOptionsPrice', $path);
    //     }
    //
    //     return $this->msOptionsPrice;
    // }

    /**
     * @param array $config
     *
     * @return msb2Manager
     */
    public function getManager(array $config = [])
    {
        if (!is_object($this->manager)) {
            if ($class = $this->modx->loadClass('manager.msb2Manager', $this->config['handlersPath'], true, true)) {
                $this->manager = new $class($this, $config);
            }
        }
        $this->manager->initialize($this->config['ctx'] ?: 'web');

        return $this->manager;
    }

    /**
     * @param array $data
     *
     * @return string|null
     */
    public function getActionType(array $data)
    {
        $output = null;

        // Custom actions
        $types = $this->modx->getOption('msb2_form_action_types');
        if ($this->tools->isJSON($types)) {
            $types = $this->modx->fromJSON($types);
        }
        if (is_array($types)) {
            if (isset($types[$data['type']]) && isset($types[$data['type']][$data['action']])) {
                $output = $types[$data['type']][$data['action']];
            }
        }

        // Load logs lexicon
        $cultureKey = $this->modx->getOption('cultureKey', null, 'ru');
        $this->modx->lexicon->load($cultureKey . ':msbonus2:logs');

        // Lexicon
        if (is_null($output)) {
            if (preg_match('/expired$/', $data['type'])) {
                if ($data['type'] !== 'expired' && $this->modx->lexicon->exists('msb2_logs_-' . $data['type'])) {
                    $output = $this->modx->lexicon(('msb2_logs_-' . $data['type']), $data) ?: null;
                }
                if (is_null($output)) {
                    $output = $this->modx->lexicon('msb2_logs_-expired', $data);
                }
            }
            if (is_null($output)) {
                $output = $this->modx->lexicon(('msb2_logs_' . $data['action'] . $data['type']), $data);
            }
        }

        return $output;
    }
}