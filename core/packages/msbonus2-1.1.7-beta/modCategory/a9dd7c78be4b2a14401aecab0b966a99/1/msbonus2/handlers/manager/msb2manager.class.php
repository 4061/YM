<?php

class msb2Manager
{
    /**
     * @var array $config
     */
    public $config = [];
    /**
     * @var modX $modx
     */
    protected $modx;
    /**
     * @var msBonus2 $msb2
     */
    protected $msb2;
    /**
     * @var miniShop2 $ms2
     */
    protected $ms2;
    /**
     * @var string $ctx
     */
    protected $ctx = 'web';

    /**
     * @param msBonus2  $msb2
     * @param array     $config
     */
    function __construct(msBonus2 &$msb2, $config = [])
    {
        $this->msb2 = &$msb2;
        $this->modx = &$this->msb2->modx;
        $this->config = $config;
    }

    /**
     * @param string $ctx
     *
     * @return bool
     */
    public function initialize($ctx = 'web')
    {
        $this->ctx = $ctx;

        //
        $this->ms2 = $this->msb2->getMiniShop2();
        if (empty($this->ms2->cart)) {
            $this->ms2->loadServices($this->ctx);
        }
        $this->ms2->cart->initialize($this->ctx);

        //
        $msb2User = $this->getJoinedUser($this->modx->user->get('id'));

        return true;
    }

    /**
     * Get current points amount from session
     *
     * @return null|int
     */
    public function getCartWriteoff()
    {
        $amount = null;
        if (!empty($_SESSION['msBonus2']['writeoff'][$this->ctx])) {
            $amount = $_SESSION['msBonus2']['writeoff'][$this->ctx];
        }

        return $amount;
    }

    /**
     * Set points to cart and save points amount to session
     *
     * @param int|float $amount
     *
     * @return bool|int|float
     */
    public function setCartWriteoff($amount)
    {
        $this->unsetCartWriteoff();
        $points = $this->getUserPoints();
        if (empty($amount)) {
            return $this->modx->lexicon('msb2_front_err_required_amount');
        } elseif ($amount < 0) {
            return $this->modx->lexicon('msb2_front_err_negative_amount');
        } elseif ($amount > $points) {
            return $this->modx->lexicon('msb2_front_err_less_points');
        }

        // Get cart products
        $products = $this->ms2->cart->get();
        if (empty($products)) {
            return $this->modx->lexicon('msb2_front_err_cart_empty');
        }

        //
        $writeoff = $this->getOrderWriteoff($products, $amount);
        if (empty($writeoff)) {
            return $this->modx->lexicon('msb2_front_err_cart_empty');
        }

        // Save points amount to session
        $_SESSION['msBonus2']['writeoff'][$this->ctx] = $writeoff;

        return $writeoff;
    }

    /**
     * Unset points and remove from session
     *
     * @return bool
     */
    public function unsetCartWriteoff()
    {
        if (!empty($_SESSION['msBonus2']['writeoff'][$this->ctx])) {
            unset($_SESSION['msBonus2']['writeoff'][$this->ctx]);
        }

        return true;
    }

    /**
     * @param array     $products
     * @param int|float $amount
     *
     * @return int|float
     */
    public function getOrderWriteoff(array $products, $amount)
    {
        if (empty($amount)) {
            return 0;
        }

        // Get options
        $category_ids = array_diff(array_map('trim', explode(',',
            $this->modx->getOption('msb2_categories_for_writing_off_points'))), ['']);

        //
        $with_old_price = $this->modx->getOption('msb2_writing_off_with_old_price');
        if (empty($with_old_price)) {
            // Get products ids
            $product_ids = [];
            foreach ($products as $v) {
                if (isset($v['old_price'])) {
                    continue;
                }
                $product_ids[] = $v['id'];
            }

            // Get products old prices
            if (!empty($product_ids)) {
                $q = $this->modx->newQuery('msProductData')
                    ->select('id, old_price')
                    ->where([
                        'id:IN' => $product_ids,
                    ])
                ;
                if ($q->prepare()->execute()) {
                    if ($rows = $q->stmt->fetchAll(PDO::FETCH_ASSOC)) {
                        $products = array_map(function ($v) use ($rows) {
                            foreach ($rows as $row) {
                                if ((int)$row['id'] === (int)$v['id']) {
                                    $v['old_price'] = (float)$row['old_price'];
                                    break;
                                }
                            }
                            return $v;
                        }, $products);
                    }
                }
            }
            unset($q, $rows, $product_ids);
        }

        // Get cost
        $cost = 0;
        foreach ($products as $product) {
            // Check writing off points from products with not empty old price
            if (empty($with_old_price)) {
                if (!empty($product['old_price'])) {
                    continue;
                }
            }

            // Check product parents is categories for writing off points
            if (!empty($category_ids)) {
                $parent_ids = $this->modx->getParentIds($product['id'], 10, ['context' => $product['ctx']]) ?: [];
                if (empty(array_intersect($category_ids, $parent_ids))) {
                    continue;
                }
            }

            //
            $cost += empty($product['cost'])
                ? ($product['price'] * $product['count'])
                : $product['cost'];
        }

        //
        $writeoff = $this->getOrderWriteoffAmount($cost, $amount);

        return $writeoff;
    }

    /**
     * @param int|float $cost
     * @param int|float $amount
     *
     * @return int|float
     */
    public function getOrderWriteoffAmount($cost, $amount)
    {
        $writeoff = 0;
        $cart_maximum_percent = floatval($this->modx->getOption('msb2_cart_maximum_percent') ?: 0);
        if (!empty($cost) && !empty($amount) && !empty($cart_maximum_percent)) {
            $writeoff = $amount;
            $cart_maximum_points = ceil(@($cost / 100 * $cart_maximum_percent) ?: 0);
            if ($writeoff > $cart_maximum_points) {
                $writeoff = $cart_maximum_points;
            }
        }

        return $writeoff;
    }

    /**
     * Set write off amount to order
     * Returns write off amount
     *
     * @param msOrder|int $order
     * @param int|float   $amount
     * @param bool        $log
     *
     * @return int|float
     */
    public function setOrderWriteoff($order, $amount, $log = true)
    {
        $order = $this->getOrder($order);
        $msb2Order = $this->getJoinedOrder($order);
        $msb2User = $this->getJoinedUser($order);
        if (empty($order) || empty($msb2Order)) {
            return 0;
        }

        //
        $points = $this->getUserPoints($order->get('user_id'));
        if (empty($amount)) {
            $amount = 0;
        } elseif ($amount > $points) {
            $amount = $points;
        }

        // Get order products
        $products = [];
        if ($orderProducts = $order->getMany('Products')) {
            /** @var msOrderProduct $orderProduct */
            foreach ($orderProducts as $orderProduct) {
                $products[] = array_merge($orderProduct->toArray(), [
                    'id' => $orderProduct->get('product_id'),
                    'ctx' => $order->get('context'),
                ]);
            }
        }
        if (empty($products)) {
            $amount = 0;
        }
        unset($orderProducts, $orderProduct);

        //
        $writeoff = $this->getOrderWriteoff($products, $amount);
        if (empty($writeoff)) {
            $writeoff = 0;
        }

        //
        $cart_cost = $order->get('cart_cost') - $writeoff;
        $cost = $order->get('cost') - $writeoff;
        $order->set('cart_cost', $cart_cost);
        $order->set('cost', $cost);

        //
        $tmp = $order->get('update_products');
        $order->set('update_products', false);
        if ($order->save()) {
            $order->set('update_products', $tmp);

            //
            $msb2Order->set('writeoff', $writeoff);
            $msb2Order->set('writeoff_tmp', 0);

            //
            if ($this->isOrderStatus($order, 'paid')) {
                $msb2Order->set('cost', $cart_cost);
            }
            $order_cost = $msb2Order->get('cost');

            //
            $paid_points = $msb2User->get('paid_points');
            $msb2User->set('paid_points', ($paid_points + $writeoff));
            if (!empty($order_cost)) {
                $paid_money = $msb2User->get('paid_money');
                $msb2User->set('paid_money', ($paid_money + $order_cost));
            }

            //
            $msb2Order->save();
            $msb2User->save();

            //
            if (!empty($writeoff)) {
                $this->setMinus(
                    'order_writeoff',
                    $writeoff,
                    $order->get('user_id'),
                    $order->get('id'),
                    0,
                    $log
                );
            }
        }

        return $writeoff;
    }

    /**
     * Unset write off from order
     *
     * @param msOrder|int $order
     * @param bool        $log
     *
     * @return int|float
     */
    public function unsetOrderWriteoff($order, $log = true)
    {
        $writeoff = 0;
        $order = $this->getOrder($order);
        $msb2Order = $this->getJoinedOrder($order);
        $msb2User = $this->getJoinedUser($order);
        if (!empty($order) && !empty($msb2Order)) {
            $writeoff = $msb2Order->get('writeoff');
            $cart_cost = $order->get('cart_cost') + $writeoff;
            $cost = $order->get('cost') + $writeoff;
            $order->set('cart_cost', $cart_cost);
            $order->set('cost', $cost);

            //
            $tmp = $order->get('update_products');
            $order->set('update_products', false);
            if ($order->save()) {
                $order->set('update_products', $tmp);

                //
                $msb2Order->set('writeoff', 0);
                $msb2Order->set('writeoff_tmp', $writeoff);

                //
                $cart_cost = $order->get('cart_cost_old') ?: ($cart_cost - $writeoff);
                $cost = $order->get('cost_old') ?: ($cost - $writeoff);

                // //
                // if ($this->isOrderStatus($order, 'paid')) {
                //     $msb2Order->set('cost', $cart_cost);
                // }
                $order_cost = $msb2Order->get('cost');

                //
                $paid_points = $msb2User->get('paid_points');
                $msb2User->set('paid_points', ($paid_points - $writeoff));
                if (!empty($order_cost)) {
                    $paid_money = $msb2User->get('paid_money');
                    $msb2User->set('paid_money', ($paid_money - $order_cost));
                }

                //
                if ($this->isOrderStatus($order, 'cancel')) {
                    $msb2Order->set('cost', 0);
                }

                //
                $msb2Order->save();
                $msb2User->save();

                //
                if (!empty($writeoff)) {
                    $this->setPlus(
                        'order_writeoff',
                        $writeoff,
                        $order->get('user_id'),
                        $order->get('id'),
                        0,
                        $log
                    );
                }
            }
        }

        return $writeoff;
    }

    /**
     * Reset write off amount to order
     *
     * @param msOrder|int $order
     *
     * @return bool
     */
    public function refreshOrderWriteoff($order)
    {
        $order = $this->getOrder($order);
        if (empty($order)) {
            return false;
        }

        // Unset write off points
        $writeoff_old = (float)$this->unsetOrderWriteoff($order);

        // For refresh writeoff_tmp
        $msb2Order = $this->getJoinedOrder($order);

        // Set write off points
        $writeoff_new = (float)$this->setOrderWriteoff($order, $msb2Order->get('writeoff_tmp'));

        // If write off points equal
        if ($writeoff_old === $writeoff_new) {
            // Remove two logs records
            $q = $this->modx->newQuery('msb2Log')
                // ->select(['id'])
                ->where([
                    'type' => 'order_writeoff',
                    'user' => $order->get('user_id'),
                    'order' => $order->get('id'),
                    'amount' => $writeoff_new,
                ])
                ->sortby('id', 'DESC')
                ->limit(2)
                ;
            if ($logs = $this->modx->getCollection('msb2Log', $q)) {
                /** @var msb2Log $log */
                foreach ($logs as $log) {
                    $log->remove();
                }
            }
        }

        return true;
    }

    /**
     * @param msOrder|int $order
     *
     * @return int|float
     */
    public function getOrderAccrual($order)
    {
        $order = $this->getOrder($order);
        $msb2Order = $this->getJoinedOrder($order);
        if (empty($order) || empty($msb2Order)) {
            return 0;
        }
        $writeoff = $msb2Order->get('writeoff');

        // Get order products
        $products = [];
        if ($orderProducts = $order->getMany('Products')) {
            /** @var msOrderProduct $orderProduct */
            foreach ($orderProducts as $orderProduct) {
                $products[] = array_merge($orderProduct->toArray(), [
                    'id' => $orderProduct->get('product_id'),
                    'ctx' => $order->get('context'),
                ]);
            }
        }
        if (empty($products)) {
            return 0;
        }
        unset($orderProducts, $orderProduct);

        // Get options
        $category_ids = array_diff(array_map('trim', explode(',',
            $this->modx->getOption('msb2_categories_for_accrual_of_points'))), ['']);

        // Get cost
        $cost = 0;
        foreach ($products as $product) {
            // Check product parents is categories for accrual of points
            if (!empty($category_ids)) {
                $parent_ids = $this->modx->getParentIds($product['id'], 10, ['context' => $product['ctx']]) ?: [];
                if (empty(array_intersect($category_ids, $parent_ids))) {
                    continue;
                }
            }

            //
            $cost += empty($product['cost'])
                ? ($product['price'] * $product['count'])
                : $product['cost'];
        }
        $cost = $cost - $writeoff;

        //
        $accrual = $this->getOrderAccrualAmount($order->get('user_id'), $cost);

        return $accrual;
    }

    /**
     * @param int       $user
     * @param int|float $cost
     *
     * @return int|float
     */
    public function getOrderAccrualAmount($user, $cost)
    {
        $accrual = 0;
        $user_bonus_percent = floatval($this->getUserPercents($user) ?: 0);
        if (!empty($cost) && !empty($user_bonus_percent)) {
            $accrual = ceil(@($cost / 100 * $user_bonus_percent) ?: 0);
        }

        return $accrual;
    }

    /**
     * Set accrual amount to order
     * Returns accrual amount
     *
     * @param msOrder|int $order
     * @param bool        $log
     *
     * @return int|float
     */
    public function setOrderAccrual($order, $log = true)
    {
        $order = $this->getOrder($order);
        $msb2Order = $this->getJoinedOrder($order);
        if (empty($order) || empty($msb2Order)) {
            return 0;
        }

        //
        $accrual = $this->getOrderAccrual($order);
        if (empty($accrual)) {
            return 0;
        }

        //
        $msb2Order->set('accrual', $accrual);
        $msb2Order->save();

        //
        $this->setPlus(
            'order_accrual',
            $accrual,
            $order->get('user_id'),
            $order->get('id'),
            0,
            $log
        );

        return $accrual;
    }

    /**
     * Unset accrual from order
     *
     * @param msOrder|int $order
     * @param bool        $log
     *
     * @return bool
     */
    public function unsetOrderAccrual($order, $log = true)
    {
        $order = $this->getOrder($order);
        $msb2Order = $this->getJoinedOrder($order);
        if (!empty($order) && !empty($msb2Order)) {
            if ($accrual = $msb2Order->get('accrual')) {
                $msb2Order->set('accrual', 0);
                $msb2Order->save();

                //
                $this->setMinus(
                    'order_accrual',
                    $accrual,
                    $order->get('user_id'),
                    $order->get('id'),
                    0,
                    $log
                );
            }
        }

        return true;
    }

    /**
     * Get all points of user
     *
     * @param int $user
     *
     * @return int|float
     */
    public function getUserPoints($user = 0)
    {
        $points = 0;
        $user = empty($user) ? $this->modx->user->get('id') : $user;
        if (empty($user)) {
            return $points;
        }

        /** @var msb2User $msb2User */
        if (!$msb2User = $this->modx->getObject('msb2User', ['user' => $user])) {
            $msb2User = $this->modx->newObject('msb2User', [
                'user' => $user,
                'points' => 0,
            ]);
            $msb2User->save();
        }

        return (float)$msb2User->get('points');
    }

    /**
     * Get level of user
     *
     * @param int $user
     *
     * @return array|null
     */
    public function getUserLevel($user = 0)
    {
        $level = null;
        $user = empty($user) ? $this->modx->user->get('id') : $user;
        if (empty($user)) {
            return $level;
        }

        /** @var msb2User $msb2User */
        if (!$msb2User = $this->modx->getObject('msb2User', ['user' => $user])) {
            $msb2User = $this->modx->newObject('msb2User', [
                'user' => $user,
                'points' => 0,
            ]);
            $msb2User->save();
        }

        /** @var msb2Level $msb2Level */
        if ($msb2Level = $msb2User->getOne('Level')) {
            $level = $msb2Level->toArray();
        }

        return $level;
    }

    /**
     * Get bonus percent of user
     *
     * @param int $user
     *
     * @return int|float
     */
    public function getUserPercents($user = 0)
    {
        $bonus = 0;
        if ($level = $this->getUserLevel($user)) {
            $bonus = (float)$level['bonus'];
        }

        return $bonus;
    }

    /**
     * @param msOrder|int $order
     *
     * @return msOrder
     */
    public function getOrder($order)
    {
        if (is_numeric($order)) {
            $order = $this->modx->getObject('msOrder', ['id' => $order]);
        }
        if (!is_object($order) || !$order instanceof msOrder) {
            $order = null;
        }

        return $order;
    }

    /**
     * @param msOrder|int $order
     *
     * @return msb2Order
     */
    public function getJoinedOrder($order)
    {
        $msb2Order = null;
        if ($order = $this->getOrder($order)) {
            /** @var msb2Order $msb2Order */
            if (!$msb2Order = $this->modx->getObject('msb2Order', [
                'order' => $order->get('id'),
            ])) {
                $msb2Order = $this->modx->newObject('msb2Order');
                $msb2Order->fromArray([
                    'order' => $order->get('id'),
                ]);
                $msb2Order->save();
            }
        }

        return $msb2Order;
    }

    /**
     * @param msOrder|modUser|msb2User|int $id
     *
     * @return msb2User
     */
    public function getJoinedUser($id)
    {
        /** @var msb2User $msb2User */
        $msb2User = null;
        if (!isset($this->modx->map['msb2User'])) {
            return $msb2User;
        }
        if (is_object($id)) {
            if ($id instanceof msOrder) {
                $id = $id->get('user_id');
            } elseif ($id instanceof modUser) {
                $id = $id->get('id');
            } elseif ($id instanceof msb2User) {
                $msb2User = $id;
                $id = null;
            }
        }
        if (is_numeric($id) && !empty($id)) {
            /** @var msb2User $msb2User */
            if (!$msb2User = $this->modx->getObject('msb2User', ['user' => $id])) {
                $msb2User = $this->modx->newObject('msb2User', [
                    'user' => $id,
                    'points' => 0,
                ]);
                $msb2User->save();
            }
        }

        return $msb2User;
    }

    /**
     * Совершает действие (прибавление или списание) с бонусами и записывает это в лог
     *
     * @param string       $action
     * @param string       $type
     * @param float        $amount
     * @param msb2User|int $user
     * @param int          $order
     * @param int          $createdby
     * @param bool         $log
     *
     * @return bool
     */
    public function setAction($action, $type, $amount, $user, $order = 0, $createdby = 0, $log = true)
    {
        if (empty($user) || empty($action) || empty($type) || empty($amount)) {
            return false;
        }

        $order = (int)($order ?: 0);
        if (in_array($type, ['order_writeoff', 'order_accrual']) && empty($order)) {
            return false;
        }

        // Get msb2User object
        if (is_object($user) && $user instanceof msb2User) {
            $msb2User = &$user;
        } else {
            $msb2User = $this->getJoinedUser($user);
        }

        // Get activation time
        $activation_time = 0;
        if (!in_array($type, ['order_writeoff'])) {
            $activation_time = $this->modx->getOption('msb2_activation_time_for_bonus');
            if ($this->msb2->tools->isJSON($activation_time)) {
                $activation_time = $this->modx->fromJSON($activation_time);
                $activation_time = isset($activation_time[$type]) ? $activation_time[$type] : 0;
            }
            $activation_time = is_numeric($activation_time) ? $activation_time : 0;
        }

        // Get activated on timestamp
        $activatedon = 0;
        if ($action === '+' && !empty($activation_time)) {
            $activatedon = time() + $activation_time;
        }

        // Get balance key for current action type
        $balance_key = 'points';
        if (!empty($activation_time)) {
            switch ($action) {
                case '+':
                    $balance_key = 'reserve';
                    break;
                case '-':
                    if (in_array($type, ['order_accrual'])) {
                        if ($orderAccrualLog = $this->modx->getObject('msb2Log', [
                            'user' => $msb2User->get('user'),
                            'order' => $order,
                            'action' => '+',
                            'type' => $type,
                            'amount' => $amount,
                            'activatedon:!=' => 0,
                        ])) {
                            $balance_key = 'reserve';
                            $orderAccrualLog->set('activatedon', 0);
                            $orderAccrualLog->save();
                        }
                        unset($orderAccrualLog);
                    }
                    break;
            }
        }

        //
        $balance_current = $msb2User->get($balance_key);
        $balance_new = $balance_current;
        switch ($action) {
            case '+':
                $balance_new = (float)($balance_current + $amount);
                break;
            case '-':
                $balance_new = (float)($balance_current - $amount);
                break;
        }
        $msb2User->set($balance_key, $balance_new);
        $msb2User->save();

        //
        if ($log) {
            $this->modx->newObject('msb2Log', [
                'user' => $msb2User->get('user'),
                'order' => $order,
                'action' => $action,
                'type' => $type,
                'amount' => $amount,
                'createdon' => time(),
                'createdby' => ($createdby ?: $this->modx->user->get('id')) ?: $msb2User->get('user'),
                'activatedon' => $activatedon,
            ])->save();
        }

        return true;
    }

    /**
     * @param string       $type
     * @param float        $amount
     * @param msb2User|int $user
     * @param int          $order
     * @param int          $createdby
     * @param bool         $log
     *
     * @return bool
     */
    public function setPlus($type, $amount, $user, $order = 0, $createdby = 0, $log = true)
    {
        return $this->setAction('+', $type, $amount, $user, $order, $createdby, $log);
    }

    /**
     * @param string       $type
     * @param float        $amount
     * @param msb2User|int $user
     * @param int          $order
     * @param int          $createdby
     * @param bool         $log
     *
     * @return bool
     */
    public function setMinus($type, $amount, $user, $order = 0, $createdby = 0, $log = true)
    {
        return $this->setAction('-', $type, $amount, $user, $order, $createdby, $log);
    }

    /**
     * Проверяет, есть ли такая транзакция в логах
     *
     * @param string       $action
     * @param string       $type
     * @param msb2User|int $user
     * @param int          $order
     * @param array        $condition
     *
     * @return bool
     */
    public function isExists($action, $type, $user, $order = 0, array $condition = [])
    {
        if (empty($user) || empty($action) || empty($type)) {
            return false;
        }
        $order = (int)($order ?: 0);
        $user_id = is_object($user) ? $user->get('user') : $user;

        return (bool)$this->modx->getCount('msb2Log', array_merge([
            'user' => $user_id,
            'order' => $order,
            'action' => $action,
            'type' => $type,
        ], $condition));
    }

    /**
     * Checking order status, variable $status_type is equal order status
     *
     * @param msOrder|int $order
     * @param string      $status_type
     *
     * @return bool
     */
    public function isOrderStatus($order, $status_type)
    {
        $flag = false;
        if ($order = $this->getOrder($order)) {
            if ($status = $this->modx->getOption('msb2_order_status_' . $status_type)) {
                $flag = (int)$order->get('status') === (int)$status;
            }
        }

        return $flag;
    }
}