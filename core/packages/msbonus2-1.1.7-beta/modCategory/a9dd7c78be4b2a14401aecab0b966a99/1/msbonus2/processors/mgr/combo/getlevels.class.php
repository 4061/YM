<?php

class msb2ComboLevelGetListProcessor extends modProcessor
{
    /** @var msBonus2 $msb2 */
    protected $msb2;

    /**
     * @return bool
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        return parent::initialize();
    }

    /**
     * @return string
     */
    public function process()
    {
        $output = [];
        $filter = $this->getProperty('filter', false);
        if ($filter) {
            $output[] = [
                'display' => '(Все)',
                'value' => '',
            ];
        }

        //
        $q = $this->modx->newQuery('msb2Level')
            ->select(['id', 'bonus', 'cost', 'name'])
            ->sortby('cost', 'ASC')
            ;
        if ($q->prepare() && $q->stmt->execute()) {
            if ($rows = $q->stmt->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($rows as $row) {
                    $output[] = [
                        'display' => $row['name'],
                        'value' => $row['id'],
                    ];
                }
            }
        }

        return $this->outputArray($output);
    }

    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return array('msbonus2:default');
    }
}

return 'msb2ComboLevelGetListProcessor';