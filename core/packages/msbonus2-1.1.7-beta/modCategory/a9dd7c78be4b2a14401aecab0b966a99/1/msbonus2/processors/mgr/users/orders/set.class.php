<?php

require_once MODX_CORE_PATH . 'model/modx/modprocessor.class.php';
if (file_exists(MODX_BASE_PATH . 'msBonus2/core/components/msbonus2/processors/mgr/users/orders/doit.class.php')) {
    require_once MODX_BASE_PATH . 'msBonus2/core/components/msbonus2/processors/mgr/users/orders/doit.class.php';
} else {
    require_once MODX_CORE_PATH . 'components/msbonus2/processors/mgr/users/orders/doit.class.php';
}

return 'msb2UserOrderDoItProcessor';