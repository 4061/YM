<?php

class msb2LevelGetProcessor extends modObjectGetProcessor
{
    public $objectType = 'msb2Level';
    public $classKey = 'msb2Level';
    public $languageTopics = array('msbonus2:default');
    public $permission = 'view';

    /**
     * @return mixed
     */
    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

        return parent::process();
    }
}

return 'msb2LevelGetProcessor';