<?php

class msb2UserGetProcessor extends modObjectGetProcessor
{
    public $objectType = 'msb2User';
    public $classKey = 'msb2User';
    public $languageTopics = array('msbonus2:default');
    public $permission = 'view';
    /** @var msBonus2 $msb2 */
    protected $msb2;

    /**
     * @return bool
     */
    public function initialize()
    {
        $this->msb2 = $this->modx->getService('msbonus2', 'msBonus2',
            $this->modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/');
        $this->msb2->initialize($this->modx->context->key);

        return parent::initialize();
    }

    /**
     * @return mixed
     */
    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

        return parent::process();
    }

    /**
     * Return the response
     *
     * @return array
     */
    public function cleanup()
    {
        $data = $this->object->toArray();

        //
        if ($user = $this->object->getOne('User')) {
            foreach (
                [
                    'username',
                    'createdon',
                    'active',
                ] as $key
            ) {
                $data[$key] = $user->get($key);
            }
        }

        //
        if ($profile = $user->getOne('Profile')) {
            foreach (
                [
                    'fullname',
                    'email',
                    'mobilephone',
                    'dob',
                    'gender',
                    'blocked',
                ] as $key
            ) {
                $data[$key] = $profile->get($key);
            }
        }
        $data['dob'] = $data['dob'] ?: null;

        //
        if ($level = $this->object->getOne('Level')) {
            foreach (
                [
                    'bonus',
                    'cost',
                    'name',
                ] as $key
            ) {
                $data['level_' . $key] = $level->get($key);
            }
        }

        //
        $data['points_formatted'] = number_format($data['points'], 0, '.', ' ');
        $data['reserve_formatted'] = number_format($data['reserve'], 0, '.', ' ');
        $data['paid_money_formatted'] = number_format($data['paid_money'], 0, '.', ' ');
        $data['paid_points_formatted'] = number_format($data['paid_points'], 0, '.', ' ');

        //
        $data['form_type'] = '';
        $types = $this->modx->getOption('msb2_form_action_types');
        if ($this->msb2->tools->isJSON($types)) {
            $types = $this->modx->fromJSON($types);
        }
        if (is_array($types)) {
            foreach ($types as $k => $v) {
                $data['form_type'] = $k;
                break;
            }
        }

        //
        return $this->success('', $data);
    }
}

return 'msb2UserGetProcessor';