<?php
if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php')) {
    /** @noinspection PhpIncludeInspection */
    require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
} else {
    require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.core.php';
}
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var msBonus2 $msBonus2 */
$msBonus2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, $modx->getOption('core_path') . 'components/msbonus2/') . 'model/msbonus2/');
$modx->lexicon->load('msbonus2:default');

// handle request
$corePath = $modx->getOption('msb2_core_path', null, $modx->getOption('core_path') . 'components/msbonus2/');
$path = $modx->getOption('processorsPath', $msBonus2->config, $corePath . 'processors/');
$modx->getRequest();

/** @var modConnectorRequest $request */
$request = $modx->request;
$request->handleRequest(array(
    'processors_path' => $path,
    'location' => '',
));