msBonus2.panel.Home = function (config) {
    config = config || {};
    Ext.apply(config, {
        bodyCssClass: 'msb2-w',
        baseCls: 'modx-formpanel',
        layout: 'anchor',
        hideMode: 'offsets',
        items: [{
            html: '<h2>' + _('msbonus2') + '</h2>',
            cls: '',
            style: {margin: '15px 0'}
        }, {
            xtype: 'modx-tabs',
            defaults: {border: false, autoHeight: true},
            border: true,
            hideMode: 'offsets',
            enableTabScroll: false,
            /*stateful: true,
            stateId: 'modextra-panel-home',
            stateEvents: ['tabchange'],
            getState: function () {
                return {activeTab: this.items.indexOf(this.getActiveTab())};
            },*/
            items: [{
                title: _('msb2_tab_users'),
                layout: 'anchor',
                items: [{
                    xtype: 'msb2-grid-users',
                    cls: 'main-wrapper',
                }]
            }, {
                title: _('msb2_tab_levels'),
                layout: 'anchor',
                items: [{
                    xtype: 'msb2-grid-levels',
                    cls: 'main-wrapper',
                }]
            }]
        }]
    });
    msBonus2.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.panel.Home, MODx.Panel);
Ext.reg('msbonus2-panel-home', msBonus2.panel.Home);
