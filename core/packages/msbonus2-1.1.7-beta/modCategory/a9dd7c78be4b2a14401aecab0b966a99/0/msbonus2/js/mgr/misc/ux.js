/**
 * @param config
 * @constructor
 */
msBonus2.ux.FieldsetOrder = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        id: config['id'] || 'msb2-fieldset-order',
        url: msBonus2.config['connector_url'],
        items: this.getItems(config),
        listeners: this.getListeners(config),
        layout: config['layout'] || 'column',
        cls: (config['cls'] || '') + ' msb2-fieldset',
        style: config['style'] || {padding: '0px', marginTop: '15px', marginBottom: '0px'},
        defaults: config['defaults'] || {msgTarget: 'under', border: false},
        onRender: function () {
            var fs = this;
            msBonus2.ux.FieldsetOrder.superclass.onRender.apply(fs, arguments);
            // console.log('msBonus2.ux.FieldsetOrder onRender fs', fs);

            // Get order window
            fs['window'] = fs.findParentBy(function(a) {
                return a['xtype'] === 'minishop2-window-order-update';
            }) || undefined;
        },
        isRequest: false,
    });
    msBonus2.ux.FieldsetOrder.superclass.constructor.call(this, config);
    this['config'] = config;

    //
    this.on('afterrender', function (fs) {
        // console.log('msBonus2.ux.FieldsetOrder afterrender fs', fs);

        //
        fs['disabledMask'] = new Ext.LoadMask(fs['bwrap'], {
            msg: _('msb2_ms2_message_disabled'),
            msgCls: 'x-mask-loading msb2-mask-loading msb2-mask-loading_without-animate',
        });
        fs['loadMask'] = new Ext.LoadMask(fs['bwrap'], {
            msg: _('msb2_ms2_message_loading'),
        });
        if (fs.isRequest) {
            fs.loadMask.show();
        }
    });

    this.initialize(config);
};
Ext.extend(msBonus2.ux.FieldsetOrder, Ext.form.FieldSet, {
    /**
     * @param config
     * @constructor
     */
    initialize: function (config) {
        var fs = this;
        fs.addEvents({
            beforeInit: true,
            afterInit: true,
        });

        //
        if (fs.fireEvent('beforeInit')) {
            fs.refresh();

            fs.fireEvent('afterInit');
        }
    },

    /**
     * @constructor
     */
    refresh: function () {
        var fs = this;
        fs.request(
            'get',
            {},
            function (response) {
                // console.log('initialize callback response', response);
            }
        );
    },

    /**
     * @param button
     * @param e
     */
    submit: function (button, e) {
        var fs = this;

        // console.log('msBonus2.ux.FieldsetOrder submit fs', fs);
        // console.log('msBonus2.ux.FieldsetOrder submit button', button);

        this.request(
            (fs.status ? 'unset' : 'set'),
            {},
            function (response) {
                // console.log('submit callback response', response);
            }
        );
    },

    /**
     */
    request: function (action, data, callback) {
        var fs = this;
        var field = fs.findBy(function (a) {return a['id'] === fs['id'] + '-writeoff'});
        field = field['length'] ? field[0] : undefined;
        var button = fs.findBy(function (a) {return a['id'] === fs['id'] + '-submit'});
        button = button['length'] ? button[0] : undefined;
        if (!field || !button) {
            return;
        }

        data = typeof(data) === 'object' ? data : {};
        data['order'] = fs['order'];
        data['writeoff'] = field.getValue();

        // console.log('msBonus2.ux.FieldsetOrder request fs', fs);
        // console.log('msBonus2.ux.FieldsetOrder request action', action);
        // console.log('msBonus2.ux.FieldsetOrder request data', data);

        fs.isRequest = true;
        fs.loadMask && fs.loadMask.show();

        MODx.Ajax.request({
            url: fs.config['url'],
            params: Ext.apply({
                action: 'mgr/users/orders/' + action,
            }, data),
            listeners: {
                success: {
                    fn: function (response) {
                        // console.log('request success response', response);

                        fs.isRequest = false;
                        fs.loadMask && fs.loadMask.hide();

                        if (response['success'] && response['object']) {
                            // Disable fieldset
                            if (response.object['disabled']) {
                                fs.disabledMask.show();
                            } else {
                                fs.disabledMask.hide();
                            }

                            // Set status
                            fs.status = response.object['status'] || false;

                            // Set field value and disabled
                            field.setValue(response.object['writeoff']);
                            field.setDisabled(fs.status); // field.setReadOnly(fs.status);

                            // Set button text
                            var buttonLexicon = fs.status
                                ? 'msb2_ms2_window_unset' : 'msb2_ms2_window_set';
                            button.setText(_(buttonLexicon));

                            // Set user points
                            ['points'].forEach(function (k) {
                                var fieldCmp = fs.findBy(function (a) {return a.name === 'msb2_' + k});
                                if (fieldCmp['length'] && k in response['object']) {
                                    fieldCmp = fieldCmp[0];
                                    fieldCmp.setValue(response.object[k] || 0);
                                }
                            });

                            // Set order amounts
                            if (response.object['order']) {
                                ['cost', 'cart_cost', 'delivery_cost'].forEach(function (k) {
                                    var fieldCmp = fs.window.findBy(function (a) {return a.name === k});
                                    if (fieldCmp['length'] && k in response.object['order']) {
                                        fieldCmp = fieldCmp[0];
                                        fieldCmp.setValue(response.object.order[k]);
                                    }
                                });
                            }
                        }

                        callback && callback(response);
                    },
                    scope: fs,
                },
                failure: {
                    fn: function (response) {
                        console.log('request failure response', response);

                        fs.isRequest = false;
                        fs.loadMask && fs.loadMask.hide();

                        callback && callback(response);
                    },
                    scope: fs,
                },
            }
        });
    },

    /**
     * @param config
     * @returns {*[]}
     */
    getItems: function (config) {
        return [{
            columnWidth: .5,
            layout: 'form',
            style: {marginTop: '-5px'},
            items: [{
                layout: 'column',
                border: false,
                style: {marginTop: '0px'},
                anchor: '100%',
                items: [{
                    columnWidth: .7,
                    layout: 'form',
                    style: {marginRight: '5px'},
                    items: [{
                        xtype: 'numberfield',
                        id: config['id'] + '-writeoff',
                        name: 'msb2_writeoff',
                        fieldLabel: _('msb2_ms2_field_writeoff'),
                        anchor: '100%',
                        allowDecimals: false,
                        allowNegative: false,
                        listeners: {
                            specialkey: {
                                fn: this.onSpecialKeyDown,
                                scope: this,
                            },
                        },
                    }],
                }, {
                    columnWidth: .3,
                    layout: 'form',
                    style: {marginTop: '37px', marginLeft: '5px'},
                    items: [{
                        xtype: 'button',
                        id: config['id'] + '-submit',
                        text: _('msb2_ms2_window_set'),
                        cls: 'msb2-button primary-button',
                        handler: this.submit,
                        scope: this,
                    }],
                }],
            }]
        }, {
            columnWidth: .5,
            layout: 'form',
            style: {marginTop: '0px', textAlign: 'center'},
            items: [{
                xtype: 'displayfield',
                id: config['id'] + '-points',
                name: 'msb2_points',
                fieldLabel: _('msb2_ms2_field_points'),
                anchor: '100%',
            }/*, {
                layout: 'column',
                border: false,
                style: {marginTop: '0px'},
                anchor: '100%',
                items: [{
                    columnWidth: .5,
                    layout: 'form',
                    style: {marginRight: '5px'},
                    items: [{
                        xtype: 'displayfield',
                        id: config['id'] + '-discount',
                        name: 'msb2_discount',
                        fieldLabel: _('msb2_ms2_field_discount'),
                        anchor: '100%',
                    }],
                }, {
                    columnWidth: .5,
                    layout: 'form',
                    style: {marginLeft: '5px'},
                    items: [{
                        xtype: 'displayfield',
                        id: config['id'] + '-discount-amount',
                        name: 'msb2_discount_amount',
                        fieldLabel: _('msb2_ms2_field_discount_amount'),
                        anchor: '100%',
                    }],
                }],
            }*/]
        }];
    },

    /**
     * @param config
     * @returns {{}}
     */
    getListeners: function (config) {
        return {};
    },

    /**
     * @param field
     * @param e
     */
    onSpecialKeyDown: function (field, e) {
        var fs = this;
        var button = fs.findBy(function (a) {
            return a['id'] === fs['id'] + '-submit';
        });
        button = button['length'] ? button[0] : undefined;
        if (e.getKey() === e.ENTER) {
            fs.submit(button, e);
        }
    },
});
Ext.reg('msb2-fieldset-order', msBonus2.ux.FieldsetOrder);


/**
 * @param config
 * @constructor
 */
msBonus2.ux.FieldsetForm = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        id: config['id'] || 'msb2-fieldset-form',
        url: msBonus2.config['connector_url'],
        items: this.getItems(config),
        listeners: this.getListeners(config),
        layout: config['layout'] || 'column',
        cls: (config['cls'] || '') + ' msb2-fieldset msb2-fieldset_large',
        style: config['style'] || {margin: '10px 0 0'},
        defaults: config['defaults'] || {msgTarget: 'under', border: false},
        onRender: function () {
            var fs = this;
            msBonus2.ux.FieldsetForm.superclass.onRender.apply(fs, arguments);
            // console.log('msBonus2.ux.FieldsetForm onRender fs', fs);

            // Get parent window
            fs['window'] = fs.findParentBy(function(a) {
                return a['xtype'] === 'msb2-window-user-update';
            }) || undefined;
        },
        isRequest: false,
    });
    msBonus2.ux.FieldsetForm.superclass.constructor.call(this, config);
    this['config'] = config;

    //
    this.on('afterrender', function (fs) {
        // console.log('msBonus2.ux.FieldsetForm afterrender fs', fs);

        //
        fs['messageMask'] = new Ext.LoadMask(fs['bwrap'], {
            msg: '&nbsp;',
            msgCls: 'x-mask-loading msb2-mask-loading msb2-mask-loading_without-animate',
        });
        fs['loadMask'] = new Ext.LoadMask(fs['bwrap'], {
            msg: _('msb2_ms2_message_loading'),
        });
        if (fs.isRequest) {
            fs.loadMask.show();
        }
    });

    this.initialize(config);
};
Ext.extend(msBonus2.ux.FieldsetForm, Ext.form.FieldSet, {
    /**
     * @param config
     * @constructor
     */
    initialize: function (config) {
        var fs = this;
        fs.addEvents({
            beforeInit: true,
            afterInit: true,
        });

        //
        if (fs.fireEvent('beforeInit')) {
            //

            fs.fireEvent('afterInit');
        }
    },

    /**
     * @param config
     * @returns {*[]}
     */
    getItems: function (config) {
        return [{
            columnWidth: 1,
            layout: 'form',
            style: {margin: ''},
            items: [{
                xtype: 'msb2-combo-action-type',
                id: config['id'] + '-type',
                name: 'msb2_type',
                fieldLabel: [
                    '<span class="action-red">*</span> ',
                    _('msb2_field_form_type')
                ].join(''),
                anchor: '100%',
                form: config['form'],
                value: 'offline',
                listeners: {
                    afterrender: {
                        fn: function (combo) {
                            var fs = this;
                            var w = fs['window'];
                            var store = combo.getStore();
                            store.on('load', function () {
                                if (!combo.getValue()) {
                                    combo.setValue(w.record.object['form_type']);
                                }
                            });
                            store.load();
                        },
                        scope: this,
                    },
                },
            }, {
                layout: 'column',
                border: false,
                style: {marginTop: '0px'},
                anchor: '100%',
                items: [{
                    columnWidth: .6,
                    layout: 'form',
                    style: {marginRight: '5px'},
                    items: [{
                        xtype: 'numberfield',
                        id: config['id'] + '-cost',
                        name: 'msb2_cost',
                        fieldLabel: [
                            '<span class="action-red">*</span> ',
                            _('msb2_field_form_cost')
                        ].join(''),
                        anchor: '100%',
                        allowDecimals: false,
                        allowNegative: false,
                        enableKeyEvents: true,
                        listeners: {
                            specialkey: {
                                fn: function (field, e) {
                                    this.refreshAccessCalculate(field, e);
                                    this.refreshAccessSubmit(field, e);
                                },
                                scope: this,
                            },
                            change: {
                                fn: function (field, e) {
                                    this.refreshAccessCalculate(field, e);
                                    this.refreshAccessSubmit(field, e);
                                },
                                scope: this,
                            },
                            keyup: {
                                fn: function (field, e) {
                                    this.refreshAccessCalculate(field, e);
                                    this.refreshAccessSubmit(field, e);
                                },
                                scope: this,
                            },
                        },
                    }],
                }, {
                    columnWidth: .4,
                    layout: 'form',
                    style: {marginTop: '37px', marginLeft: '5px'},
                    items: [{
                        xtype: 'button',
                        id: config['id'] + '-calculate',
                        text: _('msb2_button_form_calculate'),
                        cls: 'msb2-button',
                        anchor: '100%',
                        disabled: true,
                        handler: this.calculate,
                        scope: this,
                    }],
                }],
            }, {
                xtype: 'numberfield',
                id: config['id'] + '-points',
                name: 'msb2_points',
                fieldLabel: [
                    '<span class="action-red">*</span> ',
                    _('msb2_field_form_points')
                ].join(''),
                anchor: '100%',
                allowDecimals: false,
                allowNegative: false,
                enableKeyEvents: true,
                listeners: {
                    specialkey: {
                        fn: this.refreshAccessSubmit,
                        scope: this,
                    },
                    change: {
                        fn: this.refreshAccessSubmit,
                        scope: this,
                    },
                    keyup: {
                        fn: this.refreshAccessSubmit,
                        scope: this,
                    },
                },
            }, {
                xtype: 'button',
                id: config['id'] + '-submit',
                text: config['submitText'] || _('msb2_button_form_submit'),
                cls: 'msb2-button primary-button',
                style: {marginTop: '15px'},
                anchor: '100%',
                disabled: true,
                handler: this.submit,
                scope: this,
            }],
        }];
    },

    /**
     * @param button
     * @param e
     */
    calculate: function (button, e) {
        var fs = this;
        this.request(
            'calculate',
            {},
            function (eventName, response) {
                // console.log('calculate callback response', response);
            }
        );
    },

    /**
     * @param button
     * @param e
     */
    submit: function (button, e) {
        var fs = this;
        this.request(
            'submit',
            {},
            function (eventName, response) {
                // console.log('submit callback response', response);
            }
        );
    },

    /**
     */
    request: function (action, data, callback) {
        var fs = this;
        var $type = fs.getItemCmp(fs['id'] + '-type');
        var $cost = fs.getItemCmp(fs['id'] + '-cost');
        var $points = fs.getItemCmp(fs['id'] + '-points');
        if (!$type || !$cost || !$points) {
            return;
        }

        fs.isRequest = true;
        fs.loadMask && fs.loadMask.show();

        data = typeof(data) === 'object' ? data : {};
        data['user'] = fs['user'];
        data['form'] = fs['form'];
        data['type'] = $type.getValue();
        data['cost'] = $cost.getValue();
        data['points'] = $points.getValue();

        var properties = {
            url: fs.config['url'],
            params: Ext.apply({
                action: 'mgr/users/form/' + action,
            }, data),
            listeners: {
                success: {
                    fn: function (response) {
                        console.log('request success response', response);

                        fs.isRequest = false;
                        fs.loadMask && fs.loadMask.hide();

                        if (response['success'] && response['object']) {
                            // Show submit message
                            if (response['message']) {
                                fs.messageMask.msg = response['message'];
                                fs.messageMask.show();
                            }

                            // Set points
                            ['points'].forEach(function (k) {
                                var fieldCmp = fs.findBy(function (a) {return a.name === 'msb2_' + k});
                                if (fieldCmp['length'] && k in response['object'] && response.object[k] !== null) {
                                    fieldCmp = fieldCmp[0];
                                    fieldCmp.setValue(response.object[k] || 0);
                                    fieldCmp.fireEvent('keyup');
                                }
                            });

                            // Set window user amounts
                            if (response.object['user']) {
                                ['points', 'reserve', 'paid_money', 'paid_points'].forEach(function (k) {
                                    var fieldCmp = Ext.getCmp(fs.window['id'] + '-' + k.replace('_', '-'));
                                    if (!!fieldCmp) {
                                        fieldCmp.setValue(response.object.user[k]);
                                    }
                                });
                            }

                            //
                            if (fs['form'] === '-' && action === 'submit') {
                                var $formAccrual = Ext.getCmp(fs.window['id'] + '-form-accrual');
                                var $costAccrual = Ext.getCmp(fs.window['id'] + '-form-accrual-cost');
                                if (!!$formAccrual && !!$costAccrual) {
                                    if (response.object['cost']) {
                                        $costAccrual.setValue(response.object['cost']);
                                        $costAccrual.fireEvent('keyup');
                                        $formAccrual.calculate();
                                    }
                                }
                            }
                        }

                        callback && callback('success', response);
                    },
                    scope: fs,
                },
                failure: {
                    fn: function (response) {
                        console.log('request failure response', response);
                        fs.isRequest = false;
                        fs.loadMask && fs.loadMask.hide();
                        callback && callback('failure', response);
                    },
                    scope: fs,
                },
                cancel: {
                    fn: function (response) {
                        fs.isRequest = false;
                        fs.loadMask && fs.loadMask.hide();
                        callback && callback('cancel', response);
                    },
                    scope: fs,
                },
            },
        };

        if (action === 'submit') {
            MODx.msg.confirm(Ext.applyIf({
                title: _(fs['confirmTitleLexicon'] || '&nbsp;'),
                text: _(fs['confirmTextLexicon'] || '&nbsp;', {
                    cost: data['cost'],
                    points: data['points'],
                }),
            }, properties));
        } else {
            MODx.Ajax.request(properties);
        }
    },

    /**
     * @param field
     * @param e
     */
    refreshAccessCalculate: function (field, e) {
        var fs = this;
        var $field = fs.getItemCmp(fs['id'] + '-cost');
        var $button = fs.getItemCmp(fs['id'] + '-calculate');
        if (!$field || !$button) {
            return;
        }
        var isDisabled = !$field.getValue();
        $button.setDisabled(isDisabled);

        //
        if (isDisabled === false && fs.isRequest === false) {
            if (typeof(e) === 'object' && typeof(e.getKey) === 'function' && e.getKey() === e.ENTER) {
                fs.calculate($button, e);
            }
        }
    },

    /**
     * @param field
     * @param e
     */
    refreshAccessSubmit: function (field, e) {
        var fs = this;
        var $cost = fs.getItemCmp(fs['id'] + '-cost');
        var $points = fs.getItemCmp(fs['id'] + '-points');
        var $button = fs.getItemCmp(fs['id'] + '-submit');
        if (!$points || !$button || !$cost) {
            return;
        }
        var cost = parseInt($cost.getValue());
        var points = parseInt($points.getValue());
        var costEmpty = !cost && cost !== 0;
        var pointsEmpty = !points;

        console.log('typeof(cost)', typeof(cost));

        var isDisabled = costEmpty || pointsEmpty;
        $button.setDisabled(isDisabled);

        //
        if (isDisabled === false && fs.isRequest === false) {
            if (typeof(e) === 'object' && typeof(e.getKey) === 'function' && e.getKey() === e.ENTER) {
                fs.submit($button, e);
            }
        }
    },

    /**
     * @param config
     * @returns {{}}
     */
    getListeners: function (config) {
        return {};
    },

    /**
     * @param id
     * @returns {undefined}
     */
    getItemCmp: function (id) {
        var field = this.findBy(function (a) {
            return a['id'] === id;
        });
        return field['length'] ? field[0] : undefined;
    },
});
Ext.reg('msb2-fieldset-form', msBonus2.ux.FieldsetForm);