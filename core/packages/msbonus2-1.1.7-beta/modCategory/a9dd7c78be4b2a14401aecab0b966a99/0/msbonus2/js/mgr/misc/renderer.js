/**
 *
 * @param value
 * @param props
 * @param row
 * @returns {*}
 * @constructor
 */
msBonus2.renderer.Actions = function (value, props, row) {
    var res = [];
    var cls, icon, title, action, item;
    if (typeof(value) === 'object') {
        for (var i in value) {
            if (!value.hasOwnProperty(i)) {
                continue;
            }
            var a = value[i];
            if (!a['button']) {
                continue;
            }

            icon = a['icon'] ? a['icon'] : '';
            if (typeof(a['cls']) === 'object') {
                if (typeof(a['cls']['button']) !== 'undefined') {
                    icon += ' ' + a['cls']['button'];
                }
            } else {
                cls = a['cls'] ? a['cls'] : '';
            }
            action = a['action'] ? a['action'] : '';
            title = a['title'] ? a['title'] : '';

            item = String.format(
                '<li class="{0}"><button class="btn btn-default {1}" action="{2}" title="{3}"></button></li>',
                cls, icon, action, title
            );

            res.push(item);
        }
    }

    return String.format(
        '<ul class="msb2-grid-col__actions">{0}</ul>',
        res.join('')
    );
};

/**
 *
 * @param string
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.Date = function (string) {
    if (string && string != '0000-00-00 00:00:00' && string != '-1-11-30 00:00:00' && string != 0) {
        var date = /^[-0-9]+$/.test(string)
            ? new Date(string * 1000)
            : new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));
        var format = MODx.config['msb2_backend_date_format'];
        if (!format) {
            format = '%d.%m.%Y';
        }

        return String.format(
            '<div class="msb2-grid-col__date msb2-grid-col__value">{0}</div>',
            strftime(format, date)
        );
    }
    return '';
};

/**
 *
 * @param string
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.DateTime = function (string) {
    if (string && string != '0000-00-00 00:00:00' && string != '-1-11-30 00:00:00' && string != 0) {
        var date = /^[-0-9]+$/.test(string)
            ? new Date(string * 1000)
            : new Date(string.replace(/(\d+)-(\d+)-(\d+)/, '$2/$3/$1'));
        var format = MODx.config['msb2_backend_datetime_format'];
        if (!format) {
            format = '%d.%m.%Y <span class="action-gray">%H:%M</span>';
        }

        return String.format(
            '<div class="msb2-grid-col__datetime msb2-grid-col__value">{0}</div>',
            strftime(format, date)
        );
    }
    return '';
};

/**
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.Value = function (val, props, row) {
    return String.format(
        '<div class="msb2-grid-col__value">{0}</div>',
        val
    );
};

/**
 *
 * @param val
 * @returns {*}
 * @constructor
 */
msBonus2.renderer.Boolean = function (val) {
    return String.format(
        '<div class="msb2-grid-col__boolean msb2-grid-col__value {0}">{1}</div>',
        val ? 'green' : 'red',
        _(val ? 'yes' : 'no')
    );
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.Order = function (val, props, row) {
    var rec = row['json'];
    if (rec['order_id']) {
        return String.format(
            '<div class="msb2-grid-col__order msb2-grid-col__value">' +
                '<a class="msb2-grid-col__order-link" href="index.php?a=mgr/orders&namespace=minishop2&order={0}" target="_blank">' +
                    '{1}' +
                '</a>' +
            '</div>',
            rec['order_id'],
            rec['order_num']
        );
    } else {
        return String.format(
            '<div class="msb2-grid-col__order msb2-grid-col__value">—</div>'
        );
    }
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.User = function (val, props, row) {
    var rec = row['json'];
    return String.format(
        '<div class="msb2-grid-col__user msb2-grid-col__value">' +
            '<div class="msb2-grid-col__user-name">{0}</div>' +
            '<div class="msb2-grid-col__user-email">{1}</div>' +
        '</div>',
        rec['fullname'] || '',
        rec['email']
    );
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.UserLevel = function (val, props, row) {
    var rec = row['json'];
    return String.format(
        '<div class="msb2-grid-col__user-level msb2-grid-col__value">' +
            '<div class="msb2-grid-col__user-level-name">{0}</div>' +
            '<div class="msb2-grid-col__user-level-data">> {2} ({1}%)</div>' +
        '</div>',
        rec['level_name'] || '',
        rec['level_bonus'],
        rec['level_cost_formatted']
    );
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.LevelBonus = function (val, props, row) {
    var rec = row['json'];
    return String.format(
        '<div class="msb2-grid-col__level-bonus msb2-grid-col__value">{0}%</div>',
        rec['bonus']
    );
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.LevelCost = function (val, props, row) {
    var rec = row['json'];
    return String.format(
        '<div class="msb2-grid-col__level-cost msb2-grid-col__value">> {0}</div>',
        rec['cost_formatted']
    );
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.LogAction = function (val, props, row) {
    var rec = row['json'];
    return String.format(
        '<div class="msb2-grid-col__log-action msb2-grid-col__log-action_{0} msb2-grid-col__value">' +
            '{1}' +
        '</div>',
        rec['action'] === '+' ? 'plus' : 'minus',
        rec['action_formatted']
    );
};

/**
 *
 * @param val
 * @param props
 * @param row
 * @returns {string}
 * @constructor
 */
msBonus2.renderer.CustomField = function (val, props, row) {
    var rec = row['json'];
    return String.format(
        '<div class="msb2-grid-col__customfield msb2-grid-col__value">{0}</div>',
        rec['customfield']
    );
};