/**
 *
 */
Ext.ComponentMgr.onAvailable('minishop2-window-order-update', function () {
    var w = this;
    var order_id = w.record['id'] || 0;
    var tabs = w.fields.items;

    //
    var add;
    var mainTabItems = [];
    tabs[0].items.forEach(function (row) {
        var items = [];
        if ('length' in row) {
            items = row;
        } else {
            items.push(row);
        }

        var add = undefined;
        items.map(function (item) {
            if (item.xtype === 'fieldset' && item.items[0].items.some(function (i) {
                return i.name === 'num';
            })) {
                add = {
                    xtype: 'msb2-fieldset-order',
                    id: w['id'] + '-msb2',
                    order: order_id,
                };
            }
        });

        // Push
        mainTabItems.push(row);
        if (typeof(add) !== 'undefined') {
            mainTabItems.push(add);
        }
    });
    tabs[0].items = mainTabItems;

    //
    tabs.forEach(function (tab, idx) {
        if (idx === 0) {
            tab['listeners'] = typeof(tab['listeners']) === 'object' ? tab['listeners'] : {};
            tab.listeners['show'] = function (grid) {
                var msb2cmp = Ext.getCmp(w['id'] + '-msb2');
                msb2cmp && msb2cmp.refresh();
            }
        }
    });
});