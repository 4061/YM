msBonus2.grid.Users = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-grid-users';
    }
    config['actionPrefix'] = 'mgr/users/';
    Ext.applyIf(config, {
        baseParams: {
            action: config['actionPrefix'] + 'getlist',
            sort: 'createdon',
            dir: 'DESC',
        },
        multi_select: true,
        // pageSize: Math.round(MODx.config['default_per_page'] / 2),
    });
    msBonus2.grid.Users.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.grid.Users, msBonus2.grid.Default, {
    getFields: function (config) {
        return [
            'id',
            'user',

            'username',
            'fullname',
            'email',
            'mobilephone',
            'dob',

            'level',
            'points',
            'points_formatted',
            'reserve',
            'reserve_formatted',
            'paid_money',
            'paid_money_formatted',
            'paid_points',
            'paid_points_formatted',

            'actions',
        ];
    },

    getColumns: function (config) {
        return [{
            header: _('msb2_grid_id'),
            dataIndex: 'id',
            width: 45,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
            hidden: true,
        }, {
            header: _('msb2_grid_id'),
            dataIndex: 'user',
            width: 45,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_user'),
            dataIndex: 'username',
            width: 200,
            sortable: true,
            renderer: msBonus2.renderer['User'],
        }, {
            header: _('msb2_grid_mobilephone'),
            dataIndex: 'mobilephone',
            width: 110,
            sortable: false,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_dob'),
            dataIndex: 'dob',
            width: 90,
            sortable: false,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Date'],
        }, {
            header: _('msb2_grid_points'),
            dataIndex: 'points_formatted',
            width: 85,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_reserve'),
            dataIndex: 'reserve_formatted',
            width: 85,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_paid_points'),
            dataIndex: 'paid_points_formatted',
            width: 100,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_paid_money'),
            dataIndex: 'paid_money_formatted',
            width: 100,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_level'),
            dataIndex: 'level',
            width: 120,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['UserLevel'],
        }, {
            header: '&nbsp;',
            dataIndex: 'actions',
            id: 'actions',
            width: 50,
            sortable: false,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Actions'],
        }];
    },

    getTopBar: function (config) {
        return [{
            xtype: 'msb2-combo-level',
            id: config['id'] + '-level',
            filterName: 'level',
            emptyText: _('msb2_grid_level') + '...',
            width: 150,
            filter: true,
            listeners: {
                select: {fn: this._doFilter, scope: this},
            },
        }, '->', this.getSearchField(config)];
    },

    getListeners: function (config) {
        return {
            rowDblClick: function (grid, rowIndex, e) {
                var row = grid.store.getAt(rowIndex);
                this.updateObject(grid, e, row);
            },
        };
    },

    createObject: function (btn, e) {
        var w = MODx.load({
            xtype: 'msb2-window-user-create',
            id: Ext.id(),
            listeners: {
                success: {fn: this._listenerRefresh, scope: this},
                // hide: {fn: this._listenerRefresh, scope: this},
                failure: {fn: this._listenerHandler, scope: this},
            },
        });
        w.reset();
        w.setValues({
            active: true,
        });
        w.show(e['target']);
    },

    updateObject: function (btn, e, row, activeTab) {
        if (typeof(row) !== 'undefined') {
            this.menu.record = row.data;
        } else if (!this.menu.record) {
            return false;
        }
        var id = this.menu.record.id;

        if (typeof(activeTab) === 'undefined') {
            activeTab = 0;
        }

        MODx.Ajax.request({
            url: this.config['url'],
            params: {
                action: this['actionPrefix'] + 'get',
                id: id,
            },
            listeners: {
                success: {
                    fn: function (r) {
                        var values = r['object'];
                        ['dob'].forEach(function (k) {
                            if (values[k] !== '' && values[k] !== null) {
                                values[k] = '' + values[k];
                            }
                        });

                        var w = MODx.load({
                            xtype: 'msb2-window-user-update',
                            // id: Ext.id(),
                            record: r,
                            activeTab: activeTab,
                            listeners: {
                                success: {fn: this._listenerRefresh, scope: this},
                                // hide: {fn: this._listenerRefresh, scope: this},
                                failure: {fn: this._listenerHandler, scope: this},
                            },
                        });
                        w.reset();
                        w.setValues(values);
                        w.show(e['target']);
                    }, scope: this
                },
                failure: {fn: this._listenerHandler, scope: this},
            }
        });
    },

    // enableObject: function () {
    //     this.loadMask.show();
    //     return this._doAction('enable');
    // },
    //
    // disableObject: function () {
    //     this.loadMask.show();
    //     return this._doAction('disable');
    // },
    //
    // removeObject: function () {
    //     return this._doAction('remove', null, true);
    // },
});
Ext.reg('msb2-grid-users', msBonus2.grid.Users);