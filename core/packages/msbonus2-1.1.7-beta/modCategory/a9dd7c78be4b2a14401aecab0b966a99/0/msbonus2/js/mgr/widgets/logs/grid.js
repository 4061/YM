msBonus2.grid.Logs = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-grid-logs';
    }
    config['actionPrefix'] = 'mgr/logs/';
    Ext.applyIf(config, {
        baseParams: {
            action: config['actionPrefix'] + 'getlist',
            user: config['user'] || 0,
            sort: 'id',
            dir: 'DESC',
        },
        multi_select: true,
        pageSize: Math.round(MODx.config['default_per_page'] / 2),
        cls: 'msb2-grid_small',
    });
    msBonus2.grid.Logs.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.grid.Logs, msBonus2.grid.Default, {
    getFields: function (config) {
        return [
            'id',
            'user',
            'order',
            'type',
            'action',
            'action_formatted',
            'amount',
            'amount_formatted',
            'createdon',
        ];
    },

    getColumns: function (config) {
        return [{
            header: _('msb2_grid_id'),
            dataIndex: 'id',
            width: 50,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
            hidden: true,
        }, {
            header: _('msb2_grid_log_user'),
            dataIndex: 'user',
            width: 250,
            sortable: true,
            renderer: msBonus2.renderer['User'],
            // hidden: true,
        }, {
            header: _('msb2_grid_log_action'),
            dataIndex: 'action_formatted',
            width: 400,
            sortable: false,
            renderer: msBonus2.renderer['LogAction'],
        }, {
            header: _('msb2_grid_log_amount'),
            dataIndex: 'amount_formatted',
            width: 80,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_log_createdon'),
            dataIndex: 'createdon',
            width: 80,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['DateTime'],
            hidden: false,
        }, {
            header: _('msb2_grid_order'),
            dataIndex: 'order',
            width: 80,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Order'],
            hidden: true,
        }];
    },

    getTopBar: function (config) {
        return [];
    },
});
Ext.reg('msb2-grid-logs', msBonus2.grid.Logs);