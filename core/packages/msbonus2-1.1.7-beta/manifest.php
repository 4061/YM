<?php return array (
  'manifest-version' => '1.1',
  'manifest-attributes' => 
  array (
    'changelog' => '1.1.7-beta
==============
- Added setting "Days for bonuses in honor of the birthday"
- Fixed loading logs lexicon in msBonus2::getActionType

1.1.6-beta2
==============
- Fixed Russian language

1.1.6-beta
==============
- Fixed checking statuses in plugin event msOnChangeOrderStatus

1.1.5-beta
==============
- Fixed query to logs of user
- Fixed msb2Manager::setAction

1.1.4-beta
==============
- Added showing bonus discount in cart page
- Fixed injecting fieldset on opening order window

1.1.3-beta
==============
- Fixed write-off expired points if user balance is less than zero

1.1.2-beta
==============
- Added ability to specify zero bonus for a level
- Added ability remove second system level
- Fixed the addition of amount paid_money

1.1.1-beta
==============
- Added setting "Writing off points from products with not empty old price"

1.1.0-pl
==============
- Fixed method msb2Manager::unsetOrderAccrual

1.1.0-beta
==============
- Added reserve balance
- Added activation time for bonus
- Added lifetime points
- Fixed method msb2Manager::getJoinedUser, if $id equal 0
- Fixed errors on install package

1.0.3-pl2
==============
- Fixed date of birthday in users grid

1.0.3-pl
==============
- Fixed creating msb2User object on adding user from backend

1.0.3-beta
==============
- Fixed creating msb2User object

1.0.2-beta
==============
- Added prepare of order cost to JS
- Fixed refresh of order cost after reload page from browser cache
- Fixed negative cost on manual accrual and write-off
- Fixed work in other contexts

1.0.1-beta
==============
- Added refresh order cost on submit points to cart
- Added jQuery triggers msb2_set and msb2_unset
- Added method msb2Manager::refreshOrderWriteoff
- Added ability to specify empty cost of order for manual accrual and write-off
- Fixed createdby user id on accrual points for signup
- Fixed getting the user who committed action in the logs in backend

1.0.0-beta
==============
- Start',
    'license' => 'GNU GENERAL PUBLIC LICENSE
   Version 2, June 1991
--------------------------

Copyright (C) 1989, 1991 Free Software Foundation, Inc.
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.

Preamble
--------

  The licenses for most software are designed to take away your
freedom to share and change it.  By contrast, the GNU General Public
License is intended to guarantee your freedom to share and change free
software--to make sure the software is free for all its users.  This
General Public License applies to most of the Free Software
Foundation\'s software and to any other program whose authors commit to
using it.  (Some other Free Software Foundation software is covered by
the GNU Library General Public License instead.)  You can apply it to
your programs, too.

  When we speak of free software, we are referring to freedom, not
price.  Our General Public Licenses are designed to make sure that you
have the freedom to distribute copies of free software (and charge for
this service if you wish), that you receive source code or can get it
if you want it, that you can change the software or use pieces of it
in new free programs; and that you know you can do these things.

  To protect your rights, we need to make restrictions that forbid
anyone to deny you these rights or to ask you to surrender the rights.
These restrictions translate to certain responsibilities for you if you
distribute copies of the software, or if you modify it.

  For example, if you distribute copies of such a program, whether
gratis or for a fee, you must give the recipients all the rights that
you have.  You must make sure that they, too, receive or can get the
source code.  And you must show them these terms so they know their
rights.

  We protect your rights with two steps: (1) copyright the software, and
(2) offer you this license which gives you legal permission to copy,
distribute and/or modify the software.

  Also, for each author\'s protection and ours, we want to make certain
that everyone understands that there is no warranty for this free
software.  If the software is modified by someone else and passed on, we
want its recipients to know that what they have is not the original, so
that any problems introduced by others will not reflect on the original
authors\' reputations.

  Finally, any free program is threatened constantly by software
patents.  We wish to avoid the danger that redistributors of a free
program will individually obtain patent licenses, in effect making the
program proprietary.  To prevent this, we have made it clear that any
patent must be licensed for everyone\'s free use or not licensed at all.

  The precise terms and conditions for copying, distribution and
modification follow.


GNU GENERAL PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
---------------------------------------------------------------

  0. This License applies to any program or other work which contains
a notice placed by the copyright holder saying it may be distributed
under the terms of this General Public License.  The "Program", below,
refers to any such program or work, and a "work based on the Program"
means either the Program or any derivative work under copyright law:
that is to say, a work containing the Program or a portion of it,
either verbatim or with modifications and/or translated into another
language.  (Hereinafter, translation is included without limitation in
the term "modification".)  Each licensee is addressed as "you".

Activities other than copying, distribution and modification are not
covered by this License; they are outside its scope.  The act of
running the Program is not restricted, and the output from the Program
is covered only if its contents constitute a work based on the
Program (independent of having been made by running the Program).
Whether that is true depends on what the Program does.

  1. You may copy and distribute verbatim copies of the Program\'s
source code as you receive it, in any medium, provided that you
conspicuously and appropriately publish on each copy an appropriate
copyright notice and disclaimer of warranty; keep intact all the
notices that refer to this License and to the absence of any warranty;
and give any other recipients of the Program a copy of this License
along with the Program.

You may charge a fee for the physical act of transferring a copy, and
you may at your option offer warranty protection in exchange for a fee.

  2. You may modify your copy or copies of the Program or any portion
of it, thus forming a work based on the Program, and copy and
distribute such modifications or work under the terms of Section 1
above, provided that you also meet all of these conditions:

    a) You must cause the modified files to carry prominent notices
    stating that you changed the files and the date of any change.

    b) You must cause any work that you distribute or publish, that in
    whole or in part contains or is derived from the Program or any
    part thereof, to be licensed as a whole at no charge to all third
    parties under the terms of this License.

    c) If the modified program normally reads commands interactively
    when run, you must cause it, when started running for such
    interactive use in the most ordinary way, to print or display an
    announcement including an appropriate copyright notice and a
    notice that there is no warranty (or else, saying that you provide
    a warranty) and that users may redistribute the program under
    these conditions, and telling the user how to view a copy of this
    License.  (Exception: if the Program itself is interactive but
    does not normally print such an announcement, your work based on
    the Program is not required to print an announcement.)

These requirements apply to the modified work as a whole.  If
identifiable sections of that work are not derived from the Program,
and can be reasonably considered independent and separate works in
themselves, then this License, and its terms, do not apply to those
sections when you distribute them as separate works.  But when you
distribute the same sections as part of a whole which is a work based
on the Program, the distribution of the whole must be on the terms of
this License, whose permissions for other licensees extend to the
entire whole, and thus to each and every part regardless of who wrote it.

Thus, it is not the intent of this section to claim rights or contest
your rights to work written entirely by you; rather, the intent is to
exercise the right to control the distribution of derivative or
collective works based on the Program.

In addition, mere aggregation of another work not based on the Program
with the Program (or with a work based on the Program) on a volume of
a storage or distribution medium does not bring the other work under
the scope of this License.

  3. You may copy and distribute the Program (or a work based on it,
under Section 2) in object code or executable form under the terms of
Sections 1 and 2 above provided that you also do one of the following:

    a) Accompany it with the complete corresponding machine-readable
    source code, which must be distributed under the terms of Sections
    1 and 2 above on a medium customarily used for software interchange; or,

    b) Accompany it with a written offer, valid for at least three
    years, to give any third party, for a charge no more than your
    cost of physically performing source distribution, a complete
    machine-readable copy of the corresponding source code, to be
    distributed under the terms of Sections 1 and 2 above on a medium
    customarily used for software interchange; or,

    c) Accompany it with the information you received as to the offer
    to distribute corresponding source code.  (This alternative is
    allowed only for noncommercial distribution and only if you
    received the program in object code or executable form with such
    an offer, in accord with Subsection b above.)

The source code for a work means the preferred form of the work for
making modifications to it.  For an executable work, complete source
code means all the source code for all modules it contains, plus any
associated interface definition files, plus the scripts used to
control compilation and installation of the executable.  However, as a
special exception, the source code distributed need not include
anything that is normally distributed (in either source or binary
form) with the major components (compiler, kernel, and so on) of the
operating system on which the executable runs, unless that component
itself accompanies the executable.

If distribution of executable or object code is made by offering
access to copy from a designated place, then offering equivalent
access to copy the source code from the same place counts as
distribution of the source code, even though third parties are not
compelled to copy the source along with the object code.

  4. You may not copy, modify, sublicense, or distribute the Program
except as expressly provided under this License.  Any attempt
otherwise to copy, modify, sublicense or distribute the Program is
void, and will automatically terminate your rights under this License.
However, parties who have received copies, or rights, from you under
this License will not have their licenses terminated so long as such
parties remain in full compliance.

  5. You are not required to accept this License, since you have not
signed it.  However, nothing else grants you permission to modify or
distribute the Program or its derivative works.  These actions are
prohibited by law if you do not accept this License.  Therefore, by
modifying or distributing the Program (or any work based on the
Program), you indicate your acceptance of this License to do so, and
all its terms and conditions for copying, distributing or modifying
the Program or works based on it.

  6. Each time you redistribute the Program (or any work based on the
Program), the recipient automatically receives a license from the
original licensor to copy, distribute or modify the Program subject to
these terms and conditions.  You may not impose any further
restrictions on the recipients\' exercise of the rights granted herein.
You are not responsible for enforcing compliance by third parties to
this License.

  7. If, as a consequence of a court judgment or allegation of patent
infringement or for any other reason (not limited to patent issues),
conditions are imposed on you (whether by court order, agreement or
otherwise) that contradict the conditions of this License, they do not
excuse you from the conditions of this License.  If you cannot
distribute so as to satisfy simultaneously your obligations under this
License and any other pertinent obligations, then as a consequence you
may not distribute the Program at all.  For example, if a patent
license would not permit royalty-free redistribution of the Program by
all those who receive copies directly or indirectly through you, then
the only way you could satisfy both it and this License would be to
refrain entirely from distribution of the Program.

If any portion of this section is held invalid or unenforceable under
any particular circumstance, the balance of the section is intended to
apply and the section as a whole is intended to apply in other
circumstances.

It is not the purpose of this section to induce you to infringe any
patents or other property right claims or to contest validity of any
such claims; this section has the sole purpose of protecting the
integrity of the free software distribution system, which is
implemented by public license practices.  Many people have made
generous contributions to the wide range of software distributed
through that system in reliance on consistent application of that
system; it is up to the author/donor to decide if he or she is willing
to distribute software through any other system and a licensee cannot
impose that choice.

This section is intended to make thoroughly clear what is believed to
be a consequence of the rest of this License.

  8. If the distribution and/or use of the Program is restricted in
certain countries either by patents or by copyrighted interfaces, the
original copyright holder who places the Program under this License
may add an explicit geographical distribution limitation excluding
those countries, so that distribution is permitted only in or among
countries not thus excluded.  In such case, this License incorporates
the limitation as if written in the body of this License.

  9. The Free Software Foundation may publish revised and/or new versions
of the General Public License from time to time.  Such new versions will
be similar in spirit to the present version, but may differ in detail to
address new problems or concerns.

Each version is given a distinguishing version number.  If the Program
specifies a version number of this License which applies to it and "any
later version", you have the option of following the terms and conditions
either of that version or of any later version published by the Free
Software Foundation.  If the Program does not specify a version number of
this License, you may choose any version ever published by the Free Software
Foundation.

  10. If you wish to incorporate parts of the Program into other free
programs whose distribution conditions are different, write to the author
to ask for permission.  For software which is copyrighted by the Free
Software Foundation, write to the Free Software Foundation; we sometimes
make exceptions for this.  Our decision will be guided by the two goals
of preserving the free status of all derivatives of our free software and
of promoting the sharing and reuse of software generally.

NO WARRANTY
-----------

  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
REPAIR OR CORRECTION.

  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

---------------------------
END OF TERMS AND CONDITIONS',
    'readme' => '--------------------
msBonus2
--------------------
Author: John Doe <john@doe.com>
--------------------

A basic Extra for MODx Revolution.

Feel free to suggest ideas/improvements/bugs on GitHub:
http://github.com/username/msBonus2/issues',
    'chunks' => 
    array (
      'tpl.msBonus2.form' => '{*@formatter:off*}

<div class="msb2 {$is_active ? \'is-active\' : \'\'} [ js-msb2 ]">
    <div class="msb2-title">
        Оплатить бонусами
    </div>

    {* Form *}
    <div class="msb2-form">
        <input class="msb2-form__input [ js-msb2-input ]" {$is_active ? \'disabled="true"\' : \'\'}
               type="number" min="1" max="{$points}" name="msb2_amount"
               value="{$amount ?: \'\'}" placeholder="Сколько списать">
        <button class="msb2-form__button msb2-form__button_submit [ js-msb2-submit ]"
                type="button">Применить</button>
        <button class="msb2-form__button msb2-form__button_cancel [ js-msb2-cancel ]"
                type="button">Отменить</button>
    </div>

    <div class="">
        {* User points *}
        <div class="msb2-points">
            Бонусов: {$points ?: 0}
            {\'ms2_frontend_currency\' | lexicon}
        </div>

        {* Discount *}
        <div class="msb2-discount [ js-msb2-discount-wrap ]" style="display:{$is_active ? \'block\' : \'none\'}">
            Оплата бонусами:
            <span class="[ js-msb2-discount-value ]">{$amount ?: 0}</span>
            {\'ms2_frontend_currency\' | lexicon}
        </div>
    </div>

    {* Info *}
    {* Скрыто, потому что не ясно, как отображать % при применении бонусов не ко всему каталогу, а только к определённым разделам
    <div class="msb2-description">
        Вы можете оплатить бонусами до {\'msb2_cart_maximum_percent\' | config | replace : \'%\' : \'\'}% стоимости вашей корзины
    </div>
    *}

    {* Notifications *}
    <div class="msb2-message">
        <div class="msb2-message__error [ js-msb2-message-error ]">{$message_error}</div>
        <div class="msb2-message__success [ js-msb2-message-success ]">{$message_success}</div>
    </div>
</div>',
      'tpl.msBonus2.logs' => '{*@formatter:off*}
{if $_modx->user[\'id\']?}
    <div class="msb2-logs">
        <div class="msb2-logs-info">
            <div class="msb2-logs-info__level">
                <b>Уровень</b>: {$user[\'level\'][\'name\']} ({$user[\'level\'][\'bonus\']}%)
            </div>
            <div class="msb2-logs-info__points">
                <b>Бонусов</b>: {$user[\'points\'] | number : 0 : \'.\' : \' \'}
            </div>
            <div class="msb2-logs-info__reserve">
                <b>В резерве</b>: {$user[\'reserve\'] | number : 0 : \'.\' : \' \'}
            </div>
            <div class="msb2-logs-info__paid-points">
                <b>Оплачено бонусами</b>: {$user[\'paid_points\'] | number : 0 : \'.\' : \' \'}
            </div>
            <div class="msb2-logs-info__paid-money">
                <b>Оплачено деньгами</b>: {$user[\'paid_money\'] | number : 0 : \'.\' : \' \'}
            </div>
        </div>

        {if $logs?}
            <table class="msb2-logs-table">
                <thead>
                    <tr>
                        <th width="170">Когда</th>
                        <th width="120">Сколько</th>
                        <th width="">За что</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $logs as $v}
                        {var $style = \'style="color: \' ~ ($v[\'action\'] === \'+\' ? \'green\' : \'red\') ~ \'"\'}

                        <tr>
                            <td>
                                <div class="msb2-logs-table__createdon">
                                    <div class="msb2-logs-table__createdon-date">
                                        {$v[\'createdon\'] | date : \'d.m.Y\'}
                                    </div>
                                    <div class="msb2-logs-table__createdon-time">
                                        {$v[\'createdon\'] | date : \'H:i\'}
                                    </div>
                                </div>
                            </td>
                            <td {$style}>
                                {$v[\'action\']}{$v[\'amount\'] | number : 0 : \'.\' : \' \'}
                            </td>
                            <td {$style}>
                                {$v[\'action_formatted\']}
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        {/if}
    </div>
{else}
    ...
{/if}',
    ),
    'setup-options' => 'msbonus2-1.1.7-beta/setup-options.php',
  ),
  'manifest-vehicles' => 
  array (
    0 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOFileVehicle',
      'class' => 'xPDOFileVehicle',
      'guid' => 'f2dfb5e3b03ab517c24a5bce27e604fd',
      'native_key' => 'f2dfb5e3b03ab517c24a5bce27e604fd',
      'filename' => 'xPDOFileVehicle/28d97c50a6404d407359550763c4bfc7.vehicle',
    ),
    1 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOScriptVehicle',
      'class' => 'xPDOScriptVehicle',
      'guid' => '83634c10164c6f6a001dc5626c5c9122',
      'native_key' => '83634c10164c6f6a001dc5626c5c9122',
      'filename' => 'xPDOScriptVehicle/f7cda0137ffba131575470201edcde73.vehicle',
    ),
    2 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modNamespace',
      'guid' => 'db62ac2fed494180e1d026217fe6e300',
      'native_key' => 'msbonus2',
      'filename' => 'modNamespace/6d2bec924c76103504bf267b6e5f94c5.vehicle',
      'namespace' => 'msbonus2',
    ),
    3 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '648bece78050f7cf09a0ef29859e038a',
      'native_key' => 'msb2_form_action_types',
      'filename' => 'modSystemSetting/089848c84add321d7a108b14e3e9de9a.vehicle',
      'namespace' => 'msbonus2',
    ),
    4 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '21de15dcd805b1e964be7fba5c51260b',
      'native_key' => 'msb2_categories_for_accrual_of_points',
      'filename' => 'modSystemSetting/965d38574727530612df5876d8338498.vehicle',
      'namespace' => 'msbonus2',
    ),
    5 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'f6f5057c287493f2016355de16fb9aac',
      'native_key' => 'msb2_categories_for_writing_off_points',
      'filename' => 'modSystemSetting/4b1180b98a355fd3fa063b11a86c8cd2.vehicle',
      'namespace' => 'msbonus2',
    ),
    6 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '0e66125fc561edd62b5f80594874d807',
      'native_key' => 'msb2_writing_off_with_old_price',
      'filename' => 'modSystemSetting/63d186bdd9266fe7e354f6d74924115d.vehicle',
      'namespace' => 'msbonus2',
    ),
    7 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'a4ab944c12ae5e1a502ca81db79ca05f',
      'native_key' => 'msb2_cart_maximum_percent',
      'filename' => 'modSystemSetting/304031c7735d6d664453528eb23f05fd.vehicle',
      'namespace' => 'msbonus2',
    ),
    8 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4cbb0014188b49b5f5c3f1fdc22919a7',
      'native_key' => 'msb2_order_status_new',
      'filename' => 'modSystemSetting/1604e6c0365889993bec0d50406730e5.vehicle',
      'namespace' => 'msbonus2',
    ),
    9 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '78d1765e329bb4f529c68538fef18891',
      'native_key' => 'msb2_order_status_paid',
      'filename' => 'modSystemSetting/68d6553dc92b4547da94615e52fb040a.vehicle',
      'namespace' => 'msbonus2',
    ),
    10 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'a7ec34d262070530dec86aa943399ba8',
      'native_key' => 'msb2_order_status_cancel',
      'filename' => 'modSystemSetting/dc0e4ec3b782bf58bc8ddecead3be98e.vehicle',
      'namespace' => 'msbonus2',
    ),
    11 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '30d3730fcf528ce00e88381f739d9794',
      'native_key' => 'msb2_activation_time_for_bonus',
      'filename' => 'modSystemSetting/63016e3341e10ed129a41b2f812b2ebc.vehicle',
      'namespace' => 'msbonus2',
    ),
    12 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'fec84bf808fe64703cd4d537c94e5695',
      'native_key' => 'msb2_lifetime_for_bonus',
      'filename' => 'modSystemSetting/f2a6f79e5a9eae0b49f614d94798527f.vehicle',
      'namespace' => 'msbonus2',
    ),
    13 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'b97dd38ae43b5ca6b91e4902c7713f16',
      'native_key' => 'msb2_bonus_for_registration',
      'filename' => 'modSystemSetting/e2c547ffe4f7ce2f7a446a5f90e2ddaf.vehicle',
      'namespace' => 'msbonus2',
    ),
    14 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '09b0f5c009c0e3aaff8e373a6a12e5c7',
      'native_key' => 'msb2_bonus_for_birthday',
      'filename' => 'modSystemSetting/4d80e04eb4857c5117738091f2a6ffdf.vehicle',
      'namespace' => 'msbonus2',
    ),
    15 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => 'd453df031f7ab890474c3ab5d67a1ad6',
      'native_key' => 'msb2_bonus_for_birthday_days',
      'filename' => 'modSystemSetting/e34cb1a00496f61c1f3c6253f8436b8f.vehicle',
      'namespace' => 'msbonus2',
    ),
    16 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '4e2b751645d1b17384b5f0e7a8781312',
      'native_key' => 'msb2_frontend_css',
      'filename' => 'modSystemSetting/399cbf2aa3157186b5db769d4fc3e26b.vehicle',
      'namespace' => 'msbonus2',
    ),
    17 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modSystemSetting',
      'guid' => '870c2e7f2368dea3aea4ee6adfa2ce66',
      'native_key' => 'msb2_frontend_js',
      'filename' => 'modSystemSetting/eacd6b975ac10ae8d6049217a01d642b.vehicle',
      'namespace' => 'msbonus2',
    ),
    18 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOObjectVehicle',
      'class' => 'modMenu',
      'guid' => 'eb81722caf623a17a619da536c2318b0',
      'native_key' => 'msbonus2',
      'filename' => 'modMenu/68051eb81eccca219dee462b475b9fe4.vehicle',
      'namespace' => 'msbonus2',
    ),
    19 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'encryptedVehicle',
      'class' => 'modCategory',
      'guid' => 'd6fa59f1203599094c21de524a8485e6',
      'native_key' => NULL,
      'filename' => 'modCategory/a9dd7c78be4b2a14401aecab0b966a99.vehicle',
      'namespace' => 'msbonus2',
    ),
    20 => 
    array (
      'vehicle_package' => 'transport',
      'vehicle_class' => 'xPDOScriptVehicle',
      'class' => 'xPDOScriptVehicle',
      'guid' => 'ed66de116da579a9f497543cb172cd0b',
      'native_key' => 'ed66de116da579a9f497543cb172cd0b',
      'filename' => 'xPDOScriptVehicle/624e279e821be17e7fc22a3d4969c513.vehicle',
      'namespace' => 'msbonus2',
    ),
  ),
);