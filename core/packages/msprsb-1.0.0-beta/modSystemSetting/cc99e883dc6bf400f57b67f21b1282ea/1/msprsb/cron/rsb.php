<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);

/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->loadCustomClasses('payment');

if (!class_exists('RSBPaymentHandler')) {
    exit('Error: could not load payment class "RSBPaymentHandler".');
}

/** @var msOrder $order */
$order = $modx->newObject('msOrder');
$handler = new RSBPaymentHandler($order);

$handler->closeBusinessDay();