<?php
if (!class_exists('msPaymentInterface')) {
    require_once dirname(dirname(dirname(__FILE__))).'/minishop2/model/minishop2/mspaymenthandler.class.php';
}

/**
 * Class RSBPaymentHandler
 */
class RSBPaymentHandler extends msPaymentHandler implements msPaymentInterface
{
    /** @var array $config */
    public $config;
    /** @var modX $modx */
    public $modx;

    /**
     * RSBPaymentHandler constructor.
     *
     * @param xPDOObject $object
     * @param array $config
     */
    function __construct(xPDOObject $object, array $config = array())
    {
        parent::__construct($object, $config);
        $siteUrl = $this->modx->getOption('site_url');
        $assetsUrl = $this->modx->getOption('assets_url').'components/msprsb/';
        $paymentUrl = $siteUrl.substr($assetsUrl, 1).'payment/rsb.php';
        $merchantId = $this->modx->getOption('msprsb_merchand_id', null, '');
        $this->config = array_merge(array(
            'paymentUrl' => $paymentUrl,
//            'apiUrl' => 'https://testsecurepay.rsb.ru:9443/ecomm2/MerchantHandler',
//            'apiUrl' => 'https://securepay.rsb.ru:9443/ecomm2/MerchantHandler',
            'apiUrl' => $this->modx->getOption('msprsb_api_url', null, ''),
//            'checkoutUrl' => 'https://testsecurepay.rsb.ru/ecomm2/ClientHandler?trans_id=',
//            'checkoutUrl' => 'https://securepay.rsb.ru/ecomm2/ClientHandler?trans_id=',
            'checkoutUrl' => $this->modx->getOption('msprsb_checkout_url', null, ''),
            'sslPath' => dirname($this->modx->getOption('base_path')).'/ssl/',
            'tspId' => $merchantId,
            'tspKey' => $merchantId.'.key',
            'tspPem' => $merchantId.'.pem',
            'crtChain' => 'chain-ecomm-ca-root-ca.crt',

            'currency' => $this->modx->getOption('msprsb_currency_code', null, 643, true),
            'language' => $this->modx->getOption('msprsb_client_language', null, 'ru', true),
            'mrch_transaction_id' => 'SMS transaction',
            'msg_type' => 'SMS',
            'server_version' => '2.0',
        ), $config);
    }

    /**
     * @param msOrder $order
     * @return array|bool|string
     */
    public function send(msOrder $order)
    {
        if ($link = $this->getPaymentLink($order)) {
            return $this->success('', array(
                'redirect' => $link
            ));
        } else {
            return $this->success('', array(
                'msorder' => $order->get('id')
            ));
        }
    }

    /**
     * @param msOrder $order
     * @param array $params
     * @return array|bool|string
     */
    public function receive(msOrder $order, $params = array())
    {
        $accepted = false;
        if (!empty($params['trans_id'])) {
            $response = $this->request(array(
                'command' => 'c',
                'trans_id' => $params['trans_id'],
            ));
            $tmp = explode("\n", $response);
            $result = array();
            foreach ($tmp as $row) {
                $kv = explode(': ', $row);
                if (count($kv) == 2) {
                    $result[$kv[0]] = $kv[1];
                }
            }
            if (!empty($result['RESULT']) and $result['RESULT'] == 'OK'
                and !empty($result['RESULT_CODE'] and $result['RESULT_CODE'] == '000')) {
                $this->ms2->changeOrderStatus($order->get('id'), 2);
                $accepted = true;
            }
        }
        return $accepted;
    }

    /**
     * @param msOrder $order
     * @return string
     */
    public function getPaymentLink(msOrder $order)
    {
        $link = false;
        $this->modx->addPackage('msprsb', MODX_CORE_PATH.'components/msprsb/model/');
        if ($paymentLink = $this->modx->getObject('RSBLink', array('order' => $order->get('id')))) {
            $link = $this->config['checkoutUrl'].$paymentLink->get('trans_id');
        } else {
            $params = array(
                'command' => 'v',
                'amount' => round(number_format($order->get('cost'), 2, '.', '') * 100),
                'description' => 'Заказ #' . $order->get('num'),
                'currency' => $this->config['currency'],
                'language' => $this->config['language'],
                'mrch_transaction_id' => $this->config['mrch_transaction_id'],
                'msg_type' => $this->config['msg_type'],
            );
            $response = $this->request($params);
            if (preg_match('@(TRANSACTION_ID): ([a-z0-9\/\+\=]{28})@i', $response, $matches)) {
                $transId = $matches[2];
                $link = $this->config['checkoutUrl'].$transId;
                $paymentLink = $this->modx->newObject('RSBLink', array(
                    'order' => $order->get('id'),
                    'trans_id' => $transId
                ));
                $paymentLink->save();
            } else {
                $this->modx->log(modX::LOG_LEVEL_ERROR,
                    '[RSB] Error occured while creating payment. Request: ' . print_r($params,
                        1) . ', response: ' . print_r($response, 1));
            }
        }
        return $link;
    }

    public function request($params = array())
    {
        $params = array_merge(array(
            'server_version' => $this->config['server_version'],
        ), $params);

        if ($params['command'] != 'b') {
            $params['client_ip_addr'] = $this->getClientIP();
        }

        $request = http_build_query($params);
        $curlOptions = array(
            CURLOPT_URL => $this->config['apiUrl'],
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 35,
            CURLOPT_HEADER => false,
            CURLOPT_POST => true,
            CURLOPT_USERAGENT => 'Mozilla/5.0 Firefox/1.0.7',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSLKEY => $this->config['sslPath'].$this->config['tspKey'],
            CURLOPT_SSLCERT => $this->config['sslPath'].$this->config['tspPem'],
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_CAINFO => $this->config['sslPath'].$this->config['crtChain'],
            CURLOPT_SSLVERSION => 6,
            CURLOPT_POSTFIELDS => $request,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->modx->log(modX::LOG_LEVEL_ERROR, '[RSB] Bad request: '.print_r(curl_error($ch), true));
        }
        curl_close($ch);
        return $response;
    }

    public function closeBusinessDay()
    {
        $this->request(array(
            'command' => 'b',
        ));
    }

    public function getClientIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}