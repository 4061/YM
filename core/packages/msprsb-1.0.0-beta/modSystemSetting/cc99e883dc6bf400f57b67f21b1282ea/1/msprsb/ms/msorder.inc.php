<?php
return array(
    'composites' => array(
        'RSBLinks' => array(
            'class' => 'RSBLink',
            'local' => 'id',
            'foreign' => 'order',
            'cardinality' => 'many',
            'owner' => 'local'
        )
    ),
);