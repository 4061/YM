<?php
define('MODX_API_MODE', true);
require __DIR__.'/index.php';
$pdoFetch = new pdoFetch($modx);
$settings = array(
    'class' => 'modSystemSetting',
    'sortdir' => "ASC",
    'return' => 'data',
    'limit' => 10000
);
$pdoFetch->setConfig($settings);
$settings = $pdoFetch->run();
$settings = json_encode($settings);
file_put_contents(MODX_BASE_PATH."custom_settings.json", $settings);
