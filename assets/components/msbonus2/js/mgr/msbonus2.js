var msBonus2 = function (config) {
    config = config || {};
    msBonus2.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2, Ext.Component, {
    page: {},
    window: {},
    grid: {},
    tree: {},
    panel: {},
    formpanel: {},
    combo: {},
    config: {},
    view: {},
    ux: {},
    utils: {},
    renderer: {},
    fields: {},
});
Ext.reg('msbonus2', msBonus2);

msBonus2 = new msBonus2();