/**
 *
 * @param config
 * @constructor
 */
msBonus2.combo.Search = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        xtype: 'twintrigger',
        ctCls: 'x-field-search',
        allowBlank: true,
        msgTarget: 'under',
        emptyText: _('search'),
        name: 'query',
        triggerAction: 'all',
        clearBtnCls: 'x-field-search-clear',
        searchBtnCls: 'x-field-search-go',
        onTrigger1Click: this._triggerSearch,
        onTrigger2Click: this._triggerClear,
    });
    msBonus2.combo.Search.superclass.constructor.call(this, config);
    this.on('render', function () {
        this.getEl().addKeyListener(Ext.EventObject.ENTER, function () {
            this._triggerSearch();
        }, this);
        this.positionEl.setStyle('margin-right', '1px');
    });
    this.addEvents('clear', 'search');
};
Ext.extend(msBonus2.combo.Search, Ext.form.TwinTriggerField, {
    initComponent: function () {
        Ext.form.TwinTriggerField.superclass.initComponent.call(this);
        this.triggerConfig = {
            tag: 'span',
            cls: 'x-field-search-btns',
            cn: [
                {tag: 'div', cls: 'x-form-trigger ' + this.searchBtnCls},
                {tag: 'div', cls: 'x-form-trigger ' + this.clearBtnCls}
            ]
        };
    },
    _triggerSearch: function () {
        this.fireEvent('search', this);
    },
    _triggerClear: function () {
        this.fireEvent('clear', this);
    },
});
Ext.reg('msb2-field-search', msBonus2.combo.Search);


/**
 *
 * @param config
 * @constructor
 */
msBonus2.combo.DateTime = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        timePosition: 'right',
        allowBlank: true,
        hiddenFormat: 'U', // 'Y-m-d H:i:s',
        dateFormat: MODx.config['manager_date_format'],
        timeFormat: MODx.config['manager_time_format'],
        dateWidth: 120,
        timeWidth: 120,
        // hideTime: true,
    });
    msBonus2.combo.DateTime.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.combo.DateTime, Ext.ux.form.DateTime);
Ext.reg('msb2-datetime', msBonus2.combo.DateTime);


/**
 *
 * @param config
 * @constructor
 */
msBonus2.combo.Level = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        name: 'level',
        fieldLabel: config['name'] || 'level',
        hiddenName: config['name'] || 'level',
        displayField: 'display',
        valueField: 'value',
        fields: ['value', 'display'],
        url: msBonus2.config['connector_url'],
        baseParams: {
            action: 'mgr/combo/getlevels',
            filter: config['filter'] || 0,
        },
        pageSize: 20,
        typeAhead: false,
        editable: false,
        anchor: '100%',
        listEmptyText: '<div style="padding: 7px;">' + _('msb2_combo_list_empty') + '</div>',
        tpl: new Ext.XTemplate('\
            <tpl for="."><div class="x-combo-list-item msb2-combo-row">\
                <span class="msb2-combo-row__group msb2-combo-row__{value}">\
                    {display}\
                </span>\
            </div></tpl>',
            {compiled: true}
        ),
    });
    msBonus2.combo.Level.superclass.constructor.call(this, config);

    // Обновляем список при открытии
    this.on('expand', function () {
        this.getStore().load();
    }, this);
};
Ext.extend(msBonus2.combo.Level, MODx.combo.ComboBox);
Ext.reg('msb2-combo-level', msBonus2.combo.Level);


/**
 *
 * @param config
 * @constructor
 */
msBonus2.combo.ActionType = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        name: 'type',
        fieldLabel: config['name'] || 'type',
        hiddenName: config['name'] || 'type',
        displayField: 'display',
        valueField: 'value',
        fields: ['value', 'display'],
        url: msBonus2.config['connector_url'],
        baseParams: {
            action: 'mgr/combo/getactiontypes',
            form: config['form'] || 0,
        },
        pageSize: 20,
        typeAhead: false,
        editable: false,
        anchor: '100%',
        listEmptyText: '<div style="padding: 7px;">' + _('msb2_combo_list_empty') + '</div>',
        tpl: new Ext.XTemplate('\
            <tpl for="."><div class="x-combo-list-item msb2-combo-row">\
                <span class="msb2-combo-row__group msb2-combo-row__{value}">\
                    {display}\
                </span>\
            </div></tpl>',
            {compiled: true}
        ),
    });
    msBonus2.combo.ActionType.superclass.constructor.call(this, config);

    // // Обновляем список при открытии
    // this.on('afterrender', function () {
    //     this.getStore().load();
    // }, this);
};
Ext.extend(msBonus2.combo.ActionType, MODx.combo.ComboBox);
Ext.reg('msb2-combo-action-type', msBonus2.combo.ActionType);