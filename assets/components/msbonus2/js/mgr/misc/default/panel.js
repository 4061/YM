msBonus2.panel.Default = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        // id: Ext.id(),
        items: this.getItems(config),
        listeners: this.getListeners(config),

        baseCls: 'modx-formpanel',
        layout: 'anchor',
        hideMode: 'offsets',
        autoHeight: true,
    });
    msBonus2.panel.Default.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.panel.Default, MODx.Panel, {
    getItems: function (config) {
        return [];
    },

    getListeners: function (config) {
        return {};
    },
});
Ext.reg('msb2-panel-default', msBonus2.panel.Default);
