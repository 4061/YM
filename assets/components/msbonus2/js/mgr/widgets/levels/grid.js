msBonus2.grid.Levels = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-grid-levels';
    }
    config['actionPrefix'] = 'mgr/levels/';
    Ext.applyIf(config, {
        baseParams: {
            action: config['actionPrefix'] + 'getlist',
            sort: 'cost',
            dir: 'ASC',
        },
        multi_select: true,
        // pageSize: Math.round(MODx.config['default_per_page'] / 2),
    });
    msBonus2.grid.Levels.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.grid.Levels, msBonus2.grid.Default, {
    getFields: function (config) {
        return [
            'id',
            'bonus',
            'cost',
            'name',
            'description',
            'actions',
        ];
    },

    getColumns: function (config) {
        return [{
            header: _('msb2_grid_id'),
            dataIndex: 'id',
            width: 50,
            sortable: true,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_bonus'),
            dataIndex: 'bonus',
            width: 100,
            sortable: true,
            renderer: msBonus2.renderer['LevelBonus'],
        }, {
            header: _('msb2_grid_cost'),
            dataIndex: 'cost',
            width: 100,
            sortable: true,
            renderer: msBonus2.renderer['LevelCost'],
        }, {
            header: _('msb2_grid_name'),
            dataIndex: 'name',
            width: 200,
            sortable: true,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_description'),
            dataIndex: 'description',
            width: 400,
            sortable: false,
            renderer: msBonus2.renderer['Value'],
        }, {
            header: _('msb2_grid_actions'),
            dataIndex: 'actions',
            id: 'actions',
            width: 130,
            sortable: false,
            fixed: true,
            resizable: false,
            renderer: msBonus2.renderer['Actions'],
        }];
    },

    getTopBar: function (config) {
        return [{
            text: '<i class="icon icon-plus"></i>&nbsp;' + _('msb2_button_create'),
            cls: 'primary-button',
            handler: this.createObject,
            scope: this,
        }, '->', this.getSearchField(config)];
    },

    getListeners: function (config) {
        return {
            rowDblClick: function (grid, rowIndex, e) {
                var row = grid.store.getAt(rowIndex);
                this.updateObject(grid, e, row);
            },
        };
    },

    createObject: function (btn, e) {
        var w = MODx.load({
            xtype: 'msb2-window-level-create',
            id: Ext.id(),
            listeners: {
                success: {fn: this._listenerRefresh, scope: this},
                // hide: {fn: this._listenerRefresh, scope: this},
                failure: {fn: this._listenerHandler, scope: this},
            },
        });
        w.reset();
        w.setValues({
        });
        w.show(e['target']);
    },

    updateObject: function (btn, e, row, activeTab) {
        if (typeof(row) !== 'undefined') {
            this.menu.record = row.data;
        } else if (!this.menu.record) {
            return false;
        }
        var id = this.menu.record.id;

        if (typeof(activeTab) === 'undefined') {
            activeTab = 0;
        }

        MODx.Ajax.request({
            url: this.config['url'],
            params: {
                action: this['actionPrefix'] + 'get',
                id: id,
            },
            listeners: {
                success: {
                    fn: function (r) {
                        var values = r['object'];
                        ['createdon', 'updatedon'].forEach(function (k) {
                            if (values[k]) {
                                values[k] = '' + values[k];
                            }
                        });

                        var w = MODx.load({
                            xtype: 'msb2-window-level-update',
                            id: Ext.id(),
                            record: r,
                            activeTab: activeTab,
                            listeners: {
                                success: {fn: this._listenerRefresh, scope: this},
                                // hide: {fn: this._listenerRefresh, scope: this},
                                failure: {fn: this._listenerHandler, scope: this},
                            },
                        });
                        w.reset();
                        w.setValues(values);
                        w.show(e['target']);
                    }, scope: this
                },
                failure: {fn: this._listenerHandler, scope: this},
            }
        });
    },

    removeObject: function () {
        return this._doAction('remove', null, true);
    },
});
Ext.reg('msb2-grid-levels', msBonus2.grid.Levels);