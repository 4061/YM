/**
 * Вкладки/поля для окон добавления/редактирования
 *
 * @param config
 * @returns {{object}}
 * @constructor
 */
msBonus2.fields.Level = function (config) {
    var data = config['record'] ? config.record['object'] : null;

    var fields = {
        xtype: 'modx-tabs',
        border: true,
        autoHeight: true,
        // style: {marginTop: '10px'},
        anchor: '100% 100%',
        items: [{
            title: _('msb2_tab_main'),
            layout: 'form',
            cls: 'modx-panel msb2-panel',
            autoHeight: true,
            items: [],
        }],
        listeners: {
            afterrender: function (tabs) {
                // Рендерим вторую вкладку, иначе данные с неё не передаются в процессор
                tabs.setActiveTab(1);
                tabs.setActiveTab(0);

                if (config['activeTab']) {
                    tabs.setActiveTab(config['activeTab']);
                }
            },
        },
    };

    fields.items[0].items.push({
        layout: 'column',
        border: false,
        style: {marginTop: '0px'},
        anchor: '100%',
        items: [{
            columnWidth: .5,
            layout: 'form',
            style: {marginRight: '5px'},
            items: [{
                xtype: 'textfield',
                id: config['id'] + '-name',
                name: 'name',
                fieldLabel: _('msb2_field_name'),
                anchor: '100%',
            }, {
                layout: 'column',
                border: false,
                style: {marginTop: '0px'},
                anchor: '100%',
                items: [{
                    columnWidth: .5,
                    layout: 'form',
                    style: {marginRight: '5px'},
                    items: [{
                        xtype: 'numberfield',
                        id: config['id'] + '-bonus',
                        name: 'bonus',
                        fieldLabel: _('msb2_field_bonus'),
                        anchor: '100%',
                    }],
                }, {
                    columnWidth: .5,
                    layout: 'form',
                    style: {marginLeft: '5px'},
                    items: [{
                        xtype: 'numberfield',
                        id: config['id'] + '-cost',
                        name: 'cost',
                        fieldLabel: _('msb2_field_cost'),
                        anchor: '100%',
                    }],
                }],
            }],
        }, {
            columnWidth: .5,
            layout: 'form',
            style: {marginLeft: '5px'},
            items: [{
                xtype: 'textarea',
                id: config['id'] + '-description',
                name: 'description',
                fieldLabel: _('msb2_field_description'),
                height: 102,
                anchor: '100%',
            }],
        }],
    });

    if (data) {
        fields.items[0].items.push({
            xtype: 'hidden',
            id: config['id'] + '-id',
            name: 'id',
        });
    }

    return fields;
};

/**
 * Окно добавления объекта
 *
 * @param config
 * @constructor
 */
msBonus2.window.LevelCreate = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-window-level-create';
    }
    Ext.applyIf(config, {
        title: _('msb2_window_create'),
        baseParams: {
            action: 'mgr/levels/create',
        },
        modal: true,
    });
    msBonus2.window.LevelCreate.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.window.LevelCreate, msBonus2.window.Default, {
    getFields: function (config) {
        return msBonus2.fields.Level(config);
    },
});
Ext.reg('msb2-window-level-create', msBonus2.window.LevelCreate);

/**
 * Окно редактирования объекта
 *
 * @param config
 * @constructor
 */
msBonus2.window.LevelUpdate = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-window-level-update';
    }
    Ext.applyIf(config, {
        title: _('msb2_window_update'),
        baseParams: {
            action: 'mgr/levels/update',
        },
        modal: true,
    });
    msBonus2.window.LevelUpdate.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.window.LevelUpdate, msBonus2.window.Default, {
    getFields: function (config) {
        return msBonus2.fields.Level(config);
    },
});
Ext.reg('msb2-window-level-update', msBonus2.window.LevelUpdate);