/**
 * Вкладки/поля для окон добавления/редактирования
 *
 * @param config
 * @returns {{object}}
 * @constructor
 */
msBonus2.fields.User = function (config) {
    var data = config['record'] ? config.record['object'] : null;
    var fields = {
        xtype: 'modx-tabs',
        border: true,
        autoHeight: true,
        // style: {marginTop: '10px'},
        anchor: '100% 100%',
        items: [{
            title: _('msb2_tab_info'),
            layout: 'form',
            cls: 'modx-panel msb2-panel',
            autoHeight: true,
            items: [],
        }, {
            title: _('msb2_tab_action'),
            layout: 'form',
            cls: 'modx-panel msb2-panel',
            autoHeight: true,
            items: [],
        }, {
            title: _('msb2_tab_log'),
            layout: 'form',
            cls: 'modx-panel msb2-panel',
            autoHeight: true,
            items: [],
            listeners: {
                show: function (tab) {
                    var grid = Ext.getCmp('msb2-grid-logs');
                    grid.refresh();
                },
            },
        }],
        listeners: {
            // afterrender: function (tabs) {
            //     // Рендерим вторую вкладку, иначе данные с неё не передаются в процессор
            //     tabs.setActiveTab(1);
            //     tabs.setActiveTab(0);
            //
            //     if (config['activeTab']) {
            //         tabs.setActiveTab(config['activeTab']);
            //     }
            // },
        },
    };

    //
    var tabs = {
        info: fields.items[0].items,
        forms: fields.items[1].items,
        logs: fields.items[2].items,
    };

    /**
     *  Tab / Info
     */
    tabs['info'].push({
        layout: 'column',
        border: false,
        style: {marginTop: '-10px'},
        anchor: '100%',
        items: [{
            columnWidth: .5,
            layout: 'form',
            style: {marginRight: '5px'},
            items: [{
                xtype: 'textfield',
                id: config['id'] + '-username',
                name: 'username',
                fieldLabel: _('msb2_field_username'),
                anchor: '100%',
                readOnly: true,
            }, {
                xtype: 'textfield',
                id: config['id'] + '-email',
                name: 'email',
                fieldLabel: _('msb2_field_email'),
                anchor: '100%',
            }, {
                xtype: 'msb2-datetime',
                id: config['id'] + '-dob',
                name: 'dob',
                fieldLabel: _('msb2_field_dob'),
                anchor: '100%',
                hideTime: true,
                timeWidth: 0,
            }],
        }, {
            columnWidth: .5,
            layout: 'form',
            style: {marginLeft: '5px'},
            items: [{
                xtype: 'textfield',
                id: config['id'] + '-fullname',
                name: 'fullname',
                fieldLabel: _('msb2_field_fullname'),
                anchor: '100%',
            }, {
                xtype: 'textfield',
                id: config['id'] + '-mobilephone',
                name: 'mobilephone',
                fieldLabel: _('msb2_field_mobilephone'),
                anchor: '100%',
            }],
        }],
    });

    // tabs['info'].push({
    //     html: '<hr>',
    // });

    tabs['info'].push({
        layout: 'column',
        border: false,
        // style: {marginTop: '-20px'},
        anchor: '100%',
        items: [{
            columnWidth: .5,
            layout: 'form',
            style: {marginRight: '5px'},
            items: [{
                xtype: 'msb2-combo-level',
                id: config['id'] + '-level',
                name: 'level',
                fieldLabel: _('msb2_field_level'),
                anchor: '100%',
            }],
        }, {
            columnWidth: .5,
            layout: 'form',
            style: {marginLeft: '5px'},
            items: [{
                layout: 'column',
                border: false,
                style: {marginTop: '0px'},
                anchor: '100%',
                items: [{
                    columnWidth: .5,
                    layout: 'form',
                    style: {marginRight: '5px'},
                    items: [{
                        xtype: 'displayfield',
                        id: config['id'] + '-paid-points',
                        name: 'paid_points_formatted',
                        fieldLabel: _('msb2_field_paid_points'),
                        cls: 'msb2-field_display',
                    }],
                }, {
                    columnWidth: .5,
                    layout: 'form',
                    style: {marginLeft: '5px'},
                    items: [{
                        xtype: 'displayfield',
                        id: config['id'] + '-paid-money',
                        name: 'paid_money_formatted',
                        fieldLabel: _('msb2_field_paid_money'),
                        cls: 'msb2-field_display',
                    }],
                }],
            }],
        }],
    });

    if (data) {
        tabs['info'].push({
            xtype: 'hidden',
            id: config['id'] + '-id',
            name: 'id',
        });
    }

    /**
     *  Tab / Forms
     */
    if (data) {
        tabs['forms'].push({
            layout: 'column',
            border: false,
            style: {marginTop: '0px'},
            anchor: '100%',
            items: [{
                columnWidth: .5,
                layout: 'form',
                style: {marginRight: '5px'},
                items: [{
                    xtype: 'msb2-fieldset-form',
                    id: config['id'] + '-form-writeoff',
                    title: _('msb2_title_form_writeoff'),
                    submitText: _('msb2_button_form_writeoff'),
                    confirmTitleLexicon: 'msb2_title_form_writeoff',
                    confirmTextLexicon: 'msb2_confirm_form_writeoff',
                    anchor: '100%',
                    user: data['user'],
                    form: '-',
                }],
            }, {
                columnWidth: .5,
                layout: 'form',
                style: {marginLeft: '5px'},
                items: [{
                    xtype: 'msb2-fieldset-form',
                    id: config['id'] + '-form-accrual',
                    title: _('msb2_title_form_accrual'),
                    submitText: _('msb2_button_form_accrual'),
                    confirmTitleLexicon: 'msb2_title_form_accrual',
                    confirmTextLexicon: 'msb2_confirm_form_accrual',
                    anchor: '100%',
                    user: data['user'],
                    form: '+',
                }],
            }],
        });
    }

    /**
     *  Tab / Logs
     */
    if (data) {
        tabs['logs'].push({
            xtype: 'msb2-grid-logs',
            user: data['user'],
        });
    }

    return fields;
};

/**
 * Окно добавления объекта
 *
 * @param config
 * @constructor
 */
msBonus2.window.UserCreate = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-window-user-create';
    }
    Ext.applyIf(config, {
        title: _('msb2_window_create'),
        baseParams: {
            action: 'mgr/users/create',
        },
        modal: true,
        width: 700,
    });
    msBonus2.window.UserCreate.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.window.UserCreate, msBonus2.window.Default, {
    /**
     * @param config
     * @returns {{object}}
     */
    getFields: function (config) {
        return msBonus2.fields.User(config);
    },
});
Ext.reg('msb2-window-user-create', msBonus2.window.UserCreate);

/**
 * Окно редактирования объекта
 *
 * @param config
 * @constructor
 */
msBonus2.window.UserUpdate = function (config) {
    config = config || {};
    if (!config['id']) {
        config['id'] = 'msb2-window-user-update';
    }
    Ext.applyIf(config, {
        title: _('msb2_window_update'),
        baseParams: {
            action: 'mgr/users/update',
        },
        buttonAlign: 'left',
        modal: true,
        width: 700,
    });
    msBonus2.window.UserUpdate.superclass.constructor.call(this, config);

    var w = this;
    w.on('afterrender', function () {
        var $points = Ext.getCmp(w['id'] + '-points');
        if (!!$points) {
            $points.setValue(w.record.object['points_formatted']);
        }

        var $reserve = Ext.getCmp(w['id'] + '-reserve');
        if (!!$reserve) {
            $reserve.setValue(w.record.object['reserve_formatted']);
        }
    })
};
Ext.extend(msBonus2.window.UserUpdate, msBonus2.window.Default, {
    /**
     * @param config
     * @returns {{object}}
     */
    getFields: function (config) {
        return msBonus2.fields.User(config);
    },

    /**
     * @param config
     * @returns {*[]}
     */
    getButtons: function (config) {
        var buttons = msBonus2.window.UserUpdate.superclass.getButtons.apply(this, arguments);

        buttons.unshift(_('msb2_field_label_points'), {
            xtype: 'displayfield',
            id: config['id'] + '-points',
            name: 'points_formatted',
            cls: 'msb2-field_display-value',
            hideLabel: true,
        }, _('msb2_field_label_reserve'), {
            xtype: 'displayfield',
            id: config['id'] + '-reserve',
            name: 'reserve_formatted',
            cls: 'msb2-field_display-value',
            hideLabel: true,
        }, '->');

        return buttons;
    },
});
Ext.reg('msb2-window-user-update', msBonus2.window.UserUpdate);