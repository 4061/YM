msBonus2.page.Home = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        components: [{
            xtype: 'msbonus2-panel-home',
            renderTo: 'msbonus2-panel-home-div'
        }]
    });
    msBonus2.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(msBonus2.page.Home, MODx.Component);
Ext.reg('msbonus2-page-home', msBonus2.page.Home);