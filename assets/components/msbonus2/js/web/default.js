(function () {
    function msBonus2(options) {
        let self = this;
        self['initialized'] = false;
        self['running'] = false;
        self['fatal'] = false;
        ['actionUrl'].forEach(function (val, i, arr) {
            if (typeof(options[val]) === 'undefined' || options[val] === '') {
                console.error('[msBonus2] Bad config.', arr);
                self['fatal'] = true;
            }
        });
        if (self['fatal']) {
            return;
        }

        /**
         * @type {object}
         */
        self.Base = {
            /**
             * Инициализирует класс.
             * @returns {boolean}
             */
            initialize: function (options) {
                if (!self['initialized']) {
                    //
                    self['config'] = {
                    };
                    self['classes'] = {
                        loading: 'is-loading',
                        active: 'is-active',
                    };
                    self['selectors'] = {
                        wrap: '.js-msb2',
                        input: '.js-msb2-input',
                        submit: '.js-msb2-submit',
                        cancel: '.js-msb2-cancel',

                        discountWrap: '.js-msb2-discount-wrap',
                        discountValue: '.js-msb2-discount-value',

                        messageError: '.js-msb2-message-error',
                        messageSuccess: '.js-msb2-message-success',
                    };
                    self['sendDataTemplate'] = {
                        formData: null,
                    };
                    self['sendData'] = $.extend({}, self['sendDataTemplate']);

                    //
                    Object.keys(options).forEach(function (key) {
                        if (['selectors'].indexOf(key) !== -1) {
                            return;
                        }
                        self.config[key] = options[key];
                    });
                    ['selectors'].forEach(function (key) {
                        if (options[key]) {
                            Object.keys(options[key]).forEach(function (i) {
                                self.selectors[i] = options.selectors[i];
                            });
                        }
                    });
                }
                self['initialized'] = true;

                return self['initialized'];
            },

            /**
             * Запускает основные действия.
             * @returns {boolean}
             */
            run: function () {
                if (self['initialized'] && !self['running']) {
                    self.Form.initialize();

                    //
                    var ms2Interval = window.setInterval(function () {
                        if (typeof(miniShop2) !== 'undefined') {
                            clearInterval(ms2Interval);

                            //
                            miniShop2.Callbacks.add('Cart.add.response.success', 'msBonus2',
                                function(response) {
                                    var $wrap = $(document).find(self.selectors['wrap']);
                                    if ($wrap.hasClass(self.classes['active'])) {
                                        var $submit = $wrap.find(self.selectors['submit']);
                                        $submit.trigger('click');
                                    }
                                });
                            miniShop2.Callbacks.add('Cart.change.response.success', 'msBonus2',
                                function(response) {
                                    var $wrap = $(document).find(self.selectors['wrap']);
                                    if ($wrap.hasClass(self.classes['active'])) {
                                        var $submit = $wrap.find(self.selectors['submit']);
                                        $submit.trigger('click');
                                    }
                                });
                            miniShop2.Callbacks.add('Cart.remove.response.success', 'msBonus2',
                                function(response) {
                                    var $wrap = $(document).find(self.selectors['wrap']);
                                    if ($wrap.hasClass(self.classes['active'])) {
                                        var $submit = $wrap.find(self.selectors['submit']);
                                        $submit.trigger('click');
                                    }
                                });
                            miniShop2.Callbacks.add('Order.getcost.response.success', 'msBonus2',
                                function(response) {
                                    var $wrap = $(document).find(self.selectors['wrap']);
                                    var $input = $(document).find(self.selectors['input']);
                                    if ($wrap.hasClass(self.classes['active']) && $input.val() && response.data['cost']) {
                                        var order_cost = response.data['cost'] - parseFloat($input.val());
                                        $(document).find(miniShop2.Order.orderCost)
                                            .html(miniShop2.Utils.formatPrice(order_cost));
                                    }
                                });

                            //
                            miniShop2.Order.getcost();
                        }
                    }, 500);
                    window.setTimeout(function () {
                        clearInterval(ms2Interval);
                    }, 7000);
                }
                self['running'] = true;

                return self['running'];
            },
        };

        /**
         *
         * @type {{initialize: (function(): boolean)}}
         */
        self.Form = {
            /**
             * @returns {boolean}
             */
            initialize: function () {
                /**
                 * Set points
                 */
                $(document).on('click', self.selectors['submit'], function (e) {
                    e.preventDefault();

                    var $submit = $(this);
                    if (!$submit['length']) {
                        return false;
                    }
                    var $wrap = $submit.closest(self.selectors['wrap']);
                    var $input = $wrap.find(self.selectors['input']);

                    // Prepare query params
                    var sendData = $.extend({}, self['sendDataTemplate']);
                    sendData['formData'] = [{
                        name: 'ctx',
                        value: self.config['ctx'],
                    }, {
                        name: 'action',
                        value: 'points/set',
                    }, {
                        name: 'amount',
                        value: $input.val(),
                    }];
                    // console.log(sendData);

                    // Callbacks
                    var callbackBefore = function (response) {
                        var $wrap = $(document).find(self.selectors['wrap']);
                        if ($wrap.length) {
                            // Add loading class
                            $wrap.addClass(self.classes['loading']);
                        }
                    };
                    var callbackAfter = function (response) {
                        // console.log('points/set callbackAfter response', response);

                        //
                        miniShop2.Order.getcost();

                        // Show discount amount
                        self.Form.setDiscountAmount(response.data['amount'] || 0);

                        //
                        var $wrap = $(document).find(self.selectors['wrap']);
                        if ($wrap.length) {
                            var $input = $wrap.find(self.selectors['input']);

                            // Show message
                            self.Message
                                [response.success ? 'success' : 'error']
                                (response['message']);

                            //
                            if (response['success']) {
                                // Add active class
                                $wrap.addClass(self.classes['active']);

                                // Add disable attribute
                                $input.prop('disabled', true);

                                //
                                $input.val(response.data['amount']);
                            } else {
                                // Remove active class
                                $wrap.removeClass(self.classes['active']);

                                // Remove disable attribute
                                $input.prop('disabled', false);
                            }

                            // Remove loading class
                            $wrap.removeClass(self.classes['loading']);
                        }

                        $(document).trigger('msb2_set', response);
                    };

                    // Submit
                    self.sendData = $.extend({}, sendData);
                    self.Submit.post(callbackBefore, callbackAfter);
                });

                /**
                 * Unset points
                 */
                $(document).on('click', self.selectors['cancel'], function (e) {
                    e.preventDefault();

                    var $cancel = $(this);
                    if (!$cancel['length']) {
                        return false;
                    }

                    // Prepare query params
                    var sendData = $.extend({}, self['sendDataTemplate']);
                    sendData['formData'] = [{
                        name: 'ctx',
                        value: self.config['ctx'],
                    }, {
                        name: 'action',
                        value: 'points/unset',
                    }];

                    // Callbacks
                    var callbackBefore = function () {
                        var $wrap = $(document).find(self.selectors['wrap']);
                        if ($wrap.length) {
                            // Add loading class
                            $wrap.addClass(self.classes['loading']);
                        }
                    };
                    var callbackAfter = function (response) {
                        // console.log('points/unset callbackAfter response', response);

                        //
                        miniShop2.Order.getcost();

                        // Show discount amount
                        self.Form.setDiscountAmount(response.data['amount'] || 0);

                        //
                        var $wrap = $(document).find(self.selectors['wrap']);
                        if ($wrap.length) {
                            var $input = $wrap.find(self.selectors['input']);

                            // Show message
                            self.Message['error'](response['message']);

                            //
                            if (response['success']) {
                                // Remove active class
                                $wrap.removeClass(self.classes['active']);

                                // Remove disable attribute
                                $input.prop('disabled', false);
                            } else {
                                //
                            }

                            // Remove loading class
                            $wrap.removeClass(self.classes['loading']);
                        }

                        $(document).trigger('msb2_unset', response);
                    };

                    // Submit
                    self.sendData = $.extend({}, sendData);
                    self.Submit.post(callbackBefore, callbackAfter);
                });

                /**
                 * On touch Enter on keyboard
                 */
                $(document).on('keypress', self.selectors['input'], function (e) {
                    var key = e['which'];
                    if (key === 13) {
                        var $input = $(this);
                        if (!$input['length']) {
                            return false;
                        }
                        var $wrap = $input.closest(self.selectors['wrap']);
                        var $submit = $wrap.find(self.selectors['submit']);

                        $submit.trigger('click');
                        e.preventDefault();
                    }
                });

                return true;
            },

            /**
             * Show discount amount
             *
             * @param amount
             */
            setDiscountAmount: function (amount) {
                var $discountWrap = $(document).find(self.selectors['discountWrap']);
                var $discountValue = $discountWrap.find(self.selectors['discountValue']);
                if ($discountWrap.length && $discountValue.length) {
                    $discountValue.text(amount);
                    $discountWrap[amount ? 'show' : 'hide']();
                }
            }
        };

        /**
         * Отсылает запрос на сервер.
         * @type {object}
         */
        self.Submit = {
            timeout: 0, // замираем на пол секунды перед отсылкой запроса
            timestamp: 0,
            post: function (beforeCallback, afterCallback, timeout) {
                if (!self.sendData['formData']) {
                    return;
                }
                if (typeof(timeout) === 'undefined') {
                    timeout = self.Submit['timeout'];
                }
                timeout = parseInt(timeout) || 0;

                //
                var _post = function (beforeCallback, afterCallback) {
                    // Запускаем колбек перед отсылкой запроса
                    if (beforeCallback && $.isFunction(beforeCallback)) {
                        beforeCallback.call(this, self.sendData['formData']);
                    }

                    $.post(self.config['actionUrl'], self.sendData['formData'], function (response) {
                        // Запускаем колбек после отсылки запроса
                        if (afterCallback && $.isFunction(afterCallback)) {
                            afterCallback.call(this, response, self.sendData['formData']);
                        }

                        if (response['success']) {
                            //
                        } else {
                            // self.Message.error(response['message']);
                        }
                    }, 'json')
                        .fail(function () {
                            console.error('[msBonus2] Bad request.', self['sendData']);
                        })
                        .done(function () {
                        });
                };

                //
                if (timeout) {
                    // Записываем текущий timestamp и через 0.5 секунды проверяем его
                    // Если он не изменён, то посылаем запрос на сервер
                    // Нужно для того, чтобы не слать кучу запросов
                    // Шлём только последний запрос
                    var timestamp = (new Date().getTime());
                    self.Submit['timestamp'] = timestamp;
                    window.setTimeout(function () {
                        if (self.Submit['timestamp'] === timestamp) {
                            _post(beforeCallback, afterCallback);
                        }
                    }, timeout);
                } else {
                    _post(beforeCallback, afterCallback);
                }
            },
        };

        /**
         * Сообщения.
         * @type {object}
         */
        self.Message = {
            handle: function (type, message) {
                ['success', 'error'].forEach(function (val) {
                    var $message = $(self.selectors['message' + self.Tools.ucFirst(val)]);
                    if ($message.length) {
                        $message.html(type === val ? message : '');
                    }
                });
            },
            success: function (message) {
                self.Message.handle('success', message);
            },
            error: function (message) {
                self.Message.handle('error', message);
            },
        };

        /**
         * Инструменты.
         * @type {object}
         */
        self.Tools = {
            /**
             * @param string
             * @returns {string}
             */
            ucFirst: function (string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            },
        };

        /**
         * Initialize && Run!
         */
        self.Base.initialize(options) && self.Base.run();
    }

    window['msBonus2'] = msBonus2;
})();