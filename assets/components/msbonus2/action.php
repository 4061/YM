<?php
/** @var modX $modx */
if (!isset($modx)) {
    define('MODX_API_MODE', true);
    while (!isset($modx) && ($i = isset($i) ? --$i : 10)) {
        if (($file = dirname(!empty($file) ? dirname($file) : __FILE__) . '/index.php') AND !file_exists($file)) {
            continue;
        }
        require_once $file;
    }
    if (!is_object($modx)) {
        exit('{"success":false,"message":"Access denied"}');
    }
    $modx->getService('error', 'error.modError');
    $modx->getRequest();
    $modx->setLogLevel(modX::LOG_LEVEL_ERROR);
    $modx->setLogTarget('FILE');
    $modx->error->message = null;
    $modx->lexicon->load('default');
}
$ctx = !empty($_REQUEST['ctx']) ? $_REQUEST['ctx'] : $modx->context->get('key');
if ($ctx != $modx->context->get('key')) {
    $modx->switchContext($ctx);
}

/** @var msBonus2 $msb2 */
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/')) {
    exit($modx->toJSON(array('success' => false, 'message' => 'Class msBonus2 not found')));
}
$msb2->initialize($ctx, ['jsonResponse' => true]);
$manager = $msb2->getManager();

//
if (empty($_REQUEST['action'])) {
    exit($msb2->tools->failure('Access denied'));
}

//
switch ($_REQUEST['action']) {
    /**
     * Set points
     */
    case 'points/set':
        //
        $amount = $manager->setCartWriteoff(@$_REQUEST['amount'] ?: 0);
        if (is_numeric($amount) && !empty($amount)) {
            $response = $msb2->tools->success('msb2_front_success_set', [
                'amount' => $amount,
            ]);
        } elseif (is_string($amount)) {
            $response = $msb2->tools->failure($amount);
        } else {
            $response = $msb2->tools->failure('msb2_err_unexpected');
        }
        break;

    /**
     * Unset points
     */
    case 'points/unset':
        $manager->unsetCartWriteoff();
        $response = $msb2->tools->success('msb2_front_success_unset', [
            // 'points_amount' => 0,
        ]);
        break;

    default:
        $response = $msb2->tools->failure('Access denied');
}

@session_write_close();
exit($response);