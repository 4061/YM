miniShop2.plugin.msfieldsmanager = {
	getFields: function(config) {
		return {
			additive: {
				xtype: 'combo-boolean'
				,hiddenName: 'additive'
				,listeners: {
					afterrender: function(combo){
						var val = (config.record['additive'] == 'true' || config.record['additive'] == 1) ? 1 : 0;
						config.record['additive'] = val;
					}
				}
				,store: new Ext.data.SimpleStore({
					fields: ['d','v']
					,data: [[_('yes'),1],[_('no'),0]]
					})
				,fieldLabel: _('ms2_product_additive')
				,description: _('ms2_product_additive_help')
				,name: 'additive'
				,allowBlank:true
				,anchor: '100%'
			},
			price2: {
				xtype: 'numberfield'
				,fieldLabel: _('ms2_product_price2')
				,description: _('ms2_product_price2_help')
				,name: 'price2'
				,allowBlank:true
				,anchor: '100%'
			},
			restourants: {
				xtype: 'textfield'
				,fieldLabel: _('ms2_product_restourants')
				,description: _('ms2_product_restourants_help')
				,name: 'restourants'
				,allowBlank:true
				,anchor: '100%'
			},
			order_types: {
				xtype: 'textfield'
				,fieldLabel: _('ms2_product_order_types')
				,description: _('ms2_product_order_types_help')
				,name: 'order_types'
				,allowBlank:true
				,anchor: '100%'
			},
			gift: {
				xtype: 'combo-boolean'
				,hiddenName: 'gift'
				,listeners: {
					afterrender: function(combo){
						var val = (config.record['gift'] == 'true' || config.record['gift'] == 1) ? 1 : 0;
						config.record['gift'] = val;
					}
				}
				,store: new Ext.data.SimpleStore({
					fields: ['d','v']
					,data: [[_('yes'),1],[_('no'),0]]
					})
				,fieldLabel: _('ms2_product_gift')
				,description: _('ms2_product_gift_help')
				,name: 'gift'
				,allowBlank:true
				,anchor: '100%'
			},
			mailvisible: {
				xtype: 'combo-boolean'
				,hiddenName: 'mailvisible'
				,listeners: {
					afterrender: function(combo){
						var val = (config.record['mailvisible'] == 'true' || config.record['mailvisible'] == 1) ? 1 : 0;
						config.record['mailvisible'] = val;
					}
				}
				,store: new Ext.data.SimpleStore({
					fields: ['d','v']
					,data: [[_('yes'),1],[_('no'),0]]
					})
				,fieldLabel: _('ms2_product_mailvisible')
				,description: _('ms2_product_mailvisible_help')
				,name: 'mailvisible'
				,allowBlank:true
				,anchor: '100%'
			},
			third_action: {
				xtype: 'combo-boolean'
				,hiddenName: 'third_action'
				,listeners: {
					afterrender: function(combo){
						var val = (config.record['third_action'] == 'true' || config.record['third_action'] == 1) ? 1 : 0;
						config.record['third_action'] = val;
					}
				}
				,store: new Ext.data.SimpleStore({
					fields: ['d','v']
					,data: [[_('yes'),1],[_('no'),0]]
					})
				,fieldLabel: _('ms2_product_third_action')
				,description: _('ms2_product_third_action_help')
				,name: 'third_action'
				,allowBlank:true
				,anchor: '100%'
			}
		}
	},
	getColumns: function() {
		return {
			additive: {
				header: _('ms2_product_additive')
				,dataIndex: 'additive'
				,name: 'additive'
				,width: 60
				,editor: {
					xtype: 'combo-boolean'
					,renderer: 'boolean'
				}
			},
			price2: {
				header: _('ms2_product_price2')
				,dataIndex: 'price2'
				,name: 'price2'
				,editor: {
					xtype: 'numberfield'
				}
			},
			restourants: {
				header: _('ms2_product_restourants')
				,dataIndex: 'restourants'
				,name: 'restourants'
				,editor: {
					xtype: 'textfield'
				}
			},
			order_types: {
				header: _('ms2_product_order_types')
				,dataIndex: 'order_types'
				,name: 'order_types'
				,editor: {
					xtype: 'textfield'
				}
			},
			gift: {
				header: _('ms2_product_gift')
				,dataIndex: 'gift'
				,name: 'gift'
				,width: 60
				,editor: {
					xtype: 'combo-boolean'
					,renderer: 'boolean'
				}
			},
			mailvisible: {
				header: _('ms2_product_mailvisible')
				,dataIndex: 'mailvisible'
				,name: 'mailvisible'
				,width: 60
				,editor: {
					xtype: 'combo-boolean'
					,renderer: 'boolean'
				}
			},
			third_action: {
				header: _('ms2_product_third_action')
				,dataIndex: 'third_action'
				,name: 'third_action'
				,width: 60
				,editor: {
					xtype: 'combo-boolean'
					,renderer: 'boolean'
				}
			}
		}
	}
};