var SoftjetSync = function (config) {
    config = config || {};
    SoftjetSync.superclass.constructor.call(this, config);
};
Ext.extend(SoftjetSync, Ext.Component, {
    page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('softjetsync', SoftjetSync);

SoftjetSync = new SoftjetSync();