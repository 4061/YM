<?php
if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php')) {
    /** @noinspection PhpIncludeInspection */
    require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
} else {
    require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.core.php';
}
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var SoftjetSync $SoftjetSync */
$SoftjetSync = $modx->getService('SoftjetSync', 'SoftjetSync', MODX_CORE_PATH . 'components/softjetsync/model/');
$modx->lexicon->load('softjetsync:default');

// handle request
$corePath = $modx->getOption('softjetsync_core_path', null, $modx->getOption('core_path') . 'components/softjetsync/');
$path = $modx->getOption('processorsPath', $SoftjetSync->config, $corePath . 'processors/');
$modx->getRequest();

/** @var modConnectorRequest $request */
$request = $modx->request;
$request->handleRequest([
    'processors_path' => $path,
    'location' => '',
]);