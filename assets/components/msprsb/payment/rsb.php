<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);

//$modx->log(modX::LOG_LEVEL_ERROR, print_r($_POST, true));

/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->loadCustomClasses('payment');
$modx->addPackage('msprsb', MODX_CORE_PATH.'components/msprsb/model/');

if (!class_exists('RSBPaymentHandler')) {
    exit('Error: could not load payment class "RSBPaymentHandler".');
}

/**
 * @var RSBPaymentHandler $handler
 * @var RSBLink $paymentLink
 * @var msOrder $order
 */
if (!empty($_POST['trans_id'])
    and $paymentLink = $modx->getObject('RSBLink', array('trans_id' => $_POST['trans_id']))
    and $order = $paymentLink->getOne('Order')
    and $order->get('status') == 1
) {
    $handler = new RSBPaymentHandler($order);
    $success = $failure = $modx->makeUrl($modx->getOption('site_start', null, 1), 'web', array('msorder' => $order->get('id')), 'full');

    if ($id = $modx->getOption('msprsb_success_id', null, 0)) {
        $success = $modx->makeUrl($id, 'web', array('msorder' => $order->get('id')), 'full');
    }
    if ($id = $modx->getOption('msprsb_failure_id', null, 0)) {
        $failure = $modx->makeUrl($id, 'web', array('msorder' => $order->get('id')), 'full');
    }

    if ($handler->receive($order, array('trans_id' => $paymentLink->get('trans_id')))) {
        $redirect = $success;
    } else {
        $redirect = $failure;
    }
    $modx->sendRedirect($redirect);
    exit;
} else {
    exit('Access denied');
}