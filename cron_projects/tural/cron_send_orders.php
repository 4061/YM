<?php
define('MODX_API_MODE', true);
require dirname(__DIR__, 2).'/index.php';
require  MODX_BASE_PATH."integrations/integrations.class.php";
$integrations = new Integrations($modx);
$tural = $integrations->getTuralActions();
$sql="
SELECT 
    modx_ms2_orders.id as modx_order, 
    CURRENT_TIMESTAMP,
    modx_ms2_orders.createdon,
    DATE_ADD(CURRENT_TIMESTAMP, INTERVAL +600 SECOND) as current
FROM 
    modx_ms2_orders
WHERE
    modx_ms2_orders.createdon >= DATE_ADD(CURRENT_TIMESTAMP, INTERVAL +600 SECOND) AND 
    (modx_ms2_orders.status = 1 OR modx_ms2_orders.status = 15)
";
$orders = $modx->query($sql);
$orders = $orders->fetchAll(PDO::FETCH_ASSOC);
//echo json_encode($orders);

foreach ($orders as $order){
    echo $tural->sendOrder($modx->getObject("msOrder", $order['modx_order']));
    echo $tural->updateOrderStatus($modx->getObject("msOrder", $order['modx_order']));
}