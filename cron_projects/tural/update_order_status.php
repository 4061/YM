<?php
define('MODX_API_MODE', true);
require dirname(__DIR__, 2).'/index.php';
require  MODX_BASE_PATH."integrations/integrations.class.php";
$integrations = new Integrations($modx);
$tural = $integrations->getTuralActions(); 
$sql = "
SELECT 
    modx_ms2_orders.id as modx_order,
    modx_ms2_order_addresses.id_integration_order
FROM 
    modx_ms2_orders, modx_ms2_order_addresses
WHERE
    modx_ms2_order_addresses.id = modx_ms2_orders.address AND 
    modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'1' DAY)
";
$orders = $modx->query($sql);
$orders = $orders->fetchAll(PDO::FETCH_ASSOC);
foreach ($orders as $order){
    $order_object = $modx->getObject("msOrder", $order["modx_order"]);
    print_r($tural->updateOrderStatus($order_object));
}