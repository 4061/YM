<?php
echo "1 step<br>\n";
function makeDishes($items, $id)
{
    global $modx, $dishes, $notFount;
    foreach ($items as $item) {
        $query = $modx->newQuery('modResource');
        $query->where(array('introtext' => $id));
        $category = $modx->getObject('modResource', $query);
        if ($category) {
            $parent = $category->get('id');
        } else {
            echo "не найдена родительская группа для " . $item;
            return;
        }
        if ( !array_key_exists( $item, $dishes, ) ) {
            $notFount[] = $item;
        } else {
            $response = $modx->runProcessor('resource/create', array(
                'class_key' => 'msProduct',
                'pagetitle' => $dishes[$item]['name'],
                'price' => $dishes[$item]['price'],
                'parent' => $parent,
                'template' => 1,
                'published' => 1,
                'alias' => bin2hex(openssl_random_pseudo_bytes(16))
            ));
            if ($response->isError()) {
                $modx->log(modX::LOG_LEVEL_ERROR, "Error on : \n" . print_r($response->getAllErrors(), 1));
                echo $response->getAllErrors();
            } else {
                //return $response->response['object']['id'];
            }
        }


    }
}

function createCategory($arr)
{
    global $modx;

    $query = $modx->newQuery('modResource');
    $query->where(array('introtext' => $arr['id']));
    $category = $modx->getObject('modResource', $query);
    if ($category) {
        return $category->get('id');
    }

    if ($arr['parent'] == 0) {
        $parent = 2;
    } else {
        $query = $modx->newQuery('modResource');
        $query->where(array('introtext' => $arr['parent']));
        $category = $modx->getObject('modResource', $query);
        if ($category) {
            $parent = $category->get('id');
        } else {
            echo "не найдена родительская группа для " . $arr['name'];
            return;
        }
    }


    $response = $modx->runProcessor('resource/create', array(
        'class_key' => 'msCategory',
        'pagetitle' => $arr['name'],
        'longtitle' => $arr['parent'],
        'introtext' => (string)$arr['id'],
        'parent' => $parent,
        'template' => 1,
        'published' => 1,
        'alias' => bin2hex(openssl_random_pseudo_bytes(16))
    ));
    if ($response->isError()) {
        $modx->log(modX::LOG_LEVEL_ERROR, "Error on : \n" . print_r($response->getAllErrors(), 1));
        echo $response->getAllErrors();
    } else {
        return $response->response['object']['id'];
    }

}

function obhod($item, $parent)
{
    global $arr;
    if (array_key_exists("id", $item)) {
        $id = $item['id'];
        $name = $item['name'];
        $arr[$id]['id'] = $id;
        $arr[$id]['name'] = $name;
        $arr[$id]['parent'] = $parent;
        if (array_key_exists("items", $item)) {
            if (count($item['items'])) {
                obhod($item['items'], $id);
            }
        }
    } else {
        if (count($item)) {
            foreach ($item as $itemList) {
                obhod($itemList, $parent);
            }
        }
    }

}

function getDeserts($item, $parent)
{
    global $arr2;
    if (array_key_exists("id", $item)) {
        $id = $item['id'];
        $name = $item['name'];
        $arr2[$id]['id'] = $id;
        $arr2[$id]['name'] = $name;
        $arr2[$id]['parent'] = $parent;
        if (array_key_exists("dishes", $item)) {
            makeDishes($item['dishes'], $id);
        }
        if (array_key_exists("items", $item)) {
            if (count($item['items'])) {
                getDeserts($item['items'], $id);
            }
        }
    } else {
        if (count($item)) {
            foreach ($item as $itemList) {
                getDeserts($itemList, $parent);
            }
        }
    }

}

echo "step 2\n";
$notFount = [];


$str = file_get_contents('./menutree.json');
echo $str;
$json = json_decode($str, true);
echo "step 3\n";
$root = $json["data"]["selectors"][0];

$arr = [];

$id = $root['id'];
$name = $root['name'];
$parent = 0;

$arr[$id]['id'] = $id;
$arr[$id]['name'] = $name;
$arr[$id]['parent'] = 0;

obhod($root['items'], $id);
echo "step 4\n";
$str2 = file_get_contents('dishes.json');
$json2 = json_decode($str2, true);

$dishes = [];
foreach ($json2['data']['dishes'] as $dish) {
    echo $dish['id'];
    $dishes[$dish['id']]['id'] = $dish['id'];
    $dishes[$dish['id']]['name'] = $dish['name'];
    $dishes[$dish['id']]['price'] = $dish['price'];
}
echo "step 5\n";
define('MODX_API_MODE', true);
$path_to_config = (__DIR__) . '/core/config/config.inc.php';
require_once $path_to_config;
require_once 'index.php';
echo "step 6\n";

// Load main services
//$is_debug = true;
//$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');
//$modx->setLogLevel($is_debug ? modX::LOG_LEVEL_INFO : modX::LOG_LEVEL_ERROR);
$modx->getService('error', 'error.modError');
$modx->lexicon->load('minishop2:default');
$modx->lexicon->load('minishop2:manager');

// Time limit
//set_time_limit(0);
//$modx->log(modX::LOG_LEVEL_INFO, $tmp);
$miniShop2 = $modx->getService('minishop2', 'miniShop2', $modx->getOption('minishop2.core_path', null, $modx->getOption('core_path') . 'components/minishop2/') . 'model/minishop2/', array());

echo "step 7\n";
foreach ($arr as $category) {
    createCategory($category);
}
echo "step 8\n";

getDeserts($root['items'], $id);

print_r($notFount);
