<?php
require __DIR__.'/IIKO/iiko.class.php';
require __DIR__.'/PrimeHill/primehill.class.php';
require __DIR__.'/IIKO/iikoCard.class.php';
require __DIR__.'/rkeeper/rkeeper.class.php';
require __DIR__.'/Tural/tural.class.php';
require MODX_BASE_PATH."vendor/autoload.php";
//$response = $client->request('GET', 'http://core.appsj.su/get-app-settings.json');
//echo $response->getBody();

//$request = new \GuzzleHttp\Psr7\Request('GET', 'http://core.appsj.su/get-app-settings.json');
//$promise = $client->sendAsync($request)->then(function ($response) {
//    echo ($response->getBody());
//});
//$promise->wait();
global $modx;
class Integrations {
    public function __construct($modx){
        $this->modx = $modx;
        $this->pdoFetch = new pdoFetch($modx);
        $this->modx->client = new GuzzleHttp\Client();
    }
    //класс интеграции с айко
    public function getIikoActions(){
        $this->integrationActionsClass = new Iiko($this->modx);
        return $this->integrationActionsClass;
    }
    //класс интеграции с айко
    public function getIikoCardActions(){
        $this->integrationActionsClass = new IikoCard($this->modx);
        return $this->integrationActionsClass;
    }
    //класс интеграции с прайм-хилом
    public function getPrimeHillActions(){
        $this->integrationActionsClass = new PrimeHill($this->modx);
        return $this->integrationActionsClass;
    }
    //класс интеграции с кипером
    public function getRkeeperActions(){
        $this->integrationActionsClass = new Rkeeper($this->modx);
        return $this->integrationActionsClass;
    }
    public function getTuralActions(){
        $this->integrationActionsClass = new Tural($this->modx);
        return $this->integrationActionsClass;
    }
}