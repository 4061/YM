<?php
class PrimeHill extends Integrations{
    public function __construct($modx){
        $this->modx = $modx;
        $this->token = $this->modx->getOption("prime_hill_token");
        $this->loyalty_token = $this->modx->getOption("prime_hill_loyalty_token");

    }
    public function getToken(){
        return $this->token;
    }
    //зарегать клиента
    public function createClient($profile){
        if ($profile->dob) $birthday = date("Y-m-d", $profile->dob);
        $user_array["clients"][] = array(
            "lastName" => "Нет данных",
            "firstName" => $profile->fullname,
            "patronymic" => "Нет данных",
            "email" => $profile->email,
            "tags" => [],
            "sex" => 1,
            "cardBarcode" => "",
            "cardNumber" => "",
            "birthday"=> $birthday, 
            "templateId"=>$this->modx->getOption("ph_template"), //айди шаблона. пока непонятно что это, но очень важно
            "phone"=>$profile->mobilephone,
            "comment" => "Нет данных"
            );
        $vars["token"] = $this->token;
        try {
            $response = $this->modx->client->request('POST', 'https://cabinet.prime-hill.com/api/v2/createClients?'.http_build_query($vars), ['json'=>$user_array]);
            if ($response->getStatusCode() == 200){
                $ph_response = json_decode($response->getBody(),true);
                $profile->set("zip", $ph_response['response'][0]['clientId']);
                $profile->save();
            }
        } catch (\Throwable $e) {
            $this->modx->log(modX::LOG_LEVEL_INFO, $e->getMessage());
            return false;
        }
        /*$response = $this->modx->client->request('POST', 'https://cabinet.prime-hill.com/api/v2/createClients?'.http_build_query($vars), ['json'=>$user_array]);
        $ph_response = json_decode($response->getBody(),true);
        $profile->set("zip", $ph_response['response'][0]['clientId']);
        $profile->save();
        return $ph_response;*/
    }
    //обновить клиента
    public function updateClient($profile){
        if ($profile->dob) $birthday = date("Y-m-d", $profile->dob);
        $user_array["clients"][] = array(
            "clientId" => $profile->zip,
            "lastName" => "Нет данных",
            "firstName" => $profile->fullname,
            "patronymic" => "Нет данных",
            "email" => $profile->email,
            "tags" => [],
            "sex" => 1,
            "cardBarcode" => "",
            "cardNumber" => "",
            "birthday"=> $birthday , 
            "templateId"=>4374, //айди шаблона. пока непонятно что это, но очень важно
            "phone"=>$profile->mobilephone,
            "comment" => "Нет данных"
            );
        //return $user_array;
        //return json_encode($user_array,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        $vars["token"] = $this->token;
        $response = $this->modx->client->request('POST', 'https://cabinet.prime-hill.com/api/v2/updateClients?'.http_build_query($vars), ['json'=>$user_array]);
        $ph_response = json_decode($response->getBody(),true);
        return $ph_response;
    }
    //получить информацию о клиенте
    public function getClient($profile){
        $vars["token"] = $this->token;
        $vars["type"] = "clientId";
        $vars["id"] = $profile->zip;
        try {
            $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/clientInfo?'.http_build_query($vars));
            if ($response->getStatusCode() == 200){
                $ph_response = json_decode($response->getBody(),true);
                return $ph_response;
            }
        } catch (\Throwable $e) {
            $this->modx->log(modX::LOG_LEVEL_INFO, $e->getMessage());
            return false;
        }

    }
    //получить шаблоны скидок
    public function getTemplates(){
        $vars["token"] = $this->token;
        $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/getTemplates?'.http_build_query($vars));
        return $ph_response = json_decode($response->getBody(),true);
    }
    //получить список тегов
    public function getTags(){
        $vars["token"] = $this->token;
        $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/getTags?'.http_build_query($vars));
        return $ph_response = json_decode($response->getBody(),true);
    }
    //получить наименование тега
    public function getTag($id){
        $vars["token"] = $this->token;
        $vars["id"] = $id;
        $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/getTag?'.http_build_query($vars));
        return $ph_response = json_decode($response->getBody(),true);
    }
    //получить всех клиентов
    public function getAllClients(){
        $vars["token"] = $this->token;
        $vars["limit"] = 100000;
        $vars["offset"] = 0;
        $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/getAllClients?'.http_build_query($vars));
        return $ph_response = json_decode($response->getBody(),true);
    }
    //получить новыех клиентов
    public function getNewClients(){
        $vars["token"] = $this->token;
        $vars["limit"] = 100000;
        $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/getNewClients?'.http_build_query($vars));
        return $ph_response = json_decode($response->getBody(),true);
    }
    /************LOYALTY************/
    //получить инфу о юзере
    public function GuestInfo(modUserProfile $profile){
        $user_array["Phone"] = $profile->mobilephone;
        try {
            $response = $this->modx->client->request('POST', 'https://iiko-api.p-h.app/api/iiko/v3/GuetsInfo/'.$this->loyalty_token, ['json'=>$user_array]);
            if ($response->getStatusCode() == 200){
                $ph_response = json_decode($response->getBody(),true);
                return $ph_response;
            }
        } catch (\Throwable $e) {
            $this->modx->log(modX::LOG_LEVEL_INFO, $e->getMessage());
            return false;
        }
        
    }
    //получить карту лояльности
    public function getLoyaltyCard(modUserProfile $profile){
        $vars["token"] = $this->token;
        $vars["type"] = "phone";
        $vars["id"] = $profile->mobilephone;
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $ipod = strpos($user_agent,"iPod");
        $iphone = strpos($user_agent,"iPhone");
        $android = strpos($user_agent,"Android");
        $symb = strpos($user_agent,"Symbian");
        $winphone = strpos($user_agent,"WindowsPhone");
        $wp7 = strpos($user_agent,"WP7");
        $wp8 = strpos($user_agent,"WP8");
        $operam = strpos($user_agent,"Opera M");
        $palm = strpos($user_agent,"webOS");
        $berry = strpos($user_agent,"BlackBerry");
        $mobile = strpos($user_agent,"Mobile");
        $htc = strpos($user_agent,"HTC_");
        $fennec = strpos($user_agent,"Fennec/");
        $nokia = strpos($user_agent,"Nokia");
        try {
            $response = $this->modx->client->request('GET', 'https://cabinet.prime-hill.com/api/v2/clientInfo?'.http_build_query($vars));
            if ($response->getStatusCode() == 200){
                $ph_response = json_decode($response->getBody(),true);
                //генерируем карту по хэшу
                $this->modx->client->request('GET', 'https://form.p-h.app/plug/'.$ph_response["cardHash"]);
                if ($iphone) $json["pkass"] = 'https://pass.p-h.app/card/'.$ph_response["cardHash"].'.pkpass';
                if ($android) $json["pkass"] = 'https://form.p-h.app/plug/'.$ph_response["cardHash"];
                if (!$iphone && !$android) $json["pkass"] = 'https://form.p-h.app/plug/'.$ph_response["cardHash"];
                return $json;
            }
        } catch (\Throwable $e) {
            $new_client = $this->createClient($profile);
            return $this->getLoyaltyCard($profile);
        }
    }
    //получить подарок
    public function GetDiscount($profile, $cart){
        $request_array["OrganizationId"] = "0a5-1513-7f44-a67";
        $request_array["ParentOrderId"] = "123";
        $request_array["OrderId"] = "123";
        $request_array["ClientId"] = $profile->zip;
        $request_array["OrderNumber"] = 123;
        $request_array["OrderSum"] = $cart['total_cost'];
        $request_array["OrderSumWithDiscount"] = $cart['total_cost'];
        foreach ($cart["products"] as $product){
            $request_array["Order"][] = array(
                "ProductId" => $product["uuid"],
                "ProductName" => $product["title"],
                "Price" => $product["price"],
                "PriceWithDiscount" => $product["price"],
                "Amount" => $product["count"]
            );
        }
        try {
            $response = $this->modx->client->request('POST', 'https://iiko-api.p-h.app/api/iiko/v3/GetDiscount/'.$this->loyalty_token, ['json'=>$request_array]);
            if ($response->getStatusCode() == 200){
                $ph_response = json_decode($response->getBody(),true);
                return $ph_response;
            }
        } catch (\Throwable $e) {
            $this->modx->log(modX::LOG_LEVEL_INFO, $e->getMessage());
            return false;
        }
        
        
    }
    /************LOYALTY************/
}