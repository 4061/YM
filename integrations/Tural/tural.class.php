<?php
/* пример использования
include MODX_BASE_PATH."integrations/integrations.class.php";
$integrations = new Integrations($modx);
$tural = $integrations->geTuralActions(); 
return $tural->updateCategories();*/
//тест коммент
//тест коммент 2
class Tural extends Integrations{
    //получить значение
    public function getQuery($query){
        $query = json_decode(json_encode($query), TRUE)[0];
        return $query;
    }
    //получить номенклатуру
    public function getNom(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://tuna-shop.ru/yml/yml.xml",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: f1f99acd-d00d-7029-4187-e5084104ae10"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        return $response;
    }
    //удалить несуществующие категории
    public function removeOldCategories(){
        $response = $this->getNom();
        $xml = new SimpleXMLElement($response, true);
        $default = array(
            'class' => 'msProduct',
            'parents' => 1561,
            'where' => ['class_key' => 'msCategory', 'published' => 1, 'deleted' => 0],
            'sortdir' => 'ASC',
            'return' => 'data',
            'limit' => 1000
        );
        $this->pdoFetch->setConfig($default);
        $our_categories = $this->pdoFetch->run();
        foreach ($our_categories as $category){
            $tumbler = false;
            foreach ($xml->shop->categories->category as $xml_category){
                if ($category["alias"] == $this->getQuery($xml_category["id"])){
                    $tumbler = true;
                }
            }
            if (!$tumbler){
                $category_object = $this->modx->getObject("msCategory", array("alias"=>$category["alias"]));
                $category_object->remove();
            }
        }
    }
    //обновить категории
    public function updateCategories(){
        $response = $this->getNom();
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->shop->categories->category as $category){
            if (!$category_object = $this->modx->getObject('modResource', array('alias' => $this->getQuery($category["id"])))){
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'msCategory',
                  'pagetitle' => $this->getQuery($category),
                  'parent' => $this->modx->getOption("content_parent"),
                  'template' => 3,
                  'show_in_tree' => 1,
                  'alias' => $this->getQuery($category["id"])
                ));
                $id = $response->response['object']['id'];
                $errors = $response->getAllErrors();
                if ($errors) print_r($errors);
            } else {
                $category_object->set("pagetitle", $this->getQuery($category));
                $category_object->save();
            }
        }
        $this->removeOldCategories();
    }
    //удалить старые товары
    public function removeOldProducts(){
        $response = $this->getNom();
        $xml = new SimpleXMLElement($response, true);
        $default = array(
            'class' => 'msProduct',
            'parents' => 1561,
            'where' => ['class_key' => 'msProduct', 'published' => 1, 'deleted' => 0],
            'leftJoin' => ['Data' => ['class' => 'msProductData'],
            ],
            'select' => [
                'msProduct' => '`msProduct`.`id`,`msProduct`.`pagetitle`, parent, alias, `msProduct`.`description`',
                'Data' => '`Data`.`restourants`,`Data`.`price`,`Data`.`price2`,`Data`.`article`, `Data`.`image` as img_big, `Data`.`thumb` as img_small, `Data`.`new`, `Data`.`popular`, `Data`.`favorite`',
            ],
            'sortby' => 'msProduct.id',
            'sortdir' => 'ASC',
            'groupby' => 'msProduct.id',
            'return' => 'data',
            'limit' => 1000
        );
        $this->pdoFetch->setConfig($default);
        $our_products = $this->pdoFetch->run();
        foreach ($our_products as $product){
            $tumbler = false;
            foreach ($xml->shop->offers->offer as $xml_product){
                if ($product["alias"] == $this->getQuery($xml_product["id"])){
                    $tumbler = true;
                }
            }
            if (!$tumbler){
                $product_object = $this->modx->getObject("msProduct", array("alias"=>$product["alias"]));
                $product_object->remove();
            }
        }
    }
    //обновить продукты
    public function updateProducts(){
        $response = $this->getNom();
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->shop->offers->offer as $product){
            $parent_object = $this->modx->getObject('modResource', array("alias"=>$this->getQuery($product->categoryId)));
            if (!$product_object = $this->modx->getObject('msProduct', array("alias"=>$this->getQuery($product['id'])))){
                    $response = $this->modx->runProcessor('resource/create', array(
                        'class_key' => 'msProduct',
                        'pagetitle' => $this->getQuery($product->name),
                        'description' => $this->getQuery($product->description),
                        'parent' => $parent_object->id,
                        'template' => 4,
                        'show_in_tree' => 1,
                        'order_types' => "1,2,3,4", //обязательно пропсиываем типы подачи
                        'alias' => $this->getQuery($product['id']),
                        
                        //Данные
                        'price' => $this->getQuery($product->parameters->parameter->price),
                        'article' => (int)$product['code'],
                        'old_price' => 0,
                        'favorite' => 0,
                        'popular' => 0,
                        'additive' => 0,
                        'source' => 2,
                    ));
                    $id = $response->response['object']['id'];
                    $error = $response->getAllErrors();
                    if ($error){
                        echo ($error);
                    } else {
                        echo $id."</br>";
                    } 
            } else {
                $product_object->set("pagetitle", $this->getQuery($product->name));
                if ($this->getQuery($product->description)) $product_object->set("description", $this->getQuery($product->description));
                $product_object->set("parent", $parent_object->id);
                $product_object->set("source", 2);
                $product_object->save();
            }
            
        }
        $this->removeOldProducts();
    }
    //удалить фото продукта
    public function removeProductPhoto(msProduct $product_object){
        $images = $this->modx->getCollection('msProductFile', array('type' => 'image', 'product_id' => $product_object->get("id")));
        foreach ($images as $image){
            $ids[] = $image->id;
        }
        $response = $this->modx->runProcessor('gallery/multiple',
          array(
            'method' => 'remove',
            'ids' => json_encode($ids),
          ),
          array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/')
        );
        $ids = [];
    }
    //проверить фото
    private function checkPhoto($picture, msProduct $product_object){
        $basename_array = explode(".", basename($picture));
        $basename = $basename_array[0];
        $images = $this->modx->getCollection('msProductFile', array('type' => 'image', 'product_id' => $product_object->get("id")));
        foreach ($images as $image){
            $ids[] = $image->name;
        }
        //если в наименованиях фотографий есть это фото, то возвращаем true
        if (in_array($basename, $ids)){
            return true;
        } else {
            return false;
        }
    }
    //обновить фото продуктов
    public function updatePhotos(){
        $response = $this->getNom();
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->shop->offers->offer as $product){
            //$product_object = $this->modx->getObject('msProduct', array("alias"=>$this->getQuery($product['id'])));
            //$this->removeProductPhoto($product_object);
            if ($product_object = $this->modx->getObject('msProduct', array("alias"=>$this->getQuery($product['id'])))){
                if (!$this->checkPhoto($this->getQuery($product->picture),$product_object)){
                    $this->removeProductPhoto($product_object);
                }
                $data = [
                    'id' => $product_object->id,
                    'file' => $this->getQuery($product->picture),
                    //'source' => 2
                ];
                
                $response = $this->modx->runProcessor('gallery/upload', $data, [
                    'processors_path' => MODX_CORE_PATH . 'components/minishop2/processors/mgr/',
                ]);
                $error = $response->getAllErrors();
                if ($error){
                    print_r ($error);
                } else {
                    echo $id."</br>";
                }
                
            }
        }
    }
    //отправить заказ
    public function sendOrder(msOrder $order){
        $contacts = $this->modx->getObject('msOrderAddress', array('id'=> $order->address));
        $products = $order->getMany('Products');
        $profile = $this->modx->getObject("modUserProfile", array("internalKey"=>$order->user_id));
        $i = 0;
        foreach ($products as $key=> $product){
            $product_object = $this->modx->getObject("modResource", $product->product_id);
            $product_parent_object = $this->modx->getObject("modResource", $product_object->parent);
            $options = json_decode($product->options, true);
            $options = $options["childs"]["size"];
            $comment = "Заказ из приложения #".$order->id."
            ".$order->comment;
            if ($size_product_object = $this->modx->getObject("modResource", $options[0]["id"])){
$cart_items_string.= "body[cart][items][$i][description]=$size_product_object->pagetitle
";
            }
            $cart_items_string .= 
"body[cart][items][$i][category]=$product_parent_object->pagetitle
body[cart][items][$i][vendor_code]=$product_object->alias
body[cart][items][$i][name]=$product_object->pagetitle
body[cart][items][$i][count]=$product->count
body[cart][items][$i][cost]=$product->price
";
            $i++; 
        }
        switch ($order->payment){
            case 1:
                $payment = "online";
                break;
            case 3:
                $payment = "cash";
                break;
            case 4:
                $payment = "card_upon_receipt";
                break;
        }
        switch ($order->delivery){
            case 1:
                $order_type = "pickup";
                break;
            case 2:
                $order_type = "courier";
                break;
        }
        $order_shit_string = 
"body[id]=$order->id
body[order_number]=$order->id
body[total]=$order->cost
body[sender_name]=$profile->fullname
body[sender_phone]=$profile->mobilephone
body[sender_email]=$profile->email
body[recipient_name]= $profile->fullname
body[recipient_phone]=$profile->mobilephone
body[recipient_email]=$profile->email
body[persons]=$contacts->person
body[date]=$order->createdon
body[street]=$contacts->street
body[entrance]=$contacts->entrance
body[apartment]=$contacts->flat_num
body[home]=$contacts->house_num
body[floor]=$contacts->floor
body[city_name]=$contacts->city
body[comment]=>$comment
body[payment_method]=$payment
body[delivery_method]=$order_type
body[zone]=ru
action=new_order
        ";
        $final_string = $cart_items_string.$order_shit_string;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://s1.serverss.ru/unf02/hs/FoodSoul/Order",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $final_string,
          CURLOPT_HTTPHEADER => array(
            "authorization: Basic U29mdEpldDpUdW5hQXBwQClAIQ==",
            "cache-control: no-cache",
            "postman-token: 940f4ba4-d71b-901b-09b1-dd04446a1694"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        $response = json_decode($response, true);
        if ($response["id"] != false){
            $contacts->set("id_integration_order", $response["id"]);
            $contacts->save();
        }
        
    }
    //обновить статус
    public function updateOrderStatus(msOrder $order){
        $contacts = $this->modx->getObject('msOrderAddress', array('id'=> $order->address));
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://s1.serverss.ru/unf02/hs/FoodSoul/order_status/".$contacts->id_integration_order,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "authorization: Basic U29mdGpldDpUdW5hQXBwQClAIQ==",
            "cache-control: no-cache",
            "postman-token: 5efa699f-d232-db4d-7905-216eda87d2f7"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          $response = json_decode($response, true);
        }
        //return $response["status"];
        $status = $this->modx->getObject("msOrderStatus", array("name"=>$response["status"]));
        $miniShop2 = $this->modx->getService('miniShop2');
        $miniShop2->initialize($modx->context->key, $scriptProperties);   
        $miniShop2->changeOrderStatus($order->id, $status->id);
        return $status->id;
    }
    //обновить типы цен
    public function updatePriceTypes(){
        $price_types = array(
            array("id"=>1, "currency"=> "RUB", "name" => "Курьер"), array("id"=>2, "currency"=> "RUB", "name" => "Самовывоз")    
        );
        foreach ($price_types as $type){
            if (!$price_type_obj = $this->modx->getObject("SoftjetsyncPriceTypes", $type["id"])){
                $price_type_obj = $this->modx->newObject("SoftjetsyncPriceTypes", ["name"=> $type["name"], "currency"=> $type["currency"]]);
                $price_type_obj->save();
            }
        }
    }

    //обновить цены
    public function updatePrices(){
        $discount_percent = $this->modx->getOption("app_discount_percent");
        $discount_percent = 100 - $discount_percent;
        $discount_percent = $discount_percent/100;
        $response = $this->getNom();
        $xml = new SimpleXMLElement($response, true);
        $organizations = $this->modx->getCollection("modResource", array("template"=>18));
        foreach ($organizations as $organization){
            foreach ($xml->shop->offers->offer as $product){
                if ($product_object = $this->modx->getObject('msProduct', array("alias"=>$this->getQuery($product['id'])))){
                    if (!$new_price = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$product_object->id, "type"=>$organization->getTVValue("tvTypeDelivery")))){
                        $new_price = $this->modx->newObject("SoftjetsyncPrices");
                        $new_price->set("product_id", $product_object->id);
                        $new_price->set("product_uuid", $product_object->alias);
                        $new_price->set("type", $organization->getTVValue("tvTypeDelivery")); //курьером
                        $new_price->set("price", $this->getQuery($product->parameters->parameter->price));
                        $new_price->save();   
                    } else {
                        $new_price->set("product_id", $product_object->id);
                        $new_price->set("product_uuid", $product_object->alias);
                        $new_price->set("type", $organization->getTVValue("tvTypeDelivery")); //курьером
                        $new_price->set("price", $this->getQuery($product->parameters->parameter->price));
                        $new_price->save();   
                    }
                    if (!$new_price = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$product_object->id, "type"=>$organization->getTVValue("tvType")))){
                        $new_price = $this->modx->newObject("SoftjetsyncPrices");
                        $new_price->set("product_id", $product_object->id);
                        $new_price->set("product_uuid", $product_object->alias);
                        $new_price->set("type", $organization->getTVValue("tvType")); //самовывоз
                        $discount_price = $this->getQuery($product->parameters->parameter->price)*$discount_percent;
                        if ($discount_price<1) $discount_price = 1;
                        $new_price->set("price", $discount_price);
                        $new_price->save();   
                    } else {
                        $new_price->set("product_id", $product_object->id);
                        $new_price->set("product_uuid", $product_object->alias);
                        $new_price->set("type", $organization->getTVValue("tvType")); //самовывоз
                        $discount_price = $this->getQuery($product->parameters->parameter->price)*$discount_percent;
                        if ($discount_price<1) $discount_price = 1;
                        $new_price->set("price", $discount_price);
                        $new_price->save();   
                    }
                }
            }       
        }
    }
    //получить бонусы пользователя
    public function getUserBonuses($profile){
        $vars["PhoneNumber"] = substr($profile->mobilephone, 1);
        $response = $this->modx->client->request('GET', 'https://s1.serverss.ru/unf02/hs/ClientInfoApi/GetBonuses?'.http_build_query($vars), ['auth' => ['SoftJet', 'TunaApp@)@!']]);
        $response = json_decode($response->getBody(),true);
        return $response;
    }
}