<?php
class IikoCard extends Iiko{
    //создать или обновить пользователя в iikocard
    public function createOrUpdateUser(modUserProfile $profile){
        $vars["access_token"] = $this->token;
        $vars["organization"] = $this->org->id;
        $request["customer"]["phone"] = $profile->mobilephone;
        $request["customer"]["email"] = $profile->email;
        if ($profile->fullname) $request["customer"]["name"] = $profile->fullname;
        if ($profile->dob) $request["customer"]["birthday"] = date("Y-m-d", $profile->dob);
        $response = $this->modx->client->request('POST', "https://iiko.biz:9900/api/0/customers/create_or_update?".http_build_query($vars), ['json'=>$request]);
        $guid = $response->getBody();
        $guid = str_replace('"', "", $guid);
        $profile->set("bouns_integration_guid", $guid);
        $profile->save();
        return $this->getUserByGuid($profile);
    }
    //получить юзера по айди
    public function getUserByGuid(modUserProfile $profile){
        $vars["access_token"] = $this->token;
        $vars["organization"] = $this->org->id;
        $vars["id"] = $profile->bouns_integration_guid;
        $response = $this->modx->client->request('GET', "https://iiko.biz:9900/api/0/customers/get_customer_by_id?".http_build_query($vars));
        return $response = json_decode($response->getBody(),1);
    }
    //начислить бонусы юзеру. принимает профиль и начисляемую сумму
    public function addBonus(modUserProfile $profile, $sum){
        $user_data = $this->getUserByGuid($profile); //получаем данные о пользователе через айко кард
        $vars["access_token"] = $this->token;
        $request["sum"] = $sum; 
        $request["walletId"] = $user_data["walletBalances"][0]["wallet"]["id"]; //подставляем айди его кошелька
        $request["customerId"] = $profile->bouns_integration_guid;
        $request["organizationId"] = $this->org->id;
        $response = $this->modx->client->request('POST', "https://iiko.biz:9900/api/0/customers/refill_balance?".http_build_query($vars), ['json'=>$request]);
        $user_data = $this->getUserByGuid($profile);
        return $user_data; //вовзращаем обновленный массив данных о пользователе
    }
    //
    public function writeoffBonuses($sum, $phone){
        $array = array(
            "sum" => $sum,
            "paymentType" => array(
                "id" => "cd19f6e4-e885-40a8-bd06-60cdf6c6b23c",
                "code" => "CARD5",
                "name" => "iikoCard",
                "comment" => "",
                "combinable" => true,
                "externalRevision" => 21345169,
                "applicableMarketingCampaignIds" => null,
                "deleted" => false
            ),
            "additionalData" => "{'searchScope': 'PHONE', 'credential':'$phone'}",
            "isProcessedExternally" => false,
            "isPreliminary" => true,
        	"isExternal" => true
        );
        return $array;
    }
}