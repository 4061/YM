<?php
//updateGroups() - обновить категории
//updateRestourants() - обновить точки продаж
//updateProducts() - обновить продкуты
//sendOrder - отправить заказ
//updateCities - обновить городаа
//getOrderStatus - получить всю информацию о заказе
//updatePrices - обновить цены 
class Iiko extends Integrations{
    public function __construct($modx){
        $iiko_id = "Api_Milano";
        $iiko_pass = "qkMZxc0w";
        $this->modx = $modx;
        $this->token = str_replace('"',"",file_get_contents('https://iiko.biz:9900/api/0/auth/access_token?user_id='.$iiko_id.'&user_secret='.$iiko_pass));
        $this->org = json_decode( file_get_contents('https://iiko.biz:9900/api/0/organization/list?access_token='.$this->token.'&request_timeout=00%3A01%3A00'))[0];
    }
    public function getCombos(){
        return "https://iiko.biz:9900/api/0/orders/get_combos_info?access_token=".$this->token."&organization=".$this->org->id;
    }
    //спарсить рестораны
    public function updateRestourants(){
        $rests = json_decode(file_get_contents('https://iiko.biz:9900/api/0/deliverySettings/getDeliveryTerminals?access_token=' . $this->token . '&organization='.$this->org->id),true);
        if (!$rests) return $this->modx->log(modX::LOG_LEVEL_ERROR, "айка не ответила");
        foreach ($rests["deliveryTerminals"] as $key=> $point){
            if ($point["name"]){
                $name = $point["name"];
            } else {
                $name = $point["deliveryRestaurantName"];
            }
            if (!$res = $this->modx->getObject('modResource', array('pagetitle' => $name))){
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'modResource',
                  'pagetitle' => $name,
                  'longtitle' => $point["address"],
                  'parent' => 35458,
                  'isfolder' => 0,
                  'template' => 18,
                  'show_in_tree' => 1,
                  'alias' => $point["deliveryTerminalId"]
                ));
                $id = $response->response['object']['id'];
                $errors = $response->getAllErrors();
                if ($errors) print_r($errors);
                $res = $this->modx->getObject('modResource', $id);
                $res->set("alias", $point["deliveryTerminalId"]);
                $res->setTVValue("deliveryTerminalId", $point["deliveryTerminalId"]);
                $res->setTVValue("online_payment", 1);
                $res->setTVValue("cash_courier_payment", 1);
                $res->setTVValue("cashless_courier_payment", 1);
                $res->setTVValue("courier", 1);
                $res->setTVValue("selfcourier", 1);
                $res->setTVValue("restourant", 1);
                $res->setTVValue("iiko_organization_key", $this->org->id);
                $res->save();
                //создаем прайс-лист для этой организации
                $points_prices = ["tvType", "tvTypeRestourant", "tvTypeDelivery"]; //записываем все прайс-листы организации
                foreach ($points_prices as $point_price){
                    $price_list = $this->modx->newObject("SoftjetsyncPriceTypes");
                    $price_list->set("uuid", $res->alias);
                    $price_list->set("name", $res->pagetitle);
                    $price_list->set("currency", "RUB");
                    $price_list->save();
                    $res->setTVValue($point_price, $price_list->id);
                }
            } else {
                $res = $this->modx->getObject('modResource', array('pagetitle' => $name));
                $res->set("alias", $point["deliveryTerminalId"]);
                $res->setTVValue("deliveryTerminalId", $point["deliveryTerminalId"]);
                $res->setTVValue("online_payment", 1);
                $res->setTVValue("cash_courier_payment", 1);
                $res->setTVValue("cashless_courier_payment", 1);
                $res->setTVValue("courier", 1);
                $res->setTVValue("selfcourier", 1);
                $res->setTVValue("restourant", 1);
                $res->setTVValue("iiko_organization_key", $this->org->id);
                $res->save();
            }
        }
        print_r($rests);
    }
    //спарсить города
    public function updateCities(){
        $cities = json_decode(file_get_contents('https://iiko.biz:9900/api/0/cities/citiesList?access_token=' . $this->token . '&organization='.$this->org->id),true);
        //return $cities;
        foreach ($cities as $point){
            if (!$res = $this->modx->getObject('modResource', array('pagetitle' => $point["name"]))){
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'modResource',
                  'pagetitle' => $point["name"],
                  'parent' => 1802,
                  'isfolder' => 1,
                  'template' => 0,
                  'show_in_tree' => 1,
                ));
                $errors = $response->getAllErrors();
                if ($errors) return $errors;
            } 
        }
    }
    public function getToken(){
        return $this->token;
    }
    public function getDiscounts(){
        return "https://iiko.biz:9900/api/0/deliverySettings/deliveryDiscounts?access_token=".$this->token."&organization=".$this->org->id;
    }
    //спарсить продукты
    public function updateProducts(){
        return 'https://iiko.biz:9900/api/0/nomenclature/'.$this->org->id.'?access_token='.$this->token.'&revision=0';
        $nom = json_decode( file_get_contents('https://iiko.biz:9900/api/0/nomenclature/'.$this->org->id.'?access_token='.$this->token.'&revision=0'),true);
        $products = $nom['products'];
        if (!$products) return $this->modx->log(modX::LOG_LEVEL_ERROR, "айка не ответила");
        foreach ($products as $product){
            if (!$res = $this->modx->getObject("msProduct", array("alias"=>$product["id"]))){
                $parent = $this->modx->getObject("modResource", array("alias"=>$product["parentGroup"]));
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'msProduct',
                  'pagetitle' => $product['name'],
                  'description' => $product['description'],
                  'parent' => $parent->id,
                  'template' => 4,
                  'show_in_tree' => 1,
                  'order_types' => "1,2,3", //обязательно пропсиываем типы подачи
                  'alias' => $product["id"],
                
                  //Данные
                  'price' => 0,
                  'article' => (int)$product['code'],
                  'old_price' => 0,
                  'favorite' => 0,
                  'popular' => 0,
                  'additive' => 0,
                ));
                $id = $response->response['object']['id'];
                $error = $response->getAllErrors();
                if (!$error) print_r($error);
                if ($id){
                    $this->updateProduct($id, $product);
                }
            } else {
                $this->updateProduct($res->id, $product);
            }
        }
    }
    //спарсить категории
    public function updateGroups(){
        $nom = json_decode( file_get_contents('https://iiko.biz:9900/api/0/nomenclature/'.$this->org->id.'?access_token='.$this->token.'&revision=0'),true);
        $groups = $nom['groups'];
        foreach ($groups as $group){
            if (!$res = $this->modx->getObject('modResource', array('alias' => $group["id"]))){
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'msCategory',
                  'pagetitle' => $group['name'],
                  'parent' => 1561,
                  'alias' => $group['id'],
                  'template' => 3,
                  'show_in_tree' => 1,
                ));
                $id = $response->response['object']['id'];
                $resource = $this->modx->getObject('modResource', array('id' => $id));
                $resource->setTVValue('categoryId', $group['id']);
            } else {
                $res->set("pagetitle",$group['name']);
                $res->set("alias",$group['id']);
                $res->setTVValue('categoryId', $group['id']);
                $res->save();
            }
        }
    }
    //отправить заказ
    public function sendOrder($order){
        if ($order->get("status")){
            $contacts = $this->modx->getObject('msOrderAddress', array('id'=> $order->address));
            $products = $order->getMany('Products');
            $phone = $contacts->phone;
            $phone = str_replace("+", "", $phone);
            $org = $contacts->index;
            $res_obj = $this->modx->getObject('modResource', $org);
            $iiko_send_statuses[2] = $res_obj->getTVValue('courier');
            $iiko_send_statuses[1] = $res_obj->getTVValue('selfcourier');
            $iiko_send_statuses[3] = $res_obj->getTVValue('restourant');
            $json['organization'] = $res_obj->getTVValue('iiko_organization_key');
            //$profile = $this->modx->getObject("modUserProfile");
            $json['customer'] = array(
                "name" => $contacts->receiver,
                "phone" => $phone,
                "id" => "5f850000-90a3-0025-15e5-08d90578ee29"
            );
            $delivery = $order->delivery;
            if ($delivery == 2){
                $delivery = false;
            } else {
                $delivery = true;
            }
            $dt = new Datetime($order->createdon);
            $interval = new DateInterval('PT1H');
            $dt->add($interval);
            $order_time = $dt->format('Y-m-d H:i:m');
            $json['order'] = array(
                "date" => $order_time,
                "phone" =>$phone,
                "isSelfService" =>$delivery
            );
            foreach ($products as $product){
                $product_id = $product->product_id;
                $sql = "
                SELECT 
                    modx_ms2_product_options.value,
                    modx_ms2_products.article
                FROM 
                    modx_ms2_product_options,modx_ms2_products
                WHERE 
                    modx_ms2_product_options.product_id = '$product_id' AND 
                    modx_ms2_product_options.key = 'guid' AND
                    modx_ms2_product_options.product_id = modx_ms2_products.id
                ";
                $info_product = $this->modx->query($sql);
                $info_product = $info_product->fetchAll(PDO::FETCH_ASSOC);
                $guid = $info_product[0]['value'];
                $code = $info_product[0]['article'];
                $item_options = json_decode($product->options, true);
                $adds = $item_options["childs"]["additives"];
                foreach ($adds as $key=> $add){
                    $adds_array[] = array("id"=>$add["id"]);
                    $this_id = $add["id"];
                    $this_add_object = $this->modx->getObject("modResource", $this_id);
                    $this_add_object_parent = $this->modx->getObject("modResource", $this_add_object->parent);
                    $adds[$key]['amount'] = $add['count'];
                    $adds[$key]['name'] = $add['title'];
                    unset($adds[$key]['count']);
                    unset($adds[$key]['title']);
                    $adds[$key]['groupName'] = $this_add_object_parent->pagetitle;
                    $adds[$key]['groupId'] = $this_add_object_parent->alias;
                    $sql = "
                    SELECT * 
                    FROM 
                        `modx_ms2_product_options` 
                    WHERE 
                        modx_ms2_product_options.product_id = '$this_id' AND 
                        (modx_ms2_product_options.key = 'guid' OR modx_ms2_product_options.key = 'parentGroup')
                    ";
                    $guids = $this->modx->query($sql);
                    $guids = $guids->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($guids as $gui){
                        if ($gui["key"] == "guid"){
                            $adds[$key]['id'] = $gui["value"];
                        }
                        if ($gui["key"] == "parentGroup"){
                            ///$adds[$key]['groupId'] = $gui["value"];
                        }
                    }
                }
                $types = $item_options["childs"]["type"];
                foreach ($types as $key=> $add){
                    $adds_array[] = array("id"=>$add["id"]);
                    $this_id = $add["id"];
                    $types[$key]['amount'] = $add['count'];
                    $types[$key]['name'] = $add['title'];
                    unset($types[$key]['count']);
                    unset($types[$key]['title']);
                    $types[$key]['groupName'] = "";
                    $sql = "
                    SELECT * 
                    FROM 
                        `modx_ms2_product_options` 
                    WHERE 
                        modx_ms2_product_options.product_id = '$this_id' AND 
                        (modx_ms2_product_options.key = 'guid' OR modx_ms2_product_options.key = 'parentGroup')
                    ";
                    $guids = $this->modx->query($sql);
                    $guids = $guids->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($guids as $gui){
                        if ($gui["key"] == "guid"){
                            $types[$key]['id'] = $gui["value"];
                        }
                        if ($gui["key"] == "parentGroup"){
                            $types[$key]['groupId'] = $gui["value"];
                        }
                    }
                }
                $option = $this->modx->getObject("msProductOption", array("product_id"=>$product_id, "key"=>"card_type"));
                if ($option->value == "halves"){
                    foreach ($adds as $halve){
                        $json['order']['items'][] = $halve;
                    }
                } else {
                    $json['order']['items'][$product->product_id] = array(
                        "id" => $guid,
                        "code" => $code,
                        "name" => $product->name,
                        "amount" => $product->count,
                        //"sum" => 100,
                        "modifiers" => array_merge($adds, $types)
                    );
                }
            }
            foreach ($adds_array as $add){
                unset($json['order']['items'][$add['id']]);
            }
            $json['order']['items'] = array_values($json['order']['items']);
            foreach ($json['order']['items'] as $key => $item){
                $res = $this->modx->getObject("msProduct", array("alias"=>$item["id"]));
                $res_modifiers = $this->modx->getCollection("msProductLink", array("master"=>$res->id));
                foreach ($res_modifiers as $res_modifier){
                    $res_modifier_obj = $this->modx->getObject("msProduct", $res_modifier->slave);
                    $res_modifier_obj_parent = $this->modx->getObject("modResource", $res_modifier_obj->parent);
                    $order_types = explode(",",$res_modifier_obj->get("order_types"));
                    if (in_array($order->delivery, $order_types)){
                        if ($res_modifier_obj->getTVValue("technicalModifier")){
                            $arr = array(
                                "id" => $res_modifier_obj->alias,
                                "amount" => $res_modifier->amount,
                                "name" => $res_modifier_obj->pagetitle,
                                "groupName" => $res_modifier_obj_parent->pagetitle,
                                "groupId" => $res_modifier_obj_parent->alias,
                            );
                            if ($arr["groupName"] == "Тест"){
                                unset($arr["groupName"]);
                                unset($arr["groupId"]);
                            }
                        }
                    }
                    
                    if ($arr) $json['order']['items'][$key]["modifiers"][] = $arr;
                    $arr = null;
                    //print_r($arr);
                }
            }
            $json['order']['address'] = array(
                "city" =>$contacts->city,
                "street" =>$contacts->street,
                "home" => $contacts->house_num,
                "housing" => "",
                "floor" => $contacts->floor,
                "entrance" => $contacts->entrance,
                "apartment" => $contacts->flat_num,
                "comment" => $order->comment
            );
            if ($iiko_send_statuses[$order->delivery] == 3){
                $json['order']["comment"] = $json['order']["comment"]." Относится к ресторану: ".$res_obj->get("pagetitle");
            }
            if ($contacts->building){
                $json['order']["comment"] = "Номер столика: ".$contacts->building;
            }
            if ($contacts->room){
                $surrender = "Сдача с купюры: ".$contacts->room.", ";
            }
            $json['order']['comment'] = $json['order']['comment']." ".$surrender.$order->comment." Заказ в приложении №".$order->id;
            $max_cost = 0;
            foreach ($products as $product){
                $prices_obj = $this->modx->getObject("SoftjetsyncPrices", array("type"=>$res_obj->getTVValue("tvTypeDelivery"), "product_id"=>$product->product_id));
                $max_cost = $max_cost + ($prices_obj->price*$product->count);
            }
            $json["order"]["discountCardTypeId"] = "407ff1ba-c8f0-4cc4-ade9-91bcfcf646d1"; //идентификатор скидки flexible sum
            $json["order"]["discountOrIncreaseSum"] = $max_cost - $order->cost; //сумма скидки
            /*if ($order->get('payment') == 1){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '657b1ba4-0590-4193-9fbc-e9a144496a89',
            				'code' => "APSP",
            				'name' => "Аpple pay/samsung pay",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => true
            		]
            	);    
            }
            if ($order->get('payment') == 3){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '09322f46-578a-d210-add7-eec222a08871',
            				'code' => "CASH",
            				'name' => "Наличные",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => false
            		]
            	);    
            }
            if ($order->get('payment') == 4){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '1dda6a70-bdd2-4aa6-aef1-73b652796f46',
            				'code' => "CARD",
            				'name' => "Банковские карты",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => false
            		]
            	);    
            }
            if ($order->get('payment') == 6){
                $pay = array(
            		[
            			'sum' => $order->get("cost"),
            			'paymentType' => [
            				'id' => '03a3f513-ef0f-4378-935b-44e6c858f3d9',
            				'code' => "OLMP",
            				'name' => "Онлайн оплата МП",						
            				"deleted" => false				
            			],
            			"isProcessedExternally" => true
            		]
            	);    
            }
        	$json['order']['paymentItems'] = $pay;*/
        	/*$json['order']['paymentItems'][0] = array(
        	    "sum" => 10,
        	    "paymentType" => array(
        	        "id" => "cd19f6e4-e885-40a8-bd06-60cdf6c6b23c",
        	        "code" => "CARD5",
        	        "name" => "iikoCard",
        	        "comment" => "",
        	        "combinable" => true,
        	        "externalRevision" => 21345169,
        	        "applicableMarketingCampaignIds" => null,
        	        "deleted" => false
        	     ),
        	     //"additionalData"=> array(
        	   //      "searchScope" => "PHONE",
        	    //     "credential" => $phone,
        	     // ),
        	     "additionalData" => "{'searchScope': 'PHONE', 'credential':'$phone'}",
        	         "isProcessedExternally" => false,
        	         "isPreliminary" => true,
        	         "isExternal" => true
        	);*/
        	if ($this->modx->getOption("iiko_card")) $json['order']['paymentItems'] = $this->writeoffBonuses(10, $phone);
        	return $json;
        	return 'https://iiko.biz:9900/api/0/orders/add?access_token='.$this->token.'&requestTimeout=50000';
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_PORT => "9900",
              CURLOPT_URL => 'https://iiko.biz:9900/api/0/orders/add?access_token='.$this->token.'&requestTimeout=50000',
             //CURLOPT_URL => 'https://iiko.biz:9900/api/0/orders/add?access_token=rere&requestTimeout=10000',
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 50,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => json_encode($json),
              CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json"
              ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            //$modx->log(modX::LOG_LEVEL_ERROR, 'AIKO RESPONSE: '.json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
            $response = json_decode($response,1);
        	return $response;
            }
    }
    //обновить продукты
    public function updateProduct($id, $product){
        //обновляем основные поля
        $res = $this->modx->getObject("msProduct", $id);
        if ($product["type"] == "modifier"){
            $parent = $this->modx->getObject("modResource", array("alias"=>$product["groupId"]));
        } else {
            $parent = $this->modx->getObject("modResource", array("alias"=>$product["parentGroup"]));
        }
        
        $res->set("pagetitle", $product["name"]);
        $res->set("description", $product["description"]);
        if ($parent){
            $res->set("parent", $parent->id);    
        } else {
            $res->set("parent", 4285);
        }
        
        //обновляем доступность в точках
        foreach ($product["prohibitedToSaleOn"] as $prohibitedPoint){
            $point_obj = $this->modx->getObject("modResource", array("alias"=>$prohibitedPoint["terminalId"]));
            $prohibitedPoints[] = $point_obj->id;
        }
        $prohibitedPoints = implode(",",$prohibitedPoints);
        $res->set("restourants", $prohibitedPoints);
        $prohibitedPoints = null;
        //сохраняем
        $res->save();
        //обновляем пищевые значения
        $energy_oils_val = $product['fatFullAmount'];
        $energy_protein_val = $product['fiberFullAmount'];
        $energy_carbohydrates_val = $product['carbohydrateFullAmount'];
        $energy_value_val = $product['energyFullAmount']; //ккалы
        $energy_allegrens_val = NULL;
        $energy_size_val = (double) ($product['weight']);
        $options = array(
            "energy_oils" => $energy_oils_val, 
            "energy_protein" => $energy_protein_val, 
            "energy_carbohydrates" => $energy_carbohydrates_val, 
            "energy_value" => $energy_value_val, 
            "energy_allergens" => $energy_allegrens_val, 
            "energy_size" => $energy_size_val, 
            "guid" => $product['id'],
            "parentGroup" => $product['parentGroup']
        );
        foreach ($options as $key=> $option){
            if (!$option_obj=$this->modx->getObject("msProductOption", array("product_id"=>$id, "key"=>$key))){
                $option_obj = $this->modx->newObject("msProductOption");
                $option_obj->set("product_id",$id);
                $option_obj->set("key", $key);
                $option_obj->set("value", $option);
                $option_obj->save();   
            } else {
                $option_obj->set("product_id",$id);
                $option_obj->set("key", $key);
                $option_obj->set("value", $option);
                $option_obj->save(); 
            }
        }
        $this->updateModifiers($res, $product);
    }
    //обновить прайсы
    public function updatePrices(){
        $nom = json_decode( file_get_contents('https://iiko.biz:9900/api/0/nomenclature/'.$this->org->id.'?access_token='.$this->token.'&revision=0'),true);
        $products = $nom['products'];
        if (!$products) return $this->modx->log(modX::LOG_LEVEL_ERROR, "айка не ответила");
        $resources = $this->modx->getCollection("modResource", array("parent"=>35458));
        foreach ($resources as $res){ //Проходимся по всем оргнизациям циклом foreach 
            foreach ($products as $product){//Внутри запускаем еще один цикл, который проходится по всем продуктам номенклатуры
                if ($product["differentPricesOn"]){//Если у продукта есть массив differentPricesOn, то проходимся по нему и если в нем есть id итерируемой организации, то ставим цену из объекта
                    foreach ($product["differentPricesOn"] as $product_price){
                        if ($product_price["terminalId"] == $res->alias){
                            $price = $product_price["price"];
                        }
                    }
                    if (!$price) $price = $product["price"];
                } else {
                    $price = $product["price"];
                }
                $modx_product = $this->modx->getObject("msProduct", array("alias"=>$product["id"]));
                if ($modx_product){
                    //обычная цена
                    if (!$new_price = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$modx_product->id, "type"=>$res->getTVValue("tvTypeDelivery")))){
                        $new_price = $this->modx->newObject("SoftjetsyncPrices");
                        $new_price->set("product_id", $modx_product->id);
                        $new_price->set("product_uuid", $modx_product->alias);
                        $new_price->set("type", $res->getTVValue("tvTypeDelivery")); //курьером
                        $new_price->set("price", $price);
                        $new_price->save();   
                    } else {
                        $new_price->set("product_id", $modx_product->id);
                        $new_price->set("product_uuid", $modx_product->alias);
                        $new_price->set("type", $res->getTVValue("tvTypeDelivery")); //курьером
                        $new_price->set("price", $price);
                        $new_price->save();   
                    }
                    //цена на самовывоз
                    if (!$new_price = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$modx_product->id, "type"=>$res->getTVValue("tvType")))){
                        $new_price = $this->modx->newObject("SoftjetsyncPrices");
                        $new_price->set("product_id", $modx_product->id);
                        $new_price->set("product_uuid", $modx_product->alias);
                        $new_price->set("type", $res->getTVValue("tvType")); //самовывоз
                        $new_price->set("price", $price*0.9); //скидка 10 процентов
                        $new_price->save();
                    } else {
                        $new_price->set("product_id", $modx_product->id);
                        $new_price->set("product_uuid", $modx_product->alias);
                        $new_price->set("type", $res->getTVValue("tvType")); //самовывоз
                        $new_price->set("price", $price*0.9); //скидка 10 процентов
                        $new_price->save();
                    }
                }
                $price = null;
            }
        }
    }
    //создает модификатор
    public function updateModifier($link_id, $master, $slave, $amount){
        if (!$link = $this->modx->getObject("msProductLink", array("link"=>$link_id, "master"=>$master, "slave"=>$slave))){
            $link = $this->modx->newObject("msProductLink");
        }
        $link->set("link", $link_id);
        $link->set("master", $master);
        $link->set("slave", $slave);
        $link->set("amount", $amount);
        $link->save();
    }
    //обновить модификаторы
    public function updateModifiers($res, $product){
        if ($product["groupModifiers"]){
            foreach ($product["groupModifiers"] as $group_modifier){
                //если обязательный, то создаем связь (записываем ее в types)
                if ($group_modifier["required"]){
                    foreach ($group_modifier["childModifiers"] as $modifier){
                        $slave = $this->modx->getObject("msProduct", array("alias"=>$modifier["modifierId"]));
                        $this->updateModifier(9,$res->id,$slave->id,$group_modifier["minAmount"]);
                    }
                } else { //если необязательный, значит добавка
                    foreach ($group_modifier["childModifiers"] as $modifier){
                        $slave = $this->modx->getObject("msProduct", array("alias"=>$modifier["modifierId"]));
                        $this->updateModifier(3,$res->id,$slave->id,$group_modifier["minAmount"]);
                    }
                }
            }
        }
        foreach ($product["modifiers"] as $modifier){
            if ($modifier["required"] == true){
                $link = $this->modx->newObject("msProductLink");
                $slave = $this->modx->getObject("msProduct", array("alias"=>$modifier["modifierId"]));
                $this->updateModifier(14,$res->id,$slave->id,$modifier["minAmount"]);
            }
        }
    }
    //получить типы оплат
    public function getPaymentTypes(){
        $vars["access_token"] = $this->token;
        $vars["organization"] = $this->org->id;
        $response = $this->modx->client->request('GET', "https://iiko.biz:9900/api/0/rmsSettings/getPaymentTypes?".http_build_query($vars));
        return $response = json_decode($response->getBody(),1);
    }
    //получить статус заказа
    public function getOrderStatus($order_id){
        $vars["access_token"] = $this->token;
        $vars["organization"] = $this->org->id;
        $vars["order"] = $order_id;
        $response = $this->modx->client->request('GET', "https://iiko.biz:9900/api/0/orders/info?".http_build_query($vars));
        return $response = json_decode($response->getBody(),1);
    }
    public function returnTwo(){
        return $this->returnOne();
    }
    //обновить фото
    public function updatePhotos(){
        //return 'https://iiko.biz:9900/api/0/nomenclature/'.$this->org->id.'?access_token='.$this->token.'&revision=0';
        $nom = json_decode( file_get_contents('https://iiko.biz:9900/api/0/nomenclature/'.$this->org->id.'?access_token='.$this->token.'&revision=0'),true);
        $products = $nom['products'];
        if (!$products) return $this->modx->log(modX::LOG_LEVEL_ERROR, "айка не ответила");
        foreach ($products as $product){
            if ($product_obj = $this->modx->getObject("msProduct", array("alias"=>$product["id"]))){
                $images = $this->modx->getCollection('msProductFile', array('type' => 'image', 'product_id' => $product_obj->id));
                $end = end($product['images']);
                $end = explode("/", $end['imageUrl']);
                $newurl_image = end($end);
                $newurl_image = explode(".", $newurl_image);
                $newurl_image = $newurl_image[0];
                foreach ($images as $image) {
                    if ($image->get("name") != $newurl_image){
                        $ids[] = $image->get('id');
                    }
                }
                $response = $this->modx->runProcessor('gallery/multiple',
                  array(
                    'method' => 'remove',
                    'ids' => json_encode($ids),
                  ),
                  array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/')
                );
                $ids = [];
                $end = end($product['images']);
                $image = $end["imageUrl"];
                $data = [
                    'id' => $product_obj->get("id"),
                    'file' => $image,
                    'source' => 2
                ];
                
                $response = $this->modx->runProcessor('gallery/upload', $data, [
                    'processors_path' => MODX_CORE_PATH . 'components/minishop2/processors/mgr/',
                ]);
            }
        }
    }
}