<?php
class Rkeeper extends Integrations{
    //получить значение
    public function getQuery($query){
        $query = json_decode(json_encode($query), TRUE)[0];
        return $query;
    }
    //отправить запрос
    public function sendRequest($command){
        // Создаем подключение 
        $ch = curl_init();
        //URL, с которым будет производиться операция. Значение этого параметра также может быть задано в вызове функции curl_init(). 
            curl_setopt($ch, CURLOPT_URL, 'https://SOFTJET:1@213.221.44.107:5487/rk7api/v0/xmlinterface.xml');
        //При установке этого параметра в ненулевое значение CURL будет возвращать результат, а не выводить его.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //Массив с HTTP заголовками.
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml; charset=utf-8"));
        //При установке этого параметра в ненулевое значение результат будет включать полученные заголовки.
            curl_setopt($ch, CURLOPT_HEADER, 0);
        //Передаёт строку, содержащую полные данные для передачи операцией HTTP "POST".
            curl_setopt($ch, CURLOPT_POSTFIELDS, $command);
        //При установке этого параметра в ненулевое значение будет отправлен HTTP запрос методом POST типа application/x-www-form-urlencoded, используемый браузерами при отправке форм.
            curl_setopt($ch, CURLOPT_POST, 1);
        //Установите этот параметр в ноль, чтобы запретить проверку сертификата удаленного сервера
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // загрузка страницы и выдача её браузеру
            $response = curl_exec($ch);
            if (false === $response) {
               // return curl_error($ch), PHP_EOL;
                return curl_error($ch);
            }
        // завершение сеанса и освобождение ресурсов
            curl_close($ch);
            return $response;
    }
    //обновить группы
    public function updateGroups(){
        $path = MODX_BASE_PATH.'Категории (CATEGLIST).txt';
        $file = file_get_contents($path, true);
        $xml = new SimpleXMLElement($file);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Status"] == 'rsActive'){
                $arr["pagetitle"] = $this->getQuery($item["Name"]);
                $arr["id"] = $this->getQuery($item["Ident"]);
                $arr["guid"] = $this->getQuery($item["GUIDString"]);
                $arr["parent"] = $this->getQuery($item["Parent"]);
                $arr["code"] = $this->getQuery($item["Code"]);
                $json[] = $arr;
            }
            $arr = [];
        }
        //return $json;
        foreach ($json as $product){
            if (!$obj = $this->modx->getObject('modResource', array('alias' => $product["id"]))){
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'msCategory',
                  'pagetitle' => $product["pagetitle"],
                  'parent' => 35787,
                  'template' => 3,
                  'link_attributes' => $product['parent'],
                  'show_in_tree' => 1,
                  'alias' => $product["id"]
                ));
                $id = $response->response['object']['id'];
                $errors = $response->getAllErrors();
                if ($errors) print_r($errors);
            } else {
                $obj->set('pagetitle', $product["pagetitle"]);
                $obj->save();
            }
        }
        $categories = array(
            'class' => 'msCategory',
            'parents' => 35787,
            'return' => 'data',
            'select' => ["parent", "id", "alias", "link_attributes"],
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($categories);
        $categories = $this->pdoFetch->run();
        foreach ($categories as $category){
            $category_obj = $this->modx->getObject("msCategory", array("alias"=>$category["alias"]));
            $parent_category = $this->modx->getObject("msCategory", array("alias" =>$category["link_attributes"]));
            if ($parent_category) {
                //echo "Категория: ".$category_obj->id." Родитель: ".$parent_category->id.'</br>';
                $category_obj->set("parent", $parent_category->id);
                $category_obj->save();
            }
        }
    }
    //обновить принадлежность продукта к ресторану
    public function updateProductRestourants(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="CLASSIFICATORGROUPS"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            //проверяем является ли этот айтем категорией "Блюда для доставки"
            if ($org_obj = $this->modx->getObject("modResource", array("alias"=>$this->getQuery($item["Ident"])))){
                echo $org_obj->id.'</br>';
            }
        }
    }
    //обновить рестораны
    public function updateRestourants(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="RESTAURANTS"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        return $response;
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Status"] == 'rsActive'){
                if (!$rest = $this->modx->getObject("modResource", array("alias"=>$this->getQuery($item["Ident"])))){
                    $response = $this->modx->runProcessor('resource/create', array(
                      'class_key' => 'modResource',
                      'pagetitle' => $this->getQuery($item["Name"]),
                      'parent' => 45711,
                      'isfolder' => 0,
                      'template' => 18,
                      'show_in_tree' => 1,
                      'alias' => $this->getQuery($item["Ident"])
                    ));
                    $id = $response->response['object']['id'];
                    $errors = $response->getAllErrors();
                    if ($errors) print_r($errors);
                    $res = $this->modx->getObject('modResource', $id);
                    $res->set("alias", $point["deliveryTerminalId"]);
                    $res->setTVValue("deliveryTerminalId", $this->getQuery($item["Ident"]));
                    $res->setTVValue("online_payment", 1);
                    $res->setTVValue("cash_courier_payment", 1);
                    $res->setTVValue("cashless_courier_payment", 1);
                    $res->setTVValue("courier", 1);
                    $res->setTVValue("selfcourier", 1);
                    $res->setTVValue("restourant", 1);
                    $res->setTVValue("iiko_organization_key", $this->getQuery($item["Ident"]));
                    $res->save();
                }
            }
        }
    }
    //обновить типы подачи
    public function updateOrderTypes(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="CLASSIFICATORGROUPS"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            //проверяем является ли этот айтем категорией "Блюда для доставки"
            if ($item["Ident"] == 12289){
                foreach ($item->Childs->Child as $product){
                    if ($product_obj = $this->modx->getObject("modResource", array("alias" => $this->getQuery($product["ChildIdent"])))){
                        $product_obj->set("order_types", "1,2,3,4");
                        $product_obj->save();
                    }
                }
            }
        }
    }
    //обновить продукты в категориях
    public function updateProductsCategory(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="CLASSIFICATORGROUPS"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Status"] == 'rsActive'){
                $parent_obj = $this->modx->getObject("modResource", array("alias"=>$this->getQuery($item["ItemIdent"])));
                foreach ($item->Childs->Child as $product){
                    $product_obj = $this->modx->getObject("msProduct", array("alias"=> $this->getQuery($product["ChildIdent"])));
                    if ($product_obj && $parent_obj) {
                        $obj = $this->modx->newObject("msCategoryMember");
                        $obj->set("product_id", $product_obj->id);
                        $obj->set("category_id", $parent_obj->id);
                        $obj->save();
                    }
                }
            }
            $arr = [];
        }
    }
    //обновить категории
    public function updateCategories(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="CLASSIFICATORGROUPS"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Status"] == 'rsActive'){
                $arr["pagetitle"] = $this->getQuery($item["Name"]);
                $arr["id"] = $this->getQuery($item["Ident"]);
                $arr["guid"] = $this->getQuery($item["GUIDString"]);
                $arr["parent"] = $this->getQuery($item["MainParentIdent"]);
                $arr["code"] = $this->getQuery($item["Code"]);
                $json[] = $arr;
            }
            $arr = [];
        }
        //return $json;
        foreach ($json as $product){
            if (!$obj = $this->modx->getObject('modResource', array('alias' => $product["id"]))){
                $response = $this->modx->runProcessor('resource/create', array(
                  'class_key' => 'msCategory',
                  'pagetitle' => $product["pagetitle"],
                  'parent' => 42189,
                  'template' => 3,
                  'link_attributes' => $product['parent'],
                  'show_in_tree' => 1,
                  'alias' => $product["id"]
                ));
                $id = $response->response['object']['id'];
                $errors = $response->getAllErrors();
                if ($errors) print_r($errors);
            } else {
                $obj->set('pagetitle', $product["pagetitle"]);
                $obj->set('link_attributes', $product["parent"]);
                $obj->save();
            }
        }
        $categories = array(
            'class' => 'msCategory',
            'parents' => 42189,
            'return' => 'data',
            'select' => ["parent", "id", "alias", "link_attributes"],
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($categories);
        $categories = $this->pdoFetch->run();
        foreach ($categories as $category){
            $category_obj = $this->modx->getObject("msCategory", array("alias"=>$category["alias"]));
            $parent_category = $this->modx->getObject("msCategory", array("alias" =>$category["link_attributes"]));
            if ($parent_category) {
                //echo "Категория: ".$category_obj->id." Родитель: ".$parent_category->id.'</br>';
                $category_obj->set("parent", $parent_category->id);
                $category_obj->save();
            }
        }
        
    }
    //обновить продукты 
    public function updateProducts(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="MenuItems"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        $xml = new SimpleXMLElement($response, true);
        //$path = MODX_BASE_PATH.'tigrusnom.txt';
        //$file = file_get_contents($path, true);
        //$xml = new SimpleXMLElement($file, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Status"] == 'rsActive'){
                $arr["pagetitle"] = $this->getQuery($item["Name"]);
                $arr["id"] = $this->getQuery($item["Ident"]);
                $arr["guid"] = $this->getQuery($item["GUIDString"]);
                $arr["parent"] = $this->getQuery($item["Parent"]);
                $arr["code"] = $this->getQuery($item["Code"]);
                $arr["VisualType_Image"] = $this->getQuery($item["VisualType_Image"]);
                $json[] = $arr;
            } else {
                // формируем массив из отключённых у клиента товаров
                if (in_array($this->getQuery($item["Ident"]), $items)){
                    $disabledArray[] = $this->getQuery($item["Name"]);
                    //$disabledArrayId[] = $this->getQuery($item["Ident"]);
                    //echo $num.". ".htmlspecialchars($this->getQuery($item["Name"]))." - ".$item["Status"]."\n";
                    //$num++;
                }
            }
            $arr = [];
        }
        
        // отключаем товары которые отключены в клиентской выгрузке
        foreach ($disabledArray as $item) {
            //echo $item." - ";
            $itemObject = $this->modx->getObject("modResource", array("pagetitle"=>$item));
            if(is_object($itemObject)){
                //echo "Текст: ".$itemObject->get("pagetitle");
                $itemObject->set("published",0);
                $itemObject->save();
                
                // сброс ошибок MODX - для работы импорта в галерею
            	$this->modx->error->reset();
            	$this->modx->cacheManager->refresh();
            }
            //echo "\n";
        }
        
        //$parents = $this->getMainTree();
        foreach ($json as $product){
            if ($parent = $this->modx->getObject("modResource", array("alias"=>$product["parent"]))){
                if (!$obj = $this->modx->getObject("msProduct", array("alias"=>$product["id"]))){
                    $response = $this->modx->runProcessor('resource/create', array(
                        'class_key' => 'msProduct',
                        'pagetitle' => $product['pagetitle'],
                        'parent' => $parent->id,
                        'template' => 4,
                        'show_in_tree' => 1,
                        'order_types' => "", //обязательно пропсиываем типы подачи
                        'alias' => $product["id"],
                        
                        //Данные
                        'price' => 0,
                        'article' => (int)$product['code'],
                        'old_price' => 0,
                        'favorite' => 0,
                        'popular' => 0,
                        'additive' => 0,
                    ));
                    $id = $response->response['object']['id'];
                    $error = $response->getAllErrors();
                    if ($error){
                        echo ($error);
                    } else {
                        echo $id."</br>";
                    }
                } else {
                    $obj->set('pagetitle', $product['pagetitle']);
                    $obj->set('parent', $parent->id);
                    $obj->save();
                }
            }
        }
        //обновляем типы подач
        $this->updateOrderTypes();
    }
    //посмотреть скидки
    public function getDiscounts(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="DISCOUNTS"/>
        </RK7Query>';
        return $response =  $this->sendRequest($xml);
    }
    //
    public function getDiscountsTypes(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="DISCOUNTDETAILS"/>
        </RK7Query>';
        return $response =  $this->sendRequest($xml);
    }
    //иницировать создание заказ
    public function initOrder(msOrder $order){
        $order_id = $order->id;
        $order_address = $this->modx->getObject('msOrderAddress', array('id'=> $order->address));
        $products = $order->getMany('Products');
        foreach ($products as $product){
            $product_object = $this->modx->getObject("modResource", $product->product_id);
            $string .= '<Dish id="'.$product_object->alias.'" quantity="'.($product->count*1000).'" sdc="123"/>';
        }
        //return $string;
        $xml = '
        <?xml version="1.0" encoding="UTF-8"?>
        <RK7Query>
         <RK7CMD CMD="CreateOrder">
          <Order persistentComment="100500">
           <Table code="1"/>
          </Order>
         </RK7CMD>
        </RK7Query>';
        $order = $this->createOrder($xml);
        $xml = new SimpleXMLElement($order, true);
        $visit = $this->getQuery($xml->Order['visit']);
        $orderIdent = $this->getQuery($xml->Order['orderIdent']);
        $saveOrderXml =  "<?xml version='1.0' encoding='UTF-8'?>
            <RK7Query>
             <RK7CMD CMD='SaveOrder'>
              <Order visit='$visit' orderIdent='$orderIdent'/>
              <Session>
               ".$string."
              </Session>
             </RK7CMD>
            </RK7Query>
            ";
        $addOrderProducts = $this->sendRequest($saveOrderXml);
        $xml = new SimpleXMLElement($addOrderProducts, true);
        $visit = $this->getQuery($xml->Order['visit']);
        $order_address->set("id_integration_order", $visit);
        $order_address->save();
        $miniShop2 = $this->modx->getService('miniShop2');
        $miniShop2->initialize($this->modx->context->key, $scriptProperties);
        $miniShop2->changeOrderStatus($order_id, 6);
        return $addOrderProducts;
    }
    //заполнить заказ продуктами
    public function addOrderProducts(){
        
    }
    //отправить заказ
    public function createOrder($xml){
        $order = $this->sendRequest($xml);
        return $order;
    }
    //получить главную категорию, с которой мы вообще работать собираемся
    public function getMainTree(){
        $categories = array(
            'class' => 'msCategory',
            'parents' => 45507,
            'return' => 'data',
            'select' => ["parent","pagetitle", "id", "alias", "link_attributes"],
            'limit' => 10000
        );
        $this->pdoFetch->setConfig($categories);
        $categories = $this->pdoFetch->run();
        $categories = array_column($categories, 'id');
        return $categories;
    }
    //обновить прайс-листы
    public function updatePriceTypes(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="PRICETYPES"/>
        </RK7Query>';
        $response =  $this->sendRequest($xml);
        //SoftjetsyncPriceTypes
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Status"] == 'rsActive'){
                $arr["name"] = $this->getQuery($item["Name"]);
                $arr["id"] = $this->getQuery($item["Ident"]);
                $arr["currency"] = "RUB";
                $arr["code"] = $this->getQuery($item["Code"]);
                $json[] = $arr;
            }
            $arr = [];
        }
        foreach ($json as $price_type){
            if (!$price_type_obj = $this->modx->getObject("SoftjetsyncPriceTypes", array("uuid" => $price_type["id"]))){
                $price_type_obj = $this->modx->newObject("SoftjetsyncPriceTypes", array(
                    "uuid" =>  $price_type["id"],
                    "name" => $price_type["name"],
                    "currency" => $price_type["currency"]
                ));
            } else {
                $price_type_obj->set("uuid", $price_type["id"]);
                $price_type_obj->set("name", $price_type["name"]);
            }
            $price_type_obj->save();
        }
    }
    //обновить цены
    public function updatePrices(){
        $xml = '
        <?xml version="1.0" encoding="utf-8"?>
        <RK7Query>
            <RK7CMD CMD="GetRefData" RefName="PRICES"/>
        </RK7Query>';
        //$response =  $this->sendRequest($xml);
        $path = MODX_BASE_PATH.'tigrusprices.txt';
        $response = file_get_contents($path, true);
        $xml = new SimpleXMLElement($response, true);
        foreach ($xml->RK7Reference->Items->Item as $item){
            if ($item["Species"] == "psDish"){
                if ($product_obj = $this->modx->getObject("modResource", array("alias"=> $this->getQuery($item["ObjectID"])))){
                    if ($price_type = $this->modx->getObject("SoftjetsyncPriceTypes", array("uuid"=> $this->getQuery($item["PriceType"])))){
                        if (!$price = $this->modx->getObject("SoftjetsyncPrices", array("product_id"=>$product_obj->id, "type"=>$price_type->uuid))){
                            $price = $this->modx->newObject("SoftjetsyncPrices");
                            $price->set("product_id", $product_obj->id);
                            $price->set("product_uuid", $product_obj->alias);
                            $price->set("price", $this->getQuery($item["Value"])/100);
                            $price->set("type", $price_type->uuid);
                        } else {
                            $price->set("price", $this->getQuery($item["Value"])/100);
                        }
                        $price->save();
                    }
                }
            }
        }
    }
    //списать бонусы в заказе
    public function writeOffBonuses(msOrder $order){
        $profile = $this->modx->getObject("modUserProfile", array("internalKey"=>$order->user_id));
        $order_address = $this->modx->getObject('msOrderAddress', array('id'=> $order->address));
        $response = $this->prime_hill->getClient($profile);
        $card_number = $response["cardNumber"];
        $this->enableBonusCardInOrder($card_number);
        $this->addBonusCardInOrder($card_number, $order_address->id_integration_order);
        return $this->addBonusesInOrder($card_number, $order_address->id_integration_order, 100);
    }
    //включить поддержку бонусной карты в заказe
    public function enableBonusCardInOrder($card_number){
        $xml = "
        <?xml version='1.0' encoding='UTF-8'?>
        <RK7Query >
            <RK7CMD CMD='ParseMCR' data='$card_number' devicetype='Keyboard'/>   
        </RK7Query>";
        $response =  $this->sendRequest($xml);
        return $response;
    }
    //добавить бонуснуб карту в заказ
    public function addBonusCardInOrder($card_number, $visit){
        $xml = "
        <?xml version='1.0' encoding='utf-8'?>
        <RK7Query >
            <RK7CMD CMD='ApplyPersonalCard' CardCode='$card_number' >
                <Order visit='$visit' orderIdent='256'/>
                <Station id='15178'/>
                <Interface code='11'/>
            </RK7CMD>
        </RK7Query>
        ";
        $response =  $this->sendRequest($xml);
        return $response;
    }
    //добавить платеж бонусами в заказ
    public function addBonusesInOrder($card_number, $visit, $amount){
        $xml="
        <?xml version='1.0' encoding='utf-8'?>
        <RK7Query>
            <RK7CMD CMD='SaveOrder' deferred='1'>
                <Order visit='$visit' orderIdent='256' />
                <Session>
                    <Station code='58' />
                    <Prepay code='112' amount='$amount' cardCode='$card_number'>
                        <Interface code='11'/>
                        <Reason code='1'/>
                    </Prepay>
                </Session>
            </RK7CMD>
        </RK7Query>
        ";
        $response =  $this->sendRequest($xml);
        return $response;
    }
}