<?php
/*Скрипт осуществляет перенос импортированных PHP и HTML файлов из статичного вида в БД*/
$gitpath = (MODX_BASE_PATH.'/git'); //путь папке GIT
$gitdirectory = scandir($gitpath); //получаем каталог в папке GIT 
$gitdirectory = array_slice($gitdirectory,2);
foreach ($gitdirectory as $directory){
    $path = $gitpath.'/'.$directory;
    $files = scandir($path);
    $files = array_slice($files,2);
    foreach ($files as $file){
        $vowels = array(".html", ".php");
        $filedir = $path.'/'.$file; //путь файла
        $filename = str_replace($vowels, "", $file); //имя файла
        $filecontent = file_get_contents($filedir); //контент файла
        //echo($directory);//родительская папка
        //echo($file.'</br>');
        switch ($directory){
            case "_chunks": //перезаписываем чанки
                $chunk = $modx->getObject('modChunk', array("name"=>$filename));
                if ($chunk){ //если суещствует, перезаписываем
                    $content = $chunk->setContent($filecontent);
                    $chunk->save();
                } else { //если новый, создаем
                    $chunk = $modx->newObject('modChunk');
                    $chunk->set('name',$filename);
                    $chunk->setContent($filecontent);
                    $chunk->save();
                }
            break;
            case "_snippets": //перезаписываем сниппеты
                $snippet = $modx->getObject('modSnippet', array("name"=>$filename));
                if ($snippet){
                    $content = $snippet->setContent($filecontent);
                    $snippet->save();
                } else {
                    $snippet = $modx->newObject('modSnippet');
                    $snippet->set('name',$filename);
                    $snippet->setContent($filecontent);
                    $snippet->save();
                }
            break;
            case "_plugins": //перезаписываем плагины
                $plugin = $modx->getObject('modPlugin', array("name"=>$filename));
                if ($plugin){
                    $content = $plugin->setContent($filecontent);
                    $plugin->save();                    
                } else {
                    $plugin = $modx->newObject('modPlugin');
                    $plugin->set('name',$filename);
                    $plugin->setContent($filecontent);
                    $plugin->save();   
                }
            break;
            case "_templates": //перезаписываем шаблоны
                $template = $modx->getObject('modTemplate', array("templatename"=>$filename));
                if ($template){
                    $content = $template->setContent($filecontent);
                    $template->save();                    
                } else {
                    $template = $modx->newObject('modTemplate');
                    $template->set('templatename',$filename);
                    $template->setContent($filecontent);
                    $template->save();  
                }
            break;
            case "sql":
                $modx->query($filecontent);
            break;
        }
    }
}
//чистим кэш
define('MODX_API_MODE', true);
require $_SERVER['DOCUMENT_ROOT'].'/index.php';
shell_exec('rm -rf ' . MODX_CORE_PATH . 'cache/');