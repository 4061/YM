<?php
include __DIR__.'/cron/cron.class.php';
class ModxApi {
    
    public $modx;
    public $response;
    public function __construct($modx)
    {
        $this->modx = $modx;
        $ms2 = $this->modx->getService('miniShop2');
        $ms2->initialize($this->modx->context->key);
        $this->ms2 = $ms2;
        $this->pdoFetch = new pdoFetch($modx);
       //parent::__construct();
    }

    public function getResponse()
    {
        header('Content-Type: application/json');
        return json_encode($this->response, JSON_UNESCAPED_UNICODE);
    }

    public function run($uri)
    {
        
        $this->modx->uri = $uri;
        $explode_uri = explode("?", $uri);
        $cron = new Cron($this->modx);
        
        switch ($explode_uri[0]) {
            case '/cronNotyfication/cron/runNotyfication':
                $this->response = $cron->runNotyfication();
                break;
            case '/cronNotyfication/cron/changeStatusOrderCRON':
                $this->response = $cron->changeStatusOrderCRON();
                break;
        }
    }
}