<?php
class Cron extends ModxApi{
    // функция получения пользователей через сегментацию
    public function getSegment($data){
        $delivery_type = $data['delivery_type'];
        $restaurants = $data['restaurants'];
        $cities = $data['cities'];
        $tags = $data['tag_type'];
        $phone = $data['phone'];
        $birthday_1 = $data['birthday_1'];
        $birthday_2 = $data['birthday_2'];
        $min_dob = $data['min_dob'];
        $max_dob = $data['max_dob'];
        $last_buy1 = $data['last_buy1'];
        $last_buy2 = $data['last_buy2'];
        $date1 = $data['date_1'];
        $date2 = $data['date_2'];
        $min_cartcost = $data['min_cartcost'];
        $max_cartcost = $data['max_cartcost'];
        $gender = $data['gender'];
        $max_avg = $data['max_avg']; //максимальный средний чек
        $min_avg = $data['min_avg']; //минимальный средний чек
        $lastdays = $data['days']; //за сколько последних дней брать информацию
        $min_sum = $data['min_sum']; //минимальная сумма покупок от (price)
        $max_sum = $data['max_sum']; //максимальная сумма покупок до (price)
        $min_count = $data['min_count']; //минимальное количество заказов
        $max_count = $data['max_count']; //максимальное количество заказов
        $products = $data['products'];
        $products = explode(",", $products);
        $product_keys_placeholder = '';
        
        if($cities) {
            $citiesMass = explode(",",$cities);
            $cities = "";
            foreach($citiesMass as $cityId) {
                $city = $this->modx->getObject("modResource",$cityId);
                $cities .= "'".$city->get("pagetitle")."',";
            }
            $cities = mb_substr($cities, 0, -1);
        }
        
        if($min_count == "" || $max_count == "" || $min_count === null || $max_count === null || $min_count > 0 || $max_count > 0 || (($min_count != 0) && ($max_count != 0))){
            
            //$this->modx->log(1,"Я тут");
        
            foreach ($products as $product){
                if ($product){
                    $product_keys_placeholder = $product_keys_placeholder." AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
                }
            }
            if($tags) {
                $tag_keys_placeholder = "AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products,modx_ms2_product_links WHERE modx_ms2_product_links.master = modx_ms2_order_products.product_id AND modx_ms2_product_links.slave IN (".$tags."))";
            }
            if ($delivery_type){
                $delivery_type_placeholder = "AND modx_ms2_orders.delivery IN ($delivery_type)";
            }
            if ($restaurants){
                $restaurants_placeholder = "AND modx_ms2_order_addresses.id = modx_ms2_orders.address AND modx_ms2_order_addresses.index IN ($restaurants)";
            }
            if ($cities){
                $cities_placeholder = "AND modx_ms2_order_addresses.id = modx_ms2_orders.address AND modx_ms2_order_addresses.city IN ($cities)";
            }
            if ($birthday_1){
                $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
            }
            if ($birthday_2){
                $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
            }
            if ($min_dob>0 or $max_dob>0){
                $dobswither = " AND dob <> 0";
                $this->modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
            }
            if ($min_dob){
                $min_dob = $min_dob*365*24*60*60;
                $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
            }
            if ($max_dob){
                $max_dob = $max_dob*365*24*60*60;
                $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
            }
            if ($last_buy1){
                $last_buy1_placeholder = " AND time_end <= DATE_ADD(CURDATE(), INTERVAL -'$last_buy1' DAY)";
            }
            if ($last_buy2){
                $last_buy2_placeholder = " AND time_end >= DATE_ADD(CURDATE(), INTERVAL -'$last_buy2' DAY)";
            }
            if ($date1){
                $date1_placeholder = " AND modx_ms2_orders.createdon >= '$date1'";
            }
            if ($date2){
                $date2_placeholder = " AND modx_ms2_orders.createdon <= '$date2'";
            }
            if ($gender){
                $gender_placeholder = " AND modx_segment_users.user_id IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
            }
            if ($max_cartcost>0){
                $max_cartcost_placeholder = "AND modx_ms2_orders.cart_cost<='$max_cartcost'";
            }
            if ($min_cartcost>0){
                $min_cartcost_placeholder = " AND modx_ms2_orders.cart_cost>='$min_cartcost'";
            }
            if ($min_sum>0){
                $min_sum_placeholder = " AND ord_sum>='$min_sum'";
            }
            if ($max_sum>0){
                $max_sum_placeholder = " AND ord_sum<='$max_sum'";
            }
            if ($min_count>0){
                $min_count_placeholder = " AND ord_count>='$min_count'";
            }
            if ($max_count>0){
                $max_count_placeholder = " AND ord_count<='$max_count'";
            }
            if ($min_avg>0){
                $min_avg_placeholder = " AND ord_avg>='$min_avg'";
            }
            if ($max_avg>0){
                $max_avg_placeholder = " AND ord_avg<='$max_avg'";
            }
            if ($lastdays){
                $lastdays_placeholder = " AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'$lastdays' DAY)";
            }
            if ($phone){
                $phone_placeholder = " AND modx_user_attributes.mobilephone = '$phone'";
            }
            $sql = "
            SELECT 
                modx_segment_users.user_id as user_id,
                modx_segment_users.user_name as user_name,
                modx_user_attributes.email as email,
                modx_user_attributes.mobilephone as mobilephone,
                modx_user_attributes.city as city,
                modx_user_attributes.website as website
            FROM 
                modx_segment_users,modx_ms2_orders,modx_user_attributes,modx_ms2_deliveries,modx_ms2_order_addresses
            WHERE 
                modx_segment_users.user_id = modx_ms2_orders.user_id AND
                modx_segment_users.user_id = modx_user_attributes.internalKey AND
                modx_ms2_deliveries.id = modx_ms2_orders.delivery AND 
                modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>=0 $lastdays_placeholder $date1_placeholder $date2_placeholder $last_buy1_placeholder $last_buy2_placeholder) AND
                modx_segment_users.user_id IN (Select internalKey FROM modx_user_attributes WHERE id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder)
                $min_avg_placeholder 
                $max_avg_placeholder
                $min_sum_placeholder
                $max_sum_placeholder
                $gender_placeholder
                $min_count_placeholder
                $max_count_placeholder
                $product_keys_placeholder
                $min_cartcost_placeholder
                $max_cartcost_placeholder
                $delivery_type_placeholder
                $restaurants_placeholder
                $cities_placeholder
                $tag_keys_placeholder
                $phone_placeholder
            Group By 
                modx_segment_users.user_id";
                
            //$this->modx->log(1,$sql);
                
            $info_user = $this->modx->query($sql);
            $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
                
        } else {
            $sql = "SELECT user_id as id FROM modx_segment_users";
            $buyerQuery = $this->modx->query($sql);
            $buyers = $buyerQuery->fetchAll(PDO::FETCH_ASSOC);
            
            $buyersMass = array(0 => 1);
            foreach($buyers as $buyer){
              $buyersMass[] = $buyer['id'];
            }
            
            if ($birthday_1){
                $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
            }
            if ($birthday_2){
                $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
            }
            if ($min_dob>0 or $max_dob>0){
                $dobswither = " AND dob <> 0";
                $this->modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
            }
            if ($min_dob){
                $min_dob = $min_dob*365*24*60*60;
                $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
            }
            if ($max_dob){
                $max_dob = $max_dob*365*24*60*60;
                $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
            }
            if ($gender){
                $gender_placeholder = " AND ".$table.".".$idUser." IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
            }
            if ($phone){
                $phone_placeholder = " AND modx_user_attributes.mobilephone = '$phone'";
            }
            
            $sql = "
            SELECT 
                modx_user_attributes.internalKey as user_id,
                modx_user_attributes.fullname as user_name,
                modx_user_attributes.email as email,
                modx_user_attributes.mobilephone as mobilephone,
                modx_user_attributes.city as city,
                modx_user_attributes.website as website
            FROM 
                modx_user_attributes
            WHERE
                id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder
                $gender_placeholder
                $phone_placeholder
            Group By 
                modx_user_attributes.id";
            
            $getuser = $this->modx->query($sql);
            $getuser = $getuser->fetchAll(PDO::FETCH_ASSOC);
            
            $info_user = array();
            $keyMass = 0;
            foreach($getuser as $key => $user) {
                $test .= $user['user_id'].", "; 
                if(!in_array($user['user_id'],$buyersMass)){
                    $info_user[$keyMass] = $user;
                    $info_user[$keyMass]['ord_count'] = $info_user[$keyMass]['ord_avg'] = $info_user[$keyMass]['ord_sum'] = 0;
                    $keyMass++;
                }
            }
        }
            
        return($info_user);

    }
    
    // функция для сравнения текущей даты с указанной в рассылке
    public function checkTime($data,$dataNow,$maxDate){
        // если указанное время не равно *
        if($data != "*") {
            // создаём масси для ответа
            $listResult = array();
            // есть ли * в выражении
            if(stristr($data, '*') === FALSE) {
                // если нет, то выводим массив значений
                $output = explode(",",$data);
            // если * есть в выражении, то это дробь
            } else {
                // получаем делитель
                $min = explode("/",$data)[1];
                // высчитываем лимит для рассчёта
                $limit = floor($maxDate/$min);
                // счётчик выставляем в 1
                $key = 1;
                // перебераем по лимиту и высчитываем массив значений времени
                while($key <= $limit) {
                    $listResult[] = ($key * (int)$min);
                    $key++;
                }
                // присваиваем ответ после условий для передачи дальше
                $output = $listResult;
            }
            // проверяем вхождение текущего времени в полученный массив данных
            if(in_array((int)$dataNow, $output)) {
                // если есть вхождение, то ставим ответ true
                $resultMin = true;
            } else {
                // если нет вхождения, то ставим ответ false
                $resultMin = false;
            }
        } else {
            // если значение * то выставляем ответ в true
            $resultMin = true;
        }
        // возвращаем ответ из функции
        return $resultMin;
    }
    
    // функция для проверки стоит ли в данный момент запускать рассылку
    public function runNotyficationCheck($id,$timeZone){
        //(array)$data = $this->modx->request;
        
        // получаем элемент таблицы по ID
        $editCall = $this->modx->getObject("SegmentNotifications",$id);
        // установленное время из объекта, то что указано для рассылки
        $checkDate['minutes'] = $editCall->get("time_minutes");     // минуты
        $checkDate['hours'] = $editCall->get("time_hours");         // часы
        $checkDate['days'] = $editCall->get("time_days");           // дни
        $checkDate['months'] = $editCall->get("time_months");       // месяцы
        $checkDate['daysWeek'] = $editCall->get("time_days_week");  // дни недели
        
        $timeZone = ($timeZone)? $timeZone : "Europe/Moscow";
        
        // выставляем время по москве
        date_default_timezone_set($timeZone); // $this->modx->getOption("date_timezone")
        
        // текущее время
        $nowDate['minutes'] = date("i");    // минуты
        $nowDate['hours'] = date("G");      // часы
        $nowDate['days'] = date("j");       // дни
        $nowDate['months'] = date("n");     // месяцы
        $nowDate['daysWeek'] = date("N");   // дни недели
        
        // передаём значения текущего времени и заданного в параметре рассылки для сравнения
        $resultMin = $this->checkTime($checkDate['minutes'],$nowDate['minutes'],60);
        $resultHour = $this->checkTime($checkDate['hours'],$nowDate['hours'],60);
        $resultDay = $this->checkTime($checkDate['days'],$nowDate['days'],date('t', mktime(0, 0, 0, $nowDate['months'])));
        $resultMonth = $this->checkTime($checkDate['months'],$nowDate['months'],12);
        $resultDWeek = $this->checkTime($checkDate['daysWeek'],$nowDate['daysWeek'],7);
        
        /*if($resultMin){echo 'Да';} else {echo 'нет';} echo "\n";
        if($resultHour){echo 'Да';} else {echo 'нет';} echo "\n";
        if($resultDay){echo 'Да';} else {echo 'нет';} echo "\n";
        if($resultMonth){echo 'Да';} else {echo 'нет';} echo "\n";
        if($resultDWeek){echo 'Да';} else {echo 'нет';} echo "\n";
        
        print_r($checkDate);
        echo "\n";
        print_r($nowDate);
        echo "\n";
        echo $output;*/
        
        // общая проверка для всех условий
        if($resultMin && $resultHour && $resultDay && $resultMonth && $resultDWeek){
            $output = true;
            //$this->modx->log(1,"Выполняем рассылку - ".$id);
        } else {
            $output = false;
            //$this->modx->log(1,"Рассылка не запускалась - ".$id);
        }
        // возвращаем ответ о том можно ли запустить рассылку
        return $output;
    }
    // Функция запуска самой рассылки
    public function runNotyfication(){
        
        // функция удаления старыйх заказов из сессии
        $this->removeOldSessions();
        // Функци для создания промокодов на дни рождения
        $this->createPromoBirthdey();
        // обновляем реферальные ссылки
        $this->updatePromoReferal();
        
        // получаем список ID для активных рассылок
        $notyfyObject = $this->modx->getCollection("SegmentNotifications",array("active" => 1));
        // перебераем полученные объекты рассылок
        foreach($notyfyObject as $notyfication){
            // запускаем функцию для выгрузки и если вернётся положительный ответ, то выполняем действия необходимые для рассылки
            if($this->runNotyficationCheck($notyfication->get('id'),$notyfication->get('timezone'))) {
                // выполняем рассылку
                echo "Выполняем рассылку - время совпало ('".$notyfication->get('timezone')."')\n";
                
                // получаем ID настройки для получения сегмента пользователей
                $segmentId = $notyfication->get('segment_setting_id');
                // получаем по ID сегмента
                $getSegment = $this->modx->getObject("SegmentSegments",$segmentId);
                $settingsId = $getSegment->get("settings_id");
                
                // получаем по ID настройки сегмента
                $getSegmentSetting = $this->modx->getObject("SegmentSettings",$settingsId);
                $getListParameters = $getSegmentSetting->get("segment_settings");

                // разбиваем строку с параметрами на массив
                $getListParameters = explode("&",$getListParameters);
                // удаляем нулевой элемнт, он будет пустым
                unset($getListParameters[0]);
                // создаём пустой массив, в нём мы будем собирать ассоциативный массив
                $assocMassParameters = array();
                // формируем ассоциативный массив с параметрами
                foreach($getListParameters as $param) {
                    $param = explode("=",$param);
                    $assocMassParameters[$param[0]] = $param[1];
                }
                
                // обращаемся к функции для получения списка пользователей
                $users = $this->getSegment($assocMassParameters);
                
                //$this->modx->log(1,"ID сегмента: ".$segmentId.", список ID пользователей (2): ".json_encode($users));
                
                // этот новый массив пользователей в дальнейшем будет участвовать в рассылке
                $newUsers = $users;
                // проверяем, нужно нам проводить рассылку по новичкам или нет
                if($notyfication->get('only_new_users')) {
                    $where = array(
                        "segment_id" => $segmentId
                    );
                    // получаем старых пользователей в сегменте
                    $oldUsers = $this->modx->getCollection("SegmentBase",$where);
                    // формируем массив с ID старых пользователей
                    foreach($oldUsers as $oldUser){
                        $oldUsersMass[] = $oldUser->get('user_id');
                        $this->modx->log(1,$oldUser->get('user_id'));
                    }
                    // создаём массив новичков, в начале он равен новому списку юзеров
                    $newUsers = $users;
                    // перебераем массив новичков
                    foreach($newUsers as $key => $user) {
                        // проверяем существует ли в данном списке совпадения со старым списком пользователй
                        if(in_array($user['user_id'],$oldUsersMass)) {
                            // если данный пользователь есть в старом списке, то удаляем его из массива
                            unset($newUsers[$key]);
                        }
                    }
                }
                
                // удаляем список пользователей относившихся к данному сегменту
                $sql = 'DELETE FROM modx_segment_base WHERE segment_id = '.$segmentId;
                $this->modx->query($sql);
                
                // заплняем новыс списком пользователей
                foreach ($users as $user) {
                    $elem = $this->modx->newObject("SegmentBase");
                    $elem->set("segment_id",$segmentId);
                    $elem->set("user_id",$user['user_id']);
                    $elem->save();
                }
                
                // получаем промо привязанное к рассылке, для присвоения её 
                $promoCode = $notyfication->get('promo');
                
                if($promoCode) {
                    // удаляем список пользователей относившихся к данному сегменту
                    $sql = 'DELETE FROM modx_promo_users_secret WHERE segment_id = '.$segmentId;
                    $this->modx->query($sql);
                    
                    // заплняем новыс списком пользователей
                    foreach ($users as $user) {
                        $elem = $this->modx->newObject("PromoUsersSecret");
                        $elem->set("segment_id",$segmentId);
                        $elem->set("id_user",$user['user_id']);
                        $elem->set("id_promo",$promoCode);
                        $elem->save();
                    }
                }
                
                // подствляем тип рассылки по указанному варианту для самой рассылки
                switch ($notyfication->get('type_notification')) {
                    case "email":
                        $this->emailDelivery($newUsers,$notyfication->get('id'));
                    break;
                    case "sms":
                        $this->smsDelivery($newUsers,$notyfication->get('id'));
                    break;
                    case "push":
                        $this->pushDelivery($newUsers,$notyfication->get('id'));
                    break;
                }
                // отмечаем время окончания рассылки
                date_default_timezone_set($notyfication->get('timezone'));
                $notyfication->set('date_last_notification',date('Y-m-d H:i:s'));
                $notyfication->save();
            } else {
                echo "Рассылка не запускается - время не совпадает ('".$notyfication->get('timezone')."')\n";
            };
            //echo $notyfication->get("id");
        };
        
        
    }
    
    // подгрузка шаблона, если он есть
    public function loadTemplate($idNotification){
        // указываем данные для поиска шаблона под указанную рассылку
        $where = array(
            "id_notification" => $idNotification
        );
        // получаем по указанному условию подходящие варианты (он будет один)
        $templates = $this->modx->getCollection("SegmentNotificationsTemplate",$where);
        // если мы получили результат
        if($templates) {
            // перебераем полученные шаблоны (он будет один)
            foreach($templates as $template) {
                // получаем из шаблоны нужные данные: название шаблона, а так же параметры плейсхолдеров
                $templateTpl = $template->get("template_name");
                $parameters = array(
                    "title" => $template->get("title"),
                    "content" => $template->get("content")
                );
            }
            // если шаблон не равен 0
            if($templateTpl) {
                // формируем письмо с указанными параметрами
                $body = $this->modx->getChunk($templateTpl,$parameters);
            } else {
                // если шаблон равен 0, то просто берём текст
                $body = $parameters["content"];
            }
            // формируем тему письма
            $subject = $parameters["title"];
            
            
        } else {
            // тема письма
            $subject = $template->get("title");
            // текст в письме
            $body = $template->get("content");
        }
        
        $letter = array(
            "subject" => $subject,
            "body" => $body,
        );
        
        return $letter;
    }
    
    // рассылка на почту
    public function emailDelivery($users,$idNotification){
        // запускаем метод для подгрузки шаблона, если он есть
        $letter = $this->loadTemplate($idNotification);
        // получаем данные для рассылки
        $subject = $letter["subject"];
        $body = $letter["body"];
        
        foreach ($users as $user) {
            // получаем данные для письма
            $email_from = $this->modx->getOption('emailsender'); // эл.адрес оправителя письма
            $sitename = $this->modx->getOption('site_name'); // имя/название отправителя письма
            
            // отправка письма по списку почтовых ящиков
            $this->modx->getService('mail', 'mail.modPHPMailer');
            
            if($user['email'] && !(stristr($user['email'], '@') === FALSE)) {
                $this->modx->mail->set(modMail::MAIL_BODY,$body);
                $this->modx->mail->set(modMail::MAIL_FROM,$email_from);
                $this->modx->mail->set(modMail::MAIL_FROM_NAME,$sitename);
                $this->modx->mail->set(modMail::MAIL_SUBJECT,$subject);
                // это отправка в лог информации о том куда отправляется письмо
                $this->modx->log(1, "получатель: ".$user['email'].", отправитель: $email_from");
                $this->modx->mail->address('to',$user['email']);
                
                $this->modx->mail->setHTML(true);
                if (!$this->modx->mail->send()) {
                    $this->modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while trying to send the email: '.$this->modx->mail->mailer->ErrorInfo);
                }
                // сбрасываем указанные данные, для возможности повторной отправки
                $this->modx->mail->reset();
            }
            
            // задержка в отправке писем
            sleep(0.5);
        }
        
    }
    
    // рассылка через SMS
    public function smsDelivery($users,$idNotification){
        // подгружаем библиотеку с классами запроса
        include  $_SERVER["DOCUMENT_ROOT"].'/file/sms/sms.ru.php';
        // получаем ключ сервиса для формирования запросов
        $smsru_api_key = $this->modx->getOption('smsru_api_key');
        $smsru = new SMSRU($smsru_api_key);
        $data = new stdClass();
        
        // запускаем метод для подгрузки шаблона, если он есть
        $letter = $this->loadTemplate($idNotification);
        // проверяем, есть ли текст для сообщения
        if($letter["body"]) {
            // перебераем пользователей для рассылки
            foreach ($users as $user) {
                // получаем телефон и текст для отправки
                $data->to = "+".$user["mobilephone"];
                $data->text = $letter["body"];
                // отправляем запрос в сервис на рассылку по указанному номеру
                $smsru->send_one($data);
                
                //$this->modx->log(1,"Телеон: "."+".$user["mobilephone"].", текст: ".$letter["body"]);
            }
        }
    }
    
    // рассылка через PUSH уведомления
    public function pushDelivery($users,$idNotification){
        // основые настройки запроса к сервису
        $url = 'https://fcm.googleapis.com/fcm/send';
        $YOUR_API_KEY = $this->modx->getOption('ms2_push_token'); // Server key
        $request_headers = [
            'Content-Type: application/json',
            'Authorization: key=' . $YOUR_API_KEY,
            'apns-push-type: alert'
        ];
        
        // запускаем метод для подгрузки шаблона, если он есть
        $letter = $this->loadTemplate($idNotification);
        
        // перебераем полученных пользователей
        foreach ($users as $user){
            // проверяем есть ли у пользователя токен firebase
            if ($user["city"] && $user["website"]){
                // JSON массив для запроса PUSH
                $JSONcode = '{
                    "to":"'.$user["city"].'",
                    "notification": {
                        "sound":"default",
                        "title": "'.$letter["subject"].'",
                        "body":"'.$letter['body'].'",
                        "content_available": true,
                        "priority": "high",
                        "high_priority": "high",
                        "show_in_foreground": true
                    },
                    "adnroid": {
                        "tag": "location",
                        "priority": "high"
                    },
                    "data": {
                        "id": 4,
                        "action": "logout",
                        "sound": "default",
                        "body": "test body",
                        "title": "test title",
                        "content_available": true,
                        "priority": "high"
                    },
                    "apns": {
                        "headers": {
                            "apns-push-type": "background",
                            "apns-priority": "10",
                            "apns-collapse-id": "location"
                        },
                        "payload": {
                            "aps": {
                                "apns-collapse-id": "location",
                                "contentAvailable": true
                            }
                        }
                    }
                }';
                
                $request_body1 = json_decode($JSONcode);
                
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_body1));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $response = curl_exec($ch);
                curl_close($ch);
                $this->modx->log(1,"рассылка на токены: ".$user["city"].", заголовок: ".$letter["subject"].", текст: ".$letter['body'].", ответ сервера:".print_r($response,1));
                
            }
        }

    }
    
    // смена статуса заказа через крон через определённый промежуток времени
    // необходимо что бы было настроено ображение через крон к данному методу
    public function changeStatusOrderCRON(){
        // получаем параметр который отвечает за автоматическую смену статуса заказа
        $daysCange = $this->modx->getOption("app_change_status_time");
        //$this->modx->log(1,"Зашли в метод смены статуса");
        // если данный параметр > 0, то выполняем метод
        if($daysCange > 0) {
            // условие выборки заказов, заказ должен быть уже оплаченным (2 - "оплачен")
            $where = array(
                "status" => 2
            );
            // получаем перечень объектов заказа
            $orders = $this->modx->getCollection("msOrder",$where);
            // перебераем объекты заказа
            foreach($orders as $oder) {
                // если у заказа дата его изменения меньше чем текущее время на количество указанных дней помноженное на 86400 (секунд в сутках)
                if ((time() - strtotime($oder->updatedon)) >= (5)) { // $daysCange * 86400; 86400 - 1 день;
                    // то меняем статус заказа "закрыт" (ID 9)
                    $oder->set("status",9);
                    $oder->save();
                }
            }
        }
        
        return true;
    }
    // метод генерации подарка на день рождения в виде промокода
    public function createPromoBirthdey(){
        if(date("H:i") == "00:00"){ // "00:00" + 1 час
            // получаем параметр который отвечает за количество дней до дня рождения, когда можно применить промокод
            $daysBefore = $this->modx->getOption("app_birthday_days_before");
            // получаем параметр который отвечает за количество дней после дня рождения, когда можно применить промокод
            $daysAfter = $this->modx->getOption("app_birthday_days_after");
            // получаем параметр который отвечает за то, какая картинка будет у акции
            $image = $this->modx->getOption("app_birthday_image");
            // получаем параметр который отвечает за тип промокода
            $type = $this->modx->getOption("app_birthday_promo_type");
            // получаем параметр который отвечает за дополнительные данные по промокоду
            $typeValue = $this->modx->getOption("app_birthday_promo_type_value");
            
            // получаем параметр который отвечает за дополнительные данные по промокоду
            $count = $this->modx->getOption("app_birthday_count");
            // получаем параметр который отвечает за дополнительные данные по промокоду
            $title = $this->modx->getOption("app_birthday_name");
            // получаем параметр который отвечает за дополнительные данные по промокоду
            $description = $this->modx->getOption("app_birthday_description");
            
            // получаем название столбца в БД по типу промокода
            switch($type){
                case "2":
                    $nameValue = "id_product"; // для подарка
                break;
                case "1":
                    $nameValue = "child_discount"; // скидка на заказ
                break;
                case "5":
                    $nameValue = "child_discount_rub"; // скидка в рублях
                break;
            }
            
            // получаем список всех пользователей
            $users = $this->modx->getCollection("modUserProfile");
            // создаём пустой массив с днями рождения
            $birthdayMass = array();
            // перебераем пользователей
            foreach($users as $user){
                // проверяем наличие дня рождения
                if($user->dob){
                    // собираем массив с ДР
                    //$birthdayMass[$user->internalKey] = $user->dob;
                    
                    $query = $this->modx->newQuery('PromoBase');
                    $query->where(array(
                       'code:LIKE' => 'BD-'.$user->internalKey.'-%'
                    ));
                    $getPromo = $this->modx->getObject('PromoBase',$query);
                    
                    if(!$getPromo){
                        $first_code_part = $user->internalKey; // первая часть кода - это ID пользователя
                        $second_code_part = rand(1000, 9999); // вторая часть кода генерируется
                        $promocode = "BD-".$first_code_part."-".$second_code_part;
                        $code_obj = $this->modx->newObject("PromoBase");
                        $code_obj->set("code", $promocode); // промокод
                        $code_obj->set("name", $title); // промокод
                        $code_obj->set("availability", 2); // персональный
                        $code_obj->set("user_use_count", $count); // Сколько раз пользователь может применить
                        $code_obj->set("use_count", $count); // Суммарное количество использований
                        $code_obj->set("use_count_in_check", $count); // Сколько раз можно использовать в корзине
                        $code_obj->set("type", $type); //Тип 6 (на день рождение)
                        $code_obj->set("author_id", $user->internalKey); // ID пользователя
                        $code_obj->set("activity", 1); // активен
                        $code_obj->set("description", $description); // Коммент для нас
                        $code_obj->set("date_start", date("d-m-Y",strtotime(date("d-m-".date("Y"),$user->dob)." - ".$daysBefore." days"))); //Дата начала для использования промокода
                        $code_obj->set("date_end", date("d-m-Y",strtotime(date("d-m-".date("Y"),$user->dob)." + ".$daysAfter." days"))); //Дата начала для использования промокода
                        $code_obj->set("image", $image);
                        $code_obj->set($nameValue, $typeValue);
                        
                        $code_obj->save();
                        
                        $promoUser = $this->modx->newObject('PromoUsersSecret');
                        $promoUser->set("id_user",$user->id);
                        $promoUser->set("id_promo",$code_obj->get("id_promo"));
                        $promoUser->set("segment_id",0);
                        $promoUser->save();
                    } else {
                        $code_obj = $getPromo;
                        $yearOld = date("d-m-Y",strtotime($code_obj->get("date_start")));
                        $yearNew = date("d-m-Y",strtotime(date("d-m-".date("Y"),$user->dob)." - ".$daysBefore." days"));
                        
                        if($yearOld != $yearNew) {
                            $code_obj->set("date_start", $yearNew); //Дата начала для использования промокода
                            $code_obj->set("date_end", date("d-m-Y",strtotime(date("d-m-".date("Y"),$user->dob)." + ".$daysAfter." days"))); //Дата начала для использования промокода
                            $code_obj->set("activity", 1); // активен
                            $code_obj->set("type", $type); //Тип 6 (на день рождение)
                            
                            $count = $code_obj->get("use_count");
                            $code_obj->set("use_count", $count++); // Суммарное количество использований
                            $code_obj->save();
                        }
                    }
                    
                }
            }
            
            
        }
        
        return true;
    }
    
    // метод генерации подарка на день рождения в виде промокода
    public function updatePromoReferal(){
        if(date("H:i") == "00:00"){
            $query = $this->modx->newQuery('PromoBase');
            $query->where(array(
                'code:LIKE' => 'U-%',
                'is_referal' => 1
            ));
            $codes = $this->modx->getCollection('PromoBase',$query);
            
            //$this->modx->log(1,"Обновление реферальных ссылок");
            
            foreach($codes as $code){
                
                /*
                - app_referal_count_user - пользователей на промокод (0 - ограничений нет)
                - app_referal_use_count - сколько применений промокода рефералом (1)
                - app_referal_time_used - время действия промокода в днях (0 - значит постоянно)
                - app_referal_type - тип промокода (1 - скидка в процентах, 2 - товар в подарок, 5 - скидка в рублях)
                - app_referal_type_value - дополнительный параметр для промокода
                - app_referal_bonus - реферальные баллы после использования промокода]]
                */
                
                // обновление параметров
                $code->set("type",$this->modx->getOption("app_referal_type")); // тип реферального промокода
                $code->set("use_count", $this->modx->getOption("app_referal_use_count")); // Суммарное количество использований
                $code->set("author_bonus", $this->modx->getOption("app_referal_bonus")); // Сколько бонусов будет начислено автору, если его промокод будет активирован
                // если указано количество дней, то рассчитываем время применения
                if($this->modx->getOption("app_referal_time_used")) $code->set("date_end", date("Y-m-d 00:00:00",strtotime(date("Y-m-d H:i:s")." + ".$this->modx->getOption("app_referal_type")." days")));
                // в зависимости от типа промокода подбираем значение
                switch($this->modx->getOption("app_referal_type")){
                    case 1:
                        $code->set("child_discount",$this->modx->getOption("app_referal_type_value")); // скидка в процентах
                        $code->set("id_product","");
                        $code->set("child_discount_rub",0);
                    break;
                    case 2:
                        $code->set("child_discount",0);
                        $code->set("id_product",$this->modx->getOption("app_referal_type_value"));  // ID товара в подарок
                        $code->set("child_discount_rub",0);
                    break;
                    case 5:
                        $code->set("child_discount",0);
                        $code->set("id_product","");
                        $code->set("child_discount_rub",$this->modx->getOption("app_referal_type_value"));  // скидка в рублях
                    break;
                }
                
                $code->save();
            }
        }
    }
    
    // необходимо что бы было настроено ображение через крон к данному методу
    public function removeOldSessions(){
        $checkTime = date("i");
        // проверяем время в минутах, если 00 или 30 минут, то запускаем скрипт
        if($checkTime == "00" || $checkTime == "30") {
            // формируем выборку элементов по метке корзины (minishop2)
            $query = $this->modx->newQuery('modSession');
            $query->where(array(
                'data:LIKE' => 'minishop2%',
            ));
            // получаем список объектов
            $sessions = $this->modx->getCollection('modSession',$query);
            // перебераем объекты
            foreach($sessions as $session){
                echo $session->id." - ".date("d-m-Y H:i:t",$session->access)."<br>";
                // если время сессии превысило 4 часа (14400 сек)
                if(((int)time() - (int)$session->access) > 14400) {
                    // то удаляем сессию
                    $session->remove();
                }
            }
        }
    }
    
}