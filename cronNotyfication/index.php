<?php
define('MODX_API_MODE', true);
require '../index.php';
// Включаем обработку ошибок
/** @var modX $modx */
$modx->getService('error','error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
//$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');

require 'api.class.php';
$api = new ModxApi($modx);
$api->run($_SERVER['REQUEST_URI']);
echo $api->getResponse();