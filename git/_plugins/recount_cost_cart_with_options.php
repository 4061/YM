<?php
if ($modx->event->name == 'msOnAddToCart') {
    
    $tmp = $cart->get();
    if(isset($tmp[$key]['options']) && isset($tmp[$key]['options']['childs'])){
        $childs = $tmp[$key]['options']['childs'][0];
        $price = $tmp[$key]['price'];
        
        foreach(json_decode($childs,JSON_UNESCAPED_UNICODE) as $prod){
            if(isset($prod["count"])){
                $count = $prod["count"];
            }else{
                $count = 1;
            }
            $price = $price + $prod["price"] * $count;
        }
        
        $tmp[$key]['price'] = $price;
        $cart->set($tmp);
    }
}