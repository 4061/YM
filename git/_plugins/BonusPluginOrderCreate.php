<?php
if ($modx->event->name == 'msOnChangeOrderStatus') {

    if((int)$status == 6){
        
        if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
            $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/')
        ) {
            return 'Could not load msBonus2 class!';
        }
        
        $msb2->initialize($modx->context->key);
        $manager = $msb2->getManager();
        $orderId = $order->get('id');
        $bonusWriteOff = $_SESSION['msbon_order'][$orderId];
        unset($_SESSION['msbon_order'][$orderId]);
        $manager->setOrderWriteoff($orderId, $bonusWriteOff);
        return $orderId.'msOnChangeOrderStatus'.$bonusWriteOff;
    }
}

if ($modx->event->name == 'msOnCreateOrder') {

    $orderId = $msOrder->get('id');
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $bonus = $_REQUEST['JSON']['bonus'];
    $_SESSION['msbon_order'][$orderId] = $bonus;
    return 'msOnCreateOrder';
}