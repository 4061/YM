<?php
switch ($modx->event->name) {
    case 'msOnManagerCustomCssJs':
        switch ($page) {
            case 'orders':
            $modx->controller->addLastJavascript(MODX_ASSETS_URL . 'components/minishop2/js/mgr/custom/orders.grid.products.js');
            $modx->controller->addLastJavascript(MODX_ASSETS_URL . 'components/minishop2/js/mgr/custom/orders.window.js');
            $modx->controller->addLastJavascript(MODX_ASSETS_URL . 'components/minishop2/js/mgr/custom/orders.grid.js');
            break;
        }
        break;
}