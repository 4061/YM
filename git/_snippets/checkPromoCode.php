<?php
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);
$cart = $miniShop2->cart->get(); //получаю корзину
$order = $miniShop2->order->get(); // получаю заказ
$status = $miniShop2->cart->status(); 
$code = $_GET['code']; //получаю код купона из инпута
$sql = "SELECT * FROM modx_promo_base WHERE modx_promo_base.code='$code'"; //получение таблицы
$info_promo= $modx->query($sql);
$info_promo = $info_promo->fetchAll(PDO::FETCH_ASSOC); //инфа об этом купоне
if ($info_promo) //если купон есть
{   
    if ($info_promo[0]['activity']){ // активен ли код
        switch ($info_promo[0]['availability']){ //switch проверяет доступность. Если 1, то значит акция доступна всем людям. Если 2, значит только определенному кругу лиц.
        case 1: //если доступно всем
            $use_count = $info_promo[0]['use_count'];
            if ($use_count){ //Если купон еще не израсходован
                $use_count--;
                $changecount_sql ="UPDATE modx_promo_base SET use_count ='$use_count' WHERE modx_promo_base.code='$code'"; //меняем в БД use_count. 
                $modx->query($changecount_sql);
                $use_count_in_check = $info_promo[0]['use_count_in_check'];
                $add_rights = true; //по умолчанию права на добавление true
                foreach ($cart as $cartItem){
                    if ($info_promo[0]['id_product'] == $cartItem['id']){
                        if ($cartItem['count'] < $use_count_in_check){ //если количество товара в корзине меньше количества, которое может быть в корзине, то права на добавление true
                            $add_rights = true;
                        } else { //если нет, то false
                            $add_rights = false;
                        }
                    }
                }
                if ($add_rights){ // если права на добавление true
                    $user_id = $modx->user->id; //получаем айди пользователя
                    $promo_id = $info_promo[0]['id_promo']; //получаем айди промокода
                    $userCountUse = $modx->query("SELECT * FROM modx_promo_users WHERE user_id='$user_id' AND promo_id = '$promo_id'");
                    $userCountUse = $userCountUse->fetchAll(PDO::FETCH_ASSOC);
                    $userCountUse = count($userCountUse);
                    if ($userCountUse){
                        if ($info_promo[0]['user_use_count'] > $userCountUse ) { 
                            $modx->query("INSERT INTO `modx_promo_users` (`user_id`, `promo_id`, `use_count`, `use_time`) VALUES ('$user_id', '$promo_id', '1', CURRENT_TIMESTAMP)"); //записываем использование
                            $miniShop2->cart->add($info_promo[0]['id_product']); //добавляем промотовар
                            $info_promo[1] = array('response' => 1, 'response_text' => 'Вы успешно активировали промокод');
                        } else {
                            $info_promo[1] = array('response' => 3, 'response_text' => 'Вы уже активировали этот промокод 1');
                        }
                    } else {
                        $modx->query("INSERT INTO `modx_promo_users` (`user_id`, `promo_id`, `use_count`, `use_time`) VALUES ('$user_id', '$promo_id', '1', CURRENT_TIMESTAMP)"); //записываем использование
                        $miniShop2->cart->add($info_promo[0]['id_product']);
                        $info_promo[1] = array('response' => 1, 'response_text' => 'Вы успешно активировали промокод 2');
                    }
                } else {
                    $info_promo[1] = array('response' => 2, 'response_text' => 'Достигнут лимит использования промокода в чеке');
                }

                
                echo json_encode($info_promo); //воззврвщаем ответ от сервера на фронт
            } else {
                    $info_promo[1] = array('response' => 0, 'response_text' => 'Акция закончилась');
                    echo json_encode($info_promo); //воззврвщаем ответ от сервера на фронт
            }
            break;
        case 2: //если выборочным людям, пока не кодю этот модуль
            $user_id = $modx->user->id; //получаю user id
            $promo_id = $info_promo[0]['id_promo']; //получаем айди промокода
            $secret_base = $modx->query("SELECT * FROM modx_promo_users_secret WHERE id_user='$user_id' AND id_promo ='$promo_id'");
            $secret_base = $secret_base->fetchAll(PDO::FETCH_ASSOC);
            if ($secret_base){ //если юзер есть в секретной базе
                $use_count = $info_promo[0]['use_count'];
                if ($use_count){ //Если купон еще не израсходован
                    $use_count--;
                    $changecount_sql ="UPDATE modx_promo_base SET use_count ='$use_count' WHERE modx_promo_base.code='$code'"; //меняем в БД use_count. 
                    $modx->query($changecount_sql);
                    $use_count_in_check = $info_promo[0]['use_count_in_check'];
                    $add_rights = true; //по умолчанию права на добавление true
                    foreach ($cart as $cartItem){
                        if ($info_promo[0]['id_product'] == $cartItem['id']){
                            if ($cartItem['count'] < $use_count_in_check){ //если количество товара в корзине меньше количества, которое может быть в корзине, то права на добавление true
                                $add_rights = true;
                            } else { //если нет, то false
                                $add_rights = false;
                            }
                        }
                    }
                    if ($add_rights){ // если права на добавление true
                        $user_id = $modx->user->id; //получаем айди пользователя
                        $promo_id = $info_promo[0]['id_promo']; //получаем айди промокода
                        $userCountUse = $modx->query("SELECT * FROM modx_promo_users WHERE user_id='$user_id' AND promo_id = '$promo_id'");
                        $userCountUse = $userCountUse->fetchAll(PDO::FETCH_ASSOC);
                        $userCountUse = count($userCountUse);
                        if ($userCountUse){
                            if ($info_promo[0]['user_use_count'] > $userCountUse ) { 
                                $modx->query("INSERT INTO `modx_promo_users` (`user_id`, `promo_id`, `use_count`, `use_time`) VALUES ('$user_id', '$promo_id', '1', CURRENT_TIMESTAMP)"); //записываем использование
                                $miniShop2->cart->add($info_promo[0]['id_product']); //добавляем промотовар
                                $info_promo[1] = array('response' => 1, 'response_text' => 'Вы успешно активировали промокод');
                            } else {
                                $info_promo[1] = array('response' => 3, 'response_text' => 'Вы уже активировали этот промокод 1');
                            }
                        } else {
                            $modx->query("INSERT INTO `modx_promo_users` (`user_id`, `promo_id`, `use_count`, `use_time`) VALUES ('$user_id', '$promo_id', '1', CURRENT_TIMESTAMP)"); //записываем использование
                            $miniShop2->cart->add($info_promo[0]['id_product']);
                            $info_promo[1] = array('response' => 1, 'response_text' => 'Вы успешно активировали промокод 2');
                        }
                    } else {
                        $info_promo[1] = array('response' => 2, 'response_text' => 'Достигнут лимит использования промокода в чеке');
                    }
    
                    
                    echo json_encode($info_promo); //воззврвщаем ответ от сервера на фронт
                } else {
                    $info_promo[1] = array('response' => 0, 'response_text' => 'Акция закончилась');
                    echo json_encode($info_promo); //воззврвщаем ответ от сервера на фронт
                }
            } else {
                    $info_promo[1] = array('response' => 0, 'response_text' => 'Эта акция для Вас недоступна');
                    echo json_encode($info_promo); //воззврвщаем ответ от сервера на фронт
            }
            break;
        }
    } else {
        $info_promo[1] = array('response' => 0, 'response_text' => 'Промокод не активен');
        echo json_encode($info_promo);
    }
} else { //если нет
        $info_promo[1] = array('response' => 0, 'response_text' => 'Промокод недействителен');
        echo json_encode($info_promo);
}