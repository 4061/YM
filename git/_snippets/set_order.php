<?php
global $arr2,$arr,$products,$notFound,$modifiers;
function search_catalog($item, $parent)         // обрабатываем данные и переносим в массив $arr
{
    global $arr;
    if (array_key_exists('id', $item)) {
        $id = $item['id'];
        $name = $item['name'];
        $arr[$id]['id'] = $id;
        $arr[$id]['name'] = $name;
        $arr[$id]['parent'] = $parent;
        if (array_key_exists('items', $item)) {
            if (count($item['items'])) {
                search_catalog($item['items'], $id);
            }
        }
    } else {
        if (count($item)) {
            foreach ($item as $itemList) {
                search_catalog($itemList, $parent);
            }
        }
    }

}
function getProducts($item, $parent)  		 	// поиск товаров
{
    global $arr2;
    if (array_key_exists("id", $item)) {
        $id = $item['id'];
        $name = $item['name'];
        $arr2[$id]['id'] = $id;
        $arr2[$id]['name'] = $name;
        $arr2[$id]['parent'] = $parent;
        if (array_key_exists("dishes", $item)) {
            makeProducts($item['dishes'], $id);
        }
        if (array_key_exists("items", $item)) {
            if (count($item['items'])) {
                getProducts($item['items'], $id);
            }
        }
    } else {
        if (count($item)) {
            foreach ($item as $itemList) {
                getProducts($itemList, $parent);
            }
        }
    }

}
function makeProducts($items, $id)     // создание товара
{
    global $modx, $products, $notFound,$modifiers;
    foreach ($items as $item) {
        //$query = $modx->newQuery('modResource');
        $query = array('introtext' => $id);
        $category = $modx->getObject('modResource', $query);
        if ($category) {
            $parent = $category->get('id');
        } else {
            echo "не найдена родительская группа для " . $item;
            return;
        }
        if ( !array_key_exists($item, $products) ) {
            $notFound[] = $item;
        } else {
                $query = array('article' => $item);
                $prod = $modx->getObject('msProductData', $query);
                if (is_object($prod)) {
                    return;
                }else{
                    makeProducts($item['dishes'], $id);
                    $pub = '1';
                    if ($products[$item]['price'] <= 0) {
                        $pub = '0';
                        } else {
                            $products[$item]['price'] = $products[$item]['price']/100;
                        }
                    $response = $modx->runProcessor('resource/create', array(
                        'class_key' => 'msProduct',
                        'pagetitle' => $products[$item]['name'],
                        'article' => $item,
                        'introtext' => (string)$products[$item]['modischeme'],
                        'price' => $products[$item]['price'],
                        'parent' => $parent,
                        'template' => 4,
                        'published' => $pub,
                        'alias' => bin2hex(openssl_random_pseudo_bytes(16))
                    ));
                            if ($products[$item]['modischeme'] != '0') {
                                //echo "modischeme";
                                $par = array('article' => $item);
                                $get_prod_id = $modx->getObject('msProductData', $par);   
                                $get_prod_id = $get_prod_id->get('id');
                                foreach ($modifiers[$products[$item]['modischeme']]['modifiers'] as $modificator)    {
                                    $pr = array('article' => $modificator['id']);
                                    $get_id = $modx->getObject('msProductData', $pr);   
                                     if (!is_object($get_id)) {
                                       // echo "зашли в modischeme";
                                        $query = array('pagetitle' => 'Модификаторы');
                                        $mods = $modx->getObject('modResource', $query);
                                        if (is_object($mods)) $modificators_id = $mods->get('id');
                                        //echo "получили id группы модификаторов";
                                      if ($modificator['price'] > 0) {
                                            $price = $modificator['price']/100;
                                      } else {
                                            $price = $modificator['price'];
                                      }
                                        $response = $modx->runProcessor('resource/create', array(
                                                'class_key' => 'msProduct',
                                                'pagetitle' => $modificator['name'],
                                                'article' => $modificator['id'],
                                                'price' => $price,
                                                'parent' => $modificators_id,
                                                'template' => 1,
                                                'published' => '1',
                                                'alias' => bin2hex(openssl_random_pseudo_bytes(16))
                                        ));
                                        if ($response) {
                                            save_options($get_prod_id, $modificator['id']);   
                                        }
                                     }    
                                }
                            }
                }
            if ($response->isError()) {
                $modx->log(modX::LOG_LEVEL_ERROR, "Error on : \n" . print_r($response->getAllErrors(), 1));
                echo $response->getAllErrors();
            } else {
                //return $response->response['object']['id'];
            }
        }


    }
}
function createCategory($arr)       // создаем категории
{
    global $modx;

    // $query = $modx->newQuery('modResource');
    $query = array('introtext' => $arr['id']);
    $category = $modx->getObject('modResource', $query);
    if ($category) {
        return $category->get('id');
    }

    if ($arr['parent'] == 0) {
        $parent = 2;
    } else {
        //$query = $modx->newQuery('modResource');
        $query = array('introtext' => $arr['parent']);
        $category = $modx->getObject('modResource', $query);
        if ($category) {
            $parent = $category->get('id');
        } else {
            echo "не найдена родительская группа для " . $arr['name'];
            return;
        }
    }

$query = array('alias' => (string)$arr['id']);
    $prod = $modx->getObject('modResource', $query);
        if (is_object($prod)) {
            return;
        }else{
            $response = $modx->runProcessor('resource/create', array(
                'class_key' => 'msCategory',
                'pagetitle' => $arr['name'],
                'longtitle' => $arr['parent'],
                'introtext' => (string)$arr['id'],
                'parent' => $parent,
                'template' => 1,
                'published' => 1,
                'alias' => (string)$arr['id']
            ));
        }
    if ($response->isError()) {
        $modx->log(modX::LOG_LEVEL_ERROR, "Error on : \n" . print_r($response->getAllErrors(), 1));
        echo $response->getAllErrors();
    } else {
            $cat_id = $modx->getObject('modResource', array('alias'=>(string)$arr['id']));
            $cat_id = $cat_id->get('id');
            $check = $modx->getObject('msCategoryOption', array('category_id'=>$cat_id));
            if (!is_object($check)) {
                $new_cat = $modx->newObject('msCategoryOption');
                $new_cat->set('option_id',27);              //  ПРИ ПЕРЕНОСЕ ИЗМЕНИТЬ !!! ВЫТАЩИТЬ ID ПО НАЗВАНИЮ КЛЮЧА - ms2_rk_modificators
                $new_cat->set('category_id', $cat_id);
                $new_cat->set('rank',0);
                $new_cat->set('active',1);
                $new_cat->set('required',0);
                $new_cat->set('value',NULL);
                $new_cat->save();
            }
        $return = $response->response['object']['id'];
    }
    
    
    
$query = array('pagetitle' => 'Модификаторы');
    $mods = $modx->getObject('modResource', $query);
        if (is_object($mods)) {
            return $return;
        }else{
            $response = $modx->runProcessor('resource/create', array(
                'class_key' => 'msCategory',
                'pagetitle' => 'Модификаторы',
                'parent' => '2',
                'template' => 1,
                'published' => 1,
                'alias' => 'modifiers'
            ));
        }
    if ($response->isError()) {
        $modx->log(modX::LOG_LEVEL_ERROR, "Error on : \n" . print_r($response->getAllErrors(), 1));
        echo $response->getAllErrors();
    } else {
        return $return;
    }
    
    

}
function get_curl ($method)         // запросы к API
{
    global $modx;
$sid = $modx->getOption('ms2_sid');
$ojectid = $modx->getOption('ms2_object_rkeeper');
$rk_url = $modx->getOption('ms2_rkeeper_url');
$vars = array();
$headers = array("sid:$sid",
                 "Content-Type: application/json");
$ch = curl_init($rk_url . $method .'?objectid=' . $ojectid . '&lang=ru' . http_build_query($vars));       
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($res, true);
        return $res; 
}
function save_options($product_id,$value) 
{
    global $modx;
$sql = "SELECT * FROM `modx_ms2_product_options` WHERE `product_id` = '$product_id' AND `key` = 'ms2_rk_modificators' ORDER BY `product_id` ASC";
$query = $modx->query($sql);
$query = $query->fetchAll(PDO::FETCH_ASSOC); 
if ($query[0]) {
    if ($query[0]['value'] == '') {
    $val = $value;    
} else {
    $val = $query[0]['value'];
    $val .= ';'.$value;
}

$sql = "UPDATE `modx_ms2_product_options` SET `value` = '$val' WHERE `modx_ms2_product_options`.`id` = ".$query[0]['id'];
if ($query = $modx->query($sql)) return true;
    } else {
        $sql = "INSERT INTO `modx_ms2_product_options` (`id`, `product_id`, `key`, `value`) VALUES (NULL, '$product_id', 'ms2_rk_modificators', '$value');";
        $query = $modx->query($sql);
        $query = $query->fetchAll(PDO::FETCH_ASSOC); 
    }
}

$notFound = [];
$menu = get_curl('menutree');
$root = $menu["data"]["selectors"][0];
$arr = [];
$arr2 = [];

$id = $root['id'];
$name = $root['name'];
$parent = 0;

$arr[$id]['id'] = $id;
$arr[$id]['name'] = $name;
$arr[$id]['parent'] = 0;





$prod = get_curl('dishes');        
$products = [];
foreach ($prod['data']['dishes'] as $product) {
    $products[$product['id']]['id'] = $product['id'];
    $products[$product['id']]['name'] = $product['name'];
    $products[$product['id']]['price'] = $product['price'];
    $products[$product['id']]['modischeme'] = $product['modischeme'];
}

$modifiers = [];
foreach ($prod['data']['modifiers'] as $modi) {
    foreach ($modi['group'] as $group) {
        if (array_key_exists('id', $group)) {
            $modifiers[$modi['id']]['group_id'] = $modi['id'];    // id группы идентификаторов
            $modifiers[$modi['id']]['modifiers'] = array();
            foreach ($group['modi'] as $mod)        // id непосредственно иждентификаторов
            {
                $modifiers[$modi['id']]['modifiers'][] = array (
                    'price' => $mod['price'],
                    'id' => $mod['id'],
                    'name' => $mod['name']
                    ) ;
            }
        }
    }    
}


//print_r($root['items']);

search_catalog($root['items'], $id);

foreach ($arr as $category) {
  createCategory($category);
}


getProducts($root['items'], $id);