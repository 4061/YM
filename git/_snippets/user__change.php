<?php
/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
*/

if ('application/json' == $_SERVER['CONTENT_TYPE']
 && 'POST' == $_SERVER['REQUEST_METHOD'])
{
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    if ($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) {
    $token = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
    $token = substr($token, 7);
    }

   //$test = json_decode($test);
    $_POST['JSON'] = & $_REQUEST['JSON'];
    if($token) {
    
    $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
    


    if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
        
        $birthday = $_POST['JSON']['birthday'];
        $birthday = strtotime($birthday);

        $gender = $_POST['JSON']['gender'];
        if ($gender == "Мужской"){
            $gender=1;
        } elseif ($gender == "Женский"){
            $gender=2;
        }
        $profile->set('fullname',$_POST['JSON']['name']);
        $profile->set('email',$_POST['JSON']['email']); 
        $profile->set('gender', $gender);
        if ($birthday){
            $profile->set('dob', $birthday);
        }
        $profile->save(); //сохранили, осталось вернуть норм массив

        $phone = $profile->get('mobilephone');
$sql = "SELECT *
FROM modx_msbonus2_users, modx_user_attributes, modx_msbonus2_levels
WHERE modx_user_attributes.mobilephone = '$phone' and modx_msbonus2_users.user = modx_user_attributes.internalKey and modx_msbonus2_users.level = modx_msbonus2_levels.id";
$info_user= $modx->query($sql);
$info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
if ($info_user) {
$bonus = $info_user[0]['points'];
$bonus_persent = $info_user[0]['bonus'];
$paid_money = $info_user[0]['paid_money'];
$level = $info_user[0]['level'];
$level_name = $info_user[0]['name'];
$sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = $level + 1 or id = $level ORDER BY id DESC LIMIT 1";
if ($modx->query($sql2)) {
$next_level = $modx->query($sql2);
$next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
//$next_level = '{"id":"'.$next_level[0]['id'].'","bonus":"'.$next_level[0]['bonus'].'","cost":"'.$next_level[0]['cost'].'","name":"'.$next_level[0]['name'].'"}';
$next_level = array(
        'id'=> $next_level[0]['id'],
        'bonus'=> $next_level[0]['bonus'],
        'cost'=> $next_level[0]['cost'],
        'name'=> $next_level[0]['name']
    );
}
$cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');

} else {
    $bonus = '0';
    $bonus_persent = '0';
    $cart_maximum_percent = $modx->getOption('msb2_cart_maximum_percent');
    $level = '1';
    $sql2 = "SELECT id,bonus,cost,name FROM modx_msbonus2_levels WHERE id = '2'";
if ($modx->query($sql2)) {
$next_level = $modx->query($sql2);
$next_level = $next_level->fetchAll(PDO::FETCH_ASSOC);
$level_name = $next_level[0]['name'];
$next_level = array(
        'id'=> $next_level[0]['id'],
        'bonus'=> $next_level[0]['bonus'],
        'cost'=> $next_level[0]['cost'],
        'name'=> $next_level[0]['name']
    );
//$next_level = '{"id":"'.$next_level[0]['id'].'","bonus":"'.$next_level[0]['bonus'].'","cost":"'.$next_level[0]['cost'].'","name":"'.$next_level[0]['name'].'"}';
}
    $paid_money = '0';
}
        $gender = $profile->get('gender');
        $birthday = $birthday = date("Y-m-d\TH:i:s",$profile->get('dob'));
        if ($birthday == '01.01.1970'){
            $birthday = 'Не указан';
        }
        if ($gender == 2) {
            $gender = 'Женский';
        } elseif ($gender == 1){
            $gender = 'Мужской';
        } elseif ($gender == 0){
            $gender = 'Не указан';
        }
        $json['data'][]=[
            "name" => $profile->get('fullname'),
            "email" => $profile->get('email'),
            "bonus" => $bonus,
            "bonus_persent" => $bonus_persent,
            "cart_maximum_percent" => $cart_maximum_percent,
            "level" => $level,
            "gender" => $gender,
            "birthday" => $birthday,
            "next_level" => $next_level,
            "level_name" => $level_name,
            "paid_money" => $paid_money,
            "phone" => '+'.$phone
            
        ];
        return json_encode($json);
    }        
        
        
}  else {
echo '
{
    "data": [
        {"error":"Нет Токена"}
    ]
}';      
http_response_code(401);
}

    
} else {
echo '
{
    "data": [
        {"error":"Ошибка формата"}
    ]
}';
http_response_code(400);
}