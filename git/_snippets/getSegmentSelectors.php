<?php
$sql = "
SELECT 
    modx_segment_segments.segment_id,
    modx_segment_segments.settings_id,
    modx_segment_segments.segment_createdon,
    modx_segment_segments.segment_name,
    modx_segment_settings.segment_settings
FROM 
    modx_segment_segments,modx_segment_settings
WHERE
    modx_segment_segments.settings_id = modx_segment_settings.segment_id
ORDER BY 
    segment_id DESC ";
$segment_ids= $modx->query($sql);
$segment_ids= $segment_ids->fetchAll(PDO::FETCH_ASSOC);
return ($segment_ids);