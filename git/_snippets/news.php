<?php
$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);

$sql = "SELECT content.id,content.pagetitle,content.parent,content.content
FROM modx_site_content content
WHERE content.deleted = 0 and content.published = 1 and content.parent = '8'";
$news__sql = $modx->query($sql);
$news = $news__sql->fetchAll(PDO::FETCH_ASSOC);

//print_r($news);

$news__json = [];

foreach ($news as $news__one) {

    $sql = "SELECT value
    FROM modx_site_tmplvar_contentvalues
    WHERE tmplvarid = 2 and contentid = " . $news__one['id'];
    $img__str= $modx->query($sql);
    $img = $img__str->fetchAll(PDO::FETCH_ASSOC);

    $sql = "SELECT value
    FROM modx_site_tmplvar_contentvalues
    WHERE tmplvarid = 9 and contentid = " . $news__one['id'];
    $id__tovar__str= $modx->query($sql);
    $product_id = $id__tovar__str->fetchAll(PDO::FETCH_ASSOC);


    $content =  str_replace(array("\r\n", "\r", "\n"), ' ',  strip_tags($news__one['content']));
    
    $news__json[] = '
        {
        "id":"'.$news__one['id'].'",
        "title":"'.$news__one['pagetitle'].'",
        "content":"'.$content.'",
        "product_id":"'.$product_id[0]['value'].'",        
        "imgUrl":"/'.$img[0]['value'].'"
        }
    ';
}


echo '
{
    "data": [
    '.implode(",", $news__json).'
    ]
}';