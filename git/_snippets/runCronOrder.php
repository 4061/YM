<?php
$q = $modx->prepare("SELECT id,status FROM modx_ms2_orders where (status = 6 or status = 2) and createdon <= (NOW() - INTERVAL 24 HOUR)");
$q->execute();
$orders = $q->fetchAll(PDO::FETCH_ASSOC);

$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key, $scriptProperties);

foreach($orders as $order){
    if($order['status'] == 6){
        $miniShop2->changeOrderStatus($order['id'], 4);
    }
    if($order['status'] == 2){
        $miniShop2->changeOrderStatus($order['id'], 7);
    }
}