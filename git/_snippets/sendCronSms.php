<?php
$sql = "SELECT * FROM `modx_messages_cron`";
$cron_segments= $modx->query($sql);
$cron_segments = $cron_segments->fetchAll(PDO::FETCH_ASSOC);
$minutes = date('i');
$hours = date('H');
$day = date('d');
$today = date('N');
foreach ($cron_segments as $segment){
    if ($segment['Activity'] == 1){
        $segment_id = $segment['segment_id'];
        $message_id = $segment['message_id'];
        $user_id = $segment['user_id'];
        $cronid = $segment['id'];
        $hourval = 0;
        if ($segment['hours']){
            $segmenthours = explode(",", $segment['hours']);
            if ($segmenthours[0]=='*'){
                foreach ($segmenthours as $segmenthour){
                    if ($hours == $segmenthour){
                        $hourval = true;
                    }
                }
            } else {
                foreach ($segmenthours as $segmenthour){
                    if (($hours % $segmenthour)==0){
                        $hourval = true;
                    }
                }
            }
        } else {
           $hourval = true; 
        }

        $minutesval = 0;
        if ($segment['mins']){
            $segmentmins = explode(",", $segment['mins']);
            if ($segmentmins[0]=='*'){
                foreach ($segmentmins as $segmentmin){
                    if ($minutes == $segmentmin){
                        $minutesval = true;
                    }
                }
            } else {
                foreach ($segmentmins as $segmentmin){
                    if (($minutes % $segmentmin)==0){
                        $minutesval = true;
                    }
                }
            }
        } else {
            $minutesval = true;
        }
        $daysval = 0;
        if ($segment['days']){
            $segmentdays = explode(",", $segment['days']);
            if ($segmentdays[0]=='*'){
                foreach ($segmentdays as $segmentday){
                    if ($day == $segmentday){
                        $daysval = true;
                    }
                }
            } else {
                foreach ($segmentdays as $segmentday){
                    if (($day % $segmentday)==0){
                        $daysval = true;
                    }
                }
            }    
        } else {
            $daysval = true;
        }
        $weekdaysval = 0;
        if ($segment['weekdays']){
            $segmentweekdays = explode(",", $segment['weekdays']);
            foreach ($segmentweekdays as $segmentweekday){
                if ($today == $segmentweekday){
                    $weekdaysval = true;
                }
            }
        } else {
            $weekdaysval = true;
        }
        echo("MINUTES: ".$minutesval);
        echo("HOURS: ".$hourval);
        echo("DAYS: ".$daysval);
        echo("WEEKDAYS: ".$weekdaysval);
        echo('</br>');
        if($hourval && $minutesval && $daysval && $weekdaysval ){
            $sql = "
            UPDATE 
                modx_messages_cron
            SET 
                last_send = CURRENT_TIMESTAMP
            WHERE 
                id = '$cronid'
            ";
            $modx->query($sql);
            $sql = "
            INSERT INTO 
                modx_messages_logs
                (
                    `segment_id`,
                    `message_id`,
                    `user_id`
                )
                VALUES
                (
                    '$segment_id',
                    '$message_id',
                    '$user_id'
                )
            ";
            $modx->query($sql);
            $sql = "SELECT * FROM `modx_messages_logs` ORDER BY `modx_messages_logs`.`id` desc";
            $messages_base= $modx->query($sql);
            $messages_base = $messages_base->fetchAll(PDO::FETCH_ASSOC);
            //return json_encode($messages_base);
            $id = $messages_base[0]['id'];
            $segment_id=$messages_base[0]['segment_id'];
            $message_id=$messages_base[0]['message_id'];
            $sql="
            SELECT 
                modx_segment_base.user_id,
                modx_messages_logs.message_id,
                modx_messages_blanks.message,
                modx_messages_logs.channel,
                modx_messages_logs.createdon
            FROM
                modx_segment_base,modx_messages_blanks,modx_messages_logs
            WHERE
                modx_messages_blanks.message_id = '$message_id' AND
                modx_segment_base.segment_id='$segment_id' AND
                modx_messages_logs.id = '$id'
                
                
            ";
            $messages_new_users= $modx->query($sql);
            $messages_new_users = $messages_new_users->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($messages_new_users);
            foreach ($messages_new_users as $new_user){
                $user_id = $new_user['user_id'];
                $message_id = $new_user['message_id'];
                $message = $new_user['message'];
                $channel = $new_user['channel'];
                $createdon = $new_user['createdon'];
                $sql = "
                INSERT INTO 
                    modx_messages_base
                    (
                        `user_id`, 
                        `message_id`,
                        `message`,
                        `createdon`,
                        `channel`
                    )
                    VALUES
                    (
                        '$user_id',
                        '$message_id',
                        '$message',
                        '$createdon',
                        '$channel'
                    )
                ";
                $modx->query($sql);
            }
    
        }
    }
}
/*Отправка*/
        function send($host, $port, $login, $password, $phone, $text, $sender = false, $wapurl = false ){
          $fp = fsockopen($host, $port, $errno, $errstr);
          if (!$fp) {
            return "errno: $errno \nerrstr: $errstr\n";
          }
          fwrite($fp, "GET /send/" .
            "?phone=" . rawurlencode($phone) .
            "&text=" . rawurlencode($text) .
            ($sender ? "&sender=" . rawurlencode($sender) : "") .
            ($wapurl ? "&wapurl=" . rawurlencode($wapurl) : "") .
            " HTTP/1.0\n");
          fwrite($fp, "Host: " . $host . "\r\n");
          if ($login != "") {
            fwrite($fp, "Authorization: Basic " . 
              base64_encode($login. ":" . $password) . "\n");
          }
          fwrite($fp, "\n");
          $response = "";
          while(!feof($fp)) {
            $response .= fread($fp, 1);
          }
          fclose($fp);
          list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
          return $responseBody;
        }
        $sql = "SELECT * FROM modx_messages_base WHERE createdon >= DATE_ADD(CURDATE(), INTERVAL -12 HOUR)";
        $sms_users= $modx->query($sql);
        $sms_users = $sms_users->fetchAll(PDO::FETCH_ASSOC);
        foreach ($sms_users as $user){
            if ($user['status'] == 0){
                $current_user = $modx->getObject('modUserProfile', array('internalKey' => $user['user_id'])); 
                $mobilephone = $current_user->get('mobilephone');
                //send($modx->getOption('ms2_sms_gate'), 80, $modx->getOption('ms2_sms_login'), $modx->getOption('ms2_sms_password'), $mobilephone, $user['message']);
                $id = $user['id'];
                $sql = "UPDATE modx_messages_base SET status = 1, datesend = CURRENT_TIMESTAMP WHERE modx_messages_base.id = '$id'";
                $modx->query($sql);
            }
        }