<?php
$pageId= 1;

$resource = $modx->getObject('modResource', $pageId);

$modx->log(modX::LOG_LEVEL_ERROR,'reviews '.$pageId);

//$modx->log($pageId);

$input  = $resource->getTVValue('reviews');
$modx->log(modX::LOG_LEVEL_ERROR,'reviews'.$input);
if($input !='')
$res = $modx->fromJSON($input);
else
$res = array();
$count = count($res);
$modx->log(modX::LOG_LEVEL_ERROR,'reviews'.$count);

$new = array(
    'MIGX_id'       => $count + 1,
    'title'          => $hook->getValue('fio'),
    'email'          => $hook->getValue('email'),   
    'social'         => $hook->getValue('social'),
    'text'       => $hook->getValue('message'),
    'image_big'       => '',
    'yes_no'        =>'0'
);
$res[] = $new;
 
if (!$resource->setTVValue('reviews', $modx->toJson($res))) {
  $modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your data!');
  return true;
}


return true;