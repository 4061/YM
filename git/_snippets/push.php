<?php
/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
*/

$title =  $modx->resource->get('longtitle');
$body =  $modx->resource->get('description');

if ($title == '') {
echo "Заполните заголовок Push рассылки. <b>Расширенный заголовок</b> - в документе </br>";    
}

if ($body == '') {
echo "Заполните текст Push рассылки. <b>Описание</b> - в документе </br>";      
}


if ($title != '' && $body != '')
{

$token = $modx->getOption('ms2_push_token');
$url = 'https://fcm.googleapis.com/fcm/send';
$data = array(
    "to" => "/topics/promo",
    "notification" => array(
        "data" => "",
        "title" => $title,
        "body" => $body
        )
    );

function dadata($url, $data,$token) {
    $options = [
        'http' => [
            'method'  => 'POST',
            'header'  => [
                'Content-type: application/json',
                'Authorization: key=' . $token,
                
            ],
            'content' => json_encode($data),
        ],
    ];
    $context = stream_context_create($options);
    $contents = file_get_contents($url, false, $context);
    return $contents;
}

$massege =  dadata($url, $data,$token);
echo "{$massege}</br>";
if ($massege != '') {
   $massege_a =  json_decode($massege);
   
   if ($massege_a->message_id != '')
   echo "Собщение успешно отправлено";
}

}