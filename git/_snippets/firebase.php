<?php
if ('application/json' == $_SERVER['CONTENT_TYPE']
 && 'POST' == $_SERVER['REQUEST_METHOD'])
{
    $_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    $_POST['JSON'] = & $_REQUEST['JSON'];


  if($_POST['JSON']['token']) {
      
    $token = $_POST['JSON']['token'];
    
    
    if ($profile = $modx->getObject('modUserProfile', ['website' => $token])) {
     
    $profile->set('city',$_POST['JSON']['fcm_token']); 
    $profile->save(); 

    echo '
        {
            "data": [
                    {}
            ]
        }';
     
        
    }  else {
    echo '
    {
        "data": [
            {"error":"Пользователь не найден"}
        ]
    }';      
    }
    
    }  else {
    echo '
    {
        "data": [
            {"error":"Нет Токена"}
        ]
    }';      
    }
    
} else {
    echo '

{
    "data": [
     {"error":"Ошибка формата"}
    ]
}    
    
    ';    
}