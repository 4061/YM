<?php
$segment_id = $_GET['segment_id'];
if (!$segment_id){
    $birthday_1 = $_GET['birthday_1'];
    $birthday_2 = $_GET['birthday_2'];
    $min_dob = $_GET['min_dob'];
    $max_dob = $_GET['max_dob'];
    $last_buy1 = $_GET['last_buy1'];
    $last_buy2 = $_GET['last_buy2'];
    $date1 = $_GET['date_1'];
    $date2 = $_GET['date_2'];
    $min_cartcost = $_GET['min_cartcost'];
    $max_cartcost = $_GET['max_cartcost'];
    $gender = $_GET['gender'];
    $max_avg = $_GET['max_avg']; //максимальный средний чек
    $min_avg = $_GET['min_avg']; //минимальный средний чек
    $lastdays = $_GET['days']; //за сколько последних дней брать информацию
    $min_sum = $_GET['min_sum']; //минимальная сумма покупок от (price)
    $max_sum = $_GET['max_sum']; //максимальная сумма покупок до (price)
    $min_count = $_GET['min_count']; //минимальное количество заказов
    $max_count = $_GET['max_count']; //максимальное количество заказов
    $products = array( //покупки определенных продуктов
        "1" => $_GET['product_1'], 
        "2" => $_GET['product_2'],
        "3" => $_GET['product_3'],
        "4" => $_GET['product_4']
    );
    $product_keys_placeholder = '';
    foreach ($products as $product){
        if ($product){
            $product_keys_placeholder = $product_keys_placeholder." AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
        }
    }
    if ($birthday_1){
        $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
    }
    if ($birthday_2){
        $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
    }
    if ($min_dob>0 or $max_dob>0){
        $dobswither = " AND dob <> 0";
        $modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
    }
    if ($min_dob){
        $min_dob = $min_dob*365*24*60*60;
        $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
    }
    if ($max_dob){
        $max_dob = $max_dob*365*24*60*60;
        $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
    }
    if ($last_buy1){
        $last_buy1_placeholder = " AND time_end <= DATE_ADD(CURDATE(), INTERVAL -'$last_buy1' DAY)";
    }
    if ($last_buy2){
        $last_buy2_placeholder = " AND time_end >= DATE_ADD(CURDATE(), INTERVAL -'$last_buy2' DAY)";
    }
    if ($date1){
        $date1_placeholder = " AND modx_ms2_orders.createdon >= '$date1'";
    }
    if ($date2){
        $date2_placeholder = " AND modx_ms2_orders.createdon <= '$date2'";
    }
    if ($gender){
        $gender_placeholder = " AND modx_segment_users.user_id IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
    }
    if ($max_cartcost>0){
        $max_cartcost_placeholder = "AND modx_ms2_orders.cart_cost<='$max_cartcost'";
    }
    if ($min_cartcost>0){
        $min_cartcost_placeholder = " AND modx_ms2_orders.cart_cost>='$min_cartcost'";
    }
    if ($min_sum>0){
        $min_sum_placeholder = " AND ord_sum>='$min_sum'";
    }
    if ($max_sum>0){
        $max_sum_placeholder = " AND ord_sum<='$max_sum'";
    }
    if ($min_count>0){
        $min_count_placeholder = " AND ord_count>='$min_count'";
    }
    if ($max_count>0){
        $max_count_placeholder = " AND ord_count<='$max_count'";
    }
    if ($min_avg>0){
        $min_avg_placeholder = " AND ord_avg>='$min_avg'";
    }
    if ($max_avg>0){
        $max_avg_placeholder = " AND ord_avg<='$max_avg'";
    }
    if ($lastdays){
        $lastdays_placeholder = " AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'$lastdays' DAY)";
    }
    $sql = "
    SELECT 
        modx_segment_users.user_id as user_id,
        modx_segment_users.user_name as user_name,
        modx_user_attributes.email as email,
        modx_user_attributes.mobilephone as mobilephone,
        modx_segment_users.time_start as time_start,
        modx_segment_users.time_end as time_end,
        modx_segment_users.ord_count as ord_count,
        modx_segment_users.ord_avg as ord_avg,
        modx_segment_users.ord_sum as ord_sum,
        SUM(modx_ms2_orders.cart_cost) as  ord_sum2
    FROM 
        modx_segment_users,modx_ms2_orders,modx_user_attributes
    WHERE 
        modx_segment_users.user_id = modx_ms2_orders.user_id AND
        modx_segment_users.user_id = modx_user_attributes.internalKey AND
        modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>=0 $lastdays_placeholder $date1_placeholder $date2_placeholder $last_buy1_placeholder $last_buy2_placeholder) AND
        modx_segment_users.user_id IN (Select internalKey FROM modx_user_attributes WHERE id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder)
        $min_avg_placeholder 
        $max_avg_placeholder
        $min_sum_placeholder
        $max_sum_placeholder
        $gender_placeholder
        $min_count_placeholder
        $max_count_placeholder
        $product_keys_placeholder
        $min_cartcost_placeholder
        $max_cartcost_placeholder
    Group By 
        modx_segment_users.user_id";
    
    $info_user= $modx->query($sql);
    $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
    return($info_user);
} else {
    $birthday_1 = $_GET['birthday_1'];
    $birthday_2 = $_GET['birthday_2'];
    $min_dob = $_GET['min_dob'];
    $max_dob = $_GET['max_dob'];
    $last_buy1 = $_GET['last_buy1'];
    $last_buy2 = $_GET['last_buy2'];
    $date1 = $_GET['date_1'];
    $date2 = $_GET['date_2'];
    $min_cartcost = $_GET['min_cartcost'];
    $max_cartcost = $_GET['max_cartcost'];
    $gender = $_GET['gender'];
    $max_avg = $_GET['max_avg']; //максимальный средний чек
    $min_avg = $_GET['min_avg']; //минимальный средний чек
    $lastdays = $_GET['days']; //за сколько последних дней брать информацию
    $min_sum = $_GET['min_sum']; //минимальная сумма покупок от (price)
    $max_sum = $_GET['max_sum']; //максимальная сумма покупок до (price)
    $min_count = $_GET['min_count']; //минимальное количество заказов
    $max_count = $_GET['max_count']; //максимальное количество заказов
    $products = array( //покупки определенных продуктов
        "1" => $_GET['product_1'], 
        "2" => $_GET['product_2'],
        "3" => $_GET['product_3'],
        "4" => $_GET['product_4']
    );
    $product_keys_placeholder = '';
    foreach ($products as $product){
        if ($product){
            $product_keys_placeholder = $product_keys_placeholder." AND modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id='$product')";
        }
    }
    if ($birthday_1){
        $birthday_1_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) >= DAYOFMONTH(now()) -'$birthday_1' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
    }
    if ($birthday_2){
        $birthday_2_placeholder = " AND DAYOFMONTH(FROM_UNIXTIME(dob)) <= DAYOFMONTH(now()) +'$birthday_2' AND MONTH(FROM_UNIXTIME(modx_user_attributes.dob)) = MONTH(now())";
    }
    if ($min_dob>0 or $max_dob>0){
        $dobswither = " AND dob <> 0";
        $modx->log(modX::LOG_LEVEL_ERROR, $dobswither);
    }
    if ($min_dob){
        $min_dob = $min_dob*365*24*60*60;
        $min_dob_placeholder = " AND (UNIX_TIMESTAMP() - $min_dob) >= dob";
    }
    if ($max_dob){
        $max_dob = $max_dob*365*24*60*60;
        $max_dob_placeholder = " AND (UNIX_TIMESTAMP() - $max_dob) <= dob";
    }
    if ($last_buy1){
        $last_buy1_placeholder = " AND time_end <= DATE_ADD(CURDATE(), INTERVAL -'$last_buy1' DAY)";
    }
    if ($last_buy2){
        $last_buy2_placeholder = " AND time_end >= DATE_ADD(CURDATE(), INTERVAL -'$last_buy2' DAY)";
    }
    if ($date1){
        $date1_placeholder = " AND modx_ms2_orders.createdon >= '$date1'";
    }
    if ($date2){
        $date2_placeholder = " AND modx_ms2_orders.createdon <= '$date2'";
    }
    if ($gender){
        $gender_placeholder = " AND modx_segment_users.user_id IN (Select id FROM modx_user_attributes WHERE gender='$gender')";
    }
    if ($max_cartcost>0){
        $max_cartcost_placeholder = "AND modx_ms2_orders.cart_cost<='$max_cartcost'";
    }
    if ($min_cartcost>0){
        $min_cartcost_placeholder = " AND modx_ms2_orders.cart_cost>='$min_cartcost'";
    }
    if ($min_sum>0){
        $min_sum_placeholder = " AND ord_sum>='$min_sum'";
    }
    if ($max_sum>0){
        $max_sum_placeholder = " AND ord_sum<='$max_sum'";
    }
    if ($min_count>0){
        $min_count_placeholder = " AND ord_count>='$min_count'";
    }
    if ($max_count>0){
        $max_count_placeholder = " AND ord_count<='$max_count'";
    }
    if ($min_avg>0){
        $min_avg_placeholder = " AND ord_avg>='$min_avg'";
    }
    if ($max_avg>0){
        $max_avg_placeholder = " AND ord_avg<='$max_avg'";
    }
    if ($lastdays){
        $lastdays_placeholder = " AND modx_ms2_orders.createdon >= DATE_ADD(CURDATE(), INTERVAL -'$lastdays' DAY)";
    }
    $sql = "
    SELECT 
        modx_segment_users.user_id as user_id,
        modx_segment_users.user_name as user_name,
        modx_user_attributes.email as email,
        modx_user_attributes.mobilephone as mobilephone,
        modx_segment_users.time_start as time_start,
        modx_segment_users.time_end as time_end,
        modx_segment_users.ord_count as ord_count,
        modx_segment_users.ord_avg as ord_avg,
        modx_segment_users.ord_sum as ord_sum,
        SUM(modx_ms2_orders.cart_cost) as  ord_sum2
    FROM 
        modx_segment_users,modx_ms2_orders,modx_user_attributes
    WHERE 
        modx_segment_users.user_id = modx_ms2_orders.user_id AND
        modx_segment_users.user_id = modx_user_attributes.internalKey AND
        modx_ms2_orders.id IN (Select order_id FROM modx_ms2_order_products WHERE product_id>=0 $lastdays_placeholder $date1_placeholder $date2_placeholder $last_buy1_placeholder $last_buy2_placeholder) AND
        modx_segment_users.user_id IN (Select internalKey FROM modx_user_attributes WHERE id >= 0 $dobswither $min_dob_placeholder $max_dob_placeholder $birthday_1_placeholder $birthday_2_placeholder)
        $min_avg_placeholder 
        $max_avg_placeholder
        $min_sum_placeholder
        $max_sum_placeholder
        $gender_placeholder
        $min_count_placeholder
        $max_count_placeholder
        $product_keys_placeholder
        $min_cartcost_placeholder
        $max_cartcost_placeholder AND
        modx_segment_users.user_id IN (Select user_id FROM modx_segment_base WHERE segment_id = '$segment_id')
    Group By 
        modx_segment_users.user_id";
    
    $info_user= $modx->query($sql);
    $info_user = $info_user->fetchAll(PDO::FETCH_ASSOC);
    return($info_user);
}