<?php
$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);

function no_sk($str)
{
	$str = str_replace('"', "'", $str);
	//$str = addslashes($str);
	$str = str_replace('\\', "-", $str);
	$str = str_replace(':', " - ", $str);

	return  $str;
}

$json = [];

$add_cart_id = $modx->getOption('ms2_id_category_cart_add');
$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, price.additive
FROM modx_site_content content , modx_ms2_products price 
WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and content.parent > 0 and content.isfolder != 1 AND price.additive=1 ORDER BY content.menuindex ASC";
$add_carts = $modx->query($sql);
$add_carts = $add_carts->fetchAll(PDO::FETCH_ASSOC);

if ($add_carts) {
	
	foreach ($add_carts as $new) {
		$content =  str_replace(array("\r\n", "\r", "\n"), ' ',  strip_tags($new['content']));
	 
	//size
    $sql = "SELECT value
	        FROM modx_site_tmplvar_contentvalues
        	WHERE tmplvarid = 1 and contentid = " . $new['id'];
	$size= $modx->query($sql);
	$size__value = $size->fetchAll(PDO::FETCH_ASSOC);
	//end size
	
	$parent_id = $new['parent'];
	//additives
	$additives_res = [];
	$sql = "SELECT * FROM modx_site_content WHERE parent = $parent_id AND isfolder = 1";
	$additives__value= $modx->query($sql);
	$additives__value = $additives__value->fetchAll(PDO::FETCH_ASSOC);
	
	if ($additives__value) {
	
    	$additives__id = $additives__value[0]['id'];
    
    	$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image
                FROM modx_site_content content , modx_ms2_products price 
                WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                price.id = content.id and content.parent = $additives__id and content.isfolder != 1 ORDER BY content.menuindex ASC";
    	$additives= $modx->query($sql);
    
    	$additives = $additives->fetchAll(PDO::FETCH_ASSOC);
    
    
    	$additives__massive =  json_decode($additives[0]['id'], true);
    
    
    	if($additives)
    	{

    		foreach ($additives as $additives__one) {
                $bonus_cost_add = floor($additives__one["price"] / 100 * $bonus_persent);
    			$additives_res[] = [
    			        "id" => $additives__one["id"],
            			"parent" => $additives__one["parent"],    
            			"title" => no_sk($additives__one["pagetitle"]),
            			"price" => (int)$additives__one["price"],
            			"bonus_add" => $bonus_cost_add,
            			"img_small" =>  $additives__one["image"],
            			"size" => $additives__one[0]["value"]
    			    ];
    		}
    	}
	}
	//end additives
	
	//ingredients
	$ingredients_res = [];
	$sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$new['id']." AND `key` LIKE 'ingredients_%'";
	$ingredients = $modx->query($sql);
	$ingredients = $ingredients->fetchAll(PDO::FETCH_ASSOC);

	if($ingredients)
	{
	    
		$ingredients__number = 1;
		foreach ($ingredients as $ingredients__one) {
			$ingredients_res[] = [
    			"id" =>$ingredients__one['id'],
    			"title" => $ingredients__one['value'],
			];
		}
	
	}
	//end ingredients
	
	//sized
	$sized_res = [];
	$sql = "SELECT * FROM modx_ms2_product_options WHERE value = ".$new['id']." ORDER BY product_id ASC";
	$sized__value = $modx->query($sql);
	$sized__value = $sized__value->fetchAll(PDO::FETCH_ASSOC);

    if ($sized__value) {
    	
    		$sz = '';
    		$sized__number = 0;
    		foreach ($sized__value as $sized__one) {
    			
    			$id_parent = $sized__one['product_id'];
    			$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
                FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
                WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key = 'sized'
                GROUP BY price.id ASC";
            	$query= $modx->query($sql);
            	$query = $query->fetchAll(PDO::FETCH_ASSOC);
            	foreach ($query as $sized) {
            	    
            				$bonus_cost_sz = floor($sized["price"] / 100 * $bonus_persent);
            				$sized_res[] = [
            				    "id" => $sized["id"],
                				"parent" => $sized["parent"],    
                				"title" => no_sk($sized["pagetitle"]),
                				"price" => (int)$sized["price"],
                				"bonus_add" => $bonus_cost_sz,
                				"size" =>  $sized["value"]
            				];
        			}
		    }
	}
	//sized end
	
	// ******************Энергетическая ценность******************
	$nutrition_res = [
	    "nutrition_carbohydrates" => "",
                "nutrition_oils" => "",
                "nutrition_protein" => "",
                "nutrition_value" => "",
                "nutrition_size" => ""
	    ];
	$sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$new['id']." AND `key` LIKE 'energy_%'";
	$energys = $modx->query($sql);
	$energys = $energys->fetchAll(PDO::FETCH_ASSOC);

	if ($energys)
	{
		foreach ($energys as $key => $energy__one) {
		    
		    if ($energy__one['key'] == 'energy_carbohydrates') {
		        
		        $nutrition_res["nutrition_carbohydrates"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_oils')  {
		        
		        $nutrition_res["nutrition_oils"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_protein')  {
		        
		        $nutrition_res["nutrition_protein"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_value') {
		        $nutrition_res["nutrition_value"] = $energy__one['value'];
		    }
		    
		}
		$energy_res[$key]["nutrition_size"] = $size__value[0]["value"];
	
	}
	
	
	// ******************// Энергетическая ценность******************
	
	$types_res = [];
	$sql = "SELECT * FROM modx_ms2_product_options WHERE value = ".$new['parent']." ORDER BY product_id ASC";
	$type__value= $modx->query($sql);
	$type__value = $type__value->fetchAll(PDO::FETCH_ASSOC);
	
        if ($type__value) {
        	
        		$type__number = 0;
        		foreach ($type__value as $type__one) {
        			
        			$id_parent = $type__one['product_id'];
        			$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
                    FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
                    WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                    price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key LIKE 'type_%'
                    GROUP BY price.id ASC";
        	$query= $modx->query($sql);
        	$query = $query->fetchAll(PDO::FETCH_ASSOC);
        	foreach ($query as $type) {
        				
        				$bonus_cost_tp = floor($type["price"] / 100 * $bonus_persent);
        				$types_res[] = [
        				    "id" => $type["id"],
            				"parent" => $type["parent"],    
            				"title" => no_sk($type["pagetitle"]),
            				"bonus_add" => $bonus_cost_tp,
            				"price" => (int)$type["price"],
            				"type" => $type["value"]
        				];
        			}
        		}
        	}
	
	
		$json["cart_add"][] = [
			"id"=> $new["id"],
			"parent"=>  [$parent_id], 
			"title"=> no_sk($new["pagetitle"]),
			"desc"=>no_sk($content),
			"price"=> (int) $new["price"],
			"bonus_add"=>"",
			"img_small"=>  $new["image"],
			"size"=> $size__value[0]["value"],
			"additives"=> $additives_res,
			"ingredients"=> $ingredients_res,
			"sized"=> $sized_res,
			"types"=> $types_res,
			"nutrition"=> $nutrition_res,
		];
	}
}


$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image
FROM modx_site_content content , modx_ms2_products price 
WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and price.new = '1' and content.isfolder != 1 ORDER BY content.menuindex ASC";
$news = $modx->query($sql);
$news = $news->fetchAll(PDO::FETCH_ASSOC);

if ($add_carts) {
	foreach ($news as $new) {
		$content =  str_replace(array("\r\n", "\r", "\n"), ' ',  strip_tags($new['content']));
	 
	//size
    $sql = "SELECT value
	        FROM modx_site_tmplvar_contentvalues
        	WHERE tmplvarid = 1 and contentid = " . $new['id'];
	$size= $modx->query($sql);
	$size__value = $size->fetchAll(PDO::FETCH_ASSOC);
	//end size
	
	$parent_id = $new['parent'];
	//additives
	$additives_res = [];
	$sql = "SELECT * FROM modx_site_content WHERE parent = $parent_id AND isfolder = 1";
	$additives__value= $modx->query($sql);
	$additives__value = $additives__value->fetchAll(PDO::FETCH_ASSOC);
	
	if ($additives__value) {
	
    	$additives__id = $additives__value[0]['id'];
    
    	$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image
                FROM modx_site_content content , modx_ms2_products price 
                WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                price.id = content.id and content.parent = $additives__id and content.isfolder != 1 ORDER BY content.menuindex ASC";
    	$additives= $modx->query($sql);
    
    	$additives = $additives->fetchAll(PDO::FETCH_ASSOC);
    
    
    	$additives__massive =  json_decode($additives[0]['id'], true);
    
    
    	if($additives)
    	{

    		foreach ($additives as $additives__one) {
                $bonus_cost_add = floor($additives__one["price"] / 100 * $bonus_persent);
    			$additives_res[] = [
    			        "id" => $additives__one["id"],
            			"parent" => $additives__one["parent"],    
            			"title" => no_sk($additives__one["pagetitle"]),
            			"price" => (int)$additives__one["price"],
            			"bonus_add" => $bonus_cost_add,
            			"img_small" =>  $additives__one["image"],
            			"size" => $additives__one[0]["value"]
    			    ];
    		}
    	}
	}
	//end additives
	
	//ingredients
	$ingredients_res = [];
	$sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$new['id']." AND `key` LIKE 'ingredients_%'";
	$ingredients = $modx->query($sql);
	$ingredients = $ingredients->fetchAll(PDO::FETCH_ASSOC);

	if($ingredients)
	{
	    
		$ingredients__number = 1;
		foreach ($ingredients as $ingredients__one) {
			$ingredients_res[] = [
    			"id" =>$ingredients__one['id'],
    			"title" => $ingredients__one['value'],
			];
		}
	
	}
	//end ingredients
	
	//sized
	$sized_res = [];
	$sql = "SELECT * FROM modx_ms2_product_options WHERE value = ".$new['id']." ORDER BY product_id ASC";
	$sized__value = $modx->query($sql);
	$sized__value = $sized__value->fetchAll(PDO::FETCH_ASSOC);

    if ($sized__value) {
    	
    		$sz = '';
    		$sized__number = 0;
    		foreach ($sized__value as $sized__one) {
    			
    			$id_parent = $sized__one['product_id'];
    			$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
                FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
                WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key = 'sized'
                GROUP BY price.id ASC";
            	$query= $modx->query($sql);
            	$query = $query->fetchAll(PDO::FETCH_ASSOC);
            	foreach ($query as $sized) {
            	    
            				$bonus_cost_sz = floor($sized["price"] / 100 * $bonus_persent);
            				$sized_res[] = [
            				    "id" => $sized["id"],
                				"parent" => $sized["parent"],    
                				"title" => no_sk($sized["pagetitle"]),
                				"price" => (int)$sized["price"],
                				"bonus_add" => $bonus_cost_sz,
                				"size" =>  $sized["value"]
            				];
        			}
		    }
	}
	//sized end
	
	// ******************Энергетическая ценность******************
	$nutrition_res = [
	    "nutrition_carbohydrates" => "",
                "nutrition_oils" => "",
                "nutrition_protein" => "",
                "nutrition_value" => "",
                "nutrition_size" => ""
	    ];
	$sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$new['id']." AND `key` LIKE 'energy_%'";
	$energys = $modx->query($sql);
	$energys = $energys->fetchAll(PDO::FETCH_ASSOC);

	if ($energys)
	{
		foreach ($energys as $key => $energy__one) {
		    
		    if ($energy__one['key'] == 'energy_carbohydrates') {
		        
		        $nutrition_res["nutrition_carbohydrates"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_oils')  {
		        
		        $nutrition_res["nutrition_oils"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_protein')  {
		        
		        $nutrition_res["nutrition_protein"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_value') {
		        $nutrition_res["nutrition_value"] = $energy__one['value'];
		    }
		    
		}
		$energy_res[$key]["nutrition_size"] = $size__value[0]["value"];
	
	}
	
	
	// ******************// Энергетическая ценность******************
	
	$types_res = [];
	$sql = "SELECT * FROM modx_ms2_product_options WHERE value = ".$new['parent']." ORDER BY product_id ASC";
	$type__value= $modx->query($sql);
	$type__value = $type__value->fetchAll(PDO::FETCH_ASSOC);
	
        if ($type__value) {
        	
        		$type__number = 0;
        		foreach ($type__value as $type__one) {
        			
        			$id_parent = $type__one['product_id'];
        			$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
                    FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
                    WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                    price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key LIKE 'type_%'
                    GROUP BY price.id ASC";
        	$query= $modx->query($sql);
        	$query = $query->fetchAll(PDO::FETCH_ASSOC);
        	foreach ($query as $type) {
        				
        				$bonus_cost_tp = floor($type["price"] / 100 * $bonus_persent);
        				$types_res[] = [
        				    "id" => $type["id"],
            				"parent" => $type["parent"],    
            				"title" => no_sk($type["pagetitle"]),
            				"bonus_add" => $bonus_cost_tp,
            				"price" => (int)$type["price"],
            				"type" => $type["value"]
        				];
        			}
        		}
        	}
	
	
	        $image_small = $modx->runSnippet('pthumb', [
    'input' => substr($new["image"], 1),
    'options' => '&h=120&q=50',
]);
        $image_big = $modx->runSnippet('pthumb', [
    'input' => substr($new["image"], 1),
    'options' => '&h=400&q=50',
]);

		$json["new"][] = [
			"id"=> $new["id"],
			"parent"=>  [$parent_id], 
			"title"=> no_sk($new["pagetitle"]),
			"desc"=>no_sk($content),
			"price"=> (int) $new["price"],
			"bonus_add"=>"",
			"img_big"=>$image_big,
			"img_small"=> $image_small,
			"size"=> $size__value[0]["value"],
			"additives"=> $additives_res,
			"ingredients"=> $ingredients_res,
			"sized"=> $sized_res,
			"types"=> $types_res,
			"nutrition"=> $nutrition_res,
		];
	}
}

$add_cart_id_free = $modx->getOption('ms2_id_free_cart_add');
$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image
FROM modx_site_content content , modx_ms2_products price 
WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
price.id = content.id and content.parent = '$add_cart_id_free' and content.isfolder != 1 ORDER BY content.menuindex ASC";
$news = $modx->query($sql);
$news = $news->fetchAll(PDO::FETCH_ASSOC);

if ($add_carts) {
	foreach ($news as $new) {
    $content =  str_replace(array("\r\n", "\r", "\n"), ' ',  strip_tags($new['content']));
	 
	//size
    $sql = "SELECT value
	        FROM modx_site_tmplvar_contentvalues
        	WHERE tmplvarid = 1 and contentid = " . $new['id'];
	$size= $modx->query($sql);
	$size__value = $size->fetchAll(PDO::FETCH_ASSOC);
	//end size
	
	$parent_id = $new['parent'];
	//additives
	$additives_res = [];
	$sql = "SELECT * FROM modx_site_content WHERE parent = $parent_id AND isfolder = 1";
	$additives__value= $modx->query($sql);
	$additives__value = $additives__value->fetchAll(PDO::FETCH_ASSOC);
	
	if ($additives__value) {
	
    	$additives__id = $additives__value[0]['id'];
    
    	$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image
                FROM modx_site_content content , modx_ms2_products price 
                WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                price.id = content.id and content.parent = $additives__id and content.isfolder != 1 ORDER BY content.menuindex ASC";
    	$additives= $modx->query($sql);
    
    	$additives = $additives->fetchAll(PDO::FETCH_ASSOC);
    
    
    	$additives__massive =  json_decode($additives[0]['id'], true);
    
    
    	if($additives)
    	{

    		foreach ($additives as $additives__one) {
                $bonus_cost_add = floor($additives__one["price"] / 100 * $bonus_persent);
    			$additives_res[] = [
    			        "id" => $additives__one["id"],
            			"parent" => $additives__one["parent"],    
            			"title" => no_sk($additives__one["pagetitle"]),
            			"price" => (int)$additives__one["price"],
            			"bonus_add" => $bonus_cost_add,
            			"img_small" =>  $additives__one["image"],
            			"size" => $additives__one[0]["value"]
    			    ];
    		}
    	}
	}
	//end additives
	
	//ingredients
	$ingredients_res = [];
	$sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$new['id']." AND `key` LIKE 'ingredients_%'";
	$ingredients = $modx->query($sql);
	$ingredients = $ingredients->fetchAll(PDO::FETCH_ASSOC);

	if($ingredients)
	{
	    
		$ingredients__number = 1;
		foreach ($ingredients as $ingredients__one) {
			$ingredients_res[] = [
    			"id" =>$ingredients__one['id'],
    			"title" => $ingredients__one['value'],
			];
		}
	
	}
	//end ingredients
	
	//sized
	$sized_res = [];
	$sql = "SELECT * FROM modx_ms2_product_options WHERE value = ".$new['id']." ORDER BY product_id ASC";
	$sized__value = $modx->query($sql);
	$sized__value = $sized__value->fetchAll(PDO::FETCH_ASSOC);

    if ($sized__value) {
    	
    		$sz = '';
    		$sized__number = 0;
    		foreach ($sized__value as $sized__one) {
    			
    			$id_parent = $sized__one['product_id'];
    			$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
                FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
                WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key = 'sized'
                GROUP BY price.id ASC";
            	$query= $modx->query($sql);
            	$query = $query->fetchAll(PDO::FETCH_ASSOC);
            	foreach ($query as $sized) {
            	    
            				$bonus_cost_sz = floor($sized["price"] / 100 * $bonus_persent);
            				$sized_res[] = [
            				    "id" => $sized["id"],
                				"parent" => $sized["parent"],    
                				"title" => no_sk($sized["pagetitle"]),
                				"price" => (int)$sized["price"],
                				"bonus_add" => $bonus_cost_sz,
                				"size" =>  $sized["value"]
            				];
        			}
		    }
	}
	//sized end
	
	// ******************Энергетическая ценность******************
	$nutrition_res = [
	    "nutrition_carbohydrates" => "",
                "nutrition_oils" => "",
                "nutrition_protein" => "",
                "nutrition_value" => "",
                "nutrition_size" => ""
	    ];
	$sql = "SELECT *  FROM `modx_ms2_product_options` WHERE `product_id` = ".$new['id']." AND `key` LIKE 'energy_%'";
	$energys = $modx->query($sql);
	$energys = $energys->fetchAll(PDO::FETCH_ASSOC);

	if ($energys)
	{
		foreach ($energys as $key => $energy__one) {
		    
		    if ($energy__one['key'] == 'energy_carbohydrates') {
		        
		        $nutrition_res["nutrition_carbohydrates"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_oils')  {
		        
		        $nutrition_res["nutrition_oils"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_protein')  {
		        
		        $nutrition_res["nutrition_protein"] = $energy__one['value'];
		        
		    } elseif ($energy__one['key'] == 'energy_value') {
		        $nutrition_res["nutrition_value"] = $energy__one['value'];
		    }
		    
		}
		$energy_res[$key]["nutrition_size"] = $size__value[0]["value"];
	
	}
	
	
	// ******************// Энергетическая ценность******************
	
	$types_res = [];
	$sql = "SELECT * FROM modx_ms2_product_options WHERE value = ".$new['parent']." ORDER BY product_id ASC";
	$type__value= $modx->query($sql);
	$type__value = $type__value->fetchAll(PDO::FETCH_ASSOC);
	
        if ($type__value) {
        	
        		$type__number = 0;
        		foreach ($type__value as $type__one) {
        			
        			$id_parent = $type__one['product_id'];
        			$sql = "SELECT content.id,content.pagetitle,content.parent,content.content,price.price,price.image, opt.value
                    FROM modx_site_content content , modx_ms2_products price , modx_ms2_product_options opt
                    WHERE content.deleted = 0 and content.published = 1 and content.template = '4' and 
                    price.id = content.id and content.isfolder != 1 and price.id = $id_parent and opt.product_id = price.id and opt.key LIKE 'type_%'
                    GROUP BY price.id ASC";
        	$query= $modx->query($sql);
        	$query = $query->fetchAll(PDO::FETCH_ASSOC);
        	foreach ($query as $type) {
        				
        				$bonus_cost_tp = floor($type["price"] / 100 * $bonus_persent);
        				$types_res[] = [
        				    "id" => $type["id"],
            				"parent" => $type["parent"],    
            				"title" => no_sk($type["pagetitle"]),
            				"bonus_add" => $bonus_cost_tp,
            				"price" => (int)$type["price"],
            				"type" => $type["value"]
        				];
        			}
        		}
        	}
	
	
		$json["gift"][] = [
			"id"=> $new["id"],
			"parent"=>  [$parent_id], 
			"title"=> no_sk($new["pagetitle"]),
			"desc"=>no_sk($content),
			"price"=> (int) $new["price"],
			"bonus_add"=>"",
			"img_small"=> $new["image"],
			"size"=> $size__value[0]["value"],
			"additives"=> $additives_res,
			"ingredients"=> $ingredients_res,
			"sized"=> $sized_res,
			"types"=> $types_res,
			"nutrition"=> $nutrition_res,
			"max_gift"=> $modx->getOption('ms2_max_gift')
		];
	}
}
echo json_encode($json);