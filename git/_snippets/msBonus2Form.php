<?php
/** @var modX $modx */
/** @var msBonus2 $msb2 */
/** @var array $scriptProperties */
$sp = &$scriptProperties;
if (!$msb2 = $modx->getService('msbonus2', 'msBonus2',
    $modx->getOption('msb2_core_path', null, MODX_CORE_PATH . 'components/msbonus2/') . 'model/msbonus2/', $sp)
) {
    return 'Could not load msBonus2 class!';
}
$msb2->initialize($modx->context->key);
$manager = $msb2->getManager();

// Check auth
if (!$modx->user->isAuthenticated($modx->context->key)) {
    return;
}

//
$tpl = $modx->getOption('tpl', $sp, 'tpl.msBonus2.form');

// Check is active
$is_active = false;
if ($amount = $manager->getCartWriteoff()) {
    $is_active = is_numeric($amount);
}
// if ($is_active === false) {
//     $manager->unsetCartWriteoff();
// }

//
$msb2->loadFrontendScripts();

//
$output = $msb2->tools->getChunk($tpl, [
    'points' => number_format($manager->getUserPoints(), 0, '.', ' '),
    'amount' => $amount,
    'is_active' => $is_active,
]);

return $output;