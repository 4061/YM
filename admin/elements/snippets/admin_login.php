<?php
$url =  $modx->config['site_url'];
$url = str_replace('//','-',$url);
$url = str_replace('/','',$url);
$url = str_replace('-','//',$url);
$mail = $_GET['param1'];
$pass = $_GET['param2'];
$logindata = array(
  'username' => $mail,   // имя пользователя
  'password' => $pass, // пароль
  'rememberme' => true        // запомнить?
);
// сам процесс авторизации
$response = $modx->runProcessor('/security/login', $logindata);
// проверяем, успешно ли
if ($response->isError()) {
  // произошла ошибка, например неверный пароль
  $modx->log(modX::LOG_LEVEL_ERROR, 'Login error. Message: '.$response->getMessage());
  $data = 0;
} else {
    $data = true; 
}
return $data;