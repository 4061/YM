<?php
$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    foreach ($_REQUEST['JSON'] as $key => $system){
        $USA = $modx->getObject('modSystemSetting', $key);
        if (!$system){
            $system = 0;
        }
        $USA->set('value', $system);
        $USA->save();
        $arr[$key] = $system;
    }
    $modx->cacheManager->refresh(array('system_settings' => array()));
    return json_encode($arr);