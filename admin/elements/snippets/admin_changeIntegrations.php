<?php
$_REQUEST['JSON'] = json_decode(
             file_get_contents('php://input'), true
    );
    
    if ($_REQUEST['JSON']["true"]){
        $USA = $modx->getObject('modSystemSetting', "IIKO");
        $USA->set('value', true);
        $USA->save();
    } else {
        $USA = $modx->getObject('modSystemSetting', "IIKO");
        $USA->set('value', false);
        $USA->save();
    }
    $modx->cacheManager->refresh(array('system_settings' => array()));
    return json_encode($arr);