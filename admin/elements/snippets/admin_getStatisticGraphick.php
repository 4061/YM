<?php
// массив с названием месяцев на русском
$massMonthName = array(
    1 => "Янв",
    2 => "Фев",
    3 => "Март",
    4 => "Апр",
    5 => "Май",
    6 => "Июнь",
    7 => "Июль",
    8 => "Авг",
    9 => "Сент",
    10 => "Окт",
    11 => "Ноябр",
    12 => "Дек");
// переменные для формирования строки для вывода на странице
$costMass = $monthMass = $orderMass = "";
// текущая дата
$dateNow = date("Y-m-d");
// номер текущего месяца
$month = date("n");
// формируем дату начала/конца текущего месяца
$dateCheckMonthStart = strtotime(date("Y-m-1"));
$dateCheckMonthEnd = strtotime(date("Y-m-t"));
// переменная для отслеживания текущего месяца
$firstMonth = $totalSummMonth = $totalSummDay = 0;
// формируем цикл для перебора 7 последних месяцев
while($month != date("n",strtotime(date("Y-m-d") . " - 7 month"))){
    // обнуляем переменную для подсчёта суммы заказов за месяц
    $summ = $ordersCount = 0;
    // записываем в строку с месяцами название полученного месяца
    $monthMass = ",".$massMonthName[$month].$monthMass;
    // получаем список всех заказов
    $orderCollection = $modx->getCollection("msOrder");
    // перебераем полученный список заказов
    foreach ($orderCollection as $order){
        // получаем дату заказа
        $dateOrder = strtotime($order->get("createdon"));
        // если зказ был сформирован в указанном месяце, то прибавляем его сумму к общей сумме за месяц
        if($dateCheckMonthStart <= $dateOrder && $dateOrder <= $dateCheckMonthEnd) {
            $summ += $order->get("cost");
            $totalSummMonth += $order->get("cost");
        }
        // обнуляем переменную дня недели
        $dayWeek = 0;
        // проверяем, текущий ли это месяц (для этого вводится переменная)
        if($firstMonth != 1){
            // перебераем цыкл по всем дням в этом месяце
            for($day = 1; $day <= date("t"); $day++) {
                /* ============== Считаем заказы за месяц на каждый день ============== */
                // если дата у товара совпала с полученой датой в цикле, то увеличиваем в ней переменную
                if (date("Y-m-d",$dateOrder) == date("Y-m-d",strtotime(date("Y-m-1") . " + ".($day - 1)." days"))) {
                    // увеличиваем переменную счётчика для дня в месяце
                    $massMonthCount[$day]++;
                } else if (!$massMonthCount[$day]) {
                    // если за данный день ничего не купили, то заносим 0
                    $massMonthCount[$day] = 0;
                }
            }
            
            // перебераем цыкл по всем дням в этом месяце
            for($dayWeek = 0; $dayWeek < 7; $dayWeek++) {
                
                $checkDayWeek = date("Y-m-d",strtotime(date("Y-m-d") . " - ".($dayWeek)." days"));
                if (date("Y-m-d",$dateOrder) == $checkDayWeek) {
                    $massWeekCost[$checkDayWeek] += $order->get("cost");
                    $totalSummWeek += $order->get("cost");
                } else if (!$massWeekCost[$checkDayWeek]) {
                    $massWeekCost[$checkDayWeek] = 0;
                }
                
            }
            
            // перебераем цыкл по всем часам данного дня
            for($dayHour = 0; $dayHour < 24; $dayHour++) {
                
                $checkDayHour = date("Y-m-d H",strtotime(date("Y-m-d 00:i:s") . " + ".($dayHour)." hours"));
                $dayHourNum = date("H",strtotime(date("Y-m-d 00:i:s") . " + ".($dayHour)." hours"));
                if (date("Y-m-d H",$dateOrder) == $checkDayHour) {
                    $massDayCost[$dayHourNum] += $order->get("cost");
                    $totalSummDay += $order->get("cost");
                } else if (!$massDayCost[$dayHourNum]) {
                    $massDayCost[$dayHourNum] = 0;
                }
                
            }
        }
        
    };
    
    // заносим сумму за месяц в строку с значениями для вывода на странице
    $costMass = ",".$summ.$costMass;
    // получаем дату начала и конца месяца на предыдущий месяц
    $dateCheckMonthStart = strtotime(date("Y-m-1",strtotime($dateNow . " - 1 month")));
    $dateCheckMonthEnd = strtotime(date("Y-m-t",strtotime($dateNow . " - 1 month")));
    // получаем номер предыдущего месяца
    $month = date("n",strtotime($dateNow . " - 1 month"));
    // получаем дату предыдущего месяца для расчёта
    $dateNow = date("Y-m-d",strtotime($dateNow . " - 1 month"));
    // выставляем переменную текущего месяца в 1, что бы в дальнейшем по нему не велось расчётов
    $firstMonth = 1;
    
}

// формируем строку с перечнем количества заказов в день
foreach($massMonthCount as $countM){
   $orderMass .= $countM.",";
}
// формируем строку с перечнем количества заказов в день
foreach($massWeekCost as $key => $costW){
   $orderMassWeek = ",".$costW.$orderMassWeek;
   $orderMassWeekKey = ",".$key.$orderMassWeekKey;
}
// формируем строку с перечнем количества заказов в день
foreach($massDayCost as $key => $costD){
   $orderMassDay .= $costD.",";
   $orderMassDayKey .= $key.",";
}

$modx->log(1,"Массив: ".print_r($massWeekCost,1));

// удаляем запятые в начале из обоих полученных строк
$costMass = substr($costMass, 1);
$monthMass = substr($monthMass, 1);
$orderMass = mb_substr($orderMass, 0, -1);
$orderMassWeek = substr($orderMassWeek, 1);
$orderMassWeekKey = substr($orderMassWeekKey, 1);
$orderMassDay = mb_substr($orderMassDay, 0, -1);
$orderMassDayKey = mb_substr($orderMassDayKey, 0, -1);
// формируем массив для ответа
$output = array(
    "Month" => array(
            "strCost" => $costMass,
            "strName" => $monthMass,
            "totalCost" => $totalSummMonth
        ),
    "Week" => array(
            "strCost" => $orderMassWeek,
            "strName" => $orderMassWeekKey,
            "totalCost" => $totalSummWeek
        ),
    "Day" => array(
            "strCost" => $orderMassDay,
            "strName" => $orderMassDayKey,
            "totalCost" => $totalSummDay
        ),
    "graphick" => array(
            "orderList" => $orderMass
        ),
);

return $output;