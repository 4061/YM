<?php
$PH_PROGRAM = $modx->getOption('loyalty_PH_program');
$MAIN_PROGRAM = $modx->getOption('loyalty_main_program');
$programms["prime_hill"] = array("title" => "Прайм-Хилл", "desc" => "Бонусная система Prime-Hill.", "value" => $PH_PROGRAM, "setting" => "loyalty_PH_program" );
$programms["main_programm"] = array("title" => "SOFTJET", "desc" => "Основная бонусная система.", "value" => $MAIN_PROGRAM, "setting" => "loyalty_main_program" );
return($programms);