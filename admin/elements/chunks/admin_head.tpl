<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>{$_modx->resource.pagetitle}</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/admin/images/favicon.ico">
    <link href="/admin/css/style.css?v1.1" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="/admin/images/favicon.ico">
    <link href="/admin/vendor/jqvmap/css/jqvmap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/admin/vendor/chartist/css/chartist.min.css">
    <link href="/admin/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
	<link href="https://cdn.lineicons.com/2.0/LineIcons.css" rel="stylesheet">
	<link href="/admin/vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Daterange picker -->
    <link href="/admin/vendor/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Clockpicker -->
    <link href="/admin/vendor/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <!-- asColorpicker -->
    <link href="/admin/vendor/jquery-asColorPicker/css/asColorPicker.min.css" rel="stylesheet">
    <!-- Material color picker -->
    <link href="/admin/vendor/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <!-- Pick date -->
    <link href="/admin/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/vendor/pickadate/themes/default.css">
    <link rel="stylesheet" href="/admin/vendor/pickadate/themes/default.date.css">
    <link rel="stylesheet" href="/admin/css/custom.css?v=1.5">
    <link rel="stylesheet" href="/admin/font/css/all.min.css">
    <link rel="stylesheet" href="/assets/components/ajaxform/css/lib/jquery.jgrowl.min.css">
</head>
