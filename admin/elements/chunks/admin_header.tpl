        <div class="nav-header">
            <a href="{2558|url}" class="brand-logo">
                <img class="logo-compact" src="/admin/images/logo.png" alt="">
                <img class="brand-title" src="/admin/images/logo.png" alt="">
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>