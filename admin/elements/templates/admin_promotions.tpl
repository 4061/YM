{set $ismember = '!admin_security' | snippet : []}
{if $ismember}
<!DOCTYPE html>
<html lang="en">

[[$admin_head]]

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        [[$admin_nav_header]]
		<div style="display:none;">[[!msMiniCart]]</div>
		<!--**********************************



		
		
        <!--**********************************
            Header start
        ***********************************-->
        [[$admin_top_header]]
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->
        [[$admin_sidebar]]

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
					        <button style="margin-bottom:1rem;" type="button" class="btn btn-primary add-element" data-toggle="modal" data-target="#addPromoModal">
                              Добавить подарок от цены
                            </button>
                            
                            <!-- Модальное окно добавления/редактирвоания рассылки -->
                            <div class="modal fade" id="addPromo" tabindex="-1" role="dialog" aria-labelledby="addOrderStatusModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Добавить акцию</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div id="form-add-element" data-id="0">
                                        <div class="row">
                                            
                                            <div class="form-block col-12">
                                                <label for="status_name">Название cтатуса</label><br>
                                                <input class="add-element-property" type="text" id="status_name" value="" placeholder="Название статуса">
                                            </div>
                                            <div class="form-block col-12">
                                                <label for="status_name">Описание cтатуса</label><br>
                                                <input class="add-element-property" type="text" id="status_description" value="" placeholder="Описание статуса">
                                            </div>
                                            <div class="form-block col-12">
                                                <label for="status_name">Цвет cтатуса</label><br>
                                                <input class="add-element-property" type="text" id="status_text" value="" placeholder="Цвет статуса">
                                            </div>
                                            <div class="form-block col-12">
                                                <label for="status_name">Цвет обертки статуса</label><br>
                                                <input class="add-element-property" type="text" id="status_wrapper" value="" placeholder="Цвет обертки статуса статуса">
                                            </div>
                                            <div class="form-block col-12">
                                                <label for="template">Итоговый</label><br>
                                                <select class="add-template-property select-list" name="status_final" id="status_final">
        										    <option value="0">Нет</option>
        										    <option value="1">Да</option>
        									    </select>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                    <button type="button" class="btn btn-primary btn-add-elemen-flight-status" data-parent="form-add-element">Создать</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- Модальное окно добавления/редактирвоания рассылки -->
                <!-- row -->
                {set $promocodes = '!admin_promotions' | snippet : []}
                {foreach $promocodes as $promocode}
                    <div class="row page-titles mx-0">
                        <div class="col-sm-6 p-md-0">
                            <div class="welcome-text d-flex">
                                <h4>{$promocode.name}</h4>
                                <a href="#" data-toggle="modal" data-target="#promoModal{$promocode.id_promo}" class="btn btn-primary shadow btn-xs sharp mr-1 edit-promotion-btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" style="margin-top: 3px;" data-id="{$promocode.id_promo}" class="remove-code btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        {foreach $promocode.modificators as $product}
                        <div class="col-xl-3 col-lg-6 col-md-4 col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="new-arrival-product">
                                    <div class="new-arrivals-img-contnent">
                                        <img class="img-fluid" src="{$product.0.image|pthumb : 'w=300&h=200&zc=1'}" alt="">
                                    </div>
                                    <div class="new-arrival-content text-center mt-3">
                                        <h4>{$product.0.pagetitle}</h4>
                                        <div>{$product.0.description}</div>
                                        [[-<ul class="star-rating">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star-half-empty"></i></li>
                                            <li><i class="fa fa-star-half-empty"></i></li>
                                        </ul>]]
                                        <span class="price">{$product.0.price} P</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        {/foreach}
                    </div>
                {/foreach}
            </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="http://dexignzone.com/" target="_blank">DexignZone</a> 2020</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->

        
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
  





</body>
{set $promocodes = '!getActiveCostGifts' | snippet : []}
{foreach $promocodes as $promocode}
    
    <div class="modal fade code-modal" id="promoModal{$promocode.id_promo}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    [[-<pre>{$.php.print_r($promocode)}</pre>]]
                    <h5 class="modal-title">Промокод23 {$promocode.code}</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Название</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="name" class="form-control code-{$promocode.id_promo}" value="{$promocode.name}">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Описание</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="description" class="form-control code-{$promocode.id_promo}" value="{$promocode.description}">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Доступен</label>
                                        <div class="col-sm-9 avail-parent">
                                            <div data-name="availability" data-id="1" class="code-{$promocode.id_promo} btn avail-type {if $promocode.availability == 1} active {/if}" style="margin-right: 10px;">Для всех</div>
                                            <div data-name="availability" data-id="2" class="code-{$promocode.id_promo} btn avail-type {if $promocode.availability == 2} active {/if}">Персональный</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Статус</label>
                                        <div class="col-sm-9 avail-parent">
                                            <div data-name="activity" data-id="1" class="code-{$promocode.id_promo} btn activity-type {if $promocode.activity == 1} active {/if}" style="margin-right: 10px;">Активный</div>
                                            <div data-name="activity" data-id="0" class="code-{$promocode.id_promo} btn activity-type {if $promocode.activity == 0} active {/if}">Заблокирован</div>
                                        </div>
                                    </div>
                                </div>
                                [[-<div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Промокод</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="code"  class="form-control code-{$promocode.id_promo}" value="{$promocode.code}">
                                            <div class="custom-control custom-checkbox mb-3 code-count-box">
											    <input data-name="user_use_count" type="checkbox" {if $promocode.user_use_count == 1} checked {/if} class="custom-control-input code-{$promocode.id_promo}" id="userUseCount{$promocode.code}">
											    <label class="custom-control-label" for="userUseCount{$promocode.code}">Нельзя применить дважды одному и тому же пользователю</label>
										    </div>
										    <div class="custom-control custom-checkbox mb-3 code-count-box">
											    <input data-name="use_count_in_check" type="checkbox" {if $promocode.use_count_in_check == 1} checked {/if} class="custom-control-input code-{$promocode.id_promo}" id="useCountInCheck{$promocode.code}">
											    <label class="custom-control-label" for="useCountInCheck{$promocode.code}">Нельзя применить в коризне дважды</label>
										    </div>
                                        </div>
                                    </div>
                                </div>]]
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Дата начала акции</label>
                                        <div class="col-sm-9">
                                            <input name="datepicker" data-name="date_start" value="{$promocode.date_start}" class="datepicker-default form-control picker__input picker__input--active code-{$promocode.id_promo}" id="datepicker{$promocode.code}" readonly="" aria-haspopup="true" aria-expanded="true" aria-readonly="false" aria-owns="datepicker_root">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Дата конца акции</label>
                                        <div class="col-sm-9">
                                            <input name="datepicker" data-name="date_end" value="{$promocode.date_end}" class="datepicker-default form-control picker__input picker__input--active code-{$promocode.id_promo}" id="datepicker{$promocode.code}" readonly="" aria-haspopup="true" aria-expanded="true" aria-readonly="false" aria-owns="datepicker_root">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Организации</label>
                                        {set $organisationList = '!admin_organisationObjects' | snippet : [
                                            'id_promo' => $promocode.id_promo
                                        ]}
                                        <div class="col-sm-9">
                                            <select class="segment-input form-control input-default select-control code-{$promocode.id_promo}" data-name="orgs" multiple name="organisation[]" id="org_types" type="text">
                                                <option disabled>Выберите организацию</option>
                                                {$checkOptgroup = 0}
                                                {foreach $organisationList as $organisatioElement}
                                                    {if $organisatioElement.tag == "optgroup"}
                                                        {if $checkOptgroup == 1}</optgroup>{/if}
                                                        <optgroup label="{$organisatioElement.title}">
                                                        {$checkOptgroup = 1}
                                                    {else}
                                                        <option value="{$organisatioElement.id}" {$organisatioElement.selected}>{$organisatioElement.title}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Способы доставки</label>
                                        {set $deliveryList = '!admin_deliveryObjects' | snippet : [
                                            'id_promo' => $promocode.id_promo
                                        ]}
                                        <div class="col-sm-9">
                                            <select class="segment-input form-control input-default select-control code-{$promocode.id_promo}" data-name="order_types" multiple name="delivery[]" id="order_types" type="text">
                                                <option disabled>Выберите значения</option>
                                                {foreach $deliveryList as $deliveryElement}
                                                    <option value="{$deliveryElement.id}" {$deliveryElement.selected}>{$deliveryElement.title}</option>
                                                {/foreach}
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">От какой цены</label>
                                        <div class="col-sm-9">
                                            <input type="text" value="{$promocode.value}" data-name="value" class="form-control add-code-placeholder code-{$promocode.id_promo}" placeholder="От">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Изменить блюда</label>
                                        <div class="col-sm-9 edit-price-gift">
                                            [[-<input class="segment-input form-control input-default" placeholder="Поиск" value="" id="products" type="text">
                                            <div  data-id="{$promocode.id_promo}" class="selected-results">
                                            </div>
                                            <div data-id="{$promocode.id_promo}" class="modal-results"></div>]]
        										<select   class="add-element-property select-list add-code-placeholder code-{$promocode.id_promo}" multiple name="edit_price_gifts" data-name="id_product" id="edit_price_gifts">
        										    {foreach $promocode.gifts as $gift}
                                                      <option {if $gift.active == 1} selected {/if} value="{$gift.id}">
                                                          {$gift.pagetitle}
                                                        </option>
                                                    {/foreach}
        									    </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Блюда для учета</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="products_to_gift" class="form-control code-{$promocode.id_promo}" value="{$promocode.products_to_gift}">
                                        </div>
                                    </div>
                                </div>
                                [[-{if $promocode.id_product}
                                <div>Блюда</div>
                                <div class="row">
                                    
                        {foreach $promocode.modificators as $product}
                        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div data-id="{$product.0.id}" class="res-prod-selected rescode-{$promocode.id_promo} new-arrival-product row">
                                    <div class="new-arrivals-img-contnent col-md-4">
                                        <img class="img-fluid" src="{$product.0.image|pthumb : 'w=300&h=200&zc=1'}" alt="">
                                    </div>
                                    <div class="new-arrival-content text-center col-md-8">
                                        <h4>{$product.0.pagetitle}</h4>
                                        [[-<ul class="star-rating">
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star-half-empty"></i></li>
                                            <li><i class="fa fa-star-half-empty"></i></li>
                                        </ul>]]
                                        <span class="price">{$product.0.price} P</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                        {/foreach}
                    </div>
                                {/if}]]
                                <div class="basic-form">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input data-name="image" class="photo-dir code-{$promocode.id_promo}" type="hidden">
                                            <input data-id="{$promocode.id_promo}" accept="image/*" type="file" class="file-drop custom-file-input">
                                            <label class="custom-file-label">Выберите фотографию</label>
                                        </div>
    								</div>
    								<div class="loder-items-title loder-items-title-{$promocode.id_promo}">
    								    Фотография добавлена
    								</div>
    								<div class="loader-items loader-items-{$promocode.id_promo}"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Закрыть</button>
                    <button data-id2="{$promocode.id_promo}" data-id="code-{$promocode.id_promo}" type="button" class="btn btn-primary save-promocode">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

{/foreach}
<div class="modal fade code-modal" id="addPromoModal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить подарок</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Название</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="name" class="form-control add-code-placeholder" value="Название">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Описание</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="description" class="form-control add-code-placeholder" value="Небольшое описание">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    {set $deliveryList = '!admin_deliveryObjects' | snippet : [
                                        'id_promo' => $promocode.id_promo
                                    ]}
                                    <div class="form-group row align-center">
                                        <div class="segment group-title col-sm-3 col-form-label">
                                            Способы доставки:
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control input-default add-code-placeholder" data-name="order_types" multiple name="delivery[]" id="order_types" type="text">
                                                <option disabled>Выберите значения</option>
                                                {foreach $deliveryList as $deliveryElement}
                                                    <option value="{$deliveryElement.id}">{$deliveryElement.title}</option>
                                                {/foreach}
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                </div>
<div class="basic-form">
                                    {set $organisationList = '!admin_organisationObjects' | snippet : [
                                        'id_promo' => $promocode.id_promo
                                    ]}
                                    <div class="form-group row align-center">
                                        <div class="segment group-title col-sm-3 col-form-label">
                                            Организации:
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control input-default add-code-placeholder" data-name="orgs" multiple name="organisation[]" id="org_types" type="text">
                                                <option disabled>Выберите организацию</option>
                                                {$checkOptgroup = 0}
                                                {foreach $organisationList as $organisatioElement}
                                                    {if $organisatioElement.tag == "optgroup"}
                                                        {if $checkOptgroup == 1}</optgroup>{/if}
                                                        <optgroup label="{$organisatioElement.title}">
                                                        {$checkOptgroup = 1}
                                                    {else}
                                                        <option value="{$organisatioElement.id}">{$organisatioElement.title}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Товары</label>
                                        <div class="col-sm-9 add-price-gift">
                                            [[-<input type="text" data-name="id_product" class="form-control add-code-placeholder" placeholder="айди продуктов">]]
        										<select   class="add-element-property select-list add-code-placeholder" multiple name="price_gifts" data-name="id_product" id="price_gifts">
        										    {set $gifts = '!getGifts' | snippet : []}
        										    {foreach $gifts as $gift}
                                                      <option value="{$gift.id}">
                                                          {$gift.pagetitle}
                                                        </option>
                                                    {/foreach}
        									    </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Товары для учета суммы</label>
                                        <div class="col-sm-9 add-price-gift">
                                            [[-<input type="text" data-name="id_product" class="form-control add-code-placeholder" placeholder="айди продуктов">]]
        										<select   class="add-element-property select-list add-code-placeholder" multiple name="products_to_gift" data-name="products_to_gift" id="products_to_gift">
        										    {set $gifts = '!getGifts' | snippet : []}
        										    {foreach $gifts as $gift}
                                                      <option value="{$gift.id}">
                                                          {$gift.pagetitle}
                                                        </option>
                                                    {/foreach}
        									    </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">От какой цены</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="value" class="form-control add-code-placeholder" placeholder="От">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Статус</label>
                                        <div class="col-sm-9 avail-parent">
                                            <div data-name="activity" data-id="1" class="add-code-placeholder btn activity-type active" style="margin-right: 10px;">Активный</div>
                                            <div data-name="activity" data-id="0" class="add-code-placeholder btn activity-type ">Заблокирован</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Дата начала акции</label>
                                        <div class="col-sm-9">
                                            <input name="datepicker" data-name="date_start" class="datepicker-default form-control picker__input picker__input--active add-code-placeholder" id="datepickerstart" readonly="" aria-haspopup="true" aria-expanded="true" aria-readonly="false" aria-owns="datepicker_root">
                                        </div>
                                    </div>
                                </div>
                                <div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Дата конца акции</label>
                                        <div class="col-sm-9">
                                            <input name="datepicker" data-name="date_end"  class="datepicker-default form-control picker__input picker__input--active add-code-placeholder" id="datepickerend" readonly="" aria-haspopup="true" aria-expanded="true" aria-readonly="false" aria-owns="datepicker_root">
                                        </div>
                                    </div>
                                </div>
                                [[-<div class="basic-form">
                                    <div class="form-group row align-center">
                                        <label class="col-sm-3 col-form-label">Сколько раз можно использовать</label>
                                        <div class="col-sm-9">
                                            <input type="text" data-name="use_count" class="form-control add-code-placeholder">
                                        </div>
                                    </div>
                                </div>]]
                                <div class="basic-form">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input data-name="image" class="photo-dir add-code-placeholder add-code-placeholder-input" type="hidden">
                                            <input data-id="{$promocode.id_promo}" accept="image/*" type="file" class="file-drop-newcode custom-file-input">
                                            <label class="custom-file-label">Выберите фотографию</label>
                                        </div>
    								</div>
    								<div class="loder-items-title loder-items-title-add-code-placeholder">
    								    Фотография добавлена
    								</div>
    								<div class="loader-items loader-items-add-code-placeholder"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-dismiss="modal">Закрыть</button>
                    <button  type="button" class="btn btn-primary save-new-promocode">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
<div class="alert alert-success alert-dismissible fade success-changes">
									<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><polyline points="9 11 12 14 22 4"></polyline><path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path></svg>	
									<strong>Принято!</strong> Сохранения изменены.
									<button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                    </button>
								</div>
    <script src="/admin/vendor/moment/moment.min.js"></script>
    <script src="/admin/vendor/global/global.min.js"></script>
	<script src="/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/admin/vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="/admin/js/custom.min.js"></script>
	<script src="/admin/js/deznav-init.js"></script>
    <script src="/admin/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="/admin/vendor/moment/moment.min.js"></script>
    <script src="/admin/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- clockpicker -->
    <script src="/admin/vendor/clockpicker/js/bootstrap-clockpicker.min.js"></script>
    <!-- asColorPicker -->
    <script src="/admin/vendor/jquery-asColor/jquery-asColor.min.js"></script>
    <script src="/admin/vendor/jquery-asGradient/jquery-asGradient.min.js"></script>
    <script src="/admin/vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js"></script>
    <!-- Material color picker -->
    <script src="/admin/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- pickdate -->
    <script src="/admin/vendor/pickadate/picker.js"></script>
    <script src="/admin/vendor/pickadate/picker.time.js"></script>
    <script src="/admin/vendor/pickadate/picker.date.js"></script>

    <script src="assets/components/ajaxform/js/lib/jquery.jgrowl.min.js"></script>

    <!-- Daterangepicker -->
    <script src="/admin/js/plugins-init/bs-daterange-picker-init.js"></script>
    <!-- Clockpicker init -->
    <script src="/admin/js/plugins-init/clock-picker-init.js"></script>
    <!-- asColorPicker init -->
    <script src="/admin/js/plugins-init/jquery-asColorPicker.init.js"></script>
    <!-- Material color picker init -->
    <script src="/admin/js/plugins-init/material-date-picker-init.js"></script>
    <!-- Pickdate -->
    <script src="/admin/js/plugins-init/pickadate-init.js"></script>
	<!-- Apex Chart -->
	<script src="/admin/vendor/apexchart/apexchart.js"></script>
	<script>
	    $(".remove-code").on("click",function(){
	        let delcode = {};
	        delcode["id"] = $(this).data("id");
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "[[++site_url]]admin-remove-promocode.json",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "3cde162e-5e08-d09b-8a1f-de8660e6019a"
                  },
                  "processData": false,
                  "data":  JSON.stringify(delcode)
                }
                
                $.ajax(settings).done(function (response) {
                  if (response){
                  console.log(response);
                      $('.success-changes').addClass("show");
                      location.reload();
                    }
                });
	    });
	</script>
	<script>
        $('.file-drop').on('change', function(){
        	files = this.files;
        	dataid = $(this).data("id");
        	let newfile = files[0].name;
        	var data = new FormData();
        	$.each( files, function( key, value ){
        		data.append( key, value );
        	});
        	data.append( 'my_file_upload', 1 );
	$.ajax({
		url         : '[[++site_url]]add-photo.json',
		type        : 'POST', // важно!
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
		contentType : false, 
		// функция успешного ответа сервера
		success     : function( respond, status, jqXHR ){

			// ОК - файлы загружены
			if( typeof respond.error === 'undefined' ){
				// выведем пути загруженных файлов в блок '.ajax-reply'
				var files_path = respond.files;
				var html = '';
				$.each( files_path, function( key, val ){
					 html += val +'<br>';
				} )
                let filedir = "[[++site_url]]uploads/"+newfile;
                $("<img src="+filedir+">").appendTo(".loader-items-"+dataid);
                $(".loder-items-title-"+dataid).show();
                $(".photo-dir.code-"+dataid).val(filedir);
			}
			// ошибка
			else {
				console.log('ОШИБКА: ' + respond.data );
			}
		},
		// функция ошибки ответа сервера
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});
        });
	</script>
    <script>
        $('.avail-type').on('click', function(){
            $('.avail-type').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        });
        $('.promo-type').on('click', function(){
            $('.promo-type').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
            if ($(this).data("id") == 1){
                $(".gift-block-parent").addClass("d-none");
                $('.discount-block-parent').removeClass("d-none");
                $(".id_product").val("");
            } else if ($(this).data("id") == 2){
                $(".gift-block-parent").removeClass("d-none");
                $('.discount-block-parent').addClass("d-none");
                $('.child_discount').val("");
            }
        });
        $('.activity-type').on('click', function(){
            $('.activity-type').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        });
            $('.save-promocode').on('click', function(){
                var id = $(this).data("id");
                str = "."+id;
                let promocode = {};
                promocode['id_promo'] = $(this).data("id2");
                $(str).each(function(){
                    if ($(this).attr("type") == "checkbox"){
                        promocode[$(this).data('name')] = $(this).prop("checked");
                    } else {
                        if ($(this).hasClass('avail-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else if ($(this).hasClass('activity-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else {
                            promocode[$(this).data('name')] = $(this).val();
                        }
                    }
                });
                console.log(promocode);
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "[[++site_url]]api/admin/changePromo",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache", 
                    "x-api-token": "htug9ji0oqpr329442u891q3sdasw"
                  },
                  "processData": false,
                  "data":  JSON.stringify(promocode)
                }
                $.ajax(settings).done(function (response) {
                  if (response){
                  console.log(response);
                      $('.success-changes').addClass("show");
                        function sayHi() {
                          location.reload();
                        }
                        setTimeout(sayHi, 1500);
                    }
                });
            });
    </script>
    <script>
        $(".save-new-promocode").on("click", function(){
            let promocode = {};
                $(".add-code-placeholder").each(function(){
                    if ($(this).attr("type") == "checkbox"){
                        promocode[$(this).data('name')] = $(this).prop("checked");
                    } else {
                        if ($(this).hasClass('avail-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else if ($(this).hasClass('activity-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else if ($(this).hasClass('promo-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else {
                            promocode[$(this).data('name')] = $(this).val();
                        }
                    }
                });
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "api/admin/addGiftFromCostPromo",
                  "method": "POST",
                  "headers": {
                    "x-api-token": "htug9ji0oqpr329442u891q3sdasw",
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                  },
                  "processData": false,
                  "data": JSON.stringify(promocode)
                }
                $.ajax(settings).done(function (response) {
                    $.jGrowl("Акция успешно добавлена.", { theme: 'saccess'});
                    function reload() {
                        location.reload();
                    }
                    setTimeout(reload, 1500);
                });
        });
    </script>
	<script>
        $('.file-drop-newcode').on('change', function(){
        	files = this.files;
        	dataid = $(this).data("id");
        	let newfile = files[0].name;
        	var data = new FormData();
        	$.each( files, function( key, value ){
        		data.append( key, value );
        	});
        	data.append( 'my_file_upload', 1 );
	$.ajax({
		url         : '[[++site_url]]add-photo.json',
		type        : 'POST', // важно!
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
		contentType : false, 
		// функция успешного ответа сервера
		success     : function( respond, status, jqXHR ){

			// ОК - файлы загружены
			if( typeof respond.error === 'undefined' ){
				// выведем пути загруженных файлов в блок '.ajax-reply'
				var files_path = respond.files;
				var html = '';
				$.each( files_path, function( key, val ){
					 html += val +'<br>';
				} )
                let filedir = "[[++site_url]]uploads/"+newfile;
                $("<img src="+filedir+">").appendTo(".loder-items-title-add-code-placeholder");
                $(".loder-items-title-add-code-placeholder").show();
                $(".add-code-placeholder-input").val(filedir);
			}
			// ошибка
			else {
				console.log('ОШИБКА: ' + respond.data );
			}
		},
		// функция ошибки ответа сервера
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});
        });
	</script>
<script>
    $(function(){
        $('#products').on('input', function(){
        var keys = $(this).val();
        console.log(keys);
        if (keys.length>2){
            $('.modal-results').slideDown();
            $.get('http://localmodx.appsj.su/searchproducts.html?query='+encodeURIComponent(keys), function(result){
                $('.modal-results').html(result);
                $(".hover-trigger").on("click", function(){
                    let test = $(this).data("id");
                    let idcode = $(this).parent().data("id")
                    $("<div class='res-prod-selected rescode-"+idcode+" selected-res"+test+"' data-id='"+$(this).data("id")+"'></div>").appendTo(".selected-results");
                    $('.selected-res'+test).html($(this).html());
                    $('.modal-results').slideUp();
                }); 
            });
        }
        else{
            $('.modal-results').slideUp();
        }
    });
    });
</script>
</html>
{/if}