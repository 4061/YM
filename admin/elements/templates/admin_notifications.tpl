{set $ismember = '!admin_security' | snippet : []}
{if $ismember}
<!DOCTYPE html>
<html lang="en">
[[$admin_head]]
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        [[$admin_nav_header]]
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        [[$admin_top_header]]
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        [[$admin_sidebar]]
        <!--**********************************
            Sidebar end
        ***********************************-->
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body minimal-font-size-content">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head d-flex mb-3 align-items-start">
					<div class="mr-auto d-none d-lg-block">
						<h2 class="text-black font-w600 mb-0">{$_modx->resource.pagetitle}</h2>
					</div>
					<div class="dropdown custom-dropdown d-none">
						<button type="button" class="btn btn-primary light d-flex align-items-center svg-btn" data-toggle="dropdown" aria-expanded="false">
							<svg width="16" class="scale5" height="16" viewBox="0 0 22 28" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.16647 27.9558C9.25682 27.9856 9.34946 28.0001 9.44106 28.0001C9.71269 28.0001 9.97541 27.8732 10.1437 27.6467L21.5954 12.2248C21.7926 11.9594 21.8232 11.6055 21.6746 11.31C21.526 11.0146 21.2236 10.8282 20.893 10.8282H13.1053V0.874999C13.1053 0.495358 12.8606 0.15903 12.4993 0.042327C12.1381 -0.0743215 11.7428 0.0551786 11.5207 0.363124L0.397278 15.7849C0.205106 16.0514 0.178364 16.403 0.327989 16.6954C0.477614 16.9878 0.77845 17.1718 1.10696 17.1718H8.56622V27.125C8.56622 27.5024 8.80816 27.8373 9.16647 27.9558ZM2.81693 15.4218L11.3553 3.58389V11.7032C11.3553 12.1865 11.7471 12.5782 12.2303 12.5782H19.1533L10.3162 24.479V16.2968C10.3162 15.8136 9.92444 15.4218 9.44122 15.4218H2.81693Z" fill="#2F4CDD"/></svg>
							<span class="fs-16 ml-3">All Status</span>
							<i class="fa fa-angle-down scale5 ml-3"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="#">2020</a>
							<a class="dropdown-item" href="#">2019</a>
							<a class="dropdown-item" href="#">2018</a>
							<a class="dropdown-item" href="#">2017</a>
							<a class="dropdown-item" href="#">2016</a>
						</div>
					</div>
					<div class="dropdown custom-dropdown ml-3 d-none">
						<button type="button" class="btn btn-primary light d-flex align-items-center svg-btn" data-toggle="dropdown" aria-expanded="false">
							<svg width="16" height="16" class="scale5" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.4281 2.856H21.8681V1.428C21.8681 0.56 21.2801 0 20.4401 0C19.6001 0 19.0121 0.56 19.0121 1.428V2.856H9.71606V1.428C9.71606 0.56 9.15606 0 8.28806 0C7.42006 0 6.86006 0.56 6.86006 1.428V2.856H5.57206C2.85606 2.856 0.560059 5.152 0.560059 7.868V23.016C0.560059 25.732 2.85606 28.028 5.57206 28.028H22.4281C25.1441 28.028 27.4401 25.732 27.4401 23.016V7.868C27.4401 5.152 25.1441 2.856 22.4281 2.856ZM5.57206 5.712H22.4281C23.5761 5.712 24.5841 6.72 24.5841 7.868V9.856H3.41606V7.868C3.41606 6.72 4.42406 5.712 5.57206 5.712ZM22.4281 25.144H5.57206C4.42406 25.144 3.41606 24.136 3.41606 22.988V12.712H24.5561V22.988C24.5841 24.136 23.5761 25.144 22.4281 25.144Z" fill="#2F4CDD"/></svg>
							<span class="fs-16 ml-3">Today</span>
							<i class="fa fa-angle-down scale5 ml-3"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="#">Monday</a>
							<a class="dropdown-item" href="#">Tuesday</a>
							<a class="dropdown-item" href="#">Wednesday</a>
							<a class="dropdown-item" href="#">Thursday</a>
							<a class="dropdown-item" href="#">Friday</a>
							<a class="dropdown-item" href="#">Saturday</a>
							<a class="dropdown-item" href="#">Sunday</a>
						</div>
					</div>
				</div>
				{set $notificationsTable = '!notificationsSegment' | snippet : []}
                
                <div class="row">
					<div class="col-12">
					    
					    <div class="control-block filtration-block">
					        <button type="button" class="btn btn-primary add-element" data-toggle="modal" data-target="#addleModal">
                              Добавить элемент
                            </button>
                            
                            <!-- Модальное окно добавления/редактирвоания рассылки -->
                            <div class="modal fade" id="addleModal" tabindex="-1" role="dialog" aria-labelledby="addleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Добавить элемент</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div id="form-add-element" data-id="0">
                                        <div class="row">
                                            
                                            <div class="form-block col-12">
                                                <label for="title_notification">Название</label><br>
                                                <input class="add-element-property" type="text" id="title_notification" value="" placeholder="Название рассылки">
                                            </div>
                                            
                                            <div class="form-block col-12 col-sm-4">
        										<label for="segment_setting_id">Настройки</label><br>
        										<select class="add-element-property select-list" name="segment-setting-id" id="segment_setting_id">
        										    {set $segmentList = '!segmentList' | snippet : []}
        										    {foreach $segmentList as $segmentOption}
                                                      <option value="{$segmentOption.id}">{$segmentOption.title}</option>
                                                    {/foreach}
        									    </select>
        									</div>
    										
    										<div class="form-block col-12 col-sm-4">
        										<label for="type_notification">Тип</label><br>
        										<select class="add-element-property select-list" name="type-notification" id="type_notification">
        									        <option value="sms">sms</option>
        									        <option value="push">push</option>
        									        <option value="email">email</option>
        									    </select>
        									</div>
        									
        									<div class="form-block col-12 col-sm-4">
        										<label for="timezone">Часовой пояс</label><br>
        										<input class="add-element-property" type="text" id="timezone" value="{'date_timezone' | option}" placeholder="Часовой пояс">
        									</div>
        									
        									<div class="form-block col-12 col-sm-6 d-none">
        										<label for="promo">Промокод</label><br>
        										<select class="add-element-property select-list" name="promo" id="promo">
        										    {set $promoList = '!getPromoList' | snippet : []}
            									    <option value="0">Не выбран</option>
    										        {foreach $promoList as $promo}
    										            <option value="{$promo.id}">{$promo.name}</option>
    										        {/foreach}
        									    </select>
        									</div>
        									
        									<div class="form-block col-12">
        										<label for="only_new_users">Рассылка новичкам</label><br>
        										<select class="add-element-property select-list" name="only-new-users" id="only_new_users">
        									        <option value="0">Нет</option>
        									        <option value="1">Да</option>
        									    </select>
        									</div>
    									    
    										<fieldset class="col-12">
    										    <legend>Время запуска</legend>
    										    <div class="row">
    										        <div class="form-block col-6 col-sm-3">
                										<label for="time_minutes">Минуты</label><br>
                										<input class="add-element-property" type="text" id="time_minutes" value="" placeholder="Минуты">
                									</div>
            										
            										<div class="form-block col-6 col-sm-3">
                										<label for="time_hours">Часы</label><br>
                										<input class="add-element-property" type="text" id="time_hours" value="" placeholder="Часы">
                									</div>
            										
            										<div class="form-block col-6 col-sm-3">
                										<label for="time_days">Дни</label><br>
                										<input class="add-element-property" type="text" id="time_days" value="" placeholder="Дни">
                									</div>
            										
            										<div class="form-block col-6 col-sm-3">
                										<label for="time_months">Месяцы</label><br>
                										<input class="add-element-property" type="text" id="time_months" value="" placeholder="Месяцы">
                									</div>
            										
            										<div class="form-block col-12">
                										<label for="time_days_week">Дни недели</label><br>
                										<input class="add-element-property" type="text" id="time_days_week" value="" placeholder="Дни недели">
                									</div>
                									
                									<div class="description">
                									    <p>Поля с минутами, часами, днями, месяцами, а так же с днями недели заполняются в особом виде. Здесь можно указать что бы рассылка проводилась в определённое время или повторялась в какое-то определённое время. </p><p><strong>Внимание! Необходимо что бы все поля были заполнены для времени</strong></p>
                									    <p>Нужно соблюдать особый синтаксим в записи:</p>
                									    <ul>
                									        <li>- Если мы укажем <strong>*</strong> в поле, то условие проверки срабатывает всегда</li>
                									        <li>- Если мы указываем <strong>1</strong> то условие проверки срабатывает в указанное значение</li>
                									        <li>- Если мы указываем <strong>1,2,5,6</strong> то условие проверки срабатывает в каждый раз когда значение совпадает</li>
                									        <li>- Если мы указываем <strong>*/2</strong> то условие проверки срабатывает в каждое второе значение времени</li>
                									        <li>- начало отсчёта у минут и часов с 0, у дней и месяцев с 1</li>
                									        <li>- У минут максимально указываемое значение <strong>60</strong>, у часов <strong>24</strong>, у дней <strong>363</strong>, у дней недели <strong>7</strong></li>
                									    </ul>
                									    <p>Напримеры:</p>
                									    <p>- если мы указали: минут 1, часы */3, дни *, месяцы *, дни недели *, то рассылка будет срабатывать каждый третий час каждого дня в 01 минуту;</br>
                									    - если мы указали: минут 30, часы *, дни *, месяцы *, дни недели 6,7, то рассылка будет срабатывать каждый час, в 30 минут - в выходные (суббота и воскресенье);</br>
                									    - если мы указали: минут 5, часы 5, дни *, месяцы *, дни недели *, то рассылка будет срабатывать каждый день в 5 часов 5 минут;</br>
                									    - если мы указали: минут 30, часы 12, дни 1, месяцы *, дни недели *, то рассылка будет срабатывать каждый первый день месяца, в 12 часов 30 минут;</br>
                									    </p>
                									</div>
            									</div>
        									</fieldset>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                    <button type="button" class="btn btn-primary btn-add-element" data-parent="form-add-element">Создать</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- Модальное окно добавления/редактирвоания рассылки -->
                            
                            <!-- Модальное окно шаблона -->
                            <div class="modal fade" id="templateModal" tabindex="-1" role="dialog" aria-labelledby="templateModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Шаблон рассылки</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div id="form-add-template" data-id="0">
                                        <div class="row">
                                            
                                            <div class="form-block col-12 template-block">
                                                <label for="template">Выберете шаблон</label><br>
                                                <select class="add-template-property select-list" name="template" id="template">
        										    <option value="0">Укажите шаблон</option>
        										    {set $templateList = '!getTemplateList' | snippet : []}
        										    {foreach $templateList as $templateElement}
                                                      <option value="{$templateElement.title}">{$templateElement.title}</option>
                                                    {/foreach}
        									    </select>
                                            </div>
                                            
                                            <div class="form-block col-12 col-sm-12 title-block">
        										<label for="template_name">Заголовок</label><br>
        										<input class="add-template-property" type="text" id="template_title" value="" placeholder="Заголовок в письме">
        									</div>
        									
        									<div class="form-block col-12 col-sm-12">
        										<label for="template_name">Текст в рассылке</label><br>
        										<textarea class="add-template-property" id="template_content" placeholder="Текст в рассылке"></textarea>
        									</div>
                                            
                                        </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                    <button type="button" class="btn btn-primary btn-add-template" data-parent="form-add-template">Сохранить шаблон</button>
                                  </div>
                                </div>
                              </div>
                            </div>
					        <!-- /Модальное окно шаблона -->
                            
					    </div>
					    
					    
					    
					    
						<div class="table-responsive table">
							<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;" data-table="SegmentNotifications">
								<thead>
									<tr>
										<th>id</th>
										<th>Название</th>
										<th>Сегмент</th>
										<th>Тип</th>
										<th class="d-none">Промокод</th>
										<th>Новичкам</th>
										<th>последняя рассылка</th>
										<th>Управление</th>
									</tr>
								</thead>
								<tbody>
								    {foreach $notificationsTable as $item}
                                    [[-<pre>{$.php.json_encode($item)}</pre>]]
                                    <tr class="notification-{$item.id} {if $item.active == 0}off{/if}" data-item="{$item.id}">
										<td class="element-property" data-name="id">{$item.id}</td>
										<td class="element-property edit-property" data-name="title_notification">{$item.title_notification}</td>
										<td class="element-property edit-property open-modal" data-name="segment_setting_id" data-value="{$item.segment_setting_id}">{$item.segment_name}</td>
										<td class="element-property edit-property edit-list" data-name="type_notification">
										    <select name="type-notification" id="">
										        <option value="sms" {if $item.type_notification == "sms"}selected{/if}>sms</option>
										        <option value="push" {if $item.type_notification == "push"}selected{/if}>push</option>
										        <option value="email" {if $item.type_notification == "email"}selected{/if}>email</option>
										    </select>
										</td>
										<td class="element-property edit-property edit-list d-none" data-name="promo" data-test="{$item.promo}">
										    <select name="promo" id="">
										        {set $promoList = '!getPromoList' | snippet : []}
										        <option value="0">Не выбран</option>
										        {foreach $promoList as $promo}
										            <option value="{$promo.id}" {if $promo.id == $item.promo}selected{/if}>{$promo.name}</option>
										        {/foreach}
										    </select>
										</td>
										<td class="element-property edit-property edit-list" data-name="only_new_users">
										    <select name="only_new_users" id="">
										        <option value="0" {if $item.only_new_users == "0"}selected{/if}>Нет</option>
										        <option value="1" {if $item.only_new_users == "1"}selected{/if}>Да</option>
										    </select>
										</td>
										<td class="element-property" data-name="date_last_notification">{$item.date_last_notification}</td>
										[[-<td>{$item.delivery_name}</td>]]
										<td data-element-id="{$item.id}" data-element="notification" width="110px">
										    <span class="btn control-btn" title="Редактировать рассылку" data-action="edit"><i class="far fa-edit"></i></span>
										    <span class="btn control-btn" title="Настройка шаблона рассылки" data-action="template" data-toggle="modal" data-noti="{$item.type_notification}" data-target="#templateModal"><i class="far fa-newspaper"></i></span>
										    <span class="btn control-btn" title="Отключить" data-action="disable"><i class="fas fa-power-off"></i></span>
										    <span class="btn control-btn warning" title="Удалить" data-action="delete"><i class="fas fa-trash-alt"></i></span>
										</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
                    </div>
				</div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="modal fade" id="savenewsettings" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cохранить настройки текущего сегмента</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>×</span>
                                                    </button>
                                                </div>
                                                  <div class="modal-body">
                                                    <div class="form-group-segment">
                                                        <label for="savename">Название</label>
                                                    <input id="savename" type="text" class="save-settings-segment-input form-control input-default">
                                                    </div>
                                                    <div class="form-group-segment">
                                                        <label for="savecomment">Комментарий</label>
                                                    <input id="savecomment" type="text" class="save-settings-segment-input form-control input-default">
                                                    </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                     <button type="button" class="btn light btn-primary segment-add-settings-btn">Сохранить</button>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
        <div class="modal fade" id="savenewsegment" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cохранить новый сегмент</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>×</span>
                                                    </button>
                                                </div>
                                              <div class="modal-body">
                                                <div class="form-group-segment">
                                                    <label for="newsegment_name">Название</label>
                                                <input id="newsegment_name" type="text" class="form-control input-default">
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="add-segment-btn btn light btn-primary">Сохранить</button>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
        <div class="modal fade" id="selectnewsettings" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cохранить настройки текущего сегмента</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>×</span>
                                                    </button>
                                                </div>
      <div class="modal-body">
          {set $segmentSettings = '!getSegmentSettings' | snippet : []}
          {foreach $segmentSettings as $setting}
          {if $setting.segment_id != 0}
          <div data-id="{$setting.segment_id}" class="segment-setting-group-wrapper">
              <div title="{$setting.segment_comment}" class="segment-setting-group">
                <div class="segment-setting">
                    {$setting.segment_name}
                </div>
                <div class="segment-setting">
                    {$setting.segment_createdon}
                </div>
                <div class="segment-setting segment-get-params">{$setting.segment_settings}</div>
              </div>
              <div class="segment-setting-change">
                    Изменить
                </div>
              <div class="segment-setting-delete">
                    Удалить
                </div>
              
          </div>
          {/if}
          {/foreach}
      </div>
                                            </div>
                                        </div>
                                    </div>
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="http://dexignzone.com/" target="_blank">DexignZone</a> 2020</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="/admin/vendor/global/global.min.js"></script>
	<script src="/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/admin/js/custom.min.js"></script>
	<script src="/admin/js/deznav-init.js"></script>
	<script src="/admin/vendor/chart.js/Chart.bundle.min.js"></script>
	<script src="admin/js/admin-actions.js?ver=2.1"></script>
	<script src="assets/components/ajaxform/js/lib/jquery.jgrowl.min.js"></script>
	
	<!-- Datatable -->
    <script src="/admin/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/admin/vendor/apexchart/apexchart.js"></script>
	[[-$scripts]]
	<script>
	(function($) {
	 
		var table = $('#example5').DataTable({
			searching: false,
			paging:true,
			select: false,
			//info: false,         
			lengthChange:false 
			
		});
		$('#example tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			
		});
	   
	})(jQuery);
	</script>

<script>
    $(document).ready(function(){
        $('.send-push-btn-wrapper').on('click', function(){
            var title = $('#push_title').val();
            var desc = $('#push_body').val();
            var pic = $('#push-pic').val();
            var link = $('#push-link').val();
            var usersArray = [];
            $('.hidden-input__user-id').each(function(){
                var user = $(this).val();
                usersArray.push(user);
            });
            $.get(
                "[[++site_url]]/send-push-to-segment.html",
                {
                    "pic":pic,
                    "title":title,
                    "desc":desc,
                    "link":link,
                    "users":usersArray
                },
                onPic
            );
                    function onPic(data){
                    console.log(data);
                    }
        });
    });
</script>
<script>
    $('.save-xls-btn').on('click', function(){
            var params = window
                .location
                .search
                .replace('?','')
                .split('&')
                .reduce(
                    function(p,e){
                        var a = e.split('=');
                        p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                        return p;
                    },
                    {}
                );
            var segment_id= params['segment_id'];
            $.get(
                "[[++site_url]]/getsegmentxls.html",
                {
                    "segment_id":segment_id
                },
                onSuccessConvert
            );
            function onSuccessConvert(data){
                window.location.href = data;
            }
    });
</script>
<script>
    $(function(){
        $('#products').on('input', function(){
        var keys = $(this).val();
        console.log(keys);
        if (keys.length>2){
            $('.modal-results').slideDown();
            $.get('[[++site_url]]/searchproducts.html?query='+encodeURIComponent(keys), function(result){
                $('.modal-results').html(result);
                $(".hover-trigger").on("click", function(){
                    let test = $(this).data("id");
                    $("<div class='res-prod-selected selected-res"+test+"' data-id='"+$(this).data("id")+"'></div>").appendTo(".selected-results");
                    $('.selected-res'+test).html($(this).html());
                    $('.modal-results').slideUp();
                }); 
            });
        }
        else{
            $('.modal-results').slideUp();
        }
    });
    });
</script>
<script>
    $(".all-send-push-btn-wrapper").on("click", function(){
            var title = $('#all-push_title').val();
            var body = $("#all-push_body").val();
            $.get(
                "[[++site_url]]/send-push-to-segment.html",
                {
                    "action":"all",
                    "title": title,
                    "body": body
                },
                onPic
            );
                    function onPic(data){
                    console.log(data);
                    }
    });
</script>
</body>
</html>
{/if}