{set $ismember = '!admin_security' | snippet : []}
{if $ismember}
<!DOCTYPE html>
<html lang="en">
[[$admin_head]]
<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        [[$admin_nav_header]]
        <!--**********************************
            Nav header end
        ***********************************-->
		
		<!--**********************************
            Header start
        ***********************************-->
        [[$admin_top_header]]
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        [[$admin_sidebar]]
        <!--**********************************
            Sidebar end
        ***********************************-->
		
		<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body minimal-font-size-content">
            <!-- row -->
			<div class="container-fluid">
				<div class="form-head d-flex mb-3 align-items-start">
					<div class="mr-auto d-none d-lg-block">
						<h2 class="text-black font-w600 mb-0">{$_modx->resource.pagetitle}</h2>
					</div>
					<div class="dropdown custom-dropdown d-none">
						<button type="button" class="btn btn-primary light d-flex align-items-center svg-btn" data-toggle="dropdown" aria-expanded="false">
							<svg width="16" class="scale5" height="16" viewBox="0 0 22 28" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.16647 27.9558C9.25682 27.9856 9.34946 28.0001 9.44106 28.0001C9.71269 28.0001 9.97541 27.8732 10.1437 27.6467L21.5954 12.2248C21.7926 11.9594 21.8232 11.6055 21.6746 11.31C21.526 11.0146 21.2236 10.8282 20.893 10.8282H13.1053V0.874999C13.1053 0.495358 12.8606 0.15903 12.4993 0.042327C12.1381 -0.0743215 11.7428 0.0551786 11.5207 0.363124L0.397278 15.7849C0.205106 16.0514 0.178364 16.403 0.327989 16.6954C0.477614 16.9878 0.77845 17.1718 1.10696 17.1718H8.56622V27.125C8.56622 27.5024 8.80816 27.8373 9.16647 27.9558ZM2.81693 15.4218L11.3553 3.58389V11.7032C11.3553 12.1865 11.7471 12.5782 12.2303 12.5782H19.1533L10.3162 24.479V16.2968C10.3162 15.8136 9.92444 15.4218 9.44122 15.4218H2.81693Z" fill="#2F4CDD"/></svg>
							<span class="fs-16 ml-3">All Status</span>
							<i class="fa fa-angle-down scale5 ml-3"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="#">2020</a>
							<a class="dropdown-item" href="#">2019</a>
							<a class="dropdown-item" href="#">2018</a>
							<a class="dropdown-item" href="#">2017</a>
							<a class="dropdown-item" href="#">2016</a>
						</div>
					</div>
					<div class="dropdown custom-dropdown ml-3 d-none">
						<button type="button" class="btn btn-primary light d-flex align-items-center svg-btn" data-toggle="dropdown" aria-expanded="false">
							<svg width="16" height="16" class="scale5" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.4281 2.856H21.8681V1.428C21.8681 0.56 21.2801 0 20.4401 0C19.6001 0 19.0121 0.56 19.0121 1.428V2.856H9.71606V1.428C9.71606 0.56 9.15606 0 8.28806 0C7.42006 0 6.86006 0.56 6.86006 1.428V2.856H5.57206C2.85606 2.856 0.560059 5.152 0.560059 7.868V23.016C0.560059 25.732 2.85606 28.028 5.57206 28.028H22.4281C25.1441 28.028 27.4401 25.732 27.4401 23.016V7.868C27.4401 5.152 25.1441 2.856 22.4281 2.856ZM5.57206 5.712H22.4281C23.5761 5.712 24.5841 6.72 24.5841 7.868V9.856H3.41606V7.868C3.41606 6.72 4.42406 5.712 5.57206 5.712ZM22.4281 25.144H5.57206C4.42406 25.144 3.41606 24.136 3.41606 22.988V12.712H24.5561V22.988C24.5841 24.136 23.5761 25.144 22.4281 25.144Z" fill="#2F4CDD"/></svg>
							<span class="fs-16 ml-3">Today</span>
							<i class="fa fa-angle-down scale5 ml-3"></i>
						</button>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="#">Monday</a>
							<a class="dropdown-item" href="#">Tuesday</a>
							<a class="dropdown-item" href="#">Wednesday</a>
							<a class="dropdown-item" href="#">Thursday</a>
							<a class="dropdown-item" href="#">Friday</a>
							<a class="dropdown-item" href="#">Saturday</a>
							<a class="dropdown-item" href="#">Sunday</a>
						</div>
					</div>
				</div>
				{set $cityTable = '!getCityList' | snippet : []}
                
                <div class="row">
					<div class="col-12">
					    
						<div class="table-responsive table">
							<table id="example5" class="display mb-4 dataTablesCard" style="min-width: 845px;" data-table="modResource">
								<thead>
									<tr>
										<th>id</th>
										<th>Город</th>
										<th>Максимальная сумма заказа</th>
										<th>Управление</th>
									</tr>
								</thead>
								<tbody>
								    {foreach $cityTable as $city}
                                    [[-<pre>{$.php.json_encode($item)}</pre>]]
                                    <tr class="user-{$city.id} {if $city.published == 0}off{/if}" data-item="{$city.id}">
										<td class="element-property" data-name="id">{$city.id}</td>
										<td class="element-property" data-name="pagetitle">{$city.pagetitle}</td>
										<td class="element-property edit-property" data-name="tvMaxPrice">{$city.tvMaxPrice}</td>
										<td data-element-id="{$user.id}" data-element="modResource">
										    [[-<span class="btn control-btn" title="Отключить" data-action="disable"><i class="fas fa-power-off"></i></span>]]
										    [[-<span class="btn control-btn warning" title="Удалить" data-action="delete"><i class="fas fa-trash-alt"></i></span>]]
										</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
						</div>
                    </div>
				</div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <!--**********************************
            Footer start
        ***********************************-->
        <div class="modal fade" id="savenewsettings" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cохранить настройки текущего сегмента</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>×</span>
                                                    </button>
                                                </div>
                                                  <div class="modal-body">
                                                    <div class="form-group-segment">
                                                        <label for="savename">Название</label>
                                                    <input id="savename" type="text" class="save-settings-segment-input form-control input-default">
                                                    </div>
                                                    <div class="form-group-segment">
                                                        <label for="savecomment">Комментарий</label>
                                                    <input id="savecomment" type="text" class="save-settings-segment-input form-control input-default">
                                                    </div>
                                                  </div>
                                                  <div class="modal-footer">
                                                     <button type="button" class="btn light btn-primary segment-add-settings-btn">Сохранить</button>
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
        <div class="modal fade" id="savenewsegment" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cохранить новый сегмент</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>×</span>
                                                    </button>
                                                </div>
                                              <div class="modal-body">
                                                <div class="form-group-segment">
                                                    <label for="newsegment_name">Название</label>
                                                <input id="newsegment_name" type="text" class="form-control input-default">
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="add-segment-btn btn light btn-primary">Сохранить</button>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
        <div class="modal fade" id="selectnewsettings" style="display: none;" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Cохранить настройки текущего сегмента</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>×</span>
                                                    </button>
                                                </div>
      <div class="modal-body">
          {set $segmentSettings = '!getSegmentSettings' | snippet : []}
          {foreach $segmentSettings as $setting}
          {if $setting.segment_id != 0}
          <div data-id="{$setting.segment_id}" class="segment-setting-group-wrapper">
              <div title="{$setting.segment_comment}" class="segment-setting-group">
                <div class="segment-setting">
                    {$setting.segment_name}
                </div>
                <div class="segment-setting">
                    {$setting.segment_createdon}
                </div>
                <div class="segment-setting segment-get-params">{$setting.segment_settings}</div>
              </div>
              <div class="segment-setting-change">
                    Изменить
                </div>
              <div class="segment-setting-delete">
                    Удалить
                </div>
              
          </div>
          {/if}
          {/foreach}
      </div>
                                            </div>
                                        </div>
                                    </div>
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="http://dexignzone.com/" target="_blank">DexignZone</a> 2020</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="/admin/vendor/global/global.min.js"></script>
	<script src="/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/admin/js/custom.min.js"></script>
	<script src="/admin/js/deznav-init.js"></script>
	<script src="/admin/vendor/chart.js/Chart.bundle.min.js"></script>
	<!--script src="admin/js/admin-actions.js"></script -->
	<script src="assets/components/ajaxform/js/lib/jquery.jgrowl.min.js"></script>
	
	<!-- Datatable -->
    <script src="/admin/vendor/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/admin/vendor/apexchart/apexchart.js"></script>
	[[$scripts]]
	<script>
	(function($) {
	 
		var table = $('#example5').DataTable({
			searching: false,
			paging:true,
			select: false,
			//info: false,         
			lengthChange:false 
			
		});
		$('#example tbody').on('click', 'tr', function () {
			var data = table.row( this ).data();
			
		});
	   
	})(jQuery);
	</script>

<script>
    $(document).ready(function(){
        $('.send-push-btn-wrapper').on('click', function(){
            var title = $('#push_title').val();
            var desc = $('#push_body').val();
            var pic = $('#push-pic').val();
            var link = $('#push-link').val();
            var usersArray = [];
            $('.hidden-input__user-id').each(function(){
                var user = $(this).val();
                usersArray.push(user);
            });
            $.get(
                "[[++site_url]]/send-push-to-segment.html",
                {
                    "pic":pic,
                    "title":title,
                    "desc":desc,
                    "link":link,
                    "users":usersArray
                },
                onPic
            );
                    function onPic(data){
                    console.log(data);
                    }
        });
    });
</script>
<script>
    $('.save-xls-btn').on('click', function(){
            var params = window
                .location
                .search
                .replace('?','')
                .split('&')
                .reduce(
                    function(p,e){
                        var a = e.split('=');
                        p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                        return p;
                    },
                    {}
                );
            var segment_id= params['segment_id'];
            $.get(
                "[[++site_url]]/getsegmentxls.html",
                {
                    "segment_id":segment_id
                },
                onSuccessConvert
            );
            function onSuccessConvert(data){
                window.location.href = data;
            }
    });
</script>
<script>
    $(function(){
        $('#products').on('input', function(){
        var keys = $(this).val();
        console.log(keys);
        if (keys.length>2){
            $('.modal-results').slideDown();
            $.get('[[++site_url]]/searchproducts.html?query='+encodeURIComponent(keys), function(result){
                $('.modal-results').html(result);
                $(".hover-trigger").on("click", function(){
                    let test = $(this).data("id");
                    $("<div class='res-prod-selected selected-res"+test+"' data-id='"+$(this).data("id")+"'></div>").appendTo(".selected-results");
                    $('.selected-res'+test).html($(this).html());
                    $('.modal-results').slideUp();
                }); 
            });
        }
        else{
            $('.modal-results').slideUp();
        }
    });
    });
</script>
<script>
    $(".all-send-push-btn-wrapper").on("click", function(){
            var title = $('#all-push_title').val();
            var body = $("#all-push_body").val();
            $.get(
                "[[++site_url]]/send-push-to-segment.html",
                {
                    "action":"all",
                    "title": title,
                    "body": body
                },
                onPic
            );
                    function onPic(data){
                    console.log(data);
                    }
    });
</script>
</body>
</html>
{/if}