{set $ismember = '!admin_security' | snippet : []}
{if $ismember}
<!DOCTYPE html>
<html lang="en">

[[$admin_head]]

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        [[$admin_nav_header]]
		<div style="display:none;">[[!msMiniCart]]</div>
		<!--**********************************
            Chat box start
  
		<!--**********************************
            Chat box End
        ***********************************-->


		
		
        <!--**********************************
            Header start
        ***********************************-->
        [[$admin_top_header]]
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->
        [[$admin_sidebar]]

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <!-- row -->
                <div class="row page-titles mx-0">
                        <div class="col-sm-6 p-md-0">
                            <div class="welcome-text d-flex">
                                <h4>{$_modx->resource.pagetitle}</h4>
                            </div>
                        </div>
                    </div>
                <fieldset class="form-group">
                                            <div class="row">
                                                {set $iiko = '!admin_integrations' | snippet : []}
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input id="iiko-true" value="true" class="form-check-input bonus-system-val" type="radio" name="gridRadios" {if $iiko} checked {/if}>
                                                        <label for="iiko-true" class="form-check-label">
                                                            Вкл
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input id="iiko-false" value="false" class="form-check-input bonus-system-val" type="radio" name="gridRadios" {if !$iiko} checked {/if}>
                                                        <label for="iiko-false" class="form-check-label">
                                                            Выкл
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <button type="button" class="btn btn-success save-bonus-system">Сохранить</button>
                                        <h2>Обновление номенклатуры</h2>
                            <div class="welcome-text d-flex">
                                
                                <div class="update-system-wrapper">
                                <button type="button" data-action="category" class="btn btn-success update-system-btn">Обновить категории</button>
                                <button type="button" data-action="products" class="btn btn-success update-system-btn">Обновить продукты</button>
                                <button type="button" data-action="photos" class="btn btn-success update-system-btn">Обновить фотографии</button>
                                <button type="button" data-action="prices" class="btn btn-success update-system-btn">Обновить цены</button>
                                </div>
                            </div>
            </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed &amp; Developed by <a href="http://dexignzone.com/" target="_blank">DexignZone</a> 2020</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        <!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->

        
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
  





</body>
    <script src="/admin/vendor/moment/moment.min.js"></script>
    <script src="/admin/vendor/global/global.min.js"></script>
	<script src="/admin/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="/admin/vendor/chart.js/Chart.bundle.min.js"></script>
    <script src="/admin/js/custom.min.js"></script>
	<script src="/admin/js/deznav-init.js"></script>
    <script src="/admin/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="/admin/vendor/moment/moment.min.js"></script>
    <script src="/admin/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- clockpicker -->
    <script src="/admin/vendor/clockpicker/js/bootstrap-clockpicker.min.js"></script>
    <!-- asColorPicker -->
    <script src="/admin/vendor/jquery-asColor/jquery-asColor.min.js"></script>
    <script src="/admin/vendor/jquery-asGradient/jquery-asGradient.min.js"></script>
    <script src="/admin/vendor/jquery-asColorPicker/js/jquery-asColorPicker.min.js"></script>
    <!-- Material color picker -->
    <script src="/admin/vendor/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- pickdate -->
    <script src="/admin/vendor/pickadate/picker.js"></script>
    <script src="/admin/vendor/pickadate/picker.time.js"></script>
    <script src="/admin/vendor/pickadate/picker.date.js"></script>



    <!-- Daterangepicker -->
    <script src="/admin/js/plugins-init/bs-daterange-picker-init.js"></script>
    <!-- Clockpicker init -->
    <script src="/admin/js/plugins-init/clock-picker-init.js"></script>
    <!-- asColorPicker init -->
    <script src="/admin/js/plugins-init/jquery-asColorPicker.init.js"></script>
    <!-- Material color picker init -->
    <script src="/admin/js/plugins-init/material-date-picker-init.js"></script>
    <!-- Pickdate -->
    <script src="/admin/js/plugins-init/pickadate-init.js"></script>
	<!-- Apex Chart -->
	<script src="/admin/vendor/apexchart/apexchart.js"></script>
	<script>
	    $(".remove-code").on("click",function(){
	        let delcode = {};
	        delcode["id"] = $(this).data("id");
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "[[++site_url]]admin-remove-promocode.json",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "3cde162e-5e08-d09b-8a1f-de8660e6019a"
                  },
                  "processData": false,
                  "data":  JSON.stringify(delcode)
                }
                
                $.ajax(settings).done(function (response) {
                  if (response){
                  console.log(response);
                      $('.success-changes').addClass("show");
                      $(".tr-code-"+response).fadeOut();
                    }
                });
	    });
	</script>
	<script>
        $('.file-drop').on('change', function(){
        	files = this.files;
        	dataid = $(this).data("id");
        	let newfile = files[0].name;
        	var data = new FormData();
        	$.each( files, function( key, value ){
        		data.append( key, value );
        	});
        	data.append( 'my_file_upload', 1 );
	$.ajax({
		url         : '[[++site_url]]add-photo.json',
		type        : 'POST', // важно!
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
		contentType : false, 
		// функция успешного ответа сервера
		success     : function( respond, status, jqXHR ){

			// ОК - файлы загружены
			if( typeof respond.error === 'undefined' ){
				// выведем пути загруженных файлов в блок '.ajax-reply'
				var files_path = respond.files;
				var html = '';
				$.each( files_path, function( key, val ){
					 html += val +'<br>';
				} )
                let filedir = "[[++site_url]]uploads/"+newfile;
                $("<img src="+filedir+">").appendTo(".loader-items-"+dataid);
                $(".loder-items-title-"+dataid).show();
                $(".photo-dir.code-"+dataid).val(filedir);
			}
			// ошибка
			else {
				console.log('ОШИБКА: ' + respond.data );
			}
		},
		// функция ошибки ответа сервера
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});
        });
	</script>
    <script>
        $('.avail-type').on('click', function(){
            $('.avail-type').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        });
        $('.promo-type').on('click', function(){
            $('.promo-type').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
            if ($(this).data("id") == 1){
                $(".gift-block-parent").addClass("d-none");
                $('.discount-block-parent').removeClass("d-none");
                $(".id_product").val("");
            } else if ($(this).data("id") == 2){
                $(".gift-block-parent").removeClass("d-none");
                $('.discount-block-parent').addClass("d-none");
                $('.child_discount').val("");
            }
        });
        $('.activity-type').on('click', function(){
            $('.activity-type').each(function(){
                $(this).removeClass('active');
            });
            $(this).addClass('active');
        });
            $('.save-promocode').on('click', function(){
                var id = $(this).data("id");
                str = "."+id;
                let promocode = {};
                promocode['id_promo'] = $(this).data("id2");
                $(str).each(function(){
                    if ($(this).attr("type") == "checkbox"){
                        promocode[$(this).data('name')] = $(this).prop("checked");
                    } else {
                        if ($(this).hasClass('avail-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else if ($(this).hasClass('activity-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else {
                            promocode[$(this).data('name')] = $(this).val();
                        }
                    }
                });
                console.log(promocode);
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "[[++site_url]]admin-change-promocode.json",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "3cde162e-5e08-d09b-8a1f-de8660e6019a"
                  },
                  "processData": false,
                  "data":  JSON.stringify(promocode)
                }
                
                $.ajax(settings).done(function (response) {
                  if (response){
                  console.log(response);
                      $('.success-changes').addClass("show");
                        function sayHi() {
                          location.reload();
                        }
                        setTimeout(sayHi, 1500);
                    }
                });
            });
    </script>
    <script>
        $(".save-new-promocode").on("click", function(){
            let promocode = {};
                $(".add-code-placeholder").each(function(){
                    if ($(this).attr("type") == "checkbox"){
                        promocode[$(this).data('name')] = $(this).prop("checked");
                    } else {
                        if ($(this).hasClass('avail-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else if ($(this).hasClass('activity-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else if ($(this).hasClass('promo-type')){
                            if ($(this).hasClass("active")){
                                promocode[$(this).data('name')] = $(this).data("id");
                            }
                        } else {
                            promocode[$(this).data('name')] = $(this).val();
                        }
                    }
                });
                finalobj = {};
                for (key in promocode){
                    var value = promocode[key];
                    if (value){
                        finalobj[key]=value;
                    }    
                }
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "[[++site_url]]admin-add-promocode.json",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "postman-token": "3cde162e-5e08-d09b-8a1f-de8660e6019a"
                  },
                  "processData": false,
                  "data":  JSON.stringify(finalobj)
                }
                
                $.ajax(settings).done(function (response) {
                  if (response){
                      console.log(response);
                     $('.success-changes').addClass("show");
                        function sayHi() {
                          location.reload();
                        }
                        setTimeout(sayHi, 1500);
                    }
                });
        });
    </script>
	<script>
        $('.file-drop-newcode').on('change', function(){
        	files = this.files;
        	dataid = $(this).data("id");
        	let newfile = files[0].name;
        	var data = new FormData();
        	$.each( files, function( key, value ){
        		data.append( key, value );
        	});
        	data.append( 'my_file_upload', 1 );
	$.ajax({
		url         : '[[++site_url]]add-photo.json',
		type        : 'POST', // важно!
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
		contentType : false, 
		// функция успешного ответа сервера
		success     : function( respond, status, jqXHR ){

			// ОК - файлы загружены
			if( typeof respond.error === 'undefined' ){
				// выведем пути загруженных файлов в блок '.ajax-reply'
				var files_path = respond.files;
				var html = '';
				$.each( files_path, function( key, val ){
					 html += val +'<br>';
				} )
                let filedir = "[[++site_url]]uploads/"+newfile;
                $("<img src="+filedir+">").appendTo(".loder-items-title-add-code-placeholder");
                $(".loder-items-title-add-code-placeholder").show();
                $(".add-code-placeholder-input").val(filedir);
			}
			// ошибка
			else {
				console.log('ОШИБКА: ' + respond.data );
			}
		},
		// функция ошибки ответа сервера
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});
        });
	</script>
	<script>
	    $(".save-bonus-system").on("click", function(){
	    let obj = {};
	        $(".bonus-system-val").each(function(){
	            if ($(this).prop("checked")){
	                obj[$(this).val()] = true;
	            } else {
	                obj[$(this).val()] = false;
	            }
	        });
	        console.log(obj);
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "[[++site_url]]api/admin/changeIntegrationSystem",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "x-api-token": "[[++x-api-token]]"
                  },
                  "processData": false,
                  "data":  JSON.stringify(obj)
                }
                
                $.ajax(settings).done(function (response) {
                  if (response){
                  console.log(response);
                      $('.success-changes').addClass("show");
                    }
                });
	    });
	</script>
	<script>
	    $(".update-system-btn").on("click", function(){
	        let data = {
	            "action": $(this).data("action")
	        }
                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "/api/admin/updateIntegrationSystem",
                  "method": "POST",
                  "headers": {
                    "content-type": "application/json",
                    "cache-control": "no-cache",
                    "x-api-token": "[[++x-api-token]]"
                  },
                  "processData": false,
                  "data":  JSON.stringify(data)
                }
                
                $.ajax(settings).done(function (response) {
                    if (response){
                        console.log(response);
                    }
                });
	    })
	</script>
</html>
{/if}