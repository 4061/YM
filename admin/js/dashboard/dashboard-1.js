(function($) {
    /* "use strict" */


 var dzChartlist = function(){
	
	var screenWidth = $(window).width();
		
	
	var activityBar = function(){
		var activity = document.getElementById("activity");
		let daysFirst = $(".order-list-date").attr("data-list").split(",");
		
		if (activity !== null) {
			var activityData = [{
					first: daysFirst
				},
				{
					first: daysFirst
				},
				{
					first: daysFirst
				},
				{
					first: daysFirst
				}
			];
			activity.height = 170;
			
			var config = {
				type: "bar",
				data: {
					labels: [
						"01",
						"02",
						"03",
						"04",
						"05",
						"06",
						"07",
						"08",
						"09",
						"10",
						"11",
						"12",
						"13",
						"14",
						"15",
						"16",
						"17",
						"18",
						"19",
						"20",
						"21",
						"22",
						"23",
						"24",
						"25",
						"26",
						"27",
						"28",
						"29",
						"30",
						"31"
					],
					datasets: [
						{
							label: "Количество заказов",
							data: daysFirst,
							borderColor: 'rgba(47, 76, 221, 1)',
							borderWidth: "0",
							barThickness:'flex',
							backgroundColor: 'rgba(47, 76, 221, 1)',
							minBarLength:10
						}
					]
				},
				options: {
					responsive: true,
					maintainAspectRatio: false,
					
					legend: {
						display: false
					},
					scales: {
						yAxes: [{
							gridLines: {
								color: "rgba(233,236,255,1)",
								drawBorder: true
							},
							ticks: {
								fontColor: "#3e4954",
								 max: 60,
                min: 0,
                stepSize: 20
							},
						}],
						xAxes: [{
							barPercentage: 0.3,
							
							gridLines: {
								display: false,
								zeroLineColor: "transparent"
							},
							ticks: {
								stepSize: 20,
								fontColor: "#3e4954",
								fontFamily: "Nunito, sans-serif"
							}
						}]
					},
					tooltips: {
						mode: "index",
						intersect: false,
						titleFontColor: "#888",
						bodyFontColor: "#555",
						titleFontSize: 12,
						bodyFontSize: 15,
						backgroundColor: "rgba(255,255,255,1)",
						displayColors: true,
						xPadding: 10,
						yPadding: 7,
						borderColor: "rgba(220, 220, 220, 1)",
						borderWidth: 1,
						caretSize: 6,
						caretPadding: 10
					}
				}
			};

			var ctx = document.getElementById("activity").getContext("2d");
			var myLine = new Chart(ctx, config);

			var items = document.querySelectorAll("#user-activity .nav-tabs .nav-item");
			items.forEach(function(item, index) {
				item.addEventListener("click", function() {
					config.data.datasets[0].data = activityData[index].first;
					myLine.update();
				});
			});
		}
	}
	var donutChart = function(){
		$("span.donut").peity("donut", {
			width: "140",
			height: "140",
			stroke: "#4d89f9",
			strokeWidth: "10",
		});
	}
	
	var chartBar = function(nameBlock){
		
		var options = {
			  series: [
				{
				  name: 'Заказы',
				  data: $("#" + nameBlock + " .data-statistic-graphick").attr("data-cost").split(",")
				}
				
			],
				chart: {
				type: 'area',
				height: 350,
				
				toolbar: {
					show: false,
				},
				
			},
			plotOptions: {
			  bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'rounded'
			  },
			},
			colors:['#2f4cdd', '#b519ec', '#2bc155'],
			dataLabels: {
			  enabled: false,
			},
			markers: {
		shape: "circle",
		},
		
		
			legend: {
				show: true,
				fontSize: '12px',
				
				labels: {
					colors: '#000000',
					
				},
				position: 'top',
				horizontalAlign: 'left', 	
				markers: {
					width: 19,
					height: 19,
					strokeWidth: 0,
					strokeColor: '#fff',
					fillColors: undefined,
					radius: 4,
					offsetX: -5,
					offsetY: -5	
				}
			},
			stroke: {
			  show: true,
			  width: 4,
			  colors:['#2f4cdd', '#b519ec', '#2bc155'],
			},
			
			grid: {
				borderColor: '#eee',
			},
			xaxis: {
				
			  categories: $("#" + nameBlock + " .data-statistic-graphick").attr("data-month").split(","),
			  labels: {
				style: {
					colors: '#3e4954',
					fontSize: '13px',
					fontFamily: 'Poppins',
					fontWeight: 100,
					cssClass: 'apexcharts-xaxis-label',
				},
			  },
			  crosshairs: {
			  show: false,
			  }
			},
			yaxis: {
				labels: {
			   style: {
				  colors: '#3e4954',
				  fontSize: '13px',
				   fontFamily: 'Poppins',
				  fontWeight: 100,
				  cssClass: 'apexcharts-xaxis-label',
			  },
			  },
			},
			fill: {
			  opacity: 1
			},
			tooltip: {
			  y: {
				formatter: function (val) {
				  return val + " руб"
				}
			  }
			}
			};

			var chartBar = new ApexCharts(document.querySelector("#chartBar-" + nameBlock), options);
			chartBar.render();
	}
	
	var counterBar = function(){
		$(".counter").counterUp({
			delay: 30,
			time: 3000
		});
	}
	
	
	/* Function ============ */
		return {
			init:function(){
			},
			
			
			load:function(){
				activityBar();		
				donutChart();	
				chartBar("Month-graphick");
				chartBar("Week-graphick");
				chartBar("Day-graphick");
				counterBar();
			},
			
			resize:function(){
				
			}
		}
	
	}();

	jQuery(document).ready(function(){
	});
	
	/*$("body").on("click",".get-graphick", function(){
	    chartBar();
	});*/
		
	jQuery(window).on('load',function(){
		setTimeout(function(){
			dzChartlist.load();
		}, 1000); 
		
	});

	jQuery(window).on('resize',function(){
		
		
	});     

})(jQuery);