// обработка двойного нажатия курсора
$('table').on("dblclick",".edit-property:not(.open-modal), .edit-price",function(){
    if(!$(this).hasClass("edit-list") && !$(this).hasClass("more-text")) {
        let text = $(this).text();
        $(this).text("");
        $(this).append('<input class="edit-input" name="edit" value="' + text + '">');
        $(this).children(".edit-input").focus();
    } else if ($(this).hasClass("more-text")) {
        let text = $(this).children(".hidden-text").text();
        $(this).children(".abbreviated-text").text("");
        $(this).append('<input class="edit-input" name="edit" value="' + text + '">');
        $(this).children(".edit-input").focus();
    }
    if($(this).hasClass("edit-price")){
        let text = $(this).children().val();
        $(this).attr("data-oldprice",text);
        console.log(text);
    }
});

// функция по редактированияю параметра в таблице
function editElement(dataEdit) {
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/adminEditCell",
        data: JSON.stringify(dataEdit),
        success: function(data){
            //console.log(data);
            $.jGrowl(data['message'], { theme: 'saccess'}); // sticky: true,
        },
        error: function(errMsg) {
            
        }
    });
}

// обработка снятия фокуса с редактируемого поля
$('table').on("blur",".edit-input",function(){
    let text = $(this).val();
    let parent = $(this).parent();
    
    let dataEdit = {};
    
    dataEdit['table'] = $(this).closest("table").attr("data-table");
    dataEdit['callName'] = parent.attr("data-name");
    dataEdit['elemenetId'] = $(this).closest("tr").attr("data-item");
    dataEdit['newText'] = text;
    
    $(this).remove();
    
    if (parent.hasClass("more-text")) {
        parent.children(".hidden-text").text(text);
        parent.children(".abbreviated-text").text(text.substr(0, 100));
        //console.log(text.substr(1, 5));
    } else if (parent.hasClass("edit-price")) {
        dataEdit['table'] = "SoftjetsyncPrices";
        dataEdit['elemenetId'] = parent.attr("data-id");
        let priceid = parent.attr("data-idprice");
        let oldprice = parent.attr("data-oldprice");
        let oldPeiceElem = parent.closest("tr").find(".old_price-"+priceid);
        //console.log(oldprice);
        //console.log(oldPeiceElem.text());
        if(oldPeiceElem.text() == "0" && oldprice != text) {
            oldPeiceElem.text(oldprice);
            parent.text(text);
        } else if (oldPeiceElem.text() == text) {
            oldPeiceElem.text(0);
            parent.text(text);
        } else {
            parent.text(text);
        }
        
        let dataEditOldPrice = {};
        
        dataEditOldPrice['table'] = "SoftjetsyncPrices";
        dataEditOldPrice['callName'] = "old_price";
        dataEditOldPrice['elemenetId'] = dataEdit['elemenetId'];
        dataEditOldPrice['newText'] = oldPeiceElem.text();
        
        //console.log(dataEditOldPrice);
        
        editElement(dataEditOldPrice);
    } else {
        parent.text(text);
    }
    
    //console.log(JSON.stringify(dataEdit));
    editElement(dataEdit);

});

// Переключение цен для товаров по городам
$('.price-filter').on("click","a[role=option]",function(){
    let select = $(this).closest(".bootstrap-select").children("select");
    let name = select.attr("name");
    let tv = 0;
    let id = 0;
    
    if (name == "organisation-list") {
        tv = $("select#delivery-list").val();
        id = select.val();
    } else {
        tv = select.val();
        id = $("select#organisation-list").val();
    }
    
    //console.log(id);
    
    $(".element-property .prices").addClass("hidden");
    $(".element-property .old_prices").addClass("hidden");
    $(".element-property .prices[data-idorg="+id+"][data-tv="+tv+"]").removeClass("hidden");
    $(".element-property .old_prices[data-idorg="+id+"][data-tv="+tv+"]").removeClass("hidden");
});

// Подгрузка цен по нажатию на пагинацию
$('body').on("mousedown",".dataTables_paginate .paginate_button",function(){
    
    
    setTimeout(function (){
        
        tv = $("select#delivery-list").val();
        id = $("select#organisation-list").val();
    
        $(".element-property .prices").addClass("hidden");
        $(".element-property .old_prices").addClass("hidden");
        $(".element-property .prices[data-idorg="+id+"][data-tv="+tv+"]").removeClass("hidden");
        $(".element-property .old_prices[data-idorg="+id+"][data-tv="+tv+"]").removeClass("hidden");
        
        console.log(tv + " - " + id);
    
    }, 100);
    
});

// Изменение данных в БД через выпадающий список
$('.edit-property.edit-list').on("click","a[role=option]",function(){
    let select = $(this).closest(".bootstrap-select").children("select");
    
    let dataEdit = {};
    
    dataEdit['table'] = select.closest("table").attr("data-table");
    dataEdit['callName'] = select.closest("td").attr("data-name");
    dataEdit['elemenetId'] = select.closest("tr").attr("data-item");
    dataEdit['newText'] = select.val();
    
    console.log(JSON.stringify(dataEdit));
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/adminEditCell",
        data: JSON.stringify(dataEdit),
        success: function(data){
            if (dataEdit['table'] == "modUser") {
                select.closest("tr").removeClass("black-list-1").removeClass("black-list-2").removeClass("black-list-3").addClass("black-list-"+dataEdit['newText']);
                if(dataEdit['newText'] == 0) {select.closest("tr").removeClass("black-list");} else {select.closest("tr").addClass("black-list");}
            }
            $.jGrowl(data['message'], { theme: 'saccess'}); // sticky: true,
        },
        error: function(errMsg) {
            
        }
    });
});

// Снятие фокуса с поля
$("body").on("focus",".error",function(){
    $(this).removeClass("error");
});

// Смена категории
$('.category-block').on("click","a[role=option]",function(){
    let select = $(this).closest(".bootstrap-select").children("select");
    let idCategory = select.val();
    //console.log(window.location.href);
    document.location.href = window.location.href.split("?")[0] + "?categoryId=" + idCategory;
});


// Добавить элемент в базу/обновление
$(".modal").on("click",".btn-add-element",function(){
    let parentName = $(this).attr("data-parent");
    //console.log("Нажал - " + parentName);
    let errorCheck = true;
    let propertyMass = {};
    propertyMass["checkId"] = $(this).closest(".modal").find("#form-add-element").attr("data-id");
    console.log(propertyMass);
    // если имя родителя получено, то перебераем поля для передачи в php код
    if(parentName){
        $("#" + parentName + " .add-element-property:not(div)").each(function(i,elem) {
        	//console.log($(this).val());
        	if($(this).val() == "") {
        	    $(this).addClass("error");
        	    errorCheck = false;
        	} else {
        	    propertyMass[$(this).attr("id")] = $(this).val();
        	}
        });
    }
    
    //console.log(propertyMass);
    
    if(errorCheck){
        console.log(JSON.stringify(propertyMass));
        $.ajax({
            type: "POST",
            headers: {
                'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
            },
            url: "/api/admin/adminNotyficationAdd",
            data: JSON.stringify(propertyMass),
            success: function(data){
                $.jGrowl(data['message'], { theme: 'saccess'}); // sticky: true,
                $('#addleModal').modal('toggle');
            },
            error: function(errMsg) {
                
            }
        });
        
    } else {
        $.jGrowl("Заполнены не все поля!", { theme: 'error'});
    }
    
});
// удаляем элемент
$("body").on("click",".btn[data-action=delete]",function(){
    let dataDelete = {};
    
    dataDelete['bd'] = $(this).closest("table").attr("data-table");
    dataDelete['elemenetId'] = $(this).closest("tr").attr("data-item");
    
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/adminDeleteRow",
        data: JSON.stringify(dataDelete),
        success: function(data){
            //console.log(data);
            $.jGrowl(data['message'], { theme: 'saccess'}); // sticky: true,
        },
        error: function(errMsg) {
            
        }
    });
});
// Отключить элемент
$("body").on("click",".btn[data-action=disable]",function(){
    let dataDesabled = {};
    let parent = $(this).closest("tr");
    
    dataDesabled['table'] = $(this).closest("table").attr("data-table");
    dataDesabled['elemenetId'] = parent.attr("data-item");
    dataDesabled['toggle'] = (parent.hasClass("off"))? 1 : 0;
    
    console.log(dataDesabled);
    
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/adminDesabledRow",
        data: JSON.stringify(dataDesabled),
        success: function(data){
            console.log(data);
            parent.toggleClass("off");
            $.jGrowl(data['message'], { theme: 'saccess'}); // sticky: true,
        },
        error: function(errMsg) {
            
        }
    });
});

// Очистка модального окна при открытие по кнопке "добавить"
$("body").on("click",".btn.add-element",function(){
    let idElement = $("#form-add-element").attr("data-id");
    if(idElement != 0){
        $("#form-add-element").attr("data-id",0);
        
        $("#form-add-element .add-element-property:not(div)").each(function(i,elem) {
            
            if($(this).attr("id") != "timezone"){
                $(this).val("").attr("value","");
            }
        	
        });
    }
});

// добавить ID к модальному окну шаблона
$("body").on("click",".btn[data-action=template]",function(){
    let id = $(this).parent().attr("data-element-id");
    $("#form-add-template").attr("data-id",id);
    
    let propertyMass = {};
    propertyMass['id'] = id;
    
    console.log(propertyMass);
    
    if($(this).attr("data-noti") == "push") {
        $(".template-block").addClass("d-none");
    }
    if($(this).attr("data-noti") == "sms") {
        $(".template-block").addClass("d-none");
        $(".title-block").addClass("d-none");
    }
    if($(this).attr("data-noti") == "email") {
        $(".template-block").removeClass("d-none");
        $(".title-block").removeClass("d-none");
    }
    
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/getTemplate",
        data: JSON.stringify(propertyMass),
        success: function(data){
            console.log(data["data"]);
            
            if(data["data"]) {
                for (key in data["data"]) {
                    if(key != "template"){
                        $("#" + key).val(data["data"][key]);
                    } else {
                        let element = $("#" + key).children("option[value='" + data["data"][key] + "']");
                        element.prop('selected', true);
                        let textSelected = element.text();
                        $("button[data-id=" + key + "] .filter-option-inner-inner").text(textSelected);
                    }
                }
            } else {
                $("input.add-template-property,textarea.add-template-property").val("");
                $("select.add-template-property").children("option").prop('selected', false);
                $("select.add-template-property").children("option[value=0]").prop('selected', true);
                $("button[data-id=template] .filter-option-inner-inner").text("Укажите шаблон");
            }
            
        },
        error: function(errMsg) {
            
        }
    });
    
});


// Сохранение шаблона (создание/редактирование)
$("body").on("click",".btn.btn-add-template",function(){
    let parentName = $(this).attr("data-parent");
    let element = $(this).closest(".modal").find("#form-add-template");
    let key = "";
    let propertyMass = {};
    
    propertyMass["id"] = element.attr("data-id");
    $("#" + parentName + " .add-template-property:not(div)").each(function(i,elem) {
    	//console.log($(this).val());
    	key = $(this).attr("id");
    	propertyMass[key] = $(this).val();
    });
    
    //console.log(propertyMass);
    
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/adminTemplateAdd",
        data: JSON.stringify(propertyMass),
        success: function(data){
            //console.log(data);
            $.jGrowl(data['message'], { theme: 'saccess'}); // sticky: true,
            $('#templateModal').modal('toggle');
        },
        error: function(errMsg) {
            
        }
    });
    
});

// открытие модального окна для редактирования существующего объекта
$("body").on("click",".btn[data-action=edit]",function(){
    
    let parent = $(this).parent();
    
    let data = {
        "id" : parent.attr("data-element-id"),
        "table" : parent.closest("table").attr("data-table")
    };
    
    $.ajax({
        type: "POST",
        headers: {
            'x-api-token':'htug9ji0oqpr329442u891q3sdasw'
        },
        url: "/api/admin/adminEditItem",
        data: JSON.stringify(data),
        success: function(data){
            console.log(data['data']);
            
            let dataMass = data['data'][0];
            
            $("#form-add-element").attr("data-id",dataMass['id']);
            
            for (key in dataMass) {
                let element = $("#form-add-element .add-element-property#" + key);
                if(!element.hasClass("select-list")) {
                    element.val(dataMass[key]).attr("value",dataMass[key]);
                } else {
                    element.val(dataMass[key]);
            
                    if(dataMass[key]) {
                        nameCheck = element.children("option[value="+dataMass[key]+"]").text();
                        $("#form-add-element button[data-id="+key+"] .filter-option-inner-inner").text(nameCheck);
                    }
                }
            }
            
            $('#addleModal').modal('toggle');
        },
        error: function(errMsg) {
            
        }
    });
    
});

$(".content-body").on("click",".total-stats,.nav-item", function(){
    let idBlock = $(this).attr("date-id");
    console.log(idBlock);
    $(".date-block").removeClass("active");
    $("#"+idBlock).addClass("active");
});

$(".btn-add-elemen-order-status").on("click", function(){
    obj = {};
    obj["name"] = $("#status_name").val();
    obj["description"] = $("#status_description").val();
    obj["text_color"] = $("#status_text").val();
    obj["wrapper_color"] = $("#status_wrapper").val();
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "/api/courier/addOrderStatus",
      "method": "POST",
      "headers": {
        "x-api-token": "htug9ji0oqpr329442u891q3sdasw",
        "content-type": "application/json",
        "cache-control": "no-cache"
      },
      "processData": false,
      "data": JSON.stringify(obj)
    }
    
    $.ajax(settings).done(function (response) {
      console.log(response);
    });
});