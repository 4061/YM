<?php
define('MODX_API_MODE', true);
require __DIR__.'/index.php';
$settings = json_decode(file_get_contents(MODX_BASE_PATH."custom_settings.json"), true);
foreach ($settings as $setting){
    $setting_obj = $modx->getObject("modSystemSetting", array("key"=>$setting["key"]));
    if (is_array($setting["value"])){
        //$value = json_encode($setting["value"]);
        //$setting_obj->save();
    } else {
        if($setting_obj->key) {
            $setting_obj->set("value", $setting["value"]);
            $setting_obj->save();
        }
    }
    
}
$modx->cacheManager->refresh(array('system_settings' => array()));