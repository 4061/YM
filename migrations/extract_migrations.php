<?php
define('MODX_API_MODE', true);
require dirname(__DIR__, 1).'/index.php';
$migrations = json_decode(file_get_contents(__DIR__."/migrations.json"), true);
foreach ($migrations as $migration){
    $table = $migration['table'];
    $sql = "SHOW COLUMNS FROM $table";
    $table_columns = $modx->query($sql);
    $table_columns = $table_columns->fetchAll(PDO::FETCH_ASSOC);
    $master_fields = array_column($table_columns, 'Field');
    if (!in_array($migration['field'],$master_fields)){
        $modx->query($migration['sql']);
    }
}